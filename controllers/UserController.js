let UserController = function () { };
import moment from "moment";
import User_OTPS from "../models/User_OTPS";
import User_OTP_Tries from "../models/User_OTP_Tries";
import CommonController from "./CommonController";
import MessagesController from "./MessagesController";
import config from "../config/config";
import ApiMessages from "../models/ApiMessages";
import Users from "../models/Users";
import crypto from "crypto";
import schedule from 'node-schedule';
import uuid from "uuid";
import Store_SECTIONS from "../models/Store_SECTIONS";
import Store_Products from "../models/Store_Products";
import Store_MENU from "../models/Store_MENU";
import Orders from "../models/Orders";
import async from "async";
import Store_Branch from "../models/Store_Branch";
import Buyers from "../models/Buyers";
import Buyer_Address_Log from "../models/Buyer_Address_Log";
import Buyer_Orders from "../models/Buyer_Orders";
import Buyer_Share_Logs from "../models/Buyer_Share_Logs";
import { compileFunction } from "vm";
import Help_Data from "../models/Help_Data";
import Referal_Price from "../models/Referal_Price";
import Rating_Logs from "../models/Rating_Logs";
import Company_Wallet_Log from "../models/Company_Wallet_Log";
import Company_Wallet from "../models/Company_Wallet";
import Buyer_Images from "../models/Buyer_Images";
import AWSController from "./AWSController";
import Delivery_Price from "../models/Delivery_Price";
import Banner from "../models/Banner";
import Quote from "../models/Quote";
import Category from "../models/Category";
import ZONES from "../models/ZONES";
import Discounts from "../models/Discounts";
import User_Discount_Apply from "../models/User_Discount_Apply";
import { ValidationRequestList } from "twilio/lib/rest/api/v2010/account/validationRequest";
import Wishlist_Log from "../models/Wishlist_Log";
import Stack_Log from "../models/Stack_Log";
import Write_And_Earn_Log from "../models/Write_And_Earn_Log";
import FcmController from "./FcmController";
import Buyer_Notification_Log from "../models/Buyer_Notification_Log";
import Buyers_Current_Location from "../models/Buyers_Current_Location";
import Title_Management from "../models/Title_Management";
import Title_Management_Products from "../models/Title_Management_Products";
import Zone_Fees from "../models/Zone_Fees";
import AdminController from "./AdminController";
import { Boolify } from "node-boolify";
import Taxes from "../models/Taxes";
import Trips from "../models/Trips";
import NotificationController from "./NotificationController";



// UserController.List_All_Orders = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let query = {
//                     UserID: values.UserID
//                 };
//                 let toSkip = parseInt(values.skip);
//                 let toLimit = parseInt(values.limit);
//                 let sortOptions = {
//                     created_at: -1
//                 };
//                 if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
//                     sortOptions = values.sortOptions;
//                 };
//                 let Count = await Orders.countDocuments(query).lean().exec();
//                 let Result = await Orders.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
//                 async.eachSeries(Result, async (item, callback) => {
//                     try {
//                         let BranchData = await Store_Branch.findOne({ BranchID: item.BranchID }).lean();
//                         item.Store_Entity_Name = BranchData.Store_Entity_Name;
//                         item.Branch_Name = BranchData.Branch_Name;
//                         item.Branch_PhoneNumber = BranchData.Branch_PhoneNumber;
//                         item.BranchURL = (item.BranchURL == "") ? "" : config.S3URL + BranchData.BranchURL;
//                         item.Description = BranchData.Description;
//                         item.Address = BranchData.Address;
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve({ success: true, extras: { Count: Count, Data: Result } });
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Payment_Order_Update_Status = (PaymentData, Order_Status) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let oquery = {
//                     OrderID: PaymentData.notes.OrderID
//                 };
//                 let ochanges = {
//                     $set: {
//                         Order_Status: Order_Status,
//                         updated_at: new Date()
//                     },
//                     $push: {
//                         Complete_Razorpay_Payment_Information: PaymentData
//                     }
//                 };
//                 let updateOrder = await Orders.updateOne(oquery, ochanges).select().lean();
//                 resolve("Updated Successfully");
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Login = (values, UserData) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Password = String(values.Password);
//                 let PasswordSalt = UserData.PasswordSalt;
//                 let pass = Password + PasswordSalt;
//                 let PasswordHash = crypto.createHash('sha512').update(pass).digest("hex");
//                 if (PasswordHash === UserData.PasswordHash) {
//                     let fndupdquery = {
//                         USERID: UserData.USERID
//                     };
//                     let fndupdchanges = {
//                         $setOnInsert: {
//                             "USER_SESSIONS.SessionID": uuid.v4(),
//                             created_at: new Date(),
//                             updated_at: new Date()
//                         }
//                     };
//                     let fndupdoptions = {
//                         upsert: true,
//                         setDefaultsOnInsert: true,
//                         new: true
//                     }
//                     let Data = await Users.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -PasswordHash -PasswordSalt -__v').lean();
//                     resolve({ success: true, extras: { Status: "Login Successfully", Data: Data } });
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.INVALID_PASSWORD } })
//                 }
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Check_for_Phone_Number_Registered = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let query = {
//                     PhoneNumber: values.PhoneNumber
//                 };
//                 let Result = await Users.findOne(query).lean();
//                 if (Result == null) {
//                     reject({ success: false, extras: { msg: ApiMessages.PHONE_NUMBER_NOT_EXIST } })
//                 } else {
//                     resolve(Result);
//                 };
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Register = values => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Password = String(values.Password);
//                 let PasswordSalt = await CommonController.Random_OTP_Number();
//                 let pass = Password + PasswordSalt;
//                 let PasswordHash = crypto.createHash('sha512').update(pass).digest("hex");
//                 let fndupdquery = {
//                     PhoneNumber: values.PhoneNumber
//                 };
//                 let fndupdchanges = {
//                     $setOnInsert: {
//                         USERID: uuid.v4(),
//                         "USER_SESSIONS.SessionID": uuid.v4(),
//                         Name: values.Name,
//                         EmailID: values.EmailID,
//                         PasswordHash: PasswordHash,
//                         PasswordSalt: PasswordSalt,
//                         created_at: new Date(),
//                         updated_at: new Date()
//                     }
//                 };
//                 let fndupdoptions = {
//                     upsert: true,
//                     setDefaultsOnInsert: true,
//                     new: true
//                 }
//                 let Data = await Users.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -PasswordHash -PasswordSalt -__v').lean();
//                 resolve({ success: true, extras: { Status: "Registered Successfully", Data: Data } })
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Check_for_Phone_Already_Registered = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let query = {
//                     PhoneNumber: values.PhoneNumber
//                 };
//                 let Result = await Users.findOne(query).lean();
//                 if (Result == null) {
//                     resolve("Validated Successfully");
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.PHONE_NUMBER_ALREADY_REGISTERED } })
//                 };
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Check_for_User = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(() => {
//             try {
//                 let query = {
//                     USERID: values.UserID
//                 };
//                 let Result = Users.findOne(query).lean();
//                 if (Result != null) {
//                     resolve("Validated Successfully");
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.USER_NOT_FOUND } })
//                 };
//             } catch (error) {
//                 reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
//             }
//         });
//     });
// }

// UserController.Validate_User_OTP = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let obj1 = {
//                     $ne: null
//                 };
//                 let obj2 = {
//                     $eq: values.OTP
//                 }
//                 let OTP_Query = (String(config.SECRET_OTP_CODE) === String(values.OTP)) ? obj1 : obj2;
//                 let query = {
//                     PhoneNumber: values.PhoneNumber,
//                     OTP: OTP_Query,
//                     Latest: true
//                 };
//                 let Result = await User_OTPS.findOne(query).lean();
//                 if (Result == null) {
//                     reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
//                     let Data = {
//                         PhoneNumber: values.PhoneNumber,
//                         Time: new Date()
//                     };
//                     let SaveResult = await User_OTP_Tries(Data).save();
//                 } else {
//                     resolve("Validated Successfully");
//                     let RemoveTries = await User_OTP_Tries.deleteMany({
//                         PhoneNumber: values.PhoneNumber
//                     }).lean();
//                 };
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Check_for_User_OTP_Tries_Count = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
//                 let query = {
//                     PhoneNumber: values.PhoneNumber,
//                     Time: {
//                         $gte: time
//                     }
//                 };
//                 let Count = await User_OTP_Tries.countDocuments(query).lean().exec();
//                 if (Count <= config.OTP_TRIES_COUNT) {
//                     resolve('Validated Successfully');
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.OTP_TRIES_EXCEED_TRY_AFTER_SOME_TIME } });
//                 }
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }


// UserController.Check_for_OTP_Count = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let time = moment().subtract(config.OTP_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
//                 let query = {
//                     PhoneNumber: values.PhoneNumber,
//                     Time: {
//                         $gte: time
//                     }
//                 };
//                 let Count = await User_OTPS.countDocuments(query).lean();
//                 if (Count <= config.OTP_COUNT) {
//                     resolve('Validated Successfully')
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.OTP_REQUEST_EXCEED_TRY_AFTER_SOME_TIME } })
//                 }
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Generate_User_OTP_Send_Message = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let OTP = await CommonController.Random_OTP_Number();
//                 let Data = {
//                     PhoneNumber: values.PhoneNumber,
//                     OTP: OTP,
//                     Latest: true,
//                     Time: new Date()
//                 }
//                 let SaveResult = await User_OTPS(Data).save();
//                 resolve({ success: true, extras: { Status: "OTP Sent Successfully" } });
//                 let PhoneNumber = values.PhoneNumber;
//                 let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, OTP);
//                 let query = {
//                     PhoneNumber: values.PhoneNumber,
//                     OTP: { $ne: OTP }
//                 };
//                 let changes = {
//                     Latest: false
//                 };
//                 let UpdatedStatus = await User_OTPS.updateMany(query, changes).lean();
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Fetch_Complete_Menu_Section_Product_Data = (ProductID_Array) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Data = [];
//                 async.eachSeries(ProductID_Array, async (ProductID, callback) => {
//                     try {
//                         let item = new Object();
//                         let FetchData = await Store_Products.findOne({ ProductID: ProductID }).lean();
//                         item.ProductID = FetchData.ProductID;
//                         item.ProductName = FetchData.ProductName;
//                         item.ProductDescription = FetchData.ProductDescription;
//                         Data.push(item);
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve(Data);
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Fetch_Complete_Menu_Section_Data = (SectionData) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Data = [];
//                 async.eachSeries(SectionData, async (item, callback) => {
//                     try {
//                         let FetchData = await Store_SECTIONS.findOne({ SECTION_ID: item.SECTION_ID }).lean();
//                         item.SECTION_ID = FetchData.SECTION_ID;
//                         item.Sl_NO = FetchData.Sl_NO;
//                         item.SECTION_NAME = FetchData.SECTION_NAME;
//                         item.SECTION_DESCRIPTION = FetchData.SECTION_DESCRIPTION;
//                         item.MAX_SELECTION = FetchData.MAX_SELECTION;
//                         item.ProductData = await UserController.Fetch_Complete_Menu_Section_Product_Data(item.ProductID_Array);
//                         Data.push(item);
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve(Data);
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Fetch_Complete_Menu_Data = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Data = [];
//                 async.eachSeries(values.MenuData, async (item, callback) => {
//                     try {
//                         let FetchData = await Store_MENU.findOne({ MENU_ID: item.MENU_ID }).lean();
//                         item.MENU_ID = FetchData.MENU_ID;
//                         item.MENU_NAME = FetchData.MENU_NAME;
//                         item.MENU_DESCRIPTION = FetchData.MENU_DESCRIPTION;
//                         item.MENU_URL = FetchData.MENU_URL;
//                         item.MENU_TYPE = FetchData.MENU_TYPE;
//                         item.MENU_PRICE = FetchData.MENU_PRICE;
//                         item.NumberOfPlates = parseInt(item.NumberOfPlates);
//                         item.SectionData = await UserController.Fetch_Complete_Menu_Section_Data(item.Sectionvalues);
//                         Data.push(item);
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve(Data);
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// UserController.Create_Order = (values, TotalAmount) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let OrderID = uuid.v4();
//                 let Order_Number = await CommonController.GENERATE_TEN_DIGIT_INCREMENT_COUNTER_SEQUENCE("ORDER", "C");
//                 resolve({ success: true, extras: { Status: "Order Placed Successfully", OrderID: OrderID, Order_Number: Order_Number } });
//                 let MenuData = await UserController.Fetch_Complete_Menu_Data(values);
//                 let body = {
//                     OrderID: OrderID,
//                     Order_Number: Order_Number,
//                     UserID: values.UserID,
//                     BranchID: values.BranchID,
//                     MenuData: MenuData,
//                     contactName: values.contactName,
//                     contactNumber: values.contactNumber,
//                     amountToPaid: TotalAmount,
//                     dueAmount: values.dueAmount,
//                     totalAmount: values.totalAmount,
//                     eventDate: new Date(values.eventDate),
//                     eventTime: values.eventTime,
//                     eventAddress: values.eventAddress,
//                     eventPincode: values.eventPincode,
//                     eventType: values.eventType,
//                     created_at: new Date(),
//                     updated_at: new Date()
//                 };
//                 let result = await Orders(body).save();
//             } catch (error) {
//                 reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//             }
//         });
//     });
// }

UserController.List_All_Products_Search = (values, CurrentLocationData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let queryTM = {
                    CountryID: CurrentLocationData.CountryID,
                    CityID: CurrentLocationData.CityID,
                    ZoneID: CurrentLocationData.ZoneID
                }
                if (values.Search != "") {
                    let Search = {
                        $regex: String(values.Search),
                        $options: "i"
                    };
                    queryTM.$or = [
                        {
                            Unique_ProductID: Search
                        },
                        {
                            Product_Name: Search
                        },
                        {
                            Category: Search
                        },
                        {
                            SubCategory: Search
                        }
                    ]
                }
                let sortOptions1 = {
                    created_at: -1
                };
                let Count = await Title_Management_Products.countDocuments(queryTM).lean();
                let ProductsID_Array = await Title_Management_Products.find(queryTM).select('ProductID').sort(sortOptions1).lean().skip(toSkip).limit(toLimit).exec();
                let ProductsData = []
                for (const Product of ProductsID_Array) {
                    let ProductData = await UserController.Single_Product_Data(values, Product.ProductID)
                    ProductsData.push(ProductData)
                }
                let Data = {
                    Count: Count,
                    Title: values.Search,
                    Description: "Search Result of " + values.Search,
                    ProductsData: ProductsData
                }
                resolve({ success: true, extras: Data })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Products = (values, CurrentLocationData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let queryTM = {
                    CountryID: CurrentLocationData.CountryID,
                    CityID: CurrentLocationData.CityID,
                    ZoneID: CurrentLocationData.ZoneID
                }
                let sortOptions1 = {
                    created_at: -1
                };
                let Count = await Title_Management_Products.countDocuments(queryTM).lean();
                let ProductsID_Array = await Title_Management_Products.find(queryTM).select('ProductID').sort(sortOptions1).lean().skip(toSkip).limit(toLimit).exec();
                let ProductsData = []
                for (const Product of ProductsID_Array) {
                    let ProductData = await UserController.Single_Product_Data(values, Product.ProductID)
                    ProductsData.push(ProductData)
                }
                let Data = {
                    Count: Count,
                    TitleID: "",
                    Title: "Bookafella Books",
                    Description: "",
                    ProductsData: ProductsData
                }
                resolve({ success: true, extras: Data })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Products_Dynamic_Title = (values, CurrentLocationData, TitleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let queryStory = {
                    Status: true,
                    ZoneID: CurrentLocationData.ZoneID
                }
                let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
                let queryx = {
                    'Product_Data_For_Branch.BranchID': {
                        $in: StoreID_Array
                    },
                    Status: true,
                    Whether_Deleted: {
                        $ne: true
                    },
                    ZoneID: CurrentLocationData.ZoneID
                }
                let ProductID_Array = await Store_Products.distinct('ProductID', queryx).lean();

                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let queryTM = {
                    ProductID: {
                        $in: ProductID_Array
                    },
                    TitleID: TitleData.TitleID,
                    CountryID: CurrentLocationData.CountryID,
                    CityID: CurrentLocationData.CityID,
                    ZoneID: CurrentLocationData.ZoneID
                }
                let sortOptions1 = {
                    created_at: -1
                };
                let Count = await Title_Management_Products.countDocuments(queryTM).lean();
                let ProductsID_Array = await Title_Management_Products.find(queryTM).select('ProductID').sort(sortOptions1).lean().skip(toSkip).limit(toLimit).exec();
                let ProductsData = []
                for (const Product of ProductsID_Array) {
                    let ProductData = await UserController.Single_Product_Data(values, Product.ProductID)
                    ProductsData.push(ProductData)
                }
                let Data = {
                    Count: Count,
                    TitleID: TitleData.TitleID,
                    Title: TitleData.Title,
                    Description: "",//TitleData.Description,
                    ProductsData: ProductsData
                }
                resolve({ success: true, extras: Data })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Check_Store_Available = (BranchID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Today = new Date();
                Today.setMinutes(Today.getMinutes() + 330);
                let Day = Today.getDay();
                let Hours = Today.getHours();
                let Minutes = Today.getMinutes();
                function tConvert(time) {
                    // Check correct time format and split into components
                    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
                    if (time.length > 1) { // If time format correct
                        time = time.slice(1);  // Remove full string match value
                        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
                        time[0] = +time[0] % 12 || 12; // Adjust hours
                    }
                    return time.join(''); // return adjusted time or original string
                }
                let Time = tConvert(Hours + ':' + Minutes);
                let TimeX = "01/01/1900 " + Time;
                var aDate = new Date(Date.parse(TimeX));
                // var aDate = moment(aDatex, config.Take_Date_Format).add(330, 'minutes');
                // var aDate = new Date(aDatex.getTime() + 330*60000);

                let query = {};

                switch (Day) {
                    case 0:
                        query = {
                            Sunday_Available: true,
                            'Sunday_Timings.From_Time': { $lte: aDate },
                            'Sunday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 1:
                        query = {
                            Monday_Available: true,
                            'Monday_Timings.From_Time': { $lte: aDate },
                            'Monday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 2:
                        query = {
                            Tuesday_Available: true,
                            'Tuesday_Timings.From_Time': { $lte: aDate },
                            'Tuesday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 3:
                        query = {
                            Wednesday_Available: true,
                            'Wednesday_Timings.From_Time': { $lte: aDate },
                            'Wednesday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 4:
                        query = {
                            Thursday_Available: true,
                            'Thursday_Timings.From_Time': { $lte: aDate },
                            'Thursday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 5:
                        query = {
                            Friday_Available: true,
                            'Friday_Timings.From_Time': { $lte: aDate },
                            'Friday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 6:
                        query = {
                            Saturday_Available: true,
                            'Saturday_Timings.From_Time': { $lte: aDate },
                            'Saturday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    default:
                        break;
                }
                // query.Point = {
                //     '$near': {
                //         '$minDistance': 0,
                //         '$maxDistance': 20000,
                //         '$geometry': {
                //             type: "Point",
                //             coordinates: Point
                //         }
                //     }
                // }
                query.Status = true
                query.BranchID = BranchID;
                query.Whether_Branch_Online = {
                    $ne: false
                }
                // console.log(query);
                let Result = await Store_Branch.distinct('BranchID', query).lean();
                if (Result.length == 0) {
                    resolve(false)
                } else {
                    resolve(true)
                }
            } catch (error) {
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

UserController.Single_Product_Data = (values, ProductID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ProductID: ProductID
                };
                let Result = await Store_Products.findOne(query).select('-_id -__v -updated_at').lean().exec()

                if (Result != null) {
                    // console.log(Result.Unique_ProductID)
                    let uquery = {
                        BuyerID: values.BuyerID,
                        'Cart_Information.ProductID': ProductID
                    };
                    let Quantity = 0;
                    let Uresult = await Buyers.findOne(uquery).lean().exec();
                    if (Uresult != null) {
                        let PData = Uresult.Cart_Information.filter(function (ResultTemp) {
                            return ResultTemp.ProductID == Result.ProductID
                        });
                        Quantity = PData[0].Product_Quantity;
                    }
                    let PDatax
                    let pd = [];
                    async.eachSeries(Result.Product_Data_For_Branch, async (singleBranch, callbackx) => {
                        try {
                            // console.log(singleBranch.StoreID)
                            let Store_Availablity = await UserController.Check_Store_Available(singleBranch.BranchID)
                            // console.log(Store_Availablity)
                            let Bquery = {
                                BranchID: singleBranch.BranchID
                            }
                            let Bresult = await Store_Branch.findOne(Bquery).lean().exec()
                            PDatax = Result.Product_Data_For_Branch.filter(function (ResultTemp) {
                                if (ResultTemp.BranchID == singleBranch.BranchID) {
                                    let Bookstore_Type = false;
                                    if (Bresult.Bookstore_Type != undefined || Bresult.Bookstore_Type != null) {
                                        Bookstore_Type = Bresult.Bookstore_Type
                                    }
                                    ResultTemp.Featured = Bookstore_Type
                                    ResultTemp.Branch_Discount_Percent = parseFloat(ResultTemp.Branch_Discount_Percent.toFixed(2))
                                    ResultTemp.Bookafella_Discount_Percent = parseFloat(ResultTemp.Bookafella_Discount_Percent.toFixed(2))
                                    ResultTemp.Admin_Share_Percent = parseFloat(ResultTemp.Admin_Share_Percent.toFixed(2))
                                    ResultTemp.Branch_Open = Store_Availablity
                                    pd.push(ResultTemp)
                                    return ResultTemp
                                }
                            });
                            callbackx();
                        } catch (error) {
                            callbackx(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        if (Result.Format_of_the_Book == 1 || Result.Format_of_the_Book == 2 || Result.Format_of_the_Book == 0) {
                            Result.Format_of_the_Book = Result.Format_of_the_Book
                        } else {
                            Result.Format_of_the_Book = 0
                        }
                        Result.Product_Data_For_Branch = await pd.sort(function (a, b) { return a.Final_Price - b.Final_Price })
                        Result.Product_Data_For_Branch = await pd.sort(function (a, b) { return b.Avaiable_Quantity - a.Avaiable_Quantity })
                        Result.Quantity = Quantity;
                        Result.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, Result.Product_Image_Data);
                        let changes = {
                            $inc: {
                                Views: 1
                            }
                        }
                        let update = await Store_Products.updateOne(query, changes).lean().exec();
                        resolve(Result);
                    })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

UserController.List_All_Products_Dynamic = (values, CurrentLocationData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = 0;
                let toLimit = 10;
                let queryTitle = {
                    Status: true
                }
                let sortOptions = {
                    SNo: 1
                };
                let TitlesData = await Title_Management.find(queryTitle).select('-_id -__v').sort(sortOptions).lean().exec();
                let TitleProductsData = [];
                for (const TitleData of TitlesData) {
                    let queryStory = {
                        Status: true,
                        ZoneID: CurrentLocationData.ZoneID
                    }
                    let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
                    let queryx = {
                        'Product_Data_For_Branch.BranchID': {
                            $in: StoreID_Array
                        },
                        Status: true,
                        Whether_Deleted: {
                            $ne: true
                        },
                        ZoneID: CurrentLocationData.ZoneID
                    }
                    let ProductID_Array = await Store_Products.distinct('ProductID', queryx).lean();

                    let queryTM = {
                        ProductID: {
                            $in: ProductID_Array
                        },
                        TitleID: TitleData.TitleID,
                        Status: true,
                        // CountryID: CurrentLocationData.CountryID,
                        // CityID: CurrentLocationData.CityID,
                        ZoneID: CurrentLocationData.ZoneID
                    }
                    // console.log(queryTM)
                    let sortOptions1 = {
                        created_at: -1
                    };
                    let Count = await Title_Management_Products.countDocuments(queryTM).lean();
                    let ProductsID_Array = await Title_Management_Products.find(queryTM).select('ProductID').sort(sortOptions1).lean().skip(toSkip).limit(toLimit).exec();
                    let ProductsData = []
                    for (const Product of ProductsID_Array) {
                        let ProductData = await UserController.Single_Product_Data(values, Product.ProductID)
                        ProductsData.push(ProductData)
                    }
                    let Data = {
                        Count: Count,
                        TitleID: TitleData.TitleID,
                        Title: TitleData.Title,
                        Description: "",//TitleData.Description,
                        ProductsData: ProductsData
                    }
                    TitleProductsData.push(Data);
                }
                // let queryPro = {
                //     // CountryID: CurrentLocationData.CountryID,
                //     // CityID: CurrentLocationData.CityID,
                //     Status: true,
                //     ZoneID: CurrentLocationData.ZoneID
                // }
                // console.log(queryPro)
                // let sortOptions2 = {
                //     created_at: -1
                // };
                // let CountPro = await Store_Products.countDocuments(queryPro).lean();
                // let ProductsID_ArrayPro = await Store_Products.find(queryPro).select('ProductID').sort(sortOptions2).lean().skip(toSkip).limit(toLimit).exec();
                // let ProductsDataPro = []
                // for (const ProductPro of ProductsID_ArrayPro) {
                //     let ProductDataPro = await UserController.Single_Product_Data(values, ProductPro.ProductID)
                //     ProductsDataPro.push(ProductDataPro)
                // }
                // let DataPro = {
                //     Count: CountPro,
                //     TitleID: "",
                //     Title: "Bookafella Books",
                //     Description: "",
                //     ProductsData: ProductsDataPro
                // }
                // TitleProductsData.push(DataPro);
                resolve({ success: true, extras: { Data: TitleProductsData } })
                // console.log(TitleProductsData)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Update_Buyer_Location = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude);
                let Longitude = parseFloat(values.Longitude);
                let Point = [parseFloat(values.Longitude), parseFloat(values.Latitude)];
                let ZoneData = await CommonController.Validate_Service_Zone_Location(1, Latitude, Longitude);
                if (ZoneData == null) {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                } else {
                    let query = {
                        BuyerID: values.BuyerID
                    }
                    let DeleteData = await Buyers_Current_Location.deleteMany(query).lean().exec();
                    let ZoneID = ZoneData.ZoneID;
                    let CityID = ZoneData.CityID;
                    let CountryID = ZoneData.CountryID;
                    let Cfndupdquery = {
                        DeviceID: BuyerData.DeviceID
                    }
                    let Cfndupdchanges = {
                        $set: {
                            BuyerID: BuyerData.BuyerID,
                            DeviceID: BuyerData.DeviceID,
                            Buyer_Name: BuyerData.Buyer_Name,
                            Buyer_Email: BuyerData.Buyer_Email,
                            Buyer_CountryCode: BuyerData.Buyer_CountryCode,
                            Buyer_PhoneNumber: BuyerData.Buyer_PhoneNumber,
                            Latitude: Latitude,
                            Longitude: Longitude,
                            Point: Point,
                            CountryID: CountryID,
                            CityID: CityID,
                            ZoneID: ZoneID,
                            updated_at: new Date()
                        }
                    }
                    let Cfndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let CfindupdateData = await Buyers_Current_Location.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                    resolve({ success: true, extras: { Status: "Updated successfully" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Check_for_OTP_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    CountryCode: values.Buyer_CountryCode,
                    PhoneNumber: values.Buyer_PhoneNumber,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await User_OTPS.countDocuments(query).lean();
                if (Count <= config.OTP_COUNT) {
                    resolve('Validated Successfully')
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_REQUEST_EXCEED_TRY_AFTER_SOME_TIME } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Generate_Buyer_OTP_Send_Message = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OTP = await CommonController.Random_OTP_Number();
                let Data = {
                    CountryCode: values.Buyer_CountryCode,
                    PhoneNumber: values.Buyer_PhoneNumber,
                    OTP: OTP,
                    Latest: true,
                    Time: new Date()
                }
                let SaveResult = await User_OTPS(Data).save();
                resolve({ success: true, extras: { Status: "OTP Sent Successfully" } })
                let PhoneNumber = `${values.Buyer_CountryCode}${values.Buyer_PhoneNumber}`;
                let MsgData = 'Welcome To Bookafella. OTP for login is ' + OTP + ' Do not share OTP with anyone for security reasons.  \n --Thank you,  \n Bookafella Team'
                let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, MsgData);
                let query = {
                    CountryCode: values.Buyer_CountryCode,
                    PhoneNumber: values.Buyer_PhoneNumber,
                    OTP: { $ne: OTP }
                };
                let changes = {
                    Latest: false
                };
                let UpdatedStatus = await User_OTPS.updateMany(query, changes).lean();
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Check_for_User_OTP_Tries_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    CountryCode: values.Buyer_CountryCode,
                    PhoneNumber: values.Buyer_PhoneNumber,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await User_OTP_Tries.countDocuments(query).lean().exec();
                if (Count <= config.OTP_TRIES_COUNT) {
                    resolve('Validated Successfully');
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_TRIES_EXCEED_TRY_AFTER_SOME_TIME } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Validate_Buyer_OTP = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let obj1 = {
                    $ne: null
                };
                let obj2 = {
                    $eq: values.OTP
                }
                let OTP_Query = (config.SECRET_OTP_CODE === String(values.OTP)) ? obj1 : obj2;
                let query = {
                    CountryCode: values.Buyer_CountryCode,
                    PhoneNumber: values.Buyer_PhoneNumber,
                    OTP: OTP_Query,
                    Latest: true
                };
                let Result = await User_OTPS.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                    let Data = {
                        CountryCode: values.Buyer_CountryCode,
                        PhoneNumber: values.Buyer_PhoneNumber,
                        Time: new Date()
                    };
                    let SaveResult = await User_OTP_Tries(Data).save();
                } else {
                    resolve("OTP Verified Successfully");
                    let RemoveTries = await User_OTP_Tries.deleteMany({
                        CountryCode: values.Buyer_CountryCode,
                        PhoneNumber: values.Buyer_PhoneNumber,
                    }).lean();
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Find_or_Create_Buyer_Information = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Buyer_CountryCode: values.Buyer_CountryCode,
                    Buyer_PhoneNumber: values.Buyer_PhoneNumber
                };
                let Result = await Buyers.findOne(query).lean();
                let deviceID = '';
                let session = '';
                if (DeviceData != '') {
                    deviceID = DeviceData.DeviceID;
                    session = uuid.v4()
                } else {
                    if (Result == null) {
                        session = uuid.v4()
                    } else {
                        session = Result.SessionID
                        deviceID = Result.DeviceID
                    }
                }

                let deviceQuery = {
                    DeviceID: deviceID
                }
                let deviceChanges = {
                    $set: {
                        DeviceID: '',
                    }
                }
                let DeviceArray = await Buyers.updateMany(deviceQuery, deviceChanges).lean()
                if (Result == null) {
                    let Data = {
                        BuyerID: uuid.v4(),
                        SessionID: session,
                        DeviceID: deviceID,
                        Buyer_Name: values.Buyer_Name,
                        Buyer_CountryCode: values.Buyer_CountryCode,
                        Buyer_PhoneNumber: values.Buyer_PhoneNumber,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let SaveResult = await Buyers(Data).save();
                    if (DeviceData != '') {
                        resolve({ success: true, extras: { Data: JSON.parse(JSON.stringify(SaveResult)) } });
                    } else {
                        resolve(JSON.parse(JSON.stringify(SaveResult)));
                    }
                } else {
                    let changes = {
                        $set: {
                            SessionID: session,
                            DeviceID: deviceID,
                            updated_at: new Date()
                        }
                    }
                    let updateDate = await Buyers.findOneAndUpdate(query, changes).lean();
                    let Resultx = await Buyers.findOne(query).lean();
                    if (DeviceData != '') {
                        resolve({ success: true, extras: { Data: JSON.parse(JSON.stringify(Resultx)) } });
                    } else {
                        resolve(JSON.parse(JSON.stringify(Resultx)));
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Buyer_Register = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }

                let ref = false;
                if (values.Ref_PhoneNumber != '') { ref = true }
                let changes = {
                    $set: {
                        Buyer_Name: values.Buyer_Name,
                        Buyer_Email: values.Buyer_Email,
                        DOB: moment(values.DOB, config.Take_Date_Format).toDate(),
                        Ref_PhoneNumber: values.Ref_PhoneNumber,
                        Buyer_Basic_Info_Available: true,
                        Ref_PhoneNumber_Available: ref,
                        updated_at: new Date()
                    }
                }
                let UpdateData = await Buyers.updateOne(query, changes).lean()
                /// add amount to referal user
                if (values.Ref_PhoneNumber != '') {
                    let RefPriceData = await Referal_Price.findOne({}).select('-_id -__v').lean().exec();
                    let ftl = RefPriceData.First_Time_Login
                    let RQuery = {
                        Buyer_PhoneNumber: values.Ref_PhoneNumber
                    }
                    let Result = await Buyers.findOne(query).lean().exec();
                    let RData = await Buyers.findOne(RQuery).lean().exec();
                    let Ref_Length = 0
                    if (RData.My_3_Referals != undefined || RData.My_3_Referals != null) {
                        Ref_Length = RData.My_3_Referals.length
                    }
                    if (Ref_Length < 3 || RData.My_3_Referals == null || RData.My_3_Referals == undefined) {
                        let Rchanges = {
                            $push: {
                                My_3_Referals: Result
                            },
                            $inc: {
                                Total_Amount: ftl,
                                Available_Amount: ftl
                            },
                            $set: {
                                update: new Date(),
                            }
                        }
                        let RUpdateData = await Buyers.updateOne(RQuery, Rchanges).lean()
                        // add log for user wallet
                        let Data = {
                            BuyerData: RData,
                            Amount: ftl,
                            ReferalData: Result
                        }
                        let WData = {
                            LogID: uuid.v4(),
                            BuyerID: RData.BuyerID,
                            Type: 1, // amount Credited for Referal login 
                            Amount: ftl,
                            Data: Data,
                            Time: new Date()
                        }
                        let Resultlog = Buyer_Share_Logs(WData).save();
                        // deduct Amount from Company Wallet
                        let WDataw = {
                            LogID: uuid.v4(),
                            AdminID: '06f374d4-fa3f-4911-b9b6-b367fc0b3e26',
                            Type: 6, // amount deduct from Bookafella Admin wallet for Referal
                            Amount: ftl,
                            Data: Data,
                            Time: new Date()
                        }
                        let Resultlogw = Company_Wallet_Log(WDataw).save();
                        let Cfndupdquery = {

                        }
                        let Cfndupdchanges = {
                            $inc: {
                                Available_Amount: ftl * -1,
                                Withdrawn_Amount: ftl
                            }
                        }
                        let Cfndupdoptions = {
                            upsert: true,
                            setDefaultsOnInsert: true,
                            new: true
                        }
                        let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                    }
                    resolve({ success: true, extras: { Status: "Profile Updated Successfully" } })
                    //Send Notification RData(Parent), Result(child), ftl(Amount)
                    let SendNotification = await FcmController.Notification_For_Referal_First_Login(RData, Result, ftl)

                } else {
                    resolve({ success: true, extras: { Status: "Profile Updated Successfully" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Validate_Referral_Phone_Number = (values, UserData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Buyer_PhoneNumber: values.PhoneNumber
                };
                let Result = await Buyers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.REFERRAL_NOT_AVAILABLE } })
                } else {
                    if (Result.Buyer_PhoneNumber == UserData.Buyer_PhoneNumber) {
                        reject({ success: false, extras: { msg: ApiMessages.REFERRAL_PHONE_NUMBER_AND_SELF_PHONE_NUMBER_MUST_BE_DIFFERENT } })
                    } else {
                        resolve(Result.Buyer_Name);
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Add_Buyer_Address = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // if (values.Address_Type == 1 || values.Address_Type == 2 || values.Address_Type == 3) {
                if (values.Address_Default == true) {
                    let query = {
                        BuyerID: values.BuyerID,
                    }
                    let changes = {
                        Address_Default: false
                    }
                    let options = {
                        multi: true
                    }
                    let UpdateData = await Buyer_Address_Log.updateMany(query, changes, options).lean();
                }
                let Data = {
                    AddressID: uuid.v4(),
                    BuyerID: values.BuyerID,
                    Name: values.Name,
                    PhoneNumber: values.PhoneNumber,
                    Flat_No: values.Flat_No,
                    Plot_No: values.Plot_No,
                    Postal_Code: values.Postal_Code,
                    State: values.State,
                    City: values.City,
                    Land_Mark: values.Land_Mark,
                    Address_Type: values.Address_Type,
                    Address_Default: values.Address_Default,
                    Status: true,
                    Latitude: parseFloat(values.Latitude),
                    Longitude: parseFloat(values.Longitude),
                    Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
                    created_at: new Date(),
                    updated_at: new Date()
                }
                let saveData = await Buyer_Address_Log(Data).save();
                resolve({ success: true, extras: { Status: "Address Created Successfully" } })
                // } else {
                //     reject({ success: false, extras: { msg: ApiMessages.INVALID_ADDRESS_TYPE } });
                // }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Edit_Buyer_Address = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // if (values.Address_Type == 1 || values.Address_Type == 2 || values.Address_Type == 3) {
                let query = {
                    AddressID: values.AddressID
                }
                let Result = await Buyer_Address_Log.findOne(query).lean();
                if (Result != null) {
                    if (values.Address_Default == true) {
                        let query = {
                            BuyerID: values.BuyerID,
                        }
                        let changes = {
                            Address_Default: false
                        }
                        let options = {
                            multi: true
                        }
                        let UpdateData = await Buyer_Address_Log.updateMany(query, changes, options).lean();
                    }
                    let changes = {
                        $set: {
                            Name: values.Name,
                            PhoneNumber: values.PhoneNumber,
                            Flat_No: values.Flat_No,
                            Plot_No: values.Plot_No,
                            Postal_Code: values.Postal_Code,
                            State: values.State,
                            City: values.City,
                            Land_Mark: values.Land_Mark,
                            Address_Type: values.Address_Type,
                            Address_Default: values.Address_Default,
                            Latitude: parseFloat(values.Latitude),
                            Longitude: parseFloat(values.Longitude),
                            Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Buyer_Address_Log.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Address Updated Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ADDRESS } });
                }
                // } else {
                //     reject({ success: false, extras: { msg: ApiMessages.INVALID_ADDRESS_TYPE } });
                // }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Remove_Buyer_Address = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AddressID: values.AddressID
                }
                let Result = await Buyer_Address_Log.findOne(query).lean();
                if (Result != null) {
                    let changes = {
                        $set: {
                            Status: false,
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Buyer_Address_Log.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Address Remove Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ADDRESS } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_Buyer_Address = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let query = {
                    BuyerID: values.BuyerID,
                    Status: true
                }
                let sortOptions = {
                    Address_Default: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Count = await Buyer_Address_Log.countDocuments(query).lean();
                let Result = await Buyer_Address_Log.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Products_For_BuyerX = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Today = new Date();
                let Day = Today.getDay();
                let Hours = Today.getHours();
                let Minutes = Today.getMinutes();
                function tConvert(time) {
                    // Check correct time format and split into components
                    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
                    if (time.length > 1) { // If time format correct
                        time = time.slice(1);  // Remove full string match value
                        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
                        time[0] = +time[0] % 12 || 12; // Adjust hours
                    }
                    return time.join(''); // return adjusted time or original string
                }
                let Time = tConvert(Hours + ':' + Minutes);
                let TimeX = "01/01/1900 " + Time;
                var aDatex = new Date(Date.parse(TimeX));
                let Latitude = parseFloat(values.Latitude);
                let Longitude = parseFloat(values.Longitude);
                var aDate = moment(aDatex, config.Take_Date_Format).add(330, 'minutes');
                let Point = [
                    Longitude,
                    Latitude
                ];
                let query = {};

                switch (Day) {
                    case 0:
                        query = {
                            Sunday_Available: true,
                            'Sunday_Timings.From_Time': { $lte: aDate },
                            'Sunday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 1:
                        query = {
                            Monday_Available: true,
                            'Monday_Timings.From_Time': { $lte: aDate },
                            'Monday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 2:
                        query = {
                            Tuesday_Available: true,
                            'Tuesday_Timings.From_Time': { $lte: aDate },
                            'Tuesday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 3:
                        query = {
                            Wednesday_Available: true,
                            'Wednesday_Timings.From_Time': { $lte: aDate },
                            'Wednesday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 4:
                        query = {
                            Thursday_Available: true,
                            'Thursday_Timings.From_Time': { $lte: aDate },
                            'Thursday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 5:
                        query = {
                            Friday_Available: true,
                            'Friday_Timings.From_Time': { $lte: aDate },
                            'Friday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    case 6:
                        query = {
                            Saturday_Available: true,
                            'Saturday_Timings.From_Time': { $lte: aDate },
                            'Saturday_Timings.To_Time': { $gte: aDate }
                        }
                        break;
                    default:
                        break;
                }
                query.Point = {
                    '$near': {
                        '$minDistance': 0,
                        '$maxDistance': 20000,
                        '$geometry': {
                            type: "Point",
                            coordinates: Point
                        }
                    }
                }
                query.Status = true
                query.Whether_Branch_Online = {
                    $ne: false
                }
                let result = await Store_Branch.distinct('BranchID', query).lean();
                // let resp = await UserController.List_All_Products_For_BuyerX(values, result);
                resolve(result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}




UserController.List_All_Products_For_Buyer = (values, Barray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            let toSkip = Math.abs(parseInt(values.skip));
            let toLimit = Math.abs(parseInt(values.limit));
            let query = {
                $and: [{
                    'Product_Data_For_Branch.BranchID': {
                        '$in': Barray
                    },
                    'Product_Data_For_Branch.Approve': 1,
                }],
                Status: true
            };
            let sortOptions = {
                Rating: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            Store_Products.countDocuments(query).then((Count) => {
                //console.log(Count)
                if (Count >= 0) {
                    let ProductData = [];
                    Store_Products.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).then((Result) => {
                        async.eachSeries(Result, async (item, callback) => {
                            try {
                                //check for Products with filter
                                if (item.Format_of_the_Book == 1 || item.Format_of_the_Book == 2 || item.Format_of_the_Book == 0) {
                                    item.Format_of_the_Book = item.Format_of_the_Book
                                } else {
                                    item.Format_of_the_Book = 0
                                }
                                let PDatax
                                let pd = []
                                async.eachSeries(Barray, async (singleBranch, callbackx) => {
                                    try {
                                        let Bquery = {
                                            BranchID: singleBranch
                                        }
                                        let Bresult = await Store_Branch.findOne(Bquery).lean().exec()
                                        PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                                            if (ResultTemp.BranchID == singleBranch && ResultTemp.Approve == 1) {
                                                let Bookstore_Type = false;
                                                if (Bresult.Bookstore_Type != undefined || Bresult.Bookstore_Type != null) {
                                                    Bookstore_Type = Bresult.Bookstore_Type
                                                }
                                                ResultTemp.Featured = Bookstore_Type
                                                pd.push(ResultTemp)
                                                return ResultTemp
                                            }
                                        });
                                        callbackx();
                                    } catch (error) {
                                        callbackx(error);
                                    }
                                }, async (err) => {
                                    if (err) reject(err);
                                    item.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                                    item.Product_Data_For_Branch = await pd.sort(function (a, b) { return a.Final_Price - b.Final_Price })
                                    item.Product_Data_For_Branch = await pd.sort(function (a, b) { return b.Avaiable_Quantity - a.Avaiable_Quantity })
                                    ProductData.push(item)
                                    callback();
                                })
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            resolve({ success: true, extras: { Count: Count, Data: ProductData } });
                        });
                    });
                } else {
                    console.log("sdadasddsa===--->")
                }
            });
        });
    });
}

UserController.List_All_Rainning_Discount_Products_For_Buyer = (values, Barray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            let toSkip = Math.abs(parseInt(values.skip));
            let toLimit = Math.abs(parseInt(values.limit));
            let query = {
                $and: [{
                    'Product_Data_For_Branch.BranchID': {
                        '$in': Barray
                    },
                    'Product_Data_For_Branch.Approve': 1,
                }],
                Status: true
            };
            let sortOptions = {
                'Product_Data_For_Branch.Discount_Percent': -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            Store_Products.countDocuments(query).then((Count) => {
                //console.log(Count)
                if (Count >= 0) {
                    let ProductData = [];
                    Store_Products.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).then((Result) => {
                        async.eachSeries(Result, async (item, callback) => {
                            try {
                                if (item.Format_of_the_Book == 1 || item.Format_of_the_Book == 2 || item.Format_of_the_Book == 0) {
                                    item.Format_of_the_Book = item.Format_of_the_Book
                                } else {
                                    item.Format_of_the_Book = 0
                                }
                                //check for Products with filter
                                let PDatax
                                let pd = []
                                async.eachSeries(Barray, async (singleBranch, callbackx) => {
                                    try {
                                        let Bquery = {
                                            BranchID: singleBranch
                                        }
                                        let Bresult = await Store_Branch.findOne(Bquery).lean().exec()

                                        PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                                            if (ResultTemp.BranchID == singleBranch && ResultTemp.Approve == 1) {
                                                let Bookstore_Type = false;
                                                if (Bresult.Bookstore_Type != undefined || Bresult.Bookstore_Type != null) {
                                                    Bookstore_Type = Bresult.Bookstore_Type
                                                }
                                                ResultTemp.Featured = Bookstore_Type

                                                pd.push(ResultTemp)
                                                return ResultTemp
                                            }
                                        });
                                        callbackx();
                                    } catch (error) {
                                        callbackx(error);
                                    }
                                }, async (err) => {
                                    if (err) reject(err);
                                    item.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                                    item.Product_Data_For_Branch = await pd.sort(function (a, b) { return a.Final_Price - b.Final_Price })
                                    item.Product_Data_For_Branch = await pd.sort(function (a, b) { return b.Discount_Percent - a.Discount_Percent })
                                    item.Product_Data_For_Branch = await pd.sort(function (a, b) { return b.Avaiable_Quantity - a.Avaiable_Quantity })
                                    ProductData.push(item)
                                    callback();
                                })
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            ProductData = await ProductData.sort(function (a, b) { return b.Product_Data_For_Branch[0].Discount_Percent - a.Product_Data_For_Branch[0].Discount_Percent })
                            resolve({ success: true, extras: { Count: Count, Data: ProductData } });
                        });
                    });
                } else {
                    console.log("sdadasddsa===--->")
                }
            });
        });
    });
}

UserController.Get_Single_Products_For_Buyer = (values, Barray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {

                let query = {
                    $and: [{
                        'Product_Data_For_Branch.BranchID': {
                            '$in': Barray
                        },
                        ProductID: values.ProductID,
                        'Product_Data_For_Branch.Approve': 1,
                    }],
                    Status: true
                };
                let Result = await Store_Products.findOne(query).select('-_id -__v -updated_at').lean().exec()

                if (Result != null) {
                    let uquery = {
                        BuyerID: values.BuyerID,
                        'Cart_Information.ProductID': Result.ProductID
                    };
                    let Quantity = 0;
                    let Uresult = await Buyers.findOne(uquery).lean().exec();
                    if (Uresult != null) {
                        let PData = Uresult.Cart_Information.filter(function (ResultTemp) {
                            return ResultTemp.ProductID == Result.ProductID
                        });
                        Quantity = PData[0].Product_Quantity;
                    }
                    let PDatax
                    let pd = [];
                    async.eachSeries(Barray, async (singleBranch, callbackx) => {
                        try {
                            let Bquery = {
                                BranchID: singleBranch
                            }
                            let Bresult = await Store_Branch.findOne(Bquery).lean().exec()
                            PDatax = Result.Product_Data_For_Branch.filter(function (ResultTemp) {
                                if (ResultTemp.BranchID == singleBranch && ResultTemp.Approve == 1) {
                                    let Bookstore_Type = false;
                                    if (Bresult.Bookstore_Type != undefined || Bresult.Bookstore_Type != null) {
                                        Bookstore_Type = Bresult.Bookstore_Type
                                    }
                                    ResultTemp.Featured = Bookstore_Type
                                    pd.push(ResultTemp)
                                    return ResultTemp
                                }
                            });
                            callbackx();
                        } catch (error) {
                            callbackx(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        if (Result.Format_of_the_Book == 1 || Result.Format_of_the_Book == 2 || Result.Format_of_the_Book == 0) {
                            Result.Format_of_the_Book = Result.Format_of_the_Book
                        } else {
                            Result.Format_of_the_Book = 0
                        }
                        Result.Product_Data_For_Branch = await pd.sort(function (a, b) { return a.Final_Price - b.Final_Price })
                        Result.Product_Data_For_Branch = await pd.sort(function (a, b) { return b.Avaiable_Quantity - a.Avaiable_Quantity })
                        Result.Quantity = Quantity;
                        Result.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, Result.Product_Image_Data);
                        let changes = {
                            $inc: {
                                Views: 1
                            }
                        }
                        let update = await Store_Products.updateOne(query, changes).lean().exec();
                        resolve({ success: true, extras: { Data: Result } });
                    })


                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Add_Product_To_Cart = (values, ProductData, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let qty = 0;
                let Total = 0;
                if (BuyerData.Cart_Information.length > 0) {
                    let Result = BuyerData.Cart_Information.filter(function (ResultTemp) {
                        return ResultTemp.ProductID == ProductData.ProductID;
                    });
                    if (Result.length != 0) {
                        qty = parseInt(Result[0].Product_Quantity);
                        let changespull = {
                            $pull: {
                                Cart_Information: {
                                    ProductID: ProductData.ProductID
                                }
                            }
                        }
                        //let ResultPull = await Buyers.updateOne(query, changespull).lean().exec();
                    }
                }
                let cartItem = 1;
                let Qnty = qty + 1;
                let sttus = "Product Added Successfully"
                if (values.Add_Remove == -1) {
                    cartItem = -1;
                    Qnty = qty - 1;
                    sttus = "Product Removed Successfully"
                }
                let cartData;
                if (Qnty > 0) {
                    cartData = {
                        ProductID: ProductData.ProductID,
                        Product_Name: ProductData.Product_Name,
                        Product_Quantity: Qnty,
                        BranchID: values.BranchID,
                    }
                    if (qty == 0) {
                        let changespush = {
                            $inc: {
                                Cart_Total_Items: cartItem,
                            },
                            $push: {
                                Cart_Information: cartData
                            }
                        }
                        let ResultPush = await Buyers.updateOne(query, changespush).lean().exec();
                    } else {
                        let queryx = {
                            BuyerID: values.BuyerID,
                            'Cart_Information.ProductID': ProductData.ProductID

                        }
                        let changespush = {
                            $inc: {
                                Cart_Total_Items: cartItem,
                                'Cart_Information.$.Product_Quantity': values.Add_Remove
                            },
                        }
                        let ResultPushx = await Buyers.updateOne(queryx, changespush).lean().exec();

                    }
                } else if (Qnty == 0) {
                    let changespush = {
                        $inc: {
                            Cart_Total_Items: cartItem,
                        },
                        $pull: {
                            Cart_Information: {
                                ProductID: ProductData.ProductID
                            }
                        }
                    }
                    let ResultPush = await Buyers.updateOne(query, changespush).lean().exec();
                } else {
                    cartData = {}
                    if (qty > 0) {
                        let changespush = {
                            $inc: {
                                Cart_Total_Items: cartItem,
                            }
                        }
                        let ResultPush = await Buyers.updateOne(query, changespush).lean().exec();
                    }
                }
                let ResultFinal = await Buyers.findOne(query).select('-_id Cart_Total_Items Cart_Information').lean().exec()
                let Resultx = ResultFinal.Cart_Information.filter(function (ResultTemp) {
                    return ResultTemp.ProductID == ProductData.ProductID;
                });
                ResultFinal.Cart_Information = Resultx[0]
                resolve({ success: true, extras: { Status: sttus, Data: ResultFinal } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Remove_Product_From_Cart = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let Result = await Buyers.findOne(query).lean().exec();
                if (Result.Cart_Information.length != 0) {
                    let Cart_Info = Result.Cart_Information.filter(function (ResultTemp) {
                        return ResultTemp.ProductID == values.ProductID;
                    });
                    let Qty = Cart_Info[0].Product_Quantity
                    let changespull = {
                        $inc: {
                            Cart_Total_Items: Qty * -1,
                        },
                        $pull: {
                            Cart_Information: {
                                ProductID: values.ProductID
                            }
                        }
                    }
                    let ResultPull = await Buyers.updateOne(query, changespull).lean().exec();
                    resolve({ success: true, extras: { Status: "Item in Cart Removed Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.CART_EMPTY } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Remove_All_Product_From_Cart = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let changespull = {
                    $set: {
                        Cart_Total_Items: 0,
                        Cart_Total_Price: 0,
                    },
                    $pull: {
                        Cart_Information: {
                            ProductID: {
                                $ne: null
                            }
                        }
                    }
                }
                let ResultPull = await Buyers.updateOne(query, changespull).lean().exec();
                resolve({ success: true, extras: { Status: "All Items in Cart Removed Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


UserController.List_All_Products_In_Buyer_Order = (values, buyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = [];
                let Book_Variants = 0;
                let Cart_Total_Price = 0;
                let Cart_Total_Discount = 0;
                let Cart_Total_Items = 0;
                let Cart_Total_Final_Price = 0;
                let Cart_Total_Admin_Share_Amount = 0;
                let Cart_Total_Branch_Share_Amount = 0;
                // console.log(buyerData.Cart_Information)
                async.eachSeries(buyerData.Cart_Information, async (item, callback) => {
                    // console.log(item)
                    try {
                        Book_Variants++;
                        let query = {
                            ProductID: item.ProductID,
                            'Product_Data_For_Branch.BranchID': values.BranchID
                        }
                        let PInfo = await Store_Products.findOne(query).lean().exec();
                        if (PInfo != null) {
                            let PDatax = PInfo.Product_Data_For_Branch.filter(function (ResultTemp) {
                                if (ResultTemp.BranchID == values.BranchID) {
                                    return ResultTemp
                                }
                            })
                            PInfo.Product_Data_For_Branch = PDatax[0]
                            let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, PInfo.Product_Image_Data);

                            let Price = PInfo.Product_Data_For_Branch.Price;

                            let Branch_Discount_Percent = PInfo.Product_Data_For_Branch.Branch_Discount_Percent;
                            let Branch_Discount_Price = PInfo.Product_Data_For_Branch.Branch_Discount_Price;
                            let Bookafella_Discount_Percent = PInfo.Product_Data_For_Branch.Bookafella_Discount_Percent;
                            let Bookafella_Discount_Price = PInfo.Product_Data_For_Branch.Bookafella_Discount_Price;
                            let Discount_Price = PInfo.Product_Data_For_Branch.Total_Discount_Price;

                            let Final_Price = PInfo.Product_Data_For_Branch.Final_Price;

                            let Admin_Share_Percent = PInfo.Product_Data_For_Branch.Admin_Share_Percent;
                            let Admin_Share_Amount = PInfo.Product_Data_For_Branch.Admin_Share_Amount;

                            let Branch_Share_Amount = PInfo.Product_Data_For_Branch.Branch_Share_Amount;

                            let Total_Price = parseFloat(Price * item.Product_Quantity);
                            let Total_Discount_Price = parseFloat(Discount_Price * item.Product_Quantity);
                            let Total_Final_Price = parseFloat(Final_Price * item.Product_Quantity);
                            let Total_Admin_Share_Amount = parseFloat(Admin_Share_Amount * item.Product_Quantity);
                            let Total_Branch_Share_Amount = parseFloat(Branch_Share_Amount * item.Product_Quantity);
                            Cart_Total_Price += Total_Price;
                            Cart_Total_Discount += Total_Discount_Price;
                            Cart_Total_Items += item.Product_Quantity;
                            Cart_Total_Final_Price += Total_Final_Price;
                            Cart_Total_Admin_Share_Amount += Total_Admin_Share_Amount;
                            Cart_Total_Branch_Share_Amount += Total_Branch_Share_Amount;
                            let Cart_Information = {
                                ProductID: PInfo.ProductID,
                                Unique_ProductID: PInfo.Unique_ProductID,
                                Product_Name: PInfo.Product_Name,
                                Product_Image_Data: Product_Image_Data,
                                Product_Edition: PInfo.Product_Edition,
                                Product_Published_On: PInfo.Product_Published_On,
                                Product_Publisher: PInfo.Product_Publisher,
                                Total_Pages: PInfo.Total_Pages,
                                Avg_Rating: PInfo.Avg_Rating,
                                Total_Rating_Count: PInfo.Total_Rating_Count,
                                Avaiable_Quantity: PInfo.Product_Data_For_Branch.Avaiable_Quantity,
                                Price: Price,
                                Discount_Percent: Branch_Discount_Percent + Bookafella_Discount_Percent,
                                Discount_Price: Discount_Price,
                                Final_Price: Final_Price,
                                BranchID: PInfo.Product_Data_For_Branch.BranchID,
                                Branch_Name: PInfo.Product_Data_For_Branch.Branch_Name,
                                Product_Quantity: item.Product_Quantity,
                                Total_Price: Total_Price,
                                Total_Discount_Price: Total_Discount_Price,
                                Total_Final_Price: Total_Final_Price,
                                Admin_Share_Percent: Admin_Share_Percent,
                                Admin_Share_Amount: Admin_Share_Amount,
                                Branch_Share_Amount: Branch_Share_Amount,
                                Total_Admin_Share_Amount: Total_Admin_Share_Amount,
                                Total_Branch_Share_Amount: Total_Branch_Share_Amount,
                                CategoryID: PInfo.CategoryID,
                                Category: PInfo.Category,
                                SubCategoryID: PInfo.SubCategoryID,
                                SubCategory: PInfo.SubCategory
                            }
                            Data.push(Cart_Information);
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    let Buyer_Cart_Data = {
                        Cart_Total_Items: parseInt(Cart_Total_Items),
                        Cart_Total_Price: parseFloat(Cart_Total_Price),
                        Discount: parseFloat((parseFloat(Cart_Total_Discount)).toFixed(2)),
                        Book_Variants: Book_Variants,
                        Cart_Total_Final_Price: parseFloat(parseFloat(Cart_Total_Final_Price).toFixed(2)),
                        Cart_Total_Admin_Share_Amount: parseFloat(parseFloat(Cart_Total_Admin_Share_Amount).toFixed(2)),
                        Cart_Total_Branch_Share_Amount: parseFloat(parseFloat(Cart_Total_Branch_Share_Amount).toFixed(2)),
                        Cart_Information: Data
                    }
                    resolve({ success: true, extras: { Data: Buyer_Cart_Data } })
                });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Products_In_Buyer_Cart = (values, buyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = [];
                let Book_Variants = 0;
                let Cart_Total_Price = 0;
                let Cart_Total_Discount = 0;
                let Cart_Total_Items = 0;
                let Cart_Total_Final_Price = 0;
                let Cart_Total_Admin_Share_Amount = 0;
                let Cart_Total_Branch_Share_Amount = 0;
                // console.log(buyerData.Cart_Information)
                async.eachSeries(buyerData.Cart_Information, async (item, callback) => {
                    // console.log(item)
                    try {
                        Book_Variants++;
                        let query = {
                            ProductID: item.ProductID,
                            'Product_Data_For_Branch.BranchID': item.BranchID
                        }
                        let PInfo = await Store_Products.findOne(query).lean().exec();
                        if (PInfo != null) {
                            let PDatax = PInfo.Product_Data_For_Branch.filter(function (ResultTemp) {
                                if (ResultTemp.BranchID == item.BranchID) {
                                    return ResultTemp
                                }
                            })
                            PInfo.Product_Data_For_Branch = PDatax[0]
                            let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, PInfo.Product_Image_Data);

                            let Price = PInfo.Product_Data_For_Branch.Price;

                            let Branch_Discount_Percent = PInfo.Product_Data_For_Branch.Branch_Discount_Percent;
                            let Branch_Discount_Price = PInfo.Product_Data_For_Branch.Branch_Discount_Price;
                            let Bookafella_Discount_Percent = PInfo.Product_Data_For_Branch.Bookafella_Discount_Percent;
                            let Bookafella_Discount_Price = PInfo.Product_Data_For_Branch.Bookafella_Discount_Price;
                            let Discount_Price = PInfo.Product_Data_For_Branch.Total_Discount_Price;

                            let Final_Price = PInfo.Product_Data_For_Branch.Final_Price;

                            let Admin_Share_Percent = PInfo.Product_Data_For_Branch.Admin_Share_Percent;
                            let Admin_Share_Amount = PInfo.Product_Data_For_Branch.Admin_Share_Amount;

                            let Branch_Share_Amount = PInfo.Product_Data_For_Branch.Branch_Share_Amount;

                            let Total_Price = parseFloat(Price * item.Product_Quantity);
                            let Total_Discount_Price = parseFloat(Discount_Price * item.Product_Quantity);
                            let Total_Final_Price = parseFloat(Final_Price * item.Product_Quantity);
                            let Total_Admin_Share_Amount = parseFloat(Admin_Share_Amount * item.Product_Quantity);
                            let Total_Branch_Share_Amount = parseFloat(Branch_Share_Amount * item.Product_Quantity);
                            Cart_Total_Price += Total_Price;
                            Cart_Total_Discount += Total_Discount_Price;
                            Cart_Total_Items += item.Product_Quantity;
                            Cart_Total_Final_Price += Total_Final_Price;
                            Cart_Total_Admin_Share_Amount += Total_Admin_Share_Amount;
                            Cart_Total_Branch_Share_Amount += Total_Branch_Share_Amount;
                            let Cart_Information = {
                                ProductID: PInfo.ProductID,
                                Unique_ProductID: PInfo.Unique_ProductID,
                                Product_Name: PInfo.Product_Name,
                                CityID: PInfo.CityID,
                                CountryID: PInfo.CountryID,
                                ZoneID: PInfo.ZoneID,
                                Format_of_the_Book: PInfo.Format_of_the_Book,
                                Authors: PInfo.Authors,
                                Product_Image_Data: Product_Image_Data,
                                Product_Edition: PInfo.Product_Edition,
                                Product_Published_On: PInfo.Product_Published_On,
                                Product_Publisher: PInfo.Product_Publisher,
                                Total_Pages: PInfo.Total_Pages,
                                Avg_Rating: PInfo.Avg_Rating,
                                Total_Rating_Count: PInfo.Total_Rating_Count,
                                Avaiable_Quantity: PInfo.Product_Data_For_Branch.Avaiable_Quantity,
                                Price: Price,
                                Discount_Percent: Branch_Discount_Percent + Bookafella_Discount_Percent,
                                Discount_Price: Discount_Price,
                                Final_Price: Final_Price,
                                BranchID: PInfo.Product_Data_For_Branch.BranchID,
                                StoreID: PInfo.Product_Data_For_Branch.StoreID,
                                Branch_Name: PInfo.Product_Data_For_Branch.Branch_Name,
                                Product_Quantity: item.Product_Quantity,
                                Total_Price: Total_Price,
                                Total_Discount_Price: Total_Discount_Price,
                                Total_Final_Price: Total_Final_Price,
                                Admin_Share_Percent: Admin_Share_Percent,
                                Admin_Share_Amount: Admin_Share_Amount,
                                Branch_Share_Amount: Branch_Share_Amount,
                                Total_Admin_Share_Amount: Total_Admin_Share_Amount,
                                Total_Branch_Share_Amount: Total_Branch_Share_Amount,
                                CategoryID: PInfo.CategoryID,
                                Category: PInfo.Category,
                                SubCategoryID: PInfo.SubCategoryID,
                                SubCategory: PInfo.SubCategory
                            }
                            Data.push(Cart_Information);
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    let Buyer_Cart_Data = {
                        Cart_Total_Items: parseInt(Cart_Total_Items),
                        Cart_Total_Price: parseFloat(Cart_Total_Price),
                        Discount: parseFloat((parseFloat(Cart_Total_Discount)).toFixed(2)),
                        Book_Variants: Book_Variants,
                        Cart_Total_Final_Price: parseFloat(parseFloat(Cart_Total_Final_Price).toFixed(2)),
                        Cart_Total_Admin_Share_Amount: parseFloat(parseFloat(Cart_Total_Admin_Share_Amount).toFixed(2)),
                        Cart_Total_Branch_Share_Amount: parseFloat(parseFloat(Cart_Total_Branch_Share_Amount).toFixed(2)),
                        Cart_Information: Data
                    }
                    resolve({ success: true, extras: { Data: Buyer_Cart_Data } })
                });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Products_In_Order = (values, buyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OrderQuery = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(OrderQuery).lean().exec()
                if (Result != null) {
                    let Data = [];
                    let Book_Variants = 0;
                    let Cart_Total_Price = 0;
                    let Cart_Total_Items = 0;
                    let Cart_Total_Discount = 0;
                    let Cart_Total_Final_Price = 0;
                    let Cart_Total_Admin_Share_Amount = 0;
                    let Cart_Total_Buyer_Share_Amount = 0;
                    async.eachSeries(Result.Cart_Information, async (item, callback) => {
                        //console.log(item)
                        try {
                            Book_Variants++;
                            let query = {
                                ProductID: item.ProductID,
                                'Product_Data_For_Branch.BranchID': Result.Branch_Information.BranchID
                            }
                            let PInfo = await Store_Products.findOne(query).lean().exec();
                            let PDatax = PInfo.Product_Data_For_Branch.filter(function (ResultTemp) {
                                // if (ResultTemp.BranchID == item.BranchID && ResultTemp.Approve == 1) {
                                //     return ResultTemp
                                // }
                                if (ResultTemp.Approve == 1) {
                                    return ResultTemp
                                }
                            })
                            PInfo.Product_Data_For_Branch = PDatax[0]
                            let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, PInfo.Product_Image_Data);
                            let Price = PInfo.Product_Data_For_Branch.Price;
                            let Discount_Percent = PInfo.Product_Data_For_Branch.Discount_Percent;
                            let Discount_Price = PInfo.Product_Data_For_Branch.Discount_Price;
                            let Final_Price = PInfo.Product_Data_For_Branch.Final_Price;
                            let Admin_Share_Percent = PInfo.Product_Data_For_Branch.Admin_Share_Percent;
                            let Admin_Share_Amount = PInfo.Product_Data_For_Branch.Admin_Share_Amount;
                            let Buyer_Share_Amount = PInfo.Product_Data_For_Branch.Buyer_Share_Amount;
                            let Total_Price = parseFloat(PInfo.Product_Data_For_Branch.Price * item.Product_Quantity);
                            let Total_Discount_Price = parseFloat(PInfo.Product_Data_For_Branch.Discount_Price * item.Product_Quantity);
                            let Total_Final_Price = parseFloat(PInfo.Product_Data_For_Branch.Final_Price * item.Product_Quantity);
                            let Total_Admin_Share_Amount = parseFloat(PInfo.Product_Data_For_Branch.Admin_Share_Amount * item.Product_Quantity);
                            let Total_Buyer_Share_Amount = parseFloat(PInfo.Product_Data_For_Branch.Buyer_Share_Amount * item.Product_Quantity);
                            Cart_Total_Price += Total_Price;
                            Cart_Total_Discount += Total_Discount_Price;
                            Cart_Total_Final_Price += Total_Final_Price;
                            Cart_Total_Admin_Share_Amount += Total_Admin_Share_Amount;
                            Cart_Total_Buyer_Share_Amount += Total_Buyer_Share_Amount;
                            Cart_Total_Items += item.Product_Quantity;
                            let Cart_Information = {
                                ProductID: PInfo.ProductID,
                                Unique_ProductID: PInfo.Unique_ProductID,
                                Product_Name: PInfo.Product_Name,
                                Product_Image_Data: Product_Image_Data,
                                Product_Edition: PInfo.Product_Edition,
                                Product_Published_On: PInfo.Product_Published_On,
                                Product_Publisher: PInfo.Product_Publisher,
                                Total_Pages: PInfo.Total_Pages,
                                Avg_Rating: PInfo.Avg_Rating,
                                Total_Rating_Count: PInfo.Total_Rating_Count,
                                Avaiable_Quantity: PInfo.Product_Data_For_Branch.Avaiable_Quantity,
                                Price: Price,
                                Discount_Percent: Discount_Percent,
                                Discount_Price: Discount_Price,
                                Final_Price: Final_Price,
                                BranchID: PInfo.Product_Data_For_Branch.BranchID,
                                Branch_Name: PInfo.Product_Data_For_Branch.Branch_Name,
                                Product_Quantity: item.Product_Quantity,
                                Total_Price: Total_Price,
                                Total_Discount_Price: Total_Discount_Price * -1,
                                Total_Final_Price: Total_Final_Price,
                                Admin_Share_Percent: Admin_Share_Percent,
                                Admin_Share_Amount: Admin_Share_Amount,
                                Buyer_Share_Amount: Buyer_Share_Amount,
                                Total_Admin_Share_Amount: Total_Admin_Share_Amount,
                                Total_Buyer_Share_Amount: Total_Buyer_Share_Amount,
                                CategoryID: PInfo.CategoryID,
                                Category: PInfo.Category,
                                SubCategoryID: PInfo.SubCategoryID,
                                SubCategory: PInfo.SubCategory
                            }
                            Data.push(Cart_Information);
                            callback();
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        let Buyer_Cart_Data = {
                            Cart_Total_Items: parseInt(Cart_Total_Items),
                            Cart_Total_Price: parseFloat(Cart_Total_Price),
                            Discount: parseFloat((parseFloat(Cart_Total_Discount) * -1).toFixed(2)),
                            Book_Variants: Book_Variants,
                            Cart_Total_Final_Price: parseFloat(parseFloat(Cart_Total_Final_Price).toFixed(2)),
                            Cart_Total_Admin_Share_Amount: parseFloat(parseFloat(Cart_Total_Admin_Share_Amount).toFixed(2)),
                            Cart_Total_Buyer_Share_Amount: parseFloat(parseFloat(Cart_Total_Buyer_Share_Amount).toFixed(2)),
                            Cart_Information: Data
                        }
                        resolve({ success: true, extras: { Data: Buyer_Cart_Data } })
                    });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } })
                }

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Get_Product_Data_For_Branch = (values, buyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = [];
                let Book_Variants = 0;
                let Cart_Total_Price = 0;
                let Cart_Total_Items = 0;
                let Cart_Total_Discount = 0;
                let Cart_Total_Final_Price = 0;
                let Cart_Total_Admin_Share_Amount = 0;
                let Cart_Total_Buyer_Share_Amount = 0;
                Book_Variants++;
                let query = {
                    ProductID: values.ProductID,
                    'Product_Data_For_Branch.BranchID': values.BranchID
                }
                let PInfo = await Store_Products.findOne(query).lean().exec();
                let PDatax = PInfo.Product_Data_For_Branch.filter(function (ResultTemp) {
                    if (ResultTemp.BranchID == values.BranchID) {
                        return ResultTemp
                    }
                })
                PInfo.Product_Data_For_Branch = PDatax[0]
                let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, PInfo.Product_Image_Data);
                let Price = PInfo.Product_Data_For_Branch.Price;
                let Discount_Percent = PInfo.Product_Data_For_Branch.Discount_Percent;
                let Discount_Price = PInfo.Product_Data_For_Branch.Discount_Price;
                let Final_Price = PInfo.Product_Data_For_Branch.Final_Price;
                let Admin_Share_Percent = PInfo.Product_Data_For_Branch.Admin_Share_Percent;
                let Admin_Share_Amount = PInfo.Product_Data_For_Branch.Admin_Share_Amount;
                let Buyer_Share_Amount = PInfo.Product_Data_For_Branch.Buyer_Share_Amount;
                let Total_Price = parseFloat(PInfo.Product_Data_For_Branch.Price * values.Product_Quantity);
                let Total_Discount_Price = parseFloat(PInfo.Product_Data_For_Branch.Discount_Price * values.Product_Quantity);
                let Total_Final_Price = parseFloat(PInfo.Product_Data_For_Branch.Final_Price * values.Product_Quantity);
                let Total_Admin_Share_Amount = parseFloat(PInfo.Product_Data_For_Branch.Admin_Share_Amount * values.Product_Quantity);
                let Total_Buyer_Share_Amount = parseFloat(PInfo.Product_Data_For_Branch.Buyer_Share_Amount * values.Product_Quantity);
                Cart_Total_Price += Total_Price;
                Cart_Total_Discount += Total_Discount_Price;
                Cart_Total_Final_Price += Total_Final_Price;
                Cart_Total_Admin_Share_Amount += Total_Admin_Share_Amount;
                Cart_Total_Buyer_Share_Amount += Total_Buyer_Share_Amount;
                Cart_Total_Items += values.Product_Quantity;
                let Cart_Information = {
                    ProductID: PInfo.ProductID,
                    Unique_ProductID: PInfo.Unique_ProductID,
                    Product_Name: PInfo.Product_Name,
                    Product_Image_Data: Product_Image_Data,
                    Product_Edition: PInfo.Product_Edition,
                    Product_Published_On: PInfo.Product_Published_On,
                    Product_Publisher: PInfo.Product_Publisher,
                    Total_Pages: PInfo.Total_Pages,
                    Avg_Rating: PInfo.Avg_Rating,
                    Total_Rating_Count: PInfo.Total_Rating_Count,
                    Avaiable_Quantity: PInfo.Product_Data_For_Branch.Avaiable_Quantity,
                    Price: Price,
                    Discount_Percent: Discount_Percent,
                    Discount_Price: Discount_Price,
                    Final_Price: Final_Price,
                    BranchID: PInfo.Product_Data_For_Branch.BranchID,
                    Branch_Name: PInfo.Product_Data_For_Branch.Branch_Name,
                    Product_Quantity: parseInt(values.Product_Quantity),
                    Total_Price: Total_Price,
                    Total_Discount_Price: Total_Discount_Price * -1,
                    Total_Final_Price: Total_Final_Price,
                    Admin_Share_Percent: Admin_Share_Percent,
                    Admin_Share_Amount: Admin_Share_Amount,
                    Buyer_Share_Amount: Buyer_Share_Amount,
                    Total_Admin_Share_Amount: Total_Admin_Share_Amount,
                    Total_Buyer_Share_Amount: Total_Buyer_Share_Amount,
                    CategoryID: PInfo.CategoryID,
                    Category: PInfo.Category,
                    SubCategoryID: PInfo.SubCategoryID,
                    SubCategory: PInfo.SubCategory
                }
                Data.push(Cart_Information);
                let Buyer_Cart_Data = {
                    Cart_Total_Items: parseInt(Cart_Total_Items),
                    Cart_Total_Price: parseFloat(Cart_Total_Price),
                    Discount: parseFloat((parseFloat(Cart_Total_Discount) * -1).toFixed(2)),
                    Book_Variants: Book_Variants,
                    Cart_Total_Final_Price: parseFloat(parseFloat(Cart_Total_Final_Price).toFixed(2)),
                    Cart_Total_Admin_Share_Amount: parseFloat(parseFloat(Cart_Total_Admin_Share_Amount).toFixed(2)),
                    Cart_Total_Buyer_Share_Amount: parseFloat(parseFloat(Cart_Total_Buyer_Share_Amount).toFixed(2)),
                    Cart_Information: Data
                }
                resolve({ success: true, extras: { Data: Buyer_Cart_Data } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//////////////////////////


UserController.Buyer_Pre_Order_Array_Summary = (values, CartData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                const array = CartData.Cart_Information;
                const result = []; //for unique BranchID
                const map = new Map();
                for (const item of array) {
                    if (!map.has(item.BranchID)) {
                        map.set(item.BranchID, true);
                        result.push({
                            BranchID: item.BranchID,
                        });
                    }
                }
                let OrderData = {}
                let OrderData1 = []
                let Other_Discount = 0;
                let Delivery_Charge = 0;
                let Dquery = {
                    BuyerID: values.BuyerID,
                    Whether_Offer_Applied: true,
                    Whether_Order_Placed: false
                }
                let DiscountData = await User_Discount_Apply.findOne(Dquery).lean().exec()
                let Minimum_Invoice_Amount = 0;
                let Discount_Percentage = 0;
                let Max_Discount_Amount = 0;
                if (DiscountData != null) {
                    let Disquery = {
                        DiscountID: DiscountData.DiscountID,
                        Status: true
                    }
                    let FinalDiscountData = await Discounts.findOne(Disquery).lean().exec();
                    if (FinalDiscountData != null) {
                        Minimum_Invoice_Amount = FinalDiscountData.Minimum_Invoice_Amount;
                        Discount_Percentage = FinalDiscountData.Discount_Percentage;
                        Max_Discount_Amount = FinalDiscountData.Max_Discount_Amount;
                    }
                }


                async.timesSeries(result.length, async (index, callback) => {
                    try {
                        let AQuery = {
                            AddressID: values.AddressID
                        };
                        let AddressData = await Buyer_Address_Log.findOne(AQuery).lean().exec();

                        let OrderNO = index + 1
                        let Sub_Order = []
                        let XOrder = {}
                        let PDatax = CartData.Cart_Information.filter(function (ResultTemp) {
                            return ResultTemp.BranchID == result[index].BranchID
                        })
                        let Order_Total_Price = 0;
                        let Order_Book_Discount = 0;
                        let Order_Other_Discount = 0;
                        let Order_Delivery_Charge = 0;
                        let Order_Package_Charge = 0;
                        let Order_Surge_Charge = 0;
                        let Order_Taxes = 0;
                        let Previous_Order_Cancellation_Charge = 0;
                        let Order_Total_Admin_Share_Amount = 0;
                        let Order_Total_Branch_Share_Amount = 0;
                        let TaxData = await Taxes.findOne().lean().exec();
                        let Bquery = {
                            BranchID: result[index].BranchID
                        }
                        let BranchData = await Store_Branch.findOne(Bquery).lean().exec();
                        let xlat = BranchData.Latitude;
                        let xlng = BranchData.Longitude;
                        let ylat = AddressData.Latitude;
                        let ylng = AddressData.Longitude;
                        let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(xlat, xlng, ylat, ylng);
                        let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                        Distance /= 1000;//mtrs to Kms
                        parseInt(Distance.toFixed(0))

                        let Fee_Data
                        let ZoneData = await CommonController.Validate_Service_Zone_Location(1, ylat, ylng);

                        if (ZoneData == null) {
                            reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                        } else {
                            Fee_Data = await Zone_Fees.findOne({ ZoneID: ZoneData.ZoneID }).lean().exec();
                            if (Fee_Data != null) {
                                Order_Delivery_Charge = parseInt(Fee_Data.Delivery_Fee)
                                Order_Package_Charge = parseInt(Fee_Data.Packaging_Fee)
                                Order_Surge_Charge = parseInt(Fee_Data.Surge_Fee)
                            }
                        }
                        let POCC = await Buyer_Orders.findOne({ BuyerID: values.BuyerID, Whether_Cancellation_Charges_For_Next_Order: true }).select('Cancellation_Charges').lean().exec()
                        if (POCC != null) {
                            Previous_Order_Cancellation_Charge = POCC.Cancellation_Charges
                        }
                        async.eachSeries(PDatax, (item, callback1) => {
                            Order_Total_Price += item.Total_Price
                            Order_Book_Discount += item.Total_Discount_Price
                            Order_Total_Admin_Share_Amount += item.Total_Admin_Share_Amount
                            Order_Total_Branch_Share_Amount += item.Total_Branch_Share_Amount
                            Sub_Order.push(item)
                            callback1();
                        }, (err) => {
                            if (Minimum_Invoice_Amount <= parseFloat(Order_Total_Price + Order_Book_Discount)) {
                                let Max_Amount_Per_Order = parseFloat(Max_Discount_Amount / result.length);
                                if (Max_Amount_Per_Order >= parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)) {
                                    Order_Other_Discount = parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)
                                } else {
                                    Order_Other_Discount = Max_Amount_Per_Order
                                }
                            }
                            XOrder.Cart_Information = Sub_Order
                            Other_Discount += Order_Other_Discount;
                            Delivery_Charge += Order_Delivery_Charge;
                            let Bookstore_Taxes = 0;
                            let Delivery_Taxes = 0;
                            if (TaxData != null) {
                                let Taxes_Percent_Book = TaxData.Bookstore_GST
                                let Taxes_Percent_Delivery = TaxData.Delivery_GST
                                Bookstore_Taxes = parseFloat((Order_Total_Price - Order_Other_Discount - parseFloat(Order_Book_Discount.toFixed(2))) * Taxes_Percent_Book / 100)
                                Delivery_Taxes = parseFloat(Order_Delivery_Charge) * Taxes_Percent_Delivery / 100
                                Order_Taxes = Bookstore_Taxes + Delivery_Taxes;
                            }
                            XOrder.Order_Invoice = {
                                Order_Total_Price: Order_Total_Price,
                                Order_Book_Discount: parseFloat(Order_Book_Discount.toFixed(2)),
                                Order_Other_Discount: Order_Other_Discount,
                                Order_Delivery_Charge: Order_Delivery_Charge,
                                Order_Bookstore_Taxes: Bookstore_Taxes,
                                Order_Delivery_Taxes: Delivery_Taxes,
                                Order_Taxes: Order_Taxes,
                                Order_Package_Charge: Order_Package_Charge,
                                Order_Surge_Charge: Order_Surge_Charge,
                                Previous_Order_Cancellation_Charge: Previous_Order_Cancellation_Charge,
                                Order_Total_Final_Price: parseFloat(Order_Total_Price - Order_Book_Discount - Order_Other_Discount + Order_Taxes + Order_Delivery_Charge + Order_Package_Charge + Order_Surge_Charge + Previous_Order_Cancellation_Charge),
                                Order_Total_Admin_Share_Amount: Order_Total_Admin_Share_Amount,
                                Order_Total_Branch_Share_Amount: Order_Total_Branch_Share_Amount,
                            }
                            OrderData1.push(XOrder)
                            //OrderData.Order = XOrder
                        });
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    // OrderData.OrderData = OrderData1
                    // OrderData.Cart_Total_Items = CartData.Cart_Total_Items;
                    // OrderData.Cart_Total_Price = parseFloat(CartData.Cart_Total_Price);
                    // OrderData.Discount = parseFloat(CartData.Discount + Other_Discount);
                    // OrderData.Delivery_Charge = parseFloat(Delivery_Charge);
                    // OrderData.Book_Variants = CartData.Book_Variants;
                    // OrderData.Cart_Total_Final_Price = parseFloat(CartData.Cart_Total_Price + CartData.Discount - Other_Discount + Delivery_Charge);
                    resolve({ success: true, extras: { Data: OrderData1 } })
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Buyer_Order_Array_Summary = (values, CartData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OrderData1 = []
                let Dquery = {
                    BuyerID: values.BuyerID,
                    Whether_Offer_Applied: true,
                    Whether_Order_Placed: false
                }
                let DiscountData = await User_Discount_Apply.findOne(Dquery).lean().exec()
                let Minimum_Invoice_Amount = 0;
                let Discount_Percentage = 0;
                let Max_Discount_Amount = 0;
                if (DiscountData != null) {
                    let Disquery = {
                        DiscountID: DiscountData.DiscountID,
                        Status: true
                    }
                    let FinalDiscountData = await Discounts.findOne(Disquery).lean().exec();
                    if (FinalDiscountData != null) {
                        Minimum_Invoice_Amount = FinalDiscountData.Minimum_Invoice_Amount;
                        Discount_Percentage = FinalDiscountData.Discount_Percentage;
                        Max_Discount_Amount = FinalDiscountData.Max_Discount_Amount;
                    }
                }
                let AQuery = {
                    AddressID: values.AddressID
                };
                let AddressData = await Buyer_Address_Log.findOne(AQuery).lean().exec();

                let Sub_Order = []
                let XOrder = {}
                let PDatax = CartData.Cart_Information.filter(function (ResultTemp) {
                    return ResultTemp.BranchID == values.BranchID
                })
                let Order_Total_Price = 0;
                let Order_Book_Discount = 0;
                let Order_Other_Discount = 0;
                let Order_Delivery_Charge = 0;
                let Order_Package_Charge = 0;
                let Order_Surge_Charge = 0;
                let Previous_Order_Cancellation_Charge = 0;
                let Order_Total_Admin_Share_Amount = 0;
                let Order_Total_Branch_Share_Amount = 0;
                let Bquery = {
                    BranchID: values.BranchID
                }
                let BranchData = await Store_Branch.findOne(Bquery).lean().exec();
                let Order_Taxes = 0;
                let TaxData = await Taxes.findOne().lean().exec();
                let xlat = BranchData.Latitude;
                let xlng = BranchData.Longitude;
                let ylat = AddressData.Latitude;
                let ylng = AddressData.Longitude;
                let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(xlat, xlng, ylat, ylng);
                let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                Distance /= 1000;//mtrs to Kms
                parseInt(Distance.toFixed(0))

                let Fee_Data
                let ZoneData = await CommonController.Validate_Service_Zone_Location(1, ylat, ylng);
                if (ZoneData == null) {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                } else {
                    Fee_Data = await Zone_Fees.findOne({ ZoneID: ZoneData.ZoneID }).lean().exec();
                    if (Fee_Data != null) {
                        Order_Delivery_Charge = parseInt(Fee_Data.Delivery_Fee)
                        Order_Package_Charge = parseInt(Fee_Data.Packaging_Fee)
                        Order_Surge_Charge = parseInt(Fee_Data.Surge_Fee)
                    }
                }

                let POCC = await Buyer_Orders.findOne({ BuyerID: values.BuyerID, Whether_Cancellation_Charges_For_Next_Order: true }).select('Cancellation_Charges').lean().exec()
                if (POCC != null) {
                    Previous_Order_Cancellation_Charge = POCC.Cancellation_Charges
                }

                async.eachSeries(PDatax, (item, callback1) => {
                    Order_Total_Price += item.Total_Price
                    Order_Book_Discount += item.Total_Discount_Price
                    Order_Total_Admin_Share_Amount += item.Total_Admin_Share_Amount
                    Order_Total_Branch_Share_Amount += item.Total_Branch_Share_Amount
                    Sub_Order.push(item)
                    callback1();
                }, (err) => {
                    if (Minimum_Invoice_Amount <= parseFloat(Order_Total_Price + Order_Book_Discount)) {
                        let Max_Amount_Per_Order = parseFloat(Max_Discount_Amount); // / result.length);
                        if (Max_Amount_Per_Order >= parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)) {
                            Order_Other_Discount = parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)
                        } else {
                            Order_Other_Discount = Max_Amount_Per_Order
                        }
                    }
                    XOrder.Cart_Information = Sub_Order
                    let Bookstore_Taxes = 0;
                    let Delivery_Taxes = 0;
                    if (TaxData != null) {
                        let Taxes_Percent_Book = TaxData.Bookstore_GST
                        let Taxes_Percent_Delivery = TaxData.Delivery_GST
                        Bookstore_Taxes = parseFloat((Order_Total_Price - Order_Other_Discount - parseFloat(Order_Book_Discount.toFixed(2))) * Taxes_Percent_Book / 100)
                        Delivery_Taxes = parseFloat(Order_Delivery_Charge) * Taxes_Percent_Delivery / 100
                        Order_Taxes = Bookstore_Taxes + Delivery_Taxes;
                    }

                    XOrder.Order_Invoice = {
                        Order_Total_Price: Order_Total_Price,
                        Order_Book_Discount: parseFloat(Order_Book_Discount.toFixed(2)),
                        Order_Other_Discount: Order_Other_Discount,
                        Order_Bookstore_Taxes: Bookstore_Taxes,
                        Order_Delivery_Taxes: Delivery_Taxes,
                        Order_Taxes: Order_Taxes,
                        Order_Delivery_Charge: Order_Delivery_Charge,
                        Order_Package_Charge: Order_Package_Charge,
                        Order_Surge_Charge: Order_Surge_Charge,
                        Previous_Order_Cancellation_Charge: Previous_Order_Cancellation_Charge,
                        Order_Total_Final_Price: parseFloat(Order_Total_Price - Order_Book_Discount - Order_Other_Discount + Order_Taxes + Order_Delivery_Charge + Order_Package_Charge + Order_Surge_Charge + Previous_Order_Cancellation_Charge),
                        Order_Total_Admin_Share_Amount: Order_Total_Admin_Share_Amount,
                        Order_Total_Branch_Share_Amount: Order_Total_Branch_Share_Amount,
                    }
                    OrderData1.push(XOrder)
                });
                resolve({ success: true, extras: { Data: OrderData1[0] } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

/////////////////////////

UserController.Buyer_Order_Summary = (values, CartData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                const array = CartData.Cart_Information;
                const result = []; //for unique BranchID
                const map = new Map();
                for (const item of array) {
                    if (!map.has(item.BranchID)) {
                        map.set(item.BranchID, true);
                        result.push({
                            BranchID: item.BranchID,
                        });
                    }
                }
                let OrderData = {}
                let OrderData1 = {}
                let Other_Discount = 0;
                let Delivery_Charge = 0;
                let Dquery = {
                    BuyerID: values.BuyerID,
                    Whether_Offer_Applied: true,
                    Whether_Order_Placed: false
                }
                let DiscountData = await User_Discount_Apply.findOne(Dquery).lean().exec()
                let Minimum_Invoice_Amount = 0;
                let Discount_Percentage = 0;
                let Max_Discount_Amount = 0;
                if (DiscountData != null) {
                    let Disquery = {
                        DiscountID: DiscountData.DiscountID,
                        Status: true
                    }
                    let FinalDiscountData = await Discounts.findOne(Disquery).lean().exec();
                    if (FinalDiscountData != null) {
                        Minimum_Invoice_Amount = FinalDiscountData.Minimum_Invoice_Amount;
                        Discount_Percentage = FinalDiscountData.Discount_Percentage;
                        Max_Discount_Amount = FinalDiscountData.Max_Discount_Amount;
                    }
                }
                async.timesSeries(result.length, async (index, callback) => {
                    try {
                        let AQuery = {
                            AddressID: values.AddressID
                        };
                        let AddressData = await Buyer_Address_Log.findOne(AQuery).lean().exec();

                        let OrderNO = index + 1
                        let Sub_Order = []
                        let XOrder = {}
                        let PDatax = CartData.Cart_Information.filter(function (ResultTemp) {
                            return ResultTemp.BranchID == result[index].BranchID
                        })
                        let Order_Total_Price = 0;
                        let Order_Book_Discount = 0;
                        let Order_Other_Discount = 0
                        let Order_Delivery_Charge = 0;
                        let Order_Total_Admin_Share_Amount = 0;
                        let Order_Total_Buyer_Share_Amount = 0;
                        let Bquery = {
                            BranchID: result[index].BranchID
                        }
                        let BranchData = await Store_Branch.findOne(Bquery).lean().exec();
                        let xlat = BranchData.Latitude;
                        let xlng = BranchData.Longitude;
                        let ylat = AddressData.Latitude;
                        let ylng = AddressData.Longitude;
                        let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(xlat, xlng, ylat, ylng);
                        let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                        Distance /= 1000;//mtrs to Kms
                        parseInt(Distance.toFixed(0))
                        let DCharges = await Delivery_Price.findOne().lean().exec();
                        if (Distance <= parseInt(DCharges.Slab_Km.toFixed(0))) {
                            Order_Delivery_Charge = parseInt(DCharges.Slab_Amount.toFixed(0))
                        } else {
                            let Distance_Outof_Slab = parseInt((Distance - DCharges.Slab_Km).toFixed(0));
                            Order_Delivery_Charge = parseInt((DCharges.Slab_Amount + Distance_Outof_Slab * DCharges.Per_Km_Amount).toFixed(0));
                        }
                        async.eachSeries(PDatax, (item, callback1) => {
                            Order_Total_Price += item.Total_Price
                            Order_Book_Discount += item.Total_Discount_Price
                            Order_Total_Admin_Share_Amount += item.Total_Admin_Share_Amount
                            Order_Total_Buyer_Share_Amount += item.Total_Buyer_Share_Amount
                            Sub_Order.push(item)
                            callback1();
                        }, (err) => {
                            if (Minimum_Invoice_Amount <= parseFloat(Order_Total_Price + Order_Book_Discount)) {
                                let Max_Amount_Per_Order = parseFloat(Max_Discount_Amount / result.length);
                                if (Max_Amount_Per_Order >= parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)) {
                                    Order_Other_Discount = parseFloat(parseFloat(Order_Total_Price + Order_Book_Discount) * Discount_Percentage / 100)
                                } else {
                                    Order_Other_Discount = Max_Amount_Per_Order
                                }
                            }
                            XOrder.Cart_Information = Sub_Order
                            Other_Discount += Order_Other_Discount;
                            Delivery_Charge += Order_Delivery_Charge
                            XOrder.Order_Invoice = {
                                Order_Total_Price: Order_Total_Price,
                                Order_Book_Discount: parseFloat(Order_Book_Discount.toFixed(2)),
                                Order_Other_Discount: Order_Other_Discount * -1,
                                Order_Delivery_Charge: Order_Delivery_Charge,
                                Order_Total_Final_Price: parseFloat(Order_Total_Price + Order_Book_Discount - Order_Other_Discount + Order_Delivery_Charge),
                                Order_Total_Admin_Share_Amount: Order_Total_Admin_Share_Amount,
                                Order_Total_Buyer_Share_Amount: Order_Total_Buyer_Share_Amount,
                            }
                            OrderData['Order_No_' + OrderNO] = XOrder
                            OrderData1['Order_No_' + OrderNO] = XOrder
                        });
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    OrderData.Cart_Total_Items = CartData.Cart_Total_Items;
                    OrderData.Cart_Total_Price = parseFloat(CartData.Cart_Total_Price);
                    OrderData.Discount = parseFloat(CartData.Discount + Other_Discount * -1);
                    OrderData.Delivery_Charge = parseFloat(Delivery_Charge)
                    OrderData.Book_Variants = CartData.Book_Variants;
                    OrderData.Cart_Total_Final_Price = parseFloat(CartData.Cart_Total_Price + CartData.Discount - Other_Discount + Delivery_Charge);
                    if (values.AddressID != null) {
                        let PickupDropValidation = await UserController.Validate_Pickup_Drop_Location(values, result)
                        if (PickupDropValidation == 'Success') {
                            resolve([{ success: true, extras: { Data: OrderData } }, OrderData1])
                        } else {
                            reject({ success: false, extras: { msg: PickupDropValidation } });
                        }
                    } else {
                        resolve([{ success: true, extras: { Data: OrderData } }, OrderData1])
                    }
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Validate_Pickup_Drop_Location = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let bq = {
                    BranchID: values.BranchID
                };
                let br = await Store_Branch.findOne(bq).lean().exec();
                if (br != null) {
                    let aq = {
                        AddressID: values.AddressID
                    }
                    let ar = await Buyer_Address_Log.findOne(aq).lean().exec();
                    if (ar != null) {
                        // console.log(br.Latitude, br.Longitude)
                        // console.log(ar.Latitude, ar.Longitude)
                        let PickupData = await CommonController.Validate_Service_Zone_Location(1, br.Latitude, br.Longitude);
                        let DropData = await CommonController.Validate_Service_Zone_Location(1, ar.Latitude, ar.Longitude);
                        if (PickupData == null || DropData == null) {
                            if (PickupData == null) {
                                reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_PICKUP_LOCATION } })
                            } else {
                                reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_DROP_LOCATION } })
                            }
                        } else {
                            // console.log(PickupData.ZoneID)
                            // console.log(DropData.ZoneID)
                            if (PickupData.ZoneID != DropData.ZoneID) {
                                reject({ success: false, extras: { msg: ApiMessages.STORE_DOES_NOT_SERVE_FOR_ADDRESS } })
                            } else {
                                resolve("Success")
                            }
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_ADDRESS } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_BRANCH } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Save_Buyer_Order = (values, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OrderIDArray = []
                let aq = {
                    AddressID: values.AddressID
                }
                let AddressData = await Buyer_Address_Log.findOne(aq).lean().exec();
                let bq = {
                    BranchID: values.BranchID
                }
                let BranchData = await Store_Branch.findOne(bq).select('BranchID StoreID Branch_Name Branch_PhoneNumber Latitude Longitude Point Address').lean().exec();
                let qLatest = {
                    BuyerID: values.BuyerID
                };
                let cLatest = {
                    $set: {
                        Whether_Latest_Order: false
                    }
                };
                let rLatest = await Buyer_Orders.updateMany(qLatest, cLatest).lean().exec();
                let orderid = uuid.v4()
                let Data = {
                    OrderID: orderid,
                    BuyerID: values.BuyerID,
                    Cart_Information: OrderData.Cart_Information,
                    Order_Invoice: OrderData.Order_Invoice,
                    Delivery_Address_Information: AddressData,
                    Branch_Information: BranchData,
                    created_at: new Date(),
                    updated_at: new Date()
                }
                let SaveOrderData = await Buyer_Orders(Data).save();
                OrderIDArray.push(orderid)
                resolve(OrderIDArray)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Payment_for_Buyer_Order = (values, OrderIDArrayData, OrderData, buyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (values.Payment_Mode == 1) {
                    let calbk;
                    let onlineamount = OrderData.Order_Invoice.Order_Total_Final_Price - buyerData.Available_Amount;
                    let Amount_Used_From_Wallet = buyerData.Available_Amount;
                    if (onlineamount <= 0) {
                        onlineamount = 0;
                        Amount_Used_From_Wallet = OrderData.Order_Invoice.Order_Total_Final_Price;
                    }
                    if (parseInt(onlineamount) == 0) {
                        calbk = false
                    } else {
                        calbk = true
                    }
                    let Orderlength = OrderIDArrayData.length
                    let Amount_Used_From_Wallet_Per_Order = parseFloat(Amount_Used_From_Wallet / Orderlength);
                    let Online_Amount_Per_Order = parseFloat(onlineamount / Orderlength);
                    let TransactionIDArrayData = [];
                    async.eachSeries(OrderIDArrayData, async (item, callback) => {
                        try {
                            let query = {
                                OrderID: item,
                                Status: false
                            };
                            let Result = await Buyer_Orders.findOne(query).lean().exec();
                            if (Result != null) {
                                let TranxID = uuid.v4();
                                TransactionIDArrayData.push(TranxID);
                                let Order_Number = await CommonController.Random_12_Digit_Number();
                                let OTP = await CommonController.Random_OTP_Number()
                                let Amount_Paid = {
                                    Amount_Used_From_Wallet: Amount_Used_From_Wallet_Per_Order,
                                    Amount_Paid_Online: Online_Amount_Per_Order,
                                    Total_Amount_Paid: Amount_Used_From_Wallet_Per_Order + Online_Amount_Per_Order
                                }
                                let qerryPay = {
                                    OrderID: item,
                                };
                                let changes = {
                                    $set: {
                                        Status: true,
                                        TransactionID: TranxID,
                                        OTP: OTP,
                                        Amount_Paid: Amount_Paid,
                                        Order_Number: Order_Number,//12 digits randaom number                            
                                    }
                                }
                                let updateOrderData = await Buyer_Orders.findOneAndUpdate(qerryPay, changes).lean().exec()
                            }
                            callback();
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        let ResultData = {
                            OrderIDArrayData: OrderIDArrayData,
                            TransactionIDArrayData: TransactionIDArrayData,
                            Callback: calbk,
                            Final_Amount: parseFloat(onlineamount.toFixed(2)),
                            RFinal_Amount: Math.floor(parseInt(parseFloat(onlineamount.toFixed(2)) * 100))
                        }
                        resolve({ success: true, extras: { Data: ResultData } })
                        if (calbk == false) {
                            let updateTranx = await UserController.Complete_Payment_for_Buyer_Order(TransactionIDArrayData, '', values.Payment_Mode);
                        }
                    });
                } else if (values.Payment_Mode == 2) {

                    let calbk = false
                    let Orderlength = OrderIDArrayData.length
                    let Amount_COD = OrderData.Order_Invoice.Order_Total_Final_Price;
                    let TransactionIDArrayData = [];
                    async.eachSeries(OrderIDArrayData, async (item, callback) => {
                        try {
                            let query = {
                                OrderID: item,
                                Status: false
                            };
                            let Result = await Buyer_Orders.findOne(query).lean().exec();
                            if (Result != null) {
                                let TranxID = uuid.v4();
                                TransactionIDArrayData.push(TranxID);
                                let Order_Number = await CommonController.Random_12_Digit_Number();
                                let OTP = await CommonController.Random_OTP_Number()

                                let qerryPay = {
                                    OrderID: item,
                                };
                                let changes = {
                                    $set: {
                                        Status: true,
                                        TransactionID: TranxID,
                                        OTP: OTP,
                                        Amount_COD: Amount_COD,
                                        Order_Number: Order_Number,//12 digits randaom number                            
                                    }
                                }
                                let updateOrderData = await Buyer_Orders.findOneAndUpdate(qerryPay, changes).lean().exec()
                            }
                            callback();
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        let ResultData = {
                            OrderIDArrayData: OrderIDArrayData,
                            TransactionIDArrayData: TransactionIDArrayData,
                            Callback: calbk,
                            Final_Amount: 0
                        }
                        resolve({ success: true, extras: { Data: ResultData } })
                        if (calbk == false) {
                            let updateTranx = await UserController.Complete_Payment_for_Buyer_Order(TransactionIDArrayData, '', values.Payment_Mode);
                        }
                    });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Complete_Payment_for_Buyer_Order = (TransactionIDArrayData, WebhookData, Payment_Mode) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OrdersLength = TransactionIDArrayData.length;
                let BuyersQuery;
                async.eachSeries(TransactionIDArrayData, async (item, callback) => {
                    try {
                        let query = {
                            TransactionID: item,
                            Status: true
                        }
                        let Result = await Buyer_Orders.findOne(query).lean();
                        if (Result != null) {
                            let pay_type;
                            if (WebhookData == '') {
                                if (Payment_Mode == 2) {
                                    pay_type = 4;
                                } else {
                                    pay_type = 1;
                                }
                            } else {
                                if ((WebhookData.Amount / OrdersLength) == Result.Order_Invoice.Order_Total_Final_Price) {
                                    pay_type = 2;
                                } else {
                                    pay_type = 3;
                                }
                            }
                            BuyersQuery = {
                                BuyerID: Result.BuyerID
                            }
                            let BuyerData = await Buyers.findOne(BuyersQuery).lean().exec();
                            let Payment_Status = 1;
                            if (Payment_Mode == 1) {
                                Payment_Status = 3
                                /// amount deduct from from buyer wallet and maintain log for it
                                let Wallet_bal_Used = Result.Amount_Paid.Amount_Used_From_Wallet;
                                if (Wallet_bal_Used > 0) {
                                    let Data = {
                                        OrderID: Result.OrderID,
                                        Amount: Wallet_bal_Used,
                                        BuyerData: BuyerData
                                    }
                                    let WData = {
                                        LogID: uuid.v4(),
                                        BuyerID: Result.BuyerID,
                                        Type: 3, // amount debited from user wallet for completing Order
                                        Amount: Wallet_bal_Used,
                                        Data: Data,
                                        Time: new Date()
                                    }
                                    let Resultlog = Buyer_Share_Logs(WData).save();
                                    let BuyerChanges = {
                                        $inc: {
                                            Available_Amount: Wallet_bal_Used * -1,
                                            Withdrawn_Amount: Wallet_bal_Used
                                        },
                                    };
                                    let fndupdoptions = {
                                        upsert: true,
                                        setDefaultsOnInsert: true,
                                        new: true
                                    }
                                    let updatBuyerData = await Buyers.findOneAndUpdate(BuyersQuery, BuyerChanges, fndupdoptions).lean().exec();
                                }
                            } else {
                                Payment_Status = 4
                            }
                            //////
                            let changes = {
                                $set: {
                                    Payment_Status: Payment_Status,
                                    Order_Status: 1,
                                    WebHookData: WebhookData,
                                    Payment_Type: pay_type,
                                    Order_Report: {
                                        Title: "Order Received",
                                        Description: "Order Received",
                                        Time: new Date()
                                    },
                                    Updated_at: new Date()
                                }
                            }
                            let Resultx = await Buyer_Orders.findOneAndUpdate(query, changes).lean().exec();
                            let Order_Item_Count = 0;
                            let Cart_Products = BuyerData.Cart_Information.filter(function (ResultTemp) {
                                if (ResultTemp.BranchID == Result.Branch_Information.BranchID) {
                                    Order_Item_Count += ResultTemp.Product_Quantity
                                } else {
                                    return ResultTemp
                                }
                            });

                            let finalChanges = {
                                $set: {
                                    Cart_Information: Cart_Products,
                                    updated_at: new Date()
                                },
                                $inc: {
                                    Cart_Total_Items: Order_Item_Count * -1,
                                    Cart_Total_Price: Result.Order_Invoice.Order_Total_Price * -1,
                                },
                            }
                            let fndupdoptionsx = {
                                upsert: true,
                                setDefaultsOnInsert: true,
                                new: true
                            }
                            let updatBuyerData = await Buyers.findOneAndUpdate(BuyersQuery, finalChanges, fndupdoptionsx).lean().exec();

                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Orders Generated Successfully");
                    console.log("Orders Generated Successfully");
                    //change status of Cancellation_Allowed
                    let TimeinSec = config.Cancel_Time;
                    let Today = moment().utcOffset(330).format();
                    let Date_for_Cancellation_Allow = moment(Today).add(TimeinSec, 'seconds').utcOffset(330).format()
                    // console.log(Today)
                    // console.log(Date_for_Cancellation_Allow)
                    let queryTx = {
                        TransactionID: TransactionIDArrayData[0],
                        Status: true
                    }
                    let finalChangesTx = {
                        $set: {
                            Cancellation_Allowed_DateTime: Date_for_Cancellation_Allow,
                            updated_at: new Date()
                        },
                    }
                    let fndupdoptionsTx = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let preUpdate = await Buyer_Orders.findOneAndUpdate(queryTx, finalChangesTx, fndupdoptionsTx).lean().exec();
                    var j = schedule.scheduleJob(Date_for_Cancellation_Allow, function () {

                        let queryT = {
                            TransactionID: TransactionIDArrayData[0],
                            Status: true
                        }
                        let finalChangesT = {
                            $set: {
                                Cancellation_Allowed: false,
                                updated_at: new Date()
                            },
                        }
                        let fndupdoptionsT = {
                            upsert: true,
                            setDefaultsOnInsert: true,
                            new: true
                        }
                        Buyer_Orders.findOneAndUpdate(queryT, finalChangesT, fndupdoptionsT).lean().exec().then((UpdateS) => {
                            console.log("Completed");
                        })

                    });

                    let finalChangesx = {
                        $set: {
                            updated_at: new Date()
                        },
                        $inc: {
                            Orders_Count: 1
                        },
                    }
                    let fndupdoptionsxx = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let update = await Buyers.findOneAndUpdate(BuyersQuery, finalChangesx, fndupdoptionsxx).lean().exec();

                    //
                    let updatBuyerData = await Buyers.findOne(BuyersQuery).lean().exec();

                    if (updatBuyerData.Ref_PhoneNumber != '') {
                        let queryyy = {
                            TransactionID: TransactionIDArrayData[0],
                            Status: true
                        }
                        let Resultyy = await Buyer_Orders.findOne(queryyy).lean();
                        let Referal_Price_Data = await Referal_Price.findOne().lean().exec();
                        let ftl = Referal_Price_Data.First_Time_Purchase;
                        let Referal_Data = await Buyers.findOne({ Buyer_PhoneNumber: updatBuyerData.Ref_PhoneNumber, Status: true }).lean().exec()
                        let Referal_3_Data = Referal_Data.My_3_Referals
                        let PDatax = Referal_3_Data.filter(function (ResultTemp) {
                            if (ResultTemp.BuyerID == updatBuyerData.BuyerID && ResultTemp.wether_Order_Completed == false) {
                                return ResultTemp
                            }
                        });
                        if (PDatax.length == 1) {
                            let BuyerQuery = {
                                BuyerID: Referal_Data.BuyerID,
                                'My_3_Referals.BuyerID': updatBuyerData.BuyerID
                            }
                            let BuyerData = await Buyers.findOne(BuyerQuery).lean().exec();
                            let Rchanges = {
                                $inc: {
                                    Total_Amount: ftl,
                                    Available_Amount: ftl
                                },
                                $set: {
                                    update: new Date(),
                                    'My_3_Referals.$.wether_Order_Completed': true
                                }
                            }
                            let RUpdateData = await Buyers.updateOne(BuyerQuery, Rchanges).lean()
                            // add log for user wallet
                            let Data = {
                                BuyerData: Referal_Data,
                                Amount: ftl,
                                ReferalData: updatBuyerData,
                                OrderData: Resultyy
                            }
                            let WData = {
                                LogID: uuid.v4(),
                                BuyerID: Referal_Data.BuyerID,
                                Type: 7, // amount Credited for Referal First Order 
                                Amount: ftl,
                                Data: Data,
                                Time: new Date()
                            }
                            let Resultlog = Buyer_Share_Logs(WData).save();
                            // deduct Amount from Company Wallet
                            let WDataw = {
                                LogID: uuid.v4(),
                                AdminID: '06f374d4-fa3f-4911-b9b6-b367fc0b3e26',
                                Type: 6, // amount deduct from Bookafella Admin wallet for Referal
                                Amount: ftl,
                                Data: Data,
                                Time: new Date()
                            }
                            let Resultlogw = Company_Wallet_Log(WDataw).save();
                            let Cfndupdquery = {

                            }
                            let Cfndupdchanges = {
                                $inc: {
                                    Available_Amount: ftl * -1,
                                    Withdrawn_Amount: ftl
                                }
                            }
                            let Cfndupdoptions = {
                                upsert: true,
                                setDefaultsOnInsert: true,
                                new: true
                            }
                            let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                            //Send Notification BuyerData(Parent), Referal_Data(child), ftl(Amount), Resultyy(Order)
                            let SendNotification = await FcmController.Notification_For_Referal_First_Order(BuyerData, Referal_Data, ftl, Resultyy)
                        }
                    }
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_Buyer_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: {
                        $ne: 0
                    },
                    BuyerID: values.BuyerID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let finalResult = await UserController.Get_Buyer_Single_Order(item, 2);
                        Data.push(finalResult);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_Buyer_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Orderstats;
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                if (values.Status == true) {
                    Orderstats = {
                        $nin: [3, 11]
                    }
                } else {
                    Orderstats = {
                        $in: [3]
                    }
                }
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Order_Status: Orderstats,
                    // Cancel_Status: 0,
                    Whether_Cancellation_Prodessed: {
                        $ne: 1
                    },
                    BuyerID: values.BuyerID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let finalResult = await UserController.Get_Buyer_Single_Order(item, 2);
                        Data.push(finalResult);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


UserController.Get_Buyer_Single_Order = (values, Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    // for (let Order of Result) {
                    let OrderReport = []
                    for (let Order_Report of Result.Order_Report) {
                        let Data = new Object();
                        if (Order_Report.Title == "Order Received") {
                            Data = {
                                Status: "Order Received",
                                Description: Result.Branch_Information.Branch_Name + " has received your order successfully",
                                Time: moment(Order_Report.Time).utcOffset("+05:30").format()
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Driver Assigned") {
                            Data = {
                                Status: "Driver Assigned",
                                Description: "Your assigned delivery executive for this order is Mr." + Result.Driver_Information.Driver_Name,
                                Time: moment(Order_Report.Time).utcOffset("+05:30").format()
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Order Picked") {
                            Data = {
                                Status: "Order Picked Up",
                                Description: "Mr." + Result.Driver_Information.Driver_Name + " has arrived at the bookstore to pick up your order",
                                Time: moment(Order_Report.Time).utcOffset("+05:30").format()
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Drop Stated") {
                            let q = {
                                OrderID: Result.OrderID
                            }
                            let TripData = await Trips.findOne(q).lean().exec();
                            console.log(q)
                            console.log(TripData)
                            console.log(TripData.Total_Eta.toFixed(2))
                            Data = {
                                Status: "Order In Transit",
                                Description: "Your order has been picked up and is on the way for delivery ETA: approximately " + TripData.Total_Eta.toFixed(2) + " Min",
                                Time: moment(Order_Report.Time).utcOffset("+05:30").format()
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Order Delivered") {
                            Data = {
                                Status: "Order Completed",
                                Description: "Your order has been delivered successfully, Received by: " + Result.Delivered_To + ", Ordered by: " + Result.Delivery_Address_Information.Name,
                                Time: moment(Order_Report.Time).utcOffset("+05:30").format()
                            }
                            OrderReport.push(Data)
                        }
                    }
                    delete Result.Order_Report;
                    Result.Order_Report = OrderReport
                    // }
                    if (Boolify(Result.Cancellation_Allowed)) {
                        let today = new Date()
                        var seconds = (new Date(Result.Cancellation_Allowed_DateTime).getTime() - today.getTime()) / 1000;
                        Result.Free_Cancellation_Time_Left = seconds
                    } else {
                        Result.Free_Cancellation_Time_Left = 0
                    }
                    delete Result.WebHookData
                    async.eachSeries(Result.Cart_Information, async (item, callback) => {
                        try {
                            let queryx = {
                                BuyerID: values.BuyerID,
                                OrderID: values.OrderID,
                                ProductID: item.ProductID
                            };
                            let ResultRate = await Rating_Logs.findOne(queryx).lean().exec();
                            if (ResultRate == null) {
                                item.Rating_Status = false
                            } else {
                                item.Rating_Status = true
                            }
                            callback();
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        if (Type == 1) {
                            resolve({ success: true, extras: { Data: Result } });
                        } else {
                            resolve(Result)
                        }
                    });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.PERMISSION_DENIED } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

// UserController.Check_Cancellation_Refund_Amount = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let query = {
//                     BuyerID: values.BuyerID,
//                     OrderID: values.OrderID
//                 };
//                 let Result = await Buyer_Orders.findOne(query).lean().exec();
//                 if (Result != null) {
//                     if(Result.Cancel_Status == 0){
//                     let Refund_Amount = 0;
//                     let Cancellation_Charges = 0;
//                     let Free_Cancellation_Time_Left = 0;
//                     if (Boolify(Result.Cancellation_Allowed)) {
//                         let today = new Date()
//                         var seconds = (Result.Cancellation_Allowed_DateTime.getTime() - today.getTime()) / 1000;
//                         Free_Cancellation_Time_Left = seconds
//                         Cancellation_Charges = 0
//                         if (Result.Payment_Type == 4) {
//                             Refund_Amount = 0
//                         } else {
//                             Refund_Amount = Result.Amount_Paid.Total_Amount_Paid
//                         }
//                     } else {
//                         Free_Cancellation_Time_Left = 0
//                         let BranchData = await CommonController.Check_for_Branch({ BranchID: Result.Branch_Information.BranchID });
//                         let FeeData = await Zone_Fees.findOne({ ZoneID: BranchData.ZoneID }).lean().exec()
//                         let Cancel_Percent = 0;
//                         Cancellation_Charges = 0
//                         if (FeeData != null) {
//                             Cancel_Percent = FeeData.Cancel_Percent;
//                         }
//                         if (Result.Payment_Type == 4) {
//                             Cancellation_Charges = parseFloat((Result.Amount_COD * Cancel_Percent / 100).toFixed(2));
//                             Refund_Amount = 0
//                         } else {
//                             Cancellation_Charges = parseFloat((Result.Amount_Paid.Total_Amount_Paid * Cancel_Percent / 100).toFixed(2));
//                             Refund_Amount = parseFloat((Result.Amount_Paid.Total_Amount_Paid - Cancellation_Charges).toFixed(2))
//                         }
//                     }
//                     let Data = {
//                         Cancellation_Charges: Cancellation_Charges,
//                         Refund_Amount: Refund_Amount,
//                         Free_Cancellation_Time_Left: Free_Cancellation_Time_Left,
//                         Free_Cancellation_Allowed_DateTime: moment(Result.Cancellation_Allowed_DateTime).utcOffset(330).format()
//                     }
//                     resolve({ success: true, extras: { Data: Data } })
//                 }else {
//                     reject({success: false, extras:{msg: ApiMessages.ORDER_ALREADY_CANCELLED}})
//                 }
//                 } else {
//                     reject({ success: false, extras: { msg: ApiMessages.PERMISSION_DENIED } });
//                 }
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }


UserController.Check_Cancellation_Validatity = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    if (Result.Cancel_Status == 0) {
                        let Free_Cancellation_Time_Left = 0;
                        if (Boolify(Result.Cancellation_Allowed)) {
                            let today = new Date()
                            var seconds = (new Date(Result.Cancellation_Allowed_DateTime).getTime() - today.getTime()) / 1000;
                            Free_Cancellation_Time_Left = seconds
                        } else {
                            Free_Cancellation_Time_Left = 0
                        }
                        let Data = {
                            Free_Cancellation_Time_Left: Free_Cancellation_Time_Left,
                            Free_Cancellation_Allowed_DateTime: moment(Result.Cancellation_Allowed_DateTime).utcOffset(330).format()
                        }
                        resolve({ success: true, extras: { Data: Data } })
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ORDER_ALREADY_CANCELLED } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.PERMISSION_DENIED } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


UserController.Cancel_Single_Order = (values, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    if (Boolify(OrderData.Cancellation_Allowed)) {
                        let Refund_Amount = 0
                        if (Result.Payment_Type != 4) {
                            Refund_Amount = Result.Amount_Paid.Total_Amount_Paid
                        }
                        let changes = {
                            $set: {
                                Cancel_Status: 1, //1- buyer
                                Whether_Cancellation_Prodessed: 1,
                                Whether_Cancellation_Refund_Prodessed: true,
                                Cancellation_Charges: 0,
                                Refund_Amount: Refund_Amount,
                                Cancelled_By: {
                                    BuyerID: values.BuyerID,
                                },
                                Cancel_Reason: values.Cancel_Reason,
                                updated_at: new Date()
                            }
                        }
                        let updateStatus = await Buyer_Orders.updateOne(query, changes).lean().exec();
                        resolve({ success: true, extras: { Status: "Cancelled Successfully" } })

                        if (Refund_Amount != 0) {
                            // Refund Process
                            
                        }
                    } else {
                        let changes = {
                            $set: {
                                Cancel_Status: 1, //1- buyer
                                Cancelled_By: {
                                    BuyerID: values.BuyerID,
                                },
                                Cancel_Reason: values.Cancel_Reason,
                                updated_at: new Date()
                            }
                        }
                        let updateStatus = await Buyer_Orders.updateOne(query, changes).lean().exec();
                        resolve({ success: true, extras: { Status: "Cancelled Successfully" } })
                        // Admin will process the Refund
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.PERMISSION_DENIED } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


UserController.Home_Screen_Details = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                };
                let Result = await Buyers.findOne(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID -Product_Price_Divisions -Product_Image_Data').lean().exec();
                Result.Available_Amount = parseFloat(Result.Available_Amount.toFixed(2))
                Result.Withdrawn_Amount = parseFloat(Result.Withdrawn_Amount.toFixed(2))
                Result.Total_Amount = parseFloat(Result.Total_Amount.toFixed(2))
                let ImageData = await Buyer_Images.findOne(query).lean().exec()
                if (ImageData != null) {
                    ImageData.Image_Data = await CommonController.Common_Image_Response_Single_Image(ImageData.Status, ImageData.Image_Data);
                    Result.Image_Data = ImageData.Image_Data;
                    Result.Buyer_Image_Available = true;
                } else {
                    Result.Image_Data = {
                        ImageID: "",
                        Image50: "",
                        Image100: "",
                        Image250: "",
                        Image550: "",
                        Image900: "",
                        ImageOriginal: ""
                    };
                    Result.Buyer_Image_Available = false;
                }
                resolve({ success: true, extras: { Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Buyer_Wallet_log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                };
                let sortOptions = {
                    Time: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Buyer_Share_Logs.countDocuments(query).lean().exec();
                let Result = await Buyer_Share_Logs.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                for (let Log of Result) {
                    if (Log.Type == 1) {
                        Log.Type_Value = "Amount Credited for Referal login"
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    } else if (Log.Type == 3) {
                        // console.log(Log.Data.OrderID)
                        let q = {
                            OrderID: Log.Data.OrderID
                        }
                        let OrderData = await Buyer_Orders.findOne(q).lean().exec();
                        // console.log(1)
                        // console.log(OrderData)
                        Log.Type_Value = "Amount of " + Log.Data.Amount + " debited from user wallet for completing Order " + OrderData.Order_Number
                        Log.Tx_Type = 2;
                        Log.Tx_Type_Value = "Debit"
                    } else if (Log.Type == 4 || Log.Type == 14) {
                        let q = {
                            OrderID: Log.Data.OrderID
                        }
                        let OrderData = await Buyer_Orders.findOne(q).lean().exec();
                        // console.log(2)
                        // console.log(OrderData)
                        Log.Type_Value = "Refund Amount of " + Log.Data.Amount + " credit to user wallet for Order " + OrderData.Order_Number
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    } else if (Log.Type == 7) {
                        Log.Type_Value = "Amount of " + Log.Amount + " Credited for Referal First Completing Order"
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    } else if (Log.Type == 15) {
                        Log.Type_Value = "You Earned Amount of Rs: " + Log.Amount + " from Write and Earn"
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    } else if (Log.Type == 16) {
                        Log.Type_Value = "Amount Credited By Bookafella"
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    } else {
                        Log.Type_Value = "Amount Credited By Bookafella"
                        Log.Tx_Type = 1;
                        Log.Tx_Type_Value = "Credit"
                    }
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Help_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    SNo: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Help_Data.countDocuments(query).lean().exec();
                let Result = await Help_Data.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Product_Rating_On_Order = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let RCheck = {
                    ProductID: values.ProductID,
                    BuyerID: values.BuyerID
                }
                let RResult = await Rating_Logs.findOne(RCheck).lean().exec();
                if (RResult == null) {


                    let Oquery = {
                        OrderID: values.OrderID
                    };
                    let OData = await Buyer_Orders.findOne(Oquery).lean().exec();
                    if (OData != null) {
                        let Pquery = {
                            ProductID: values.ProductID,
                            'Product_Data_For_Branch.BranchID': OData.Branch_Information.BranchID
                        }
                        let PResult = await Store_Products.findOne(Pquery).lean().exec();
                        let PDatax = PResult.Product_Data_For_Branch.filter(function (ResultTemp) {
                            return ResultTemp.BranchID == OData.Branch_Information.BranchID
                        });

                        let Avg_Rating = (parseInt(values.Rating) + PResult.Rating) / (PResult.Rating_Count + 1);
                        let Avg_Rating1 = ((parseInt(values.Rating) + PDatax[0].Rating) / (PDatax[0].Rating_Count + 1))
                        let changes = {
                            $set: {
                                Avg_Rating: Avg_Rating,
                                'Product_Data_For_Branch.$.Avg_Rating': Avg_Rating1,
                            },
                            $inc: {
                                Rating_Count: 1,
                                Rating: parseInt(values.Rating),
                                'Product_Data_For_Branch.$.Rating_Count': 1,
                                'Product_Data_For_Branch.$.Rating': parseInt(values.Rating),
                            },
                        };
                        let UpdateRating = await Store_Products.updateOne(Pquery, changes).lean().exec();
                        let RateData = {
                            RatingID: uuid.v4(),
                            BranchID: OData.Branch_Information.BranchID,
                            OrderID: values.OrderID,
                            ProductID: values.ProductID,
                            Branch_Name: OData.Branch_Information.Branch_Name,
                            BuyerID: BuyerData.BuyerID,
                            Buyer_Name: BuyerData.Buyer_Name,
                            Rating: values.Rating,
                            Rating_Description: values.Rating_Description,
                            Time: new Date()
                        }
                        let SaveDat = await Rating_Logs(RateData).save();
                        resolve({ success: true, extras: { Status: "Rating Updated Successfully" } });
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.RATING_ALREADY_EXIST } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Buyer_Rating = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Rating_Logs.countDocuments(query).lean().exec();
                let Result = await Rating_Logs.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let o = {
                            OrderID: item.OrderID,
                            BuyerID: values.BuyerID
                        };
                        let R = await Buyer_Orders.findOne(o).lean().exec();
                        if (R != null) {
                            item.Order_Number = R.Order_Number;
                            Data.push(item);
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } });
                });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_Product_Rating = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ProductID: values.ProductID
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Rating_Logs.countDocuments(query).lean().exec();
                let Result = await Rating_Logs.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let Bquery = {
                            BuyerID: item.BuyerID
                        }
                        let ImageData = await Buyer_Images.findOne(Bquery).lean().exec()
                        if (ImageData != null) {
                            ImageData.Image_Data = await CommonController.Common_Image_Response_Single_Image(ImageData.Status, ImageData.Image_Data);
                            item.Image_Data = ImageData.Image_Data;
                            item.Buyer_Image_Available = true;
                        } else {
                            item.Image_Data = {
                                ImageID: "",
                                Image50: "",
                                Image100: "",
                                Image250: "",
                                Image550: "",
                                Image900: "",
                                ImageOriginal: ""
                            };
                            item.Buyer_Image_Available = false;
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Update_Buyer_Image = (values, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let Result = await Buyer_Images.findOne(query).lean().exec()
                if (Result != null) {
                    let RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.Image50);
                    RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.Image100);
                    RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.Image250);
                    RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.Image550);
                    RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.Image900);
                    RemoveImage = await AWSController.DeleteAWSImage(Result.Image_Data.ImageOriginal);
                    let changes = {
                        $set: {
                            Image_Data: ImageData,
                            updated_at: new Date(),
                        }
                    }
                    let UpdateData = await Buyer_Images.updateOne(query, changes).lean().exec();
                } else {
                    let Data = {
                        BuyerImageID: uuid.v4(),
                        BuyerID: values.BuyerID,
                        Image_Data: ImageData,
                        created_at: new Date(),
                        updated_at: new Date(),
                    }
                    let SaveData = await Buyer_Images(Data).save();
                }
                resolve({ success: true, extras: { Status: "Buyer Image Uploaded Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Update_Buyer_Profile = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let changes = {
                    $set: {
                        Buyer_Name: values.Buyer_Name,
                        Buyer_Email: values.Buyer_Email,
                        DOB: moment(values.DOB, config.Take_Date_Format).toDate(),
                        updated_at: new Date()
                    }
                }
                let UpdateData = await Buyers.updateOne(query, changes).lean()
                resolve({ success: true, extras: { Status: "Profile Updated Successfully" } })

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


UserController.Get_Buyer_Recent_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let FinalData = {};
                let Sold_By_Branch = [];
                let Order_Status = [];
                let Delivery_Status = [];
                let ETA_Status = [];
                let Cart_Information = [];
                let Delivery_Information;
                // async.eachSeries(values.TransactionIDArrayData, async (item, callback) => {
                //     try {
                //         console.log(item)
                let query = {
                    Whether_Latest_Order: true,
                    Status: true
                }
                let Result = await Buyer_Orders.findOne(query).lean();
                if (Result != null) {
                    Cart_Information.push(Result.Cart_Information);
                    Delivery_Information = Result.Delivery_Address_Information;
                    let BData = {
                        Branch_Information: Result.Branch_Information,
                        Order_Number: Result.Order_Number
                    }
                    Sold_By_Branch.push(BData);
                    let OData = {
                        Order_Status: Result.Order_Status,
                        Order_Number: Result.Order_Number
                    }

                    Order_Status.push(OData);
                    let ORData = {
                        Order_Report: Result.Order_Report,
                        Order_Number: Result.Order_Number
                    }
                    Delivery_Status.push(ORData)
                    let ETA = {};
                    if (Result.Order_Status > 8) {
                        let xlat = await Result.Branch_Information.Latitude;
                        let xlng = await Result.Branch_Information.Longitude;
                        let ylat = await Result.Delivery_Address_Information.Latitude;
                        let ylng = await Result.Delivery_Address_Information.Longitude;
                        // console.log(xlat, xlng, ylat, ylng)
                        let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(xlat, xlng, ylat, ylng);
                        let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                        let Duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
                        Duration *= 1000;//seconds to milliseconds
                        ETA.Duration = Duration;
                        ETA.Order_Number = Result.Order_Number
                    } else {
                        ETA.Duration = 0,
                            ETA.Order_Number = Result.Order_Number
                    }
                    ETA_Status.push(ETA);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                }
                //         callback();
                //     } catch (error) {
                //         callback(error);
                //     }
                // }, async (err) => {
                //     if (err) reject(err);
                FinalData.Cart_Information = Cart_Information
                FinalData.Sold_By_Branch = Sold_By_Branch
                FinalData.Order_Status = Order_Status
                FinalData.Delivery_Status = Delivery_Status
                FinalData.ETA_Status = ETA_Status
                FinalData.Delivery_Information = Delivery_Information
                resolve({ success: true, extras: { Data: FinalData } });
                // });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Banner = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    SNo: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Banner.countDocuments(query).lean().exec();
                let Result = await Banner.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                ////////////////////
                let Data = []
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let ImageData = await CommonController.Common_Image_Response_Single_Image(true, item.Image_Data);
                        item.Image_Data = ImageData
                        Data.push(item);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } })
                });
                /////////////////////
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Get_Quote = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let Result = await Quote.find(query).select('-_id -__v -updated_at').lean().exec();
                resolve({ success: true, extras: { Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Categories = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    SNo: 1
                };
                let Result = await Category.find(query).select('-_id -__v').sort(sortOptions).lean();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Get_Similar_Products = (values, CurrentLocationData, ProductData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let queryStory = {
                    Status: true,
                    ZoneID: CurrentLocationData.ZoneID
                }
                let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
                let query = {
                    'Product_Data_For_Branch.BranchID': {
                        $in: StoreID_Array
                    },
                    Status: true,
                    Whether_Deleted: {
                        $ne: true
                    },
                    CountryID: CurrentLocationData.CountryID,
                    CityID: CurrentLocationData.CityID,
                    ZoneID: CurrentLocationData.ZoneID,
                    CategoryID: ProductData.CategoryID,
                    Status: true,
                };
                if (!Boolify(values.Show_Current_Product)) {
                    query.ProductID = {
                        $ne: values.ProductID
                    }
                }
                let sortOptions = {
                    Views: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Store_Products.countDocuments(query).lean().exec();
                let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                let ProductsData = [];
                for (const Product of Result) {
                    let ProductData = await UserController.Single_Product_Data(values, Product.ProductID)
                    ProductsData.push(ProductData)
                }
                resolve({success: true, extras: {Count: Count, Data: ProductsData}})
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.List_All_Available_Discounts_Fetch_Zone_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let lat = parseFloat(values.lat);
                let lng = parseFloat(values.lng);
                let Point = [lng, lat];
                let query = {
                    Service_Type: {
                        $in: [1, 2, 3]
                    },
                    Geometry: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: Point
                            }
                        }
                    },
                    Status: true
                };
                let Result = await ZONES.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.List_All_Available_Discounts = (values, UserData, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let User_Orders_Count = await Buyer_Orders.countDocuments({ BuyerID: values.BuyerID, Order_Status: 1 }).lean();
                // console.log(User_Orders_Count)
                let date = new Date();
                let query = {
                    Status: true,
                    CountryID: ZoneData.CountryID,
                    CityID: ZoneData.CityID,
                    Start_Date: {
                        $lte: date
                    },
                    End_Date: {
                        $gt: date
                    }
                };
                let All_Others_query = {
                    Whether_Customer_Target_Discount: true,
                    Customer_Target_Type: 3,
                    Whether_Referral_Discount: false
                }
                All_Others_query = Object.assign({}, All_Others_query, query);
                // console.log(All_Others_query);
                let All_Others_DiscountID_Array = await Discounts.distinct('DiscountID', All_Others_query).lean();
                // console.log(All_Others_DiscountID_Array)
                let Referral_DiscountID_Array = [];
                let New_Customer_DiscountID_Array = [];
                let Old_Customer_DiscountID_Array = [];
                let Selected_Customer_DiscountID_Array = [];
                let Final_DiscountID_Array = [];
                if (UserData.Whether_Referral) {
                    let Referral_Query = {
                        Whether_Customer_Target_Discount: false,
                        Whether_Referral_Discount: true
                    }
                    Referral_Query = Object.assign({}, Referral_Query, query);
                    Referral_DiscountID_Array = await Discounts.distinct('DiscountID', Referral_Query).lean();
                }
                if (User_Orders_Count <= 0) {
                    let thisquery = {
                        Whether_Customer_Target_Discount: true,
                        Customer_Target_Type: 1,
                        Whether_Referral_Discount: false
                    }
                    thisquery = Object.assign({}, thisquery, query);
                    New_Customer_DiscountID_Array = await Discounts.distinct('DiscountID', thisquery).lean();
                }
                if (User_Orders_Count > 0) {
                    let thisquery = {
                        Whether_Customer_Target_Discount: true,
                        Customer_Target_Type: 2,
                        Whether_Referral_Discount: false
                    }
                    thisquery = Object.assign({}, thisquery, query);
                    Old_Customer_DiscountID_Array = await Discounts.distinct('DiscountID', thisquery).lean();
                }
                let selectedcustomerquery = {
                    Whether_Customer_Target_Discount: true,
                    Customer_Target_Type: 4,
                    "Targeted_Customers.BuyerID": UserData.BuyerID,
                    Whether_Referral_Discount: false
                }
                selectedcustomerquery = Object.assign({}, selectedcustomerquery, query);
                // console.log(selectedcustomerquery)
                Selected_Customer_DiscountID_Array = await Discounts.distinct('DiscountID', selectedcustomerquery).lean();

                Final_DiscountID_Array = await Final_DiscountID_Array.concat(All_Others_DiscountID_Array);
                Final_DiscountID_Array = await Final_DiscountID_Array.concat(Referral_DiscountID_Array);
                Final_DiscountID_Array = await Final_DiscountID_Array.concat(New_Customer_DiscountID_Array);
                Final_DiscountID_Array = await Final_DiscountID_Array.concat(Old_Customer_DiscountID_Array);
                Final_DiscountID_Array = await Final_DiscountID_Array.concat(Selected_Customer_DiscountID_Array);
                // console.log(Final_DiscountID_Array);
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let dddquery = {
                    DiscountID: {
                        $in: Final_DiscountID_Array
                    }
                };
                // console.log(dddquery)
                let Count = await Discounts.countDocuments(dddquery).lean().exec();
                let Result = await Discounts.find(dddquery).select('-_id -__v -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Apply_Discount_Offer_Fetch_Zone_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Service_Type = 1;
                // let Sub_Trip_Type = PriceQuoteData.Sub_Trip_Type;
                let lat = values.lat;//(Sub_Trip_Type == 1 || Sub_Trip_Type == 2) ? PriceQuoteData.Order_Input_Information[0].plat : PriceQuoteData.Order_Input_Information[0].dlat;
                let lng = values.lng;//(Sub_Trip_Type == 1 || Sub_Trip_Type == 2) ? PriceQuoteData.Order_Input_Information[0].plng : PriceQuoteData.Order_Input_Information[0].dlng;
                let Point = [lng, lat];
                let query = {
                    Service_Type: Service_Type,
                    Geometry: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: Point
                            }
                        }
                    },
                    Status: true
                };
                let Result = await ZONES.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


UserController.Apply_Discount_Offer_Validate_Discount_Code = (values, UserData, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ValidateReferral = (DiscountData, UserData) => {
                    return new Promise(async (resolve, reject) => {
                        try {
                            if (DiscountData.Whether_Referral_Discount) {
                                if (UserData.Whether_Referral) {
                                    resolve("Validated Successfully");
                                } else {
                                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_NOT_APPLICABLE_DUE_TO_NON_REFERRAL_ACCOUNT } })
                                }
                            } else {
                                resolve("Validated Successfully");
                            }
                        } catch (error) {
                            reject(await CommonController.Common_Error_Handler(error));
                        }
                    });
                }
                // let ValidateAge = (DiscountData, UserData) => {
                //     return new Promise((resolve, reject) => {
                //         setImmediate(async () => {
                //             try {
                //                 if (DiscountData.Whether_Discount_Age_Filter) {
                //                     let now = moment();
                //                     if (UserData.DOB == null) {
                //                         reject({ success: false, extras: { msg: ApiMessages.PLEASE_UPDATE_DATE_OF_BIRTH_FOR_APPLYING_DISCOUNT } })
                //                     } else {
                //                         let DOB = moment(UserData.DOB);
                //                         let age = Math.abs(now.diff(DOB, 'years'));
                //                         if (age >= DiscountData.Minimum_Age && age <= DiscountData.Maximum_Age) {
                //                             resolve("Validated Successfully")
                //                         } else {
                //                             reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_NOT_APPLICABLE_DUE_TO_YOUR_AGE } })
                //                         }
                //                     }
                //                 } else {
                //                     resolve("Validated Successfully");
                //                 }
                //             } catch (error) {
                //                 reject(await CommonController.Common_Error_Handler(error));
                //             }
                //         });
                //     });
                // }
                let Validate_Discount_Count = (DiscountData, UserData) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(async () => {
                            try {
                                let query = {
                                    BuyerID: UserData.BuyerID,
                                    DiscountID: DiscountData.DiscountID,
                                    Whether_Order_Placed: true
                                };
                                let Count = await User_Discount_Apply.countDocuments(query).lean().exec();
                                if (Count < DiscountData.Max_Discount_For_User) {
                                    resolve("Validated Successfully");
                                } else {
                                    reject({ success: false, extras: { msg: ApiMessages.YOU_HAVE_USED_MAXIMUM_AVAILABLE_DISCOUNT } })
                                }
                            } catch (error) {
                                reject(await CommonController.Common_Error_Handler(error));
                            }
                        });
                    });
                }
                let Validate_Additional_Parameters = (DiscountData, UserData) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(async () => {
                            try {
                                let query = {
                                    BuyerID: UserData.BuyerID,
                                    Order_Status: {
                                        $in: [1, 2, 3, 4, 6, 7, 8, 9, 10]
                                    }
                                };
                                let User_Orders_Count = await Buyer_Orders.countDocuments(query).lean().exec();
                                if (DiscountData.Whether_Customer_Target_Discount) {
                                    if (DiscountData.Customer_Target_Type === 1) {
                                        //New Customer
                                        if (User_Orders_Count <= 0) {
                                            resolve("Validated Successfully");
                                        } else {
                                            reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_APPLICABLE_ONLY_ON_FIRST_TIME_ORDER } })
                                        }
                                    } else if (DiscountData.Customer_Target_Type === 2) {
                                        //More than one order
                                        if (User_Orders_Count > 0) {
                                            resolve("Validated Successfully");
                                        } else {
                                            reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_NOT_APPLICABLE_ON_FIRST_TIME_ORDER } })
                                        }
                                    } else if (DiscountData.Customer_Target_Type === 3) {
                                        //All Customers
                                        resolve("Validated Successfully");
                                    } else if (DiscountData.Customer_Target_Type === 4) {
                                        //Selected Customer
                                        let WhetherAvailable = await DiscountData.Targeted_Customers.find(ele => ele.BuyerID === UserData.BuyerID);
                                        if (WhetherAvailable == null || WhetherAvailable == undefined) {
                                            reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_APPLICABLE_ONLY_TO_PROMOTION_CUSTOMERS } })
                                        } else {
                                            resolve("Validated Successfully");
                                        }
                                    }
                                } else {
                                    resolve("Validated Successfully");
                                }
                            } catch (error) {
                                reject(await CommonController.Common_Error_Handler(error));
                            }
                        });
                    });
                }
                let Validate_Order_Filter = (DiscountData, UserData) => {
                    return new Promise(async (resolve, reject) => {
                        try {
                            if (DiscountData.Whether_Order_Filter) {
                                let query = {
                                    BuyerID: UserData.BuyerID,
                                    Order_Status: {
                                        $in: [1, 2, 3, 4, 6, 7, 8, 9, 10]
                                    }
                                };
                                let User_Orders_Count = await Buyer_Orders.countDocuments(query).lean().exec();
                                let Minimum_Orders = DiscountData.Minimum_Orders;
                                let Maximum_Orders = DiscountData.Maximum_Orders;
                                if (User_Orders_Count >= Minimum_Orders && User_Orders_Count <= Maximum_Orders) {
                                    resolve("Validated Successfully");
                                } else {
                                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_APPLICABLE_ONLY_TO_PROMOTION_CUSTOMERS } })
                                }
                            } else {
                                resolve("Validated Successfully");
                            }
                        } catch (error) {
                            reject(await CommonController.Common_Error_Handler(error));
                        }
                    });
                }
                let Validate_Inactive_Days_Filter = (DiscountData, UserData) => {
                    return new Promise(async (resolve, reject) => {
                        try {
                            if (DiscountData.Whether_Inactive_Days_Filter) {
                                let query = {
                                    BuyerID: UserData.BuyerID,
                                    Order_Status: {
                                        $in: [1, 2, 3, 4]
                                    }
                                };
                                let Last_Ordered_Data = await Buyer_Orders.findOne(query).sort({ created_at: -1 }).lean().exec();
                                if (Last_Ordered_Data === null) {
                                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_APPLICABLE_ONLY_TO_PROMOTION_CUSTOMERS } })
                                } else {
                                    let Main_Order_Placed_Time = moment(Last_Ordered_Data.created_at);
                                    let now = moment();
                                    let Inactive_Days = Math.abs(now.diff(Main_Order_Placed_Time, 'days'));
                                    let Minimum_Days = DiscountData.Minimum_Days;
                                    let Maximum_Days = DiscountData.Maximum_Days;
                                    if (Inactive_Days >= Minimum_Days && Inactive_Days <= Maximum_Days) {
                                        resolve("Validated Successfully");
                                    } else {
                                        reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_APPLICABLE_ONLY_TO_PROMOTION_CUSTOMERS } })
                                    }
                                }
                            } else {
                                resolve("Validated Successfully");
                            }
                        } catch (error) {
                            reject(await CommonController.Common_Error_Handler(error));
                        }
                    });
                }
                let now = moment();
                let query = {
                    Discount_Code: values.Discount_Code,
                    Status: true,
                    CountryID: ZoneData.CountryID,
                    CityID: ZoneData.CityID,
                };
                let Result = await Discounts.findOne(query).sort({ created_at: -1 }).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DISCOUNT_CODE } })
                } else {
                    let Start_Date = moment(Result.Start_Date);
                    let End_Date = moment(Result.End_Date);
                    if (now.isSameOrAfter(Start_Date) && now.isSameOrBefore(End_Date)) {
                        let ValidityStatus = await ValidateReferral(Result, UserData);
                        ValidityStatus = await Validate_Additional_Parameters(Result, UserData);
                        //ValidityStatus = await ValidateAge(Result, UserData);
                        ValidityStatus = await Validate_Discount_Count(Result, UserData);
                        ValidityStatus = await Validate_Order_Filter(Result, UserData);
                        ValidityStatus = await Validate_Inactive_Days_Filter(Result, UserData);
                        resolve(Result);
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_NOT_APPLICABLE } })
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Apply_Discount_Offer = (values, UserData, DiscountData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Service_Type = 1;//1.Instant 2.Schedule 3.Vendor
                let Order_Type = 2; //1.Cash  2.Online 
                //let Collection_Type = parseInt(values.Collection_Type); //1. Collect cash Pickup 2. Collect cash drop
                //let Sub_Trip_Type = PriceQuoteData.Sub_Trip_Type; //1.SP-SD 2.SP-MD 3.MP-SD
                // let Total_Final_Driver_Delivery_Price = 0;
                // let Total_Final_Delivery_Price = 0;
                // await Price_Data_Array.forEach(async element => {
                //     Total_Final_Driver_Delivery_Price += element.Final_Driver_Delivery_Price;
                //     Total_Final_Delivery_Price += element.Final_Delivery_Price;
                // });
                // let Schedule_Date = null;
                // if (Service_Type == 2) {
                //     Schedule_Date = moment(values.Date, config.Take_Date_Format);
                // }
                let Data = {
                    BuyerDiscountID: uuid.v4(),
                    BuyerID: values.BuyerID,
                    DiscountID: DiscountData.DiscountID,
                    // PriceQuoteID: PriceQuoteData.PriceQuoteID,
                    // Sub_Trip_Type: Sub_Trip_Type,
                    Service_Type: Service_Type,
                    Order_Type: Order_Type,
                    // Collection_Type: Collection_Type,
                    CountryID: DiscountData.CountryID,
                    CityID: DiscountData.CityID,
                    Discount_Version: DiscountData.Version,
                    Discount_Code: DiscountData.Discount_Code,
                    Discount_Offer_Percentage: DiscountData.Discount_Percentage,
                    // Schedule_Date: Schedule_Date,
                    // Discount_Pricing_Information: Price_Data_Array,
                    // Total_Final_Driver_Delivery_Price: Total_Final_Driver_Delivery_Price,
                    // Total_Final_Delivery_Price: Total_Final_Delivery_Price,
                    Whether_Offer_Applied: true,
                    Request_Time: new Date(),
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let OfferAppliedData = await User_Discount_Apply(Data).save();
                OfferAppliedData = await JSON.parse(JSON.stringify(OfferAppliedData));
                resolve({ success: true, extras: { Whether_Offer_Applied: true, Status: "Offer Applied Successfully", UserDiscountID: OfferAppliedData.UserDiscountID, Total_Final_Delivery_Price: OfferAppliedData.Total_Final_Delivery_Price, Price_Data_Array: OfferAppliedData.Discount_Pricing_Information } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.Add_Product_To_Wishlist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    ProductID: values.ProductID
                }
                let Result = await Wishlist_Log.findOne(query).lean().exec();
                if (Result == null) {
                    let Data = {
                        WishlistID: uuid.v4(),
                        ProductID: values.ProductID,
                        BuyerID: values.BuyerID,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let SaveData = await Wishlist_Log(Data).save();
                } else {
                    let changes = {
                        $set: {
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Wishlist_Log.updateOne(query, changes).lean().exec();
                }
                resolve({ success: true, extras: { Status: "Wishlist Added Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.List_Product_In_Wishlist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    Status: true
                };
                let sortOptions = {
                    created_at: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Wishlist_Log.countDocuments(query).lean().exec();
                let Result = await Wishlist_Log.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let PQuery = {
                            ProductID: item.ProductID
                        }
                        let RData = await Store_Products.findOne(PQuery).lean().exec()
                        RData.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, RData.Product_Image_Data);
                        Data.push(RData)
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


UserController.Remove_Product_In_Wishlist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    ProductID: values.ProductID
                };
                let changes = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                }
                let SaveData = await Wishlist_Log.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Wishlist Product Removed Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.Search_All_Products_For_Buyer = (values, Barray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            let toSkip = Math.abs(parseInt(values.skip));
            let toLimit = Math.abs(parseInt(values.limit));
            let search = {
                $regex: String(values.Search),
                $options: "i"
            }
            let query = {
                $and: [{
                    'Product_Data_For_Branch.BranchID': {
                        '$in': Barray
                    },
                    'Product_Data_For_Branch.Approve': 1,
                    $or: [{
                        Product_Name: search
                    }],
                }],
                Status: true
            };
            let sortOptions = {
                created_at: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            Store_Products.countDocuments(query).then((Count) => {
                //console.log(Count)
                if (Count >= 0) {
                    let ProductData = [];
                    Store_Products.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).then((Result) => {
                        async.eachSeries(Result, async (item, callback) => {
                            try {
                                //check for Products with filter

                                let PDatax
                                let pd = []
                                async.eachSeries(Barray, async (singleBranch, callbackx) => {

                                    PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                                        if (ResultTemp.BranchID == singleBranch && ResultTemp.Approve == 1) {
                                            pd.push(ResultTemp)
                                            return ResultTemp
                                        }
                                    });
                                    callbackx();
                                })
                                item.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                                item.Product_Data_For_Branch = pd.sort(function (a, b) { return a.Final_Price - b.Final_Price })
                                item.Product_Data_For_Branch = pd.sort(function (a, b) { return b.Avaiable_Quantity - a.Avaiable_Quantity })
                                ProductData.push(item)
                                callback();
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            resolve({ success: true, extras: { Count: Count, Data: ProductData } });
                        });
                    });
                } else {
                    console.log("sdadasddsa===--->")
                }
            });
        });
    });
}

UserController.Remove_Stack = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    StackID: values.StackID,
                    Status: true
                };
                let changes = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                };
                let UpdateDtack = await Stack_Log.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.Request_Stack_ETA = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    StackID: values.StackID,
                    Status: true
                };
                let changes = {
                    $set: {
                        Whether_ETA_Requested: true,
                        updated_at: new Date()
                    }
                };
                let UpdateDtack = await Stack_Log.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
                let SendNotification = await NotificationController.SendNotification_Request_Stack_ETA(values, BuyerData)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.List_User_Stack_Items = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    Status: true
                };
                let sortOptions = {
                    created_at: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Stack_Log.countDocuments(query).lean().exec();
                let Result = await Stack_Log.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                // let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let PQuery = {
                            ProductID: item.ProductID
                        }
                        let RData = await Store_Products.findOne(PQuery).lean().exec()
                        RData.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, RData.Product_Image_Data);
                        item.Product_Data = RData
                        // Data.push(RData);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.Create_or_Generate_Buyer_Session = (values, FirebaseUserData, FirebaseUserTokenData, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Name = (FirebaseUserData.displayName == null || FirebaseUserData.displayName == '') ? '' : FirebaseUserData.displayName;
                let EmailID = (FirebaseUserData.email == null || FirebaseUserData.email == '') ? '' : FirebaseUserData.email;
                let PhoneNumber = (FirebaseUserData.phoneNumber == null || FirebaseUserData.phoneNumber == '') ? '' : FirebaseUserData.phoneNumber;
                let query = {
                    BuyerID: values.BuyerID
                };
                let Result = await Buyers.findOne(query).lean();
                let deviceID = '';
                let session = '';
                if (DeviceData != '') {
                    deviceID = DeviceData.DeviceID;
                    session = uuid.v4()
                } else {
                    if (Result == null) {
                        session = uuid.v4()
                    } else {
                        session = Result.SessionID
                        deviceID = Result.DeviceID
                    }
                }

                let deviceQuery = {
                    DeviceID: deviceID
                }
                let deviceChanges = {
                    $set: {
                        DeviceID: '',
                    }
                }
                let DeviceArray = await Buyers.updateMany(deviceQuery, deviceChanges).lean()
                if (Result == null) {
                    let Data = {
                        BuyerID: values.BuyerID,
                        SessionID: session,
                        DeviceID: deviceID,
                        Buyer_Name: Name,
                        Buyer_PhoneNumber: PhoneNumber,
                        Buyer_Email: EmailID,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let SaveResult = await Buyers(Data).save();
                    if (DeviceData != '') {
                        resolve({ success: true, extras: { Data: JSON.parse(JSON.stringify(SaveResult)) } });
                    } else {
                        resolve(JSON.parse(JSON.stringify(SaveResult)));
                    }
                } else {
                    let changes = {
                        $set: {
                            SessionID: session,
                            DeviceID: deviceID,
                            updated_at: new Date()
                        }
                    }
                    let updateDate = await Buyers.findOneAndUpdate(query, changes).lean();
                    let Resultx = await Buyers.findOne(query).lean();
                    if (DeviceData != '') {
                        resolve({ success: true, extras: { Data: JSON.parse(JSON.stringify(Resultx)) } });
                    } else {
                        resolve(JSON.parse(JSON.stringify(Resultx)));
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

UserController.Add_Write_And_Earn = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                    Unique_ProductID: values.Unique_ProductID
                };
                let Result = await Write_And_Earn_Log.findOne(query).lean().exec()
                if (Result == null) {
                    let ProductData = await Store_Products.findOne({ Unique_ProductID: values.Unique_ProductID }).lean().exec()
                    if (ProductData == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } })
                    } else {
                        let Data = {
                            LogID: uuid.v4(),
                            BuyerID: values.BuyerID,
                            Buyer_Name: BuyerData.Buyer_Name,
                            Buyer_PhoneNumber: BuyerData.Buyer_PhoneNumber,
                            ProductID: ProductData.ProductID,
                            Unique_ProductID: ProductData.Unique_ProductID,
                            Product_Name: ProductData.Product_Name,
                            BranchID: ProductData.Product_Data_For_Branch[0].BranchID,
                            StoreID: ProductData.Product_Data_For_Branch[0].StoreID,
                            Branch_Name: ProductData.Product_Data_For_Branch[0].Branch_Name,
                            Description: values.Description,
                            created_at: new Date(),
                            updated_at: new Date()
                        }
                        let SaveData = await Write_And_Earn_Log(Data).save();
                        resolve({ success: true, extras: { Status: "Updated Successfully" } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.ALLOWED_ONLY_ONCE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

UserController.List_All_Buyer_Notifications = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID,
                };
                let sortOptions = {
                    created_at: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Buyer_Notification_Log.countDocuments(query).lean().exec();
                let Unviewd_Count = await Buyer_Notification_Log.countDocuments({
                    BuyerID: values.BuyerID,
                    Viewed: false
                }).lean().exec();
                let Result = await Buyer_Notification_Log.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ sucess: true, extras: { Count: Count, Unviewd_Count: Unviewd_Count, Data: Result } })
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let NQuery = {
                            NotificationID: item.NotificationID,
                            BuyerID: values.BuyerID,
                        }
                        let Nchanges = {
                            $set: {
                                Viewed: true
                            }
                        }
                        let updateData = await Buyer_Notification_Log.updateOne(NQuery, Nchanges).lean().exec()
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

export default UserController;