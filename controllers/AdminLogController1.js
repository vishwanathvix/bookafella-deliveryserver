let AdminLogController1 = function () { };
import Admin_logs1 from "../models/Admin_logs1";
import uuid from "uuid";
import CommonController from "./CommonController";

AdminLogController1.Edit_Link_Driver_Contract_Salary_Log = (AdminData, DriverData, DriverSalaryData, DriverLeasedOwnerData, OldDriverContractSalaryLinkedData, NewDriverContractSalaryLinkedData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 18; //Driver Contract Salary
                AdminData.LogSubType = 2;
                let Data = new Object();
                Data.DriverData = DriverData;
                Data.DriverSalaryData = DriverSalaryData;
                Data.DriverLeasedOwnerData = DriverLeasedOwnerData;
                Data.OldDriverContractSalaryLinkedData = OldDriverContractSalaryLinkedData;
                Data.NewDriverContractSalaryLinkedData = NewDriverContractSalaryLinkedData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Create_Link_Driver_Contract_Salary_Log = (AdminData, DriverData, DriverSalaryData, DriverLeasedOwnerData, DriverContractSalaryLinkedData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 18; //Driver Contract Salary
                AdminData.LogSubType = 1;
                let Data = new Object();
                Data.DriverData = DriverData;
                Data.DriverSalaryData = DriverSalaryData;
                Data.DriverLeasedOwnerData = DriverLeasedOwnerData;
                Data.DriverContractSalaryLinkedData = DriverContractSalaryLinkedData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Activate_Driver_Leased_Owner_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 8;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Inactivate_Driver_Leased_Owner_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 7;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Edit_Driver_Leased_Owner_Total_Vehicle_and_Drivers_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 6;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Edit_Driver_Leased_Owner_Driver_Vehicle_Price_Per_Day_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 5;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Edit_Driver_Leased_Owner_GST_Number_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 4;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Leased_Owner_Phone_Number_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 3;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Leased_Owner_Name_Log = (AdminData, OldDriverLeasedOwnerData, NewDriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 2;
                let Data = new Object();
                Data.OldDriverLeasedOwnerData = OldDriverLeasedOwnerData;
                Data.NewDriverLeasedOwnerData = NewDriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Add_Driver_Leased_Owners_Log = (AdminData, DriverLeasedOwnerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 17; //Driver Leased Owner
                AdminData.LogSubType = 1;
                let Data = new Object();
                Data.DriverLeasedOwnerData = DriverLeasedOwnerData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Activate_Driver_Salary_Rule_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 7;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Inactivate_Driver_Salary_Rule_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 6;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Salary_Rule_Update_Rule_KMS_Data_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 5;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Salary_Rule_Update_Rule_Tasks_Data_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 4;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Salary_Rule_Whether_Incentive_Rule_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 3;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Salary_Rule_Leased_Salary_Per_Day_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 8;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Driver_Salary_Rule_Title_Log = (AdminData, NewDriverSalaryRuleData, OldDriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 2;
                let Data = new Object();
                Data.OldDriverSalaryRuleData = OldDriverSalaryRuleData;
                Data.NewDriverSalaryRuleData = NewDriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Add_Driver_Salary_Rule_Log = (AdminData, DriverSalaryRuleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 16; //Driver salary rule Logs
                AdminData.LogSubType = 1;
                let Data = new Object();
                Data.DriverSalaryRuleData = DriverSalaryRuleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Update_Base_Price_Fragile_Information_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 9; //Update Base Price --Fragile Information
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Activate_GST_Price_Log = (AdminData, Old_GST_DATA, New_GST_DATA) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 11; //GST Logs
                AdminData.LogSubType = 4; //Activate GST Price
                let Data = new Object();
                Data.Old_GST_DATA = Old_GST_DATA;
                Data.New_GST_DATA = New_GST_DATA;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Inactive_GST_Price_Log = (AdminData, Old_GST_DATA, New_GST_DATA) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 11; //GST Logs
                AdminData.LogSubType = 3; //Inactive GST Price
                let Data = new Object();
                Data.Old_GST_DATA = Old_GST_DATA;
                Data.New_GST_DATA = New_GST_DATA;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_GST_Price_Information_Log = (AdminData, Old_GST_DATA, New_GST_DATA) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 11; //GST Logs
                AdminData.LogSubType = 2; //Edit GST Price
                let Data = new Object();
                Data.Old_GST_DATA = Old_GST_DATA;
                Data.New_GST_DATA = New_GST_DATA;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Activate_Convenience_Fees_Log = (AdminData, OldConvenienceFeeData, NewConvenienceFeeData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 10; //ConvenienceFees Logs
                AdminData.LogSubType = 5; //Active Convenience Fees
                let Data = new Object();
                Data.OldConvenienceFeeData = OldConvenienceFeeData;
                Data.NewConvenienceFeeData = NewConvenienceFeeData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Inactivate_Convenience_Fees_Log = (AdminData, OldConvenienceFeeData, NewConvenienceFeeData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 10; //ConvenienceFees Logs
                AdminData.LogSubType = 4; //Inactive Convenience Fees
                let Data = new Object();
                Data.OldConvenienceFeeData = OldConvenienceFeeData;
                Data.NewConvenienceFeeData = NewConvenienceFeeData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Convenience_Fees_Zone_Information_Log = (AdminData, OldConvenienceFeeData, NewConvenienceFeeData, OldZoneData, NewZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 10; //ConvenienceFees Logs
                AdminData.LogSubType = 3; //Edit Convenience Fees - Zone Information
                let Data = new Object();
                Data.OldConvenienceFeeData = OldConvenienceFeeData;
                Data.NewConvenienceFeeData = NewConvenienceFeeData;
                Data.OldZoneData = OldZoneData;
                Data.NewZoneData = NewZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Convenience_Fees_Price_Information_Log = (AdminData, OldConvenienceFeeData, NewConvenienceFeeData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 10; //ConvenienceFees Logs
                AdminData.LogSubType = 2; //Edit Convenience Fees - Price Information
                let Data = new Object();
                Data.OldConvenienceFeeData = OldConvenienceFeeData;
                Data.NewConvenienceFeeData = NewConvenienceFeeData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Activate_Base_Price_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 8; //Active Base Price
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Inactivate_Base_Price_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 7; //Inactive Base Price
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Update_Base_Price_Zone_Information_Log = (AdminData, OldBasePriceData, NewBasePriceData, OldZoneData, NewZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 6; //Update Base Price --Zone Edit
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                Data.OldZoneData = OldZoneData;
                Data.NewZoneData = NewZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Update_Base_Price_Package_Information_Log = (AdminData, OldBasePriceData, NewBasePriceData, OldPackageData, NewPackageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 5; //Update Base Price --Package Edit
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                Data.OldPackageData = OldPackageData;
                Data.NewPackageData = NewPackageData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Update_Base_Price_Order_Type_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 4; //Update Base Price --Order Trip Type
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Update_Base_Price_Sub_Trip_Type_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 3; //Update Base Price --Sub Trip Type
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Update_Base_Price_Pricing_Information_Log = (AdminData, OldBasePriceData, NewBasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 2; //Update Base Price --Pricing Information
                let Data = new Object();
                Data.OldBasePriceData = OldBasePriceData;
                Data.NewBasePriceData = NewBasePriceData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};
AdminLogController1.Activate_Driver_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 9;
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Inactivate_Driver_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 8;
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Driver_Image_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 7; //Driver Image Edit
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Update_App_Pool_Driver_Setting_Day_Log = (AdminData, OldDriverPoolData, NewDriverPoolData, CountryData, CityData, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 15; //Driver Pool Setting Logs
                AdminData.LogSubType = 2; //Updated Pool Setting
                let Data = new Object();
                Data.OldDriverPoolData = OldDriverPoolData;
                Data.NewDriverPoolData = NewDriverPoolData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.ZoneData = ZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_App_Pool_Driver_Setting_Day_Log = (AdminData, DriverPoolData, CountryData, CityData, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 15; //Driver Pool Setting Logs
                AdminData.LogSubType = 1; //Created Pool Setting
                let Data = new Object();
                Data.DriverPoolData = DriverPoolData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.ZoneData = ZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Update_App_Automated_Offer_Log = (AdminData, OldOfferData, NewOfferData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 14; //Automated Offer Logs
                AdminData.LogSubType = 2; //Update Offer
                let Data = new Object();
                Data.OldOfferData = OldOfferData;
                Data.NewOfferData = NewOfferData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_App_Automated_Offer_Log = (AdminData, OfferData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 14; //Automated Offer Logs
                AdminData.LogSubType = 1; //Create Offer
                let Data = new Object();
                Data.OfferData = OfferData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
AdminLogController1.Activate_Discount_Log = (AdminData, OldDiscountData, NewDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 13; //Discount Logs
                AdminData.LogSubType = 4; //Activate Discount
                let Data = new Object();
                Data.OldDiscountData = OldDiscountData;
                Data.NewDiscountData = NewDiscountData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
AdminLogController1.Inactivate_Discount_Log = (AdminData, OldDiscountData, NewDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 13; //Discount Logs
                AdminData.LogSubType = 3; //Inactivate Discount
                let Data = new Object();
                Data.OldDiscountData = OldDiscountData;
                Data.NewDiscountData = NewDiscountData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
AdminLogController1.Update_Discount_Log = (AdminData, OldDiscountData, NewDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 13; //Discount Logs
                AdminData.LogSubType = 2; //Update Discount
                let Data = new Object();
                Data.OldDiscountData = OldDiscountData;
                Data.NewDiscountData = NewDiscountData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Create_Discount_Log = (AdminData, DiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 13; //Discount Logs
                AdminData.LogSubType = 1; //Create Discount
                let Data = new Object();
                Data.DiscountData = DiscountData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_GST_Log = (AdminData, CountryData, CityData, GSTDATA) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 11; //GST Logs
                AdminData.LogSubType = 1; //Add GST
                let Data = new Object();
                Data.GSTDATA = GSTDATA;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminLogController1.Add_Convenience_Fees_Log = (AdminData, CountryData, CityData, ZoneData, ConvenienceFeeData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 10; //ConvenienceFees Logs
                AdminData.LogSubType = 1; //Add Convenience Fees
                let Data = new Object();
                Data.ConvenienceFeeData = ConvenienceFeeData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.ZoneData = ZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_Base_Price_Log = (AdminData, CountryData, CityData, ZoneData, PackageData, BasePriceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 9; //BasePrices Logs
                AdminData.LogSubType = 1; //Add BasePrices
                let Data = new Object();
                Data.BasePriceData = BasePriceData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.ZoneData = ZoneData;
                Data.PackageData = PackageData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Zone_Log = (AdminData, OldZoneData, NewZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 8; //Zone Logs
                AdminData.LogSubType = 1; //Edit Log
                let Data = new Object();
                Data.OldZoneData = OldZoneData;
                Data.NewZoneData = NewZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Trip_Allocate_New_Driver_Log = (AdminData, TripData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 7; //Trips Logs
                AdminData.LogSubType = 1; //Allocate New Trip Driver 
                let Data = new Object();
                Data.TripData = TripData;
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Order_Drop_Person_Details_Log = (AdminData, OrderData, Old_Details, New_Details) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 6; //Orders Logs
                AdminData.LogSubType = 2; //Edit Drop Details
                let Data = new Object();
                Data.OrderData = OrderData;
                Data.Old_Details = Old_Details;
                Data.New_Details = New_Details;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminLogController1.Edit_Order_Pickup_Person_Details_Log = (AdminData, OrderData, Old_Details, New_Details) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 6; //Orders Logs
                AdminData.LogSubType = 1; //Edit Pick Details
                let Data = new Object();
                Data.OrderData = OrderData;
                Data.Old_Details = Old_Details;
                Data.New_Details = New_Details;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminLogController1.Make_Vendor_Inactive_Log = (AdminData, UserData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 5; //User Logs
                AdminData.LogSubType = 2; //Vendor Inactivate
                let Data = new Object();
                Data = UserData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Admin_New_Vendor_Log = (AdminData, UserData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 5; //User Logs
                AdminData.LogSubType = 1; //Vendor Added
                let Data = new Object();
                Data = UserData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Driver_Zone_Log = (AdminData, DriverData, CountryData, CityData, OldZoneData, NewZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 6; //Driver Zone or Service Type Edit
                let Data = new Object();
                Data.DriverData = DriverData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.OldZoneData = OldZoneData;
                Data.NewZoneData = NewZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Driver_Vehicle_Log = (AdminData, DriverData, OldVehicleData, NewVehicleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 5; //Driver Vehicle Edit
                let Data = new Object();
                Data.DriverData = DriverData;
                Data.OldVehicleData = OldVehicleData;
                Data.NewVehicleData = NewVehicleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Driver_Timings_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 4; //Driver Timings Edit
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Driver_Address_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 3; //Driver Address Edit
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
AdminLogController1.Edit_Driver_Profile_Log = (AdminData, OldDriverData, NewDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 2; //Driver Profile Edit
                let Data = new Object();
                Data.OldDriverData = OldDriverData;
                Data.NewDriverData = NewDriverData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Create_Driver_Log = (AdminData, DriverData, CountryData, CityData, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 4; //Driver Logs
                AdminData.LogSubType = 1; //New Driver
                let Data = new Object();
                Data.DriverData = DriverData;
                Data.CountryData = CountryData;
                Data.CityData = CityData;
                Data.ZoneData = ZoneData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Edit_Vehicle_Log = (AdminData, OldVehicleData, NewVehicleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 3; //Vehicle Logs
                AdminData.LogSubType = 2; //Vehicle Edited
                let Data = new Object();
                Data.OldVehicleData = OldVehicleData;
                Data.NewVehicleData = NewVehicleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_Vehicle_Log = (AdminData, VehicleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 3; //Vehicle Logs
                AdminData.LogSubType = 1; //New Vehicle Added
                let Data = VehicleData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminLogController1.Edit_Package_Log = (AdminData, OldPackageData, NewPackageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 2; //Package Logs
                AdminData.LogSubType = 2; //Package Edited
                let Data = new Object();
                Data.OldPackageData = OldPackageData;
                Data.NewPackageData = NewPackageData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Add_Package_Log = (AdminData, PackageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 2; //Package Logs
                AdminData.LogSubType = 1; //New Package Added
                let Data = PackageData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Create_Admin_User_Log = (AdminData, NewAdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 1; //Admin Profile Logs
                AdminData.LogSubType = 3; //Password Update
                let Data = NewAdminData;
                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Update_Password_Log = (AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 1; //Admin Profile Logs
                AdminData.LogSubType = 2; //Password Update
                let Data = AdminData;


                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



AdminLogController1.Login_Log = (AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                AdminData.LogType = 1; //Admin Profile Logs
                AdminData.LogSubType = 1; //Login
                let Data = AdminData;


                resolve("Log Stored Successfully");
                let StoreLog = await AdminLogController1.Store_Log(AdminData, Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminLogController1.Store_Log = (AdminData, Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                delete AdminData._id;
                delete AdminData.__v;
                let StoreData = {};
                StoreData.LogID = uuid.v4();
                StoreData = Object.assign(StoreData, AdminData);
                StoreData.Data = Data;
                StoreData.created_at = new Date();
                StoreData.updated_at = new Date();
                let SaveResult = await Admin_logs1(StoreData).save();
                resolve("Log Stored Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

export default AdminLogController1;