let AdminController = function () { };

//Models
//  // import logger from '../core/logger/logger';;
import ApiMessages from '../models/ApiMessages';
import ApiResponce from '../models/Apiresponce';
import config from '../config/config';
import Admin_User from '../models/Admin_User';
import Admin_Images from '../models/Admin_Images';
import Store_Entity from '../models/Store_Entity';
import Store_Branch from '../models/Store_Branch';
import Customers from '../models/Customers';
import CustomerPasswordTries from '../models/CustomerPasswordTries';
import Store_Admin_Sessions from '../models/Store_Admin_Sessions';
import CommonController from '../controllers/CommonController';
import Store_SECTIONS_PRODUCTS from "../models/Store_SECTIONS_PRODUCTS"
import ZONES from "../models/ZONES";
import City_Zone_Hub_Version from "../models/City_Zone_Hub_Version";
import State from "../models/State";
import City from "../models/City";
import City_Zone_Version from "../models/City_Zone_Version";
import Country from "../models/Country";
import Country_JSON from "../config/countries.json";
import City_JSON from "../config/cities.json";
import MSG91MOD from "../controllers/msg91mod";
//Dependencies
import uuid from "uuid";
import async from "async";
import sync from "sync";
import moment from "moment";
import crypto from "crypto";
import rand from "csprng";
import formidable from "formidable"
import fs from "fs";
import path from "path";
import os from "os";
import Jimp from "jimp";
import AWS from 'aws-sdk';
import { isBoolean, Boolify } from "node-boolify";


AWS.config.update({
    accessKeyId: config.S3AccessKey,
    secretAccessKey: config.S3Secret
});
let S3 = new AWS.S3();

//Knox Client
import knox from 'knox';
import Store_Service_Area from '../models/Store_Service_Area';
import Counters from '../models/Counters';
import MSG91Controller from './MSG91Controller';
import Images from '../models/Images';
import Vehicles from '../models/Vehicles';
import AdminLogController1 from './AdminLogController1';
import Store_Products from '../models/Store_Products';
import Help_Data from '../models/Help_Data';
import Referal_Price from '../models/Referal_Price';
import Buyer_Orders from '../models/Buyer_Orders';
import Buyers from '../models/Buyers';
import Buyer_Share_Logs from '../models/Buyer_Share_Logs';
import Company_Wallet_Tranx from '../models/Company_Wallet_Tranx';
import Company_Wallet_Log from '../models/Company_Wallet_Log';
import Company_Wallet from '../models/Company_Wallet';
import Driver_Salary_Record_Payout_Transactions from '../models/Driver_Salary_Record_Payout_Transactions';
import Driver_All_Salary_Record_Payouts from '../models/Driver_All_Salary_Record_Payouts';
import RazorpayController from './RazorpayController';
import Delivery_Price from '../models/Delivery_Price';
import Banner from '../models/Banner';
import Quote from '../models/Quote';
import Category from '../models/Category';
import Special_Offers from '../models/Special_Offers';
import Discounts from '../models/Discounts';
import Discount_Versions from '../models/Discount_Versions';
import App_Automated_Offers from '../models/App_Automated_Offers';
import App_Automated_Offer_Versions from '../models/App_Automated_Offer_Versions';
import State_Cities from '../models/State_Cities';
import Stack_Log from '../models/Stack_Log';
import Branch_Stack_Log from '../models/Branch_Stack_Log';
import Store_Branch_OTP from '../models/Store_Branch_OTP';
import MessagesController from './MessagesController';
import Store_OTP_Tries from '../models/Store_OTP_Tries';
import Files from '../models/Files';
import Reorder_log from '../models/Reorder_log';
import Write_And_Earn_Log from '../models/Write_And_Earn_Log';
import ZONES_Archive from '../models/ZONES_Archive';
import Store_Product_Reorder from '../models/Store_Product_Reorder';
import Custom_Notification_Log from '../models/Custom_Notification_Log';
import Buyers_Current_Location from '../models/Buyers_Current_Location';
import Devices from '../models/Devices';
import FcmController from './FcmController';
import Title_Management from '../models/Title_Management'
import Title_Management_Products from '../models/Title_Management_Products';
import Installed_Devices from '../models/Installed_Devices';
import Taxes from '../models/Taxes';
import Zone_Fees from '../models/Zone_Fees';
import Buyer_Points_Log from '../models/Buyer_Points_Log';
import Driver_COD_Collected from '../models/Driver_COD_Collected';
import Driver_COD_Collected_Log from '../models/Driver_COD_Collected_Log';
import Drivers from '../models/Drivers';
import Driver_Wallet_Logs from '../models/Driver_Wallet_Logs';
import Store_Branch_Wallet_Logs from '../models/Store_Branch_Wallet_Logs';
import StoreController from './StoreController';
import Trips from '../models/Trips';
import Trips_Orders_Routings from '../models/Trips_Orders_Routings';
import PubnubController from './PubnubController';
import Driver_Devices from '../models/Driver_Devices';
import DriverController from './DriverController';
import Instant_Order_Request from '../models/Instant_Order_Request';
import AdminMediator from '../mediators/AdminMediator';
import NotificationController from './NotificationController';
import Admin_Notification_Log from '../models/Admin_Notification_Log';

let knoxClient = knox.createClient({
    key: config.S3AccessKey,
    secret: config.S3Secret,
    bucket: config.S3Bucket
});

AdminController.List_All_Notifications = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip)
            let toLimit = parseInt(values.limit)
            let sortOptions = {
                created_at: -1
            }
            let query = {

            }
            let queryR = {
                Viewed: true
            };
            let queryU = {
                Viewed: false
            };
            if (values.CountryID != "") {
                query.CountryID = values.CountryID
                queryR.CountryID = values.CountryID
                queryU.CountryID = values.CountryID
            }
            if (values.CityID != "") {
                query.CityID = values.CityID
                queryR.CityID = values.CityID
                queryU.CityID = values.CityID
            }
            if (values.ZoneID != "") {
                query.ZoneID = values.ZoneID
                queryR.ZoneID = values.ZoneID
                queryU.ZoneID = values.ZoneID
            }
            if (values.User_Type == 1) {
                query.BuyerID = {
                    $ne: ""
                }
                queryR.BuyerID = {
                    $ne: ""
                }
                queryU.BuyerID = {
                    $ne: ""
                }
            } else if (values.User_Type == 2) {
                query.BranchID = {
                    $ne: ""
                }
                queryR.BranchID = {
                    $ne: ""
                }
                queryU.BranchID = {
                    $ne: ""
                }
            } else if (values.User_Type == 3) {
                query.DriverID = {
                    $ne: ""
                }
                queryR.DriverID = {
                    $ne: ""
                }
                queryU.DriverID = {
                    $ne: ""
                }
            }
            if (values.BranchID != "") {
                query.BranchID = values.BranchID
                delete query.BuyerID
                delete query.DriverID
                queryR.BranchID = values.BranchID
                delete queryR.BuyerID
                delete queryR.DriverID
                queryU.BranchID = values.BranchID
                delete queryU.BuyerID
                delete queryU.DriverID
            }else if (values.BuyerID != "") {
                query.BuyerID = values.BuyerID
                delete query.BranchID
                delete query.DriverID
                queryR.BuyerID = values.BuyerID
                delete queryR.BranchID
                delete queryR.DriverID
                queryU.BuyerID = values.BuyerID
                delete queryU.BranchID
                delete queryU.DriverID
            }else if (values.DriverID != "") {
                query.DriverID = values.DriverID
                delete query.BranchID
                delete query.BuyerID
                queryR.DriverID = values.DriverID
                delete queryR.BranchID
                delete queryR.BuyerID
                queryU.DriverID = values.DriverID
                delete queryU.BranchID
                delete queryU.BuyerID
            }
            let UnRead_Count = await Admin_Notification_Log.countDocuments(queryU).lean()
            let Read_Count = await Admin_Notification_Log.countDocuments(queryR).lean()
            let Count = await Admin_Notification_Log.countDocuments(query).lean()
            let Result = await Admin_Notification_Log.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            resolve({ success: true, extras: { Read_Count: Read_Count, UnRead_Count: UnRead_Count, Count: Count, Data: Result } })
            for (let notification of Result) {
                let q1 = {
                    NotificationID: notification.NotificationID
                }
                let changes = {
                    $set: {
                        Viewed: true
                    }
                }
                let updateData = await Admin_Notification_Log.updateOne(q1, changes).lean().exec();
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_Buyer_Wallet_Log_Date = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip)
                let toLimit = parseInt(values.limit)
                let sortOptions = {
                    Time: -1
                }
                let query = {
                    BuyerID: values.BuyerID,
                    Time: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Share_Logs.countDocuments(query).lean().exec()
                let Result = await Buyer_Share_Logs.find(query).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec()
                let Final_Data = [];
                for (let itteraton of Result) {
                    let Description = ""
                    let Whether_Credited = false;
                    if (itteraton.Type == 16) {
                        Description = "Amount Credited By Bookafella"
                        Whether_Credited = true
                    } else if (itteraton.Type == 14 || itteraton.Type == 4) {
                        Description = "Amount Credited from Order Refund"
                        Whether_Credited = true
                    } else if (itteraton.Type == 15) {
                        Description = "Amount Credited for Write And Review"
                        Whether_Credited = true
                    } else if (itteraton.Type == 1) {
                        Description = "Amount Credited for Referal login"
                        Whether_Credited = true
                    } else if (itteraton.Type == 3) {
                        Description = "Amount debited from user wallet for completing Order"
                        Whether_Credited = false
                    } else if (itteraton.Type == 7) {
                        Description = "Amount Credited for Referal First Order"
                        Whether_Credited = true
                    }
                    let Data = {
                        Amount: itteraton.Amount,
                        Description: Description,
                        Whether_Credited: Whether_Credited,
                        Time: itteraton.Time,
                        Status: true
                    }
                    Final_Data.push(Data)
                }
                resolve({ success: true, extras: { Count: Count, Data: Final_Data } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Buyer_Points_Log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip)
                let toLimit = parseInt(values.limit)
                let sortOptions = {
                    Time: -1
                }
                let query = {
                    BuyerID: values.BuyerID
                };
                let Count = await Buyer_Points_Log.countDocuments(query).lean().exec()
                let Result = await Buyer_Points_Log.find(query).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec()
                let Final_Data = [];
                for (let itteraton of Result) {
                    let Description = ""
                    if (itteraton.Type == 1) {
                        Description = "Write And Earn"
                    } else {
                        Description = "Points Credited By Bookafella"
                    }
                    let Data = {
                        Points: itteraton.Points,
                        Amount: itteraton.Data.Amount,
                        Convertion: itteraton.Data.Convertion,
                        Description: Description,
                        Time: itteraton.Time,
                        Status: true
                    }
                    Final_Data.push(Data)
                }
                resolve({ success: true, extras: { Count: Count, Data: Final_Data } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Credits_Rewards_To_Multiple_Buyers = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                for (let BuyerID of values.BuyerID_Array) {
                    let Refund_Points = parseFloat(values.Points); // Add this amount to type 4
                    let Refund_Amount = Refund_Points * config.Points_to_Amount / 100;
                    let BuyersQuery = {
                        BuyerID: BuyerID
                    }
                    let BuyerData = await Buyers.findOne(BuyersQuery).lean().exec();
                    let Data = {
                        BuyerData: BuyerData,
                        Amount: Refund_Amount,
                        Points: Refund_Points,
                        BuyerData: BuyerData,
                        Convertion: config.Points_to_Amount
                    }
                    let WData = {
                        LogID: uuid.v4(),
                        BuyerID: BuyerID,
                        Type: 2, // Points Credited By Bookafella 
                        Points: Refund_Points,
                        Data: Data,
                        Time: new Date()
                    }
                    let SData = {
                        LogID: uuid.v4(),
                        BuyerID: BuyerID,
                        Type: 16, // Amount Credited By Bookafella 
                        Amount: Refund_Amount,
                        Data: Data,
                        Time: new Date()
                    }
                    let Resultlog = Buyer_Points_Log(WData).save();
                    let Resultlog1 = Buyer_Share_Logs(SData).save();
                    let BuyerChanges = {
                        $inc: {
                            Available_Points: Refund_Points,
                            Total_Points: Refund_Points,
                            Available_Amount: Refund_Amount,
                            Total_Amount: Refund_Amount
                        },
                    };
                    let fndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let updatBuyerData = await Buyers.findOneAndUpdate(BuyersQuery, BuyerChanges, fndupdoptions).lean().exec();

                    //deduct amount from company wallet 

                    let WDataw = {
                        LogID: uuid.v4(),
                        AdminID: values.AdminID,
                        Type: 3, // amount deduct from Bookafella Admin for Points Convetion to Amount
                        Amount: Refund_Amount,
                        Data: Data,
                        Time: new Date()
                    }
                    let Resultlogw = Company_Wallet_Log(WDataw).save();
                    let Cfndupdquery = {

                    }
                    let Cfndupdchanges = {
                        $inc: {
                            Available_Amount: Refund_Amount * -1,
                            Withdrawn_Amount: Refund_Amount
                        }
                    }
                    let Cfndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                    let SendNotification = await FcmController.SendNotification_Credit_Amount(BuyerData, Refund_Amount, Refund_Points);
                }
                resolve({ success: true, extras: { Status: "Amount Credited Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Buyers_Wallets = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip)
            let toLimit = parseInt(values.limit)
            let sortOptions = {
                created_at: -1
            }
            let query = {
                Status: true
            }
            if (values.CityID != "") {
                query.CityID = values.CityID
            }
            if (values.ZoneID != "") {
                query.ZoneID = values.ZoneID
            }
            let Count = await Buyers_Current_Location.countDocuments(query).lean()
            let BuyerID_Array = await Buyers_Current_Location.find(query).select('-_id BuyerID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let BuyerDatax = []
            for (let BuyerID of BuyerID_Array) {
                let BuyerData = await CommonController.Check_For_Buyer(BuyerID)
                let Data = {
                    BuyerID: BuyerData.BuyerID,
                    Buyer_Name: BuyerData.Buyer_Name,
                    Buyer_Email: BuyerData.Buyer_Email,
                    Join_Date: BuyerData.created_at,
                    Available_Amount: BuyerData.Available_Amount,
                    Withdrawn_Amount: BuyerData.Withdrawn_Amount,
                    Total_Amount: BuyerData.Total_Amount
                }
                BuyerDatax.push(Data)
            }
            resolve({ success: true, extras: { Count: Count, Data: BuyerDatax } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Instant_Order_Send_Driver_Request_Save_Request_Send_Response = (ZoneData, OrderData, ALL_DRIVERS_DATA, DriverID_Array) => {
    return new Promise(async (resolve, reject) => {
        try {
            // if (ALL_DRIVERS_DATA.length > 0) {
            let DistanceCalculation = await CommonController.Routing_Processing_With_Location(OrderData);
            let InstantOrderRequestID = uuid.v4();
            let Instant_Order_Request_Status = 1;//Requested
            let Instant_Order_Request_Status_Logs = {
                ReferenceID: uuid.v4(),
                Instant_Order_Request_Status: Instant_Order_Request_Status,
                time: new Date()
            };
            let Data = {
                InstantOrderRequestID: InstantOrderRequestID,
                BuyerID: OrderData.BuyerID, // BuyerID
                // PriceQuoteID: PriceQuoteData.PriceQuoteID, //
                // Sub_Trip_Type: PriceQuoteData.Sub_Trip_Type,//
                CountryID: ZoneData.CountryID,
                CityID: ZoneData.CityID,
                ZoneID: ZoneData.ZoneID,
                // Customer_Selected_Price: await CommonController.Common_Floating_Beautify_Value(values.Customer_Selected_Price), //
                Service_Type: ZoneData.Service_Type,
                // Whether_Discount_Apply: Boolify(values.Whether_Discount_Apply), //
                // UserDiscountID: (Boolify(values.Whether_Discount_Apply)) ? values.UserDiscountID : '', //
                // Total_Package_Volume_Weight: PriceQuoteData.Total_Package_Volume_Weight, //
                Total_Distance: DistanceCalculation.geo_distance,
                Total_Time: DistanceCalculation.geo_duration_minutes, // modify to Total_Load_Time to Total_Time
                // Total_Unload_Time: DistanceCalculation.Total_Unload_Time, //
                Total_Duration_Minutes: DistanceCalculation.geo_duration_minutes,
                Order_Type: 2,//online
                ALL_Driver_Information: ALL_DRIVERS_DATA,
                Instant_Order_Request_Status: Instant_Order_Request_Status,
                Instant_Order_Request_Status_Logs: Instant_Order_Request_Status_Logs,
                // No_Of_Orders: PriceQuoteData.No_Of_Orders, //
                // Order_Input_Information: PriceQuoteData.Order_Input_Information, //
                Order_Data: await CommonController.Check_for_Order(OrderData),
                Whether_Checked: true,
                Checked_Time: new Date(),
                Whether_Request: true,
                Request_Time: new Date(),
                created_at: new Date(),
                updated_at: new Date()
            };
            let InstantOrderRequestData = await Instant_Order_Request(Data).save();
            InstantOrderRequestData = await JSON.parse(JSON.stringify(InstantOrderRequestData));
            let DataX = {
                Whether_Driver_Available: true,
                InstantOrderRequestID: InstantOrderRequestID,
                Status: "Request Send to All Driver"
            }
            resolve(DataX);
            // let NotificationPubNubfunction = await StoreController.Instant_Order_Request_Send_Driver_Notification_Pubnub_Socket_Information(InstantOrderRequestData, DriverID_Array, ALL_DRIVERS_DATA);
            // } else {
            //     let DataX = {
            //         Whether_Driver_Available: false,
            //         Status: COMMON_SYSTEM_MESSAGES.INSTANT_ORDER_PLACING_DRIVER_NOT_AVAILABLE
            //     }
            //     resolve(DataX);
            //     let storeRequest = await StoreController.Instant_Order_Send_Driver_Request_Drivers_Not_Available_Save_Request(ZoneData, OrderData);
            // }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Find_All_Instant_Available_Driver_Information = (ZoneData, OrderData, MappedDriverIDArray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DriverIDArray = [OrderData.Driver_Information.DriverID];//await StoreController.Find_Instant_Available_DriverID_Array(ZoneData, MappedDriverIDArray);
                let query = {
                    DriverID: {
                        $in: DriverIDArray
                    },
                    CityID: ZoneData.CityID,
                    ZoneID: ZoneData.ZoneID,
                    Whether_Driver_Online: true,
                    Status: true
                };
                let lookupmap = {
                    from: 'Vehicles',
                    localField: 'VehicleID',
                    foreignField: 'VehicleID',
                    as: 'Vehicle_Data'
                };
                let addfieldsmap = {
                    "Vehicle_Type": "$Vehicle_Data.Vehicle_Type",
                    // "Weight_Type": "$Vehicle_Data.Weight_Type",
                    // "Actual_Maximum_Weight": "$Vehicle_Data.Maximum_Weight",
                    // "Maximum_Weight": {
                    //     $divide: [
                    //         {
                    //             $multiply: [
                    //                 "$Vehicle_Data.Maximum_Weight",
                    //                 config.Vehicle_Maximum_Weight_Capacitance
                    //             ]
                    //         },
                    //         100
                    //     ]
                    // },
                    // "Minimum_Weight": {
                    //     $divide: [
                    //         {
                    //             $multiply: [
                    //                 "$Vehicle_Data.Maximum_Weight",
                    //                 config.Vehicle_Minimum_Weight_Capacitance
                    //             ]
                    //         },
                    //         100
                    //     ]
                    // },
                    // "Dimensions": "$Vehicle_Data.Dimensions"
                };
                let Data = await Drivers.aggregate().match(query).lookup(lookupmap).unwind('Vehicle_Data').addFields(addfieldsmap).project({ _id: 0, __v: 0, Vehicle_Data: 0 }).sort({ created_at: -1 }).exec();
                resolve(JSON.parse(JSON.stringify(Data)));
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Instant_Order_Driver_Request = (BranchData, ZoneData, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let MappedDriverIDArray = [];
                let DriverID_Array = [];
                let Driver_Radius_KMS = await StoreController.Instant_Order_Send_Driver_Request_Fetch_Driver_Radius(BranchData.CountryID, BranchData.CityID);
                let ALL_DRIVERS_DATA = await AdminController.Find_All_Instant_Available_Driver_Information(ZoneData, OrderData, MappedDriverIDArray);
                let Latitude = await BranchData.Latitude;
                let Longitude = await BranchData.Longitude;
                ALL_DRIVERS_DATA = await StoreController.Instant_Order_Send_Driver_Request_Driver_Distance_Calculation(ALL_DRIVERS_DATA, Latitude, Longitude);
                ALL_DRIVERS_DATA = await ALL_DRIVERS_DATA.filter(item => item.geo_distance <= Driver_Radius_KMS);
                await ALL_DRIVERS_DATA.forEach((item) => DriverID_Array.push(item.DriverID));
                //online Orders
                let Result = await AdminController.Instant_Order_Send_Driver_Request_Save_Request_Send_Response(ZoneData, OrderData, ALL_DRIVERS_DATA, DriverID_Array);
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Processing_Send_Notification_Pubnub_Messaging = (OrderData, DriverData, CancellRequestData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let PubnubSending = await PubnubController.Cancel_Order_Message(DriverData, CancellRequestData);
            let ddquery = {
                DriverID: DriverData.DriverID,
                DeviceID: {
                    $ne: ""
                }
            };
            let Data1 = await Drivers.findOne(ddquery).select('-_id -__v').lean();
            if (Data1 == null) {
                resolve("Sent Successfully")
            } else {
                let ffquery = {
                    DeviceID: Data1.DeviceID,
                    FCM_Token: {
                        $ne: ""
                    }
                };
                let Data2 = await Driver_Devices.findOne(ffquery).select('-_id -__v').lean();
                if (Data2 == null) {
                    resolve("Sent Successfully");
                } else {
                    let FCM_Token = Data2.FCM_Token;
                    let FCMSending = await FcmController.Cancell_Order_Request_Send_Driver_Notifications(DriverData, CancellRequestData, FCM_Token);
                    resolve("Sent Successfully");
                };
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Cancel_Order_Processing_Driver_Task_Report_Processing = (OrderData, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Route_Status = 6;
            let Route_Status_Comment = "Order Cancelled";
            let Route_Status_Logs = {
                LogID: uuid.v4(),
                Route_Status: Route_Status,
                Route_Status_Comment: Route_Status_Comment,
                Time: new Date()
            };
            let query = {
                DriverID: DriverData.DriverID,
                "Mapping_Information.OrderID": OrderData.OrderID
            };
            let changes = {
                $set: {
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Whether_Cancelled: true,
                    CancellationType: 2,
                    Status: false,
                    updated_at: new Date()
                },
                $push: {
                    Route_Status_Logs: Route_Status_Logs
                }
            };
            let UpdatedStatus = await Trips_Orders_Routings.updateMany(query, changes).lean();
            resolve("Trip cancel functionality completed");
            let TripID_Array = await Trips_Orders_Routings.distinct('TripID', query).lean();
            let All_Orders_ID_Array = await Trips_Orders_Routings.distinct('All_Orders_ID_Array', query).lean();
            let trtquery = {
                TripID: {
                    $in: TripID_Array
                },
                Route_Status: {
                    $in: [1, 2, 3, 4]
                },
                Status: true
            };
            let OtherDropsAvailable = await Trips_Orders_Routings.findOne(trtquery).lean();
            if (OtherDropsAvailable == null) {
                //Update trip completed
                let tquery = {
                    TripID: {
                        $in: TripID_Array
                    },
                };
                let tchanges = {
                    $set: {
                        Total_Trip_Status: 3,
                        Trip_Completion_Time: new Date(),
                        updated_at: new Date()
                    }
                };
                let tUpdatedStatus = await Trips.updateOne(tquery, tchanges).lean();
            } else {
                //Nothing to do
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Common_Order_All_Drivers = (OrderID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let oquery = {
                    OrderID: OrderID
                };
                let All_DriverID_Array = await Buyer_Orders.distinct('Event_Logs.DriverID', oquery).lean();
                let Dquery = {
                    DriverID: {
                        $in: All_DriverID_Array
                    }
                };
                let Result = await Drivers.find(Dquery).select('-_id -__v -updated_at -SessionID').lean();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        item.ImageData = await CommonController.Common_Image_Response_Single_Image(item.ImageAvailable, item.ImageData);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Result);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Processing = (OrderData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let All_Drivers_Data = await AdminController.Common_Order_All_Drivers(OrderData.OrderID);
            async.eachSeries(All_Drivers_Data, async (DriverData, callback) => {
                try {
                    let Cancell_Request_Status = 1;
                    let Cancell_Request_Status_Logs = {
                        ReferenceID: uuid.v4(),
                        Cancell_Request_Status: Cancell_Request_Status,
                        Time: new Date()
                    };
                    let CancellRequestData = JSON.parse(JSON.stringify(Cancell_Request_Status_Logs));
                    let CancellTasks = await AdminController.Cancel_Order_Processing_Driver_Task_Report_Processing(OrderData, DriverData);
                    let SendMessagingProcessing = await AdminController.Cancel_Order_Processing_Send_Notification_Pubnub_Messaging(OrderData, DriverData, CancellRequestData);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve("Processed Successfully");
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Order_Return_From_Driver = (OrderData) => {
    return new Promise(async (resolve, reject) => {
        try {
            //Cancel current Trip
            // console.log(1)
            let Functionality = await AdminController.Cancel_Order_Processing(OrderData);
            //Auto Accept new Trip drive and initate the process for the driver till order drop initiate
            let ZoneData = await StoreController.Fetch_Zone_Data(OrderData.Delivery_Address_Information);
            let AddressData = {
                CountryID: ZoneData.CountryID,
                CityID: ZoneData.CityID,
                Latitude: OrderData.Delivery_Address_Information.Latitude,
                Longitude: OrderData.Delivery_Address_Information.Longitude
            }
            let Result = await AdminController.Instant_Order_Driver_Request(AddressData, ZoneData, OrderData);
            //Step 1 Accept instant Order request
            let InstantOrderRequestID = await Result.InstantOrderRequestID
            let DriverData = await OrderData.Driver_Information;
            let Data = {
                InstantOrderRequestID: InstantOrderRequestID,
                DriverID: DriverData.DriverID
            }
            let InstantRequestData = await CommonController.Check_for_Instant_Order_Request(Result);
            let Result5 = await DriverController.Accept_Instant_Order_Request(Data, InstantRequestData);
            InstantRequestData.newDriverID = await DriverData.DriverID;
            // let SendingPubnubMessage = await PubnubController.Send_Common_Instant_Request_Pubnub_Messaging_for_Common_Channel(InstantRequestData, 1);
            let OrderProcessing = await CommonController.Common_Order_Creation_Order_Placing_Processing(InstantRequestData, DriverData);
            //step 2 Start trip
            Data.TripID = await OrderProcessing.TripID
            let TripData = await CommonController.Check_for_Trip(Data);
            // console.log(455)
            let StartTripData = await DriverController.Start_Driver_Trip(TripData);
            //Step 3 initiate pickup and drop(First side)
            let querytripx1 = {
                DriverID: Data.DriverID,
                Status: true,
                TripID: OrderProcessing.TripID,
                Route_Status: 1
            };
            let TripRouteData = await Trips_Orders_Routings.findOne(querytripx1).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').lean().exec();
            Data.TripRouteID = await TripRouteData.TripRouteID

            TripData = await CommonController.Check_for_Trip(Data);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            TripRouteData = await CommonController.Check_for_Trip_Route(Data);
            let Result1 = await DriverController.Trip_Pickup_Drop_Initiated(Data, TripData, TripRouteData, DriverData);
            //Step 4 reach pickup and drop(First side)
            TripData = await CommonController.Check_for_Trip(Data);
            ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            TripRouteData = await CommonController.Check_for_Trip_Route(Data);
            let Result2 = await DriverController.Trip_Pickup_Drop_Reached(Data, TripRouteData, DriverData);
            //Step 5 receive Order items
            TripData = await CommonController.Check_for_Trip(Data);
            OrderData = await CommonController.Check_for_Order(TripData);
            ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            TripRouteData = await CommonController.Check_for_Trip_Route(Data);
            let Result3 = await DriverController.Complete_Pickup_Type3(DriverData, TripData, TripRouteData, OrderData);
            //step 6 initiate pickup and drop(second side)
            let querytripx2 = {
                DriverID: Data.DriverID,
                Status: true,
                TripID: OrderProcessing.TripID,
                Route_Action_Next: 2
            };
            // console.log(querytripx2)
            TripRouteData = await Trips_Orders_Routings.findOne(querytripx2).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').lean().exec();
            // console.log(TripRouteData)
            Data.TripRouteID = await TripRouteData.TripRouteID

            TripData = await CommonController.Check_for_Trip(Data);
            ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            TripRouteData = await CommonController.Check_for_Trip_Route(Data);
            let Result4 = await DriverController.Trip_Pickup_Drop_Initiated(Data, TripData, TripRouteData, DriverData);
            resolve('Completed')
            // console.log('Completed')
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Order_Return_From_Buyer = (OrderData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let ZoneData = await StoreController.Fetch_Zone_Data(OrderData.Delivery_Address_Information);
            let AddressData = {
                CountryID: ZoneData.CountryID,
                CityID: ZoneData.CityID,
                Latitude: OrderData.Delivery_Address_Information.Latitude,
                Longitude: OrderData.Delivery_Address_Information.Longitude
            }
            let Result = await StoreController.Instant_Order_Send_Driver_Request(AddressData, ZoneData, OrderData);
            // console.log("done")
            resolve("Completed");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Request_Branch_Stack_Reorder = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                StackID: values.StackID
            }
            let change = {
                $set: {
                    Reorder_Quantity: values.Reorder_Quantity,
                    Reorder_Status: 1,
                    Whether_Branch_ETA_Requested: true,
                    updated_at: new Date()
                }
            };
            let UpdateData = await Stack_Log.updateOne(query, change).lean().exec();
            resolve({ success: true, extras: { Status: "Updated Successfully" } })
            let SendNotification = await NotificationController.SendNotification_Request_Branch_Stack_Reorder(values);
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.List_All_Stack_Log = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                Whether_ETA_Requested: true,
                Status: true
            }
            if (Boolify(values.Whether_Date_Filter)) {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                query.created_at = {
                    $gte: Start_Date,
                    $lte: End_Date
                }
            };
            if (Boolify(values.Whether_Incoming)) {
                query.Reorder_Status = {
                    $ne: 3
                }
            } else {
                query.Reorder_Status = 3
            }
            if (values.Search == "") {
                if (values.ProductID != "") {
                    query.ProductID = values.ProductID
                };
                if (values.ZoneID != "") {
                    query.ZoneID = values.ZoneID
                }
            } else {
                let Search = {
                    $regex: String(values.Search),
                    $options: "i"
                };
                query.$or = [
                    {
                        Branch_Name: Search
                    },
                    {
                        Product_Name: Search
                    }
                ]
            }
            let toSkip = parseInt(values.skip)
            let toLimit = parseInt(values.limit)
            let sortOptions = {
                created_at: -1
            }
            let Count = await Stack_Log.countDocuments(query).lean().exec();
            let Result = await Stack_Log.find(query).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            for (let iteration of Result) {
                let BuyerData = await CommonController.Check_For_Buyer(iteration);
                iteration.BuyerInfo = {
                    BuyerID: iteration.BuyerID,
                    Buyer_Name: BuyerData.Buyer_Name,
                    Buyer_Email: BuyerData.Buyer_Email,
                    Buyer_PhoneNumber: BuyerData.Buyer_PhoneNumber
                }
                iteration.CityData = await CommonController.Check_for_City(iteration);
                iteration.ZoneData = await CommonController.Check_for_Zone(iteration);
                iteration.CountryData = await CommonController.Check_for_Country(iteration);
                iteration.ProductData = await CommonController.Check_For_Product(iteration);
                iteration.ProductData.Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, iteration.ProductData.Product_Image_Data);
                delete iteration.CityID
                delete iteration.ZoneID
                delete iteration.CountryID
            }
            resolve({ success: true, extras: { Count: Count, Data: Result } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Collect_COD_Amount_From_Driver = (values, Amount_Collected, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                DriverID: values.DriverID
            }
            let changes = {
                $set: {
                    Whether_Amount_Submited: true,
                    Amount_Collected: 0,
                    Delivery_Amount: 0,
                    Branch_Amount: 0,
                    Admin_Amount: 0,
                    Branch_Data: [],
                    updated: new Date()
                }
            }
            let updateData = await Driver_COD_Collected.updateOne(query, changes).lean().exec();
            Amount_Collected.LogID = uuid.v4()
            delete Amount_Collected._id
            delete Amount_Collected.__v
            let SaveLog = await Driver_COD_Collected_Log(Amount_Collected).save();
            resolve({ success: true, extras: { Status: "COD Amount Collected Successfully" } })
            //Process Amount

            // Amount to Driver
            let Delivery_Amount = parseFloat(Amount_Collected.Delivery_Amount);
            //Amount to Admin/Company
            let Admin_Amount = parseFloat(Amount_Collected.Admin_Amount)
            // Add Driver COD Amount Complete
            let Dquery = {
                DriverID: values.DriverID
            }
            let Dchanges = {
                $inc: {
                    "Driver_Wallet_Information.Available_Amount": Delivery_Amount,
                    "Driver_Wallet_Information.Total_Collected_Amount": Delivery_Amount,
                    "Driver_Wallet_Information.Total_Earned_Amount": Delivery_Amount
                },
                $set: {
                    updated_at: new Date()
                }
            }
            let SaveAmount = await Drivers.updateOne(Dquery, Dchanges).lean().exec();
            let L1Data = {
                LogID: uuid.v4(),
                DriverID: values.DriverID,
                Type: 20, // COD Amount Credited for Completing Order
                Amount: Delivery_Amount,
                Data: {
                    Amount_Collected: Amount_Collected.Amount_Collected,
                    Delivery_Amount: Delivery_Amount,
                    COD_Log_Data: Amount_Collected
                },
                Time: new Date()
            };
            let L1SaveResult = await Driver_Wallet_Logs.create(L1Data);

            ////////////

            ///Add COD Amount to Company(Admin)
            let WData = {
                LogID: uuid.v4(),
                AdminID: "",
                Type: 5, //COD amount Credited for Order 
                Amount: Admin_Amount,
                Data: {
                    Amount_Collected: Amount_Collected.Amount_Collected,
                    COD_Log_Data: Amount_Collected,
                    DriverID: values.DriverID,
                    DriverData: DriverData,
                    Admin_Amount: Admin_Amount,
                },
                Time: new Date()
            }
            let Resultlog = Company_Wallet_Log(WData).save();

            let Aquery = {

            };
            let Achanges = {
                $inc: {
                    Available_Amount: Admin_Amount,
                    Total_Amount: Admin_Amount
                }
            };
            let Cfndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let CfindupdateData = await Company_Wallet.findOneAndUpdate(Aquery, Achanges, Cfndupdoptions).select('-_id -__v').lean();

            ///Add Branch COD Amount 
            for (let iteration of Amount_Collected.Branch_Data) {
                let Amount = parseFloat(iteration.Branch_Amount)
                let Bquery = {
                    BranchID: iteration.BranchID
                }
                let Bchanges = {
                    $inc: {
                        "Branch_Wallet_Information.Available_Amount": Amount,
                        "Branch_Wallet_Information.Total_Amount": Amount
                    },
                    $set: {
                        updated_at: new Date()
                    }
                }
                let SaveAmountB = await Store_Branch.updateOne(Bquery, Bchanges).lean().exec();
                let OrderData = await CommonController.Check_for_Order(iteration)
                let L2Data = {
                    LogID: uuid.v4(),
                    BranchID: iteration.BranchID,
                    Type: 10, // Amount credited to wallet for order
                    Amount: Amount,
                    Data: {
                        OrderData: OrderData,
                        Order_Number: OrderData.Order_Number,
                        OrderID: iteration.OrderID,
                        Amount: Amount,
                    },
                    Time: new Date()
                };
                let L2SaveResult = await Store_Branch_Wallet_Logs.create(L2Data);

            }
            // ////////////

        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Check_COD_Amount_at_Driver1 = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                DriverID: values.DriverID
            }
            let Result = await Driver_COD_Collected.findOne(query).lean().exec();
            if (Result != null) {
                if (Boolify(Result.Whether_Amount_Submited)) {
                    // reject({ success: false, extras: { msg: ApiMessages.COD_AMOUNT_ALREADY_SUBMITED_BY_DRIVER } })
                    resolve(0)
                } else {
                    let Amount_Collected = Result.Amount_Collected
                    resolve(Amount_Collected)
                }
            } else {
                resolve(0)
                // reject({ success: false, extras: { msg: ApiMessages.DRIVER_DOES_NOT_HAVE_ANY_COD_AMOUNT } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Check_COD_Amount_at_Driver = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                DriverID: values.DriverID
            }
            let Result = await Driver_COD_Collected.findOne(query).lean().exec();
            if (Result != null) {
                if (Boolify(Result.Whether_Amount_Submited)) {
                    reject({ success: false, extras: { msg: ApiMessages.COD_AMOUNT_ALREADY_SUBMITED_BY_DRIVER } })
                } else {
                    let Amount_Collected = Result.Amount_Collected
                    resolve([{ success: true, extras: { Data: { Amount_Collected: Amount_Collected } } }, Result])
                }
            } else {
                reject({ success: false, extras: { msg: ApiMessages.DRIVER_DOES_NOT_HAVE_ANY_COD_AMOUNT } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.List_All_Drivers_With_COD_Amount = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip)
            let toLimit = parseInt(values.limit)
            let sortOptions = {
                created_at: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                Whether_Amount_Submited: false
            }
            let DriverIDArray = await Driver_COD_Collected.distinct('DriverID', query).lean();
            let queryDriver = {
                DriverID: {
                    $in: DriverIDArray
                }
            };
            if (values.Search == '') {
                if (values.CountryID != '') {
                    queryDriver.CountryID = values.CountryID;
                }
                if (values.CityID != '') {
                    queryDriver.CityID = values.CityID
                }
                if (values.ZoneID != '') {
                    queryDriver.ZoneID = values.ZoneID
                }

            } else {
                let Search = {
                    $regex: String(values.Search),
                    $options: "i"
                };
                queryDriver.$or = [
                    {
                        Driver_Name: Search
                    },
                    {
                        Driver_Phone_Number: Search
                    },
                    {
                        Driver_EmailID: Search
                    }
                ]
            }
            let Count = await Drivers.countDocuments(queryDriver).lean().exec();
            let ResultDriver = await Drivers.find(queryDriver).lean().exec();
            let Final_Data = []
            for (let iteration of ResultDriver) {
                let Amount_Collected = await AdminController.Check_COD_Amount_at_Driver1({ DriverID: iteration.DriverID });
                let Amount = Amount_Collected
                let Data = {
                    DriverID: iteration.DriverID,
                    Driver_Name: iteration.Driver_Name,
                    Driver_Country_Code: iteration.Driver_Country_Code,
                    Driver_Phone_Number: iteration.Driver_Phone_Number,
                    Driver_EmailID: iteration.Driver_EmailID,
                    CountryData: await CommonController.Check_for_Country(iteration),
                    CityData: await CommonController.Check_for_City(iteration),
                    ZoneData: await CommonController.Check_for_Zone(iteration),
                    ImageAvailable: iteration.ImageAvailable,
                    ImageData: await CommonController.Common_Image_Response_Single_Image(iteration.ImageAvailable, iteration.ImageData),
                    VehicleID: iteration.VehicleID,
                    VehicleNumber: iteration.VehicleNumber,
                    COD_Amount_Collected: Amount
                }
                Final_Data.push(Data);
            }
            resolve({ success: true, extras: { Count: Count, Data: Final_Data } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Fetch_Total_Number_of_Downloads = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Android_Downloads = await Devices.countDocuments({ DeviceType: 1 }).lean().exec();
            let Android_Details = await Devices.find({ DeviceType: 1 }).lean().exec();
            let IOS_Downloads = await Devices.countDocuments({ DeviceType: 2 }).lean().exec();
            let IOS_Details = await Devices.find({ DeviceType: 2 }).lean().exec();
            let Android_Data = [];
            let Android_Version_Array = await Devices.distinct("AppVersion", { DeviceType: 1 }).lean();
            for (const Version of Android_Version_Array) {
                let PDatax = Android_Details.filter(function (ResultTemp) {
                    if (ResultTemp.AppVersion == Version) {
                        return ResultTemp
                    }
                });
                let Data = {
                    Version: Version,
                    Count: PDatax.length
                };
                Android_Data.push(Data)
            }
            let IOS_Data = [];
            let IOS_Version_Array = await Devices.distinct("AppVersion", { DeviceType: 2 }).lean();
            for (const Version of IOS_Version_Array) {
                let PDatax = IOS_Details.filter(function (ResultTemp) {
                    if (ResultTemp.AppVersion == Version) {
                        return ResultTemp
                    }
                });
                let Data = {
                    Version: Version,
                    Count: PDatax.length
                };
                IOS_Data.push(Data)
            }
            let Data = {
                Android_Downloads: Android_Downloads,
                Android_Data: Android_Data,
                IOS_Downloads: IOS_Downloads,
                IOS_Data: IOS_Data,
            }
            resolve({ success: true, extras: { Data: Data } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.ZoneCount_Process = (values, TitleProductData) => {
    return new Promise(async (resolve, reject) => {
        try {
            values.Service_Type = 1;
            if (values.CityID != "") {
                let ZoneDatax = await AdminController.List_All_City_Zones(values)
                let ZoneData = ZoneDatax.extras.Data
                let Result = [];
                async.eachSeries(ZoneData, async (item, callback) => {
                    try {
                        // console.log(item)
                        // console.log(item.Zone_Title)
                        let PDatax = TitleProductData.filter(function (ResultTemp) {
                            if (ResultTemp.CityID == values.CityID && ResultTemp.ZoneID == item.ZoneID) {
                                return ResultTemp
                            }
                        });
                        let Data = {
                            CountryID: item.CountryID,
                            CityID: item.CityID,
                            ZoneID: item.ZoneID,
                            Zone_Title: item.Zone_Title,
                            Count: PDatax.length
                        }
                        Result.push(Data)
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Result);
                });

            } else {
                let ZoneDatax = await AdminController.List_All_Zones(values)
                let ZoneData = ZoneDatax.extras.Data
                let Result = []
                async.eachSeries(ZoneData, async (item, callback) => {
                    try {
                        let PDatax = TitleProductData.filter(function (ResultTemp) {
                            return ResultTemp.ZoneID == item.ZoneID;
                        });
                        let Data = {
                            CountryID: item.CountryID,
                            CityID: item.CityID,
                            ZoneID: item.ZoneID,
                            Zone_Title: item.Zone_Title,
                            Count: PDatax.length
                        }
                        Result.push(Data)
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Result);
                });
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.CityCount_Process = (values, TitleProductData) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (values.CountryID != "") {
                let CityDatax = await AdminController.List_All_Country_Cities(values)
                let CityData = CityDatax.extras.Data
                let Result = [];
                async.eachSeries(CityData, async (item, callback) => {
                    try {
                        let PDatax = TitleProductData.filter(function (ResultTemp) {
                            if (ResultTemp.CityID == item.CityID && ResultTemp.CountryID == values.CountryID) {
                                return ResultTemp
                            }
                        });
                        let Data = {
                            CountryID: item.CountryID,
                            CityID: item.CityID,
                            CityName: item.CityName,
                            Count: PDatax.length
                        }
                        Result.push(Data)
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Result);
                });

            } else {
                let CityDatax = await AdminController.List_All_Cities(values)
                let CityData = CityDatax.extras.Data
                let Result = []
                async.eachSeries(CityData, async (item, callback) => {
                    try {
                        let PDatax = TitleProductData.filter(function (ResultTemp) {
                            return ResultTemp.CityID == item.CityID;
                        });
                        let Data = {
                            CountryID: item.CountryID,
                            CityID: item.CityID,
                            CityName: item.CityName,
                            Count: PDatax.length
                        }
                        Result.push(Data)
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Result);
                });
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.CountryCount_Process = (values, TitleProductData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Filter = {
                Whether_Status_Filter: true,
                Status: true
            }
            let CountryDatax = await AdminController.List_All_Countries_with_Filter(Filter)
            let CountryData = CountryDatax.extras.Data
            let Result = []
            async.eachSeries(CountryData, async (item, callback) => {
                try {
                    let PDatax = TitleProductData.filter(function (ResultTemp) {
                        return ResultTemp.CountryID == item.CountryID;
                    });
                    let Data = {
                        CountryID: item.CountryID,
                        CountryName: item.CountryName,
                        Count: PDatax.length
                    }
                    Result.push(Data)
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve(Result);
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.List_All_Products_Titles_Dynamic_With_Filter = (values, TitleData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let queryStory = {
                Status: true
            }
            let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
            let queryx = {
                'Product_Data_For_Branch.BranchID': {
                    $in: StoreID_Array
                },
                Status: true,
                Whether_Deleted: {
                    $ne: true
                }
            }
            let ProductID_Array = await Store_Products.distinct('ProductID', queryx).lean();

            //
            let queryTitle = {
                ProductID: {
                    $in: ProductID_Array
                },
                TitleID: values.TitleID,
                Status: true
            };
            let ResultCount = await Title_Management_Products.find(queryTitle).lean().exec();
            let CountriesCountData = await AdminController.CountryCount_Process(values, ResultCount);
            let CitiesCountData = await AdminController.CityCount_Process(values, ResultCount);
            let ZonesCountData = await AdminController.ZoneCount_Process(values, ResultCount);

            //
            let query = {
                ProductID: {
                    $in: ProductID_Array
                },
                TitleID: values.TitleID,
                Status: values.Status
            };
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            if (values.CountryID != "") {
                query.CountryID = values.CountryID
            }
            if (values.CityID != "") {
                query.CityID = values.CityID
            }
            if (values.ZoneID != "") {
                query.ZoneID = values.ZoneID
            }
            let Count = await Title_Management_Products.countDocuments(query).lean().exec();
            let Result = await Title_Management_Products.find(query).select('-_id -__v -updated_at -Description').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            resolve({
                success: true, extras: {
                    CountriesCount: CountriesCountData,
                    CitiesCount: CitiesCountData,
                    ZonesCount: ZonesCountData,
                    Data: { Count: Count, Data: Result }
                }
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Activate_Inactivate_Products_Title_Dynamic = (values, Dynamic_Product_Data, TitleData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                TitleID: values.TitleID,
                ProductID: values.ProductID
            }
            let changes = {
                $set: {
                    Status: !Dynamic_Product_Data.Status,
                    updated_at: new Date()
                }
            };
            let UpdateData = await Title_Management_Products.updateOne(query, changes).lean().exec();
            let Status;
            if (Dynamic_Product_Data.Status == true) {
                Status = "Inactivate Successfully"
            } else {
                Status = 'Activate Successfully'
            }
            resolve({ success: true, extras: { Status: Status } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Add_Products_Title_Dynamic = (values, Products_Array_Data, TitleData) => {
    return new Promise(async (resolve, reject) => {
        try {
            async.eachSeries(Products_Array_Data, async (ProductsData, callback) => {
                try {
                    let Query = {
                        TitleID: TitleData.TitleID,
                        ProductID: ProductsData.ProductID
                    };
                    let ResultCheck = await Title_Management_Products.findOne(Query).lean().exec();
                    if (ResultCheck == null) {
                        let Data = {
                            TitleID: TitleData.TitleID,
                            Title: TitleData.Title,
                            Description: TitleData.Description,
                            CountryID: ProductsData.CountryID,
                            CityID: ProductsData.CityID,
                            ZoneID: ProductsData.ZoneID,
                            ProductID: ProductsData.ProductID,
                            BranchID: ProductsData.BranchID,
                            StoreID: ProductsData.StoreID,
                            Branch_Name: ProductsData.Branch_Name,
                            Unique_ProductID: ProductsData.Unique_ProductID,
                            Product_Name: ProductsData.Product_Name,
                            Format_of_the_Book: ProductsData.Format_of_the_Book,
                            created_at: new Date(),
                            updated_at: new Date()
                        }
                        let SaveData = await Title_Management_Products(Data).save();
                    } else {
                        let changes = {
                            $set: {
                                Status: true,
                                updated_at: new Date()
                            }
                        }
                        let UpdatedData = await Title_Management_Products.updateOne(Query, changes).lean().exec();
                    }
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Status: "Products Added Successfully" } });
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Products_Array_Processing = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Products_Data = []
            var uniqueProductIDs = [...new Set(values)]
            async.eachSeries(uniqueProductIDs, async (item, callback) => {
                try {
                    let pQuery = {
                        ProductID: item,
                        Whether_Deleted: {
                            $ne: true
                        },
                        Status: true
                    }
                    let ProductsData = await Store_Products.findOne(pQuery).lean().exec();
                    if (ProductsData == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } })
                    } else {
                        let Data = {
                            CountryID: ProductsData.CountryID,
                            CityID: ProductsData.CityID,
                            ZoneID: ProductsData.ZoneID,
                            ProductID: ProductsData.ProductID,
                            BranchID: ProductsData.Product_Data_For_Branch[0].BranchID,
                            StoreID: ProductsData.Product_Data_For_Branch[0].StoreID,
                            Branch_Name: ProductsData.Product_Data_For_Branch[0].Branch_Name,
                            Unique_ProductID: ProductsData.Unique_ProductID,
                            Product_Name: ProductsData.Product_Name,
                            Format_of_the_Book: ProductsData.Format_of_the_Book
                        }
                        Products_Data.push(Data)
                    }
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve(Products_Data);
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.List_All_Titles_Dynamic = (values) => {
    return new Promise(async (resolve, reject) => {

        try {
            let query = {
                Status: true
            };
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                SNo: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let Count = await Title_Management.countDocuments(query).lean().exec();
            let Result = await Title_Management.find(query).select('-_id -__v -updated_at -Description').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let queryStory = {
                        Status: true,
                    }
                    let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
                    let queryx = {
                        'Product_Data_For_Branch.BranchID': {
                            $in: StoreID_Array
                        },
                        Status: true,
                        Whether_Deleted: {
                            $ne: true
                        }
                    }
                    let ProductID_Array = await Store_Products.distinct('ProductID', queryx).lean();
                    let pQuery = {
                        ProductID: {
                            $in: ProductID_Array
                        },
                        TitleID: item.TitleID,
                        Status: true
                    }
                    let Products_Count = await Title_Management_Products.countDocuments(pQuery).lean().exec();
                    item.Products_Count = Products_Count
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Activate_Inactivate_Title = (values, TitleData) => {
    return new Promise(async (resolve, reject) => {

        try {
            let query = {
                TitleID: values.TitleID
            }
            let changes = {
                $set: {
                    Status: !TitleData.Status,
                    updated_at: new Date()
                }
            };
            let UpdateData = await Title_Management.updateOne(query, changes).lean().exec();
            let Status;
            if (TitleData.Status == true) {
                Status = "Inactivate Successfully"
            } else {
                Status = 'Activate Successfully'
            }
            resolve({ success: true, extras: { Status: Status } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Edit_Title = (values) => {
    return new Promise(async (resolve, reject) => {

        try {
            let query = {
                TitleID: values.TitleID
            }
            let changes = {
                $set: {
                    Title: values.Title,
                    Description: values.Description,
                    SNo: values.SNo,
                    updated_at: new Date()
                }
            };
            let UpdateData = await Title_Management.updateOne(query, changes).lean().exec();
            resolve({ success: true, extras: { Status: 'Updated Sucessfully' } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.List_All_Titles = (values) => {
    return new Promise(async (resolve, reject) => {

        try {
            let query = {
                Status: values.Status
            };
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                SNo: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let Count = await Title_Management.countDocuments(query).lean().exec();
            let Result = await Title_Management.find(query).select('-_id -__v -updated_at -Description').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            resolve({ success: true, extras: { Count: Count, Data: Result } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Create_Title = (values) => {
    return new Promise(async (resolve, reject) => {

        try {
            let Data = {
                TitleID: uuid.v4(),
                Title: values.Title,
                Description: values.Description,
                SNo: values.SNo,
                created_at: new Date(),
                updated_at: new Date()
            };
            let SaveData = await Title_Management(Data).save();
            resolve({ success: true, extras: { Status: 'Created Sucessfully' } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Add_Store_Product = (values, BranchData) => {
    return new Promise(async (resolve, reject) => {

        try {
            let Unique_ProductID = values.Unique_ProductID.toUpperCase();
            let Product_Name = values.Product_Name;
            Product_Name = Product_Name.replace(/\s\s+/g, ' ');
            Product_Name = Product_Name.replace(/  +/g, ' ');
            Product_Name = Product_Name.replace(/^ /, '');
            Product_Name = Product_Name.replace(/\s\s*$/, '');
            var myArr = Product_Name.toLowerCase().split(" ");
            for (var a = 0; a < myArr.length; a++) {
                myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
            }
            Product_Name = myArr.join(" ");
            var date = new Date();
            let CategoryData = await Category.findOne({ CategoryID: values.CategoryID }).lean().exec();
            let SubCategory = CategoryData.SubCategory_Data.filter(function (ResultTemp) {
                return ResultTemp.SubCategoryID == values.SubCategoryID;
            });
            async.eachSeries(values.Sales_Info, async (item, callback) => {
                try {
                    var Admin_Share_Amount = item.Admin_Share_Amount || 0;
                    var ProductID = uuid();
                    let Product_Image_Data = item.ImageData;
                    var Actual_Price = parseFloat(item.Price)
                    var Bookafella_Dis_Price = parseFloat(item.Bookafella_Discount_Price);
                    var Bookafella_Dis_Percent = parseFloat(((Bookafella_Dis_Price * 100) / Actual_Price).toFixed(2));
                    var Branch_Dis_Price = parseFloat(item.Branch_Discount_Price);
                    var Branch_Dis_Percent = parseFloat(((Branch_Dis_Price * 100) / Actual_Price).toFixed(2));
                    var Total_Discount_Price = parseFloat(Bookafella_Dis_Price + Branch_Dis_Price);
                    var Final_Price = parseFloat(Actual_Price - Total_Discount_Price); //
                    var Admin_Share_Percent = parseFloat(((Admin_Share_Amount * 100) / Final_Price).toFixed(2));
                    var Branch_Share_Amount = Final_Price - Admin_Share_Amount;
                    var ProductData = {
                        ProductID: ProductID,
                        ReorderID: uuid.v4(),
                        BranchID: BranchData.BranchID,
                        StoreID: BranchData.StoreID,
                        Unique_ProductID: Unique_ProductID,
                        Product_Name: Product_Name,
                        ZoneID: values.ZoneID,
                        CountryID: values.CountryID,
                        CityID: values.CityID,
                        Product_Edition: item.Product_Edition,
                        Product_Published_On: item.Product_Published_On,
                        Product_Publisher: item.Product_Publisher,
                        Product_Description: values.Product_Description,
                        About_Authors: values.About_Authors,
                        Authors: values.Authors,
                        CategoryID: values.CategoryID,
                        Category: CategoryData.Category,
                        SubCategoryID: values.SubCategoryID,
                        SubCategory: SubCategory[0].SubCategory,
                        Format_of_the_Book: item.Format_of_the_Book,
                        Total_Pages: item.Total_Pages,
                        Product_Image_Data: Product_Image_Data,
                        Product_ISBN: values.Product_ISBN,
                        Who_Created: values.AdminID,
                        Who_Approved: values.AdminID,
                        Product_Dimensions: {
                            Height: item.Height,
                            Width: item.Width,
                            Length: item.Length,
                        },
                        Product_Data_For_Branch: {
                            BranchID: BranchData.BranchID,
                            StoreID: BranchData.StoreID,
                            Branch_Name: BranchData.Branch_Name,
                            Avaiable_Quantity: item.Avaiable_Quantity,
                            Total_Quantity: item.Avaiable_Quantity,
                            Status: true,
                            Price: Actual_Price,
                            Branch_Discount_Percent: Branch_Dis_Percent,
                            Branch_Discount_Price: Branch_Dis_Price,
                            Bookafella_Discount_Percent: Bookafella_Dis_Percent,
                            Bookafella_Discount_Price: Bookafella_Dis_Price,
                            Total_Discount_Price: Total_Discount_Price,
                            Final_Price: Final_Price,
                            Condition: item.Condition,
                            Admin_Share_Percent: Admin_Share_Percent,
                            Admin_Share_Amount: Admin_Share_Amount,
                            Branch_Share_Amount: Branch_Share_Amount,
                            created_at: new Date(),
                            updated_at: new Date()
                        },
                        Status: true,
                        Who_Created: values.AdminID,
                        created_at: date,
                        updated_at: date
                    };
                    let SaveProduct = Store_Products(ProductData).save();
                    let SaveReOrder = Store_Product_Reorder(ProductData).save();

                    let SendNotification = await NotificationController.SendNotification_Add_Store_Product(ProductData)

                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Status: "Stored Successfully" } });
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Inactivate_Branch = (values, BranchData) => {
    return new Promise(async (resolve, reject) => {

        try {
            let query = {
                BranchID: BranchData.BranchID
            };
            let changes = {
                $set: {
                    Status: values.ChangeStatus,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Store_Branch.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: 'Status Updated Sucessfully' } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

AdminController.Create_Admin_User = (values, callback) => {
    let salt = rand(160, 36);
    let Password = values.Password;//Random Password
    let pass = Password + salt;
    let AdminID = uuid.v4();
    let date = new Date();
    let data = {
        AdminID: AdminID,
        Whether_Master: values.Whether_Master,
        Admin_Permissions: values.Admin_Permissions,
        Name: TitleCase(values.Name),
        EmailID: LowerCase(values.EmailID),
        PhoneNumber: values.PhoneNumber,
        PasswordHash: crypto.createHash('sha512').update(pass).digest("hex"),
        PasswordSalt: salt,
        FirstTimeLogin: true,
        WHO_CREATED: values.AdminID,
        created_at: date,
        updated_at: date
    };
    // console.log(data)
    Admin_User(data).save((err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            callback(false, { success: true, extras: { Status: "Admin Created Successfully" } }, Password);
        }
    })
};

AdminController.Check_Whether_Admin_Email_Already_Exist = (values, callback) => {
    Admin_User.findOne({ EmailID: LowerCase(values.EmailID) }, (err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            if (Result != null) {
                callback(true, { success: false, extras: { msg: ApiMessages.EMAIL_ALREADY_EXIST } });
            } else if (Result == null) {
                callback(false);
            }
        }
    })
}

// AdminController.Check_for_Master_Admin_and_Session = (values, callback) => {
//     AdminController.Check_for_Mater_Admin(values, (err, MasterData) => {
//         if (err) {
//             callback(true, MasterData);
//         } else {
//             AdminController.Check_for_Master_Admin_Session(values, (err, SessionStatus) => {
//                 if (err) {
//                     callback(true, SessionStatus);
//                 } else {
//                     callback(false, MasterData);
//                 }
//             })
//         }
//     })
// }
// AdminController.Check_for_Mater_Admin = (values, callback) => {
//     Admin_User.findOne({ AdminID: values.AdminID }, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             if (Result != null) {
//                 // if (Result.Whether_Master == true) {
//                 if (Result.Status == true) {
//                     callback(false, Result);
//                 } else if (Result.Status == false) {
//                     callback(false, { success: false, extras: { msg: ApiMessages.MASTER_ADMIN_NOT_ACTIVE } });
//                 }
//                 // } else if (Result.Whether_Master == false) {
//                 //     callback(false, { success: false, extras: { msg: ApiMessages.ADMIN_IS_NOT_MASTER_ADMIN } });
//                 // }
//             } else if (Result == null) {
//                 callback(true, { success: false, extras: { msg: ApiMessages.MASTER_ADMIN_NOT_FOUND } });
//             }
//         }
//     })
// }
// AdminController.Check_for_Master_Admin_Session = (values, callback) => {
//     Admin_User.findOne({ AdminID: values.AdminID, SessionID: values.SessionID }, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             if (Result != null) {
//                 callback(false, "Session Validated Successfully");
//             } else if (Result == null) {
//                 callback(true, { success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } });
//             }
//         }
//     })
// }

// AdminController.List_All_Admin_User = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let query = {
//                     Whether_God: false,
//                     Status: true
//                 };
//                 let toSkip = parseInt(values.skip);
//                 let toLimit = parseInt(values.limit);
//                 let sortOptions = {
//                     Name: 1
//                 };
//                 if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
//                     sortOptions = values.sortOptions;
//                 };
//                 let Count = await Admin_User.countDocuments(query).lean().exec();
//                 let Result = await Admin_User.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
//                 resolve({ success: true, extras: { Count: Count, Data: Result } });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// AdminController.Check_for_Inactive_Admin = (values, callback) => {
//     Admin_User.findOne({ AdminID: values.AdminID }, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             if (Result != null) {
//                 callback(false, Result);
//             } else if (Result == null) {
//                 callback(true, { success: false, extras: { msg: ApiMessages.ADMIN_NOT_FOUND } });
//             }
//         }
//     })
// }
// AdminController.Activate_Admin_User = (values, callback) => {
//     let query = {
//         AdminID: values.AdminID
//     };
//     let changes = {
//         $set: {
//             Status: true
//         }
//     };
//     Admin_User.updateMany(query, changes, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             callback(false, { success: true, extras: { Status: "Admin Activated Successfully" } });
//         }
//     });
// }
// AdminController.List_All_Inactive_Admin_Users = (values, callback) => {
//     let toSkip = parseInt(values.skip);
//     let toLimit = parseInt(values.limit);
//     let sortOptions = {
//         created_at: -1
//     };
//     if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
//         sortOptions = values.sortOptions;
//     };
//     let query = {
//         Whether_Master: false,
//         Status: false
//     };
//     let CountFunction = (callback) => {
//         process.nextTick(() => {
//             Admin_User.count(query).exec((err, Result) => {
//                 callback(null, Result);
//             })
//         })
//     };
//     let ListFunction = (callback) => {
//         process.nextTick(() => {
//             Admin_User.find(query).sort(sortOptions).skip(toSkip).limit(toLimit).exec((err, Result) => {
//                 callback(null, Result);
//             })
//         })
//     };
//     sync(() => {
//         let Count = CountFunction.sync(null);
//         let Result = ListFunction.sync(null);
//         let Data = [];
//         async.eachSeries(Result, (item, resp) => {

//             Data.push({
//                 AdminID: item.AdminID,
//                 Whether_Master: item.Whether_Master,
//                 Name: item.Name,
//                 EmailID: item.EmailID,
//                 PhoneNumber: item.PhoneNumber,
//                 FirstTimeLogin: item.FirstTimeLogin,
//                 created_at: moment(item.created_at).utcOffset(330).format(config.DateFormat)
//             });
//             resp();
//         }, (err) => {
//             callback({ success: true, extras: { Count: Count, Data: Data } });
//         })
//     })
// }
// AdminController.Remove_Admin_User = (values, callback) => {
//     let query = {
//         AdminID: values.AdminID
//     };
//     let changes = {
//         $set: {
//             Status: false
//         }
//     };
//     Admin_User.updateMany(query, changes, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             callback(false, { success: true, extras: { Status: "Admin Removed Successfully" } });
//         }
//     });
// }
AdminController.List_All_Admin_Users = (values, callback) => {
    let toSkip = parseInt(values.skip);
    let toLimit = parseInt(values.limit);
    let sortOptions = {
        created_at: -1
    };
    if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
        sortOptions = values.sortOptions;
    };
    let query = {
        Status: true
    };
    let CountFunction = (callback) => {
        process.nextTick(() => {
            Admin_User.countDocuments(query).exec((err, Result) => {
                callback(null, Result);
            })
        })
    };
    let ListFunction = (callback) => {
        process.nextTick(() => {
            //.sort(sortOptions)
            Admin_User.find(query).skip(toSkip).limit(toLimit).exec((err, Result) => {
                callback(null, Result);
            })
        })
    };
    sync(() => {
        let Count = CountFunction.sync(null);
        let Result = ListFunction.sync(null);
        let Data = [];
        async.eachSeries(Result, (item, resp) => {
            //      callback({ success: true, extras: { Count: Count, Data: item } });
            Data.push({
                AdminID: item.AdminID,
                Whether_Master: item.Whether_Master,
                Name: item.Name,
                EmailID: item.EmailID,
                PhoneNumber: item.PhoneNumber,
                FirstTimeLogin: item.FirstTimeLogin,
                created_at: moment(item.created_at).utcOffset(330).format(config.DateFormat)
            });
            resp();
        }, (err) => {
            callback({ success: true, extras: { Count: Count, Data: Data } });
        })
    })
}

AdminController.RandomString = () => {
    let charBank = "123456789";
    let fstring = '';
    for (let i = 0; i < 8; i++) {
        fstring += charBank[parseInt(Math.random() * charBank.length)];
    }
    return String(fstring);
};

// AdminController.Admin_Upload_Image_Functionality = (values, ResizeData, AdminData, contentType, ImageID, Image100, Image250, Image550, Image900, ImageOriginal, nfile_Original, callback) => {
//     async.eachSeries(ResizeData, (item, resp) => {
//         Jimp.read(nfile_Original, (err, image) => {
//             console.log(image);
//             image.resize(Jimp.AUTO, item.height)
//                 .quality(100)
//                 .write(item.nfile, (err, Result) => {
//                     //now upload it to AWS and unlink the image
//                     AdminController.UploadFileAWS(item.nfile, item.fname, contentType, (err, responcer) => {
//                         if (!err) {
//                             resp();
//                         }
//                     })
//                 });
//         })
//     }, (err) => {
//         if (err) {
//             logger.info(JSON.parse(JSON.stringify(err)));
//         } else {
//             AdminController.UploadFileAWS(nfile_Original, ImageOriginal, contentType, (err, responcer) => {
//                 if (!err) {
//                     callback("Image Functionality Completed");
//                 }
//             })
//         }
//     })
// }

// //Aws upload
// AdminController.UploadFileAWS = (nfile, fname, contentType, callback) => {
//     // Upload to the S3 Bucket
//     fs.readFile(nfile, (err, buf) => {
//         let req = knoxClient.put(fname, {
//             'Content-Length': buf.length,
//             'Content-Type': contentType
//         });
//         req.on('response', (rest) => {
//             if (rest.statusCode == 200) {
//                 // Delete the Local File
//                 fs.unlink(nfile, () => {
//                     callback(false, { success: true, extras: { Status: "Uploaded Successfully" } });
//                 })
//             } else {
//                 logger.error("AWS Upload Fails");
//             }
//         });
//         req.end(buf);
//     })
// };

AdminController.Check_for_Admin_and_Session = (values, callback) => {
    AdminController.Check_for_Admin(values, (err, AdminData) => {
        if (err) {
            callback(true, AdminData);
        } else {
            AdminController.Check_for_Admin_Session(values, (err, SessionStatus) => {
                if (err) {
                    callback(true, SessionStatus);
                } else {
                    callback(false, AdminData);
                }
            })
        }
    })
}

AdminController.Check_for_Admin = (values, callback) => {
    let query = new Object();
    if (config.AdminID == values.AdminID) {
        query = {
            Status: true
        };
    } else {
        query = {
            AdminID: values.AdminID
        };
    }
    Admin_User.findOne(query, (err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            if (Result != null) {
                if (Result.Status == true) {
                    callback(false, Result);
                } else if (Result.Status == false) {
                    callback(true, { success: false, extras: { msg: ApiMessages.ADMIN_INACTIVE_NOT_FOUND } });
                }
            } else if (Result == null) {
                callback(true, { success: false, extras: { msg: ApiMessages.ADMIN_INACTIVE_NOT_FOUND } });
            }
        }
    })
}

AdminController.Check_for_Admin_Session = (values, callback) => {
    let query = new Object();
    if (config.AdminID == values.AdminID) {
        query = {
            Status: true
        };
    } else {
        query = {
            AdminID: values.AdminID
        };
    }
    Admin_User.findOne(query, (err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            if (Result != null) {
                if (Result.SessionID == values.SessionID || config.SECRET_SESSIONID == values.SessionID || config.SessionID == values.SessionID) {
                    callback(false, "Session Validated Successfully");
                } else {
                    callback(true, { success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } });
                }
            } else {
                callback(true, { success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } });
            }
        }
    })
}

// AdminController.Check_For_Admin_Image_Serial_Number = (values, AdminData, callback) => {
//     let query = {
//         AdminID: AdminData.AdminID,
//         ImageUsed: false,
//         Serial_Number: parseInt(values.Serial_Number)
//     }
//     Admin_Images.findOne(query, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             if (Result != null) {
//                 callback(true, { success: false, extras: { msg: ApiMessages.IMAGE_SERIAL_NUMBER_ALREADY_EXIST } });
//             } else {
//                 callback(false, "new Image")
//             }
//         }
//     })
// }

AdminController.Get_Branch_Data = (BranchID, callback) => {
    Store_Branch.findOne({ BranchID: BranchID }, (err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            if (Result != null) {
                callback(false, Result);
            } else if (Result == null) {
                callback(true, { success: false, extras: { msg: ApiMessages.STORE_BRANCH_DATA_NOT_FOUND } });
            }
        }
    })
};


// AdminController.Delete_Existing_AWS_Images = (ImageId, callback) => {
//     Admin_Images.findOne({ ImageID: ImageId }, (err, Result) => {
//         if (err) {
//             callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
//         } else {
//             if (Result != null) {

//                 let FileNames = [];
//                 FileNames.push(Result.Image100);
//                 FileNames.push(Result.Image250);
//                 FileNames.push(Result.Image550);
//                 FileNames.push(Result.Image900);
//                 FileNames.push(Result.ImageOriginal);
//                 knoxClient.deleteMultiple(FileNames, function (err, res) {
//                     if (err) {
//                         callback(true, { success: false, extras: { msg: ApiMessages.COVER_IMAGE_ERROR } });
//                     } else {
//                         callback(false, "Images Deleted Successfully");
//                     }
//                 });
//             } else if (Result == null) {
//                 callback(true, { success: false, extras: { msg: ApiMessages.STORE_BRANCH_DATA_NOT_FOUND } });
//             }
//         }
//     })
// };

// //This Api is use for Removing the AWS Image based on key
// AdminController.DeleteAWSImage = function (fname, callback) {
//     var aws = require('aws-sdk');
//     var S3AccessKey = config.S3AccessKey;
//     var S3Secret = config.S3Secret;
//     var S3Bucket = config.S3Bucket;
//     aws.config.update({
//         accessKeyId: S3AccessKey,
//         secretAccessKey: S3Secret
//     });
//     var s3 = new aws.S3();
//     var params = {
//         Bucket: S3Bucket,
//         Delete: { // required
//             Objects: [ // required
//                 {
//                     Key: fname // required
//                 }
//             ]
//         }
//     };
//     s3.deleteObjects(params, function (err, data) {
//         return callback(false, 'Deleted Successfully');
//     });
// };

AdminController.findCustomerPasswordTries = (values, callback) => {
    var error;
    var valid_attempts = new Date(moment().subtract(30, 'minute').toISOString());
    CustomerPasswordTries.find({
        Phone: values.Phone,
        time: {
            $gt: valid_attempts
        }
    }, function (err, Result) {
        var Count = Result.length;
        if (Count >= 5) {
            error = true;
            return callback(error, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.PASSWORD_TRIES_LIMIT_EXCEDDED
                }
            }));
        } else {
            error = false;
            return callback(error);
        }

    });
};

//Check Whether Phone parseFloat Exist or Not for Loginn and Security
AdminController.Check_Whether_PhoneNumber_Exist = (values, callback) => {
    var error;
    var query = {
        Phone: values.Phone,
        Whether_Guest: {
            $ne: true
        }
    };
    Customers.findOne(query).exec(function (err, Result) {
        if (Result) {
            error = false;
            return callback(false, Result);
        } else {
            error = true;
            return callback(error, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.PHONE_Number_NOT_EXIST
                }
            }));
        }
    })
};

//Customer Singin Module
AdminController.CustomerSignIn = (values, CustomerData, callback) => {
    var error;
    var SecretPassword = config.SECRET_OTP_CODE;
    var Salt = CustomerData.PasswordSalt;
    var newpass = values.Password + Salt;
    var ExistPasswordHash = CustomerData.PasswordHash;
    var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");
    if (ExistPasswordHash === NewPasswordHash || SecretPassword === values.Password) {
        AdminController.DeleteCustomerPasswordTries(values, function (err, Result) {
            error = false;
            return callback(error, 'Login Successful')
        });
    } else {
        AdminController.RegisterCustomerPasswordTries(values, function (err, Result) {
            error = true;
            return callback(error, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.INVALID_PASSWORD
                }
            }));
        });
    }
};

//Storing Customer Password Tries
AdminController.RegisterCustomerPasswordTries = (values, callback) => {
    var error;
    var date = new Date();
    var CustomerPasswordTryData = new CustomerPasswordTries({
        Phone: values.Phone,
        time: date
    });
    CustomerPasswordTryData.save(function (err, Result) {
        error = false;
        return callback(error, 'Password Try Registered Successfully');
    });
};

//Removing Customer Password Tries
AdminController.DeleteCustomerPasswordTries = (values, callback) => {
    CustomerPasswordTries.remove({
        Phone: values.Phone
    }, function (err, Result) {
        if (Result) {
            var error = false;
            return callback(error, new ApiResponce({
                success: true
            }));
        }
    });
};


//Checking Whether Customer Session is Exist or Not Based on Customer ID
AdminController.Check_Customer_Session_Stores = (CustomerData, callback) => {
    var error;
    var query = {
        StoreAdminID: CustomerData.CustomerID
    };
    Store_Admin_Sessions.findOne(query).exec(function (err, Result) {
        if (Result) {
            error = false;
            return callback(error);
        } else {
            error = true;
            return callback(error);
        }
    })
};
//Registering the Customer Session and Generating the Session ID for Stores
AdminController.RegisteringCustomerSession_Store = function (CustomerData, callback) {
    var SessionID = uuid();
    var StoreAdminID = CustomerData.CustomerID;
    var date = new Date();
    var error;
    var SessionData = new Store_Admin_Sessions({
        StoreAdminID: StoreAdminID,
        SessionID: SessionID,
        created_at: date,
        updated_at: date
    });
    SessionData.save();
    var StoreAdminData = {
        StoreAdminID: StoreAdminID,
        First_name: CustomerData.First_name,
        SessionID: SessionID,
        First_Time_Login: CustomerData.First_Time_Login,
        BranchData: CustomerData.BranchData,
        Active_BranchID_Exist: CustomerData.Active_BranchID_Exist,
        Active_BranchID: CustomerData.Active_BranchID
    };
    error = false;
    return callback(error, new ApiResponce({
        success: true,
        extras: {
            Status: 'Login Successfully',
            StoreAdminData: StoreAdminData,
            LoginStatus: true
        }
    }));
};

//Updating teh Customer Session and Updating the new Session ID for Stores
AdminController.UpdatingCustomerSession_Store = (CustomerData, callback) => {
    var SessionID = uuid();
    var StoreAdminID = CustomerData.CustomerID;
    var date = new Date();
    var error;
    var StoreAdminData = {
        StoreAdminID: StoreAdminID,
        SessionID: SessionID,
        First_name: CustomerData.First_name,
        First_Time_Login: CustomerData.First_Time_Login,
        BranchData: CustomerData.BranchData,
        Active_BranchID_Exist: CustomerData.Active_BranchID_Exist,
        Active_BranchID: CustomerData.Active_BranchID
    };
    var query = {
        StoreAdminID: StoreAdminID,
    };
    var changes = {
        SessionID: SessionID,
        updated_at: date
    };
    var multiplicity = {
        multi: false
    };
    Store_Admin_Sessions.update(query, changes, multiplicity).exec(function (err, Result) {
        if (Result) {
            error = false;
            return callback(error, new ApiResponce({
                success: true,
                extras: {
                    Status: 'Login Successfully',
                    StoreAdminData: StoreAdminData,
                    LoginStatus: true
                }
            }));
        }
    });

};

//CHECK WHETHER CATEGORY NAME EXIST OR NOT
AdminController.Validate_Store_Branch_URL = (BranchURL, callback) => {
    Store_Branch.findOne({ BranchURL: BranchURL }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.BRANCH_URL_ALREADY_IN_USE
                    }
                }));
            } else if (Result == null) {
                callback(false, new ApiResponce({
                    success: true,
                    extras: {
                        msg: "Branch URL can be used"
                    }
                }));
            }
        }
    })
};

//Check for Store Branch
AdminController.Check_For_Store_BranchID = (values, callback) => {
    Store_Branch.findOne({ BranchID: values.BranchID }).exec(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            if (Result == null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.Store_Branch_Not_Found
                    }
                }))
            } else if (Result != null) {
                callback(false, Result);
            }
        }
    })
};

//Get Branch in Details
AdminController.Branch_In_Detail = (BranchData, callback) => {
    var AdminData = [];
    var StoreAdminID;
    var Name;
    var EmailID;
    var PhoneNumber;
    var date = moment(BranchData.created_at).utcOffset(330).format('MMMM Do YYYY, h:mm:ss A');
    async.eachSeries(BranchData.AdminData, function (item, resp) {
        Customers.findOne({ CustomerID: item.StoreAdminID }).exec(function (err, Result) {
            if (err) {
                console.log(err);
            } else {
                if (Result == null) {
                    StoreAdminID = item.StoreAdminID;
                    Name = '';
                    EmailID = '';
                    PhoneNumber = '';
                } else if (Result != null) {
                    StoreAdminID = item.StoreAdminID;
                    Name = Result.First_name;
                    EmailID = Result.Email;
                    PhoneNumber = Result.Phone;
                }
                AdminData.push({
                    StoreAdminID: StoreAdminID,
                    Name: Name,
                    EmailID: EmailID,
                    PhoneNumber: PhoneNumber
                })
                resp();
            }
        })
    }, function (err) {
        Country.findOne({ CountryID: BranchData.CountryID }).lean().exec().then((CountryData) => {
            City.findOne({ CityID: BranchData.CityID }).lean().exec().then((CityData) => {
                ZONES.findOne({ ZoneID: BranchData.ZoneID }).lean().exec().then((ZoneData) => {
                    if (!err) {
                        var BranchDetails = {
                            EntityID: BranchData.EntityID,
                            BranchID: BranchData.BranchID,
                            MenuList: [],
                            Store_Entity_Name: BranchData.Store_Entity_Name,
                            Branch_Name: BranchData.Branch_Name,
                            Branch_PhoneNumber: BranchData.Branch_PhoneNumber,
                            BranchURL: BranchData.BranchURL,
                            Description: BranchData.Description,
                            Branch_Gallery_Images: BranchData.Branch_Gallery_Images,
                            Branch_Cover_ImageID: BranchData.Branch_Cover_ImageID,
                            ImageID: BranchData.ImageID,
                            Branch_Cover_ImageURL: "",
                            ImageURL: "",
                            AdminData: AdminData,
                            Address: BranchData.Address,
                            Latitude: BranchData.Latitude,
                            Longitude: BranchData.Longitude,
                            Monday_Available: BranchData.Monday_Available,
                            Monday_Timings: BranchData.Monday_Timings,
                            Tuesday_Available: BranchData.Tuesday_Available,
                            Tuesday_Timings: BranchData.Tuesday_Timings,
                            Wednesday_Available: BranchData.Wednesday_Available,
                            Wednesday_Timings: BranchData.Wednesday_Timings,
                            Thursday_Available: BranchData.Thursday_Available,
                            Thursday_Timings: BranchData.Thursday_Timings,
                            Friday_Available: BranchData.Friday_Available,
                            Friday_Timings: BranchData.Friday_Timings,
                            Saturday_Available: BranchData.Saturday_Available,
                            Saturday_Timings: BranchData.Saturday_Timings,
                            Sunday_Available: BranchData.Sunday_Available,
                            Sunday_Timings: BranchData.Sunday_Timings,
                            Today_Working: BranchData.Today_Working,
                            Status: BranchData.Status,
                            CountryData: CountryData,
                            CityData: CityData,
                            ZoneData: ZoneData,
                            date: date
                        };
                        return callback(false, BranchDetails);
                    }
                    return callback(true);
                }).catch();
            }).catch();
        }).catch();
    })
}

//Get Branch Gallery Details
AdminController.Branch_Gallery_Details = (BranchData, callback) => {
    var Branch_Gallery_Data = [], ImageID, Sl_No, ImageURL;
    async.eachSeries(BranchData.Branch_Gallery_Images, function (item, resp) {
        Images.findOne({ ImageID: item.ImageID }).exec(function (err, Result) {
            if (err) {
                console.log(err);
            } else {
                if (Result == null) {
                    ImageID = item.ImageID;
                    Sl_No = item.Sl_No;
                    ImageURL = "";
                } else if (Result != null) {
                    ImageID = item.ImageID;
                    Sl_No = item.Sl_No;
                    ImageURL = config.S3URL + Result.ImageOriginal;
                }
                Branch_Gallery_Data.push({
                    ImageID: ImageID,
                    Sl_No: Sl_No,
                    ImageURL: ImageURL
                })
                resp();
            }
        })
    }, function (err) {
        if (!err) {
            BranchData.Branch_Gallery_Images = Branch_Gallery_Data;
            return callback(false, BranchData);
        }
        return callback(true);
    });
};

//Returns image url
AdminController.Gets_Image_Data = (ImageID, callback) => {
    if (ImageID !== "") {
        Images.findOne({ ImageID: ImageID }).exec(function (err, Result) {
            if (err) {
                console.log(err);
            } else {
                if (Result == null) {
                    callback(false, "")
                } else if (Result != null) {
                    callback(false, config.S3URL + Result.ImageOriginal);
                }
            }
        })
    } else {
        callback(false, "");
    }
};

//Returns file url
AdminController.Gets_File_Data = (FileID, callback) => {
    if (FileID !== "") {
        Files.findOne({ FileID: FileID }).exec(function (err, Result) {
            if (err) {
                console.log(err);
            } else {
                if (Result == null) {
                    callback(false, "")
                } else if (Result != null) {
                    callback(false, config.S3URL + Result.File_Path);
                }
            }
        })
    } else {
        callback(false, "");
    }
};

//Admin Singin Module
AdminController.AdminSignIn = (values, CustomerData, callback) => {
    var error;
    var SecretPassword = config.SecretPassword;
    var Salt = CustomerData.PasswordSalt;
    var newpass = values.Password + Salt;
    var ExistPasswordHash = CustomerData.PasswordHash;
    var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");
    if (ExistPasswordHash === NewPasswordHash || SecretPassword === values.Password) {
        AdminController.DeleteCustomerPasswordTries(values, function (err, Result) {
            error = false;
            return callback(error, 'Login Successful')
        });
    } else {
        error = true;
        return callback(error, new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.INVALID_PASSWORD
            }
        }));
    }
};

//Updating the admin Session
AdminController.UpdatingAdmin_Session = (CustomerData, callback) => {
    var SessionID = uuid();
    var date = new Date();
    var error;
    var query = {
        AdminID: CustomerData.AdminID,
    };
    var changes = {
        SessionID: SessionID,
        updated_at: date
    };
    var multiplicity = {
        multi: false
    };
    Admin_User.update(query, changes, multiplicity).exec(function (err, Result) {
        if (Result) {
            error = false;
            return callback(error, SessionID);
        }
    });
};

AdminController.List_All_Stores = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    Store_Entity_Name: 1
                };
                let Result = await Store_Entity.find(query).select('-_id -__v -Status -created_at -Website -Description -updated_at').sort(sortOptions).exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Store_Branch_Data_By_URL = (BranchURL, callback) => {
    Store_Branch.findOne({ BranchURL: BranchURL }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(false, Result);
            } else if (Result == null) {
                callback(true);
            }
        }
    })
};

AdminController.List_All_Products_With_Filters = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let queryStory = {
                    Status: true
                }
                let StoreID_Array = await Store_Branch.distinct('BranchID', queryStory).lean()
                let query = {
                    'Product_Data_For_Branch.BranchID': {
                        $in: StoreID_Array
                    },
                    Status: values.Status,
                    Whether_Deleted: {
                        $ne: true
                    }
                };
                if (values.CategoryID != null && values.CategoryID != ""
                    && values.CategoryID != undefined && values.CategoryID != " ") {
                    query.CategoryID = values.CategoryID
                }
                if (values.Search == '') {
                    if (values.CountryID != '') {
                        query.CountryID = values.CountryID;
                    }
                    if (values.CityID != '') {
                        query.CityID = values.CityID
                    }
                    if (values.ZoneID != '') {
                        query.ZoneID = values.ZoneID
                    }
                    if (values.BranchID != '') {
                        // query.Product_Data_For_Branch.BranchID = 
                        query['Product_Data_For_Branch.BranchID'] = values.BranchID
                    }
                    // } else {
                    //     reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                    // }
                } else {
                    let Search = {
                        $regex: String(values.Search),
                        $options: "i"
                    };
                    query.$or = [
                        {
                            Unique_ProductID: Search
                        },
                        {
                            Product_Name: Search
                        },
                        {
                            Category: Search
                        },
                        {
                            SubCategory: Search
                        }
                    ]
                }
                // console.log(query)
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let CountDoc = await Store_Products.countDocuments(query).lean().exec();
                var AllProductsData = await Store_Products.find(query).select('-_id -__v -created_at -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                async.eachSeries(AllProductsData, function (ProductData, resp) {
                    //add country name and country code city and zone
                    // console.log(ProductData)
                    Country.findOne({ CountryID: ProductData.CountryID }).lean().exec().then((CountryData) => {
                        City.findOne({ CityID: ProductData.CityID }).lean().exec().then((CityData) => {
                            ZONES.findOne({ ZoneID: ProductData.ZoneID }).lean().exec().then((ZoneData) => {
                                CommonController.Common_Image_Response_Single_Image(true, ProductData.Product_Image_Data).then((Product_Image_Data) => {
                                    ProductData.Product_Image_Data = Product_Image_Data;
                                    ProductData.CountryData = CountryData;
                                    ProductData.CityData = CityData;
                                    ProductData.ZoneData = ZoneData;
                                    resp();
                                }).catch();
                            }).catch();
                        }).catch();
                    }).catch();
                }, function (err) {
                    if (!err) {
                        resolve({ success: true, extras: { Count: CountDoc, Data: AllProductsData } });
                    }
                })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Branches = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: values.Status
                };
                if (values.ZoneID != '') {
                    query.ZoneID = values.ZoneID
                }
                if (values.CityID != '') {
                    query.CityID = values.CityID
                }
                if (values.CountryID != '') {
                    query.CountryID = values.CountryID;
                }
                if (values.BranchID != '') {
                    query.BranchID = values.BranchID
                }
                if (values.Search != '') {
                    let Search = {
                        $regex: String(values.Search),
                        $options: "i"
                    };
                    query.$or = [
                        {
                            StoreID: Search
                        },
                        {
                            Branch_Name: Search
                        }
                    ]
                }
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                var AllBranches = [];
                let CountDoc = await Store_Branch.countDocuments(query).lean().exec();
                var AllBranchData = await Store_Branch.find(query).select('-_id -__v -created_at -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                async.eachSeries(AllBranchData, function (BranchData, resp) {
                    //add country name and country code city and zone
                    Country.findOne({ CountryID: BranchData.CountryID }).lean().exec().then((CountryData) => {
                        City.findOne({ CityID: BranchData.CityID }).lean().exec().then((CityData) => {
                            ZONES.findOne({ ZoneID: BranchData.ZoneID }).lean().exec().then((ZoneData) => {
                                let FileID = '';
                                if (BranchData.FileID == null || BranchData.FileID == undefined) {
                                    FileID = '';
                                } else {
                                    FileID = BranchData.FileID;
                                }
                                var BranchDetails = {
                                    Store_Entity_Name: BranchData.Store_Entity_Name,
                                    BranchID: BranchData.BranchID,
                                    StoreID: BranchData.StoreID,
                                    Branch_Name: BranchData.Branch_Name,
                                    Branch_PhoneNumber: BranchData.Branch_PhoneNumber,
                                    BranchURL: BranchData.BranchURL,
                                    Description: BranchData.Description,
                                    Branch_Gallery_Images: BranchData.Branch_Gallery_Images,
                                    Branch_Cover_ImageID: BranchData.Branch_Cover_ImageID,
                                    FileID: FileID,
                                    FileURL: "",
                                    ImageID: BranchData.ImageID,
                                    Branch_Cover_ImageURL: "",
                                    ImageURL: "",
                                    Address: BranchData.Address,
                                    Latitude: BranchData.Latitude,
                                    Longitude: BranchData.Longitude,
                                    Monday_Available: BranchData.Monday_Available,
                                    Monday_Timings: BranchData.Monday_Timings,
                                    Tuesday_Available: BranchData.Tuesday_Available,
                                    Tuesday_Timings: BranchData.Tuesday_Timings,
                                    Wednesday_Available: BranchData.Wednesday_Available,
                                    Wednesday_Timings: BranchData.Wednesday_Timings,
                                    Thursday_Available: BranchData.Thursday_Available,
                                    Thursday_Timings: BranchData.Thursday_Timings,
                                    Friday_Available: BranchData.Friday_Available,
                                    Friday_Timings: BranchData.Friday_Timings,
                                    Saturday_Available: BranchData.Saturday_Available,
                                    Saturday_Timings: BranchData.Saturday_Timings,
                                    Sunday_Available: BranchData.Sunday_Available,
                                    Sunday_Timings: BranchData.Sunday_Timings,
                                    Today_Working: BranchData.Today_Working,
                                    CountryData: CountryData,
                                    Whether_Branch_Online: BranchData.Whether_Branch_Online,
                                    CityData: CityData,
                                    ZoneData: ZoneData,
                                    Status: BranchData.Status
                                };
                                AdminController.Branch_Gallery_Details(BranchDetails, function (err, BranchDetails) {
                                    if (err) {
                                        res.send(new ApiResponce({
                                            success: false,
                                            extras: {
                                                msg: ApiMessages.DATABASE_ERROR
                                            }
                                        }));
                                    } else {
                                        AdminController.Gets_Image_Data(BranchDetails.Branch_Cover_ImageID, function (err, CoverImageData) {
                                            if (!err) {
                                                BranchDetails.Branch_Cover_ImageURL = CoverImageData;
                                                AdminController.Gets_Image_Data(BranchDetails.ImageID, function (err, ImageData) {
                                                    if (!err) {
                                                        BranchDetails.ImageURL = ImageData;
                                                        AdminController.Gets_File_Data(FileID, function (err, FileData) {
                                                            if (!err) {
                                                                BranchDetails.FileURL = FileData;
                                                                AllBranches.push(BranchDetails);
                                                                resp();
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        });
                                    }
                                })
                            }).catch();
                        }).catch();
                    }).catch();

                }, function (err) {
                    if (!err) {
                        resolve({ success: true, extras: { Count: CountDoc, Data: AllBranches } });
                    }
                })

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Branches_Lite = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                if (values.ZoneID != null && values.ZoneID != ""
                    && values.ZoneID != " " && values.ZoneID != undefined
                ) {
                    query.ZoneID = values.ZoneID
                }
                let sortOptions = {
                    Branch_Name: 1
                };
                var AllBranches = [];
                let CountDoc = await Store_Branch.countDocuments(query).lean().exec();
                var AllBranchData = await Store_Branch.find(query).select('-_id -__v -created_at -updated_at').sort(sortOptions).lean().exec();

                async.eachSeries(AllBranchData, function (BranchData, resp) {
                    var BranchDetails = {
                        Store_Entity_Name: BranchData.Store_Entity_Name,
                        BranchID: BranchData.BranchID,
                        StoreID: BranchData.StoreID,
                        Branch_Name: BranchData.Branch_Name,
                        Branch_PhoneNumber: BranchData.Branch_PhoneNumber,
                        BranchURL: BranchData.BranchURL,
                    };
                    AllBranches.push(BranchDetails);
                    resp();
                }, function (err) {
                    if (!err) {
                        resolve({ success: true, extras: { Count: CountDoc, Data: AllBranches } });
                    }
                })

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Find_All_Products_Lite = function (values, callback) {
    let sortOptions = {
        created_at: -1
    };
    let Search = {
        $regex: String(values.Search),
        $options: "i"
    };
    let query = {
        $or: [{
            Product_Name: Search
        }],
        Status: true,
        Whether_Deleted: {
            $ne: true
        }
    };

    Store_Products.countDocuments(query).exec(function (err, Count) {
        if (Count >= 0) {
            Store_Products.find(query).sort(sortOptions).exec(function (err, Result) {
                if (!err) {
                    var ProductData = [];
                    async.eachSeries(Result, function (item, resp) {
                        CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data).then((Product_Image_Data) => {
                            ProductData.push({
                                ProductID: item.ProductID,
                                Product_Name: item.Product_Name,
                                Unique_ProductID: item.Unique_ProductID,
                                Authors: item.Authors,
                                Product_Image_Data: Product_Image_Data,
                                Status: item.Status
                            })
                            resp();
                        }).catch(err => callback(err));
                    }, function (err) {
                        if (!err) {
                            return callback(new ApiResponce({
                                success: true,
                                extras: {
                                    Count: Count,
                                    ProductData: ProductData,
                                }
                            }));
                        }
                    })
                }
            })
        }
    })
}

AdminController.List_All_Branch_Admins = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                var AdminData = [], StoreAdminID, Name, EmailID, PhoneNumber;
                let BranchData = await Store_Branch.findOne(query).select('-_id AdminData').exec();

                if (BranchData.length != 0) {
                    async.eachSeries(BranchData.AdminData, function (item, resp) {
                        Customers.findOne({ CustomerID: item.StoreAdminID }).exec(function (err, Result) {
                            if (err) {
                                console.log(err);
                            } else {
                                if (Result == null) {
                                    StoreAdminID = item.StoreAdminID;
                                    Name = '';
                                    EmailID = '';
                                    PhoneNumber = '';
                                } else if (Result != null) {
                                    StoreAdminID = item.StoreAdminID;
                                    Name = Result.First_name;
                                    EmailID = Result.Email;
                                    PhoneNumber = Result.Phone;
                                }
                                AdminData.push({
                                    StoreAdminID: StoreAdminID,
                                    Name: Name,
                                    EmailID: EmailID,
                                    PhoneNumber: PhoneNumber
                                })
                                resp();
                            }


                        })


                    }, function (err) {
                        if (!err) {
                            resolve({ success: true, extras: { Data: AdminData } });
                        }
                    })

                } else {
                    resolve({ success: true, extras: { Data: AdminData } });
                }

            } catch (error) {
                resolve({ success: true, extras: { Data: AdminData } });
                //  reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Delete_Branch_Admins = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let updateQuery = { $pull: { AdminData: { StoreAdminID: values.StoreAdminID } } };
                let multi = { upsert: true };
                let Result = await Store_Branch.findOne(query).select('-_id AdminData').exec();
                let TotalAdmins = Result.AdminData.length;
                if (TotalAdmins === 1) {
                    resolve({
                        success: false,
                        extras: {
                            msg: ApiMessages.STORE_ADMIN_CANNOT_BE_EMPTY
                        }
                    });
                }
                if (TotalAdmins > 1) {
                    Store_Branch.updateOne(query, updateQuery, multi).exec(function (err, Result) {
                        if (Result) {
                            return resolve({
                                success: true,
                                extras: {
                                    Status: "Store Admin Deleted Successfully."
                                }
                            });
                        }
                    });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Branch_Admins = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Phone: values.PhoneNumber
                };
                let updateQuery = {
                    First_name: values.Name,
                    Email: values.EmailID
                };
                let multi = { upsert: true };

                Customers.updateOne(query, updateQuery, multi).exec(function (err, Result) {
                    if (Result) {
                        return resolve({
                            success: true,
                            extras: {
                                Status: "Store Admin Updated Successfully."
                            }
                        });
                    }
                });
                let SendNotification = await NotificationController.SendNotification_Update_Branch_Admin(values)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//CHECK WHETHER CATEGORY NAME EXIST OR NOT
AdminController.Check_Whether_Store_Branch_Name_Already_Exists_Update = function (values, callback) {
    Store_Branch.findOne({ Branch_Name: values.Branch_Name, "BranchID": { $ne: values.BranchID } }).exec(function (err, Result) {
        if (err) {
            console.log(err);
            return callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result == null) {
                callback(false);
            } else if (Result != null) {
                return callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.STORE_BRANCH_NAME_ALREADY_EXIST
                    }
                }));
            }
        }
    })
};


//CHECK WHETHER CATEGORY NAME EXIST OR NOT
AdminController.Validate_Store_Branch_URL_Update = (values, callback) => {
    Store_Branch.findOne({ BranchURL: values.BranchURL, "BranchID": { $ne: values.BranchID } }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.BRANCH_URL_ALREADY_IN_USE
                    }
                }));
            } else if (Result == null) {
                callback(false, new ApiResponce({
                    success: true,
                    extras: {
                        msg: "Branch URL can be used"
                    }
                }));
            }
        }
    })
};

AdminController.Update_Branch_Details = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var date = new Date();
                let query = {
                    BranchID: values.BranchID
                };
                let updateQuery = {
                    Branch_Name: values.Branch_Name,
                    Branch_PhoneNumber: values.Branch_PhoneNumber,
                    BranchURL: values.BranchURL,
                    Description: values.Description,
                    ImageID: values.ImageID,
                    Address: values.Address,
                    Latitude: parseFloat(values.Latitude),
                    Longitude: parseFloat(values.Longitude),
                    Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    updated_at: date
                };
                // console.log(updateQuery)
                Store_Branch.updateOne(query, updateQuery).exec(function (err, Result) {
                    if (Result) {
                        return resolve({
                            success: true,
                            extras: {
                                Status: "Store branch updated successfully."
                            }
                        });
                    }
                });
                let SendNotification = await NotificationController.SendNotification_Update_Branch_Details(values)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Branch_Timings = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var Monday_Available;
                var Tuesday_Available;
                var Wednesday_Available;
                var Thursday_Available;
                var Friday_Available;
                var Saturday_Available;
                var Sunday_Available;
                var Monday_Timings = {};
                var Tuesday_Timings = {};
                var Wednesday_Timings = {};
                var Thursday_Timings = {};
                var Friday_Timings = {};
                var Saturday_Timings = {};
                var Sunday_Timings = {};

                if (values.Monday_Available == true || values.Monday_Available == "true") {
                    Monday_Available = true;
                    //convert time string to date
                    var a = "01/01/1900 " + values.Monday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Monday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    }
                    Monday_Timings = MT;
                } else {
                    Monday_Available = false;
                }
                if (values.Tuesday_Available == true || values.Tuesday_Available == "true") {
                    Tuesday_Available = true;
                    var a = "01/01/1900 " + values.Tuesday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Tuesday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    }
                    Tuesday_Timings = MT;
                } else {
                    Tuesday_Available = false;
                }
                if (values.Wednesday_Available == true || values.Wednesday_Available == "true") {
                    Wednesday_Available = true;
                    var a = "01/01/1900 " + values.Wednesday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Wednesday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    }
                    Wednesday_Timings = MT;
                } else {
                    Wednesday_Available = false;
                }
                if (values.Thursday_Available == true || values.Thursday_Available == "true") {

                    Thursday_Available = true;
                    var a = "01/01/1900 " + values.Thursday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Thursday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    };
                    Thursday_Timings = MT;
                } else {
                    Thursday_Available = false;
                }
                if (values.Friday_Available == true || values.Friday_Available == "true") {
                    var a = "01/01/1900 " + values.Friday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Friday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    };
                    Friday_Timings = MT;
                    Friday_Available = true;
                } else {
                    Friday_Available = false;
                }
                if (values.Saturday_Available == true || values.Saturday_Available == "true") {
                    var a = "01/01/1900 " + values.Saturday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Saturday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    };
                    Saturday_Timings = MT;
                    Saturday_Available = true;
                } else {
                    Saturday_Available = false;
                }
                if (values.Sunday_Available == true || values.Sunday_Available == "true") {
                    var a = "01/01/1900 " + values.Sunday_Timings[0].From_Time
                    var b = "01/01/1900 " + values.Sunday_Timings[0].To_Time
                    var aDate = new Date(Date.parse(a));
                    var bDate = new Date(Date.parse(b));
                    let MT = {
                        From_Time: aDate,
                        To_Time: bDate,
                    };
                    Sunday_Timings = MT;
                    Sunday_Available = true;
                } else {
                    Sunday_Available = false;
                }
                var date = new Date();
                let query = {
                    BranchID: values.BranchID
                };
                let updateQuery = {
                    Monday_Available: Monday_Available,
                    Monday_Timings: Monday_Timings,
                    Tuesday_Available: Tuesday_Available,
                    Tuesday_Timings: Tuesday_Timings,
                    Wednesday_Available: Wednesday_Available,
                    Wednesday_Timings: Wednesday_Timings,
                    Thursday_Available: Thursday_Available,
                    Thursday_Timings: Thursday_Timings,
                    Friday_Available: Friday_Available,
                    Friday_Timings: Friday_Timings,
                    Saturday_Available: Saturday_Available,
                    Saturday_Timings: Saturday_Timings,
                    Sunday_Available: Sunday_Available,
                    Sunday_Timings: Sunday_Timings,
                    updated_at: date
                };
                let multi = { multi: false };
                Store_Branch.updateOne(query, updateQuery, multi).exec(function (err, Result) {
                    if (Result) {
                        return resolve({
                            success: true,
                            extras: {
                                Status: "Store branch timings updated successfully."
                            }
                        });
                    }
                });
                let SendNotification = await NotificationController.SendNotification_Update_Branch_Timings(values, updateQuery)
            } catch (error) {
                console.log(error)
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_InActive_Store_Branch = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var date = new Date();
                let query = {
                    BranchID: values.BranchID
                };
                let Res = await Store_Branch.findOne(query).lean().exec();
                let updateQuery = {
                    updated_at: date
                };
                let msg = ''
                if (Res.Status == true) {
                    updateQuery.Status = false;
                    msg = "Inactivated successfully."
                } else {
                    updateQuery.Status = true;
                    msg = "Activated successfully."
                }
                let multi = { multi: false };
                Store_Branch.updateOne(query, updateQuery, multi).exec(function (err, Result) {
                    if (Result) {
                        return resolve({
                            success: true,
                            extras: {
                                Status: msg
                            }
                        });
                    }
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Admin_Change_Password = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AdminID: values.AdminID
                };
                var date = new Date();
                var multiplicity = { upsert: true };
                var CustomerData = await Admin_User.findOne(query).exec();

                var Salt = CustomerData.PasswordSalt;
                var Old_Password = values.Old_Password + Salt;
                var newpass = values.New_Password + Salt;
                var ExistPasswordHash = CustomerData.PasswordHash;
                var extpassword = crypto.createHash('sha512').update(Old_Password).digest("hex")
                var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");
                var changes = {
                    PasswordHash: NewPasswordHash,
                    updated_at: date
                };

                if (ExistPasswordHash === extpassword) {
                    await Admin_User.updateOne(query, changes, multiplicity).exec(function (err, Result) {
                        if (Result) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Status: 'Password changed Successfully'
                                }
                            }));
                        }
                    });
                } else {
                    resolve(new ApiResponce({
                        success: false,
                        extras: {
                            msg: ApiMessages.INVALID_Old_PASSWORD
                        }
                    }));
                }

            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

AdminController.Inactive_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AdminID: values.XAdminID
                };
                var changes = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                };
                await Admin_User.updateOne(query, changes).exec(function (err, Result) {
                    if (Result) {
                        resolve(new ApiResponce({
                            success: true,
                            extras: {
                                Status: "Delected Successfully"
                            }
                        }));
                    }
                });
            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

AdminController.Admin_Update_Password = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AdminID: values.XAdminID
                };
                var date = new Date();
                var CustomerData = await Admin_User.findOne(query).exec();
                var Salt = CustomerData.PasswordSalt;
                var newpass = values.Password + Salt;
                var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");
                var changes = {
                    PasswordHash: NewPasswordHash,
                    updated_at: date
                };
                await Admin_User.updateOne(query, changes).exec(function (err, Result) {
                    if (Result) {
                        resolve(new ApiResponce({
                            success: true,
                            extras: {
                                Status: 'Password Updated Successfully'
                            }
                        }));
                    }
                });
            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}


AdminController.Add_Serviceable_Area = (values, callback) => {

    let SID = uuid.v4();
    let date = new Date();
    let data = {
        ServiceableID: SID,
        AddressID: values.AddressID,
        Address_Content: values.Address_Content,
        Latitude: parseFloat(values.Latitude),
        Longitude: parseFloat(values.Longitude),
        Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
        BranchID: values.BranchID,
        Status: true,
        WHO_CREATED: values.AdminID,
        created_at: date,
        updated_at: date
    };
    Store_Service_Area(data).save((err, Result) => {
        if (err) {
            callback(true, { success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
        } else {
            callback(false, { success: true, extras: { Status: "Serviceable Area Activated Successfully" } });
        }
    })
};

AdminController.List_Serviceable_Area = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {

                let query = {
                    BranchID: values.BranchID,
                    Status: values.Status
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Count = await Store_Service_Area.countDocuments(query).lean().exec();
                var ServiceData = await Store_Service_Area.find(query).sort(sortOptions).select('-_id -__v').lean().skip(toSkip).limit(toLimit).exec();

                resolve({ success: true, extras: { Count: Count, Data: ServiceData } });

            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

AdminController.InActive_Serviceable_Area = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let date = new Date();
                let query = {
                    ServiceableID: values.ServiceableID
                };
                let changes = {
                    $set: {
                        Status: false,
                        update_at: date
                    }
                };

                Store_Service_Area.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Serviceable Area InActivated Successfully" } })
                })
            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
};

/////////////////

AdminController.Create_Zone = (values, coordinates, Zone_Number, City_Zone_Version, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    ZoneID: uuid.v4(),
                    Zone_Number: Zone_Number,
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    City_Zone_Version: City_Zone_Version,
                    Zone_Title: values.Zone_Title,
                    Service_Type: Service_Type,
                    Polygon_Properties: {
                        strokeColor: values.strokeColor,
                        strokeOpacity: values.strokeOpacity,
                        strokeWeight: values.strokeWeight,
                        fillColor: values.fillColor,
                        fillOpacity: values.fillOpacity,
                        draggable: values.draggable,
                        editable: values.editable,
                        visible: values.visible
                    },
                    Polygon_Paths: values.Polygon_Paths,
                    Geometry: {
                        coordinates: coordinates
                    },
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await ZONES(Data).save();
                resolve({ success: true, extras: { Status: "Zone Created Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminController.Edit_Zone = (values, coordinates, City_Zone_Version) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    ZoneID: values.ZoneID
                };
                let fndupdchanges = {
                    $set: {
                        City_Zone_Version: City_Zone_Version,
                        Zone_Title: values.Zone_Title,
                        Polygon_Properties: {
                            strokeColor: values.strokeColor,
                            strokeOpacity: values.strokeOpacity,
                            strokeWeight: values.strokeWeight,
                            fillColor: values.fillColor,
                            fillOpacity: values.fillOpacity,
                            draggable: values.draggable,
                            editable: values.editable,
                            visible: values.visible
                        },
                        Polygon_Paths: values.Polygon_Paths,
                        "Geometry.coordinates": coordinates,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let ZoneData = await ZONES.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve(ZoneData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Delete_Zone = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ZoneID: values.ZoneID,
                }
                let Result = await ZONES.find(query).lean().exec();
                if (Result != null) {
                    let queryx = {
                        ZoneID: values.ZoneID,
                        Status: true
                    }
                    let Resultx = await ZONES.find(queryx).lean().exec();
                    if (Resultx != null) {
                        let changesx = {
                            Status: false,
                            updated_at: new Date()
                        }
                        let UpdateData = await ZONES.updateOne(queryx, changesx).lean().exec();
                        resolve({ success: true, extras: { Status: "Deleted Successfully" } })
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ZONE_ALREADY_DELETED } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ZONE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_City_Zones_Lite = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Zone_Number: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Service_Type: parseInt(values.Service_Type),
                    Status: true
                };
                let Result = await ZONES.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings -Polygon_Properties -Polygon_Paths').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Zone_Hub = (values, City_Zone_Hub_Version, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude) || 0;
                let Longitude = parseFloat(values.Longitude) || 0;
                let Point = [
                    Longitude,
                    Latitude
                ];
                let Location = new Object();
                Location.Address = values.Address;
                Location.Latitude = Latitude;
                Location.Longitude = Longitude;
                let Dimensions = new Object();
                Dimensions.Dimensions_Type = parseInt(values.Dimensions_Type) || 0;
                Dimensions.Length = parseFloat(values.Length) || 0;
                Dimensions.Breadth = parseFloat(values.Breadth) || 0;
                Dimensions.Height = parseFloat(values.Height) || 0;
                let Data = {
                    Zone_Hub_ID: uuid.v4(),
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    City_Zone_Hub_Version: City_Zone_Hub_Version,
                    ZoneID: values.ZoneID,
                    Zone_Hub_Name: values.Zone_Hub_Name,
                    Service_Type: Service_Type,
                    Location: Location,
                    Point: Point,
                    Dimensions: Dimensions,
                    Weight_Type: parseInt(values.Weight_Type) || 0,
                    Maximum_Weight: parseFloat(values.Maximum_Weight) || 0,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Zone_Hubs(Data).save();
                resolve({ success: true, extras: { Status: "Zone Hub Created Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Update_Zone_Hob_Move_All_Exist_City_Zone_Hubs_Archive = (CityID, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let All_Zone_Hubs_Data = await Zone_Hubs.find(query).select('-_id -__v').lean().exec();
                let SaveResult = await Zone_Hubs_Archive.insertMany(All_Zone_Hubs_Data);
                resolve("All Current Zone Hubs are archived");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Zone_Hub_Update_All_City_Zone_Hubs_Version = (CityID, City_Zone_Hub_Version, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let changes = {
                    $set: {
                        City_Zone_Hub_Version: City_Zone_Hub_Version,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Zone_Hubs.updateMany(query, changes).lean();
                resolve("Update Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Zones = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Zone_Number: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    Service_Type: parseInt(values.Service_Type),
                    Status: true
                };
                let Result = await ZONES.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_City_Zones = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Zone_Number: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Service_Type: parseInt(values.Service_Type),
                    Status: true
                };
                let Result = await ZONES.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Validate_Zone_Path_and_Get_Geometry = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = [];
                var CoordinatesArray = [];
                if (values.Polygon_Paths.length >= 4) {
                    async.eachSeries(values.Polygon_Paths, (item, callback) => {
                        let PointArray = [];
                        if (
                            item.lat != null && isFinite(item.lat)
                            && item.lng != null && isFinite(item.lng)
                        ) {
                            PointArray.push(item.lng);
                            PointArray.push(item.lat);
                            CoordinatesArray.push(PointArray);
                            callback();
                        } else {
                            callback({ success: false, extras: { msg: ApiMessages.SUBMIT_CORRECT_LATITUDE_AND_LONGITUDES } });
                        }
                    }, (error) => {
                        if (error) reject(error);
                        var PointArray = [];
                        PointArray.push(values.Polygon_Paths[0].lng);
                        PointArray.push(values.Polygon_Paths[0].lat);
                        CoordinatesArray.push(PointArray);
                        Data.push(CoordinatesArray);
                        resolve(Data);
                    })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.POLYGON_PATH_MUST_HAVE_ATLEAST_FOUR_PATHS } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Zone_Update_All_City_Zones_Version = (CityID, City_Zone_Version, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let changes = {
                    $set: {
                        City_Zone_Version: City_Zone_Version,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await ZONES.updateMany(query, changes).lean();
                resolve("Update Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Update_Zone_Move_All_Exist_City_Zones_Archive = (CityID, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let All_Zones_Data = await ZONES.find(query).select('-_id -__v').lean().exec();
                let SaveResult = await ZONES_Archive.insertMany(All_Zones_Data);
                resolve("All Current Zones are archived");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Zone_Hubs = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Zone_Hub_Name: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    Status: true
                };
                let Result = await Zone_Hubs.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_City_JSON = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                resolve({ success: true, extras: { Data: City_JSON.filter(item => item.country == values.CountryCode) } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Country_JSON = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                resolve({ success: true, extras: { Data: Country_JSON } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_Country_Name_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryName: values.CountryName
                };
                let Result = await Country.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.NAME_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_Country_Number_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.CountryID = (values.CountryID == null) ? "" : values.CountryID;
                let query = {
                    CountryID: {
                        $ne: values.CountryID
                    },
                    CountryNumber: parseInt(values.CountryNumber) || 0
                };
                let Result = await Country.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Country = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    CountryID: uuid.v4(),
                    CountryNumber: parseInt(values.CountryNumber) || 0,
                    CountryName: values.CountryName,
                    CountryCode: values.CountryCode,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Country(Data).save();
                resolve({ success: true, extras: { Status: "Added Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Common_Whether_Status_Filter = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Boolify(values.Whether_Status_Filter)) {
                    if (
                        values.Status != null && values.Status != undefined && isBoolean(values.Status)
                    ) {
                        resolve("Validated Successfully");
                    } else {
                        throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                    }
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Countries_with_Filter = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                };
                let sortOptions = {
                    CountryName: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Data = await Country.find(query).select('-_id -__v  -Point').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Inactivate_Country = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                CountryID: values.CountryID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Country.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Activate_Country = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                CountryID: values.CountryID
            };
            let changes = {
                $set: {
                    Status: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Country.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Activated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Activate_State_Information = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                StateID: values.StateID
            };
            let changes = {
                $set: {
                    Status: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await State.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Activated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Inactivate_State_Information = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                StateID: values.StateID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await State.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Check_Whether_State_Number_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.StateID = (values.StateID == null) ? "" : values.StateID;
                let query = {
                    StateID: {
                        $ne: values.StateID
                    },
                    CountryID: values.CountryID,
                    StateNumber: parseInt(values.StateNumber) || 0
                };
                let Result = await State.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_State_Name_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.StateID = (values.StateID == null) ? "" : values.StateID;
                let query = {
                    StateID: {
                        $ne: values.StateID
                    },
                    CountryID: values.CountryID,
                    StateName: values.StateName
                };
                let Result = await State.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.NAME_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_State_Information = (values, CountryData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Latitude = parseFloat(values.Latitude);
            let Longitude = parseFloat(values.Longitude);
            let Point = [
                Longitude,
                Latitude
            ];
            let query = {
                StateID: values.StateID
            };
            let changes = {
                $set: {
                    StateNumber: parseInt(values.StateNumber),
                    StateName: values.StateName,
                    Location: {
                        Latitude: Latitude,
                        Longitude: Longitude
                    },
                    Point: Point,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await State.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_States_with_Filter = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryID: values.CountryID
                };
                let toSkip = Math.abs(parseInt(values.skip)) || 0;
                let toLimit = parseInt(values.limit) || 0;
                let sortOptions = {
                    StateName: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Data = await State.find(query).select('-_id -__v  -Point').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_State = (values, CountryData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude) || 0;
                let Longitude = parseFloat(values.Longitude) || 0;
                let Point = [
                    Longitude,
                    Latitude
                ];
                let Data = {
                    StateID: uuid.v4(),
                    CountryID: values.CountryID,
                    StateNumber: parseInt(values.StateNumber) || 0,
                    StateName: values.StateName,
                    CountryName: CountryData.CountryName,
                    Location: {
                        Latitude: Latitude,
                        Longitude: Longitude
                    },
                    Point: Point,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await State(Data).save();
                resolve({ success: true, extras: { Status: "Added Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Activate_City = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                CityID: values.CityID
            };
            let changes = {
                $set: {
                    Status: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await City.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Activated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Inactivate_City = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                CityID: values.CityID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await City.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Cities = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let CountryID_Array = await Country.distinct("CountryID", { Status: true }).lean();
                let query = {
                    CountryID: {
                        $in: CountryID_Array
                    },
                    Status: true
                };
                let toSkip = Math.abs(parseInt(values.skip)) || 0;
                let toLimit = parseInt(values.limit) || 0;
                let sortOptions = {
                    CityName: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Data = await City.find(query).select('-_id -__v  -Point').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Country_Cities = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryID: values.CountryID
                };
                if (parseInt(values.Type) == 1) {
                    let queryx = {
                        CountryID: values.CountryID,
                        Status: true
                    }
                    let CityID_Array = await State_Cities.distinct('CityID', queryx).lean()
                    query.CityID = {
                        $nin: CityID_Array,
                    }
                }
                let toSkip = Math.abs(parseInt(values.skip)) || 0;
                let toLimit = parseInt(values.limit) || 0;
                let sortOptions = {
                    CityName: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Data = await City.find(query).select('-_id -__v  -Point').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_City_Number_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.CityID = (values.CityID == null) ? "" : values.CityID;
                let query = {
                    CityID: {
                        $ne: values.CityID
                    },
                    CountryID: values.CountryID,
                    CityNumber: parseInt(values.CityNumber) || 0
                };
                let Result = await City.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_Whether_City_Name_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.CityID = (values.CityID == null) ? "" : values.CityID;
                let query = {
                    CityID: {
                        $ne: values.CityID
                    },
                    CountryID: values.CountryID,
                    CityName: values.CityName
                };
                let Result = await City.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.NAME_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_City = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Latitude = parseFloat(values.Latitude) || 0;
            let Longitude = parseFloat(values.Longitude) || 0;
            let Point = [
                Longitude,
                Latitude
            ];
            let query = {
                CityID: values.CityID
            };
            let changes = {
                $set: {
                    CityNumber: parseInt(values.CityNumber) || 0,
                    CityName: values.CityName,
                    Location: {
                        Latitude: Latitude,
                        Longitude: Longitude
                    },
                    Point: Point,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await City.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Add_City = (values, CountryData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude) || 0;
                let Longitude = parseFloat(values.Longitude) || 0;
                let Point = [
                    Longitude,
                    Latitude
                ];
                let Data = {
                    CityID: uuid.v4(),
                    CountryID: values.CountryID,
                    CityNumber: parseInt(values.CityNumber) || 0,
                    CityName: values.CityName,
                    CountryName: CountryData.CountryName,
                    Location: {
                        Latitude: Latitude,
                        Longitude: Longitude
                    },
                    Point: Point,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await City(Data).save();
                resolve({ success: true, extras: { Status: "Added Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_State_Cities = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true,
                    StateID: values.StateID
                };
                let CityID_Array = await State_Cities.distinct('CityID', query).lean();
                let cquery = {
                    Status: true,
                    CityID: {
                        $in: CityID_Array
                    }
                };
                let toSkip = Math.abs(parseInt(values.skip)) || 0;
                let toLimit = parseInt(values.limit) || 0;
                let sortOptions = {
                    CityName: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Data = await City.find(cquery).select('-_id -__v  -Point').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Link_States_City = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Data = {
                StateID: values.StateID,
                CountryID: values.CountryID,
                CityID: values.CityID,
                created_at: new Date(),
                updated_at: new Date()
            };
            let SaveResult = await State_Cities(Data).save();
            resolve({ success: true, extras: { Status: "Linked Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Check_Whether_State_City_Validity = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                CityID: values.CityID
            };
            let Result = await State_Cities.findOne(query).select('-_id -__v').lean();
            if (Result == null) {
                resolve("Validated Successfully");
            } else {
                reject({ success: false, extras: { msg: ApiMessages.CITY_ALREADY_LINKED } })
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


////////////////////////////////////////////////////////////////////////////////////////////

AdminController.Store_Branch = (values, StoreData) => {
    return new Promise(async (resolve, reject) => {
        try {
            var Branch_Name = String(values.Branch_Name);
            Branch_Name = Branch_Name.replace(/\s\s+/g, ' ');
            Branch_Name = Branch_Name.replace(/  +/g, ' ');
            Branch_Name = Branch_Name.replace(/^ /, '');
            Branch_Name = Branch_Name.replace(/\s\s*$/, '');
            Branch_Name = format_str(Branch_Name);
            if (values.AdminData.length > 0) {
                CommonController.Check_Whether_Store_Branch_Name_Already_Exists(Branch_Name, function (err, BranchStaus) {
                    if (err) {
                        resolve(BranchStaus);
                    } else {
                        CommonController.Validate_Store_Branch_URL(values.BranchURL, (err, MasterData) => {
                            if (err) {
                                resolve(MasterData);
                            } else {
                                AdminController.Add_Entity_Branch(values, Branch_Name, StoreData, function (Result, BranchData) {
                                    resolve(Result);
                                    AdminController.Add_Update_RazorpayX_Branch_Contact(BranchData).then((RazorpayXContactStoring) => {
                                        async.eachSeries(values.AdminData, function (item, callback) {
                                            // console.log(item);
                                            var Name = item.Name;
                                            var PhoneNumber = item.PhoneNumber;
                                            var EmailID = item.EmailID;
                                            var Password = item.Password;
                                            AdminMediator.Check_For_Admin_Phone_And_Update_IF_Exists(Name, PhoneNumber, EmailID, BranchData, function (err, AdminValidityStatus) {
                                                if (err) {
                                                    res.send(AdminValidityStatus);
                                                } else {
                                                    CommonController.Find_and_Update_CustomerSeqID(function (err, SequenceNumber) {
                                                        if (!err) {
                                                            AdminController.Create_Admin_Users(Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, function (err, AdminStatus) {
                                                                console.log("Admin Created and Message Sent");
                                                                callback();
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                            // CommonController.Check_For_Admin_Phone_Branch(Password, Name, PhoneNumber, EmailID, BranchData, function (err, AdminValidityStatus) {
                                            //     if (err) {
                                            //         console.log("Admin Operations finishesd");
                                            //         callback();
                                            //     } else {
                                            //         CommonController.Find_and_Update_CustomerSeqID(function (err, SequenceNumber) {
                                            //             if (!err) {
                                            //                 console.log(1)
                                            //                 AdminController.Create_Admin_Users(Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, function (err, AdminStatus) {
                                            //                     console.log("Admin Created and Message Sent");
                                            //                     callback();
                                            //                 })
                                            //             }
                                            //         })
                                            //     }
                                            // })
                                        }, function (err) {
                                            if (!err) {
                                                console.log("All Admin Operations Completed");
                                            } else {
                                                console.log(err);
                                            }
                                        })
                                    })
                                })

                            }
                        })
                    }
                });
            } else if (values.AdminData.length <= 0) {
                res.send(new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.DETAILS_ALREADY_EXISTS
                    }
                }));
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Store_Branch_With_OTP = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            var Branch_Name = String(values.Branch_Name);
            Branch_Name = Branch_Name.replace(/\s\s+/g, ' ');
            Branch_Name = Branch_Name.replace(/  +/g, ' ');
            Branch_Name = Branch_Name.replace(/^ /, '');
            Branch_Name = Branch_Name.replace(/\s\s*$/, '');
            Branch_Name = format_str(Branch_Name);
            if (values.AdminData.length > 0) {
                CommonController.Check_Whether_Store_Branch_Name_Already_Exists(Branch_Name, function (err, BranchStaus) {
                    if (err) {
                        resolve(BranchStaus);
                    } else {
                        CommonController.Validate_Store_Branch_URL(values.BranchURL, (err, MasterData) => {
                            if (err) {
                                resolve(MasterData);
                            } else {
                                let OTP = 0;
                                CommonController.Random_OTP_Number().then((OTPX) => {
                                    OTP = OTPX

                                    var StoreOTPID = uuid.v4()
                                    var BranchData = new Store_Branch_OTP({
                                        StoreOTPID: StoreOTPID,
                                        Branch_Name: Branch_Name,
                                        Branch_PhoneNumber: values.Branch_PhoneNumber,
                                        BranchURL: values.BranchURL,
                                        Website: values.Website,
                                        StoreID: values.StoreID,
                                        Store_Entity_Name: values.Store_Entity_Name,
                                        Description: values.Description,
                                        ImageID: values.ImageID,
                                        FileID: values.FileID,
                                        Address: values.Address,
                                        Latitude: parseFloat(values.Latitude),
                                        Longitude: parseFloat(values.Longitude),
                                        Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
                                        Monday_Available: values.Monday_Available,
                                        Monday_Timings: values.Monday_Timings,
                                        Tuesday_Available: values.Tuesday_Available,
                                        Tuesday_Timings: values.Tuesday_Timings,
                                        Wednesday_Available: values.Wednesday_Available,
                                        Wednesday_Timings: values.Wednesday_Timings,
                                        Thursday_Available: values.Thursday_Available,
                                        Thursday_Timings: values.Thursday_Timings,
                                        Friday_Available: values.Friday_Available,
                                        Friday_Timings: values.Friday_Timings,
                                        Saturday_Available: values.Saturday_Available,
                                        Saturday_Timings: values.Saturday_Timings,
                                        Sunday_Available: values.Sunday_Available,
                                        Sunday_Timings: values.Sunday_Timings,
                                        AdminData: values.AdminData,
                                        CountryID: values.CountryID,
                                        Bookstore_Type: values.Bookstore_Type,
                                        CityID: values.CityID,
                                        ZoneID: values.ZoneID,
                                        OTP: OTP,
                                        created_at: new Date(),
                                        updated_at: new Date()
                                    });
                                    BranchData.save(function (err, Result) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            let PhoneNumber = `${+91}${values.AdminData[0].PhoneNumber}`;
                                            let MsgData = 'Welcome To Bookafella Store. OTP for Verification is ' + OTP + ' Please Share OTP with Bookafella Team to Complete Verification and Create Bookstore.  \n --Thank you,  \n Bookafella Team'
                                            MessagesController.Send_OTP_TO_Mobile(PhoneNumber, MsgData).then((OTPStatus) => {
                                                resolve({
                                                    success: true,
                                                    extras: {
                                                        Status: 'OTP Sent Successfully',
                                                        Data: {
                                                            StoreOTPID: StoreOTPID,
                                                            PhoneNumber: values.AdminData[0].PhoneNumber
                                                        }
                                                    }
                                                })
                                            });
                                        }
                                    });
                                });
                            }
                        })
                    }
                });
            } else if (values.AdminData.length <= 0) {
                res.send(new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.DETAILS_ALREADY_EXISTS
                    }
                }));
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Check_for_Store_OTP_Tries_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    StoreOTPID: values.StoreOTPID,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await Store_OTP_Tries.countDocuments(query).lean().exec();
                if (Count <= config.OTP_TRIES_COUNT) {
                    resolve('Validated Successfully');
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_TRIES_EXCEED_TRY_AFTER_SOME_TIME } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Entity_Branch = function (values, Branch_Name, EntityData, callback) {

    var Monday_Available;
    var Tuesday_Available;
    var Wednesday_Available;
    var Thursday_Available;
    var Friday_Available;
    var Saturday_Available;
    var Sunday_Available;
    var Monday_Timings = {};
    var Tuesday_Timings = {};
    var Wednesday_Timings = {};
    var Thursday_Timings = {};
    var Friday_Timings = {};
    var Saturday_Timings = {};
    var Sunday_Timings = {};

    if (values.Monday_Available == true || values.Monday_Available == "true") {
        Monday_Available = true;
        //convert time string to date
        var a = "01/01/1900 " + values.Monday_Timings[0].From_Time
        b = "01/01/1900 " + values.Monday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        }
        Monday_Timings = MT;
    } else {
        Monday_Available = false;
    }
    if (values.Tuesday_Available == true || values.Tuesday_Available == "true") {
        Tuesday_Available = true;
        var a = "01/01/1900 " + values.Tuesday_Timings[0].From_Time
        b = "01/01/1900 " + values.Tuesday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        }
        Tuesday_Timings = MT;
    } else {
        Tuesday_Available = false;
    }
    if (values.Wednesday_Available == true || values.Wednesday_Available == "true") {
        Wednesday_Available = true;
        var a = "01/01/1900 " + values.Wednesday_Timings[0].From_Time
        b = "01/01/1900 " + values.Wednesday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        }
        Wednesday_Timings = MT;
    } else {
        Wednesday_Available = false;
    }
    if (values.Thursday_Available == true || values.Thursday_Available == "true") {
        Thursday_Available = true;
        var a = "01/01/1900 " + values.Thursday_Timings[0].From_Time
        var b = "01/01/1900 " + values.Thursday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        };
        Thursday_Timings = MT;
    } else {
        Thursday_Available = false;
    }
    if (values.Friday_Available == true || values.Friday_Available == "true") {
        var a = "01/01/1900 " + values.Friday_Timings[0].From_Time
        var b = "01/01/1900 " + values.Friday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        };
        Friday_Timings = MT;
        Friday_Available = true;
    } else {
        Friday_Available = false;
    }
    if (values.Saturday_Available == true || values.Saturday_Available == "true") {
        var a = "01/01/1900 " + values.Saturday_Timings[0].From_Time
        var b = "01/01/1900 " + values.Saturday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        };
        Saturday_Timings = MT;
        Saturday_Available = true;
    } else {
        Saturday_Available = false;
    }
    if (values.Sunday_Available == true || values.Sunday_Available == "true") {
        var a = "01/01/1900 " + values.Sunday_Timings[0].From_Time
        var b = "01/01/1900 " + values.Sunday_Timings[0].To_Time
        var aDate = new Date(Date.parse(a));
        var bDate = new Date(Date.parse(b));
        let MT = {
            From_Time: aDate,
            To_Time: bDate,
        };
        Sunday_Timings = MT;
        Sunday_Available = true;
    } else {
        Sunday_Available = false;
    }
    var BranchID = uuid();
    var date = new Date();
    var BranchData = new Store_Branch({
        EntityID: EntityData.EntityID,
        BranchID: BranchID,
        StoreID: values.StoreID,
        Store_Entity_Name: EntityData.Store_Entity_Name,
        Branch_Name: Branch_Name,
        Branch_PhoneNumber: values.Branch_PhoneNumber,
        BranchURL: values.BranchURL,
        Description: values.Description,
        ImageID: values.ImageID,
        FileID: values.FileID,
        Address: values.Address,
        Latitude: parseFloat(values.Latitude),
        Longitude: parseFloat(values.Longitude),
        Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
        Monday_Available: Monday_Available,
        Monday_Timings: Monday_Timings,
        Tuesday_Available: Tuesday_Available,
        Tuesday_Timings: Tuesday_Timings,
        Wednesday_Available: Wednesday_Available,
        Wednesday_Timings: Wednesday_Timings,
        Thursday_Available: Thursday_Available,
        Thursday_Timings: Thursday_Timings,
        Friday_Available: Friday_Available,
        Friday_Timings: Friday_Timings,
        Saturday_Available: Saturday_Available,
        Saturday_Timings: Saturday_Timings,
        Sunday_Available: Sunday_Available,
        Sunday_Timings: Sunday_Timings,
        AdminData: [],
        CountryID: values.CountryID,
        Bookstore_Type: values.Bookstore_Type,
        CityID: values.CityID,
        ZoneID: values.ZoneID,
        created_at: date,
        updated_at: date
    });

    BranchData.save(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            return callback(new ApiResponce({
                success: true,
                extras: {
                    Status: 'Store Branch Added Successfully'
                }
            }), JSON.parse(JSON.stringify(Result)));
        }
    });
};

AdminController.Add_Update_RazorpayX_Branch_Contact = (BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let RazorpayXContactData = await RazorpayController.Create_Razorpay_Branch_Contact(BranchData);
                let query = {
                    BranchID: BranchData.BranchID
                };
                let changes = {
                    $set: {
                        Whether_RazorpayX_Branch_Register: true,
                        RazorpayX_ContactID: RazorpayXContactData.id,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Store_Branch.updateOne(query, changes).lean();
                resolve("Updated Successfully");
            } catch (error) {
                console.log(error)
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.Create_Admin_Users = function (Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, callback) {
    var salt = rand(160, 36);
    // console.log("Password " + Password);
    var pass = Password + salt;
    var date = CommonController.DateTime();
    var BranchDetails = {
        BranchID: BranchData.BranchID,
        Branch_Name: BranchData.Branch_Name
    }
    var signup_date = new Date();
    var moment = require('moment');
    var format = 'H:mm:ss';
    var time = moment().utcOffset(330).format(format);
    var timearray = time.split(':');
    var hour = parseInt(timearray[0]);
    var minute = parseInt(timearray[1]);
    var interval;
    if (minute == 0) {
        interval = hour;
    } else {
        interval = hour + 1;
    }
    var AdminData = new Customers({
        CustomerID: uuid(),
        acc_status: 1,
        customerseqId: SequenceNumber,
        First_name: Name,
        Phone: PhoneNumber,
        countryCode: "+91",
        Email: EmailID,
        Verify: 0,
        Code: 0,
        PasswordHash: crypto.createHash('sha512').update(pass).digest("hex"),
        PasswordSalt: salt,
        sessionToken: '',
        First_Time_Login: true,
        CurrentStatus: 1,
        terms_cond: 1,
        Created_dt: date,
        Agreement_Time: date,
        Whether_Store_Admin: true,
        BranchData: BranchDetails,
        Active_BranchID_Exist: true,
        Active_BranchID: BranchData.BranchID,
        StoreAdminStatus: true,
        Signup_Date: signup_date,
        Signup_Interval: interval
    });
    AdminData.save(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            // var URL = 'https://';
            // var Message = 'Hi ' + BranchData.Branch_Name + ', Your UserName:' + PhoneNumber + ' ,Password: ' + Password + ' ,use this Credential and login at ' + URL;
            // MSG91MOD.sendsmstocustomer(PhoneNumber, Message, function (err, msgStatus) {
            console.log("Admin Creadted Branch Stored and Message Sent Successfully");
            callback(false, 'Admin Created Successfully and Message Sent Successfully');
            var newquery = {
                BranchID: BranchData.BranchID
            };
            var newchanges = {
                $push: {
                    AdminData: {
                        StoreAdminID: Result.CustomerID
                    }
                }
            };
            var Message = "OTP is : " + Password;
            // MSG91MOD.sendsms(PhoneNumber, Message, function (err, msg) {

            // });

            // console.log("New Query ");
            // console.log(newchanges);
            var newmultiplicity = {
                multi: false
            };
            Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Admin Updated in Branch");
                }
            })
            // })
        }
    })
}

function format_str(str) {
    var myArr = str.toLowerCase().split(" ");
    for (var a = 0; a < myArr.length; a++) {
        myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
    }
    return myArr.join(" ");
}


AdminController.Add_Vehicle = (values, AdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Dimensions = new Object();
                Dimensions.Dimensions_Type = parseInt(values.Dimensions_Type) || 0;
                Dimensions.Length = parseFloat(values.Length) || 0;
                Dimensions.Breadth = parseFloat(values.Breadth) || 0;
                Dimensions.Height = parseFloat(values.Height) || 0;
                let Data = {
                    VehicleID: uuid.v4(),
                    CountryID: values.CountryID,
                    Vehicle_Name: values.Vehicle_Name,
                    Vehicle_Description: values.Vehicle_Description,
                    Vehicle_Type: parseInt(values.Vehicle_Type) || 0,
                    Weight_Type: parseInt(values.Weight_Type) || 0,
                    Maximum_Weight: parseFloat(values.Maximum_Weight) || 0,
                    Dimensions: Dimensions,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Vehicles(Data).save();
                resolve({ success: true, extras: { Status: "Vehicle Added Successfully" } });
                let StoreLog = await AdminLogController1.Add_Vehicle_Log(AdminData, JSON.parse(JSON.stringify(SaveResult)));
            } catch (error) {
                console.error(error);
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Vehicles = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Vehicle_Name: 1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    CountryID: values.CountryID
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Result = await Vehicles.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings').sort(sortOptions).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Vehicle = (values, AdminData, VehicleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Dimensions = new Object();
                Dimensions.Dimensions_Type = parseInt(values.Dimensions_Type) || 0;
                Dimensions.Length = parseFloat(values.Length) || 0;
                Dimensions.Breadth = parseFloat(values.Breadth) || 0;
                Dimensions.Height = parseFloat(values.Height) || 0;
                let fndupdquery = {
                    VehicleID: values.VehicleID
                };
                let fndupdchanges = {
                    $set: {
                        Vehicle_Name: values.Vehicle_Name,
                        Vehicle_Description: values.Vehicle_Description,
                        Vehicle_Type: parseInt(values.Vehicle_Type) || 0,
                        Weight_Type: parseInt(values.Weight_Type) || 0,
                        Maximum_Weight: parseFloat(values.Maximum_Weight) || 0,
                        Dimensions: Dimensions,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let NewVehicleData = await Vehicles.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Vehicle Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Vehicle_Log(AdminData, VehicleData, NewVehicleData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Activate_Vehicle = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                VehicleID: values.VehicleID
            };
            let changes = {
                $set: {
                    Status: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Vehicles.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Activated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Inactivate_Vehicle = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                VehicleID: values.VehicleID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Vehicles.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Common_Validate_CategoryID_Array = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = [];
                if (Boolify(values.Whether_All_Categories_Delivery)) {
                    resolve(Data);
                } else {
                    if (values.CategoryID_Array == null || values.CategoryID_Array == undefined) {
                        reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                    } else {
                        if (values.CategoryID_Array.length > 0) {
                            async.eachSeries(values.CategoryID_Array, async (CategoryID, callback) => {
                                try {
                                    if (CategoryID == null || CategoryID == undefined || CategoryID == '') {
                                        throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
                                    } else {
                                        let CategoryData = await CommonController.Check_for_Item_Category({ CategoryID: CategoryID });
                                        Data.push(CategoryData);
                                        callback();
                                    }
                                } catch (error) {
                                    callback(error);
                                }
                            }, async (err) => {
                                if (err) reject(err);
                                resolve(Data);
                            });
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.PLEASE_SUBMIT_ATLEAST_ONE_CATEGORY } })
                        }
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Common_Whether_Search_Filter = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Boolify(values.Whether_Search_Filter)) {
                    if (
                        values.SearchValue != null && values.SearchValue != undefined && values.SearchValue != ""
                    ) {
                        resolve("Validated Successfully");
                    } else {
                        throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                    }
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Pending_Products = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                $or: [
                    { Product_Approve: 0 },
                    { 'Product_Data_For_Branch.Approve': 0 }
                ]
            };
            let Count = await Store_Products.countDocuments(query).lean().exec();
            let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let Data = [];
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                    item.Product_Image_Data = Product_Image_Data;
                    Data.push(item);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Data } })
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Approve_Product = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                ProductID: values.ProductID
            }
            if (values.Product_Approve == 1) {
                let changes = {
                    $set: {
                        Product_Approve: values.Product_Approve,
                        Who_Approved: values.AdminID,
                        updated_at: new Date()
                    }
                }
                let updateData = await Store_Products.updateOne(query, changes).lean().exec();
            } else {
                //apply Same for all (edit needed)
                let changes = {
                    $set: {
                        'Product_Data_For_Branch.$[].Approve': values.Product_Approve,
                        'Product_Data_For_Branch.$[].updated_at': new Date(),
                        Product_Approve: values.Product_Approve,
                        Who_Approved: values.AdminID,
                        updated_at: new Date()
                    }
                }
                let updateData = await Store_Products.updateOne(query, changes).lean().exec();
            }

            resolve({ success: true, extras: { Status: "Approve Status Updated Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Approve_Branch_Product = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                ProductID: values.ProductID,
                'Product_Data_For_Branch.BranchID': values.BranchID
            }
            let changes = {
                $set: {
                    'Product_Data_For_Branch.$.Approve': values.Approve,
                    'Product_Data_For_Branch.$.updated_at': new Date()
                }
            }
            let updateData = await Store_Products.updateOne(query, changes).lean().exec();
            resolve({ success: true, extras: { Status: "Approve Status Updated Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Approved_Products = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                $and: [
                    { Product_Approve: 1 },
                    { 'Product_Data_For_Branch.Approve': 1 }
                ]
            };
            let Count = await Store_Products.countDocuments(query).lean().exec();
            let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let Data = [];
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                    item.Product_Image_Data = Product_Image_Data;
                    let PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                        return ResultTemp.Approve == 1;
                    });
                    item.Product_Data_For_Branch = PDatax
                    Data.push(item);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Data } })
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Category_Products = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                Whether_Deleted: {
                    $ne: true
                }
                // $and: [
                //     { Product_Approve: 1 },
                //     { 'Product_Data_For_Branch.Approve': 1 }
                // ]
            };
            if (values.CategoryID != '') {
                query.CategoryID = values.CategoryID
            }
            if (values.SubCategoryID != '') {
                query.SubCategoryID = values.SubCategoryID
            }
            let Count = await Store_Products.countDocuments(query).lean().exec();
            let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let Data = [];
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                    item.Product_Image_Data = Product_Image_Data;
                    let PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                        // return ResultTemp.Approve == 1;
                        // if (ResultTemp.Approve == 1) {
                        if (ResultTemp.Used_Quantity == undefined) {
                            ResultTemp.Used_Quantity = 0;
                            ResultTemp.Total_Quantity = 0
                        }
                        return ResultTemp
                        // }
                    });
                    item.Product_Data_For_Branch = PDatax
                    Data.push(item);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Data } })
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Declined_Products = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                $or: [
                    { Product_Approve: 2 },
                    { 'Product_Data_For_Branch.Approve': 2 }
                ]
            };
            let Count = await Store_Products.countDocuments(query).lean().exec();
            let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let Data = [];
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                    item.Product_Image_Data = Product_Image_Data;

                    let PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                        return ResultTemp.Approve == 2;
                    });
                    item.Product_Data_For_Branch = PDatax
                    Data.push(item);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Data } })
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Blocked_Products = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                created_at: 1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                'Product_Data_For_Branch.Approve': 3
            };
            let Count = await Store_Products.countDocuments(query).lean().exec();
            let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
            let Data = [];
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let Product_Image_Data = await CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data);
                    item.Product_Image_Data = Product_Image_Data;

                    let PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                        return ResultTemp.Approve == 3;
                    });
                    item.Product_Data_For_Branch = PDatax
                    Data.push(item);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Count: Count, Data: Data } })
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.List_All_Help_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                };
                let sortOptions = {
                    SNo: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Help_Data.countDocuments(query).lean().exec();
                let Result = await Help_Data.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_Inactive_Help = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    HelpDataID: values.HelpDataID
                }
                let HelpResult = await Help_Data.findOne(query).lean().exec()
                let statys;
                if (HelpResult.Status) {
                    statys = "Inactivated Successfully";
                    let changes = {
                        $set: {
                            Status: false,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Help_Data.updateOne(query, changes).lean().exec();
                } else {
                    statys = "Activated Successfully"
                    let changes = {
                        $set: {
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Help_Data.updateOne(query, changes).lean().exec();
                }
                resolve({ success: true, extras: { Status: statys } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Help = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    HelpDataID: values.HelpDataID,
                }
                let HelpData = await Help_Data.findOne(query).lean().exec();
                if (HelpData != null) {
                    let changes = {
                        $set: {
                            Title: values.Title,
                            Description: values.Description,
                            SNo: values.SNo,
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Help_Data.updateOne(query, changes).lean().exec();
                    resolve({ success: true, extras: { Status: "Updated successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_HELP } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Help = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    HelpDataID: uuid.v4(),
                    Title: values.Title,
                    SNo: values.SNo,
                    Description: values.Description,
                    Status: true,
                    created_at: new Date(),
                    updated_at: new Date()
                }
                let ResultStatus = await Help_Data(Data).save();
                resolve({ success: true, extras: { Status: "Created successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Referal_Price_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Cfndupdquery = {

                }
                let Cfndupdchanges = {
                    $set: {
                        First_Time_Login: values.First_Time_Login,
                        First_Time_Purchase: values.First_Time_Purchase
                    }
                }
                let Cfndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let CfindupdateData = await Referal_Price.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Referal_Price_Data = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Result = await Referal_Price.findOne({}).select('-_id -__v').lean().exec();
                resolve({ success: true, extras: { Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Refund = (values, Cancel_Calculation) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID,
                }
                let changes = {
                    $set: {
                        Whether_Cancellation_Refund_Prodessed: true,
                        Whether_Cancellation_Charges_Applied: Cancel_Calculation.Whether_Cancellation_Charges_Applied,
                        Whether_Cancellation_Charges_For_Next_Order: Cancel_Calculation.Whether_Cancellation_Charges_For_Next_Order,
                        Cancellation_Charges: Cancel_Calculation.Cancellation_Amount,
                        Refund_Amount: Cancel_Calculation.Refund_Amount,
                        updated_at: new Date()
                    }
                }
                if (values.Message != "") {
                    let Today = new Date()
                    let Data = {
                        Message: values.Message,
                        Date: Today,
                        Time: moment(Today, config.Common_Time_Format).toString(),
                        Resend: false,
                        Whether_Edited: false,
                        Orginal_Message: "",
                    };
                    changes.$push = {
                        Cancel_Message_Log: Data
                    }
                }
                let updateData = await Buyer_Orders.updateOne(query, changes).lean().exec()
                resolve({ success: true, extras: { Statuus: "Updated SuccessFully" } })
                if (Cancel_Calculation.Refund_Amount > 0) {
                    // Refund Process
                    let OrderData = await Buyer_Orders.findOne(query).lean().exec();
                    let PaymentID = OrderData.WebHookData.PaymentID
                    let Refund_To_Wallet = Cancel_Calculation.Refund_To_Wallet;
                    let Refund_To_Orgin = Cancel_Calculation.Refund_To_Orgin * 100;
                    let Orgin_Refund_Processc = await RazorpayController.Refund_Razorpay_Payment(PaymentID, Refund_To_Orgin)
                    let Data = {
                        BuyerID: OrderData.BuyerID,
                        OrderData: OrderData,
                        CancelData: Cancel_Calculation
                    };
                    let WData = {
                        LogID: uuid.v4(),
                        BuyerID: OrderData.BuyerID,
                        Type: 14, // amount Credited from Order Refund 
                        Amount: Cancel_Calculation.Refund_Amount,
                        Data: Data,
                        Time: new Date()
                    }
                    let Resultlog = Buyer_Share_Logs(WData).save();
                    if (Refund_To_Wallet > 0) {
                        let queryxx = {
                            BuyerID: OrderData.BuyerID
                        };
                        let changesxx = {
                            $set: {
                                updated_at: new Date()
                            },
                            $inc: {
                                Available_Amount: Refund_To_Wallet,
                                Withdrawn_Amount: Refund_To_Wallet * -1,
                            }
                        };
                        let updateData = await Buyers.updateOne(queryxx, changesxx).lean().exec()
                    };
                    let Discount_Share_Amount = parseFloat(OrderData.Order_Invoice.Order_Other_Discount);

                    let Fianl_Branch_Amount = OrderData.Order_Invoice.Order_Total_Branch_Share_Amount + Discount_Share_Amount
                    let Bquery = {
                        BranchID: OrderData.Branch_Information.BranchID
                    }
                    let Bchanges = {
                        $inc: {
                            "Branch_Wallet_Information.Available_Amount": Fianl_Branch_Amount,
                            "Branch_Wallet_Information.Total_Amount": Fianl_Branch_Amount
                        },
                        $set: {
                            updated_at: new Date()
                        }
                    }
                    let SaveAmountB = await Store_Branch.updateOne(Bquery, Bchanges).lean().exec();
                    let L2Data = {
                        LogID: uuid.v4(),
                        BranchID: OrderData.Branch_Information.BranchID,
                        Type: 21, // Amount debited to wallet for order cancel
                        Amount: Fianl_Branch_Amount,
                        Data: {
                            Order_Number: OrderData.Order_Number,
                            OrderID: OrderData.OrderID,
                            Amount: Fianl_Branch_Amount,
                        },
                        Time: new Date()
                    };
                    let L2SaveResult = await Store_Branch_Wallet_Logs.create(L2Data);
                    ////////////
                    let Admin_Share_Amount = parseFloat(OrderData.Order_Invoice.Order_Total_Admin_Share_Amount);
                    let Order_Package_Charge = parseFloat(OrderData.Order_Invoice.Order_Package_Charge);
                    let Order_Surge_Charge = parseFloat(OrderData.Order_Invoice.Order_Surge_Charge);
                    let Previous_Order_Cancellation_Charge = parseFloat(OrderData.Order_Invoice.Previous_Order_Cancellation_Charge);
                    let Order_Taxes = parseFloat(OrderData.Order_Invoice.Order_Taxes);

                    let Final_Admin_Amount = parseFloat(
                        Admin_Share_Amount
                        + Order_Package_Charge
                        + Order_Surge_Charge
                        + Order_Taxes
                        + Previous_Order_Cancellation_Charge
                        - Discount_Share_Amount
                    )
                    ///Add Online Amount to Company(Admin)
                    let AWData = {
                        LogID: uuid.v4(),
                        AdminID: "",
                        Type: 24, // amount debited for Order cancel 
                        Amount: Final_Admin_Amount,
                        Data: {
                            Order_Number: OrderData.Order_Number,
                            OrderID: OrderData.OrderID,
                            Admin_Share_Amount: Admin_Share_Amount,
                            Order_Package_Charge: Order_Package_Charge,
                            Order_Surge_Charge: Order_Surge_Charge,
                            Order_Taxes: Order_Taxes,
                            Previous_Order_Cancellation_Charge: Previous_Order_Cancellation_Charge,
                            Discount: Discount_Share_Amount,
                            Final_Admin_Amount: Final_Admin_Amount,
                            Calculation: "Final_Admin_Amount = Admin_Share_Amount + Order_Package_Charge + Taxes + Order_Surge_Charge + Previous_Order_Cancellation_Charge - Discount_Share_Amount"
                        },
                        Time: new Date()
                    }
                    let AResultlog = Company_Wallet_Log(AWData).save();

                    let Aquery = {

                    };
                    let Achanges = {
                        $inc: {
                            Available_Amount: Final_Admin_Amount * -1,
                            Total_Amount: Final_Admin_Amount * -1
                        }
                    };
                    let Cfndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let CfindupdateData = await Company_Wallet.findOneAndUpdate(Aquery, Achanges, Cfndupdoptions).select('-_id -__v').lean();
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Refund_Calculation = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Cancellation_Amount = parseFloat(values.Cancellation_Amount)
                let query = {
                    OrderID: values.OrderID,
                    Whether_Cancellation_Prodessed: 1
                }
                let OrderData = await Buyer_Orders.findOne(query).lean().exec()
                if (OrderData == null) {
                    reject({ success: false, extras: { msg: ApiMessages.ORDER_NOT_ACCEPTED } })
                } else {
                    let Refund_Amount = 0;
                    let Refund_To_Orgin = 0;
                    let Refund_To_Wallet = 0;
                    let Whether_Cancellation_Charges_Applied = false;
                    let Whether_Cancellation_Charges_For_Next_Order = false;
                    if (Cancellation_Amount > 0) {
                        Whether_Cancellation_Charges_Applied = true
                    }
                    let Cancelled_By = "";
                    if (OrderData.Cancel_Status == 0) {
                        reject({ success: false, extras: { msg: ApiMessages.ORDER_NOT_CANCELLED } })
                    } else if (OrderData.Cancel_Status == 1) {
                        Cancelled_By = "User"
                    } else if (OrderData.Cancel_Status == 2) {
                        Cancelled_By = "Admin"
                    } else if (OrderData.Cancel_Status == 3) {
                        Cancelled_By = "Store"
                    } else if (OrderData.Cancel_Status == 4) {
                        Cancelled_By = "Driver"
                    }
                    if (OrderData.Payment_Type == 4) {
                        if (Cancellation_Amount > 0) {
                            Whether_Cancellation_Charges_For_Next_Order = true
                        }
                    } else {
                        Refund_Amount = OrderData.Amount_Paid.Total_Amount_Paid - Cancellation_Amount;
                        if (OrderData.Amount_Paid.Amount_Used_From_Wallet > Cancellation_Amount) {
                            Refund_To_Wallet = OrderData.Amount_Paid.Amount_Used_From_Wallet - Cancellation_Amount;
                            Refund_To_Orgin = OrderData.Amount_Paid.Amount_Paid_Online;
                        } else {
                            Refund_To_Orgin = OrderData.Amount_Paid.Amount_Paid_Online - (Cancellation_Amount - OrderData.Amount_Paid.Amount_Used_From_Wallet)
                        }
                    }
                    let Data = {
                        Cancellation_Amount: Cancellation_Amount,
                        Refund_Amount: Refund_Amount,
                        Refund_To_Wallet: Refund_To_Wallet,
                        Refund_To_Orgin: Refund_To_Orgin,
                        Whether_Cancellation_Charges_Applied: Whether_Cancellation_Charges_Applied,
                        Whether_Cancellation_Charges_For_Next_Order: Whether_Cancellation_Charges_For_Next_Order,
                        Cancelled_By: Cancelled_By
                    }
                    resolve(Data)
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Notify_To_Branch = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let changes = {
                    $set: {
                        Whether_Cancellation_Notified: true,
                        updated_at: new Date()
                    }
                }
                let updatedData = await Buyer_Orders.updateOne(query, changes).lean().exec()
                resolve({ success: true, extras: { Status: "Updated successfully" } })
                let SendNotification = await NotificationController.SendNotification_Cancel_Order_Notify_To_Branch(values)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_And_Resend_Messages_for_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).select('Cancel_Message_Log').lean().exec();
                let Cancel_Message_Log = Result.Cancel_Message_Log.filter(function (ResultTemp) {
                    return ResultTemp._id == values._id;
                });
                let Today = new Date()
                let Data = {
                    Message: values.Message,
                    Date: Today,
                    Time: moment(Today, config.Common_Time_Format).toString(),
                    Resend: false,
                    Whether_Edited: true,
                    Orginal_Message: Cancel_Message_Log[0].Message
                }
                let changes = {
                    $push: {
                        Cancel_Message_Log: Data
                    },
                    $set: {
                        updated_at: new Date()
                    }
                }
                let updatedData = await Buyer_Orders.updateOne(query, changes).lean().exec()
                resolve({ success: true, extras: { Status: "Updated successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Resend_Message_for_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).select('Cancel_Message_Log').lean().exec();
                let Cancel_Message_Log = Result.Cancel_Message_Log.filter(function (ResultTemp) {
                    return ResultTemp._id == values._id;
                });
                let Today = new Date()
                let Data = {
                    Message: Cancel_Message_Log[0].Message,
                    Date: Today,
                    Time: moment(Today, config.Common_Time_Format).toString(),
                    Resend: true,
                    Orginal_Message: Cancel_Message_Log[0].Message
                }
                let changes = {
                    $push: {
                        Cancel_Message_Log: Data
                    },
                    $set: {
                        updated_at: new Date()
                    }
                }
                let updatedData = await Buyer_Orders.updateOne(query, changes).lean().exec()
                resolve({ success: true, extras: { Status: "Updated successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Messages_for_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).select('Cancel_Message_Log').lean().exec();
                let Cancel_Message_Log = []
                if (Result != null) {
                    if (parseInt(values.List_Type) == 1) {
                        Cancel_Message_Log = Result.Cancel_Message_Log
                    } else if (parseInt(values.List_Type) == 2) {
                        Cancel_Message_Log = Result.Cancel_Message_Log.filter(function (ResultTemp) {
                            return ResultTemp.Resend == true;
                        });
                    } else if (parseInt(values.List_Type) == 3) {
                        Cancel_Message_Log = Result.Cancel_Message_Log.filter(function (ResultTemp) {
                            return ResultTemp.Whether_Edited == true;
                        });
                    }
                }
                resolve({ success: true, extras: { Data: Cancel_Message_Log } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Cancelled_Orders_By_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: 2,
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.Branch_Information.CityID = CityData.CityID
                    Order.Branch_Information.CityName = CityData.CityName
                    Order.Branch_Information.ZoneID = ZoneData.ZoneID
                    Order.Branch_Information.Zone_Title = ZoneData.Zone_Title
                    Order.UserData = await CommonController.Check_For_Buyer(Order)
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Cancelled_Orders_By_Drivers = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: 4,
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.Branch_Information.CityID = CityData.CityID
                    Order.Branch_Information.CityName = CityData.CityName
                    Order.Branch_Information.ZoneID = ZoneData.ZoneID
                    Order.Branch_Information.Zone_Title = ZoneData.Zone_Title
                    Order.UserData = await CommonController.Check_For_Buyer(Order)
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Cancelled_Orders_By_Branches = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };

                let matchquery = {
                    Cancel_Status: 3,
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.Branch_Information.CityID = CityData.CityID
                    Order.Branch_Information.CityName = CityData.CityName
                    Order.Branch_Information.ZoneID = ZoneData.ZoneID
                    Order.Branch_Information.Zone_Title = ZoneData.Zone_Title
                    Order.UserData = await CommonController.Check_For_Buyer(Order)
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Cancelled_Orders_By_Buyers = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: 1,
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.Branch_Information.CityID = CityData.CityID
                    Order.Branch_Information.CityName = CityData.CityName
                    Order.Branch_Information.ZoneID = ZoneData.ZoneID
                    Order.Branch_Information.Zone_Title = ZoneData.Zone_Title
                    Order.UserData = await CommonController.Check_For_Buyer(Order)
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Cancel_Order_Return_Initiate = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID,
                }
                let Cancellation_Order_Pickup_Report = {
                    Title: "Initiated",
                    Description: "Order return is initiated",
                    Time: new Date()
                }
                let changes = {
                    $set: {
                        Order_Status: 11,
                        Cancellation_Pickup_Status: 1,
                        updated_at: new Date()
                    },
                    $push: {
                        Cancellation_Order_Pickup_Report: Cancellation_Order_Pickup_Report
                    }
                }
                let updateData = await Buyer_Orders.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Accept_Reject_Cancelled_Orders_By_Buyers = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {

                let query = {
                    OrderID: values.OrderID,
                }
                let Today = new Date()
                let changes = {
                    $set: {
                        Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed,
                        Cancel_Message_Log: {
                            Message: values.Message,
                            Date: Today,
                            Time: moment(Today, config.Common_Time_Format).toString()
                        },
                        updated_at: new Date()
                    }
                }
                let updateData = await Buyer_Orders.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
                let SendNotification = await FcmController.SendNotification_Accept_Reject_Cancelled_Orders_By_Buyers(values)
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Buyers_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    },
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Buyers_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };

                let matchquery = {
                    Status: values.Status,
                    Cancel_Status: 0,
                    Payment_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();

                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Buyer_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');

                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };

                let matchquery = {
                    Cancel_Status: {
                        $ne: 0
                    },
                    BuyerID: values.BuyerID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    },
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();

                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Buyer_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                // let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let Start_Date = moment(values.Start_Date).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');

                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Status: values.Status,
                    Cancel_Status: 0,
                    BuyerID: values.BuyerID,
                    Payment_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();

                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Branch_Category_Cancelled_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');

                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    // Status: values.Status,
                    'Branch_Information.BranchID': values.BranchID,
                    Cancel_Status: {
                        $ne: 0
                    },
                    'Cart_Information.CategoryID': values.CategoryID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    },
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                // if(values.SubCategoryID != '') {
                //     matchquery.Cart_Information.SubCategoryID = values.SubCategoryID
                // }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const item of Result) {
                    item.Cart_Information = item.Cart_Information.filter(function (ResultTemp) {
                        return ResultTemp.CategoryID == values.CategoryID;
                    });
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Branch_Category_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');

                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    // Status: values.Status,
                    'Branch_Information.BranchID': values.BranchID,
                    Cancel_Status: 0,
                    Payment_Status: {
                        $ne: 0
                    },
                    'Cart_Information.CategoryID': values.CategoryID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                if (values.Status == true) {
                    matchquery.Order_Status = 3
                } else {
                    matchquery.Order_Status = {
                        $ne: 3
                    }
                }
                // if(values.SubCategoryID != '') {
                //     matchquery.Cart_Information.SubCategoryID = values.SubCategoryID
                // }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const item of Result) {
                    item.Cart_Information = item.Cart_Information.filter(function (ResultTemp) {
                        return ResultTemp.CategoryID == values.CategoryID;
                    });
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.List_Branch_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let sortOptions = {
                    created_at: -1
                };

                let matchquery = {
                    'Branch_Information.BranchID': values.BranchID,
                    Cancel_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    },
                    Whether_Cancellation_Prodessed: values.Whether_Cancellation_Prodessed
                };
                // console.log(matchquery)
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Canceled_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: values.Cancel_Status,
                    Whether_Cancellation_Refund_Prodessed: values.Whether_Cancellation_Refund_Prodessed
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                let BranchID_Array = [];
                if (values.CityID != "" && values.ZoneID == "") {
                    let Bquery = {
                        CityID: values.CityID
                    }
                    BranchID_Array = await Store_Branch.distinct('BranchID', Bquery).lean();
                    matchquery['Branch_Information.BranchID'] = {
                        $in: BranchID_Array
                    }
                }
                if (values.ZoneID != "" && values.CityID == "") {
                    let Bquery = {
                        ZoneID: values.ZoneID
                    }
                    BranchID_Array = await Store_Branch.distinct('BranchID', Bquery).lean();
                    matchquery['Branch_Information.BranchID'] = {
                        $in: BranchID_Array
                    }
                }
                if (values.ZoneID != "" && values.CityID != "") {
                    let Bquery = {
                        CityID: values.CityID,
                        ZoneID: values.ZoneID
                    }
                    BranchID_Array = await Store_Branch.distinct('BranchID', Bquery).lean();
                    matchquery['Branch_Information.BranchID'] = {
                        $in: BranchID_Array
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.Branch_Information.CityID = CityData.CityID
                    Order.Branch_Information.CityName = CityData.CityName
                    Order.Branch_Information.ZoneID = ZoneData.ZoneID
                    Order.Branch_Information.Zone_Title = ZoneData.Zone_Title
                    Order.UserData = await CommonController.Check_For_Buyer(Order)
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: 0,
                    Payment_Status: {
                        $ne: 0
                    },
                };
                if (Boolify(values.Whether_Date_Filter)) {
                    let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                    let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                    matchquery.created_at = {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                }
                if (values.Status == true) {
                    matchquery.Order_Status = 3
                } else {
                    matchquery.Order_Status = {
                        $ne: 3
                    }
                }
                let BranchID_Array = [];
                if (values.CityID != "") {
                    let Bquery = {
                        CityID: values.CityID
                    }
                    BranchID_Array = await Store_Branch.distinct('BranchID', Bquery).lean();
                    matchquery['Branch_Information.BranchID'] = {
                        $in: BranchID_Array
                    }
                }
                if (values.BranchID != "") {
                    BranchID_Array = [values.BranchID]
                    matchquery['Branch_Information.BranchID'] = {
                        $in: BranchID_Array
                    }
                }
                // console.log(matchquery)
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                for (let Order of Result) {
                    let OrderReport = []
                    for (let Order_Report of Order.Order_Report) {
                        let Data = new Object();
                        if (Order_Report.Title == "Order Received") {
                            Data = {
                                Status: "Order Received",
                                Description: Order.Branch_Information.Branch_Name + " has received your order successfully",
                                Time: Order_Report.Time
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Driver Assigned") {
                            Data = {
                                Status: "Driver Assigned",
                                Description: "Your assigned delivery executive for this order is Mr." + Order.Driver_Information.Driver_Name,
                                Time: Order_Report.Time
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Order Picked") {
                            Data = {
                                Status: "Order Picked Up",
                                Description: "Mr." + Order.Driver_Information.Driver_Name + " has arrived at the bookstore to pick up your order",
                                Time: Order_Report.Time
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Drop Stated") {
                            let q = {
                                OrderID: Order.OrderID
                            }
                            let TripData = await Trips.findOne(q).lean().exec();
                            Data = {
                                Status: "Order In Transit",
                                Description: "Your order has been picked up and is on the way for delivery ETA: approximately " + TripData.Total_Eta.toFixed(2) + " Min",
                                Time: Order_Report.Time
                            }
                            OrderReport.push(Data)
                        } else if (Order_Report.Title == "Order Delivered") {
                            Data = {
                                Status: "Order Completed",
                                Description: "Your order has been delivered successfully, Received by: " + Order.Delivered_To + ", Ordered by: " + Order.Delivery_Address_Information.Name,
                                Time: Order_Report.Time
                            }
                            OrderReport.push(Data)
                        }
                    }
                    delete Order.Order_Report;
                    Order.Order_Report = OrderReport
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Branch_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let sortOptions = {
                    created_at: -1
                };

                let matchquery = {
                    'Branch_Information.BranchID': values.BranchID,
                    Cancel_Status: 0,
                    Payment_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                if (values.Status == true) {
                    matchquery.Order_Status = 3
                } else {
                    matchquery.Order_Status = {
                        $ne: 3
                    }
                }
                // console.log(matchquery)
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.List_All_Buyers_With_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Orders_Count: {
                        $gte: values.Orders_Count
                    }
                };
                let Count = await Buyers.countDocuments(matchquery).lean().exec();
                let Result = await Buyers.find(matchquery).select('-_id -__v -Cart_Information -Cart_Total_Price -Cart_Total_Items').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Buyers_Details = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {

                };
                if (values.CityID != null && values.CityID != "") {
                    matchquery.CityID = values.CityID
                }
                if (values.ZoneID != null && values.ZoneID != "") {
                    matchquery.ZoneID = values.ZoneID
                }
                if (values.CountryID != null && values.CountryID != "") {
                    matchquery.CountryID = values.CountryID
                }
                let Count = await Buyers.countDocuments(matchquery).lean().exec();
                let Result = await Buyers.find(matchquery).select('-_id -__v -Cart_Information -Cart_Total_Price -Cart_Total_Items').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Single_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Provide_Refund_For_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                let BuyersQuery = {
                    BuyerID: Result.BuyerID
                }
                let BuyerData = await Buyers.findOne(BuyersQuery).lean().exec();
                let Refund_Amount = parseFloat(values.Refund_Amount); // Add this amount to type 4
                let Data = {
                    OrderID: Result.OrderID,
                    Amount: Refund_Amount,
                    BuyerData: BuyerData
                }
                let WData = {
                    LogID: uuid.v4(),
                    BuyerID: Result.BuyerID,
                    Type: 4, // amount Credited from Order Refund 
                    Amount: Refund_Amount,
                    Data: Data,
                    Time: new Date()
                }
                let Resultlog = Buyer_Share_Logs(WData).save();
                let BuyerChanges = {
                    $inc: {
                        Available_Amount: Refund_Amount,
                        Total_Amount: Refund_Amount
                    },
                };
                let fndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let updatBuyerData = await Buyers.findOneAndUpdate(BuyersQuery, BuyerChanges, fndupdoptions).lean().exec();

                //deduct amount from company wallet 

                let WDataw = {
                    LogID: uuid.v4(),
                    AdminID: values.AdminID,
                    Type: 2, // amount deduct from Bookafella Admin wallet for refund
                    Amount: Refund_Amount,
                    Data: Data,
                    Time: new Date()
                }
                let Resultlogw = Company_Wallet_Log(WDataw).save();
                let Cfndupdquery = {

                }
                let Cfndupdchanges = {
                    $inc: {
                        Available_Amount: Refund_Amount * -1,
                        Withdrawn_Amount: Refund_Amount
                    }
                }
                let Cfndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Refunded Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Amount_Company_Wallet = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Tranx = uuid.v4();
                let Amount = parseFloat(values.Amount.toFixed(2));
                let Data = {
                    TransactionID: Tranx,
                    AdminID: values.AdminID,
                    Amount: Amount,
                    Payment_Status: 1,
                    created_at: new Date(),
                    updated_at: new Date()
                }
                let SaveDat = await Company_Wallet_Tranx(Data).save();
                let Result = {
                    TransactionID: Tranx,
                    callback: true,
                    Amount: Amount
                }
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Complete_Add_Amount_Company_Wallet = (TransactionID, WebHookData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    TransactionID: TransactionID,
                    Status: false
                }
                let Result = await Company_Wallet_Tranx.findOne(query).lean().exec();
                if (Result != null) {
                    let changes = {
                        $set: {
                            WebHookData: WebHookData,
                            Status: true,
                            Payment_Status: 3,
                            update_at: new Date(),
                        }
                    }
                    let updatData = await Company_Wallet_Tranx.updateOne(query, changes).lean().exec();
                    //Add amount to company Wallte With Type 1 //Credit
                    let Data = {
                        TransactionID: TransactionID,
                        Amount: Result.Amount,
                        WebHookData: WebHookData
                    }
                    let WData = {
                        LogID: uuid.v4(),
                        AdminID: Result.AdminID,
                        Type: 1, // amount Credited to Bookafella Admin wallet 
                        Amount: Result.Amount,
                        Data: Data,
                        Time: new Date()
                    }
                    let Resultlog = Company_Wallet_Log(WData).save();
                    let Cfndupdquery = {

                    }
                    let Cfndupdchanges = {
                        $inc: {
                            Available_Amount: Result.Amount,
                            Total_Amount: Result.Amount
                        }
                    }
                    let Cfndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();

                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_TRANSACTION } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Company_Wallet = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let walletData = await Company_Wallet.findOne().select('-_id -__v').lean().exec();
                if (walletData != null) {
                    resolve({ success: true, extras: { Data: walletData } });
                } else {
                    let Data = {
                        "Available_Amount": 0,
                        "Total_Amount": 0,
                        "Withdrawn_Amount": 0
                    }
                    resolve({ success: true, extras: { Data: Data } });
                }

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.Send_Custom_Notification = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    NotificationID: values.NotificationID
                };
                let notificationResult = await Custom_Notification_Log.findOne(query).lean().exec();
                let bbquery = {
                    BuyerID: {
                        $in: notificationResult.BuyerID_Array
                    },
                    DeviceID: {
                        $ne: ''
                    },
                    Status: true
                };
                let DeviceID_Array = await Buyers_Current_Location.distinct('DeviceID', bbquery).lean();
                let ffquery = {
                    DeviceID: {
                        $in: DeviceID_Array
                    }
                };
                let FCM_Token_Array = await Devices.distinct('FCM_Token', ffquery).lean();
                let NotificationSending = await FcmController.Send_Buyer_Custom_Notifications(notificationResult, FCM_Token_Array);
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_for_Custom_Notification = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let Queryx = {
                    NotificationID: values.NotificationID
                }
                let Resultx = await Custom_Notification_Log.findOne(Queryx).lean().exec();
                if (Resultx != null) {
                    resolve(Resultx);
                } else {
                    reject({ success: false, extras: { msg: "INVALID CUSTOM NOTIFICATION" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Check_for_Custom_Notification_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log(values.Old_SNO, values.SNo)
                let Old_SNO;
                if (values.NotificationID != '' && values.NotificationID != null) {
                    let Queryx = {
                        NotificationID: values.NotificationID
                    }
                    let Resultx = await Custom_Notification_Log.findOne(Queryx).lean().exec();
                    Old_SNO = Resultx.SNo
                }
                let query = {
                    SNo: values.SNo
                }
                let Result = await Custom_Notification_Log.findOne(query).lean().exec();
                if (Result == null || Old_SNO == values.SNo) {
                    resolve('SNo Available');
                } else {
                    reject({ success: false, extras: { msg: "SERIAL NUMBER NOT AVAILABLE" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_Inactive_Custom_Notification = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    NotificationID: values.NotificationID
                }
                let Result = await Custom_Notification_Log.findOne(query).lean().exec()
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_NOTIFICATION } })
                } else {
                    let changes = {
                        $set: {
                            Status: !Result.Status,
                            updated_at: new Date()
                        }
                    }
                    let SaveData = await Custom_Notification_Log.updateOne(query, changes).lean().exec();
                    let Status = "Active Successfully"
                    if (Result.Status) {
                        Status = "Inactive Successfully"
                    }
                    resolve({ success: true, extras: { Status: Status } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Custom_Notification = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    NotificationID: values.NotificationID
                }
                let changes = {
                    $set: {
                        CountryID: values.CountryID,
                        CityID: values.CityID,
                        ZoneID: values.ZoneID,
                        SNo: values.SNo,
                        BuyerID_Array: values.BuyerID_Array,
                        Notification_Type: values.Notification_Type, // 1- Zone, 2- All_Users 3- Selected_Users
                        NotificationTitle: values.NotificationTitle,
                        NotificationBody: values.NotificationBody,
                        updated_at: new Date()
                    }
                }
                let SaveData = await Custom_Notification_Log.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Custom_Notifications = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: values.Status
                };
                let sortOptions = {
                    SNo: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                if (Boolify(values.Whether_Notification_Type_Filter)) {
                    query.Notification_Type = values.Notification_Type
                }
                if (values.CountryID != '') {
                    query.CountryID = values.CountryID
                }
                if (values.CityID != '') {
                    query.CityID = values.CityID
                }
                if (values.ZoneID != '') {
                    query.ZoneID = values.ZoneID
                }
                let Count = await Custom_Notification_Log.countDocuments(query).lean().exec();
                let Result = await Custom_Notification_Log.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Buyers_In_City = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {

                let query = {
                    CityID: values.CityID
                }
                let Search = {
                    $regex: String(values.Search),
                    $options: "i"
                };
                query.$or = [
                    {
                        Buyer_Name: Search
                    },
                    {
                        Buyer_Email: Search
                    },
                    {
                        Buyer_PhoneNumber: Search
                    }
                ]
                let Count = await Buyers_Current_Location.countDocuments(query).lean().exec();
                let UserData = await Buyers_Current_Location.distinct("BuyerID", query).lean().exec();
                let BuyerData = [];
                for (const BuyerID of UserData) {
                    let Buyer = await CommonController.Check_For_Buyer({ BuyerID: BuyerID })
                    let Data = {
                        Buyer_Name: Buyer.Buyer_Name,
                        BuyerID: Buyer.BuyerID,
                        Buyer_Email: Buyer.Buyer_Email,
                        Buyer_CountryCode: Buyer.Buyer_CountryCode,
                        Buyer_PhoneNumber: Buyer.Buyer_PhoneNumber,
                        Available_Amount: Buyer.Available_Amount,
                        Available_Credits: 0 //change with Buyer credits
                    }
                    BuyerData.push(Data)
                }
                resolve({ success: true, extras: { Count: Count, Data: BuyerData } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Custom_Notification = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    NotificationID: uuid.v4(),
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    SNo: values.SNo,
                    BuyerID_Array: values.BuyerID_Array,
                    Notification_Type: values.Notification_Type, // 1- Zone, 2- All_Users, 3- Selected_Users
                    NotificationTitle: values.NotificationTitle,
                    NotificationBody: values.NotificationBody,
                    updated_at: new Date(),
                    Created_at: new Date(),
                }
                let SaveData = await Custom_Notification_Log(Data).save();
                resolve({ success: true, extras: { Status: "Created Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Company_Wallet_Log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let Count = await Company_Wallet_Log.countDocuments().lean().exec();
                let walletData = await Company_Wallet_Log.find().select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: walletData } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Activate_Inactivate_Fee_Details_For_Zone = (values, Zone_Fee) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ZoneID: values.ZoneID
                };
                let changes = {
                    $set: {
                        Status: !Zone_Fee.Status,
                        updated_at: new Date()
                    }
                };
                let Updatedata = await Zone_Fees.updateOne(query, changes).lean().exec();
                let Status = "Activated Sucessfully"
                if (Zone_Fee.Status == true) {
                    Status = "Inactivated Sucessfully"
                }
                resolve({ success: true, extras: { Status: Status } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Fee_Details_For_Zones = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: values.Status
                }
                if (values.ZoneID != '') {
                    query.ZoneID = values.ZoneID
                }
                if (values.Search != '') {
                    let Search = {
                        $regex: String(values.Search),
                        $options: "i"
                    };
                    query.$or = [
                        {
                            Zone_Title: Search
                        }
                    ]
                }
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let Count = await Zone_Fees.countDocuments(query).lean().exec();
                let Data = await Zone_Fees.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Fee_Details_For_Zones = (values, ZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                async.eachSeries(ZoneData, async (item, callback) => {
                    try {
                        let Cfndupdquery = {
                            ZoneID: item.ZoneID
                        }
                        let Cfndupdchanges = {
                            $set: {
                                CountryID: item.CountryID,
                                CityID: item.CityID,
                                ZoneID: item.ZoneID,
                                Zone_Title: item.Zone_Title,
                                Delivery_Fee: values.Delivery_Fee,
                                Packaging_Fee: values.Packaging_Fee,
                                Surge_Fee: values.Surge_Fee,
                                Status: true,
                                updated_at: new Date()
                            }
                        }
                        let Cfndupdoptions = {
                            upsert: true,
                            setDefaultsOnInsert: true,
                            new: true
                        }
                        let CfindupdateData = await Zone_Fees.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Activate_Inactivate_Taxes = (values, TaxesData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Cfndupdquery = {

                }
                let Cfndupdchanges = {
                    $set: {
                        Status: !TaxesData.Status,
                    }
                }
                let Cfndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let CfindupdateData = await Taxes.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                let Status = "Activated Sucessfully"
                if (TaxesData.Status == true) {
                    Status = "Inactivated Sucessfully"
                }
                resolve({ success: true, extras: { Status: Status } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Taxes = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Cfndupdquery = {

                }
                let Cfndupdchanges = {
                    $set: {
                        Bookstore_GST: values.Bookstore_GST,
                        Delivery_GST: values.Delivery_GST,
                        Status: true,
                    }
                }
                let Cfndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let CfindupdateData = await Taxes.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Get_Taxes = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Result = await Taxes.findOne({}).select('-_id -__v').lean().exec();
                resolve({ success: true, extras: { Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

// AdminController.Update_Delivery_Charges = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Cfndupdquery = {

//                 }
//                 let Cfndupdchanges = {
//                     $set: {
//                         Slab_Km: values.Slab_Km,
//                         Slab_Amount: values.Slab_Amount,
//                         Per_Km_Amount: values.Per_Km_Amount,
//                     }
//                 }
//                 let Cfndupdoptions = {
//                     upsert: true,
//                     setDefaultsOnInsert: true,
//                     new: true
//                 }
//                 let CfindupdateData = await Delivery_Price.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
//                 resolve({ success: true, extras: { Status: "Updated successfully" } })

//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

// AdminController.Get_Delivery_Charges_Data = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Result = await Delivery_Price.findOne({}).select('-_id -__v').lean().exec();
//                 resolve({ success: true, extras: { Data: Result } })
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

AdminController.Common_Razorpay_DRIVER_PAYOUT_Update_Statuses = (PayoutData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let TransactionData = await Driver_Salary_Record_Payout_Transactions.findOne({ RazorpayX_TransactionID: PayoutData.id }).lean();
                if (TransactionData == null) {
                    resolve("Updated Successfully")
                } else {
                    let Transaction_Detailed_Data = await config.Razorpay_Transaction_Data.find(ele => ele.status == PayoutData.status);
                    if (TransactionData.Transaction_Status == 2 || TransactionData.Transaction_Status == 5 || TransactionData.Transaction_Status == 7) {
                        resolve("Updated Successfully");
                    } else {
                        let Transaction_Status = Transaction_Detailed_Data.Transaction_Status;
                        if (TransactionData.Transaction_Status == Transaction_Status) {
                            resolve("Updated Successfully");
                        } else {
                            let Transaction_Status_Logs = {
                                LogID: uuid.v4(),
                                Transaction_Status: Transaction_Status,
                                Comment: Transaction_Detailed_Data.Comment,
                                Time: new Date()
                            };
                            let Transaction_Reference_ID = (Transaction_Status == 2) ? PayoutData.utr : "";
                            let Transaction_Completion_Remarks = (Transaction_Status == 2) ? Transaction_Detailed_Data.Comment : "";
                            let query = {
                                TransactionID: TransactionData.TransactionID
                            };
                            let changes = {
                                $set: {
                                    Transaction_Status: Transaction_Status,
                                    Transaction_Reference_ID: Transaction_Reference_ID,
                                    Transaction_Completion_Remarks: Transaction_Completion_Remarks,
                                    RazorpayXPayoutData: PayoutData,
                                    updated_at: new Date()
                                },
                                $push: {
                                    Transaction_Status_Logs: Transaction_Status_Logs
                                }
                            };
                            let UpdatedStatus = await Driver_Salary_Record_Payout_Transactions.updateOne(query, changes).lean();
                            resolve("Updated Successfully");
                            if (Transaction_Status == 2) {
                                let Amount = TransactionData.Amount;
                                let aaquery = {
                                    All_DRIVER_Payout_ID: TransactionData.All_DRIVER_Payout_ID
                                };
                                let aachanges = {
                                    $set: {
                                        updated_at: new Date()
                                    },
                                    $inc: {
                                        Settled_Payout_Amount: Amount,
                                        Non_Settled_Payout_Amount: (Amount * -1)
                                    }
                                };
                                let aaUpdatedStatus = await Driver_All_Salary_Record_Payouts.updateOne(aaquery, aachanges).lean();
                            }
                        }
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Banner = (values, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    BannerID: uuid.v4(),
                    Image_Data: ImageData,
                    Banner_Name: values.Banner_Name,
                    SNo: values.SNo,
                    Status: true,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveData = await Banner(Data).save()
                resolve({ success: true, extras: { Status: "Banner Created Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_Inactive_Banner = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BannerID: values.BannerID
                }
                let BannerResult = await Banner.findOne(query).lean().exec()
                let statys;
                if (BannerResult.Status) {
                    statys = "Inactivated Successfully";
                    let changes = {
                        $set: {
                            Status: false,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Banner.updateOne(query, changes).lean().exec();
                } else {
                    let querycheck = {
                        SNo: BannerResult.SNo
                    }
                    let checkSno = await Banner.find(querycheck).lean().exec()
                    if (checkSno.length <= 1) {
                        statys = "Activated Successfully"
                        let changes = {
                            $set: {
                                Status: true,
                                updated_at: new Date()
                            }
                        }
                        let UpdatedStatus = await Banner.updateOne(query, changes).lean().exec();
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                    }
                }
                resolve({ success: true, extras: { Status: statys } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Banner = (values, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BannerID: values.BannerID,
                }
                let BannerData = await Banner.findOne(query).lean().exec();
                if (BannerData != null) {
                    let changes = {
                        $set: {
                            Image_Data: ImageData,
                            Banner_Name: values.Banner_Name,
                            SNo: values.SNo,
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let Updatedate = await Banner.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Banner Updated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_BANNER } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Banner = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                };
                let sortOptions = {
                    SNo: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Banner.countDocuments(query).lean().exec();
                let Result = await Banner.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                let Data = []
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let ImageData = await CommonController.Common_Image_Response_Single_Image(true, item.Image_Data);
                        item.Image_Data = ImageData
                        Data.push(item);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } })
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Quote = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let changes = {
                    Status: false
                }
                let updateData = await Quote.updateMany({}, changes).lean().exec();
                let Data = {
                    QuoteID: uuid.v4(),
                    Quote: values.Quote,
                    Quote_Author: values.Quote_Author,
                    Status: true,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveData = await Quote(Data).save()
                resolve({ success: true, extras: { Status: "Quote Created Successfully" } });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Quotes = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                };
                let sortOptions = {
                    updated_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Quote.countDocuments(query).lean().exec();
                let Result = await Quote.find(query).select('-_id -__v -updated_at').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_Inactive_Quote = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    QuoteID: values.QuoteID
                }
                let QuoteResult = await Quote.findOne(query).lean().exec()
                let statys;
                if (QuoteResult.Status) {
                    statys = "Inactivated Successfully";
                    let changes = {
                        $set: {
                            Status: false,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Quote.updateOne(query, changes).lean().exec();
                } else {
                    statys = "Activated Successfully"
                    let changesX = {
                        $set: {
                            Status: false,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatusX = await Quote.updateMany({}, changesX).lean().exec();
                    let changes = {
                        $set: {
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Quote.updateOne(query, changes).lean().exec();
                }
                resolve({ success: true, extras: { Status: statys } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Quote = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    QuoteID: values.QuoteID,
                }
                let QuoteData = await Quote.findOne(query).lean().exec();
                if (QuoteData != null) {
                    let changes = {
                        $set: {
                            Quote: values.Quote,
                            Quote_Author: values.Quote_Author,

                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let Updatedate = await Quote.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Quote Updated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_QUOTE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Category = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    CategoryID: uuid.v4(),
                    Category: values.Category,
                    SNo: values.SNo,
                    Status: true,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveData = await Category(Data).save()
                resolve({ success: true, extras: { Status: "Category Created Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_SubCategory = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID,
                    Status: true
                }
                let Result = await Category.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CATEGORY } })
                } else {
                    let SubID = uuid.v4();
                    let changes = {
                        $push: {
                            SubCategory_Data: {
                                SubCategoryID: SubID,
                                SubCategory: values.SubCategory
                            }
                        }
                    }
                    let UpdateData = await Category.updateOne(query, changes).lean().exec();
                    resolve({ success: true, extras: { Status: "Updated Successfulluy" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Active_Inactive_Category = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID
                }
                let CategoryResult = await Category.findOne(query).lean().exec()
                let statys;
                if (CategoryResult.Status) {
                    statys = "Inactivated Successfully";
                    let changes = {
                        $set: {
                            Status: false,
                            SNo: 0,
                            updated_at: new Date()
                        }
                    }
                    let UpdatedStatus = await Category.updateOne(query, changes).lean().exec();
                } else {
                    let querycheck = {
                        SNo: CategoryResult.SNo
                    }
                    let checkSno = await Category.find(querycheck).lean().exec()
                    if (checkSno.length <= 1) {
                        statys = "Activated Successfully"
                        let changes = {
                            $set: {
                                Status: true,
                                updated_at: new Date()
                            }
                        }
                        let UpdatedStatus = await Category.updateOne(query, changes).lean().exec();
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                    }
                }
                resolve({ success: true, extras: { Status: statys } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Remove_SubCategory = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID
                }
                let CategoryResult = await Category.findOne(query).lean().exec()
                if (CategoryResult == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CATEGORY } })
                } else {
                    if (CategoryResult.SubCategory_Data.length != 0) {
                        let changespull = {
                            $pull: {
                                SubCategory_Data: {
                                    SubCategoryID: values.SubCategoryID
                                }
                            }
                        }
                        let ResultPull = await Category.updateOne(query, changespull).lean().exec();
                        resolve({ success: true, extras: { Status: "Item Removed Successfully" } });
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SUB_CATEGORY_NOT_AVAILABLE } })
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_SubCategory = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID,
                    'SubCategory_Data.SubCategoryID': values.SubCategoryID
                }
                let CategoryData = await Category.findOne(query).lean().exec();
                if (CategoryData != null) {
                    let changes = {
                        $set: {
                            'SubCategory_Data.$.SubCategory': values.SubCategory
                        }
                    }
                    let SavaData = await Category.updateOne(query, changes).lean().exec()
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CATEGORY } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Category = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID,
                }
                let CategoryData = await Category.findOne(query).lean().exec();
                if (CategoryData != null) {
                    let changes = {
                        $set: {
                            Category: values.Category,
                            SNo: values.SNo,
                            Status: true,
                            updated_at: new Date()
                        }
                    }
                    let Updatedate = await Category.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Category Updated Successfully" } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CATEGORY } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.Branch_Product_Reorder = (values, ProductData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (ProductData.Whether_Deleted == false || ProductData.Whether_Deleted == null || ProductData.Whether_Deleted == undefined) {
                    let query = {
                        ReorderID: values.ReorderID,
                        BranchID: values.BranchID,
                        ProductID: values.ProductID,
                    }
                    let result = await Store_Product_Reorder.findOne(query).lean().exec()
                    let chnages = {
                        $inc: {
                            Reorder_Count: 1
                        },
                        $set: {
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Store_Product_Reorder.updateOne(query, chnages).lean().exec()
                    let Data = {
                        ReorderLogID: uuid.v4(),
                        ReorderID: values.ReorderID,
                        BranchID: result.BranchID,
                        StoreID: result.StoreID, // manual StoreID
                        ProductID: result.ProductID,
                        Unique_ProductID: result.Unique_ProductID,
                        Product_Name: result.Product_Name,
                        Format_of_the_Book: result.Format_of_the_Book,
                        Quantity_Intake: values.Quantity_Intake,
                        BookStore_Reorder: true,
                        ZoneID: result.ZoneID,
                        CountryID: result.CountryID,
                        CityID: result.CityID,
                        CategoryID: result.CategoryID,
                        Category: result.Category,
                        SubCategoryID: result.SubCategoryID,
                        SubCategory: result.SubCategory,
                        Reorder_Status: 0, //0- not updated, 1- Stock Available, 2- Stock Partial Available, 3- Stock Not Available
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let SaveData = Reorder_log(Data).save();
                    resolve({ success: true, extras: { Status: 'Updated Successfully' } })
                    let SendNotification = await NotificationController.SendNotification_Branch_Product_Reorder(Data)
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.PRODUCT_DOES_NOT_EXIST } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Products_For_Reorder = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                };
                if (values.CountryID != "") {
                    query.CountryID = values.CountryID
                }
                if (values.CityID != "") {
                    query.CityID = values.CityID
                }
                if (values.ZoneID != "") {
                    query.ZoneID = values.ZoneID
                }
                if (values.CategoryID != "") {
                    query.CategoryID = values.CategoryID
                }
                if (values.SubCategoryID != "") {
                    query.SubCategoryID = values.SubCategoryID
                }
                if (values.BranchID != "") {
                    query.BranchID = values.BranchID
                }
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Store_Product_Reorder.countDocuments(query).lean().exec();
                let Result = await Store_Product_Reorder.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let PQuery = {
                            ProductID: item.ProductID
                        }
                        let Presult = await Store_Products.findOne(PQuery).lean().exec();
                        item.Product_Data = Presult.Presult;
                        callback()
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } })
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Product_Reorder_Log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ProductID: values.ProductID
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Reorder_log.countDocuments(query).lean().exec();
                let Result = await Reorder_log.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Category_Sold = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let Result = await Category.find(query).lean().exec();
                let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let cquery = {
                            'Branch_Information.BranchID': values.BranchID,
                            'Cart_Information.CategoryID': item.CategoryID,
                            Payment_Status: 3
                        }
                        let Cat_Order_Data = await Buyer_Orders.find(cquery).lean().exec()
                        let Cat_Count = await AdminController.Category_Sold_Calculation(Cat_Order_Data, item.CategoryID);
                        let xCat = {};
                        xCat.Name = item.Category
                        xCat.Count = Cat_Count
                        xCat.SubCategory_Data = await AdminController.SubCategory_Sold_Calculation_1(Cat_Order_Data, item.SubCategory_Data);
                        Data.push(xCat);
                        callback()
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Data: Data } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.SubCategory_Sold_Calculation_1 = (values, SubCategory_Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = []
                async.eachSeries(SubCategory_Data, async (item, callback) => {
                    try {
                        let Datax = {}

                        let Cat_Count = await AdminController.SubCategory_Sold_Calculation_2(values, item.SubCategoryID);
                        Datax.Name = item.SubCategory;
                        Datax.Count = Cat_Count;
                        Data.push(Datax)
                        callback()
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Data);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.SubCategory_Sold_Calculation_2 = (values, SubCategoryID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Count = 0;
                async.eachSeries(values, async (item, callback) => {
                    try {
                        let PDatax = item.Cart_Information.filter(function (ResultTemp) {
                            if (ResultTemp.SubCategoryID == SubCategoryID) {
                                Count = Count + ResultTemp.Product_Quantity
                            }
                        });
                        callback()
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Count);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Category_Sold_Calculation = (values, CategoryID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Count = 0;
                async.eachSeries(values, async (item, callback) => {
                    try {
                        let PDatax = item.Cart_Information.filter(function (ResultTemp) {
                            if (ResultTemp.CategoryID == CategoryID) {
                                Count = Count + ResultTemp.Product_Quantity
                            }
                        });
                        callback()
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Count);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Category = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    SNo: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Category.countDocuments(query).lean().exec();
                let Result = await Category.find(query).select('-_id -__v -updated_at').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.List_All_SubCategory = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CategoryID: values.CategoryID
                };
                let Result = await Category.findOne(query).select('-_id SubCategory_Data').lean().exec();
                resolve({ success: true, extras: { Data: Result.SubCategory_Data } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Validate_Discount_Code = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                if (values.DiscountID == null || values.DiscountID == undefined || values.DiscountID == '') {
                    values.DiscountID = '';
                }
                let query = {
                    DiscountID: {
                        $ne: values.DiscountID
                    },
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Discount_Code: values.Discount_Code,
                    Status: true
                };
                let Result = await Discounts.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_CODE_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Start_End_Time = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(values);
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format)
                let End_Date = moment(values.End_Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms');
                let now = moment();
                if (now.isBefore(Start_Date)) {
                    if (now.isBefore(End_Date)) {
                        resolve("Validated Successfully");
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.END_DATE_MUST_BE_FUTURE_DATE } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.START_DATE_MUST_BE_FUTURE_DATE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Inactive_Days_Filter_Params = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Boolify(values.Whether_Inactive_Days_Filter)) {
                    if (
                        values.Minimum_Days != null && isFinite(values.Minimum_Days)
                        && values.Maximum_Days != null && isFinite(values.Maximum_Days)
                    ) {
                        if (parseInt(values.Minimum_Days) < parseInt(values.Maximum_Days)) {
                            resolve("Validated Successfully");
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.MINIMUM_VALUE_MUST_BE_LESS_THAN_MAXIMUM_VALUE } })
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                    }
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Order_Filter_Params = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Boolify(values.Whether_Order_Filter)) {
                    if (
                        values.Minimum_Orders != null && isFinite(values.Minimum_Orders)
                        && values.Maximum_Orders != null && isFinite(values.Maximum_Orders)
                    ) {
                        if (parseInt(values.Minimum_Orders) < parseInt(values.Maximum_Orders)) {
                            resolve("Validated Successfully");
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.MINIMUM_VALUE_MUST_BE_LESS_THAN_MAXIMUM_VALUE } })
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                    }
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Age_Filter_Params = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Boolify(values.Whether_Discount_Age_Filter)) {
                    if (
                        values.Minimum_Age != null && isFinite(values.Minimum_Age)
                        && values.Maximum_Age != null && isFinite(values.Maximum_Age)
                    ) {
                        if (parseInt(values.Minimum_Age) < parseInt(values.Maximum_Age)) {
                            resolve("Validated Successfully");
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.MINIMUM_VALUE_MUST_BE_LESS_THAN_MAXIMUM_VALUE } })
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                    }
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Default_Params = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Discount_Percentage = parseFloat(values.Discount_Percentage);
                if (Discount_Percentage >= 1 && Discount_Percentage <= 100) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_PERCENTAGE_MUST_BE_BETWEEN_1_AND_100 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Create_Discount_Validate_Whether_Customer_Target_Discount = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (Boolify(values.Whether_Customer_Target_Discount)) {
                if (
                    values.Customer_Target_Type != null && values.Customer_Target_Type != undefined && isFinite(values.Customer_Target_Type) && !isNaN(values.Customer_Target_Type)
                ) {
                    let Customer_Target_Type = parseInt(values.Customer_Target_Type);
                    if (Customer_Target_Type === 4) {
                        //Selected Customer Offer
                        if (values.Targeted_Customers != null && values.Targeted_Customers.length > 0) {
                            async.eachSeries(values.Targeted_Customers, async (item, callback) => {
                                try {
                                    if (item.BuyerID != null) {
                                        let UserData = await CommonController.Check_For_Buyer(item);
                                        callback();
                                    } else {
                                        callback({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
                                    }
                                } catch (error) {
                                    callback(error);
                                }
                            }, async (err) => {
                                if (err) reject(err);
                                resolve("Validated Successfully");
                            });
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
                        }
                    } else {
                        //Other Offers
                        resolve("Validated Successfully");
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
                }
            } else {
                resolve("Validated Successfully");
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Create_Discount = (values, AdminData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    DiscountID: uuid.v4(),
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Discount_Code: values.Discount_Code,
                    Discount_Title: values.Discount_Title,
                    Minimum_Invoice_Amount: parseFloat(values.Minimum_Invoice_Amount),
                    Discount_Percentage: parseFloat(values.Discount_Percentage),
                    Max_Discount_Amount: parseFloat(values.Max_Discount_Amount),
                    Max_Discount_For_User: parseInt(values.Max_Discount_For_User),
                    Start_Date: moment(values.Start_Date, config.Take_Date_Format),
                    End_Date: moment(values.End_Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms'),
                    Whether_Referral_Discount: Boolify(values.Whether_Referral_Discount),
                    Whether_Discount_Age_Filter: Boolify(values.Whether_Discount_Age_Filter),
                    Minimum_Age: parseInt(values.Minimum_Age) || 0,
                    Maximum_Age: parseInt(values.Maximum_Age) || 0,
                    Whether_Order_Filter: Boolify(values.Whether_Order_Filter),
                    Minimum_Orders: parseInt(values.Minimum_Orders) || 0,
                    Maximum_Orders: parseInt(values.Maximum_Orders) || 0,
                    Whether_Inactive_Days_Filter: Boolify(values.Whether_Inactive_Days_Filter),
                    Minimum_Days: parseInt(values.Minimum_Days) || 0,
                    Maximum_Days: parseInt(values.Maximum_Days) || 0,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                if (Boolify(values.Whether_Customer_Target_Discount)) {
                    let Customer_Target_Type = await parseInt(values.Customer_Target_Type);
                    Data.Whether_Customer_Target_Discount = await true;
                    Data.Customer_Target_Type = await Customer_Target_Type;
                    if (Customer_Target_Type === 4) {
                        Data.Targeted_Customers = await values.Targeted_Customers;
                    }
                }
                let SaveResult = await Discounts(Data).save();
                let SaveVersion = await Discount_Versions(Data).save();
                SaveResult = await JSON.parse(JSON.stringify(SaveResult));
                resolve({ success: true, extras: { Status: "Discount Added Successfully" } });
                let StoreLog = await AdminLogController1.Create_Discount_Log(AdminData, SaveResult, CountryData, CityData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Discounts = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                };
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Count = await Discounts.countDocuments(query).lean().exec();
                let Result = await Discounts.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                // async.eachSeries(Result, async (item, callback) => {
                //     try {
                //         item.Linked_Users_Data = await AdminController.List_All_Discounts_Fetch_Discount_Mapped_Users(item.Targeted_Customers);
                //         callback();
                //     } catch (error) {
                //         callback(error);
                //     }
                // }, async (err) => {
                //     if (err) reject(err);
                //     resolve({ success: true, extras: { Count: Count, Data: Result } });
                // });
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.List_All_Discounts_Fetch_Discount_Mapped_Users = (Targeted_Customers) => {
    return new Promise(async (resolve, reject) => {
        try {
            let USERID_Array = [];
            await Targeted_Customers.forEach(ele => USERID_Array.push(ele.USERID));
            let query = {
                BuyerID: {
                    $in: USERID_Array
                }
            };
            let sortOptions = {
                Name: 1
            }
            let Result = await Buyers.find(query).select('-_id -USER_SESSIONS -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().exec();
            async.eachSeries(Result, async (item, callback) => {
                try {
                    // let rcquery = {
                    //     Whether_Referral: true,
                    //     "Referral_Details.USERID": item.USERID
                    // };
                    // item.Referral_Counts = await Users.countDocuments(rcquery).lean();
                    item.User_Image_Data = await CommonController.Common_Image_Response_Single_Image(item.User_Image_Available, item.User_Image_Data);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve(Result);
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

AdminController.Update_Discount_Validate_Start_End_Time = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(values);
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format)
                let End_Date = moment(values.End_Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms');
                let now = moment();
                if (now.isBefore(End_Date)) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.END_DATE_MUST_BE_FUTURE_DATE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Discount = (values, AdminData, OldDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DiscountID: values.DiscountID
                };
                let fndupdchanges = {
                    $set: {
                        Discount_Code: values.Discount_Code,
                        Discount_Title: values.Discount_Title,
                        Minimum_Invoice_Amount: parseFloat(values.Minimum_Invoice_Amount),
                        Discount_Percentage: parseFloat(values.Discount_Percentage),
                        Max_Discount_Amount: parseFloat(values.Max_Discount_Amount),
                        Max_Discount_For_User: parseInt(values.Max_Discount_For_User),
                        Start_Date: moment(values.Start_Date, config.Take_Date_Format),
                        End_Date: moment(values.End_Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms'),
                        Whether_Referral_Discount: Boolify(values.Whether_Referral_Discount),
                        Whether_Discount_Age_Filter: Boolify(values.Whether_Discount_Age_Filter),
                        Minimum_Age: parseInt(values.Minimum_Age) || 0,
                        Maximum_Age: parseInt(values.Maximum_Age) || 0,
                        Whether_Order_Filter: Boolify(values.Whether_Order_Filter),
                        Minimum_Orders: parseInt(values.Minimum_Orders) || 0,
                        Maximum_Orders: parseInt(values.Maximum_Orders) || 0,
                        Whether_Inactive_Days_Filter: Boolify(values.Whether_Inactive_Days_Filter),
                        Minimum_Days: parseInt(values.Minimum_Days) || 0,
                        Maximum_Days: parseInt(values.Maximum_Days) || 0,
                        Whether_Customer_Target_Discount: false,
                        updated_at: new Date()
                    },
                    $inc: {
                        Version: 1
                    }
                };
                if (Boolify(values.Whether_Customer_Target_Discount)) {
                    let Customer_Target_Type = await parseInt(values.Customer_Target_Type);
                    fndupdchanges.$set.Whether_Customer_Target_Discount = await true;
                    fndupdchanges.$set.Customer_Target_Type = await Customer_Target_Type;
                    if (Customer_Target_Type === 4) {
                        fndupdchanges.$set.Targeted_Customers = await values.Targeted_Customers;
                    }
                }
                let fndupdoptions = {
                    new: true
                }
                let NewDiscountData = await Discounts.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                let SaveVersion = await Discount_Versions(NewDiscountData).save();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Update_Discount_Log(AdminData, OldDiscountData, NewDiscountData, CountryData, CityData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Inactivate_Discount = (values, AdminData, OldDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DiscountID: values.DiscountID
                };
                let fndupdchanges = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    },
                    $inc: {
                        Version: 1
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDiscountData = await Discounts.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                let SaveVersion = await Discount_Versions(NewDiscountData).save();
                resolve({ success: true, extras: { Status: "Inactivated Successfully" } });
                let StoreLog = await AdminLogController1.Inactivate_Discount_Log(AdminData, OldDiscountData, NewDiscountData, CountryData, CityData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Activate_Discount = (values, AdminData, OldDiscountData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DiscountID: values.DiscountID
                };
                let fndupdchanges = {
                    $set: {
                        Status: true,
                        updated_at: new Date()
                    },
                    $inc: {
                        Version: 1
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDiscountData = await Discounts.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                let SaveVersion = await Discount_Versions(NewDiscountData).save();
                resolve({ success: true, extras: { Status: "Activated Successfully" } });
                let StoreLog = await AdminLogController1.Activate_Discount_Log(AdminData, OldDiscountData, NewDiscountData, CountryData, CityData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Validate_App_Automated_Offer_Discount_Day = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = await JSON.parse(JSON.stringify(values));
                if (values.AutomatedOfferID == null || values.AutomatedOfferID == undefined || values.AutomatedOfferID == '') {
                    values.AutomatedOfferID = '';
                }
                let Discount_Day_Number = parseInt(values.Discount_Day_Number);
                let query = {
                    AutomatedOfferID: {
                        $ne: values.AutomatedOfferID
                    },
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Discount_Day_Number: Discount_Day_Number
                };
                let Result = await App_Automated_Offers.findOne(query).lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_DAY_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_App_Automated_Offer = (values, AdminData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Discount_Day_Number = parseInt(values.Discount_Day_Number);
                let Discount_Percentage = parseFloat(values.Discount_Percentage);
                let Discount_Description = values.Discount_Description;
                if (Discount_Percentage >= config.Minimun_App_Automated_Discount_Percentage && Discount_Percentage <= config.Maximum_App_Automated_Discount_Percentage) {
                    let Data = {
                        AutomatedOfferID: uuid.v4(),
                        CountryID: values.CountryID,
                        CityID: values.CityID,
                        Discount_Day_Number: Discount_Day_Number,
                        Discount_Description: Discount_Description,
                        Discount_Percentage: Discount_Percentage,
                        created_at: new Date(),
                        updated_at: new Date()
                    };
                    let SaveResult = await App_Automated_Offers(Data).save();
                    let SaveVersion = await App_Automated_Offer_Versions(Data).save();
                    SaveResult = await JSON.parse(JSON.stringify(SaveResult));
                    resolve({ success: true, extras: { Status: "Automated Offer Added Successfully" } });
                    let StoreLog = await AdminLogController1.Add_App_Automated_Offer_Log(AdminData, SaveResult, CountryData, CityData);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_PERCENTAGE_MUST_BE_BETWEEN_0_AND_100 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Automated_Offers = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    Status: true
                };
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Count = await App_Automated_Offers.countDocuments(query).lean().exec();
                let Result = await App_Automated_Offers.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_App_Automated_Offer = (values, AdminData, OldOfferData, CountryData, CityData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Discount_Percentage = parseFloat(values.Discount_Percentage);
                let Discount_Description = values.Discount_Description;
                if (Discount_Percentage >= config.Minimun_App_Automated_Discount_Percentage && Discount_Percentage <= config.Maximum_App_Automated_Discount_Percentage) {
                    let fndupdquery = {
                        AutomatedOfferID: values.AutomatedOfferID
                    };
                    let fndupdchanges = {
                        $set: {
                            Discount_Description: Discount_Description,
                            Discount_Percentage: Discount_Percentage,
                            updated_at: new Date()
                        },
                        $inc: {
                            Version: 1
                        }
                    };
                    let fndupdoptions = {
                        new: true
                    };
                    let NewOfferData = await App_Automated_Offers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                    let SaveVersion = await App_Automated_Offer_Versions(NewOfferData).save();
                    resolve({ success: true, extras: { Status: "Updated Successfully" } });
                    let StoreLog = await AdminLogController1.Update_App_Automated_Offer_Log(AdminData, OldOfferData, NewOfferData, CountryData, CityData);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.DISCOUNT_PERCENTAGE_MUST_BE_BETWEEN_0_AND_100 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Validate_Special_Offer_Number_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                if (values.SpecialOfferID == null || values.SpecialOfferID == undefined || values.SpecialOfferID == '') {
                    values.SpecialOfferID = ''
                }
                let query = {
                    SpecialOfferID: {
                        $ne: values.SpecialOfferID
                    },
                    SpecialOfferNumber: values.SpecialOfferNumber
                };
                let Result = await Special_Offers.findOne(query).select('-_id -__v').lean();
                if (Result == null) {
                    resolve({ success: true, extras: { Status: "Validated Successfully" } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.SERIAL_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Add_Special_Offer = (values, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    SpecialOfferID: uuid.v4(),
                    DiscountID: values.DiscountID,
                    SpecialOfferNumber: values.SpecialOfferNumber,
                    SpecialOfferName: values.SpecialOfferName,
                    ImageData: ImageData,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Special_Offers(Data).save();
                resolve({ success: true, extras: { Status: "Offer added successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_All_Special_Offers_with_Filter = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                };
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                }
                let Count = await Special_Offers.countDocuments(query).lean().exec();
                let Result = await Special_Offers.find(query).select('-_id -__v  -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        item.DiscountData = await CommonController.Check_for_Discount(item);
                        item.ImageData = await CommonController.Common_Image_Response_Single_Image(true, item.ImageData);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Update_Special_Offer = (values, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    SpecialOfferID: values.SpecialOfferID
                };
                let changes = {
                    $set: {
                        SpecialOfferNumber: values.SpecialOfferNumber,
                        SpecialOfferName: values.SpecialOfferName,
                        ImageData: ImageData,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Special_Offers.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


AdminController.Activate_Special_Offer = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    SpecialOfferID: values.SpecialOfferID
                };
                let changes = {
                    $set: {
                        Status: true,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Special_Offers.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Activate Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Inactivate_Special_Offer = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    SpecialOfferID: values.SpecialOfferID
                };
                let changes = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Special_Offers.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.List_Branch_Stack_Log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID,
                    Whether_Branch_ETA_Requested: true,
                    Whether_ETA_Requested: true,
                    Status: true
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Stack_Log.countDocuments(query).lean().exec();
                let Result = await Stack_Log.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                // let Data = [];
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let ProductData = await CommonController.Check_For_Product(item);
                        item.DiscountData = {
                            Branch_Discount_Percent: parseFloat(ProductData.Product_Data_For_Branch[0].Branch_Discount_Percent.toFixed(2)),
                            Branch_Discount_Price: parseFloat(ProductData.Product_Data_For_Branch[0].Branch_Discount_Price.toFixed(2)),
                            Bookafella_Discount_Percent: parseFloat(ProductData.Product_Data_For_Branch[0].Bookafella_Discount_Percent.toFixed(2)),
                            Bookafella_Discount_Price: parseFloat(ProductData.Product_Data_For_Branch[0].Bookafella_Discount_Price.toFixed(2)),
                            Total_Discount_Price: parseFloat(ProductData.Product_Data_For_Branch[0].Total_Discount_Price.toFixed(2)),
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.Update_Stack = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StackID: values.StackID,
                };
                let changes = {
                    $set: {
                        ETA: values.ETA,
                        Whether_ETA_Updated: true,
                        ETA_Description: values.ETA_Description,
                        ETA_Date: moment().add(values.ETA, 'd').format(),
                        updated_at: new Date()
                    }
                }
                let UpdateData = await Stack_Log.updateOne(query, changes).lean().exec()
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
                // send Notification
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.List_Branch_Products = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    'Product_Data_For_Branch.BranchID': values.BranchID,
                    'Product_Data_For_Branch.Status': true,
                    Status: true,
                    Whether_Deleted: {
                        $ne: true
                    }
                };
                let sortOptions = {
                    created_at: 1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Store_Products.countDocuments(query).lean().exec();
                let Result = await Store_Products.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                async.eachSeries(Result, async (ProductData, callback) => {
                    Country.findOne({ CountryID: ProductData.CountryID }).lean().exec().then((CountryData) => {
                        City.findOne({ CityID: ProductData.CityID }).lean().exec().then((CityData) => {
                            ZONES.findOne({ ZoneID: ProductData.ZoneID }).lean().exec().then((ZoneData) => {
                                CommonController.Common_Image_Response_Single_Image(true, ProductData.Product_Image_Data).then((Product_Image_Data) => {
                                    ProductData.Product_Image_Data = Product_Image_Data;
                                    ProductData.CountryData = CountryData;
                                    ProductData.CityData = CityData;
                                    ProductData.ZoneData = ZoneData;
                                    callback();
                                }).catch();
                            }).catch();
                        }).catch();
                    }).catch();
                }, function (err) {
                    if (!err) {
                        resolve({ success: true, extras: { Count: Count, Data: Result } });
                    }
                })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.Accept_Reject_Write_and_Earn = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    LogID: values.LogID
                }
                let changes = {
                    $set: {
                        Approve: values.Approve,
                        update_at: new Date()
                    }
                }
                let UpdateData = await Write_And_Earn_Log.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

AdminController.List_All_Write_and_Earn = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Approve: values.Approve
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Write_And_Earn_Log.countDocuments(query).lean().exec();
                let Result = await Write_And_Earn_Log.find(query).select('-_id -__v').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


AdminController.Amount_For_Write_and_Earn = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    LogID: values.LogID
                }
                let Result = await Write_And_Earn_Log.findOne(query).lean().exec();
                let Refund_Points = parseFloat(values.Points); // Add this amount to type 4
                let Refund_Amount = Refund_Points * config.Points_to_Amount / 100;
                let changes = {
                    Reward_Points: Refund_Points,
                    Reward_Amount: Refund_Amount,
                    updated_at: new Date()
                };
                let SaveData = await Write_And_Earn_Log.updateOne(query, changes).lean().exec()
                let BuyersQuery = {
                    BuyerID: Result.BuyerID
                }
                let BuyerData = await Buyers.findOne(BuyersQuery).lean().exec();
                let Data = {
                    EarnData: Result,
                    BuyerData: BuyerData,
                    Amount: Refund_Amount,
                    Points: Refund_Points,
                    BuyerData: BuyerData,
                    Convertion: config.Points_to_Amount
                }
                let WData = {
                    LogID: uuid.v4(),
                    BuyerID: Result.BuyerID,
                    Type: 1, // Points Credited for Write And Review 
                    Points: Refund_Points,
                    Data: Data,
                    Time: new Date()
                }
                let SData = {
                    LogID: uuid.v4(),
                    BuyerID: Result.BuyerID,
                    Type: 15, // Amount Credited for Write And Review 
                    Amount: Refund_Amount,
                    Data: Data,
                    Time: new Date()
                }
                let Resultlog = Buyer_Points_Log(WData).save();
                let Resultlog1 = Buyer_Share_Logs(SData).save();
                let BuyerChanges = {
                    $inc: {
                        Available_Points: Refund_Points,
                        Total_Points: Refund_Points,
                        Available_Amount: Refund_Amount,
                        Total_Amount: Refund_Amount
                    },
                };
                let fndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let updatBuyerData = await Buyers.findOneAndUpdate(BuyersQuery, BuyerChanges, fndupdoptions).lean().exec();

                //deduct amount from company wallet 

                let WDataw = {
                    LogID: uuid.v4(),
                    AdminID: values.AdminID,
                    Type: 3, // amount deduct from Bookafella Admin wallet for Write And Review
                    Amount: Refund_Amount,
                    Data: Data,
                    Time: new Date()
                }
                let Resultlogw = Company_Wallet_Log(WDataw).save();
                let Cfndupdquery = {

                }
                let Cfndupdchanges = {
                    $inc: {
                        Available_Amount: Refund_Amount * -1,
                        Withdrawn_Amount: Refund_Amount
                    }
                }
                let Cfndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let CfindupdateData = await Company_Wallet.findOneAndUpdate(Cfndupdquery, Cfndupdchanges, Cfndupdoptions).select('-_id -__v').lean();
                let SendNotification = await FcmController.SendNotification_Amount_For_Write_And_Earn(BuyerData, Result, Refund_Points, Refund_Amount)
                resolve({ success: true, extras: { Status: "Amount Credited Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

AdminController.Edit_Admin_Users = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AdminID: values.XAdminID
                };
                let changes = {
                    $set: {
                        Name: values.Name
                    }
                }
                let SaveData = await Admin_User.updateOne(query, changes).lean().exec();
                resolve({ success: true, extras: { Status: 'Updated Successfully' } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

export default AdminController;

//Convert String to title case
let TitleCase = (str) => {
    let myArr = str.toLowerCase().split(" ");
    for (let a = 0; a < myArr.length; a++) {
        myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
    }
    return myArr.join(" ");
}
let UpperCase = (str) => {
    return str.toUpperCase();
}
let LowerCase = (str) => {
    return str.toLowerCase();
}
