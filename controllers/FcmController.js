let FcmController = function () { };


import FCM from "fcm-push";
import ApiMessages from "../models/ApiMessages";
import config from "../config/config.js";
import CommonController from "./CommonController";
import Devices from "../models/Devices";
import Driver_Devices from "../models/Driver_Devices";
import Buyer_Notification_Log from "../models/Buyer_Notification_Log";
import async from "async";
import uuid from "uuid";
import Custom_Notification_Log from "../models/Custom_Notification_Log";
import Buyer_Orders from "../models/Buyer_Orders";
import Write_And_Earn_Log from "../models/Write_And_Earn_Log";

let customer_fcm = new FCM(config.firebase.customer_app.serverkey);
let driver_fcm = new FCM(config.firebase.driver_app.serverkey);

FcmController.SendNotification_Credit_Amount = (BuyerData, Refund_Amount, Refund_Points) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    OrderID: values.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(quer).lean().exec()
                let NotificationTitle = 'Points Credited';
                let NotificationBody = `Congratulations...!, \nPoints `+Refund_Points+` is credited to you and on convertion amount of Rs.`+Refund_Amount+` credited to your wallet. \n--Thank you.`;
                let NotificationData = {
                    type: 13,
                    Amount: Refund_Amount,
                    Points: Refund_Points,
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Cancel_Order_Return_Initiate = (BuyerData, LogData, Refund_Points, Refund_Amount) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    OrderID: values.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(quer).lean().exec()
                let NotificationTitle = 'Points Credited';
                let NotificationBody = `Congratulations...!, \nPoints `+Refund_Points+` is credited to you and on convertion amount of Rs.`+Refund_Amount+` credited to your wallet. \n--Thank you.`;
                let NotificationData = {
                    type: 13,
                    LogData: LogData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Accept_Reject_Write_and_Earn = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    LogID: values.LogID
                }
                let LogData = await Write_And_Earn_Log.findOne(quer).lean().exec()
                let NotificationTitle = '';
                let NotificationBody = '';
                let Type = 11
                if (parseInt(values.Approve) == 2) {
                    NotificationTitle = 'Write And Earn is not approved'
                    NotificationBody = `Hi, \nYour review submited for book `+ LogData.Product_Name+` in write and review is not approved.\n--Thank You.`;
                    Type = 11
                } else {
                    Type = 12
                    NotificationBody = `Hi, \nYour review submited for book `+ LogData.Product_Name+` in write and review not approved.\n--Thank You.`;
                    NotificationTitle = 'Write And Earn is approved'
                }
                let NotificationData = {
                    type: Type,
                    LogData: LogData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(LogData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Collect_COD_Amount_From_Driver = (Amount_Collected, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'COD Amount submitted';
                let NotificationBody = `Hi Driver, \nThank you for submiting COD amount Collected. Submited Amount is ` + Amount_Collected.Amount_Collected;
                let NotificationData = {
                    type: 4,
                    CartData: Cartinfo,
                    Store_Information: OrderData.Branch_Information,
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };

                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let DeviceData = await Driver_Devices.findOne({ DeviceID: DriverData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Driver_App_Notification(Data);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Cancel_Order_Return_Initiate = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    OrderID: values.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(quer).lean().exec()
                let NotificationTitle = 'Order return initiated';
                let NotificationBody = `Hi, Return for your Order with Order number: ` + OrderData.Order_Number + ` is Initiated by Team Bookafella. Please maintain item in orginal condition. \n--Thank you.`;
                let Cartinfo = []
                for (let iterate of OrderData.Cart_Information) {
                    let Data = {
                        ProductID: iterate.ProductID,
                        Unique_ProductID: iterate.Unique_ProductID,
                        Product_Name: iterate.Product_Name,
                        Product_Image_Data: await CommonController.Common_Image_Response_Single_Image(true, iterate.Product_Image_Data),
                        Avg_Rating: iterate.Avg_Rating,
                        Total_Rating_Count: iterate.Total_Rating_Count, // rating Data in another Schema //rating log
                        Total_Final_Price: iterate.Total_Final_Price,
                    }
                    Cartinfo.push(Data)
                }
                let NotificationData = {
                    type: 10,
                    CartData: Cartinfo,
                    Store_Information: OrderData.Branch_Information,
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Cancel_Order_Refund = (values, Cancel_Calculation) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    OrderID: values.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(quer).lean().exec()
                let NotificationTitle = 'Order refund initiated';
                let NotificationBody = `Hi, Refund for your Order with Order number: ` + OrderData.Order_Number + ` is Initiated by Team Bookafella. The amount ` + Cancel_Calculation.Refund_Amount + ` will is processed soon to Orginal mode of payment. \n --Thank You.`;
                let Cartinfo = []
                for (let iterate of OrderData.Cart_Information) {
                    let Data = {
                        ProductID: iterate.ProductID,
                        Unique_ProductID: iterate.Unique_ProductID,
                        Product_Name: iterate.Product_Name,
                        Product_Image_Data: await CommonController.Common_Image_Response_Single_Image(true, iterate.Product_Image_Data),
                        Avg_Rating: iterate.Avg_Rating,
                        Total_Rating_Count: iterate.Total_Rating_Count, // rating Data in another Schema //rating log
                        Total_Final_Price: iterate.Total_Final_Price,
                    }
                    Cartinfo.push(Data)
                }
                let NotificationData = {
                    type: 9,
                    Cancel_Charge_info: Cancel_Calculation,
                    CartData: Cartinfo,
                    Store_Information: OrderData.Branch_Information,
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.SendNotification_Accept_Reject_Cancelled_Orders_By_Buyers = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let quer = {
                    OrderID: values.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(quer).lean().exec()
                let NotificationTitle = '';
                let NotificationBody = '';
                let Cartinfo = []
                for (let iterate of OrderData.Cart_Information) {
                    let Data = {
                        ProductID: iterate.ProductID,
                        Unique_ProductID: iterate.Unique_ProductID,
                        Product_Name: iterate.Product_Name,
                        Product_Image_Data: await CommonController.Common_Image_Response_Single_Image(true, iterate.Product_Image_Data),
                        Avg_Rating: iterate.Avg_Rating,
                        Total_Rating_Count: iterate.Total_Rating_Count, // rating Data in another Schema //rating log
                        Total_Final_Price: iterate.Total_Final_Price,
                    }
                    Cartinfo.push(Data)
                }
                let Type = 7
                if (parseInt(values.Whether_Cancellation_Prodessed) == 1) {
                    NotificationTitle = 'Cancel request accepted'
                    NotificationBody = `Hi, Cancel request for your Order with Order number: ` + OrderData.Order_Number + ` is accepted by Team Bookafella`;
                    Type = 7
                } else {
                    Type = 8
                    NotificationBody = `Hi, Cancel request for your Order with Order number: ` + OrderData.Order_Number + ` is not accepted by Team Bookafella`;
                    NotificationTitle = 'Cancel request Rejetced'
                }
                let NotificationData = {
                    type: Type,
                    CartData: Cartinfo,
                    Store_Information: OrderData.Branch_Information,
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Send_Buyer_Custom_Notifications = (notificationResult, registration_ids) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = notificationResult.NotificationTitle;
                let NotificationBody = notificationResult.NotificationBody;
                let NotificationData = {
                    type: 6,
                    description: "Custom Notification"
                    // InstantOrderRequestData: InstantOrderRequestData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let SendingNotification = await FcmController.Send_Customer_Multiple_Apps_Notification(Data, registration_ids);
                resolve(SendingNotification);
                async.eachSeries(notificationResult.BuyerID_Array, async (item, callback) => {
                    try {
                        let SaveData = {
                            NotificationID: uuid.v4(),
                            BuyerID: item,
                            NotificationData: Data.NotificationData,
                            NotificationTitle: Data.NotificationTitle,
                            NotificationBody: Data.NotificationBody,
                            Created_at: new Date()
                        }
                        let SaveNotification = await Buyer_Notification_Log(SaveData).save();
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    let QueryUpdate = {
                        NotificationID: notificationResult.NotificationID
                    }
                    let changes = {
                        $set: {
                            NotificationData: Data.NotificationData,
                            Latest_Send_Time: new Date(),
                            updated_at: new Date()
                        },
                        $push: {
                            Send_Time_Log: {
                                Send_Time: new Date()
                            }
                        }
                    };
                    let UpdateData = await Custom_Notification_Log.updateOne(QueryUpdate, changes).lean().exec()
                });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Notification_For_Referal_First_Order = (BuyerDatax, New_BuyerData, Amount, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Referal Amount Credited for Order.';
                let NotificationBody = `Congrats ${BuyerDatax.Buyer_Name},\n Referal Amount Rs. ${Amount} is credited to your wallet as your friend ${New_BuyerData.Buyer_Name} Order in Bookafella.`;
                let NotificationData = {
                    type: 5, //Referal Amount for first Order
                    BuyerData: {
                        Buyer_Name: BuyerDatax.Buyer_Name,
                        Buyer_Email: BuyerDatax.Buyer_Email,
                        Buyer_CountryCode: BuyerDatax.Buyer_CountryCode,
                        Buyer_PhoneNumber: BuyerDatax.Buyer_PhoneNumber,
                        Ref_PhoneNumber: BuyerDatax.Ref_PhoneNumber,
                    },
                    Amount: Amount,
                    New_BuyerData: {
                        Buyer_Name: New_BuyerData.Buyer_Name,
                        Buyer_Email: New_BuyerData.Buyer_Email,
                        Buyer_CountryCode: New_BuyerData.Buyer_CountryCode,
                        Buyer_PhoneNumber: New_BuyerData.Buyer_PhoneNumber,
                        Ref_PhoneNumber: New_BuyerData.Ref_PhoneNumber,
                    },
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(BuyerDatax);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Notification_For_Referal_First_Login = (BuyerDatax, New_BuyerData, Amount) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Referal Amount Credited .';
                let NotificationBody = `Congrats ${BuyerDatax.Buyer_Name},\n Referal Amount Rs. ${Amount} is credited to your wallet as your friend ${New_BuyerData.Buyer_Name} Joined in Bookafella.`;
                let NotificationData = {
                    type: 4, //Referal Amount for first login
                    BuyerData: {
                        Buyer_Name: BuyerDatax.Buyer_Name,
                        Buyer_Email: BuyerDatax.Buyer_Email,
                        Buyer_CountryCode: BuyerDatax.Buyer_CountryCode,
                        Buyer_PhoneNumber: BuyerDatax.Buyer_PhoneNumber,
                        Ref_PhoneNumber: BuyerDatax.Ref_PhoneNumber,
                    },
                    Amount: Amount,
                    New_BuyerData: {
                        Buyer_Name: New_BuyerData.Buyer_Name,
                        Buyer_Email: New_BuyerData.Buyer_Email,
                        Buyer_CountryCode: New_BuyerData.Buyer_CountryCode,
                        Buyer_PhoneNumber: New_BuyerData.Buyer_PhoneNumber,
                        Ref_PhoneNumber: New_BuyerData.Ref_PhoneNumber,
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(BuyerDatax);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Order_Delivered_Notification = (OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Order Delivered.';
                let NotificationBody = `Your order with Order Number: ${OrderData.Order_Number} is Delivered by ${DriverData.Driver_Name}. Please provide rating for the books provided. Thank you.`;
                let NotificationData = {
                    type: 3, //Order picked up
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    },
                    DriverData: {
                        DriverID: DriverData.DriverID,
                        CountryID: DriverData.CountryID,
                        CityID: DriverData.CityID,
                        ZoneID: DriverData.ZoneID,
                        Driver_Name: DriverData.Driver_Name,
                        Driver_Country_Code: DriverData.Driver_Country_Code,
                        Driver_Phone_Number: DriverData.Driver_Phone_Number,
                        Driver_EmailID: DriverData.Driver_EmailID,
                        Driver_Timings: DriverData.Driver_Timings,
                        ImageAvailable: DriverData.ImageAvailable,
                        ImageData: await CommonController.Common_Image_Response_Single_Image(DriverData.ImageAvailable, DriverData.ImageData),
                        Location: DriverData.Location,
                        Driver_Current_Location: DriverData.Driver_Current_Location,
                        Driver_Current_Location_Point: DriverData.Driver_Current_Location_Point,
                        Point: DriverData.Point,
                        VehicleID: DriverData.VehicleID,
                        VehicleNumber: DriverData.VehicleNumber
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Order_PickUp_Notification = (OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Order Picked up.';
                let NotificationBody = `Your order with Order Number: ${OrderData.Order_Number} is Picked up by ${DriverData.Driver_Name} and it will be delivered soon.`;
                let NotificationData = {
                    type: 2, //Order picked up
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    },
                    DriverData: {
                        DriverID: DriverData.DriverID,
                        CountryID: DriverData.CountryID,
                        CityID: DriverData.CityID,
                        ZoneID: DriverData.ZoneID,
                        Driver_Name: DriverData.Driver_Name,
                        Driver_Country_Code: DriverData.Driver_Country_Code,
                        Driver_Phone_Number: DriverData.Driver_Phone_Number,
                        Driver_EmailID: DriverData.Driver_EmailID,
                        Driver_Timings: DriverData.Driver_Timings,
                        ImageAvailable: DriverData.ImageAvailable,
                        ImageData: await CommonController.Common_Image_Response_Single_Image(DriverData.ImageAvailable, DriverData.ImageData),
                        Location: DriverData.Location,
                        Driver_Current_Location: DriverData.Driver_Current_Location,
                        Driver_Current_Location_Point: DriverData.Driver_Current_Location_Point,
                        Point: DriverData.Point,
                        VehicleID: DriverData.VehicleID,
                        VehicleNumber: DriverData.VehicleNumber
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Order_Accepted_From_BookStore = (OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Order Accepted.';
                let NotificationBody = `Your order with Order Number: ${OrderData.Order_Number} is Accepted by ${OrderData.Branch_Information.Branch_Name}.`;
                let NotificationData = {
                    type: 1, //Order Accepted
                    OrderData: {
                        OrderID: OrderData.OrderID,
                        BuyerID: OrderData.BuyerID,
                        Order_Status: OrderData.Order_Status,
                        Payment_Type: OrderData.Payment_Type, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
                        Order_Number: OrderData.Order_Number, // 12 digits random number
                        Payment_Status: OrderData.Payment_Status, // 1- initial, 2- fail, 3- Success, 4- COD                    
                    }
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let BuyerData = await CommonController.Check_For_Buyer(OrderData);
                let DeviceData = await Devices.findOne({ DeviceID: BuyerData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data, BuyerData);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



FcmController.Main_Order_Slot_Not_Available_Send_Notification = (MainOrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'Slot not available.';
                let NotificationBody = `Slot not Available for ${MainOrderData.Main_Order_Number}. Please select new slot time.`;
                let NotificationData = {
                    type: 1,
                    MainOrderData: MainOrderData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let UserData = await CommonController.Check_Only_User(MainOrderData);
                let DeviceData = await Devices.findOne({ DeviceID: UserData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Customer_App_Notification(Data);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Trip_Allocate_New_Driver_Send_Driver_Notification = (TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'New trip allocated';
                let NotificationBody = 'New trip allocated from admin';
                let NotificationData = {
                    type: 2,
                    description: "New trip allocated from admin",
                    TripData: TripData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let DriverData = await CommonController.Check_Only_Driver(TripData);
                let DeviceData = await Driver_Devices.findOne({ DeviceID: UserData.DeviceID }).lean();
                if (DeviceData == null) {
                    //Device not available
                    resolve("Notification not sent")
                } else {
                    //Device available
                    if (DeviceData.FCM_Token == '') {
                        //FCM not available
                        resolve("Notification not sent")
                    } else {
                        Data.FCM_Token = DeviceData.FCM_Token;
                        let SendingNotification = await FcmController.Send_Driver_App_Notification(Data);
                        resolve(SendingNotification);
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Cancell_Order_Request_Send_Driver_Notifications = (DriverData, CancellRequestData, FCM_Token) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'New Cancell Request';
                let NotificationBody = 'You have recieved new cancell request.';
                let NotificationData = {
                    type: 3,
                    description: "Cancell Request",
                    CancellRequestData: CancellRequestData
                };
                let Data = {
                    FCM_Token: FCM_Token,
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let SendingNotification = await FcmController.Send_Driver_App_Notification(Data);
                resolve(SendingNotification);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

FcmController.Instant_Order_Request_Send_Driver_Notifications = (InstantOrderRequestData, registration_ids) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let NotificationTitle = 'New Gig Request';
                let NotificationBody = 'You have recieved new gig request.';
                let NotificationData = {
                    type: 1,
                    description: "Instant Request"
                    // InstantOrderRequestData: InstantOrderRequestData
                };
                let Data = {
                    NotificationTitle: NotificationTitle,
                    NotificationBody: NotificationBody,
                    NotificationData: NotificationData
                };
                let SendingNotification = await FcmController.Send_Driver_Multiple_App_Notification(Data, registration_ids);
                resolve(SendingNotification);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}





/************
 * 
 * Common Send Notification Body
 * 
 */

FcmController.Send_Customer_App_Notification = (Data, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log('FCM -->' + Data)
                let msg = {
                    to: Data.FCM_Token,
                    collapse_key: 'your_collapse_key',
                    priority: 'high',
                    data: Data.NotificationData,
                    notification: {
                        title: Data.NotificationTitle,
                        body: Data.NotificationBody,
                        sound: "default",
                        alert: "true",
                        vibrate: true,
                        badge: 1
                    }
                };
                let Result = await customer_fcm.send(msg);
                let SaveData = {
                    NotificationID: uuid.v4(),
                    BuyerID: BuyerData.BuyerID,
                    NotificationData: Data.NotificationData,
                    NotificationTitle: Data.NotificationTitle,
                    NotificationBody: Data.NotificationBody,
                    Created_at: new Date()
                }
                let SaveNotification = await Buyer_Notification_Log(SaveData).save();
                resolve("sent notification");
            } catch (error) {
                console.error("Firebase Error");
                console.error(error);
                resolve("sent notification");
            }
        });
    });
}

FcmController.Send_Customer_Multiple_Apps_Notification = (Data, registration_ids) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (registration_ids.length > 0) {
                    let msg = {
                        registration_ids: registration_ids,
                        collapse_key: 'your_collapse_key',
                        priority: 'high',
                        data: Data.NotificationData,
                        notification: {
                            title: Data.NotificationTitle,
                            body: Data.NotificationBody,
                            sound: "default",
                            alert: "true",
                            vibrate: true,
                            badge: 1
                        }
                    };
                    let Result = await customer_fcm.send(msg);
                    resolve("sent notification");
                } else {
                    resolve("sent notification");
                }
            } catch (error) {
                console.error("Firebase Error");
                console.error(error);
                resolve("sent notification");
            }
        });
    });
}

FcmController.Send_Driver_App_Notification = Data => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let msg = {
                    to: Data.FCM_Token,
                    collapse_key: 'your_collapse_key',
                    priority: 'high',
                    data: Data.NotificationData,
                    android_channel_id: "BookafellaDriver",
                    // notification: {
                    //     title: Data.NotificationTitle,
                    //     body: Data.NotificationBody,
                    //     sound: "buzzer_message",
                    //     channel_id: 'BookafellaDriver',
                    //     android_channel_id: "BookafellaDriver",
                    //     alert: "true",
                    //     vibrate: true,
                    //     badge: 1
                    // },
                    android: {
                        ttl: 3600 * 1000,
                        notification: {
                            color: '#68375f',
                            sound: 'buzzer_message',
                            channel_id: 'BookafellaDriver', // important to get custom sound
                            android_channel_id: "BookafellaDriver",
                            alert: "true",
                            vibrate: true,
                            badge: 1
                        }
                    }
                };
                let Result = await driver_fcm.send(msg);
                resolve("sent notification");
            } catch (error) {
                console.error("Firebase Error");
                console.error(error);
                resolve("sent notification");
            }
        });
    });
}

// FcmController.Send_Driver_App_Notification = Data => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let msg = {
//                     to: Data.FCM_Token,
//                     collapse_key: 'your_collapse_key',
//                     priority: 'high',
//                     data: Data.NotificationData,
//                     notification: {
//                         title: Data.NotificationTitle,
//                         body: Data.NotificationBody,
//                         sound: "buzzer_message",
//                         alert: "true",
//                         vibrate: true,
//                         badge: 1
//                     }
//                 };
//                 let Result = await driver_fcm.send(msg);
//                 resolve("sent notification");
//             } catch (error) {
//                 console.error("Firebase Error");
//                 console.error(error);
//                 resolve("sent notification");
//             }
//         });
//     });
// }

FcmController.Send_Driver_Multiple_App_Notification = (Data, registration_ids) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (registration_ids.length > 0) {
                    let msg = {
                        registration_ids: registration_ids,
                        collapse_key: 'your_collapse_key',
                        priority: 'high',
                        data: Data.NotificationData,
                        android_channel_id: "BookafellaDriver",
                        // notification: {
                        //     title: Data.NotificationTitle,
                        //     body: Data.NotificationBody,
                        //     sound: "buzzer_message",
                        //     channel_id: 'BookafellaDriver',
                        //     android_channel_id: "BookafellaDriver",
                        //     alert: "true",
                        //     vibrate: true,
                        //     badge: 1
                        // },
                        android: {
                            ttl: 3600 * 1000,
                            notification: {
                                color: '#68375f',
                                sound: 'buzzer_message',
                                android_channel_id: "BookafellaDriver",
                                channel_id: 'BookafellaDriver', // important to get custom sound
                                alert: "true",
                                vibrate: true,
                                badge: 1
                            }
                        }
                    };
                    let Result = await driver_fcm.send(msg);
                    resolve("sent notification");
                } else {
                    resolve("sent notification");
                }
            } catch (error) {
                console.error("Firebase Error");
                console.error(error);
                resolve("sent notification");
            }
        });
    });
}

// FcmController.Send_Driver_Multiple_App_Notification = (Data, registration_ids) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 if (registration_ids.length > 0) {
//                     let msg = {
//                         registration_ids: registration_ids,
//                         collapse_key: 'your_collapse_key',
//                         priority: 'high',
//                         data: Data.NotificationData,
//                         notification: {
//                             title: Data.NotificationTitle,
//                             body: Data.NotificationBody,
//                             sound: "buzzer_message",
//                             alert: "true",
//                             vibrate: true,
//                             badge: 1
//                         }
//                     };
//                     let Result = await driver_fcm.send(msg);
//                     resolve("sent notification");
//                 } else {
//                     resolve("sent notification");
//                 }
//             } catch (error) {
//                 console.error("Firebase Error");
//                 console.error(error);
//                 resolve("sent notification");
//             }
//         });
//     });
// }

export default FcmController;