let FirebaseController = function () { };
import * as admin from "firebase-admin";
import * as serviceAccountKey from "../config/serviceAccountKey.json";
import config from "../config/config";
import ApiMessages from "../models/ApiMessages.js";

admin.initializeApp({
    credential: admin.credential.cert(serviceAccountKey),
    databaseURL: config.firebase.customer_app.databaseURL
});

FirebaseController.Check_for_Firebase_User = uid => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Result = await admin.auth().getUser(uid);
                resolve(Result);
                console.log(Result)
            } catch (error) {
                console.error("Firebase user error:-", error);
                reject({ success: false, extras: { msg: ApiMessages.INVALID_FIREBASE_USER } })
            }
        });
    });
}

FirebaseController.Check_for_Firebase_ID_Token = (uid, idToken) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                console.log(1)
                let Result = await admin.auth().verifyIdToken(idToken);
                if (uid === Result.uid) {
                    resolve(Result);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.FIREBASE_USER_AND_TOKEN_USER_MISMATCH } })
                }
            } catch (error) {
                console.error("Firebase token error:-", error);
                reject({ success: false, extras: { msg: ApiMessages.INVALID_FIREBASE_TOKEN } })
            }
        });
    });
}

export default FirebaseController;