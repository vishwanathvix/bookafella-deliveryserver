let DeviceController = function () { };
//Dependencies
import uuid from "uuid";
import moment from "moment";

//Models
import Devices from "../models/Devices";
import ApiMessages from "../models/ApiMessages";
import Installed_Devices from "../models/Installed_Devices";
import App_Versions_Settings from "../models/App_Versions_Settings";
import CommonController from "./CommonController";
import Driver_Devices from "../models/Driver_Devices";
import Driver_Installed_Devices from "../models/Driver_Installed_Devices";
import config from "../config/config";
import Order_Delivery_Setting from "../models/Order_Delivery_Setting";

DeviceController.Update_Device_FCM_Token = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DeviceID: DeviceData.DeviceID
                };
                let changes = {
                    $set: {
                        FCM_Token: values.FCM_Token,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Devices.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Driver_Update_FCM_Token = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DeviceID: DeviceData.DeviceID
                };
                let changes = {
                    $set: {
                        FCM_Token: values.FCM_Token,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Driver_Devices.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Customer_Update_FCM_Token = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DeviceID: DeviceData.DeviceID
                };
                let changes = {
                    $set: {
                        FCM_Token: values.FCM_Token,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Devices.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Fetch_Order_Delivery_Settings = () => {
    return new Promise(async (resolve, reject) => {
        try {
            let Data = {
                Gig_Driver_KMS: config.Gig_Driver_KMS,
                Driver_Order_Near_KMS: config.Driver_Order_Near_KMS
            };
            let query = {
                Status: true
            };
            let Result = await Order_Delivery_Setting.findOne(query).select('-_id -__v -Version -Status -Operation_Percentage -created_at -updated_at -DeliverySettingID').lean();
            if (Result === null) {
                resolve(Data);
            } else {
                resolve(Result);
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


DeviceController.Validate_Driver_Splash_Screen_App_Versions_and_Send_Response = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DeviceType = parseInt(values.DeviceType) || 3;
                let AppVersion = parseFloat(values.AppVersion) || 1;
                let RazorpayData = await JSON.parse(JSON.stringify(config.razorpay));
                delete RazorpayData.host;
                delete RazorpayData.baseURL;
                delete RazorpayData.merchant_id;
                delete RazorpayData.key_secret;
                delete RazorpayData.razorpayx_account_number;
                delete RazorpayData.webhook_secret;
                let PubnubData = await JSON.parse(JSON.stringify(config.Pubnub));
                let INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST = config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST;
                let INSTANT_DRIVER_REQUEST_ACCEPTED_TIME = config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME;
                let INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH = config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH;
                //let MAIN_ORDER_RETRY_WINDOW_SLOT_TIME = config.MAIN_ORDER_RETRY_WINDOW_SLOT_TIME;
                //let DeliverySettingData = await DeviceController.Fetch_Order_Delivery_Settings();
                let AppSettingData = await CommonController.Fetch_App_Driver_Settings();
                if (DeviceType == 3) {
                    let Data = {
                        success: true,
                        extras: {
                            Status: "Device Splash Screen Completed Successfully",
                            ApiKey: DeviceData.ApiKey,
                            Whether_Latest_Version: true,
                            RazorpayData: RazorpayData,
                            PubnubData: PubnubData,
                            INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST: INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST,
                            INSTANT_DRIVER_REQUEST_ACCEPTED_TIME: INSTANT_DRIVER_REQUEST_ACCEPTED_TIME,
                            INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH: INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH,
                            //MAIN_ORDER_RETRY_WINDOW_SLOT_TIME: MAIN_ORDER_RETRY_WINDOW_SLOT_TIME,
                            //DeliverySettingData: DeliverySettingData,
                            AppSettingData: AppSettingData
                        }
                    }
                    resolve(Data);
                } else {
                    let VersionData = await CommonController.Common_Register_or_Get_App_versions();
                    let CheckingVersion = (DeviceType == 1) ? VersionData.Driver_Android_Version : VersionData.Driver_IOS_Version;
                    let Whether_Latest_Version = (AppVersion >= CheckingVersion) ? true : false;
                    let Data = {
                        success: true,
                        extras: {
                            Status: "Device Splash Screen Completed Successfully",
                            ApiKey: DeviceData.ApiKey,
                            Whether_Latest_Version: Whether_Latest_Version,
                            RazorpayData: RazorpayData,
                            PubnubData: PubnubData,
                            INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST: INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST,
                            INSTANT_DRIVER_REQUEST_ACCEPTED_TIME: INSTANT_DRIVER_REQUEST_ACCEPTED_TIME,
                            INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH: INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH,
                            //MAIN_ORDER_RETRY_WINDOW_SLOT_TIME: MAIN_ORDER_RETRY_WINDOW_SLOT_TIME,
                            //DeliverySettingData: DeliverySettingData,
                            AppSettingData: AppSettingData
                        }
                    }
                    resolve(Data);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Add_or_Update_Driver_Device_And_Get_Device_Information = (values, IPAddress) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ApiKey = uuid.v4();
                let query = {
                    DeviceID: values.DeviceID
                };
                let Result = await Driver_Devices.findOne(query).lean();
                let DeviceType = parseInt(values.DeviceType) || 3;
                if (Result == null) {
                    //New Device
                    let Interval = parseInt(moment().utcOffset(330).format('kk'));
                    let Data = {
                        ApiKey: ApiKey,
                        DeviceID: values.DeviceID,
                        DeviceType: DeviceType,
                        DeviceName: values.DeviceName,
                        AppVersion: values.AppVersion,
                        IPAddress: IPAddress,
                        InstallTime: new Date(),
                        Interval: Interval,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let DeviceResult = await Driver_Devices(Data).save();
                    resolve(JSON.parse(JSON.stringify(DeviceResult)));
                    if (DeviceType == 1 || DeviceType == 2) {
                        let SaveResult = await Driver_Installed_Devices(Data).save();
                    }
                } else {
                    //Existing Device
                    let changes = {
                        $set: {
                            ApiKey: ApiKey,
                            AppVersion: values.AppVersion,
                            IPAddress: IPAddress,
                            updated_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Driver_Devices.updateOne(query, changes).lean();
                    Result.ApiKey = ApiKey;
                    Result.AppVersion = values.AppVersion;
                    Result.IPAddress = IPAddress;
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Generate_DeviceID = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    DeviceID: uuid.v4()
                }
                resolve({ success: true, extras: { Data: Data } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Validate_Splash_Screen_App_Versions_and_Send_Response = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DeviceType = parseInt(values.DeviceType) || 1;
                let AppVersion = parseFloat(values.AppVersion) || 1;
                let VersionData = await CommonController.Common_Register_or_Get_App_versions();
                let CheckingVersion = (DeviceType == 1) ? VersionData.Android_Version : VersionData.IOS_Version;
                let Whether_Latest_Version = (AppVersion >= CheckingVersion) ? true : false;
                resolve({ success: true, extras: { Status: "Device Splash Screen Completed Successfully", ApiKey: DeviceData.ApiKey, Whether_Latest_Version: Whether_Latest_Version } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DeviceController.Add_or_Update_Device_And_Get_Device_Information = (values, IPAddress) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ApiKey = uuid.v4();
                let query = {
                    DeviceID: values.DeviceID
                };
                let Result = await Devices.findOne(query).lean();
                let DeviceType = parseInt(values.DeviceType) || 1;
                if (Result == null) {
                    //New Device
                    let Interval = parseInt(moment().utcOffset(330).format('kk'));
                    let Data = {
                        ApiKey: ApiKey,
                        DeviceID: values.DeviceID,
                        DeviceType: DeviceType,
                        DeviceName: values.DeviceName,
                        AppVersion: values.AppVersion,
                        IPAddress: IPAddress,
                        InstallTime: new Date(),
                        Interval: Interval,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let DeviceResult = await Devices(Data).save();
                    resolve(JSON.parse(JSON.stringify(DeviceResult)));
                    let SaveResult = await Installed_Devices(Data).save();
                } else {
                    //Existing Device
                    let changes = {
                        $set: {
                            ApiKey: ApiKey,
                            AppVersion: values.AppVersion,
                            IPAddress: IPAddress,
                            updated_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Devices.updateOne(query, changes).lean();
                    Result.ApiKey = ApiKey;
                    Result.AppVersion = values.AppVersion;
                    Result.IPAddress = IPAddress;
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


DeviceController.Splash_Screen_Validate_Device_Type = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DeviceType = parseInt(values.DeviceType) || 1;
                if (DeviceType == 1 || DeviceType == 2 || DeviceType == 3) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DEVICE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

export default DeviceController;