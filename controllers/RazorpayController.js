let RazorpayController = function () { };
import axios from "axios";
import uuid from "uuid";
import Razorpay_Webhooks from "../models/Razorpay_Webhooks";
import Orders from "../models/Orders";
import ApiMessages from "../models/ApiMessages";
import config from "../config/config";
import UserController from "./UserController";
import AdminController from "./AdminController";
import DriverController from "./DriverController";
import Company_Wallet_Tranx from "../models/Company_Wallet_Tranx";

RazorpayController.CallbackFunctionality = async (req, res) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let values = JSON.parse(JSON.stringify(req.body));
                res.status(200).send("Callback Completed Successfully");
                let PaymentData = new Object();
                let InvoiceData = new Object();
                let PayoutData = new Object();
                let paymentarray = ["payment.authorized", "payment.captured", "payment.failed"];
                let invoicearray = ["invoice.paid", "invoice.expired"];
                let payoutarray = ["payout.queued", "payout.initiated", "payout.processed", "payout.reversed", "payout.created"];
                if (paymentarray.indexOf(values.event) >= 0) {
                    PaymentData = await values.payload.payment.entity;
                };
                if (invoicearray.indexOf(values.event) >= 0) {
                    InvoiceData = await values.payload.invoice.entity;
                };
                if (payoutarray.indexOf(values.event) >= 0) {
                    PayoutData = await values.payload.payout.entity;
                };
                let Data = {
                    WebookID: uuid.v4(),
                    PaymentID: PaymentData.id,
                    Captured: PaymentData.status,
                    Amount: values.payload.payment.entity.amount / 100,
                    RequestData: values,
                    PaymentData: PaymentData,
                    InvoiceData: InvoiceData,
                    PayoutData: PayoutData,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Razorpay_Webhooks(Data).save();
                // console.log(values.payload.payment.entity)
                if (paymentarray.indexOf(values.event) >= 0) {
                    if (values.payload.payment.entity.notes.type != "Book Purchase" && values.payload.payment.entity.notes.type != "Admin Amount") {
                        if (PaymentData.notes.Driver_Add_Money_RequestID != null || PaymentData.notes.Driver_Add_Money_RequestID != undefined || PaymentData.notes.Driver_Add_Money_RequestID != "" || PaymentData.notes.Driver_Add_Money_RequestID != " ") {
                            //Driver Add Money Payment
                            let WalletProcessing = await DriverController.Razorpay_Add_Money_Wallet_Processing(PaymentData);
                        }
                    }
                    if (PaymentData.status == 'authorized') {
                        if (values.payload.payment.entity.notes.type == "Book Purchase" || values.payload.payment.entity.notes.type == "Admin Amount") {
                            // console.log(53)
                            PaymentData = await RazorpayController.Capture_Razorpay_Payment(PaymentData.id, PaymentData.amount);
                        }
                        if (values.payload.payment.entity.notes.type == "Book Purchase") {
                            let TransactionIDArrayData = values.payload.payment.entity.notes.TransactionIDArrayData
                            // console.log(TransactionIDArrayData)
                            let updateTranx = await UserController.Complete_Payment_for_Buyer_Order([TransactionIDArrayData], Data, 1);
                        } else if (values.payload.payment.entity.notes.type == "Admin Amount") {
                            // console.log(values.payload.payment.entity)
                            let TransactionID = values.payload.payment.entity.notes.TransactionID

                            // console.log(TransactionID)

                            let SaveData = await AdminController.Complete_Add_Amount_Company_Wallet(TransactionID, Data); 
                        }
                        resolve("Functionality Completed");
                    } else if (PaymentData.status == 'captured') { 
                        // if (values.payload.payment.entity.notes.type == "Book Purchase") {
                        //     let TransactionIDArrayData = values.payload.payment.entity.notes.TransactionIDArrayData
                        //     console.log(TransactionIDArrayData)
                        //     let updateTranx = await UserController.Complete_Payment_for_Buyer_Order(TransactionIDArrayData, Data, 1);
                        // } else if (values.payload.payment.entity.notes.type == "Admin Amount") {
                        //     console.log(values.payload.payment.entity)
                        //     let TransactionID = values.payload.payment.entity.notes.TransactionID

                        //     console.log(TransactionID)

                        //     let SaveData = await AdminController.Complete_Add_Amount_Company_Wallet(TransactionID, Data); 
                        // }
                        resolve("Captured Completed");
                    } else if (PaymentData.status == 'failed') {
                        if (values.payload.payment.entity.notes.type == "Book Purchase") {
                            let TransactionIDArrayData = values.payload.payment.entity.notes.TransactionIDArrayData;
                            async.eachSeries(TransactionIDArrayData, async (item, callback) => {
                                try {
                                    let query = {
                                        TransactionID: item,
                                        Status: true
                                    }
                                    let Result = await Buyer_Orders.findOne(query).lean();

                                    let changes = {
                                        $set: {
                                            Payment_Status: 2,
                                            Order_Status: 0,
                                            WebHookData: Data,
                                            Updated_at: new Date()
                                        }
                                    };
                                    let UpdatedStatus = await Buyer_Orders.updateOne(query, changes).lean();
                                    callback();
                                } catch (error) {
                                    callback(error);
                                }
                            }, async (err) => {
                                if (err) reject(err);
                                resolve("Payment Failed");
                            });
                        } else if(values.payload.payment.entity.notes.type == "Admin Amount"){
                            let TransactionID = JSON.parse(values.payload.payment.entity.notes.TransactionID);
                            let query = {
                                TransactionID: TransactionID,
                            }
                            let Result = await Company_Wallet_Tranx.findOne(query).lean();
                            let changes = {
                                $set: {
                                    WebHookData: WebHookData,
                                    Payment_Status: 2,
                                    update_at: new Date(),
                                }
                            }
                            let updatData = await Company_Wallet_Tranx.updateOne(query, changes).lean().exec();
                        }
                    }
                };
                if (payoutarray.indexOf(values.event) >= 0) {
                    if (PayoutData.notes.TransactionType == 1) {
                        //Driver Amount Payout
                        let TransactionStatusUpdate = await AdminController.Common_Razorpay_DRIVER_PAYOUT_Update_Statuses(PayoutData);
                    } else if (PayoutData.notes.TransactionType == 2) {
                        //User Refund
                        //let TransactionStatusUpdate = await UserController.Common_Razorpay_Main_Order_Refund_Payout_Statuses(PayoutData);
                    } else if (PayoutData.notes.TransactionType == 1) {
                        //Driver Amount Payout
                        let TransactionStatusUpdate = await DriverController.Common_Razorpay_Driver_Withdrwal_Functionality(PayoutData);
                    }
                };

            } catch (error) {
                if (!res.headersSent) {
                    console.error("Something RazorpayX Callback error-->", error)
                    res.status(200).send("Callback Completed Successfully");
                }
            }
        });
    });
}

RazorpayController.Create_Razorpay_Driver_Contact = (DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (DriverData.Driver_Name.length <= 2) {
                    DriverData.Driver_Name = `${DriverData.Driver_Name} Bookafella`;
                }
                DriverData.Driver_Name = DriverData.Driver_Name.replace(/[^a-zA-Z0-9 ]/g, "");
                if (DriverData.Whether_RazorpayX_Driver_Register == null || !DriverData.Whether_RazorpayX_Driver_Register) {
                    let request_options = {
                        method: 'post',
                        baseURL: config.razorpay.baseURL,
                        url: `/contacts`,
                        data: {
                            "name": DriverData.Driver_Name,
                            "email": DriverData.Driver_EmailID,
                            "contact": DriverData.Driver_Phone_Number,
                            "type": "driver",
                            "reference_id": DriverData.DriverID,
                            "notes": {
                                "note_key": "Bookafella Driver Registration"
                            }
                        },
                        auth: {
                            username: config.razorpay.key_id,
                            password: config.razorpay.key_secret
                        },
                    };
                    let Response = await axios(request_options);
                    if (Response.status == 200 || Response.status == 201) {
                        resolve(Response.data);
                    } else if (Response.status == 400) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                    } else if (Response.status == 401) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                    }
                } else {
                    //Fetch Customer
                    let request_options = {
                        method: 'get',
                        baseURL: config.razorpay.baseURL,
                        url: `/contacts/${DriverData.RazorpayX_ContactID}`,
                        auth: {
                            username: config.razorpay.key_id,
                            password: config.razorpay.key_secret
                        },
                    };
                    let Response = await axios(request_options);
                    if (Response.status == 200 || Response.status == 201) {
                        resolve(Response.data);
                    } else if (Response.status == 400) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                    } else if (Response.status == 401) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                    }
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Create_Razorpay_Branch_Contact = (BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (BranchData.Branch_Name.length <= 2) {
                    BranchData.Branch_Name = `${BranchData.Branch_Name} Bookafella`;
                }
                BranchData.Branch_Name = BranchData.Branch_Name.replace(/[^a-zA-Z0-9 ]/g, "");
                if (BranchData.Whether_RazorpayX_Branch_Register == null || !BranchData.Whether_RazorpayX_Branch_Register) {
                    let request_options = {
                        method: 'post',
                        baseURL: config.razorpay.baseURL,
                        url: `/contacts`,
                        data: {
                            "name": BranchData.Branch_Name,
                            "email": "",
                            "contact": BranchData.Branch_PhoneNumber,
                            "type": "vendor",
                            "reference_id": BranchData.BranchID,
                            "notes": {
                                "note_key": "Bookafella Branch Registration"
                            }
                        },
                        auth: {
                            username: config.razorpay.key_id,
                            password: config.razorpay.key_secret
                        },
                    };
                    let Response = await axios(request_options);
                    if (Response.status == 200 || Response.status == 201) {
                        resolve(Response.data);
                    } else if (Response.status == 400) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                    } else if (Response.status == 401) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                    }
                } else {
                    //Fetch Customer
                    let request_options = {
                        method: 'get',
                        baseURL: config.razorpay.baseURL,
                        url: `/contacts/${BranchData.RazorpayX_ContactID}`,
                        auth: {
                            username: config.razorpay.key_id,
                            password: config.razorpay.key_secret
                        },
                    };
                    let Response = await axios(request_options);
                    if (Response.status == 200 || Response.status == 201) {
                        resolve(Response.data);
                    } else if (Response.status == 400) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                    } else if (Response.status == 401) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                    }
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Create_Razorpay_Driver_Beneficiary_Account_for_Bank_Account = (values, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (values.Name.length <= 4) {
                    values.Name = `${values.Name} Bookafella`;
                };
                values.Name = values.Name.replace(/[^a-zA-Z0-9 ]/g, "");
                let request_options = {
                    method: 'post',
                    baseURL: config.razorpay.baseURL,
                    url: `/fund_accounts`,
                    data: {
                        "contact_id": DriverData.RazorpayX_ContactID,
                        "account_type": "bank_account",
                        "bank_account": {
                            "name": values.Name,
                            "ifsc": values.IFSC,
                            "account_number": values.Account_Number
                        }
                    },
                    auth: {
                        username: config.razorpay.key_id,
                        password: config.razorpay.key_secret
                    },
                };
                let Response = await axios(request_options);
                if (Response.status == 200 || Response.status == 201) {
                    resolve(Response.data);
                } else if (Response.status == 400) {
                    console.error("Razorpay Error")
                    console.error(Response.data);
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                } else if (Response.status == 401) {
                    console.error("Razorpay Error")
                    console.error(Response.data);
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Create_Razorpay_Branch_Beneficiary_Account_for_Bank_Account = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (values.Name.length <= 4) {
                    values.Name = `${values.Name} Bookafella`;
                };
                values.Name = values.Name.replace(/[^a-zA-Z0-9 ]/g, "");
                let request_options = {
                    method: 'post',
                    baseURL: config.razorpay.baseURL,
                    url: `/fund_accounts`,
                    data: {
                        "contact_id": BranchData.RazorpayX_ContactID,
                        "account_type": "bank_account",
                        "bank_account": {
                            "name": values.Name,
                            "ifsc": values.IFSC,
                            "account_number": values.Account_Number
                        }
                    },
                    auth: {
                        username: config.razorpay.key_id,
                        password: config.razorpay.key_secret
                    },
                };
                let Response = await axios(request_options);
                if (Response.status == 200 || Response.status == 201) {
                    resolve(Response.data);
                } else if (Response.status == 400) {
                    console.error("Razorpay Error")
                    console.error(Response.data);
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                } else if (Response.status == 401) {
                    console.error("Razorpay Error")
                    console.error(Response.data);
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Razorpay_Beneficiary_Account_Payout = (BeneficiaryData, Amount, TransactionID, TransactionType) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                //Note------->   TransactionType ----->  1. Driver Payout  2. User Main Order Refund 3. Driver Wallet Withdrawl

                Amount = parseFloat(Amount) * 100;//converting to paise;
                Amount = parseInt(Amount);
                let mode = (BeneficiaryData.BeneficiaryType == 1) ? 'IMPS' : 'UPI';
                if (config.Whether_Production_Settings) {
                    let request_options = {
                        method: 'post',
                        baseURL: config.razorpay.baseURL,
                        url: `/payouts`,
                        data: {
                            "account_number": config.razorpay.razorpayx_account_number,
                            "fund_account_id": BeneficiaryData.RazorpayX_BeneficiaryID,
                            "amount": Amount,
                            "currency": "INR",
                            "mode": mode,
                            "purpose": "payout",
                            "queue_if_low_balance": true,
                            "reference_id": TransactionID,
                            "narration": "Bookafella-Bank-Transfer",
                            "notes": {
                                "note_key": "Bookafella-Bank-Transfer",
                                "TransactionType": TransactionType
                            }
                        },
                        auth: {
                            username: config.razorpay.key_id,
                            password: config.razorpay.key_secret
                        },
                    };
                    let Response = await axios(request_options);
                    if (Response.status == 200 || Response.status == 201) {
                        resolve(Response.data);
                    } else if (Response.status == 400) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_RAZORPAY_REQUEST } });
                    } else if (Response.status == 401) {
                        console.error("Razorpay Error")
                        console.error(Response.data);
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                    }
                } else {
                    let Data = {
                        id: uuid.v4(),
                        amount: Amount,
                        currency: "INR",
                        fees: 30,
                        tax: 10,
                        status: "processed",
                        utr: uuid.v4(),
                        mode: mode,
                        reference_id: TransactionID,
                        failure_reason: "",
                        notes: {
                            "note_key": "Bookafella-Bank-Transfer",
                            "TransactionType": TransactionType
                        }
                    };
                    resolve(Data);
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Check_Razorpay_Payment = (PaymentID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let url = `https://${config.razorpay.key_id}:${config.razorpay.key_secret}@${config.razorpay.host}/payments/${PaymentID}`;
                let request_options = {
                    method: 'get',
                    url: url
                };
                let Response = await axios(request_options);
                if (Response.status == 200) {
                    let Data = Response.data;
                    if (Data.status == "created" || Data.status == "failed") {
                        reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_PAYMENT_FAILED } });
                    } else {
                        resolve(Data);
                    }
                } else if (Response.status == 400) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PAYMENT_ID } });
                } else if (Response.status == 401) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Refund_Razorpay_Payment = (PaymentID, amount) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let url = `https://${config.razorpay.key_id}:${config.razorpay.key_secret}@${config.razorpay.host}/payments/${PaymentID}/refund`;
                let request_options = {
                    method: 'post',
                    url: url,
                    data: {
                        amount: amount,// In paise
                        // "speed": "optimum"
                    }
                };
                let Response = await axios(request_options);
                if (Response.status == 200) {
                    resolve(Response.data);
                } else if (Response.status == 400) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_REFUND_AMOUNT } });
                } else if (Response.status == 401) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

RazorpayController.Capture_Razorpay_Payment = (PaymentID, amount) => {
    // console.log(342)
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let url = `https://${config.razorpay.key_id}:${config.razorpay.key_secret}@${config.razorpay.host}/payments/${PaymentID}/capture`;
                let request_options = {
                    method: 'post',
                    url: url,
                    data: {
                        amount: amount// In paise
                    }
                };
                let Response = await axios(request_options);
                if (Response.status == 200) {
                    resolve(Response.data);
                } else if (Response.status == 400) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CAPTURE_AMOUNT } });
                } else if (Response.status == 401) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } });
                }
            } catch (error) {
                console.error("Razorpay Error-->", error);
                reject({ success: false, extras: { msg: ApiMessages.RAZORPAY_ERROR } });
            }
        });
    });
}

export default RazorpayController;