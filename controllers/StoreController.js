let StoreController = function () { };

//Models
import ApiMessages from '../models/ApiMessages';
import ApiResponce from '../models/Apiresponce';
import Store_MENU from '../models/Store_MENU';
import Store_Branch from '../models/Store_Branch';
import Customers from '../models/Customers';
import Store_SECTIONS from '../models/Store_SECTIONS';
import Store_Admin_Sessions from '../models/Store_Admin_Sessions';
import Store_Products from '../models/Store_Products';
import Store_SECTIONS_PRODUCTS from '../models/Store_SECTIONS_PRODUCTS';
//Dependencies

// import config from '../core/config/config';
import crypto from "crypto";
import uuid from "uuid";
import async from "async";
import Admin_Images from '../models/Admin_Images';
import CommonController from './CommonController';
import Orders from '../models/Orders';
import Store_Service_Area from '../models/Store_Service_Area';
import Images from '../models/Images';
import Buyer_Orders from '../models/Buyer_Orders';
import State_Cities from '../models/State_Cities';
import State from '../models/State';
import config from '../config/config';
import Order_Delivery_Setting from '../models/Order_Delivery_Setting';
import Drivers from '../models/Drivers';
import Instant_Order_Request from '../models/Instant_Order_Request';
import Trips from '../models/Trips';
import COMMON_SYSTEM_MESSAGES from '../config/COMMON_SYSTEM_MESSAGES';
import Driver_Devices from '../models/Driver_Devices';
import PubnubController from './PubnubController';
import FcmController from './FcmController';
import ZONES from '../models/ZONES';
import moment, { invalid } from 'moment';
import Driver_Order_OTP from '../models/Driver_Order_OTP';
import DriverController from './DriverController';
import Branch_Bank_Beneficiary_Accounts from '../models/Branch_Bank_Beneficiary_Accounts';
import RazorpayController from './RazorpayController';
import Branch_Wallet_Withdraw_Transactions from '../models/Branch_Wallet_Withdraw_Transactions';
import Store_Branch_Wallet_Logs from '../models/Store_Branch_Wallet_Logs';
import { Boolify } from 'node-boolify';
import Category from '../models/Category';
import Trips_Orders_Routings from '../models/Trips_Orders_Routings';
import MessagesController from './MessagesController';
import Branch_Stack_Log from '../models/Branch_Stack_Log';
import Reorder_log from '../models/Reorder_log';
import Stack_Log from '../models/Stack_Log';
import Branch_Notification_Log from '../models/Branch_Notification_Log';
import NotificationController from './NotificationController';

StoreController.List_All_Branch_Notifications = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let sortOptions = {
                    Created_at: -1
                };
                let queryR = {
                    BranchID: values.BranchID,
                    Viewed: true
                };
                let queryU = {
                    BranchID: values.BranchID,
                    Viewed: false
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let UnRead_Count = await Branch_Notification_Log.countDocuments(queryU).lean()
                let Read_Count = await Branch_Notification_Log.countDocuments(queryR).lean()
                let Count = await Branch_Notification_Log.countDocuments(query).lean().exec();
                let Result = await Branch_Notification_Log.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Read_Count: Read_Count, UnRead_Count: UnRead_Count, Count: Count, Data: Result } })
                for (let notification of Result) {
                    let q1 = {
                        NotificationID: notification.NotificationID
                    }
                    let changes = {
                        $set: {
                            Viewed: true
                        }
                    }
                    let updateData = await Branch_Notification_Log.updateOne(q1, changes).lean().exec();
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_All_Branch_Reorder = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let sortOptions = {
                    created_at: -1
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let Count = await Reorder_log.countDocuments(query).lean().exec();
                let Result = await Reorder_log.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings').sort(sortOptions).skip(toSkip).limit(toLimit).lean().exec();
                resolve({ success: true, extras: { Count: Count, Data: Result } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



/************************
 * Website functions below
 * 
 */
StoreController.Fetch_All_Nearest_Branches = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            let Latitude = parseFloat(values.Latitude);
            let Longitude = parseFloat(values.Longitude);
            let Point = [
                Longitude,
                Latitude
            ];

            let query = {
                Point: {
                    '$near': {
                        '$minDistance': 0,
                        '$maxDistance': 50000,
                        '$geometry': {
                            type: "Point",
                            coordinates: Point
                        }
                    }
                },
                Status: true
            };


            let result = await Store_Service_Area.distinct('BranchID', query).lean();
            //console.log(result);
            let resp = await StoreController.Fetch_All_Nearest_Branchesx(values, result);
            resolve(resp);

        });
    });
}

StoreController.Fetch_All_Nearest_Branchesx = (values, Barray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            // let Latitude = parseFloat(values.Latitude);
            // let Longitude = parseFloat(values.Longitude);
            // let Point = [
            //     Longitude,
            //     Latitude
            // ];
            let toSkip = Math.abs(parseInt(values.skip));
            let toLimit = Math.abs(parseInt(values.limit));
            let query = {
                // Point: {
                //     '$near': {
                //         '$minDistance': 0,
                //         '$maxDistance': 50000,
                //         '$geometry': {
                //             type: "Point",
                //             coordinates: Point
                //         }
                //     }
                // },
                BranchID: {
                    '$in': Barray
                },
                Status: true
            };
            let sortOptions = {
                created_at: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            Store_Branch.countDocuments(query).then((Count) => {
                //console.log(Count)
                if (Count >= 0) {
                    let ImageData;
                    Store_Branch.find(query).select('-_id -__v -updated_at -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).then((Result) => {
                        async.eachSeries(Result, async (item, callback) => {
                            try {
                                item.Veg_Menu_Data = await Store_MENU.find({ BranchID: item.BranchID, MENU_TYPE: 1, Status: true }).select('-_id -__v -updated_at').lean();
                                item.Non_Veg_Menu_Data = await Store_MENU.find({ BranchID: item.BranchID, MENU_TYPE: 2, Status: true }).select('-_id -__v -updated_at').lean();
                                ImageData = await Admin_Images.findOne({ ImageID: item.ImageID }).select('ImageOriginal').lean();
                                item.ImageURL = config.S3URL + ImageData.ImageOriginal;
                                callback();
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            resolve({ success: true, extras: { Count: Count, Data: Result } });
                        });
                    });
                } else {
                    console.log("sdadasddsa===--->")
                }
            });
        });
    });
}

StoreController.List_All_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let Count = await Orders.countDocuments(query).lean().exec();
                let Result = await Orders.find(query).select('-_id -__v -updated_at -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let BranchData = await Store_Branch.findOne({ BranchID: item.BranchID }).lean();
                        item.Store_Entity_Name = BranchData.Store_Entity_Name;
                        item.Branch_Name = BranchData.Branch_Name;
                        item.Branch_PhoneNumber = BranchData.Branch_PhoneNumber;
                        item.BranchURL = (item.BranchURL == "") ? "" : config.S3URL + BranchData.BranchURL;
                        item.Description = BranchData.Description;
                        item.Address = BranchData.Address;
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


/************************
* Website functions above
* 
*/

StoreController.Branch_Password_Change = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CustomerID: values.StoreAdminID
                };
                var date = new Date();
                var multiplicity = { upsert: true };
                var CustomerData = await Customers.findOne(query).exec();

                var Salt = CustomerData.PasswordSalt;
                var Old_Passwordx = values.Old_Password + Salt;
                var newpass = values.New_Password + Salt;
                var ExistPasswordHash = CustomerData.PasswordHash;
                var extpassword = crypto.createHash('sha512').update(Old_Passwordx).digest("hex")
                var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");
                var changes = {
                    PasswordHash: NewPasswordHash,
                    updated_at: date
                };
                if (ExistPasswordHash === extpassword) {
                    await Customers.updateOne(query, changes, multiplicity).exec(function (err, Result) {
                        if (Result) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Status: 'Password changed Successfully'
                                }
                            }));
                        }
                    });
                } else {
                    resolve(new ApiResponce({
                        success: false,
                        extras: {
                            msg: ApiMessages.INVALID_Old_PASSWORD
                        }
                    }));
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Update_Branch_Product_Reorder = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var ProductID = values.ProductID;

                let query = {
                    ReorderLogID: values.ReorderLogID,
                    BranchID: values.BranchID,
                    ProductID: ProductID,
                    Status: true
                }
                let ReorderData = await Reorder_log.findOne(query).lean().exec();
                if (ReorderData != null) {
                    if (ReorderData.Reorder_Status == 0) {
                        let changes = {
                            $set: {
                                Reorder_Status: values.Availablity,
                                Quantity_Available: values.Quantity_Available,
                                updated_at: new Date()
                            }
                        };
                        let UpdateData = await Reorder_log.updateOne(query, changes).lean().exec();
                        resolve({ success: true, extras: { Status: "Updated Successfully" } })
                        // Stock updating
                        let queryProduct = {
                            'Product_Data_For_Branch.BranchID': values.BranchID,
                            ProductID: ProductID
                        };
                        var changesProduct = {
                            $set: {
                                updated_at: new Date()
                            },
                            $inc: {
                                'Product_Data_For_Branch.$.Avaiable_Quantity': values.Quantity_Available,
                                'Product_Data_For_Branch.$.Total_Quantity': values.Quantity_Available,
                            }
                        }
                        let UpdateProduct = Store_Products.updateOne(queryProduct, changesProduct).lean().exec();
                        let sendNotification = await NotificationController.SendNotification_Update_Branch_Product_Reorder(req.body);
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.REORDER_STATUS_ALREADY_UPDATED } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_REORDER } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_Branch_Wallet_Log = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                }
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let Count = await Store_Branch_Wallet_Logs.countDocuments(query).lean().exec();
                let walletData = await Store_Branch_Wallet_Logs.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                resolve({ success: true, extras: { Count: Count, Data: walletData } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Get_Branch_Wallet = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                }
                let walletData = await Store_Branch.findOne(query).select('-_id Branch_Wallet_Information').lean().exec();
                resolve({ success: true, extras: { Data: walletData.Branch_Wallet_Information } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Fullfill_Stack_Quantity = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StackID: values.StackID,
                    BranchID: values.BranchID
                };
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {
                    let changes = {
                        $set: {
                            Fullfilled_Quantity: parseInt(values.Fullfilled_Quantity),
                            Reorder_Status: 3,
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Stack_Log.updateOne(query, changes).lean().exec()
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                    let queryProduct = {
                        ProductID: Result.ProductID,
                        'Product_Data_For_Branch.BranchID': values.BranchID
                    };
                    let changesProduct = {
                        $inc: {
                            'Product_Data_For_Branch.$.Avaiable_Quantity': parseInt(values.Fullfilled_Quantity),
                            'Product_Data_For_Branch.$.Total_Quantity': parseInt(values.Fullfilled_Quantity)
                        }
                    }
                    let UpdateProduct = await Store_Products.updateOne(queryProduct, changesProduct).lean().exec();
                    let SendNotification = await NotificationController.SendNotification_Fullfill_Stack_Quantity(values)
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_STACK } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Update_Stack_ETA = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StackID: values.StackID,
                    BranchID: values.BranchID
                };
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {
                    let changes = {
                        $set: {
                            Branch_ETA: values.Branch_ETA,
                            Whether_Branch_ETA_Updated: true,
                            Branch_ETA_Description: values.Branch_ETA_Description,
                            Branch_ETA_Date: moment().add(values.Branch_ETA, 'd').format(),
                            Reorder_Status: 2,
                            updated_at: new Date()
                        }
                    }
                    let UpdateData = await Stack_Log.updateOne(query, changes).lean().exec()
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                    let SendNotification = await NotificationController.SendNotification_Update_Stack_ETA(values)
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_STACK } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_All_Categories = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Status: true
                };
                let sortOptions = {
                    SNo: 1
                };
                let Result = await Category.find(query).select('-_id -__v').sort(sortOptions).lean();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Branch_Withdraw_Amount = (values, BeneficiaryData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let BQuery = {
                BranchID: values.BranchID
            }
            let Bresult = await Store_Branch.findOne(BQuery).lean().exec();
            // let SettingData = await CommonController.Fetch_App_Driver_Settings();
            let Amount = parseFloat(values.Amount);
            //let Minimum_Withdraw_Transaction_Amount = SettingData.Minimum_Withdraw_Transaction_Amount;
            //let Maximum_Withdraw_Transaction_Amount = SettingData.Maximum_Withdraw_Transaction_Amount;
            let Available_Amount = Bresult.Branch_Wallet_Information.Available_Amount;
            // if (Minimum_Withdraw_Transaction_Amount <= Amount && Amount <= Maximum_Withdraw_Transaction_Amount) {
            let Service_Amount = await config.RazorpayX_Service_Amount(Amount);
            let Total_Amount = Amount + Service_Amount;
            Amount = await CommonController.Common_Floating_Beautify_Value(Amount);
            Service_Amount = await CommonController.Common_Floating_Beautify_Value(Service_Amount);
            Total_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Amount);
            if (Total_Amount <= Available_Amount) {
                let Withdraw_TransactionID = uuid.v4();
                let RazorpayXPayoutData = await RazorpayController.Razorpay_Beneficiary_Account_Payout(BeneficiaryData, Amount, Withdraw_TransactionID, 3);
                let Transaction_Detailed_Data = await config.Razorpay_Transaction_Data.find(ele => ele.status == RazorpayXPayoutData.status);
                let Transaction_Status = Transaction_Detailed_Data.Transaction_Status;
                let Transaction_Status_Logs = {
                    LogID: uuid.v4(),
                    Transaction_Status: Transaction_Status,
                    Comment: Transaction_Detailed_Data.Comment,
                    Time: new Date()
                };
                let Transaction_Reference_ID = (Transaction_Status == 2) ? RazorpayXPayoutData.utr : "";
                let Transaction_Completion_Remarks = (Transaction_Status == 2) ? Transaction_Detailed_Data.Comment : "";
                let Data = {
                    Withdraw_TransactionID: Withdraw_TransactionID,
                    RazorpayX_TransactionID: RazorpayXPayoutData.id,
                    CountryID: Bresult.CountryID,
                    CityID: Bresult.CityID,
                    BranchID: Bresult.BranchID,
                    Amount: Amount,
                    Service_Amount: Service_Amount,
                    Total_Amount: Total_Amount,
                    BeneficiaryID: BeneficiaryData.BeneficiaryID,
                    RazorpayX_BeneficiaryID: BeneficiaryData.RazorpayX_BeneficiaryID,
                    RazorpayX_ContactID: BeneficiaryData.RazorpayX_ContactID,
                    BeneficiaryType: BeneficiaryData.BeneficiaryType,
                    Name: BeneficiaryData.Name,
                    Account_Number: BeneficiaryData.Account_Number,
                    IFSC: BeneficiaryData.IFSC,
                    Whether_Default_Account: BeneficiaryData.Whether_Default_Account,
                    Bank_Details: BeneficiaryData.Bank_Details,
                    UPI: BeneficiaryData.UPI,
                    Transaction_Status: Transaction_Status,
                    Transaction_Reference_ID: Transaction_Reference_ID,
                    Transaction_Completion_Remarks: Transaction_Completion_Remarks,
                    Transaction_Status_Logs: Transaction_Status_Logs,
                    RazorpayXPayoutData: RazorpayXPayoutData,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let TransactionData = await Branch_Wallet_Withdraw_Transactions.create(Data);
                resolve({ success: true, extras: { Status: "Withdrawl have been initiated" } })
                let WalletProcessing = await StoreController.Razorpay_Withdrawl_Wallet_Functionality(TransactionData);
            } else {
                reject({ success: false, extras: { msg: ApiMessages.INSUFFICIENT_BALANCE } });
            }
            // } else {
            //     reject({ success: false, extras: { msg: ApiMessages.AMOUNT_NOT_SATISFY_MINIMUM_MAXIMUM_TRANSACTION_AMOUNT } });
            // }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}



StoreController.Razorpay_Withdrawl_Wallet_Functionality = (TransactionData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let BranchID = TransactionData.BranchID;
            let Amount = TransactionData.Amount;
            let Service_Amount = TransactionData.Service_Amount;
            let Total_Amount = TransactionData.Total_Amount;
            let L1Data = {
                LogID: uuid.v4(),
                BranchID: BranchID,
                Type: 3, // Amount Debited for withdrawal from wallet
                Amount: Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L1SaveResult = await Store_Branch_Wallet_Logs.create(L1Data);
            let L2Data = {
                LogID: uuid.v4(),
                BranchID: BranchID,
                Type: 4, // Amoount debited for trancation charges
                Amount: Service_Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L2SaveResult = await Store_Branch_Wallet_Logs.create(L2Data);
            let fndupdquery = {
                BranchID: BranchID
            };
            let fndupdchanges = {
                $set: {
                    updated_at: new Date()
                },
                $inc: {
                    "Branch_Wallet_Information.Available_Amount": (Total_Amount * -1),
                    "Branch_Wallet_Information.Total_Withdrawn_Amount": Amount,
                    "Branch_Wallet_Information.Total_Withdrawn_Service_Tax_Amount": Service_Amount,
                }
            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let findupdateData = await Store_Branch.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve("Wallet Functionality Completed");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Inactivate_Branch_Beneficiary_Account_For_Bank_Account = values => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BranchID: values.BranchID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Branch_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Deleted Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Update_Branch_Beneficiary_Account_For_Bank_Account = (values, BankData,) => {
    return new Promise(async (resolve, reject) => {
        try {
            let BranchQ = {
                BranchID: values.BranchID
            };
            let BranchData = await Store_Branch.findOne(BranchQ).lean().exec();
            let RazorpayXBeneficiaryData = await RazorpayController.Create_Razorpay_Branch_Beneficiary_Account_for_Bank_Account(values, BranchData);
            let query = {
                BranchID: values.BranchID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    RazorpayX_BeneficiaryID: RazorpayXBeneficiaryData.id,
                    BeneficiaryType: 1,
                    Name: values.Name,
                    Account_Number: values.Account_Number,
                    IFSC: values.IFSC,
                    Bank_Details: BankData,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Branch_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


StoreController.Make_Default_Branch_Beneficiary_Account_For_Bank_Account = values => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BranchID: values.BranchID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    Whether_Default_Account: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Branch_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } });
            let oquery = {
                BranchID: values.BranchID,
                BeneficiaryID: {
                    $ne: values.BeneficiaryID
                }
            };
            let ochanges = {
                $set: {
                    Whether_Default_Account: false,
                    updated_at: new Date()
                }
            };
            let oUpdatedStatus = await Branch_Bank_Beneficiary_Accounts.updateMany(oquery, ochanges).lean();
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


StoreController.List_All_Branch_Beneficiary_Accounts = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let sortOptions = {
                    created_at: -1
                };
                if (values.Whether_Status_Filter === null || values.Whether_Status_Filter === undefined) {
                    query.Status = true;
                }

                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Result = await Branch_Bank_Beneficiary_Accounts.find(query).select('-_id -__v -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Add_Branch_Beneficiary_Account_For_Bank_Account = (values, BankData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let BranchQ = {
                    BranchID: values.BranchID
                };
                let BranchData = await Store_Branch.findOne(BranchQ).lean().exec();
                let Whether_Default_Account = false;
                if (Boolify(values.Whether_Default_Account)) {
                    Whether_Default_Account = true;
                } else {
                    let FetchDefaultAccountAvailable = await Branch_Bank_Beneficiary_Accounts.findOne({ BranchID: values.BranchID, Whether_Default_Account: true, Status: true }).lean();
                    if (FetchDefaultAccountAvailable == null) {
                        Whether_Default_Account = true;
                    } else {
                        let FetchWhetherAccountAvailable = await Branch_Bank_Beneficiary_Accounts.findOne({ BranchID: values.BranchID, Status: true }).lean();
                        if (FetchWhetherAccountAvailable == null) {
                            Whether_Default_Account = true;
                        } else {
                            Whether_Default_Account = false;
                        }
                    }
                }
                let RazorpayXBeneficiaryData = await RazorpayController.Create_Razorpay_Branch_Beneficiary_Account_for_Bank_Account(values, BranchData);
                let Data = {
                    BeneficiaryID: uuid.v4(),
                    RazorpayX_BeneficiaryID: RazorpayXBeneficiaryData.id,
                    BranchID: values.BranchID,
                    RazorpayX_ContactID: BranchData.RazorpayX_ContactID,
                    BeneficiaryType: 1,
                    Name: values.Name,
                    Account_Number: values.Account_Number,
                    IFSC: values.IFSC,
                    Whether_Default_Account: Whether_Default_Account,
                    Bank_Details: BankData,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let BranchBeneficiaryData = await Branch_Bank_Beneficiary_Accounts(Data).save();
                BranchBeneficiaryData = JSON.parse(JSON.stringify(BranchBeneficiaryData));
                resolve({ success: true, extras: { Status: "Beneficiary Account Added Successfully" } });
                if (Whether_Default_Account) {
                    let dfquery = {
                        BranchId: values.BranchId,
                        BeneficiaryID: {
                            $ne: BranchBeneficiaryData.BeneficiaryID
                        }
                    };
                    let dfchanges = {
                        $set: {
                            Whether_Default_Account: false,
                            updated_at: new Date()
                        }
                    };
                    let dfUpdatedStatus = await Branch_Bank_Beneficiary_Accounts.updateMany(dfquery, dfchanges).lean();
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Validate_Branch_Beneficiary_Account_For_Bank_Account_Number_Already_Exist = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                if (values.BeneficiaryID == null || values.BeneficiaryID == undefined || values.BeneficiaryID == '') {
                    values.BeneficiaryID = '';
                }
                let query = {
                    BranchID: values.BranchID,
                    Account_Number: values.Account_Number,
                    BeneficiaryID: {
                        $ne: values.BeneficiaryID
                    }
                };
                let Result = await Branch_Bank_Beneficiary_Accounts.findOne(query).lean();
                if (Result == null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.BENEFICIARY_ACCOUNT_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



StoreController.Check_for_Store_Admin = function (values, callback) {
    Customers.findOne({
        CustomerID: values.StoreAdminID
    }).exec(function (err, StoreAdminData) {
        if (err) {
            console.log(err);
        } else {
            if (StoreAdminData == null) {
                return callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.Store_Admin_Not_Found
                    }
                }));
            } else if (StoreAdminData != null) {
                callback(false, StoreAdminData);
            }
        }
    })
};

StoreController.CheckSessionID = function (values, callback) {
    Store_Admin_Sessions.findOne({
        SessionID: values.SessionID
    }, function (err, users) {
        if (err) {
            console.log(err);
        } else {
            if (users == null) {
                return callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.SESSION_EXPIRED
                    }
                }));
            } else if (users != null) {
                return callback(false);
            }
        }
    });
};

StoreController.Check_For_Store_BranchID = function (values, callback) {
    Store_Branch.findOne({ BranchID: values.BranchID }).exec(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            if (Result == null) {
                return callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.Store_Branch_Not_Found
                    }
                }))
            } else if (Result != null) {
                return callback(false, Result);
            }
        }
    })
};

StoreController.Create_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let Data = {
                    MENU_ID: uuid.v4(),
                    BranchID: values.BranchID,
                    MENU_NAME: values.MENU_NAME,
                    MENU_DESCRIPTION: values.MENU_DESCRIPTION,
                    MENU_TYPE: values.MENU_TYPE,
                    MENU_URL: values.MENU_URL,
                    MENU_PRICE: parseFloat(values.MENU_PRICE),
                    created_at: new Date(),
                    updated_at: new Date(),
                };
                Store_MENU(Data).save().then((Result) => {
                    resolve({ success: true, extras: { Status: "Menu Added Successfully" } })
                }).catch(err => {
                    console.error("Database Error---->", err)
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                });
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.List_ALL_Menu = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    BranchID: values.BranchID,
                    Status: true
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let CountFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            Store_MENU.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_MENU.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                CountFunction().then((Count) => {
                    ListFunction().then((Data) => {
                        resolve({ success: true, extras: { Count: Count, Data: Data } })
                    }).catch(err => reject(err));
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Check_for_STORE_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                };
                Store_MENU.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_MENU } });
                    } else if (Result != null) {


                        resolve(Result);
                    }

                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};



StoreController.Check_for_STORE_MENU_forWebsite = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MenuData[0].MENU_ID
                };
                Store_MENU.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_MENU } });
                    } else if (Result != null) {

                        var TotalPrice = (Result.MENU_PRICE * values.MenuData[0].NumberOfPlates);


                        if (values.MenuData.length > 1) {

                            let query = {
                                MENU_ID: values.MenuData[1].MENU_ID
                            };

                            Store_MENU.findOne(query).lean().exec().then((Result) => {
                                if (Result == null) {
                                    reject({ success: false, extras: { msg: ApiMessages.INVALID_MENU } });
                                } else if (Result != null) {
                                    var TotalPrices = (Result.MENU_PRICE * values.MenuData[1].NumberOfPlates);
                                    var total = TotalPrices + TotalPrice;

                                    // console.log("--->298");
                                    resolve(total);
                                }

                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                            })



                        } else {

                            // console.log("--->311");
                            resolve(TotalPrice);
                        }

                    }

                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.Create_MENU_SECTION = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let Data = {
                    SECTION_ID: uuid.v4(),
                    MENU_ID: values.MENU_ID,
                    BranchID: values.BranchID,
                    SECTION_NAME: values.SECTION_NAME,
                    SECTION_DESCRIPTION: values.SECTION_DESCRIPTION,
                    MAX_SELECTION: values.MAX_SELECTION,
                    Sl_NO: values.Sl_NO,
                    created_at: new Date(),
                    updated_at: new Date()

                };
                Store_SECTIONS(Data).save().then((Result) => {
                    resolve({ success: true, extras: { Status: "Menu Section Added Successfully" } })
                }).catch(err => {
                    console.error("Database Error---->", err)
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                });
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.List_ALL_Menu_Sections = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID,
                    Status: true
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let CountFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            Store_SECTIONS.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_SECTIONS.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                let Section_Products_Count = (SECTION_ID) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                let query = {
                                    SECTION_ID: SECTION_ID
                                };
                                Store_SECTIONS.countDocuments(query).lean().then((Result) => {
                                    resolve(Result)
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }

                CountFunction().then((Count) => {
                    ListFunction().then((Data) => {
                        async.eachSeries(Data, (item, callback) => {
                            Section_Products_Count(item.SECTION_ID).then((ProductCount) => {
                                item.ProductCount = ProductCount;
                                callback();
                            }).catch(err => callback(err));
                        }, (err) => {
                            resolve({ success: true, extras: { Count: Count, Data: Data } })
                        })
                    }).catch(err => reject(err));
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};
//Formatted String
// StoreController.Format_Beautify_String = (Product_Name) => {
//     Product_Name = Product_Name.replace(/\s\s+/g, ' ');
//     Product_Name = Product_Name.replace(/  +/g, ' ');
//     Product_Name = Product_Name.replace(/^ /, '');
//     Product_Name = Product_Name.replace(/\s\s*$/, '');

//     var myArr = Product_Name.toLowerCase().split(" ");
//     for (var a = 0; a < myArr.length; a++) {
//         myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
//     }
//     myArr.join(" ");

//     return myArr;
// };

StoreController.Store_Product_Information = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Unique_ProductID = values.Unique_ProductID;
                Unique_ProductID = Unique_ProductID.toUpperCase();
                let productIDValidation = await CommonController.Check_For_Unique_Product(Unique_ProductID);

                var Admin_Share_Percent = values.Admin_Share_Percent || 0;

                let Product_Name = values.Product_Name;
                Product_Name = Product_Name.replace(/\s\s+/g, ' ');
                Product_Name = Product_Name.replace(/  +/g, ' ');
                Product_Name = Product_Name.replace(/^ /, '');
                Product_Name = Product_Name.replace(/\s\s*$/, '');
                var myArr = Product_Name.toLowerCase().split(" ");
                for (var a = 0; a < myArr.length; a++) {
                    myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
                }
                Product_Name = myArr.join(" ");
                // let query = {
                //     Product_Name: Product_Name
                // };
                // let Result = await Store_Products.findOne(query).lean().exec();
                // if (Result == null) {
                var ProductID = uuid();
                var date = new Date();
                let CategoryData = await Category.findOne({ CategoryID: values.CategoryID }).lean().exec();
                let SubCategory = CategoryData.SubCategory_Data.filter(function (ResultTemp) {
                    return ResultTemp.SubCategoryID == values.SubCategoryID;
                });
                let Product_Image_Data = await Images.findOne({ ImageID: values.ImageID }).lean().exec();
                var Actual_Price = parseFloat(values.Price)
                var Dis_Price = parseFloat(Actual_Price * values.Discount_Percent / 100);
                var Final_Price = parseFloat(Actual_Price - Dis_Price); //
                var Admin_Share_Amount = parseFloat(Final_Price * Admin_Share_Percent / 100);
                var Buyer_Share_Amount = Final_Price - Admin_Share_Amount;
                var ProductData = {
                    ProductID: ProductID,
                    Unique_ProductID: Unique_ProductID,
                    Product_Name: Product_Name,
                    Product_Edition: values.Product_Edition,
                    Product_Published_On: values.Product_Published_On,
                    Product_Publisher: values.Product_Publisher,
                    Product_Description: {
                        Review: values.Review,
                        About_Author: values.About_Author
                    },
                    Authors: values.Authors,
                    CategoryID: values.CategoryID,
                    Category: CategoryData.Category,
                    SubCategoryID: values.SubCategoryID,
                    SubCategory: SubCategory[0].SubCategory,
                    Format_of_the_Book: values.Format_of_the_Book,
                    Total_Pages: values.Total_Pages,
                    Product_Image_Data: Product_Image_Data,
                    Product_ISBN: values.Product_ISBN,
                    Product_Approve: 1,
                    Who_Created: values.AdminID,
                    Who_Approved: values.AdminID,
                    Product_Dimensions: {
                        Height: values.Height,
                        Width: values.Width,
                        Length: values.Length,
                    },
                    Product_Data_For_Branch: {
                        Approve: 1,
                        BranchID: BranchData.BranchID,
                        Branch_Name: BranchData.Branch_Name,
                        Avaiable_Quantity: values.Avaiable_Quantity,
                        Total_Quantity: values.Avaiable_Quantity,
                        Status: true,
                        Price: Actual_Price,
                        Discount_Percent: values.Discount_Percent,
                        Discount_Price: Dis_Price,
                        Final_Price: Final_Price,
                        Condition: values.Condition,
                        Admin_Share_Percent: Admin_Share_Percent,
                        Admin_Share_Amount: Admin_Share_Amount,
                        Buyer_Share_Amount: Buyer_Share_Amount,
                        created_at: new Date(),
                        updated_at: new Date()
                    },
                    Status: true,
                    Who_Created: values.StoreAdminID,
                    created_at: date,
                    updated_at: date
                };
                let SaveProduct = Store_Products(ProductData).save();
                resolve({ success: true, extras: { Status: 'Product Added Successfully' } });
                // } else {
                //     reject({ success: false, extras: { msg: ApiMessages.PRODUCT_ALREADY_EXIST } });
                // }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Store_Product_Information_Existing = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var Admin_Share_Percent = values.Admin_Share_Percent || 0;
                var ProductID = values.ProductID;
                let query = {
                    ProductID: ProductID
                };
                let Result = await Store_Products.findOne(query).lean().exec();
                if (Result != null) {
                    let queryx = {
                        ProductID: ProductID,
                        'Product_Data_For_Branch.BranchID': BranchData.BranchID,
                    };
                    let Resultx = await Store_Products.findOne(queryx).lean().exec();
                    if (Resultx == null) {
                        var date = new Date();
                        var Actual_Price = parseFloat(values.Price)
                        var Dis_Price = parseFloat(Actual_Price * values.Discount_Percent / 100);
                        var Final_Price = parseFloat(Actual_Price - Dis_Price); //
                        var Admin_Share_Amount = parseFloat(Final_Price * Admin_Share_Percent / 100);
                        var Buyer_Share_Amount = Final_Price - Admin_Share_Amount;
                        var changes = {
                            $push: {
                                Product_Data_For_Branch: {
                                    Approve: 1,
                                    BranchID: BranchData.BranchID,
                                    Branch_Name: BranchData.Branch_Name,
                                    Avaiable_Quantity: values.Avaiable_Quantity,
                                    Total_Quantity: values.Avaiable_Quantity,
                                    Status: true,
                                    Price: Actual_Price,
                                    Discount_Percent: values.Discount_Percent,
                                    Discount_Price: Dis_Price,
                                    Final_Price: Final_Price,
                                    Condition: values.Condition,
                                    Admin_Share_Percent: Admin_Share_Percent,
                                    Admin_Share_Amount: Admin_Share_Amount,
                                    Buyer_Share_Amount: Buyer_Share_Amount,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                },
                            },
                            $set: {
                                updated_at: date
                            }
                        };
                        let UpdateProduct = Store_Products.updateOne(query, changes).lean().exec();
                        resolve({ success: true, extras: { Status: 'Product Added Successfully' } });
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.PRODUCT_ALREADY_EXIST } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//Find All Store Products
StoreController.Find_All_Store_Products = function (values, callback) {
    var query = {
        'Product_Data_For_Branch.BranchID': values.BranchID,
        'Product_Data_For_Branch.Status': true,
        Status: true,
        Whether_Deleted: {
            $ne: true
        }
    }
    var toSkip = parseInt(values.skip);
    var toLimit = parseInt(values.limit);
    Store_Products.countDocuments(query).exec(function (err, Count) {
        if (Count >= 0) {
            Store_Products.find(query).sort({ created_at: -1 }).skip(toSkip).limit(toLimit).exec(function (err, Result) {
                if (!err) {
                    var ProductData = [];
                    async.eachSeries(Result, function (item, resp) {
                        let PDatax = item.Product_Data_For_Branch.filter(function (ResultTemp) {
                            return ResultTemp.BranchID == values.BranchID;
                        });
                        let PData = PDatax[0]
                        CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data).then((Product_Image_Data) => {
                            ProductData.push({
                                ProductID: item.ProductID,
                                Unique_ProductID: item.Unique_ProductID,
                                Product_Name: item.Product_Name,
                                Product_Description: item.Product_Description,
                                Product_Image_Data: Product_Image_Data,
                                Authors: item.Authors,
                                Avaiable_Quantity: PData.Avaiable_Quantity,
                                Used_Quantity: PData.Used_Quantity,
                                Total_Quantity: PData.Total_Quantity,
                                Product_Edition: item.Product_Edition,
                                Product_Published_On: item.Product_Published_On,
                                Product_Publisher: item.Product_Publisher,
                                Format_of_the_Book: item.Format_of_the_Book, //1- HardCover 2-PaperBack 3-ScanedCopy 0- Other
                                CategoryID: item.CategoryID,
                                Category: item.Category,
                                SubCategoryID: item.SubCategoryID,
                                SubCategory: item.SubCategory,
                                created_at: PData.created_at,
                                updated_at: PData.updated_at,
                            })
                            resp();
                        }).catch(err => callback(err));
                    }, function (err) {
                        if (!err) {
                            return callback(new ApiResponce({
                                success: true,
                                extras: {
                                    Count: Count,
                                    ProductData: ProductData,
                                }
                            }));
                        }
                    })
                }
            })
        }
    })
}

StoreController.Find_All_Products = function (values, callback) {
    var query = {
        Status: true
    }
    var toSkip = parseInt(values.skip);
    var toLimit = parseInt(values.limit);
    Store_Products.countDocuments(query).exec(function (err, Count) {
        if (Count >= 0) {
            Store_Products.find(query).sort({ created_at: -1 }).skip(toSkip).limit(toLimit).exec(function (err, Result) {
                if (!err) {
                    var ProductData = [];
                    async.eachSeries(Result, function (item, resp) {
                        CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data).then((Product_Image_Data) => {
                            ProductData.push({
                                ProductID: item.ProductID,
                                Product_Name: item.Product_Name,
                                Product_Edition: item.Product_Edition,
                                Product_Published_On: item.Product_Published_On,
                                Product_Publisher: item.Product_Publisher,
                                Product_Description: item.Product_Description,
                                Authors: item.Authors,
                                Total_Pages: item.Total_Pages,
                                Product_Image_Data: Product_Image_Data,
                                Product_Dimensions: item.Product_Dimensions,
                                Status: item.Status,
                                Avg_Rating: item.Avg_Rating,
                                Total_Rating_Count: item.Total_Rating_Count,
                                Product_Data_For_Branch: item.Product_Data_For_Branch,
                                Who_Created: item.Who_Created,
                                created_at: item.created_at,
                                updated_at: item.updated_at,
                            })
                            resp();
                        }).catch(err => callback(err));
                    }, function (err) {
                        if (!err) {
                            return callback(new ApiResponce({
                                success: true,
                                extras: {
                                    Count: Count,
                                    ProductData: ProductData,
                                }
                            }));
                        }
                    })
                }
            })
        }
    })
}

StoreController.Find_All_Products_Lite = function (values, callback) {
    let sortOptions = {
        created_at: -1
    };
    let Search = {
        $regex: String(values.Search),
        $options: "i"
    };
    let query = {
        $or: [{
            Product_Name: Search
        }],
        Status: true
    };

    Store_Products.countDocuments(query).exec(function (err, Count) {
        if (Count >= 0) {
            Store_Products.find(query).sort(sortOptions).exec(function (err, Result) {
                if (!err) {
                    var ProductData = [];
                    async.eachSeries(Result, function (item, resp) {
                        CommonController.Common_Image_Response_Single_Image(true, item.Product_Image_Data).then((Product_Image_Data) => {
                            ProductData.push({
                                ProductID: item.ProductID,
                                Product_Name: item.Product_Name,
                                Authors: item.Authors,
                                Product_Image_Data: Product_Image_Data,
                                Status: item.Status
                            })
                            resp();
                        }).catch(err => callback(err));
                    }, function (err) {
                        if (!err) {
                            return callback(new ApiResponce({
                                success: true,
                                extras: {
                                    Count: Count,
                                    ProductData: ProductData,
                                }
                            }));
                        }
                    })
                }
            })
        }
    })
}



StoreController.Get_Single_Product_Details = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var query = {
                    ProductID: values.ProductID,
                    Status: true,
                    Whether_Deleted: {
                        $ne: true
                    }
                }
                let Result = await Store_Products.findOne(query).lean().exec();
                if (Result != null) {
                    CommonController.Common_Image_Response_Single_Image(true, Result.Product_Image_Data).then((Product_Image_Data) => {
                        let Data = {
                            ProductID: Result.ProductID,
                            Product_Name: Result.Product_Name,
                            Unique_ProductID: Result.Unique_ProductID,
                            Product_Edition: Result.Product_Edition,
                            Product_Published_On: Result.Product_Published_On,
                            Product_Publisher: Result.Product_Publisher,
                            Product_Description: Result.Product_Description,
                            Authors: Result.Authors,
                            Total_Pages: Result.Total_Pages,
                            Product_Image_Data: Product_Image_Data,
                            Product_Dimensions: Result.Product_Dimensions,
                            Product_ISBN: Result.Product_ISBN,
                            CategoryID: Result.CategoryID,
                            Category: Result.Category,
                            SubCategoryID: Result.SubCategoryID,
                            SubCategory: Result.SubCategory,
                            Views: Result.Views,
                            Status: Result.Status,
                            Avg_Rating: Result.Avg_Rating,
                            Total_Rating_Count: Result.Total_Rating_Count,
                            Product_Data_For_Branch: Result.Product_Data_For_Branch,
                            Who_Created: Result.Who_Created,
                            created_at: Result.created_at,
                            updated_at: Result.updated_at,
                        }
                        resolve({ success: true, extras: { Data: Data } })
                    }).catch(err => callback(err));
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


StoreController.Update_Store_Product = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var Admin_Share_Amount = values.Admin_Share_Amount || 0;
                var ProductID = values.ProductID;
                let query = {
                    'Product_Data_For_Branch.BranchID': values.BranchID,
                    ProductID: ProductID
                };
                let Result = await Store_Products.findOne(query).lean().exec();
                if (Result != null) {
                    var date = new Date();
                    var Actual_Price = parseFloat(values.Price)
                    var Bookafella_Dis_Price = parseFloat(values.Bookafella_Discount_Price);
                    var Bookafella_Dis_Percent = parseFloat(((Bookafella_Dis_Price * 100) / Actual_Price).toFixed(2));
                    var Branch_Dis_Price = parseFloat(values.Branch_Discount_Price);
                    var Branch_Dis_Percent = parseFloat(((Branch_Dis_Price * 100) / Actual_Price).toFixed(2));
                    var Total_Discount_Price = parseFloat(Bookafella_Dis_Price + Branch_Dis_Price);
                    var Final_Price = parseFloat(Actual_Price - Total_Discount_Price); //
                    var Admin_Share_Percent = parseFloat(((Admin_Share_Amount * 100) / Final_Price).toFixed(2));
                    var Branch_Share_Amount = Final_Price - Admin_Share_Amount;
                    var changes = {
                        $set: {
                            'Product_Data_For_Branch.$.BranchID': BranchData.BranchID,
                            'Product_Data_For_Branch.$.Branch_Name': BranchData.Branch_Name,
                            'Product_Data_For_Branch.$.Avaiable_Quantity': values.Avaiable_Quantity,
                            'Product_Data_For_Branch.$.Total_Quantity': values.Avaiable_Quantity,
                            'Product_Data_For_Branch.$.Status': true,
                            'Product_Data_For_Branch.$.Price': Actual_Price,
                            'Product_Data_For_Branch.$.Branch_Discount_Percent': Branch_Dis_Percent,
                            'Product_Data_For_Branch.$.Branch_Discount_Price': Branch_Dis_Price,
                            'Product_Data_For_Branch.$.Bookafella_Discount_Percent': Bookafella_Dis_Percent,
                            'Product_Data_For_Branch.$.Bookafella_Discount_Price': Bookafella_Dis_Price,
                            'Product_Data_For_Branch.$.Total_Discount_Price': Total_Discount_Price,
                            'Product_Data_For_Branch.$.Final_Price': Final_Price,
                            'Product_Data_For_Branch.$.Admin_Share_Percent': Admin_Share_Percent,
                            'Product_Data_For_Branch.$.Admin_Share_Amount': Admin_Share_Amount,
                            'Product_Data_For_Branch.$.Branch_Share_Amount': Branch_Share_Amount,
                            'Product_Data_For_Branch.$.Condition': values.Condition,
                            'Product_Data_For_Branch.$.updated_at': date,
                            updated_at: date
                        }
                    };
                    let UpdateProduct = Store_Products.updateOne(query, changes).lean().exec();
                    resolve({ success: true, extras: { Status: 'Product Updated Successfully' } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Delete_Store_Product = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var ProductID = values.ProductID;
                let query = {
                    'Product_Data_For_Branch.BranchID': values.BranchID,
                    ProductID: ProductID
                };
                let Result = await Store_Products.findOne(query).lean().exec();
                if (Result != null) {
                    var date = new Date();
                    var changes = {
                        $set: {
                            Whether_Deleted: true,
                            Status: false,
                            'Product_Data_For_Branch.$.Status': false,
                            updated_at: date
                        }
                    };
                    let UpdateProduct = Store_Products.updateOne(query, changes).lean().exec();
                    resolve({ success: true, extras: { Status: 'Product Updated Successfully' } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Activate_Inactivate_Store_Product = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var ProductID = values.ProductID;
                let query = {
                    'Product_Data_For_Branch.BranchID': values.BranchID,
                    ProductID: ProductID
                };
                let Result = await Store_Products.findOne(query).lean().exec();
                if (Result != null) {
                    var date = new Date();
                    var changes = {
                        $set: {
                            Status: !Result.Status,
                            'Product_Data_For_Branch.$.Status': !Result.Status,
                            updated_at: date
                        }
                    };
                    let UpdateProduct = Store_Products.updateOne(query, changes).lean().exec();
                    resolve({ success: true, extras: { Status: 'Product Updated Successfully' } });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_All_Notified_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: {
                        $ne: 0
                    },
                    'Branch_Information.BranchID': values.BranchID,
                    Whether_Cancellation_Notified: true,
                    Whether_Cancellation_Notified_Accepted: values.Whether_Cancellation_Notified_Accepted
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).select('OrderID Branch_Information Order_Number Cart_Information Order_Status Cancel_Reason Cancellation_Pickup_Status Cancellation_Order_Pickup_Report Whether_Cancellation_Notified Whether_Cancellation_Notified_Accepted created_at').lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    let queryx = {
                        BranchID: Order.Branch_Information.BranchID
                    }
                    let Resultx = await Store_Branch.findOne(queryx).lean().exec();
                    let CityData = await CommonController.Check_for_City({ CityID: Resultx.CityID });
                    let ZoneData = await CommonController.Check_for_Zone({ ZoneID: Resultx.ZoneID });
                    Order.CityID = CityData.CityID
                    Order.CityName = CityData.CityName
                    Order.ZoneID = ZoneData.ZoneID
                    Order.Zone_Title = ZoneData.Zone_Title
                    // delete Order.Branch_Information
                    for (const Product of Order.Cart_Information) {
                        delete Product.Price
                        delete Product.Discount_Percent
                        delete Product.Discount_Price
                        delete Product.Final_Price
                        delete Product.Admin_Share_Percent
                        delete Product.Admin_Share_Amount
                        delete Product.Branch_Share_Amount
                        delete Product.Total_Price
                        delete Product.Total_Discount_Price
                        delete Product.Total_Final_Price
                        delete Product.Total_Admin_Share_Amount
                        delete Product.Total_Branch_Share_Amount
                    }
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_Branch_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Cancel_Status: 3,
                    'Branch_Information.BranchID': values.BranchID,
                    'Cancelled_By.BranchID': values.BranchID,
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).select('OrderID Order_Number Cart_Information Order_Status Cancel_Reason Branch_Information Cancellation_Pickup_Status Cancellation_Order_Pickup_Report Whether_Cancellation_Notified Whether_Cancellation_Notified_Accepted Whether_Cancellation_Prodessed created_at').lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    for (const Product of Order.Cart_Information) {
                        Product.Branch_Information = Order.Branch_Information
                        delete Product.Price
                        delete Product.Discount_Percent
                        delete Product.Discount_Price
                        delete Product.Final_Price
                        delete Product.Admin_Share_Percent
                        delete Product.Admin_Share_Amount
                        delete Product.Branch_Share_Amount
                        delete Product.Total_Price
                        delete Product.Total_Discount_Price
                        delete Product.Total_Final_Price
                        delete Product.Total_Admin_Share_Amount
                        delete Product.Total_Branch_Share_Amount
                    }
                    delete Order.Branch_Information
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.List_Branch_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                let matchquery = {
                    Whether_Cancellation_Refund_Prodessed: false,
                    Cancellation_Allowed: false,
                    'Branch_Information.BranchID': values.BranchID,
                    Payment_Status: {
                        $ne: 0
                    },
                    created_at: {
                        $gte: Start_Date,
                        $lte: End_Date
                    }
                };
                if (values.Status == true) {
                    matchquery.Order_Status = 3
                } else {
                    matchquery.Order_Status = {
                        $ne: 3
                    }
                }
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).select('OrderID Cancel_Status Order_Number Cart_Information Order_Status created_at ').lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    for (const Product of Order.Cart_Information) {
                        delete Product.Price
                        delete Product.Discount_Percent
                        delete Product.Discount_Price
                        delete Product.Final_Price
                        delete Product.Admin_Share_Percent
                        delete Product.Admin_Share_Amount
                        delete Product.Branch_Share_Amount
                        delete Product.Total_Price
                        delete Product.Total_Discount_Price
                        delete Product.Total_Final_Price
                        delete Product.Total_Admin_Share_Amount
                        delete Product.Total_Branch_Share_Amount
                    }
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Cancel_Order_Request = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Oquery = {
                    OrderID: values.OrderID,
                };
                let OResult = await Buyer_Orders.findOne(Oquery).lean().exec();
                if (OResult != null) {
                    if (OResult.Order_Status < 8 && OResult.Order_Status != 3) {
                        let Ochnages = {
                            $set: {
                                Cancel_Status: 3, //cancelled by branch
                                Cancelled_By: {
                                    BranchID: values.BranchID
                                },
                                Cancel_Reason: values.Cancel_Reason,
                                updated_at: new Date()
                            },
                        }
                        let UpdateOrder = await Buyer_Orders.updateOne(Oquery, Ochnages).lean().exec();
                        resolve({ success: true, extras: { Status: "Cancel Request Palced Successfully" } });
                        let SendNotification = await NotificationController.SendNotification_Cancel_Order_Request(values, BranchData)
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ORDER_CAN_NOT_BE_CANCELLED } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Accept_Notified_Cancelled_Order = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                //Accept Order changes Order_Status to Accepted and update Order_Report as accepted
                let Oquery = {
                    OrderID: values.OrderID,
                };
                let OResult = await Buyer_Orders.findOne(Oquery).lean().exec();
                if (OResult != null) {
                    if (Boolify(OResult.Whether_Cancellation_Notified) && OResult.Cancel_Status != 0) {
                        let Ochnages = {
                            $set: {
                                Whether_Cancellation_Notified_Accepted: true, //accepted
                                updated_at: new Date()
                            },
                        }
                        let UpdateOrder = await Buyer_Orders.updateOne(Oquery, Ochnages).lean().exec();
                        /// reduce the stock by qty
                        if (OResult.Order_Status >= 8) {
                            let CartInfo = OResult.Cart_Information //Array
                            async.eachSeries(CartInfo, async (item, callback) => {
                                try {
                                    let Qty = item.Product_Quantity;
                                    let query = {
                                        ProductID: item.ProductID,
                                        'Product_Data_For_Branch.BranchID': values.BranchID
                                    }
                                    let changes = {
                                        $inc: {
                                            'Product_Data_For_Branch.$.Avaiable_Quantity': Qty,
                                            'Product_Data_For_Branch.$.Used_Quantity': Qty * -1
                                        },
                                        $set: {
                                            'Product_Data_For_Branch.$.updated_at': new Date()
                                        }
                                    }
                                    let updateQty = await Store_Products.updateOne(query, changes).lean().exec()
                                    callback();
                                } catch (error) {
                                    callback(error);
                                }
                            }, async (err) => {
                                if (err) reject(err);
                                //notification if requried
                            });
                        }
                        resolve({ success: true, extras: { Status: "updated Successfully" } });
                        let SendNotification = await NotificationController.SendNotification_Accept_Notified_Cancelled_Order(values, BranchData, OResult);
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.ORDER_NOT_NOTIFIED_OR_NOT_CANCELLED } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Accept_Orders = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                //Accept Order changes Order_Status to Accepted and update Order_Report as accepted
                let Oquery = {
                    OrderID: values.OrderID,
                    Order_Status: 1
                };
                let OResult = await Buyer_Orders.findOne(Oquery).lean().exec();
                if (OResult != null) {
                    let Ochnages = {
                        $set: {
                            Order_Status: 2, //accepted
                            updated_at: new Date()
                        },
                        $push: {
                            Order_Report: {
                                Title: 'Order Accepted',
                                Description: 'Order Accepted',
                                Time: new Date()
                            }
                        }
                    }
                    let UpdateOrder = await Buyer_Orders.updateOne(Oquery, Ochnages).lean().exec();
                    /// reduce the stock by qty
                    let CartInfo = OResult.Cart_Information //Array
                    async.eachSeries(CartInfo, async (item, callback) => {
                        try {
                            let Qty = item.Product_Quantity;
                            let query = {
                                ProductID: item.ProductID,
                                'Product_Data_For_Branch.BranchID': values.BranchID
                            }
                            let changes = {
                                $inc: {
                                    'Product_Data_For_Branch.$.Avaiable_Quantity': Qty * -1,
                                    'Product_Data_For_Branch.$.Used_Quantity': Qty
                                },
                                $set: {
                                    'Product_Data_For_Branch.$.updated_at': new Date()
                                }
                            }
                            let updateQty = await Store_Products.updateOne(query, changes).lean().exec()
                            callback();
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);

                        // check for Country_City_State_Active

                        let ValidityStatus = await StoreController.Validate_Country_City_State_Active(BranchData);
                        //////////////
                        let ZoneData = await StoreController.Fetch_Zone_Data(OResult.Branch_Information);
                        let Result = await StoreController.Instant_Order_Send_Driver_Request(BranchData, ZoneData, OResult);
                        resolve({ success: true, extras: { Status: "Order Status updated Successfully", Data: Result } });
                        let SendNotification = await FcmController.Order_Accepted_From_BookStore(OResult);
                    });
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Instant_Order_Send_Driver_Request = (BranchData, ZoneData, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let MappedDriverIDArray = [];
                let DriverID_Array = [];
                let Driver_Radius_KMS = await StoreController.Instant_Order_Send_Driver_Request_Fetch_Driver_Radius(BranchData.CountryID, BranchData.CityID);
                let ALL_DRIVERS_DATA = await StoreController.Find_All_Instant_Available_Driver_Information(ZoneData, MappedDriverIDArray);
                // console.log(ALL_DRIVERS_DATA)
                let Latitude = await BranchData.Latitude;
                let Longitude = await BranchData.Longitude;
                ALL_DRIVERS_DATA = await StoreController.Instant_Order_Send_Driver_Request_Driver_Distance_Calculation(ALL_DRIVERS_DATA, Latitude, Longitude);
                ALL_DRIVERS_DATA = await ALL_DRIVERS_DATA.filter(item => item.geo_distance <= Driver_Radius_KMS);
                await ALL_DRIVERS_DATA.forEach((item) => DriverID_Array.push(item.DriverID));
                //online Orders
                // console.log(2)
                let Result = await StoreController.Instant_Order_Send_Driver_Request_Save_Request_Send_Response(ZoneData, OrderData, ALL_DRIVERS_DATA, DriverID_Array);
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Instant_Order_Send_Driver_Request_Save_Request_Send_Response = (ZoneData, OrderData, ALL_DRIVERS_DATA, DriverID_Array) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (ALL_DRIVERS_DATA.length > 0) {
                let DistanceCalculation = await CommonController.Routing_Processing_With_Location(OrderData);
                let InstantOrderRequestID = uuid.v4();
                let Instant_Order_Request_Status = 1;//Requested
                let Instant_Order_Request_Status_Logs = {
                    ReferenceID: uuid.v4(),
                    Instant_Order_Request_Status: Instant_Order_Request_Status,
                    time: new Date()
                };
                let Data = {
                    InstantOrderRequestID: InstantOrderRequestID,
                    BuyerID: OrderData.BuyerID, // BuyerID
                    // PriceQuoteID: PriceQuoteData.PriceQuoteID, //
                    // Sub_Trip_Type: PriceQuoteData.Sub_Trip_Type,//
                    CountryID: ZoneData.CountryID,
                    CityID: ZoneData.CityID,
                    ZoneID: ZoneData.ZoneID,
                    // Customer_Selected_Price: await CommonController.Common_Floating_Beautify_Value(values.Customer_Selected_Price), //
                    Service_Type: ZoneData.Service_Type,
                    // Whether_Discount_Apply: Boolify(values.Whether_Discount_Apply), //
                    // UserDiscountID: (Boolify(values.Whether_Discount_Apply)) ? values.UserDiscountID : '', //
                    // Total_Package_Volume_Weight: PriceQuoteData.Total_Package_Volume_Weight, //
                    Total_Distance: DistanceCalculation.geo_distance,
                    Total_Time: DistanceCalculation.geo_duration_minutes, // modify to Total_Load_Time to Total_Time
                    // Total_Unload_Time: DistanceCalculation.Total_Unload_Time, //
                    Total_Duration_Minutes: DistanceCalculation.geo_duration_minutes,
                    Order_Type: 2,//online
                    ALL_Driver_Information: ALL_DRIVERS_DATA,
                    Instant_Order_Request_Status: Instant_Order_Request_Status,
                    Instant_Order_Request_Status_Logs: Instant_Order_Request_Status_Logs,
                    // No_Of_Orders: PriceQuoteData.No_Of_Orders, //
                    // Order_Input_Information: PriceQuoteData.Order_Input_Information, //
                    Order_Data: await CommonController.Check_for_Order(OrderData),
                    Whether_Checked: true,
                    Checked_Time: new Date(),
                    Whether_Request: true,
                    Request_Time: new Date(),
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let InstantOrderRequestData = await Instant_Order_Request(Data).save();
                InstantOrderRequestData = await JSON.parse(JSON.stringify(InstantOrderRequestData));
                let DataX = {
                    Whether_Driver_Available: true,
                    InstantOrderRequestID: InstantOrderRequestID,
                    Status: "Request Send to All Driver"
                }
                resolve(DataX);
                let NotificationPubNubfunction = await StoreController.Instant_Order_Request_Send_Driver_Notification_Pubnub_Socket_Information(InstantOrderRequestData, DriverID_Array, ALL_DRIVERS_DATA);
            } else {
                let DataX = {
                    Whether_Driver_Available: false,
                    Status: COMMON_SYSTEM_MESSAGES.INSTANT_ORDER_PLACING_DRIVER_NOT_AVAILABLE
                }
                resolve(DataX);
                let storeRequest = await StoreController.Instant_Order_Send_Driver_Request_Drivers_Not_Available_Save_Request(ZoneData, OrderData);
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Instant_Order_Send_Driver_Request_Drivers_Not_Available_Save_Request = (ZoneData, OrderData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Order_Type = 2;
            let DistanceCalculation = await CommonController.Routing_Processing_With_Location(OrderData);
            let InstantOrderRequestID = uuid.v4();
            let Instant_Order_Request_Status = 5; //5.Drivers Not Available
            let Instant_Order_Request_Status_Logs = {
                ReferenceID: uuid.v4(),
                Instant_Order_Request_Status: Instant_Order_Request_Status,
                time: new Date()
            };
            let Data = {
                InstantOrderRequestID: InstantOrderRequestID,
                BuyerID: OrderData.BuyerID,
                // PriceQuoteID: PriceQuoteData.PriceQuoteID,
                // Sub_Trip_Type: PriceQuoteData.Sub_Trip_Type,
                CountryID: ZoneData.CountryID,
                CityID: ZoneData.CityID,
                ZoneID: ZoneData.ZoneID,
                //Customer_Selected_Price: await CommonController.Common_Floating_Beautify_Value(values.Customer_Selected_Price),
                Service_Type: ZoneData.Service_Type,
                // Whether_Discount_Apply: Boolify(values.Whether_Discount_Apply),
                // UserDiscountID: (Boolify(values.Whether_Discount_Apply)) ? values.UserDiscountID : '',
                // Total_Package_Volume_Weight: PriceQuoteData.Total_Package_Volume_Weight,
                Total_Distance: DistanceCalculation.geo_distance,
                Total_Time: DistanceCalculation.geo_duration_minutes, // modify to Total_Load_Time to Total_Time
                // Total_Unload_Time: DistanceCalculation.Total_Unload_Time, //
                Total_Duration_Minutes: DistanceCalculation.geo_duration_minutes,
                Order_Type: Order_Type,
                Instant_Order_Request_Status: Instant_Order_Request_Status,
                Instant_Order_Request_Status_Logs: Instant_Order_Request_Status_Logs,
                Order_Data: OrderData,
                //No_Of_Orders: PriceQuoteData.No_Of_Orders,
                //Order_Input_Information: PriceQuoteData.Order_Input_Information,
                Whether_Checked: true,
                Checked_Time: new Date(),
                created_at: new Date(),
                updated_at: new Date()
            };
            let InstantOrderRequestData = await Instant_Order_Request(Data).save();
            InstantOrderRequestData = await JSON.parse(JSON.stringify(InstantOrderRequestData));
            resolve("Stored Successfully");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Instant_Order_Request_Send_Driver_Notification_Pubnub_Socket_Information = (InstantOrderRequestData, DriverID_Array, ALL_DRIVERS_DATA) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let ddquery = {
                    DriverID: {
                        $in: DriverID_Array
                    },
                    DeviceID: {
                        $ne: ''
                    },
                    Status: true
                };
                let DeviceID_Array = await Drivers.distinct('DeviceID', ddquery).lean();
                let ffquery = {
                    DeviceID: {
                        $in: DeviceID_Array
                    }
                };
                let FCM_Token_Array = await Driver_Devices.distinct('FCM_Token', ffquery).lean();
                let NotificationSending = await FcmController.Instant_Order_Request_Send_Driver_Notifications(InstantOrderRequestData, FCM_Token_Array);
                let SocketMessageSending = await PubnubController.Instant_Order_Send_Driver_Request_Pubnub_Message(ALL_DRIVERS_DATA, InstantOrderRequestData);
                resolve("All Functionality Completed");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Instant_Order_Send_Driver_Request_Driver_Distance_Calculation = (ALL_DRIVERS_DATA, Latitude, Longitude) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                async.eachSeries(ALL_DRIVERS_DATA, async (item, callback) => {
                    try {
                        item.Current_Location = new Object();
                        if (item.Driver_Current_Location.Latitude == 0 || item.Driver_Current_Location.Latitude == 0) {
                            item.Current_Location.Latitude = await item.Location.Latitude;
                            item.Current_Location.Longitude = await item.Location.Longitude;
                        } else {
                            item.Current_Location.Latitude = await item.Driver_Current_Location.Latitude;
                            item.Current_Location.Longitude = await item.Driver_Current_Location.Longitude;
                        };
                        let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(Latitude, Longitude, item.Current_Location.Latitude, item.Current_Location.Longitude);
                        item.geo_distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                        item.geo_duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
                        item.geo_distance /= 1000; //mtrs to Kms
                        item.geo_duration_minutes = (item.geo_duration / 60);
                        item.geo_duration *= 1000; //seconds to milliseconds
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(ALL_DRIVERS_DATA);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Find_Instant_Available_DriverID_Array = (ZoneData, MappedDriverIDArray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let now = moment().toDate();
                let Order_Type = 2;
                let now_before_15_secs = moment().subtract(config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST, 'seconds').toDate();
                let now_before_10_mins = (Order_Type == 2) ? moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME, 'minutes').toDate() : moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH, 'minutes').toDate();
                let Availables_Driver_Data = [];
                let query = {
                    CityID: ZoneData.CityID,
                    // ZoneID: ZoneData.ZoneID,
                    Status: true
                };
                let ALL_DRIVERS_DATA = await Drivers.distinct('DriverID', query).lean();
                let ifquery = {
                    Status: true,
                    $or: [
                        {
                            Instant_Order_Request_Status: 1,
                            "ALL_Driver_Information.DriverID": {
                                $in: ALL_DRIVERS_DATA
                            },
                            Request_Time: {
                                $gte: now_before_15_secs,
                                $lte: now
                            },
                        },
                        {
                            Instant_Order_Request_Status: 2,
                            DriverID: {
                                $in: ALL_DRIVERS_DATA
                            },
                            Request_Accepted_Time: {
                                $gte: now_before_10_mins,
                                $lte: now
                            },
                        }
                    ]
                };
                let Accepted_DriverIDArray = await Instant_Order_Request.distinct('DriverID', ifquery).lean();
                let TripQuery = {
                    DriverID: {
                        $in: ALL_DRIVERS_DATA
                    },
                    Whether_Trip_Weights_Fulfilled: true,
                    Total_Trip_Status: {
                        $in: [1, 2]
                    },
                    Status: true
                };
                let Trip_Drivers = await Trips.distinct('DriverID', TripQuery).lean();
                let FinalContactedArray = await Accepted_DriverIDArray.concat(Trip_Drivers);
                FinalContactedArray = await FinalContactedArray.concat(MappedDriverIDArray);
                async.eachSeries(ALL_DRIVERS_DATA, async (item, callback) => {
                    try {
                        let found = await FinalContactedArray.find(ele => ele == item);
                        if (found == null || found == undefined) {
                            Availables_Driver_Data.push(item);
                            callback();
                        } else {
                            callback();
                        }
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(Availables_Driver_Data);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Find_All_Instant_Available_Driver_Information = (ZoneData, MappedDriverIDArray) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DriverIDArray = await StoreController.Find_Instant_Available_DriverID_Array(ZoneData, MappedDriverIDArray);
                let query = {
                    DriverID: {
                        $in: DriverIDArray
                    },
                    CityID: ZoneData.CityID,
                    // ZoneID: ZoneData.ZoneID,
                    Whether_Driver_Online: true,
                    Status: true
                };
                let lookupmap = {
                    from: 'Vehicles',
                    localField: 'VehicleID',
                    foreignField: 'VehicleID',
                    as: 'Vehicle_Data'
                };
                let addfieldsmap = {
                    "Vehicle_Type": "$Vehicle_Data.Vehicle_Type",
                    // "Weight_Type": "$Vehicle_Data.Weight_Type",
                    // "Actual_Maximum_Weight": "$Vehicle_Data.Maximum_Weight",
                    // "Maximum_Weight": {
                    //     $divide: [
                    //         {
                    //             $multiply: [
                    //                 "$Vehicle_Data.Maximum_Weight",
                    //                 config.Vehicle_Maximum_Weight_Capacitance
                    //             ]
                    //         },
                    //         100
                    //     ]
                    // },
                    // "Minimum_Weight": {
                    //     $divide: [
                    //         {
                    //             $multiply: [
                    //                 "$Vehicle_Data.Maximum_Weight",
                    //                 config.Vehicle_Minimum_Weight_Capacitance
                    //             ]
                    //         },
                    //         100
                    //     ]
                    // },
                    // "Dimensions": "$Vehicle_Data.Dimensions"
                };
                let Data = await Drivers.aggregate().match(query).lookup(lookupmap).unwind('Vehicle_Data').addFields(addfieldsmap).project({ _id: 0, __v: 0, Vehicle_Data: 0 }).sort({ created_at: -1 }).exec();
                resolve(JSON.parse(JSON.stringify(Data)));
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Instant_Order_Send_Driver_Request_Fetch_Driver_Radius = (CountryID, CityID) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Driver_Radius_KMS = config.Gig_Driver_KMS;
            let query = {
                // CountryID: CountryID,
                // CityID: CityID,
                Status: true
            };
            let Result = await Order_Delivery_Setting.findOne(query).select('-_id -__v').lean();
            if (Result == null) {
                resolve(Driver_Radius_KMS);
            } else {
                resolve(Result.Gig_Driver_KMS);
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Validate_Country_City_State_Active = (BranchData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let CountryData = await CommonController.Check_for_Country(BranchData);
            let CityData = await CommonController.Check_for_City(BranchData);
            if (CountryData.Status) {
                if (CityData.Status) {
                    let StateID_Array = await State_Cities.distinct("StateID", { CityID: CityData.CityID, Status: true }).lean();
                    if (StateID_Array.length > 0) {
                        let stquery = {
                            StateID: {
                                $in: StateID_Array
                            },
                            Status: false
                        };
                        let StateInactive = await State.findOne(stquery).select('-_id -__v').lean();
                        if (StateInactive === null || StateInactive === undefined) {
                            resolve("Validated Successfully");
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                        };
                    } else {
                        resolve("Validated Successfully");
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                }
            } else {
                reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

StoreController.Fetch_Zone_Data = (BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Service_Type = 1;
                let lat = BranchData.Latitude;
                let lng = BranchData.Longitude;
                let Point = [lng, lat];
                let query = {
                    Service_Type: Service_Type,
                    Geometry: {
                        '$near': {
                            '$minDistance': 0,
                            '$maxDistance': 20000,
                            '$geometry': {
                                type: "Point",
                                coordinates: Point
                            }
                        },
                        // $geoIntersects: {
                        //     $geometry: {
                        //         type: "Point",
                        //         coordinates: Point
                        //     }
                        // }
                    },
                    Status: true
                };
                let Result = await ZONES.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.WE_DONT_SERVE_ON_YOUR_CURRENT_LOCATION } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

StoreController.Get_Single_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).lean().select('OrderID Order_Number Cart_Information Order_Status created_at').exec();
                for (const Product of Result.Cart_Information) {
                    delete Product.Price
                    delete Product.Discount_Percent
                    delete Product.Discount_Price
                    delete Product.Final_Price
                    delete Product.Admin_Share_Percent
                    delete Product.Admin_Share_Amount
                    delete Product.Branch_Share_Amount
                    delete Product.Total_Price
                    delete Product.Total_Discount_Price
                    delete Product.Total_Final_Price
                    delete Product.Total_Admin_Share_Amount
                    delete Product.Total_Branch_Share_Amount
                }
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Validate_Driver_OTP_For_Pickup = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                }
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    let Otpquery = {
                        OrderID: values.OrderID,
                        OTP: values.OTP
                    };
                    let OTPResult = await Driver_Order_OTP.findOne(Otpquery).lean().exec();
                    if (OTPResult != null) {
                        if (OTPResult.BranchID == BranchData.BranchID) {
                            let changes = {
                                $set: {
                                    Whether_OTP_Verified: true,
                                    OTP_Verified_Time: new Date()
                                }
                            };
                            let UpdateStatus = await Driver_Order_OTP.updateOne(Otpquery, changes).lean().exec();
                            let Status = "OTP Verified Successfully";
                            let TripRouteData = await Trips_Orders_Routings.findOne({ TripRouteID: OTPResult.TripRouteID }).lean().exec();
                            let TripData = await Trips_Orders_Routings.findOne({ TripID: OTPResult.TripID }).lean().exec();
                            let All_Orders_ID_Array = TripRouteData.All_Orders_ID_Array;
                            if (TripRouteData.Route_Action_Next == 1 || TripRouteData.Route_Action_Next == 3) {
                                if (TripRouteData.Route_Status == 1) {
                                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_INITIATED } };
                                } else if (TripRouteData.Route_Status == 2) {
                                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_REACHED } };
                                } else if (TripRouteData.Route_Status == 3) {
                                    // let SelectedOrderBarcodeProcessing = await StoreController.Validate_Selected_Orders_Barcode_Array_Pickup(req.body, TripRouteData);
                                    // let Selected_Orders_ID_Array = SelectedOrderBarcodeProcessing[0];
                                    // let Un_Selected_Orders_ID_Array = SelectedOrderBarcodeProcessing[1];
                                    let Result = await StoreController.Trip_Route_Pickup_Barcode_Processed(DriverData, TripData, TripRouteData, All_Orders_ID_Array);
                                    // let ItemImageProcessing = await DriverController.Selected_Orders_Pickup_Image(Selected_Orders_ID_Array, ItemImageData);
                                } else if (TripRouteData.Route_Status == 4) {
                                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
                                } else if (TripRouteData.Route_Status == 5) {
                                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
                                } else if (TripRouteData.Route_Status == 6) {
                                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
                                }
                            } else {
                                throw { success: false, extras: { msg: ApiMessages.INVALID_TRIP_ROUTE } };
                            }

                            let DriverData = await Drivers.findOne({ DriverID: OTPResult.DriverID }).lean().exec();
                            let Order_Status = 8;
                            let Order_Report = {
                                Title: "Order Picked",
                                Description: "Order Picked",
                                Time: new Date()
                            };
                            let OrderStatusesUpdate = DriverController.Common_Update_Order_Driver_Trip_Statuses(Order_Status, All_Orders_ID_Array, TripRouteData, DriverData, Order_Report);
                            resolve({ success: true, extras: { Status: Status } });

                            let SendNotification = await FcmController.Order_PickUp_Notification(Result, DriverData)


                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.INVALID_BRANCH_ORDER } });
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

StoreController.Trip_Route_Pickup_Barcode_Processed = (DriverData, TripData, TripRouteData, All_Orders_ID_Array) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 4;
                let Route_Status_Comment = "Pickup Loaded";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let changes = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        Selected_Orders_ID_Array: All_Orders_ID_Array,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let UpdatedStatus = await Trips_Orders_Routings.updateOne(query, changes).lean();
                resolve("Order Picked Successfully");
                //let SelectedOrderProcessing = await StoreController.Trip_Route_Pickup_Barcode_Processed_Complete_Functionality_Selected_Order_Processing(DriverData, TripData, TripRouteData, All_Orders_ID_Array);
                //let UnSelectedOrderProcessing = await DriverController.Trip_Route_Pickup_Barcode_Processed_Complete_Functionality_Un_Selected_Order_Processing(DriverData, TripData, TripRouteData, Un_Selected_Orders_ID_Array, 1);
                // async.eachSeries(All_Orders_ID_Array, async (item, callback) => {
                //     try {
                //         let oquery = {
                //             OrderID: item
                //         };
                //         let ochanges = {
                //             $set: {
                //                 Whether_Barcode_Number_Available: true,
                //                 Barcode_Number: item.Barcode_Number,
                //                 updated_at: new Date()
                //             }
                //         };
                //         let oUpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                //         callback();
                //     } catch (error) {
                //         callback(error);
                //     }
                // }, async (err) => {
                //if (err) reject(err);
                Route_Status = 5;
                Route_Status_Comment = "Pickup Loaded";
                Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let trquery = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let trchanges = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        Route_Action_Completed: TripRouteData.Route_Action_Next,
                        Route_Action_Completed_Time: new Date(),
                        Selected_Orders_ID_Array: All_Orders_ID_Array,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let trUpdatedStatus = await Trips_Orders_Routings.updateOne(trquery, trchanges).lean();
                //let AmountCollection = await DriverController.Trip_Route_Pickup_Barcode_Processed_Continue_Store_Collection_Amount(DriverData, TripData, TripRouteData);
                // });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}




// StoreController.Validate_Selected_Orders_Barcode_Array_Pickup = (values, TripRouteData) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let Selected_Orders_ID_Array = [];
//                 let All_Orders_ID_Array = TripRouteData.All_Orders_ID_Array;
//                 async.eachSeries(values.Selected_Orders_Barcode_Array, async (item, callback) => {
//                     try {
//                         if (
//                             item.OrderID != null && item.OrderID != undefined && item.OrderID != ''
//                             && item.Barcode_Number != null && item.Barcode_Number != undefined && item.Barcode_Number != ''
//                         ) {
//                             let AvailableOrderID = All_Orders_ID_Array.find(ele => ele == item.OrderID);
//                             if (AvailableOrderID == null) {
//                                 callback({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } });
//                             } else {
//                                 // let ValidityStatus = await DriverController.Validate_Pickup_Barcode_Already_Exist(item);
//                                 Selected_Orders_ID_Array.push(AvailableOrderID);
//                                 callback();
//                             }
//                         } else {
//                             callback({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
//                         }
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     let Un_Selected_Orders_ID_Array = await DriverController.Find_Un_Selected_Orders_ID(All_Orders_ID_Array, Selected_Orders_ID_Array);
//                     let Checking_Unique = values.Selected_Orders_Barcode_Array.every((item, index, arr) => (arr.filter(ele => ele.OrderID == item.OrderID).length == 1 && arr.filter(ele => ele.Barcode_Number == item.Barcode_Number).length == 1));
//                     if (Checking_Unique) {
//                         resolve([Selected_Orders_ID_Array, Un_Selected_Orders_ID_Array]);
//                     } else {
//                         reject({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
//                     }
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }



StoreController.Check_for_STORE_BRANCH = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                Store_Branch.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.Store_Branch_Not_Found } });
                    } else if (Result != null) {
                        resolve(Result);
                    }
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Check_for_STORE_SECTION = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID
                };
                Store_SECTIONS.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_SECTION } });
                    } else if (Result != null) {
                        resolve(Result);
                    }
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Check_for_STORE_PRODUCT = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    ProductID: values.ProductID
                };
                Store_Products.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.Product_Not_Found } });
                    } else if (Result != null) {
                        resolve(Result);
                    }
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Check_Whether_Store_Section_Product_Already_Linked = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID,
                    ProductID: values.ProductID
                };
                Store_SECTIONS_PRODUCTS.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        resolve('Validated Successfully');
                    } else if (Result != null) {

                        let update = {
                            Status: true,
                            Sl_NO: values.Sl_NO,
                            Default: values.Default
                        }
                        Store_SECTIONS_PRODUCTS.updateOne(query, update).lean().then((RemovedStatus) => {
                            resolve({ success: true, extras: { Status: "Removed Successfully" } })
                        }).catch((err) => {
                            console.error('Some DB Error--->', err);
                            reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                        })

                        reject({ success: false, extras: { msg: ApiMessages.SECTION_PRODUCT_ALREADY_EXIST } })
                    }
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Link_MENU_SECTION_PRODUCT = (values) => {
    // console.log(values)
    return new Promise((resolve, reject) => {
        setImmediate(() => {

            try {
                let Data = {
                    MENU_ID: values.MENU_ID,
                    SECTION_ID: values.SECTION_ID,
                    ProductID: values.ProductID,
                    Sl_NO: values.Sl_NO,
                    Default: values.Default,
                    created_at: new Date(),
                    updated_at: new Date()
                }
                // console.log(Data);
                Store_SECTIONS_PRODUCTS(Data).save().then((Result) => {

                    resolve({ success: true, extras: { Status: "Product Linked Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.List_ALL_Section_Products = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID,
                    SECTION_ID: values.SECTION_ID,
                    Status: true
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let ARRAYFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_SECTIONS_PRODUCTS.distinct('ProductID', query).lean().then((Result) => {
                                    resolve(Result);
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }

                let CountFunction = (ARRAY) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            let query = {
                                ProductID: {
                                    $in: ARRAY
                                },
                                Status: true
                            };
                            Store_Products.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = (ARRAY) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                let query = {
                                    ProductID: {
                                        $in: ARRAY
                                    },
                                    Status: true
                                };
                                Store_Products.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                ARRAYFunction().then((ARRAY) => {
                    CountFunction(ARRAY).then((Count) => {
                        ListFunction(ARRAY).then((Data) => {
                            resolve({ success: true, extras: { Count: Count, Data: Data } })
                        }).catch(err => reject(err));
                    }).catch(err => reject(err));
                }).catch(err => reject(err));
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Update_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                };
                let changes = {
                    $set: {
                        MENU_NAME: values.MENU_NAME,
                        MENU_DESCRIPTION: values.MENU_DESCRIPTION,
                        MENU_TYPE: values.MENU_TYPE,
                        MENU_URL: values.MENU_URL,
                        MENU_PRICE: parseFloat(values.MENU_PRICE),
                        updated_at: new Date()
                    }
                };
                Store_MENU.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.ACTIVATE_STORE_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                };
                let changes = {
                    $set: {
                        Status: true,
                        updated_at: new Date()
                    }
                };
                Store_MENU.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Activated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.List_ALL_Inactive_Menu = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    BranchID: values.BranchID,
                    Status: false
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let CountFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            Store_MENU.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_MENU.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                CountFunction().then((Count) => {
                    ListFunction().then((Data) => {
                        resolve({ success: true, extras: { Count: Count, Data: Data } });
                    }).catch(err => reject(err));
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};


StoreController.INACTIVATE_STORE_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                };
                let changes = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                };
                Store_MENU.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.ACTIVATE_STORE_MENU = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                };
                let changes = {
                    $set: {
                        Status: true,
                        updated_at: new Date()
                    }
                };
                Store_MENU.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Activated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.UPDATE_MENU_SECTION = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID
                };
                let changes = {
                    Sl_NO: values.Sl_NO,
                    SECTION_NAME: values.SECTION_NAME,
                    SECTION_DESCRIPTION: values.SECTION_DESCRIPTION,
                    MAX_SELECTION: values.MAX_SELECTION,
                    updated_at: new Date()
                };
                Store_SECTIONS.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Updated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.INACTIVATE_MENU_SECTION = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID
                };
                let changes = {
                    Status: false,
                    updated_at: new Date()
                };
                Store_SECTIONS.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Inactivated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.ACTIVATE_MENU_SECTION = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID
                };
                let changes = {
                    Status: true,
                    updated_at: new Date()
                };
                Store_SECTIONS.updateOne(query, changes).lean().exec().then((UpdateStatus) => {
                    resolve({ success: true, extras: { Status: "Activated Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.List_ALL_Inactive_Menu_Sections = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID,
                    Status: false
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let CountFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            Store_SECTIONS.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_SECTIONS.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                let Section_Products_Count = (SECTION_ID) => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                let query = {
                                    SECTION_ID: SECTION_ID
                                };
                                Store_SECTIONS.countDocuments(query).lean().then((Result) => {
                                    resolve(Result)
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }

                CountFunction().then((Count) => {
                    ListFunction().then((Data) => {
                        async.eachSeries(Data, (item, callback) => {
                            Section_Products_Count(item.SECTION_ID).then((ProductCount) => {
                                item.ProductCount = ProductCount;
                                callback();
                            }).catch(err => callback(err));
                        }, (err) => {
                            resolve({ success: true, extras: { Count: Count, Data: Data } })
                        })
                    }).catch(err => reject(err));
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Search_All_Store_Branch_Products = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let SearchValue = values.SearchValue;
                let SearchQuery = {
                    $regex: SearchValue, $options: "i"
                }
                let query = {
                    BranchID: values.BranchID,
                    Status: true,
                    $or: [
                        {
                            ProductName: SearchQuery
                        },
                        {
                            ProductDescription: SearchQuery
                        },
                        {
                            ProductName: SearchQuery
                        },
                        {
                            Actual_Price: SearchQuery
                        },
                        {
                            Selling_Price: SearchQuery
                        },
                    ]
                };
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };

                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_Products.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }

                ListFunction().then((Data) => {
                    resolve({ success: true, extras: { Data: Data } })
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

StoreController.Check_Whether_Store_Section_Product_Linked = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID,
                    ProductID: values.ProductID
                };
                Store_SECTIONS_PRODUCTS.findOne(query).lean().exec().then((Result) => {
                    if (Result == null) {
                        reject({ success: false, extras: { msg: ApiMessages.SECTION_PRODUCT_NOT_EXIST } })
                    } else if (Result != null) {

                        resolve(Result);
                    }
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Unlink_MENU_SECTION_PRODUCT = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID,
                    ProductID: values.ProductID,
                };

                let update = {
                    Status: false
                }

                Store_SECTIONS_PRODUCTS.updateOne(query, update).lean().then((RemovedStatus) => {
                    resolve({ success: true, extras: { Status: "Removed Successfully" } })
                }).catch((err) => {
                    console.error('Some DB Error--->', err);
                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                })
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
};

//CHECK WHETHER CATEGORY NAME EXIST OR NOT
StoreController.Get_Store_Menu_Data_By_URL = (MENU_URL, BranchID, MENU_ID, callback) => {
    let query;
    if (MENU_ID === "") {
        query = { MENU_URL: MENU_URL, BranchID: BranchID };
    } else {
        query = { MENU_URL: MENU_URL, BranchID: BranchID, "MENU_ID": { $ne: MENU_ID } };
    }
    Store_MENU.findOne(query, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.MENU_URL_ALREADY_IN_USE
                    }
                }));
            } else if (Result == null) {
                callback(false, new ApiResponce({
                    success: true,
                    extras: {
                        msg: "Store menu URL can be used"
                    }
                }));
            }
        }
    })
};

StoreController.Get_ALL_Menu = (BRANCH_ID, callback) => {
    var MenuList = [];
    Store_MENU.find({ BranchID: BRANCH_ID, Status: true }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result !== null) {

                async.eachSeries(Result, function (item, resp) {
                    var MenuListObj;
                    var MenuItemsX = [];

                    StoreController.Get_ALL_Menu_Sections(item.MENU_ID, function (err, SectionsData) {

                        if (err) {
                            callback(true, new ApiResponce({
                                success: false,
                                extras: {
                                    msg: ApiMessages.DATABASE_ERROR
                                }
                            }));
                        } else {
                            //  MenuListObj.MenuItem.push(SectionsData);
                            MenuItemsX.push(SectionsData);
                            resp()
                        }
                    });
                    MenuListObj = {
                        MENU_ID: item.MENU_ID,
                        MenuName: item.MENU_NAME,
                        MenuType: item.MENU_TYPE,
                        MenuPrice: item.MENU_PRICE,
                        MenuItem: MenuItemsX
                    };
                    MenuList.push(MenuListObj)
                }, function (err) {
                    if (!err) {
                        return callback(false, MenuList);
                    }
                    return callback(true);
                })


            }
        }
    })
};


StoreController.Get_ALL_Menu_Sections = (MENU_ID, callback) => {
    let query = {
        MENU_ID: MENU_ID,
        Status: true
    };
    Store_SECTIONS.find(query).select('-_id -BranchID -Status -created_at -__v -updated_at').sort({ Sl_NO: -1 }).lean().exec(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            if (Result == null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.Store_Branch_Not_Found
                    }
                }))
            } else if (Result != null) {

                var newResult = [];

                async.eachSeries(Result, function (item, resp) {
                    var resul;

                    var Store_Product_SectionCount_A = 0;
                    var Store_Product_Array = [];
                    let query = {
                        SECTION_ID: item.SECTION_ID,
                        Status: true
                    };
                    // Store_SECTIONS_PRODUCTS.countDocuments(query).lean().exec().then((Resultd) => {

                    //  Store_Product_SectionCount_A = Resultd;

                    Store_SECTIONS_PRODUCTS.find(query).lean().exec().then((Resultdx) => {



                        Store_Product_SectionCount_A = Resultdx.length;

                        // console.log("productsvalue" + Resultdx);


                        async.eachSeries(Resultdx, function (item, respx) {


                            let queryx = {
                                ProductID: item.ProductID,
                                Status: true
                            };

                            Store_Products.findOne(queryx).lean().exec().then((Resultdxe) => {

                                Resultdxe.Sl_NO = Resultdx.Sl_NO;
                                Resultdxe.Default = Resultdx.Default;

                                Store_Product_Array.push(Resultdxe);
                                respx();

                            }).catch((err) => {
                                console.error('Some DB Error--->', err);


                                // reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })



                        }, function (err) {
                            if (!err) {
                                console.log("operations performed successfully");
                                // callback(false, newResult);
                            }
                            //  return callback(true);

                            resul = {
                                SECTION_ID: item.SECTION_ID,
                                SECTION_NAME: item.SECTION_NAME,
                                MAX_SELECTION: item.MAX_SELECTION,

                                ItemCount: Store_Product_SectionCount_A,
                                Store_Products: Store_Product_Array
                            };
                            newResult.push(resul);
                            resp();

                            //  callback(false, newResult);
                        })


                        // resul = {
                        //     SECTION_NAME: item.SECTION_NAME,
                        //     ItemCount: Store_Product_SectionCount_A,
                        //     Store_Products: Store_Product_Array
                        // };
                        // newResult.push(resul);
                        // resp();
                        //resolve(Result);
                    }).catch((err) => {
                        console.error('Some DB Error--->', err);

                        resul = {
                            SECTION_ID: item.SECTION_ID,
                            SECTION_NAME: item.SECTION_NAME,
                            MAX_SELECTION: item.MAX_SELECTION,
                            ItemCount: Store_Product_SectionCount_A,
                            Store_Products: Store_Product_Array
                        };
                        newResult.push(resul);
                        resp();
                        // reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                    })
                    //resolve(Result);
                    // }).catch((err) => {
                    //     console.error('Some DB Error--->', err);
                    //     // reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                    // })




                }, function (err) {
                    if (!err) {
                        console.log("operations performed successfully");
                        // callback(false, newResult);
                    }
                    //  return callback(true);
                    callback(false, newResult);
                })

            }
        }
    });
};

StoreController.Get_Store_Admin_Branch_Data = (StoreAdminID, callback) => {
    var BranchData = [], ImageOriginal, Image100, Image250, Image550, Image900;
    Customers.findOne({ CustomerID: StoreAdminID }).select("BranchData").exec(function (err, AdminData) {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            async.eachSeries(AdminData.BranchData, function (item, resp) {
                Store_Branch.findOne({ BranchID: item.BranchID }).exec(function (err, Branch) {
                    if (err) {
                        callback(true, new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.DATABASE_ERROR
                            }
                        }));
                    } else {
                        Admin_Images.findOne({ ImageID: Branch.ImageID }).exec(function (err, ImageData) {
                            if (err) {
                                console.log(err);
                            } else {
                                if (ImageData == null) {
                                    ImageURL = '';
                                } else if (ImageData != null) {
                                    Image100 = config.S3URL + ImageData.Image100;
                                    Image250 = config.S3URL + ImageData.Image250;
                                    Image550 = config.S3URL + ImageData.Image550;
                                    Image900 = config.S3URL + ImageData.Image900;
                                    ImageOriginal = config.S3URL + ImageData.ImageOriginal;
                                }
                                BranchData.push({
                                    BranchID: item.BranchID,
                                    Branch_Name: item.Branch_Name,
                                    ImageData: {
                                        Image100: Image100,
                                        Image250: Image250,
                                        Image550: Image550,
                                        Image900: Image900,
                                        ImageOriginal: ImageOriginal
                                    }
                                });
                                resp();
                            }
                        })
                    }
                })
            }, function (err) {
                if (!err) {
                    return callback(new ApiResponce({
                        success: true,
                        extras: {
                            BranchData: BranchData
                        }
                    }));
                }
            })
        }
    });
};

StoreController.Update_Password = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CustomerID: values.StoreAdminID
                };
                var date = new Date();
                var multiplicity = { upsert: true };
                var CustomerData = await Customers.findOne(query).exec();

                var Salt = CustomerData.PasswordSalt;
                var Old_Password = values.Old_Password + Salt;
                var newpass = values.New_Password + Salt;
                var ExistPasswordHash = CustomerData.PasswordHash;
                var extpassword = crypto.createHash('sha512').update(Old_Password).digest("hex")
                var NewPasswordHash = crypto.createHash('sha512').update(newpass).digest("hex");

                var changes = {
                    PasswordHash: NewPasswordHash,
                    updated_at: date
                };

                if (ExistPasswordHash === extpassword) {
                    await Customers.updateOne(query, changes, multiplicity).exec(function (err, Result) {
                        if (Result) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Status: 'Password changed Successfully'
                                }
                            }));
                        }
                    });
                } else {
                    resolve(new ApiResponce({
                        success: false,
                        extras: {
                            msg: ApiMessages.INVALID_Old_PASSWORD
                        }
                    }));
                }

            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
};

StoreController.List_Menu = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let CountFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            Store_MENU.countDocuments(query).lean().exec().then((Result) => {
                                resolve(Result);
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                            })
                        });
                    });
                }
                let ListFunction = () => {
                    return new Promise((resolve, reject) => {
                        setImmediate(() => {
                            try {
                                Store_MENU.find(query).select('-_id -__v -updated_at').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec().then((Result) => {
                                    resolve(Result);
                                }).catch((err) => {
                                    console.error('Some DB Error--->', err);
                                    reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                                })
                            } catch (error) {
                                console.error('Something Error--->', error);
                            }
                        });
                    });
                }
                CountFunction().then((Count) => {
                    ListFunction().then((Data) => {
                        resolve({ success: true, extras: { Count: Count, Data: Data } })
                    }).catch(err => reject(err));
                }).catch(err => reject(err));

            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

StoreController.Suggeste_Serial_Number = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    SECTION_ID: values.SECTION_ID
                }
                await Store_SECTIONS_PRODUCTS.find(query).sort({ Sl_NO: -1 }).limit(1).exec(function (err, Result) {
                    if (Result) {
                        var ResultSLNo;
                        if (Result.length == 0) {
                            ResultSLNo = 0
                        } else {
                            ResultSLNo = Result[0].Sl_NO + 1
                        }

                        // console.log(Result)
                        resolve(new ApiResponce({
                            success: true,
                            extras: {
                                Next_Serial_Number: ResultSLNo
                            }
                        }));
                    } else {

                    }
                });

            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

StoreController.Sc_Suggeste_Serial_Number = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    MENU_ID: values.MENU_ID
                }
                await Store_SECTIONS.find(query).sort({ Sl_NO: -1 }).limit(1).exec(function (err, Result) {
                    if (Result) {
                        var ResultSLNo;
                        if (Result.length == 0) {
                            ResultSLNo = 1
                        } else {
                            ResultSLNo = Result[0].Sl_NO + 1
                        }

                        // console.log(Result)
                        resolve(new ApiResponce({
                            success: true,
                            extras: {
                                Next_Serial_Number: ResultSLNo
                            }
                        }));
                    } else {

                    }
                });

            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

StoreController.Check_Serial_Number_Available = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Sl_NO: values.Sl_NO,
                    SECTION_ID: values.SECTION_ID
                };
                await Store_SECTIONS_PRODUCTS.find(query).exec(function (err, Result) {
                    if (Result) {
                        // console.log(Result)
                        if (Result.length > 0) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Number_Used: true
                                }
                            }));
                        } else if (Result.length == 0) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Number_Used: false
                                }
                            }));
                        }
                    }
                });
            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

StoreController.Sc_Check_Serial_Number_Available = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Sl_NO: values.Sl_NO,
                    MENU_ID: values.MENU_ID
                };
                await Store_SECTIONS.find(query).exec(function (err, Result) {
                    if (Result) {
                        // console.log(Result)
                        if (Result.length > 0) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Number_Used: true
                                }
                            }));
                        } else if (Result.length == 0) {
                            resolve(new ApiResponce({
                                success: true,
                                extras: {
                                    Number_Used: false
                                }
                            }));
                        }
                    }
                });
            } catch (error) {
                console.error('Something Error--->', error);
                reject({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
            }
        });
    });
}

StoreController.Branch_Update_Online_Offline = (values, StoreAdminData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let Result = await Store_Branch.findOne(query).lean();
                if (Result.Whether_Branch_Online == false) {
                    let changes = {
                        $set: {
                            Whether_Branch_Online: true,
                            updated_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Store_Branch.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Updated Successfully", Whether_Branch_Online: true } });
                    let PhoneNumber = `${StoreAdminData.countryCode}${StoreAdminData.Phone}`;
                    let MsgData = 'Dear ' + StoreAdminData.First_name + ', \n Your BookStore with name:' + Result.Branch_Name + ' is now active for sale with Bookafella. \n --Thank you,  \n Bookafella Team'
                    let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, MsgData);
                } else {
                    let changes = {
                        $set: {
                            Whether_Branch_Online: false,
                            updated_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Store_Branch.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Updated Successfully", Whether_Branch_Online: false } });
                    let PhoneNumber = `${StoreAdminData.countryCode}${StoreAdminData.Phone}`;
                    let MsgData = 'Dear ' + StoreAdminData.First_name + ', \n Your BookStore with name:' + Result.Branch_Name + ' is now inactive for sale with Bookafella. \n --Thank you,  \n Bookafella Team'
                    let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, MsgData);
                }

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};
export default StoreController;