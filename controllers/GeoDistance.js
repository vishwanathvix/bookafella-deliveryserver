let GeoDistance = function () { };
import async from "async";
import CommonController from "./CommonController";
import config from "../config/config";

GeoDistance.Find_Geo_Distance = (x1, y1, x2, y2) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let CosValue = (Math.sin((x2 * Math.PI) / 180) * Math.sin((x1 * Math.PI) / 180)) + (Math.cos((x2 * Math.PI) / 180) * Math.cos((x1 * Math.PI) / 180) * Math.cos(((y1 * Math.PI) / 180) - ((y2 * Math.PI) / 180)));
                CosValue = (CosValue > 1) ? 1 : (CosValue < -1) ? -1 : CosValue;
                const ACosValue = Math.acos(CosValue);
                const distance = await CommonController.Common_Floating_Beautify_Value((Math.abs(ACosValue * config.earthRadius)) / 1000);
                resolve(distance);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
GeoDistance.Single_Origin_Multiple_Distance_Single_Pickup_Multiple_Delivery_with_Sort = (origins, destinations) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let x1, y1, x2, y2;
                x1 = origins.lat;
                y1 = origins.lng;
                async.eachSeries(destinations, async (item, callback) => {
                    try {
                        x2 = item.Drop_Information.lat;
                        y2 = item.Drop_Information.lng;
                        item.geo_distance = await GeoDistance.Find_Geo_Distance(x1, y1, x2, y2);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(destinations.sort((a, b) => a.geo_distance - b.geo_distance));
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

GeoDistance.Single_Origin_Multiple_Distance_Multiple_Pickup_Single_Delivery_with_Sort = (origins, destinations) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let x1, y1, x2, y2;
                x1 = origins.lat;
                y1 = origins.lng;
                async.eachSeries(destinations, async (item, callback) => {
                    try {
                        x2 = item.Pickup_Information.lat;
                        y2 = item.Pickup_Information.lng;
                        item.geo_distance = await GeoDistance.Find_Geo_Distance(x1, y1, x2, y2);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve(destinations.sort((a, b) => a.geo_distance - b.geo_distance));
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


GeoDistance.Single_Origin_Multiple_Distance_Trip_Routing_with_Sort = (origins, All_Divided_Orders_Data_Array) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let x1, y1, x2, y2;
                x1 = origins.lat;
                y1 = origins.lng;
                let destinations = await All_Divided_Orders_Data_Array.filter(item => item.Whether_to_Process_Route);
                All_Divided_Orders_Data_Array = await All_Divided_Orders_Data_Array.filter(item => !item.Whether_to_Process_Route);
                async.eachSeries(destinations, async (item, callback) => {
                    try {
                        x2 = item.lat;
                        y2 = item.lng;
                       let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(x1, y1, x2, y2);
                       item.geo_origin_address = await DistanceMatrixData.origin_addresses[0];
                       item.geo_destination_address = await DistanceMatrixData.destination_addresses[0];
                       item.geo_distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                       item.geo_duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
                       item.geo_distance /= 1000; //mtrs to Kms
                       item.geo_duration_minutes = (item.geo_duration / 60);
                       item.geo_duration *= 1000; //seconds to milliseconds
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    destinations = await destinations.sort((a, b) => a.geo_distance - b.geo_distance);
                    destinations = await destinations.concat(All_Divided_Orders_Data_Array);
                    resolve(destinations);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

export default GeoDistance;