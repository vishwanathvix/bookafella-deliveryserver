let PubnubController = function () { };
import config from "../config/config";
import PubNub from 'pubnub';
import Drivers from "../models/Drivers";
import CommonController from "./CommonController";
import async from "async";
import os from "os";
import DriverController from "./DriverController";
let pubnub = new PubNub(Object.assign({}, config.Pubnub));



PubnubController.Cancel_Order_Message = (DriverData, CancellRequestData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let ChannelName = DriverData.DriverID;
            let Data = {
                type: 3,
                description: "Cancell Request",
                CancellRequestData: CancellRequestData
            };
            let HittingSocket = await PubnubController.Common_Publish_Message(ChannelName, Data);
            resolve("Send Successfully");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

PubnubController.Instant_Order_Send_Driver_Request_Pubnub_Message = (ALL_DRIVERS_DATA, InstantOrderRequestData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                InstantOrderRequestData = await CommonController.Check_for_Instant_Order_Request(InstantOrderRequestData);
                async.eachSeries(ALL_DRIVERS_DATA, async (DriverData, callback) => {
                    try {
                        let ChannelName = DriverData.DriverID;
                        let Data = {
                            type: 1,
                            description: "Instant Request",
                            InstantOrderRequestData: InstantOrderRequestData
                        };
                        let HittingSocket = await PubnubController.Common_Publish_Message(ChannelName, Data);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Send Successfully");
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

PubnubController.Send_Common_Instant_Request_Pubnub_Messaging_for_Common_Channel = (InstantOrderRequestData, type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                InstantOrderRequestData = await CommonController.Check_for_Instant_Order_Request(InstantOrderRequestData);
                let description = (type == 1) ? "Order Request Accepted" : (type == 2) ? "Order Request Rejected" : (type == 3) ? "Order Placed" : "Instant Order Request Cancelled";
                let Data = {
                    type: type,
                    description: description,
                    InstantOrderRequestData: InstantOrderRequestData
                };
                let ChannelName = config.Pubnub.instantRequestChannelName;
                let HittingSocket = await PubnubController.Common_Publish_Message(ChannelName, Data);
                resolve("Sent successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

PubnubController.Common_Publish_Message = (ChannelName, Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let PublishData = {
                    channel: ChannelName,
                    message: Data
                };
                let PublishMessage = await pubnub.publish(PublishData);
                resolve("Sent Successfully");
            } catch (error) {
                console.error("Pubnub Error------>", error);
                resolve("Sent Successfully");
            }
        });
    });
}

(async function () {
    let ListenerData = {
        message: async function (msg) {
            try {
                // console.log(msg)
                if (msg.channel == SubscribeData.channels[0] && (os.hostname() == "bookafella.in" || os.hostname() == "xapi.bookafella.in")) {
                    let PublishData = JSON.parse(JSON.stringify(msg.message));
                    if (
                        PublishData.DriverID != null && PublishData.DriverID != undefined && PublishData.DriverID != ''
                        && PublishData.Latitude != null && PublishData.Latitude != undefined && isFinite(PublishData.Latitude) && !isNaN(PublishData.Latitude)
                        && PublishData.Longitude != null && PublishData.Longitude != undefined && isFinite(PublishData.Longitude) && !isNaN(PublishData.Longitude)
                    ) {
                        let DriverID = PublishData.DriverID;
                        let Latitude = parseFloat(PublishData.Latitude);
                        let Longitude = parseFloat(PublishData.Longitude);
                        let Driver_Current_Location_Point = [Longitude, Latitude];
                        let driverupdatequery = {
                            DriverID: DriverID
                        };
                        let driverupdatechanges = {
                            $set: {
                                "Driver_Current_Location.Latitude": Latitude,
                                "Driver_Current_Location.Longitude": Longitude,
                                Driver_Current_Location_Point: Driver_Current_Location_Point,
                                "Driver_Device_Information.Last_Location_Update_Time": new Date(),
                                updated_at: new Date()
                            }
                        };
                        let UpdatedStatus = await Drivers.updateOne(driverupdatequery, driverupdatechanges).lean();
                        let DriverLatlongStoringProcessing = await DriverController.Driver_LatLong_Storing_along_with_Distance(PublishData);
                    }
                }
            } catch (error) {
                console.error("Some Pubnub Subscribe Error", error);
            }
        }
    };
    var ListenerMessage = await pubnub.addListener(ListenerData);
    let SubscribeData = {
        channels: [config.Pubnub.masterChannelName]
    };
    var SubscribeMessage = await pubnub.subscribe(SubscribeData);
}());


export default PubnubController;