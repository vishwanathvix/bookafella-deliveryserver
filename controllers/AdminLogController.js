//Controllers
import AdminController from '../controllers/AdminController';
import AdminLogController from '../controllers/AdminLogController';
//Models
 // import logger from '../core/logger/logger';
import ApiMessages from '../models/ApiMessages'
import Admin_Logs from '../models/Admin_Logs';
import config from '../config/config';


//Dependencies

import moment from "moment";

exports.Swap_Collection_Products_Logs = (values, AdminData, CountryData, CollectionData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Product1 = String(ProductData[0].ProductName);
    let Product2 = String(ProductData[1].ProductName);
    let Message = `${Name} have swapped Products of collections(${CollectionData.CollectionName}) , Products (${Product1} and ${Product2}) in ${CountryName} on ${Time}`;
    let Key = `Collection Products`;
    let Purpose = `Swap Collection Products`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Swap_Collections_Logs = (values, AdminData, CountryData, CollectionData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Collection1 = String(CollectionData[0].CollectionName);
    let Collection2 = String(CollectionData[1].CollectionName);
    let Message = `${Name} have swapped collections (${Collection1} and ${Collection2}) in ${CountryName} on ${Time}`;
    let Key = `Collection`;
    let Purpose = `Swap Collections`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Remove_Sub_Category_Image_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have removed image of subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Remove Sub Category Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Update_Sub_Category_Image_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have edited image of subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Edit Sub Category Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Remove_Category_Image_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have edited image of category(${CategoryName}) in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Remove Category Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Update_Category_Image_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have edited image of category(${CategoryName}) in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Edit Category Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Activate_Category_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have removed category(${CategoryName}) inside ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Remove Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Activate_Subcategory_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have activated subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Activate Sub Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.List_All_Inactive_SubCategories_Log = (values, AdminData, CountryData, CategoryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have viewed all Inactive Sub Categories List of Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `List Inactive Sub Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}
exports.List_All_Inactive_Category_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Inactive Categories List of ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `List Inactive Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Remove_Category_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have removed category(${CategoryName}) inside ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Remove Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Remove_Subcategory_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have removed subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Remove Sub Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Country_BRANCH_URL_TAG_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Branch URL Tag of ${CountryName} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Branch URL Tag`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Remove_Collection_Log = (values, AdminData, CountryData, CollectionData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have removed collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Remove Collection`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}

exports.Remove_Collection_Product_Log = (values, AdminData, CountryData, CollectionData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have removed Product(${ProductData.ProductName}) from collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections Products`;
    let Purpose = `Remove Collection Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}

exports.Edit_Collection_Product_Serial_Number_Log = (values, AdminData, CountryData, CollectionData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have edited serial number of Product(${ProductData.ProductName}) inside collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections Products`;
    let Purpose = `Edit Collections Product Serial Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}

exports.Edit_Collection_Name_Log = (values, AdminData, CountryData, CollectionData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have edited collection name from ${CollectionName} to ${values.CollectionName} in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Edit Collections Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}
exports.Edit_Collection_Serial_Number_Log = (values, AdminData, CountryData, CollectionData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have edited collection number inside collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Edit Collections Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}

exports.Edit_Collection_URL_Tag_Log = (values, AdminData, CountryData, CollectionData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have edited collection url inside collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Edit Collections URL`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    });
}

exports.Change_Category_of_Subcategory_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have moved subcategory-> ${SubCategoryName} to ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Change Sub Category of Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Remove_Home_Slider_Log = (values, AdminData, CountryData, Home_Slider_Data) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have removed Home Slider Image(${Home_Slider_Data.ImageName}) on ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Remove Home Slider`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Home_Slider_ImageLink_Log = (values, AdminData, CountryData, Home_Slider_Data) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Home Slider Image(${Home_Slider_Data.ImageName}) Link on ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Edit Home Slider Link`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Home_Slider_ImageName_Log = (values, AdminData, CountryData, Home_Slider_Data) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ImageName = values.ImageName;
    let Message = `${Name} have edited Home Slider Image from ${Home_Slider_Data.ImageName} to ${ImageName} Serial Number on ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Edit Home Slider Image Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Home_Slider_Serial_Number_Log = (values, AdminData, CountryData, Home_Slider_Data) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Home Slider Image(${Home_Slider_Data.ImageName}) Serial Number on ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Edit Home Slider Serial Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Forgot_Password_Log = (AdminData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let Message = `${Name} have Forgot is Password and have resetted is Password in Admin Dashboard on ${Time}`;
    let Key = `Admin`;
    let Purpose = `Admin Forgot Password`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Update_Password_Log = (AdminData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let Message = `${Name} have changed is Password in Admin Dashboard on ${Time}`;
    let Key = `Admin`;
    let Purpose = `Admin Password`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Category_Name_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have edited name of category(${CategoryName}) in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Edit Category Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Category_Serial_Number_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have edited number of category(${CategoryName}) in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Edit Category Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}


exports.Edit_Category_Tag_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have edited url tag of category(${CategoryName}) in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Edit Category Tag`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_SubCategory_Tag_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have edited url tag of subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Edit Sub Category Tag`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Country_Default_Country_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Default Branch of ${CountryName} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Default Branch`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Country_Display_Price_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Display Price of ${CountryName} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Display Price`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Country_Currency_Side_log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Currency Side of ${CountryName} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Currency Side`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Country_Currency_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have edited Currency of ${CountryName} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Currency`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Country_Branch_Name_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CountryNameNew = values.CountryName;

    let Message = `${Name} have edited Country Name from ${CountryName} to ${CountryNameNew} on ${Time}`;
    let Key = `Country`;
    let Purpose = `Edit Country Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_SubCategory_Serial_Number_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let Message = `${Name} have edited serial number of subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Edit Sub Category Serial Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_SubCategory_Name_Log = (values, AdminData, CountryData, CategoryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(values.SubCategoryName);
    let Message = `${Name} have edited name of subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Edit Sub Category Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Change_Product_Category_SubCategory_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Message = `${Name} have changed Category and Subcategory of Product->${ProductName} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Category Subcategory`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}
exports.Edit_Collection_Image_Log = (values, AdminData, CountryData, CollectionData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = String(CollectionData.CollectionName);
    let Message = `${Name} have edited Collection(${CollectionName}) Image  in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Edit Collection Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Activate_Product_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Message = `${Name} have Inactivated Product(${ProductName}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Product Activated`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.List_All_Inactive_Products_Log = (values, AdminData, CountryData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Inactive Products  in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `List all Inactive Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Inactivate_Product_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Message = `${Name} have Inactivated Product(${ProductName}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Product Inactivated`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Product_Media_SWAP_Media_Logs = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Message = `${Name} have swapped Product(${ProductName}) Media in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Swap Product Media`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_Specification_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Specifications = String(ProductData.Specifications);
    let SpecificationsNew = String(values.Specifications);
    let Message = `${Name} have changed Product(${ProductName}) Specifications from (${Specifications}) to (${SpecificationsNew}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Specifications`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_Highlights_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Highlights = String(ProductData.Highlights);
    let HighlightsNew = String(values.Highlights);
    let Message = `${Name} have changed Product(${ProductName}) Highlight from (${Highlights}) to (${HighlightsNew}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Highlight`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_Description_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Description = String(ProductData.Description);
    let DescriptionNew = String(values.Description);
    let Message = `${Name} have changed Product(${ProductName}) Description from (${Description}) to (${DescriptionNew}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Description`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Product_Model_Number_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let ProductModel_Number = String(ProductData.ProductModel_Number);
    let NewProductModel_Number = String(values.ProductModel_Number);
    let Message = `${Name} have changed Product(${ProductName}) Model Number from ${ProductModel_Number} to ${NewProductModel_Number} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Model Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Edit_Product_SKU_Code_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let SKU_Code = String(ProductData.SKU_Code);
    let SKU_CodeNew = String(values.SKU_Code);
    let Message = `${Name} have changed Product(${ProductName}) SKU Code from ${SKU_Code} to ${SKU_CodeNew} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product SKU Code`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_SerialNumber_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let ProductSerialNumber = String(ProductData.ProductSerialNumber);
    let ProductSerialNumberNew = String(values.ProductSerialNumber);
    let Message = `${Name} have changed Product(${ProductName}) SerialNumber from ${ProductSerialNumber} to ${ProductSerialNumberNew} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Serial Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_URL_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let ProductURL = String(ProductData.ProductURL);
    let ProductURLNew = String(values.ProductURL);
    let Message = `${Name} have changed Product(${ProductName}) URL from ${ProductURL} to ${ProductURLNew} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product URL`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_Price_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Price = String(ProductData.Price);
    let PriceNew = String(values.Price);
    let Message = `${Name} have changed Product(${ProductName}) Price from ${Price} to ${PriceNew} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Price`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}

exports.Edit_Product_Name_Log = (values, AdminData, CountryData, ProductData) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let ProductNameNew = String(values.ProductName);
    let Message = `${Name} have changed Product Name from (${ProductName}) to (${ProductNameNew}) in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Name`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {

    })
}
exports.Activate_Admin_User_Log = async (values, MasterData, AdminData) => {
    let Name = MasterData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let AdminName = TitleCase(AdminData.Name);
    let Message = `${Name} have Activated Admin User(${AdminName}) on ${Time}`;
    let Key = `Admin User`;
    let Purpose = `List Admin User`;
    AdminLogController.Save_Admin_Log(MasterData, Message, Key, Purpose, (Result) => {

    });
}

exports.List_All_Inactive_Admin_Users_Log = async (values, MasterData) => {
    let Name = MasterData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let Message = `${Name} have viewed all Inactive Admin Users on ${Time}`;
    let Key = `Admin User`;
    let Purpose = `List Admin User`;
    AdminLogController.Save_Admin_Log(MasterData, Message, Key, Purpose, (Result) => {

    });
}

exports.Remove_Admin_User_Log = async (values, MasterData, AdminData) => {
    let Name = MasterData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let AdminName = TitleCase(AdminData.Name);
    let Message = `${Name} have removed Admin User(${AdminName}) on ${Time}`;
    let Key = `Admin User`;
    let Purpose = `List Admin User`;
    AdminLogController.Save_Admin_Log(MasterData, Message, Key, Purpose, (Result) => {

    });
}

exports.List_All_Admin_Users_Log = async (values, MasterData) => {
    let Name = MasterData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let Message = `${Name} have viewed all Admin Users on ${Time}`;
    let Key = `Admin User`;
    let Purpose = `List Admin User`;
    AdminLogController.Save_Admin_Log(MasterData, Message, Key, Purpose, (Result) => {

    });
}

exports.Create_Admin_User_Log = async (values, MasterData) => {
    let Name = MasterData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let AdminName = TitleCase(values.Name);
    let Message = `${Name} have Created Admin Users on ${Time}`;
    let Key = `Admin User`;
    let Purpose = `Create Admin User`;
    AdminLogController.Save_Admin_Log(MasterData, Message, Key, Purpose, (Result) => {

    });
}

exports.List_All_Collection_Products_Log = (values, AdminData, CountryData, CollectionData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let Message = `${Name} have viewed all Products inside collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Collections Products List`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    });
}

exports.Add_Product_to_Collection_Log = (values, AdminData, CountryData, CollectionData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = CollectionData.CollectionName;
    let ProductName = ProductData.ProductName;
    let Message = `${Name} have added Product(${ProductName}) inside collection(${CollectionName}) in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Add Product Collections`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    });
}
exports.List_All_Collections_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Collections of ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Viewed Collections`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    });
}


exports.Add_Collection_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CollectionName = String(values.CollectionName);
    let Message = `${Name} have added Collection->${CollectionName} in ${CountryName} on ${Time}`;
    let Key = `Collections`;
    let Purpose = `Add Collection`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Remove_Trending_Product_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Trending_Serial_Number = parseInt(values.Trending_Serial_Number);
    let Message = `${Name} have remove Product->${ProductName} of Trending Product of Serial Number(${Trending_Serial_Number}) in ${CountryName} on ${Time}`;
    let Key = `Trending Product`;
    let Purpose = `Remove Trending Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}
exports.Find_All_Trending_Product_Logs = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Trending Products of ${CountryName} on ${Time}`;
    let Key = `Trending Product`;
    let Purpose = `List Trending Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}
exports.Add_Trending_Product_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let Trending_Serial_Number = parseInt(values.Trending_Serial_Number);
    let Message = `${Name} have added Product->${ProductName} as Trending Product in Serial Number(${Trending_Serial_Number}) in ${CountryName} on ${Time}`;
    let Key = `Trending Product`;
    let Purpose = `Add Trending Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}
exports.Search_All_Country_Products_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let SearchValue = String(values.SearchValue);
    let Message = `${Name} have searched(${SearchValue}) in Products of ${CountryName} on ${Time}`;
    let Key = `Products`;
    let Purpose = `Searched Country Products`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.List_All_Country_Products_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Products of ${CountryName} on ${Time}`;
    let Key = `Products`;
    let Purpose = `Viewed Country Products`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.List_All_Home_Slider_Images_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Home Slider Images of ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Viewed Slider Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}


exports.Add_Home_Slider_Image_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ImageName = String(values.ImageName);
    let Message = `${Name} have added Home Slider Image>${ImageName} in ${CountryName} on ${Time}`;
    let Key = `Home Slider`;
    let Purpose = `Add Home Slider Image`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Edit_Product_Media_Serial_Number_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let CategoryName = String(ProductData.CategoryName);
    let SubCategoryName = String(ProductData.SubCategoryName);
    let Message = `${Name} have changed Serial Number of Media in Product->${ProductName} inside Category->${SubCategoryName} inside Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Update Product Media Serial Number`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Add_More_Media_to_Product_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let CategoryName = String(ProductData.CategoryName);
    let SubCategoryName = String(ProductData.SubCategoryName);
    let Message = `${Name} have Added Media in Product->${ProductName} inside Category->${SubCategoryName} inside Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Add Product Media`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Remove_Product_Media_Log = (values, AdminData, CountryData, ProductData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let ProductName = String(ProductData.ProductName);
    let CategoryName = String(ProductData.CategoryName);
    let SubCategoryName = String(ProductData.SubCategoryName);
    let Message = `${Name} have Removed Media of Product->${ProductName} inside Category->${SubCategoryName} inside Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Remove Product Media`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.List_All_Products_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Products  in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `List All Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Add_Product_Log = (values, AdminData, CountryData, CategoryData, SubCategoryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(SubCategoryData.SubCategoryName);
    let ProductName = String(values.ProductName);
    let Message = `${Name} have created Product-> ${ProductName} inside Subcategory->${SubCategoryName} inside Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Product`;
    let Purpose = `Add Product`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.List_All_SubCategories_Log = (values, AdminData, CountryData, CategoryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let Message = `${Name} have viewed all Sub Categories List of Category->${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `List Sub Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Create_SubCategory_Log = (values, AdminData, CountryData, CategoryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(CategoryData.CategoryName);
    let SubCategoryName = String(values.SubCategoryName);
    let Message = `${Name} have created subcategory-> ${SubCategoryName} inside ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Sub Category`;
    let Purpose = `Create Sub Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.List_All_Category_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let Message = `${Name} have viewed all Categories List of ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `List Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Create_Category_Log = (values, AdminData, CountryData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let CountryName = CountryData.CountryName;
    let CategoryName = String(values.CategoryName);
    let Message = `${Name} have created category-> ${CategoryName} in ${CountryName} on ${Time}`;
    let Key = `Category`;
    let Purpose = `Create Category`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

exports.Store_Admin_Login_Log = (AdminData, callback) => {
    let Name = AdminData.Name;
    let Time = moment(new Date()).utcOffset(330).format(config.DateFormat);
    let Message = `${Name} have login in Admin Dashboard on ${Time}`;
    let Key = `Admin Login`;
    let Purpose = `Admin Login`;
    AdminLogController.Save_Admin_Log(AdminData, Message, Key, Purpose, (Result) => {
        callback(Result);
    })
}

//Save Log Common for All Admins
exports.Save_Admin_Log = (AdminData, Message, Key, Purpose, callback) => {
    let date = new Date();
    let data = {
        AdminID: AdminData.AdminID,
        Whether_Master: AdminData.Whether_Master,
        Name: AdminData.Name,
        EmailID: AdminData.EmailID,
        Message: Message,
        Key: Key,
        Purpose: Purpose,
        created_at: date,
        updated_at: date
    };
    Admin_Logs(data).save((err, Result) => {
        if (err) {
            logger.info("Admin Save Error");
            logger.info(JSON.parse(JSON.stringify(err)));
        } else {
            callback("Log Stored Successfully");
        }
    })
}
//Convert String to title case
let TitleCase = (str) => {
    let myArr = str.toLowerCase().split(" ");
    for (let a = 0; a < myArr.length; a++) {
        myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
    }
    return myArr.join(" ");
}