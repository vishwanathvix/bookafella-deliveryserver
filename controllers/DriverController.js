let DriverController = function () { };
import uuid from "uuid";
import ApiMessages from "../models/ApiMessages";
import CommonController from "./CommonController";
import Country from "../models/Country";
import City from "../models/City";
import Counters from "../models/Counters";
import async from "async";
import ZONES from "../models/ZONES";
import Country_JSON from "../config/countries.json";
import City_JSON from "../config/cities.json";
import Zone_Hubs from "../models/Zone_Hubs";
//import Packages from "../models/Packages";
import Vehicles from "../models/Vehicles";
import { isBoolean, Boolify } from "node-boolify";
import moment from "moment";
import config from "../config/config";
import Drivers from "../models/Drivers";
import AdminLogController1 from "./AdminLogController1";
import RazorpayController from "./RazorpayController";
import Driver_Documents from "../models/Driver_Documents";
import Driver_Ratings from "../models/Driver_Ratings";
import Driver_OTPS from "../models/Driver_OTPS";
import MessagesController from "./MessagesController";
import Driver_OTP_Tries from "../models/Driver_OTP_Tries";
import Driver_Devices from "../models/Driver_Devices";
import Devices from "../models/Devices";
import Trips_Orders_Routings from "../models/Trips_Orders_Routings";
import Trips from "../models/Trips";
import Instant_Order_Request from "../models/Instant_Order_Request";
import PubnubController from "./PubnubController";
import Driver_Wallet_Logs from "../models/Driver_Wallet_Logs";
import Driver_Bank_Beneficiary_Accounts from "../models/Driver_Bank_Beneficiary_Accounts";
import Driver_Wallet_Withdraw_Transactions from "../models/Driver_Wallet_Withdraw_Transactions";
import Driver_Add_Money_Requests from "../models/Driver_Add_Money_Requests";
import Driver_Salary_Records from "../models/Driver_Salary_Records";
import Buyer_Orders from "../models/Buyer_Orders";
import Driver_Order_OTP from "../models/Driver_Order_OTP";
import Driver_Location_Logs from "../models/Driver_Location_Logs";
import GeoDistance from "./GeoDistance";
import Store_Branch from "../models/Store_Branch";
import Store_Branch_Wallet_Logs from "../models/Store_Branch_Wallet_Logs";
import Company_Wallet from "../models/Company_Wallet";
import Company_Wallet_Log from "../models/Company_Wallet_Log";
import Driver_Licence from "../models/Driver_Licence";
import Create_Driver_OTPS from "../models/Create_Driver_OTPS";
import Create_Driver_OTP_Tries from "../models/Create_Driver_OTP_Tries";
import Buyers from "../models/Buyers";
import FcmController from "./FcmController";
import Driver_COD_Collected from "../models/Driver_COD_Collected";
import NotificationController from "./NotificationController";

DriverController.List_All_Driver_Cancelled_Orders = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let matchquery = {
                    Cancel_Status: 4,
                    'Cancelled_By.DriverID': values.DriverID,
                };
                let Count = await Buyer_Orders.countDocuments(matchquery).lean().exec();
                let Result = await Buyer_Orders.find(matchquery).sort(sortOptions).select('OrderID Order_Number Cart_Information Order_Status Cancel_Reason Branch_Information Cancellation_Pickup_Status Cancellation_Order_Pickup_Report Whether_Cancellation_Notified Whether_Cancellation_Notified_Accepted created_at').lean().skip(toSkip).limit(toLimit).exec();
                for (const Order of Result) {
                    for (const Product of Order.Cart_Information) {
                        Product.Branch_Information = Order.Branch_Information
                        delete Product.Price
                        delete Product.Discount_Percent
                        delete Product.Discount_Price
                        delete Product.Final_Price
                        delete Product.Admin_Share_Percent
                        delete Product.Admin_Share_Amount
                        delete Product.Branch_Share_Amount
                        delete Product.Total_Price
                        delete Product.Total_Discount_Price
                        delete Product.Total_Final_Price
                        delete Product.Total_Admin_Share_Amount
                        delete Product.Total_Branch_Share_Amount
                    }
                    delete Order.Branch_Information
                }
                resolve({ success: true, extras: { Count: Count, Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

DriverController.Driver_Order_Cancel_Request = (values, TripData, TripRouteData, OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (OrderData.Cancel_Status == 0) {
                    let Oquery = {
                        OrderID: OrderData.OrderID,
                    };
                    let Ochnages = {
                        $set: {
                            Cancel_Status: 4, //cancelled by Driver
                            Cancelled_By: {
                                DriverID: values.DriverID
                            },
                            Cancel_Reason: values.Cancel_Reason,
                            updated_at: new Date()
                        },
                    }
                    let UpdateOrder = await Buyer_Orders.updateOne(Oquery, Ochnages).lean().exec();
                    resolve({ success: true, extras: { Status: "Cancel Request Palced Successfully" } });
                    let SendNotification = await NotificationController.SendNotification_Driver_Order_Cancel_Request(values, OrderData, DriverData)
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.CANCEL_ALREADY_REQUESTED_BY_OTHER } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driver_Journey_Trip_Route_Reload = (values, TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Location = {
                    lat: parseFloat(values.lat),
                    lng: parseFloat(values.lng)
                };
                resolve({ success: true, extras: { Status: "Routing being Processing" } })
                let routing_functionality = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation(TripData, Location);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Trip_Journey_Routes_with_all_Pending_Tasks = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let end_date = moment().endOf('day').toDate();
                let time_options = {
                    $lte: end_date
                }
                let trquery = {
                    Status: true,
                    //Whether_Trip_Weights_Fulfilled: true,
                    $and: [
                        {
                            $or: [
                                {
                                    DriverID: values.DriverID
                                },
                                {
                                    "All_Driver_Logs.DriverID": values.DriverID
                                }
                            ]
                        },
                        {
                            $or: [
                                // {
                                //     Whether_Window_Trip: false,
                                //     created_at: time_options,
                                //     Total_Trip_Status: {
                                //         $in: [1, 2, 3]
                                //     }
                                // },
                                {
                                    // Whether_Window_Trip: true,
                                    //Window_Start_Time: time_options,
                                    Total_Trip_Status: {
                                        $in: [1, 2, 3]
                                    }
                                },
                                {
                                    Total_Trip_Status: {
                                        $in: [1, 2]
                                    }
                                }
                            ]
                        }
                    ]
                };
                let TripID_Arary = await Trips.distinct('TripID', trquery).lean();
                let query = {
                    DriverID: values.DriverID,
                    Status: true,
                    TripID: {
                        $in: TripID_Arary
                    },
                    Route_Status: {
                        $in: [1, 2, 3, 4]
                    }

                };
                let Result = await Trips_Orders_Routings.find(query).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ TripNumber: 1, Route_Number: 1 }).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Trip_Journey_Routes = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    TripID: values.TripID,
                    DriverID: values.DriverID,
                    Route_Status: {
                        $ne: 6
                    },
                    Status: true
                };
                let Result = await Trips_Orders_Routings.find(query).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ TripNumber: 1, Route_Number: 1 }).lean().exec();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

// DriverController.List_All_Trips_with_all_Pending_Tasks = (values) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 let trquery = {
//                     Status: true,
//                     $and: [
//                         {
//                             $or: [
//                                 {
//                                     DriverID: values.DriverID
//                                 },
//                                 {
//                                     "All_Driver_Logs.DriverID": values.DriverID
//                                 }
//                             ]
//                         },
//                         {
//                             $or: [
//                                 {
//                                     Total_Trip_Status: {
//                                         $in: [1, 2]
//                                     }
//                                 }
//                             ]
//                         }
//                     ]
//                 };
//                 let Result = await Trips.find(trquery).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ TripNumber: 1 }).lean().exec();
//                 async.eachSeries(Result, async (item, callback) => {
//                     try {
//                         let q = {
//                             OrderID: item.OrderID
//                         };
//                         let r = await Buyer_Orders.findOne(q).select('Delivery_Address_Information Branch_Information Order_Status').lean().exec();
//                         item.Pickup_Information = r.Branch_Information;
//                         item.Drop_Information = r.Delivery_Address_Information;
//                         item.Order_Status = r.Order_Status
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve({ success: true, extras: { Data: Result } })
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

DriverController.Driver_Order_History = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                let sortOptions = {
                    created_at: -1
                };
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    DriverID: values.DriverID,
                    Total_Trip_Status: 3
                };
                let Count = await Trips.countDocuments(query).lean().exec();
                let Result = await Trips.find(query).select('-_id -__v').sort(sortOptions).lean().skip(toSkip).limit(toLimit).exec();
                async.eachSeries(Result, async (item, callback) => {
                    try {
                        let q = {
                            OrderID: item.OrderID
                        };
                        let r = await Buyer_Orders.findOne(q).select('Order_Number Cart_Information Delivered_To Order_Report Driver_Information Order_Status Order_Invoice.Order_Delivery_Charge Branch_Information Delivery_Address_Information BuyerID').lean().exec();
                        item.Delivery_Charge = r.Order_Invoice.Order_Delivery_Charge;
                        item.Pickup_Address_Information = r.Branch_Information;
                        item.Drop_Address_Information = r.Delivery_Address_Information;
                        item.Order_Status = r.Order_Status
                        item.Delivered_To = r.Delivery_Address_Information.Name;
                        item.Order_Number = r.Order_Number;
                        item.Cart_Information = r.Cart_Information

                        let OrderReport = []
                        for (let Order_Report of r.Order_Report) {
                            let Data = new Object();
                            if (Order_Report.Title == "Order Received") {
                                Data = {
                                    Status: "Order Received",
                                    Description: r.Branch_Information.Branch_Name + " has received your order successfully",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Driver Assigned") {
                                Data = {
                                    Status: "Driver Assigned",
                                    Description: "Your assigned delivery executive for this order is Mr." + r.Driver_Information.Driver_Name,
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Order Picked") {
                                Data = {
                                    Status: "Order Picked Up",
                                    Description: "Mr." + r.Driver_Information.Driver_Name + " has arrived at the bookstore to pick up your order",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Drop Stated") {
                                // let q = {
                                //     OrderID: r.OrderID
                                // }
                                // let TripData = await Trips.findOne(q).lean().exec();
                                Data = {
                                    Status: "Order In Transit",
                                    Description: "Your order has been picked up and is on the way for delivery ETA: approximately " + item.Total_Eta.toFixed(2) + " Min",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Order Delivered") {
                                Data = {
                                    Status: "Order Completed",
                                    Description: "Your order has been delivered successfully, Received by: " + r.Delivered_To + ", Ordered by: " + r.Delivery_Address_Information.Name,
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            }
                        }
                        item.Order_Report = OrderReport
                        item.Total_Eta = item.Total_Eta.toFixed(2)
                        item.Total_Duration = await CommonController.Common_Time_String_From_MS(item.Total_Duration);
                        let qb = {
                            BuyerID: r.BuyerID
                        };
                        let rb = await Buyers.findOne(qb).select('BuyerID Buyer_Name Buyer_Email Buyer_CountryCode Buyer_PhoneNumber ').lean().exec();
                        item.Order_Placed_By = rb
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    // console.log(Result)
                    resolve({ success: true, extras: { Count: Count, Data: Result } })
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

DriverController.Report_Trip = (values, TripData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Driver_Comments = {
                    CommentID: uuid.v4(),
                    DriverID: DriverData.DriverID,
                    Driver_Name: DriverData.Driver_Name,
                    Driver_Country_Code: DriverData.Driver_Country_Code,
                    Driver_Phone_Number: DriverData.Driver_Phone_Number,
                    Comment: values.Comment,
                    Time: new Date()
                };
                let query = {
                    TripID: values.TripID
                };
                let changes = {
                    $set: {
                        Whether_Reported_Trip: true,
                        Total_Trip_Status: 4
                    },
                    $push: {
                        Driver_Comments: Driver_Comments
                    }
                };
                let UpdatedStatus = await Trips.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Trip Reported Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

DriverController.Order_Amount_Distrubtion_Processing = (values, TripData, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // Amount to Driver
                let Delivery_Amount = parseFloat(OrderData.Order_Invoice.Order_Delivery_Charge);

                //Deduct from company and add to Branch
                let Discount_Share_Amount = parseFloat(OrderData.Order_Invoice.Order_Other_Discount);

                //Amount to Branch
                let Branch_Share_Amount = parseFloat(OrderData.Order_Invoice.Order_Total_Branch_Share_Amount);

                let Fianl_Branch_Amount = parseFloat(
                    Branch_Share_Amount + Discount_Share_Amount
                )

                //Amount to Admin/Company
                let Admin_Share_Amount = parseFloat(OrderData.Order_Invoice.Order_Total_Admin_Share_Amount);
                let Order_Package_Charge = parseFloat(OrderData.Order_Invoice.Order_Package_Charge);
                let Order_Surge_Charge = parseFloat(OrderData.Order_Invoice.Order_Surge_Charge);
                let Previous_Order_Cancellation_Charge = parseFloat(OrderData.Order_Invoice.Previous_Order_Cancellation_Charge);
                let Order_Taxes = parseFloat(OrderData.Order_Invoice.Order_Taxes);

                let Final_Admin_Amount = parseFloat(
                    Admin_Share_Amount
                    + Order_Package_Charge
                    + Order_Surge_Charge
                    + Order_Taxes
                    + Previous_Order_Cancellation_Charge
                    - Discount_Share_Amount
                )

                if (OrderData.Payment_Type != 4) {
                    // Add Driver Online Amount Complete
                    let Dquery = {
                        DriverID: values.DriverID
                    }
                    let Dchanges = {
                        $inc: {
                            "Driver_Wallet_Information.Available_Amount": Delivery_Amount,
                            "Driver_Wallet_Information.Total_Collected_Amount": Delivery_Amount,
                            "Driver_Wallet_Information.Total_Earned_Amount": Delivery_Amount
                        },
                        $set: {
                            updated_at: new Date()
                        }
                    }
                    let SaveAmount = await Drivers.updateOne(Dquery, Dchanges).lean().exec();
                    let L1Data = {
                        LogID: uuid.v4(),
                        DriverID: values.DriverID,
                        Type: 7, // Amount Credited for Completing Order
                        Amount: Delivery_Amount,
                        Data: {
                            TripID: TripData.TripID,
                            TripNumber: TripData.TripNumber,
                            OrderData: OrderData,
                        },
                        Time: new Date()
                    };
                    let L1SaveResult = await Driver_Wallet_Logs.create(L1Data);

                    ////////////

                    ///Add Branch Online Amount 

                    let Bquery = {
                        BranchID: TripData.BranchID
                    }
                    let Bchanges = {
                        $inc: {
                            "Branch_Wallet_Information.Available_Amount": Fianl_Branch_Amount,
                            "Branch_Wallet_Information.Total_Amount": Fianl_Branch_Amount
                        },
                        $set: {
                            updated_at: new Date()
                        }
                    }
                    let SaveAmountB = await Store_Branch.updateOne(Bquery, Bchanges).lean().exec();
                    let L2Data = {
                        LogID: uuid.v4(),
                        BranchID: TripData.BranchID,
                        Type: 1, // Amount credited to wallet for order
                        Amount: Fianl_Branch_Amount,
                        Data: {
                            TripID: TripData.TripID,
                            TripNumber: TripData.TripNumber,
                            Order_Number: OrderData.Order_Number,
                            OrderID: OrderData.OrderID,
                            Amount: Fianl_Branch_Amount,
                        },
                        Time: new Date()
                    };
                    let L2SaveResult = await Store_Branch_Wallet_Logs.create(L2Data);
                    ////////////

                    ///Add Online Amount to Company(Admin)
                    let WData = {
                        LogID: uuid.v4(),
                        AdminID: "",
                        Type: 4, // amount Credited for Order 
                        Amount: Final_Admin_Amount,
                        Data: {
                            TripID: TripData.TripID,
                            TripNumber: TripData.TripNumber,
                            Order_Number: OrderData.Order_Number,
                            OrderID: OrderData.OrderID,
                            Admin_Share_Amount: Admin_Share_Amount,
                            Order_Package_Charge: Order_Package_Charge,
                            Order_Surge_Charge: Order_Surge_Charge,
                            Previous_Order_Cancellation_Charge: Previous_Order_Cancellation_Charge,
                            Discount: Discount_Share_Amount,
                            Final_Admin_Amount: Final_Admin_Amount,
                            Calculation: "Final_Admin_Amount = Admin_Share_Amount + Order_Package_Charge + Order_Surge_Charge + Previous_Order_Cancellation_Charge - Discount_Share_Amount"
                        },
                        Time: new Date()
                    }
                    let Resultlog = Company_Wallet_Log(WData).save();

                    let Aquery = {

                    };
                    let Achanges = {
                        $inc: {
                            Available_Amount: Final_Admin_Amount,
                            Total_Amount: Final_Admin_Amount
                        }
                    };
                    let Cfndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let CfindupdateData = await Company_Wallet.findOneAndUpdate(Aquery, Achanges, Cfndupdoptions).select('-_id -__v').lean();
                } else {
                    // process for COD
                    let Branch_Share_Data = {
                        BranchID: OrderData.Branch_Information.BranchID,
                        StoreID: OrderData.Branch_Information.StoreID,
                        Branch_Name: OrderData.Branch_Information.Branch_Name,
                        OrderID: OrderData.OrderID,
                        Branch_Amount: Fianl_Branch_Amount
                    }
                    let queryDriverCod = {
                        DriverID: values.DriverID
                    }
                    let fndupdchanges = {
                        $setOnInsert: {
                            DriverID: values.DriverID,
                            created_at: new Date(),
                        },
                        $set: {
                            Whether_Amount_Submited: false,
                            Status: true,
                            updated_at: new Date()
                        },
                        $inc: {
                            Amount_Collected: OrderData.Order_Invoice.Order_Total_Final_Price,
                            Delivery_Amount: Delivery_Amount,
                            Branch_Amount: Fianl_Branch_Amount,
                            Admin_Amount: Final_Admin_Amount
                        },
                        $push: {
                            Branch_Data: Branch_Share_Data
                        }
                    };
                    let fndupdoptions = {
                        upsert: true,
                        setDefaultsOnInsert: true,
                        new: true
                    }
                    let SaveData = await Driver_COD_Collected.findOneAndUpdate(queryDriverCod, fndupdchanges, fndupdoptions).lean().exec();
                }
                resolve("Process Completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Complete_Drop_Type4 = (values, TripData, TripRouteData, OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 4;
                let Route_Status_Comment = "Drop Unloaded";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    OrderID: TripData.OrderID,
                    Order_Status: 11,
                    Cancellation_Pickup_Status: 8
                };
                let Result = await Buyer_Orders.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CANCEL_ORDER_STATUS } });
                } else {
                    let queryx = {
                        TripRouteID: TripRouteData.TripRouteID
                    };
                    let changes = {
                        $set: {
                            Route_Status: Route_Status,
                            Route_Status_Comment: Route_Status_Comment,
                            Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                            updated_at: new Date()
                        },
                        $push: {
                            Route_Status_Logs: Route_Status_Logs
                        }
                    };
                    let UpdatedStatus = await Trips_Orders_Routings.updateOne(queryx, changes).lean();
                    let Cancellation_Pickup_Status = 9; //completed
                    let Cancellation_Order_Pickup_Report = {
                        Title: "Order Delivered",
                        Description: "Order Delivered to " + values.Delivered_To,
                        Delivered_To: values.Delivered_To,
                        Time: new Date()
                    };
                    let OrderStatusesUpdate = DriverController.Common_Update_Return_Order_Driver_Trip_Statuses(Cancellation_Pickup_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Cancellation_Order_Pickup_Report);
                    let tquery = {
                        TripID: values.TripID
                    }
                    let TripChanges = {
                        Total_Trip_Status: 3,
                        updated_at: new Date()
                    }
                    let UpdateData = await Trips.updateOne(tquery, TripChanges).lean().exec();
                    resolve({ success: true, extras: { Status: "Validated Successfully" } });
                    let SendNotifications = await FcmController.Order_Delivered_Notification(Result, DriverData)
                    Route_Status = 5;
                    Route_Status_Comment = "Drop Unloaded";
                    Route_Status_Logs = {
                        LogID: uuid.v4(),
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        Time: new Date()
                    };
                    let trquery = {
                        TripRouteID: TripRouteData.TripRouteID
                    };
                    let trchanges = {
                        $set: {
                            Route_Status: Route_Status,
                            Route_Status_Comment: Route_Status_Comment,
                            Route_Action_Completed: TripRouteData.Route_Action_Next,
                            Route_Action_Completed_Time: new Date(),
                            Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                            updated_at: new Date()
                        },
                        $push: {
                            Route_Status_Logs: Route_Status_Logs
                        }
                    };
                    let trUpdatedStatus = await Trips_Orders_Routings.updateOne(trquery, trchanges).lean();
                    //Amount Process-->
                    let ProcessAmount = await DriverController.Order_Amount_Distrubtion_Processing(values, TripData, OrderData)
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Complete_Pickup_Type3 = (DriverData, TripData, TripRouteData, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 4;
                let Route_Status_Comment = "Pickup Loaded";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let changes = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let UpdatedStatus = await Trips_Orders_Routings.updateOne(query, changes).lean();
                // resolve("Order Picked Successfully");
                //let SelectedOrderProcessing = await StoreController.Trip_Route_Pickup_Barcode_Processed_Complete_Functionality_Selected_Order_Processing(DriverData, TripData, TripRouteData, All_Orders_ID_Array);
                //let UnSelectedOrderProcessing = await DriverController.Trip_Route_Pickup_Barcode_Processed_Complete_Functionality_Un_Selected_Order_Processing(DriverData, TripData, TripRouteData, Un_Selected_Orders_ID_Array, 1);
                // async.eachSeries(All_Orders_ID_Array, async (item, callback) => {
                //     try {
                //         let oquery = {
                //             OrderID: item
                //         };
                //         let ochanges = {
                //             $set: {
                //                 Whether_Barcode_Number_Available: true,
                //                 Barcode_Number: item.Barcode_Number,
                //                 updated_at: new Date()
                //             }
                //         };
                //         let oUpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                //         callback();
                //     } catch (error) {
                //         callback(error);
                //     }
                // }, async (err) => {
                //if (err) reject(err);
                Route_Status = 5;
                Route_Status_Comment = "Pickup Loaded";
                Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let trquery = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let trchanges = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        Route_Action_Completed: TripRouteData.Route_Action_Next,
                        Route_Action_Completed_Time: new Date(),
                        Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let trUpdatedStatus = await Trips_Orders_Routings.updateOne(trquery, trchanges).lean();
                //let AmountCollection = await DriverController.Trip_Route_Pickup_Barcode_Processed_Continue_Store_Collection_Amount(DriverData, TripData, TripRouteData);
                // });
                let Cancellation_Pickup_Status = 6;
                let Cancellation_Order_Pickup_Report = {
                    Title: "Order Picked",
                    Description: "Order Picked",
                    Time: new Date()
                };
                let OrderStatusesUpdate = DriverController.Common_Update_Return_Order_Driver_Trip_Statuses(Cancellation_Pickup_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Cancellation_Order_Pickup_Report);
                resolve({ success: true, extras: { Status: "Order Pickedup Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


DriverController.Validate_Drop_OTP_At_Buyer = (values, TripData, TripRouteData, OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 4;
                let Route_Status_Comment = "Drop Unloaded";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    OrderID: TripData.OrderID,
                    Order_Status: 10,
                };
                let Result = await Buyer_Orders.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER_STATUS } });
                } else {
                    query.OTP = values.OTP;
                    let Resultx = await Buyer_Orders.findOne(query).lean();
                    if (Resultx == null) {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                    } else {
                        let queryx = {
                            TripRouteID: TripRouteData.TripRouteID
                        };
                        let changes = {
                            $set: {
                                Route_Status: Route_Status,
                                Route_Status_Comment: Route_Status_Comment,
                                Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                                updated_at: new Date()
                            },
                            $push: {
                                Route_Status_Logs: Route_Status_Logs
                            }
                        };
                        let UpdatedStatus = await Trips_Orders_Routings.updateOne(queryx, changes).lean();
                        let Order_Status = 3; //completed
                        let Order_Report = {
                            Title: "Order Delivered",
                            Description: "Order Delivered to " + values.Delivered_To,
                            Delivered_To: values.Delivered_To,
                            Time: new Date()
                        };
                        let OrderStatusesUpdate = DriverController.Common_Update_Order_Driver_Trip_Statuses(Order_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Order_Report);
                        let tquery = {
                            TripID: values.TripID
                        }
                        let TripChanges = {
                            Total_Trip_Status: 3,
                            updated_at: new Date()
                        }
                        let UpdateData = await Trips.updateOne(tquery, TripChanges).lean().exec();
                        resolve({ success: true, extras: { Status: "Validated Successfully" } });
                        let SendNotifications = await FcmController.Order_Delivered_Notification(Result, DriverData)
                        Route_Status = 5;
                        Route_Status_Comment = "Drop Unloaded";
                        Route_Status_Logs = {
                            LogID: uuid.v4(),
                            Route_Status: Route_Status,
                            Route_Status_Comment: Route_Status_Comment,
                            Time: new Date()
                        };
                        let trquery = {
                            TripRouteID: TripRouteData.TripRouteID
                        };
                        let trchanges = {
                            $set: {
                                Route_Status: Route_Status,
                                Route_Status_Comment: Route_Status_Comment,
                                Route_Action_Completed: TripRouteData.Route_Action_Next,
                                Route_Action_Completed_Time: new Date(),
                                Selected_Orders_ID_Array: TripRouteData.All_Orders_ID_Array,
                                updated_at: new Date()
                            },
                            $push: {
                                Route_Status_Logs: Route_Status_Logs
                            }
                        };
                        let trUpdatedStatus = await Trips_Orders_Routings.updateOne(trquery, trchanges).lean();
                        //Amount Process-->
                        let ProcessAmount = await DriverController.Order_Amount_Distrubtion_Processing(values, TripData, OrderData)
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Trip_Pickup_Drop_Reached = (values, TripRouteData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 3;
                let Route_Status_Comment = "Reached Location";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let changes = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let UpdatedStatus = await Trips_Orders_Routings.updateOne(query, changes).lean();
                let TQuery = {
                    TripID: TripRouteData.TripID
                }
                let TripData = await Trips.findOne(TQuery).lean().exec();
                let Order_Status;
                let Order_Report;
                let Cancellation_Pickup_Status;
                let Cancellation_Order_Pickup_Report = {};
                
                if (TripRouteData.Route_Action_Next == 1 || TripRouteData.Route_Action_Next == 3) {
                    // let TQuery = {
                    //     TripID: TripRouteData.TripID
                    // }
                    // let TripData = await Trips.findOne(TQuery).lean().exec();
                    // console.log(TripData)
                    let OQuery = {
                        OrderID: TripData.OrderID
                    }
                    let result = await Buyer_Orders.findOne(OQuery).lean().exec();
                    // console.log(result)
                    let Data = {
                        OTPID: uuid.v4(),
                        TripID: TripData.TripID,
                        TripRouteID: values.TripRouteID,
                        OrderID: TripData.OrderID,
                        DriverID: DriverData.DriverID,
                        Driver_Country_Code: DriverData.Driver_Country_Code,
                        Driver_Phone_Number: DriverData.Driver_Phone_Number,
                        BranchID: TripData.BranchID,
                        Branch_Name: TripData.Branch_Name,
                        Branch_PhoneNumber: TripData.Branch_PhoneNumber,
                        OTP: await CommonController.Random_OTP_Number(),
                        Latest: true,
                        Time: new Date()
                    }
                    let SaveData = await Driver_Order_OTP(Data).save();
                    for (const CartItem of result.Cart_Information) {
                        delete CartItem.Avg_Rating
                        delete CartItem.Total_Rating_Count
                        delete CartItem.Price
                        delete CartItem.Discount_Percent
                        delete CartItem.Discount_Price
                        delete CartItem.Final_Price
                        delete CartItem.Admin_Share_Percent
                        delete CartItem.Admin_Share_Amount
                        delete CartItem.Branch_Share_Amount
                        delete CartItem.Total_Price
                        delete CartItem.Total_Discount_Price
                        delete CartItem.Total_Final_Price
                        delete CartItem.Total_Admin_Share_Amount
                        delete CartItem.Total_Branch_Share_Amount
                    }
                    Data.Cart_Information = result.Cart_Information;
                    // console.log(898)
                    // console.log(result.Order_Status)
                    if (result.Order_Status == 11) {
                        // console.log(11)
                        Cancellation_Pickup_Status = 5;
                        Cancellation_Order_Pickup_Report = {
                            Title: "Reached at pickup",
                            Description: "Reached at pickup",
                            Time: new Date()
                        }
                        let OrderStatusesUpdate = DriverController.Common_Update_Return_Order_Driver_Trip_Statuses(Cancellation_Pickup_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Cancellation_Order_Pickup_Report);
                        resolve({ success: true, extras: { Status: "Reached Successfully", Type: 3, Data: Data } });
                    } else {
                        // console.log(10)
                        Order_Status = 7;
                        Order_Report = {
                            Title: "Reached at pickup",
                            Description: "Reached at pickup",
                            Time: new Date()
                        }
                        let OrderStatusesUpdate = DriverController.Common_Update_Order_Driver_Trip_Statuses(Order_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Order_Report);
                        resolve({ success: true, extras: { Status: "Reached Successfully", Type: 1, Data: Data } });
                    }
                } else if (TripRouteData.Route_Action_Next == 2 || TripRouteData.Route_Action_Next == 4) {
                    // let TQuery = {
                    //     TripID: TripRouteData.TripID
                    // }
                    // let TripData = await Trips.findOne(TQuery).lean().exec();
                    let Data = {}
                    let OQuery = {
                        OrderID: TripData.OrderID
                    }
                    let result = await Buyer_Orders.findOne(OQuery).lean().exec();
                    Data.OrderID = result.OrderID
                    Data.Order_Number = result.Order_Number
                    Data.Branch_Information = result.Branch_Information
                    for (const CartItem of result.Cart_Information) {
                        delete CartItem.Avg_Rating
                        delete CartItem.Total_Rating_Count
                        delete CartItem.Price
                        delete CartItem.Discount_Percent
                        delete CartItem.Discount_Price
                        delete CartItem.Final_Price
                        delete CartItem.Admin_Share_Percent
                        delete CartItem.Admin_Share_Amount
                        delete CartItem.Branch_Share_Amount
                        delete CartItem.Total_Price
                        delete CartItem.Total_Discount_Price
                        delete CartItem.Total_Final_Price
                        delete CartItem.Total_Admin_Share_Amount
                        delete CartItem.Total_Branch_Share_Amount
                    }
                    Data.Cart_Information = result.Cart_Information;
                    // console.log(947)
                    // console.log(result.Order_Status)
                    if (result.Order_Status == 11) {
                        // console.log(11)
                        Data.Address_Information = result.Branch_Information
                        Data.Amount_to_Collect = 0
                        Cancellation_Pickup_Status = 8;
                        Cancellation_Order_Pickup_Report = {
                            Title: "Reach at drop",
                            Description: "Driver at your location",
                            Time: new Date()
                        }
                        let OrderStatusesUpdate = DriverController.Common_Update_Return_Order_Driver_Trip_Statuses(Cancellation_Pickup_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Cancellation_Order_Pickup_Report);
                        resolve({ success: true, extras: { Status: "Reached Successfully", Type: 4, Data: Data } });

                    } else {
                        // console.log(10)
                        Data.Address_Information = result.Delivery_Address_Information
                        if (result.Payment_Type == 4) {
                            Data.Amount_to_Collect = result.Amount_COD
                        } else {
                            Data.Amount_to_Collect = 0
                        }
                        Order_Status = 10;
                        Order_Report = {
                            Title: "Reach at drop",
                            Description: "Driver at your location",
                            Time: new Date()
                        }
                        let OrderStatusesUpdate = DriverController.Common_Update_Order_Driver_Trip_Statuses(Order_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Order_Report);
                        resolve({ success: true, extras: { Status: "Reached Successfully", Type: 2, Data: Data } });
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER_STATUS } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Common_Update_Order_Driver_Trip_Statuses = (Order_Status, All_Orders_ID_Array, TripRouteData, DriverData, Order_Report) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(DriverData.Location.Latitude);
                let Longitude = parseFloat(DriverData.Location.Longitude);
                if (DriverData.Driver_Current_Location.Latitude != 0 && DriverData.Driver_Current_Location.Longitude != 0) {
                    Latitude = parseFloat(DriverData.Driver_Current_Location.Latitude);
                    Longitude = parseFloat(DriverData.Driver_Current_Location.Longitude);
                };
                let Address = '';
                if (Order_Status != 6 && Order_Status != 9) {
                    Address = TripRouteData.Address;
                }
                let Driver_Logs = {
                    LogID: uuid.v4(),
                    TripID: TripRouteData.TripID,
                    DriverID: DriverData.DriverID,
                    Order_Status: Order_Status,
                    Time: new Date()
                };
                let Event_Logs = {
                    LogID: uuid.v4(),
                    DriverID: DriverData.DriverID,
                    Order_Status: Order_Status,
                    Address: Address,
                    lat: Latitude,
                    lng: Longitude,
                    Time: new Date()
                };
                for (let OrderID of All_Orders_ID_Array) {

                    let oquery = {
                        OrderID: OrderID,
                        Order_Status: {
                            $ne: Order_Status
                        },
                        Status: true
                    };
                    let OData = await Buyer_Orders.findOne(oquery).lean().exec();
                    let Whether_Amount_COD_Received = false
                    if (OData.Payment_Status == 4) {
                        Whether_Amount_COD_Received = true
                    }
                    let ochanges = {}
                    if (Order_Status != 3) {
                        ochanges = {
                            $set: {
                                Order_Status: Order_Status,
                                updated_at: new Date()
                            },
                            $push: {
                                Driver_Logs: Driver_Logs,
                                Event_Logs: Event_Logs,
                                Order_Report: Order_Report,
                            }
                        }
                    } else {
                        ochanges = {
                            $set: {
                                Order_Status: Order_Status,
                                updated_at: new Date(),
                                Delivered_To: Order_Report.Delivered_To,
                                Whether_Amount_COD_Received: Whether_Amount_COD_Received
                            },
                            $push: {
                                Driver_Logs: Driver_Logs,
                                Event_Logs: Event_Logs,
                                Order_Report: Order_Report,
                            }
                        }
                    }
                    let sUpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                }
                resolve("Updated Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Common_Update_Return_Order_Driver_Trip_Statuses = (Cancellation_Pickup_Status, All_Orders_ID_Array, TripRouteData, DriverDatax, Cancellation_Order_Pickup_Report) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DriverData = await CommonController.Check_Only_Driver(DriverDatax)
                let Latitude = parseFloat(DriverData.Location.Latitude);
                let Longitude = parseFloat(DriverData.Location.Longitude);
                if (DriverData.Driver_Current_Location.Latitude != 0 && DriverData.Driver_Current_Location.Longitude != 0) {
                    Latitude = parseFloat(DriverData.Driver_Current_Location.Latitude);
                    Longitude = parseFloat(DriverData.Driver_Current_Location.Longitude);
                };
                let Address = '';
                if (Cancellation_Pickup_Status != 4 && Cancellation_Pickup_Status != 7) {
                    Address = TripRouteData.Address;
                }
                let Return_Driver_Logs = {
                    LogID: uuid.v4(),
                    TripID: TripRouteData.TripID,
                    DriverID: DriverData.DriverID,
                    Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                    Time: new Date()
                };
                let Return_Event_Logs = {
                    LogID: uuid.v4(),
                    DriverID: DriverData.DriverID,
                    Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                    Address: Address,
                    lat: Latitude,
                    lng: Longitude,
                    Time: new Date()
                };
                let oquery = {
                    OrderID: {
                        $in: All_Orders_ID_Array
                    },
                    Cancellation_Pickup_Status: {
                        $ne: Cancellation_Pickup_Status
                    },
                    Status: true
                };
                let ochanges = {}
                if (Cancellation_Pickup_Status != 7) {
                    ochanges = {
                        $set: {
                            Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                            updated_at: new Date()
                        },
                        $push: {
                            Return_Driver_Logs: Return_Driver_Logs,
                            Return_Event_Logs: Return_Event_Logs,
                            Cancellation_Order_Pickup_Report: Cancellation_Order_Pickup_Report,
                        }
                    }
                } else {
                    ochanges = {
                        $set: {
                            Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                            updated_at: new Date(),
                            Delivered_To: Cancellation_Order_Pickup_Report.Delivered_To,
                            // Whether_Amount_COD_Received: true
                        },
                        $push: {
                            Return_Driver_Logs: Return_Driver_Logs,
                            Return_Event_Logs: Return_Event_Logs,
                            Cancellation_Order_Pickup_Report: Cancellation_Order_Pickup_Report,
                        }
                    }
                }
                let sUpdatedStatus = await Buyer_Orders.updateMany(oquery, ochanges).lean();
                resolve("Updated Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Trip_Pickup_Drop_Initiated = (values, TripData, TripRouteData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Route_Status = 2;
                let Route_Status_Comment = "Started for Location";
                let Route_Status_Logs = {
                    LogID: uuid.v4(),
                    Route_Status: Route_Status,
                    Route_Status_Comment: Route_Status_Comment,
                    Time: new Date()
                };
                let query = {
                    TripRouteID: TripRouteData.TripRouteID
                };
                let changes = {
                    $set: {
                        Route_Status: Route_Status,
                        Route_Status_Comment: Route_Status_Comment,
                        updated_at: new Date()
                    },
                    $push: {
                        Route_Status_Logs: Route_Status_Logs
                    }
                };
                let UpdatedStatus = await Trips_Orders_Routings.updateOne(query, changes).lean();
                // console.log(TripData.OrderID)
                let queryOrder = {
                    OrderID: TripData.OrderID
                }
                let OrderData = await Buyer_Orders.findOne(queryOrder).lean().exec();
                if (OrderData.Order_Status == 11) {
                    let Cancellation_Pickup_Status = 4;
                    let Cancellation_Order_Pickup_Report = {};
                    let Order_Report;
                    if (TripRouteData.Route_Action_Next == 1 || TripRouteData.Route_Action_Next == 3) {
                        Cancellation_Pickup_Status = 4;
                        Cancellation_Order_Pickup_Report = {
                            Title: "Pickup Started",
                            Description: "Pickup Started",
                            Time: new Date()
                        }
                    } else if (TripRouteData.Route_Action_Next == 2 || TripRouteData.Route_Action_Next == 4) {
                        Cancellation_Pickup_Status = 7;
                        Cancellation_Order_Pickup_Report = {
                            Title: "Drop Stated",
                            Description: "Driver on his way",
                            Time: new Date()
                        }
                    } else {
                        Cancellation_Pickup_Status = 4;
                        Cancellation_Order_Pickup_Report = {
                            Title: "Pickup Started",
                            Description: "Pickup Started",
                            Time: new Date()
                        }
                    }
                    let OrderStatusesUpdate = DriverController.Common_Update_Return_Order_Driver_Trip_Statuses(Cancellation_Pickup_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Cancellation_Order_Pickup_Report);
                } else {
                    let Order_Status = 6;
                    let Order_Report;
                    if (TripRouteData.Route_Action_Next == 1 || TripRouteData.Route_Action_Next == 3) {
                        Order_Status = 6;
                        Order_Report = {
                            Title: "Pickup Started",
                            Description: "Pickup Started",
                            Time: new Date()
                        }
                    } else if (TripRouteData.Route_Action_Next == 2 || TripRouteData.Route_Action_Next == 4) {
                        Order_Status = 9;
                        Order_Report = {
                            Title: "Drop Stated",
                            Description: "Driver on his way",
                            Time: new Date()
                        }
                    } else {
                        Order_Status = 6;
                        Order_Report = {
                            Title: "Pickup Started",
                            Description: "Pickup Started",
                            Time: new Date()
                        }
                    }
                    let OrderStatusesUpdate = DriverController.Common_Update_Order_Driver_Trip_Statuses(Order_Status, TripRouteData.All_Orders_ID_Array, TripRouteData, DriverData, Order_Report);
                }
                resolve({ success: true, extras: { Status: "Initiated Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Common_Check_Whether_Trip_Started = (TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (TripData.Total_Trip_Status == 1) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_NOT_STARTED } });
                } else if (TripData.Total_Trip_Status == 2) {
                    resolve("Validated Successfully");
                } else if (TripData.Total_Trip_Status == 3) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_COMPLETED } });
                } else if (TripData.Total_Trip_Status == 4) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_REPORTED } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Fetch_Total_Driver_Collected_Amount_Between_Dates = (DriverID, Start_Date, End_Date) => {
    return new Promise(async (resolve, reject) => {
        try {
            let time_options = {
                $gte: Start_Date,
                $lte: End_Date
            };
            let Total_Amount = 0;
            let query = {
                DriverID: DriverID,
                DriverDate: time_options,
                Status: true
            };
            let gmapping = {
                "_id": "Total_Amount",
                "Total_Amount": {
                    $sum: "$Complete_Amount_Information.Total_Collected_Amount"
                }
            };
            let Result = await Driver_Salary_Records.aggregate().match(query).group(gmapping);
            Result = await JSON.parse(JSON.stringify(Result));
            if (Result.length > 0) {
                Total_Amount += Result[0].Total_Amount;
                resolve(Total_Amount);
            } else {
                resolve(Total_Amount);
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
};

DriverController.Fetch_Total_Driver_Earned_Amount_Between_Dates = (DriverID, Start_Date, End_Date) => {
    return new Promise(async (resolve, reject) => {
        try {
            let time_options = {
                $gte: Start_Date,
                $lte: End_Date
            };
            let Total_Amount = 0;
            let query = {
                DriverDate: time_options,
                DriverID: DriverID,
                Status: true
            };
            let gmapping = {
                "_id": "Total_Amount",
                "Total_Amount": {
                    $sum: "$Complete_Amount_Information.Total_Earned_Amount"
                }
            };
            let Result = await Driver_Salary_Records.aggregate().match(query).group(gmapping);
            Result = await JSON.parse(JSON.stringify(Result));
            if (Result.length > 0) {
                Total_Amount += Result[0].Total_Amount;
                resolve(Total_Amount);
            } else {
                resolve(Total_Amount);
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Driver_Complete_Amount_Information_Between_Dates = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Start_Date = moment(values.Start_Date, config.Take_Date_Format).toDate();
            let End_Start_Date = moment(values.End_Date, config.Take_Date_Format).toDate();
            let End_Date = moment(values.End_Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms').toDate();
            let DriverID = values.DriverID;
            let Total_Earned_Amount = await DriverController.Fetch_Total_Driver_Earned_Amount_Between_Dates(DriverID, Start_Date, End_Date);
            let Total_Collected_Amount = await DriverController.Fetch_Total_Driver_Collected_Amount_Between_Dates(DriverID, Start_Date, End_Date);
            let Last_Day_Balance_Amount = await DriverController.Fetch_Total_Driver_Balance_Between_Dates(DriverID, End_Start_Date, End_Date);
            Total_Earned_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Earned_Amount);
            Total_Collected_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Collected_Amount);
            Last_Day_Balance_Amount = await CommonController.Common_Floating_Beautify_Value(Last_Day_Balance_Amount);
            let Data = {
                Total_Earned_Amount: Total_Earned_Amount,
                Total_Collected_Amount: Total_Collected_Amount,
                Last_Day_Balance_Amount: Last_Day_Balance_Amount
            };
            resolve({ success: true, extras: { Data: Data } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Driver_Add_Money_Wallet = (PaymentRequestData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let DriverID = PaymentRequestData.DriverID;
            let Amount = PaymentRequestData.Amount;
            let Razorpay_Processing_Fee_Amount = PaymentRequestData.Razorpay_Processing_Fee_Amount;
            let Total_Amount = PaymentRequestData.Total_Amount;

            let L1Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 1, // Amount Credit to Wallet towards razorpay tx
                Amount: Amount,
                Data: PaymentRequestData,
                Time: new Date()
            };
            let L1SaveResult = await Driver_Wallet_Logs.create(L1Data);
            let L2Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 2,  // Razorpay Processing Fee Amount
                Amount: Razorpay_Processing_Fee_Amount,
                Data: PaymentRequestData,
                Time: new Date()
            };
            let L2SaveResult = await Driver_Wallet_Logs.create(L2Data);
            let fndupdquery = {
                DriverID: DriverID
            };
            let fndupdchanges = {
                $set: {
                    updated_at: new Date()
                },
                $inc: {
                    "Driver_Wallet_Information.Available_Amount": Amount,
                    "Driver_Wallet_Information.Total_Wallet_Added_Online_Amount": Amount,
                    "Driver_Wallet_Information.Total_Wallet_Added_Online_Processing_Fee_Amount": Razorpay_Processing_Fee_Amount,
                }
            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let findupdateData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve("Amount Added to Wallet");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Razorpay_Add_Money_Wallet_Processing = (PaymentData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (PaymentData.status == "authorized") {
                    // console.log(187)
                    let NewPaymentData = await RazorpayController.Capture_Razorpay_Payment(PaymentData.id, PaymentData.amount);
                }
                if (PaymentData.notes.Driver_Add_Money_RequestID != null && PaymentData.notes.Driver_Add_Money_RequestID != undefined && PaymentData.notes.Driver_Add_Money_RequestID != '' && PaymentData.notes.Driver_Add_Money_RequestID != ' ') {
                    let PaymentRequestData = await Driver_Add_Money_Requests.findOne({ Driver_Add_Money_RequestID: PaymentData.notes.Driver_Add_Money_RequestID }).lean();
                    if (PaymentRequestData == null) {
                        resolve("Functionality Completed");
                    } else {
                        if (PaymentData.status == 'authorized') {
                            // PaymentData = await RazorpayController.Capture_Razorpay_Payment(PaymentData.id, PaymentData.amount);
                            PaymentData.status = 'captured';
                            let uquery = {
                                Driver_Add_Money_RequestID: PaymentRequestData.Driver_Add_Money_RequestID
                            }
                            let uchanges = {
                                $set: {
                                    Whether_Payment_Done: true,
                                    Payment_Time: new Date(),
                                    PaymentID: PaymentData.id,
                                    PaymentData: PaymentData,
                                    updated_at: new Date()
                                }
                            };
                            let uoptions = {
                                upsert: true,
                                setDefaultsOnInsert: true,
                                new: true
                            };
                            PaymentRequestData = await Driver_Add_Money_Requests.findOneAndUpdate(uquery, uchanges, uoptions).lean();
                            let WalletProcessing = await DriverController.Driver_Add_Money_Wallet(PaymentRequestData);
                            resolve("Functionality Completed");
                        } else if (PaymentData.status == 'captured') {
                            resolve("Captured Completed");
                        } else if (PaymentData.status == 'failed') {
                            //Do nothing
                            resolve("Functionality Completed");
                        }
                    }
                } else {
                    resolve("Functionality Completed");
                }
            } catch (error) {
                console.error("some error-->", error);
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driver_Fetch_Complete_Add_Money_Total_Amount_Information = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let SettingData = await CommonController.Fetch_App_Driver_Settings();
            let Amount = parseFloat(values.Amount);
            let Razorpay_Processing_Fee_Percentage = parseFloat(SettingData.Razorpay_Processing_Fee_Percentage);
            let Razorpay_Processing_Fee_Amount = ((Amount * Razorpay_Processing_Fee_Percentage) / 100);
            let Total_Amount = Amount + Razorpay_Processing_Fee_Amount;
            Amount = await CommonController.Common_Floating_Beautify_Value(Amount);
            Razorpay_Processing_Fee_Percentage = await CommonController.Common_Floating_Beautify_Value(Razorpay_Processing_Fee_Percentage);
            Razorpay_Processing_Fee_Amount = await CommonController.Common_Floating_Beautify_Value(Razorpay_Processing_Fee_Amount);
            Total_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Amount);
            let sData = {
                Driver_Add_Money_RequestID: uuid.v4(),
                DriverID: values.DriverID,
                Amount: Amount,
                Razorpay_Processing_Fee_Percentage: Razorpay_Processing_Fee_Percentage,
                Razorpay_Processing_Fee_Amount: Razorpay_Processing_Fee_Amount,
                Total_Amount: Total_Amount,
                Whether_Request: true,
                Request_Time: new Date(),
                created_at: new Date(),
                updated_at: new Date()
            };
            let Data = await Driver_Add_Money_Requests.create(sData);
            resolve({ success: true, extras: { Status: "Requested Successfully", Data: Data } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Razorpay_Reversal_Amount_Transafer_Failed = (TransactionData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let DriverID = TransactionData.DriverID;
            let Amount = TransactionData.Amount;
            let Service_Amount = TransactionData.Service_Amount;
            let Total_Amount = TransactionData.Total_Amount;
            let L1Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 5, // Amount Revesaral for Failed Tranction
                Amount: Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L1SaveResult = await Driver_Wallet_Logs.create(L1Data);
            let L2Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 6, // Service Amount Revesaral for Failed Tranction
                Amount: Service_Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L2SaveResult = await Driver_Wallet_Logs.create(L2Data);
            let fndupdquery = {
                DriverID: DriverID
            };
            let fndupdchanges = {
                $set: {
                    updated_at: new Date()
                },
                $inc: {
                    "Driver_Wallet_Information.Available_Amount": Total_Amount,
                    "Driver_Wallet_Information.Total_Withdrawn_Amount": (Amount * -1),
                    "Driver_Wallet_Information.Total_Withdrawn_Service_Tax_Amount": (Service_Amount * -1),
                }
            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let findupdateData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve("Wallet Functionality Completed");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Common_Razorpay_Driver_Withdrwal_Functionality = (PayoutData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let TransactionData = await Driver_Wallet_Withdraw_Transactions.findOne({ RazorpayX_TransactionID: PayoutData.id }).lean();
                if (TransactionData == null) {
                    resolve("Updated Successfully")
                } else {
                    let Transaction_Detailed_Data = await config.Razorpay_Transaction_Data.find(ele => ele.status == PayoutData.status);
                    if (TransactionData.Transaction_Status == 2 || TransactionData.Transaction_Status == 5 || TransactionData.Transaction_Status == 7) {
                        resolve("Updated Successfully");
                    } else {
                        let Transaction_Status = Transaction_Detailed_Data.Transaction_Status;
                        if (TransactionData.Transaction_Status == Transaction_Status) {
                            resolve("Updated Successfully");
                        } else {
                            let Transaction_Status_Logs = {
                                LogID: uuid.v4(),
                                Transaction_Status: Transaction_Status,
                                Comment: Transaction_Detailed_Data.Comment,
                                Time: new Date()
                            };
                            let Transaction_Reference_ID = (Transaction_Status == 2) ? PayoutData.utr : "";
                            let Transaction_Completion_Remarks = (Transaction_Status == 2) ? Transaction_Detailed_Data.Comment : "";
                            let query = {
                                Withdraw_TransactionID: TransactionData.Withdraw_TransactionID
                            };
                            let changes = {
                                $set: {
                                    Transaction_Status: Transaction_Status,
                                    Transaction_Reference_ID: Transaction_Reference_ID,
                                    Transaction_Completion_Remarks: Transaction_Completion_Remarks,
                                    RazorpayXPayoutData: PayoutData,
                                    updated_at: new Date()
                                },
                                $push: {
                                    Transaction_Status_Logs: Transaction_Status_Logs
                                }
                            };
                            let options = {
                                upsert: true,
                                setDefaultsOnInsert: true,
                                new: true
                            }
                            TransactionData = await Driver_Wallet_Withdraw_Transactions.updateOne(query, changes, options).lean();
                            resolve("Updated Successfully");
                            if (Transaction_Status === 5 || Transaction_Status === 7) {
                                //Amount Reversal
                                let WalletProcessing = await DriverController.Razorpay_Reversal_Amount_Transafer_Failed(TransactionData);

                            };
                        }
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Razorpay_Withdrawl_Wallet_Functionality = (TransactionData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let DriverID = TransactionData.DriverID;
            let Amount = TransactionData.Amount;
            let Service_Amount = TransactionData.Service_Amount;
            let Total_Amount = TransactionData.Total_Amount;
            let L1Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 3, // Amount Deducted for Withdraw from wallet 
                Amount: Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L1SaveResult = await Driver_Wallet_Logs.create(L1Data);
            let L2Data = {
                LogID: uuid.v4(),
                DriverID: DriverID,
                Type: 4, //  Service Amount Deducted for Withdraw from wallet
                Amount: Service_Amount,
                Data: TransactionData,
                Time: new Date()
            };
            let L2SaveResult = await Driver_Wallet_Logs.create(L2Data);
            let fndupdquery = {
                DriverID: DriverID
            };
            let fndupdchanges = {
                $set: {
                    updated_at: new Date()
                },
                $inc: {
                    "Driver_Wallet_Information.Available_Amount": (Total_Amount * -1),
                    "Driver_Wallet_Information.Total_Withdrawn_Amount": Amount,
                    "Driver_Wallet_Information.Total_Withdrawn_Service_Tax_Amount": Service_Amount,
                }
            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let findupdateData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve("Wallet Functionality Completed");
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Driver_Withdraw_Amount = (values, DriverData, BeneficiaryData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let SettingData = await CommonController.Fetch_App_Driver_Settings();
            let Amount = parseFloat(values.Amount);
            let Minimum_Withdraw_Transaction_Amount = SettingData.Minimum_Withdraw_Transaction_Amount;
            let Maximum_Withdraw_Transaction_Amount = SettingData.Maximum_Withdraw_Transaction_Amount;
            let Available_Amount = DriverData.Driver_Wallet_Information.Available_Amount;
            if (Minimum_Withdraw_Transaction_Amount <= Amount && Amount <= Maximum_Withdraw_Transaction_Amount) {
                let Service_Amount = await config.RazorpayX_Service_Amount(Amount);
                let Total_Amount = Amount + Service_Amount;
                Amount = await CommonController.Common_Floating_Beautify_Value(Amount);
                Service_Amount = await CommonController.Common_Floating_Beautify_Value(Service_Amount);
                Total_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Amount);
                if (Total_Amount <= Available_Amount) {
                    let Withdraw_TransactionID = uuid.v4();
                    let RazorpayXPayoutData = await RazorpayController.Razorpay_Beneficiary_Account_Payout(BeneficiaryData, Amount, Withdraw_TransactionID, 3);
                    let Transaction_Detailed_Data = await config.Razorpay_Transaction_Data.find(ele => ele.status == RazorpayXPayoutData.status);
                    let Transaction_Status = Transaction_Detailed_Data.Transaction_Status;
                    let Transaction_Status_Logs = {
                        LogID: uuid.v4(),
                        Transaction_Status: Transaction_Status,
                        Comment: Transaction_Detailed_Data.Comment,
                        Time: new Date()
                    };
                    let Transaction_Reference_ID = (Transaction_Status == 2) ? RazorpayXPayoutData.utr : "";
                    let Transaction_Completion_Remarks = (Transaction_Status == 2) ? Transaction_Detailed_Data.Comment : "";
                    let Data = {
                        Withdraw_TransactionID: Withdraw_TransactionID,
                        RazorpayX_TransactionID: RazorpayXPayoutData.id,
                        CountryID: DriverData.CountryID,
                        CityID: DriverData.CityID,
                        Service_Type: DriverData.Service_Type,
                        DriverID: DriverData.DriverID,
                        Amount: Amount,
                        Service_Amount: Service_Amount,
                        Total_Amount: Total_Amount,
                        BeneficiaryID: BeneficiaryData.BeneficiaryID,
                        RazorpayX_BeneficiaryID: BeneficiaryData.RazorpayX_BeneficiaryID,
                        RazorpayX_ContactID: BeneficiaryData.RazorpayX_ContactID,
                        BeneficiaryType: BeneficiaryData.BeneficiaryType,
                        Name: BeneficiaryData.Name,
                        Account_Number: BeneficiaryData.Account_Number,
                        IFSC: BeneficiaryData.IFSC,
                        Whether_Default_Account: BeneficiaryData.Whether_Default_Account,
                        Bank_Details: BeneficiaryData.Bank_Details,
                        UPI: BeneficiaryData.UPI,
                        Transaction_Status: Transaction_Status,
                        Transaction_Reference_ID: Transaction_Reference_ID,
                        Transaction_Completion_Remarks: Transaction_Completion_Remarks,
                        Transaction_Status_Logs: Transaction_Status_Logs,
                        RazorpayXPayoutData: RazorpayXPayoutData,
                        created_at: new Date(),
                        updated_at: new Date()
                    };
                    let TransactionData = await Driver_Wallet_Withdraw_Transactions.create(Data);
                    resolve({ success: true, extras: { Status: "Withdrawl have been initiated" } })
                    let WalletProcessing = await DriverController.Razorpay_Withdrawl_Wallet_Functionality(TransactionData);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INSUFFICIENT_BALANCE } });
                }
            } else {
                reject({ success: false, extras: { msg: ApiMessages.AMOUNT_NOT_SATISFY_MINIMUM_MAXIMUM_TRANSACTION_AMOUNT } });
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


DriverController.Driver_Fetch_Service_Amount = (values, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let SettingData = await CommonController.Fetch_App_Driver_Settings();
            let Amount = parseFloat(values.Amount);
            let Minimum_Withdraw_Transaction_Amount = SettingData.Minimum_Withdraw_Transaction_Amount;
            let Maximum_Withdraw_Transaction_Amount = SettingData.Maximum_Withdraw_Transaction_Amount;
            let Available_Amount = DriverData.Driver_Wallet_Information.Available_Amount;
            if (Minimum_Withdraw_Transaction_Amount <= Amount && Amount <= Maximum_Withdraw_Transaction_Amount) {
                let Service_Amount = await config.RazorpayX_Service_Amount(Amount);
                let Total_Amount = Amount + Service_Amount;
                Amount = await CommonController.Common_Floating_Beautify_Value(Amount);
                Service_Amount = await CommonController.Common_Floating_Beautify_Value(Service_Amount);
                Total_Amount = await CommonController.Common_Floating_Beautify_Value(Total_Amount);
                if (Total_Amount <= Available_Amount) {
                    let Data = {
                        Amount: Amount,
                        Service_Amount: Service_Amount,
                        Total_Amount: Total_Amount,
                    };
                    resolve({ success: true, extras: { Data: Data } })
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INSUFFICIENT_BALANCE } });
                }
            } else {
                reject({ success: false, extras: { msg: ApiMessages.AMOUNT_NOT_SATISFY_MINIMUM_MAXIMUM_TRANSACTION_AMOUNT } });
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Inactivate_Driver_Beneficiary_Account_For_Bank_Account = values => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                DriverID: values.DriverID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    Status: false,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Driver_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Deleted Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Update_Driver_Beneficiary_Account_For_Bank_Account = (values, BankData, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let RazorpayXBeneficiaryData = await RazorpayController.Create_Razorpay_Driver_Beneficiary_Account_for_Bank_Account(values, DriverData);
            let query = {
                DriverID: values.DriverID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    RazorpayX_BeneficiaryID: RazorpayXBeneficiaryData.id,
                    BeneficiaryType: 1,
                    Name: values.Name,
                    Account_Number: values.Account_Number,
                    IFSC: values.IFSC,
                    Bank_Details: BankData,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Driver_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Make_Default_Driver_Beneficiary_Account_For_Bank_Account = values => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                DriverID: values.DriverID,
                BeneficiaryID: values.BeneficiaryID
            };
            let changes = {
                $set: {
                    Whether_Default_Account: true,
                    updated_at: new Date()
                }
            };
            let UpdatedStatus = await Driver_Bank_Beneficiary_Accounts.updateOne(query, changes).lean();
            resolve({ success: true, extras: { Status: "Updated Successfully" } });
            let oquery = {
                DriverID: values.DriverID,
                BeneficiaryID: {
                    $ne: values.BeneficiaryID
                }
            };
            let ochanges = {
                $set: {
                    Whether_Default_Account: false,
                    updated_at: new Date()
                }
            };
            let oUpdatedStatus = await Driver_Bank_Beneficiary_Accounts.updateMany(oquery, ochanges).lean();
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.List_All_Driver_Beneficiary_Accounts = (values, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DriverID: values.DriverID
                };
                let sortOptions = {
                    created_at: -1
                };
                if (values.Whether_Status_Filter === null || values.Whether_Status_Filter === undefined) {
                    query.Status = true;
                }

                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Result = await Driver_Bank_Beneficiary_Accounts.find(query).select('-_id -__v -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).lean();
                resolve({ success: true, extras: { Data: Result } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Add_Driver_Beneficiary_Account_For_Bank_Account = (values, BankData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Whether_Default_Account = false;
                if (Boolify(values.Whether_Default_Account)) {
                    Whether_Default_Account = true;
                } else {
                    let FetchDefaultAccountAvailable = await Driver_Bank_Beneficiary_Accounts.findOne({ DriverID: values.DriverID, Whether_Default_Account: true, Status: true }).lean();
                    if (FetchDefaultAccountAvailable == null) {
                        Whether_Default_Account = true;
                    } else {
                        let FetchWhetherAccountAvailable = await Driver_Bank_Beneficiary_Accounts.findOne({ DriverID: values.DriverID, Status: true }).lean();
                        if (FetchWhetherAccountAvailable == null) {
                            Whether_Default_Account = true;
                        } else {
                            Whether_Default_Account = false;
                        }
                    }
                }
                let RazorpayXBeneficiaryData = await RazorpayController.Create_Razorpay_Driver_Beneficiary_Account_for_Bank_Account(values, DriverData);
                let Data = {
                    BeneficiaryID: uuid.v4(),
                    RazorpayX_BeneficiaryID: RazorpayXBeneficiaryData.id,
                    DriverID: values.DriverID,
                    RazorpayX_ContactID: DriverData.RazorpayX_ContactID,
                    BeneficiaryType: 1,
                    Name: values.Name,
                    Account_Number: values.Account_Number,
                    IFSC: values.IFSC,
                    Whether_Default_Account: Whether_Default_Account,
                    Bank_Details: BankData,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let DriverBeneficiaryData = await Driver_Bank_Beneficiary_Accounts(Data).save();
                DriverBeneficiaryData = JSON.parse(JSON.stringify(DriverBeneficiaryData));
                resolve({ success: true, extras: { Status: "Beneficiary Account Added Successfully" } });
                if (Whether_Default_Account) {
                    let dfquery = {
                        DriverID: values.DriverID,
                        BeneficiaryID: {
                            $ne: DriverBeneficiaryData.BeneficiaryID
                        }
                    };
                    let dfchanges = {
                        $set: {
                            Whether_Default_Account: false,
                            updated_at: new Date()
                        }
                    };
                    let dfUpdatedStatus = await Driver_Bank_Beneficiary_Accounts.updateMany(dfquery, dfchanges).lean();
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Validate_Driver_Beneficiary_Account_For_Bank_Account_Number_Already_Exist = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                if (values.BeneficiaryID == null || values.BeneficiaryID == undefined || values.BeneficiaryID == '') {
                    values.BeneficiaryID = '';
                }
                let query = {
                    DriverID: values.DriverID,
                    Account_Number: values.Account_Number,
                    BeneficiaryID: {
                        $ne: values.BeneficiaryID
                    }
                };
                let Result = await Driver_Bank_Beneficiary_Accounts.findOne(query).lean();
                if (Result == null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.BENEFICIARY_ACCOUNT_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driver_Fetch_Complete_Wallet_Logs_with_Filter = (values, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let toSkip = parseInt(values.skip);
            let toLimit = parseInt(values.limit);
            let sortOptions = {
                Time: -1
            };
            if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                sortOptions = values.sortOptions;
            };
            let query = {
                DriverID: values.DriverID
            };
            let Count = await Driver_Wallet_Logs.countDocuments(query).lean().exec();
            let Result = await Driver_Wallet_Logs.find(query).select('-_id -__v -Point -Geometry -Delivery_Pricings -Whether_God -PasswordHash -PasswordSalt -SessionID').sort(sortOptions).skip(toSkip).limit(toLimit).lean();
            resolve({ success: true, extras: { Count: Count, Data: Result } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Driver_Fetch_Complete_Wallet_Information = (values, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Driver_Wallet_Information = DriverData.Driver_Wallet_Information;
            let allKeys = await Object.keys(Driver_Wallet_Information);
            async.eachSeries(allKeys, async (key, callback) => {
                try {
                    Driver_Wallet_Information[key] = await CommonController.Common_Floating_Beautify_Value(Driver_Wallet_Information[key]);
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Data: Driver_Wallet_Information } });
            });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Fetch_Google_Road_Distance = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let lat1 = parseFloat(values.lat1);
            let lng1 = parseFloat(values.lng1);
            let lat2 = parseFloat(values.lat2);
            let lng2 = parseFloat(values.lng2);
            let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(lat1, lng1, lat2, lng2);
            let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
            let Duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
            Distance /= 1000; //mtrs to kms
            Duration /= 60;//sec to minutes
            Distance = await CommonController.Common_Floating_Beautify_Value(Distance);
            Duration = await CommonController.Common_Floating_Beautify_Value(Duration);
            resolve({ success: true, extras: { Distance: Distance, Duration: Duration } });
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Reject_Instant_Order_Request = (values, InstantRequestData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    InstantOrderRequestID: values.InstantOrderRequestID
                };
                let changes = {
                    $set: {
                        updated_at: new Date()
                    },
                    $pull: {
                        ALL_Driver_Information: {
                            DriverID: values.DriverID
                        }
                    }
                };
                let UpdatedStatus = await Instant_Order_Request.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Rejected Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Accept_Instant_Order_Request_Validate_Already_Accepted = (values, DriverData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let now = moment().toDate();
            let now_before_15_secs = moment().subtract(config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST, 'seconds').toDate();
            let now_before_10_mins = moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME, 'minutes').toDate();
            let now_before_2_mins = moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH, 'minutes').toDate();
            let query = {
                Status: true,
                $or: [
                    {
                        Instant_Order_Request_Status: 2,
                        Order_Type: 2,
                        DriverID: values.DriverID,
                        Request_Accepted_Time: {
                            $gte: now_before_10_mins,
                            $lte: now
                        },
                    },
                    {
                        Instant_Order_Request_Status: 2,
                        Order_Type: {
                            $in: [1, 3, null, undefined]
                        },
                        DriverID: values.DriverID,
                        Request_Accepted_Time: {
                            $gte: now_before_2_mins,
                            $lte: now
                        },
                    }
                ]
            };
            let Result = await Instant_Order_Request.findOne(query).select('-_id -__v').lean();
            if (Result === null) {
                let trquery = {
                    DriverID: values.DriverID,
                    Total_Trip_Status: {
                        $in: [1, 2]
                    },
                    Status: true
                };
                let TripData = await Trips.findOne(trquery).select('-_id -__v').lean();
                if (TripData === null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.YOU_CANNOT_ACCEPT_NEW_ORDERS } })
                };
            } else {
                reject({ success: false, extras: { msg: ApiMessages.YOU_CANNOT_ACCEPT_NEW_ORDERS } })
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Accept_Instant_Order_Request = (values, InstantRequestData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let now = moment();
                let now_before_15_secs = moment().subtract(config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST, 'seconds');
                let Request_Time = moment(InstantRequestData.Request_Time);
                if (Request_Time.isSameOrAfter(now_before_15_secs) && Request_Time.isSameOrBefore(now)) {
                    if (InstantRequestData.Instant_Order_Request_Status == 1) {
                        let Instant_Order_Request_Status = 2;
                        let Instant_Order_Request_Status_Logs = {
                            ReferenceID: uuid.v4(),
                            Instant_Order_Request_Status: Instant_Order_Request_Status,
                            DriverID: values.DriverID,
                            time: new Date()
                        };
                        let query = {
                            InstantOrderRequestID: values.InstantOrderRequestID
                        };
                        let changes = {
                            $set: {
                                DriverID: values.DriverID,
                                Instant_Order_Request_Status: Instant_Order_Request_Status,
                                Request_Accepted_Time: new Date(),
                                updated_at: new Date()
                            },
                            $push: {
                                Instant_Order_Request_Status_Logs: Instant_Order_Request_Status_Logs
                            }
                        };
                        let UpdatedStatus = await Instant_Order_Request.updateOne(query, changes).lean();
                        resolve({ success: true, extras: { Status: "Accepted Successfully" } });
                        let PubnubMessaging = await PubnubController.Instant_Order_Send_Driver_Request_Pubnub_Message(InstantRequestData.ALL_Driver_Information, InstantRequestData);
                    } else if (InstantRequestData.Instant_Order_Request_Status == 2 || InstantRequestData.Instant_Order_Request_Status == 3) {
                        reject({ success: false, extras: { msg: ApiMessages.INSTANT_ORDER_REQUEST_ALREADY_ACCEPTED } })
                    } else if (InstantRequestData.Instant_Order_Request_Status == 4) {
                        reject({ success: false, extras: { msg: ApiMessages.INSTANT_ORDER_REQUEST_CANCELLED } })
                    } else if (InstantRequestData.Instant_Order_Request_Status == 5) {
                        reject({ success: false, extras: { msg: ApiMessages.DRIVER_NOT_AVAILABLE } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INSTANT_ORDER_REQUEST_EXPIRED } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Instant_Order_Request = (values, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let now = moment().toDate();
                let now_before_15_secs = moment().subtract(config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST, 'seconds').toDate();
                let now_before_10_mins = moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME, 'minutes').toDate();
                let now_before_2_mins = moment().subtract(config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH, 'minutes').toDate();
                let query = {
                    Status: true,
                    CountryID: DriverData.CountryID,
                    CityID: DriverData.CityID,
                    ZoneID: DriverData.ZoneID,
                    Status: true,
                    $or: [
                        {
                            Instant_Order_Request_Status: 1,
                            "ALL_Driver_Information.DriverID": values.DriverID,
                            Order_Type: 2,
                            Request_Time: {
                                $gte: now_before_15_secs,
                                $lte: now
                            },
                        },
                        // {
                        //     Instant_Order_Request_Status: 2,
                        //     Order_Type: 2,
                        //     DriverID: values.DriverID,
                        //     Request_Accepted_Time: {
                        //         $gte: now_before_10_mins,
                        //         $lte: now
                        //     },
                        // },
                        {
                            Instant_Order_Request_Status: 2,
                            Order_Type: {
                                $in: [1, 3, null, undefined]
                            },
                            DriverID: values.DriverID,
                            Request_Accepted_Time: {
                                $gte: now_before_2_mins,
                                $lte: now
                            },
                        }
                    ]
                };
                // console.log(query)
                let Result = await Instant_Order_Request.find(query).select('-_id -__v -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ Request_Time: -1 }).lean().exec();

                async.eachSeries(Result, async (item, callback) => {
                    try {
                        // item.Delivery_Amount = item.Order_Data.Order_Invoice.Order_Delivery_Charge;
                        if (item.Order_Data.Order_Status == 11) {
                            item.Pickup_Location = item.Order_Data.Delivery_Address_Information;
                        } else {
                            item.Pickup_Location = item.Order_Data.Branch_Information;
                        }
                        delete item.Order_Data;
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Start_Driver_Trip_Update_Order_Status = (TripData, DriverData) => { // changes requried
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Order_Status = 5; // Trip Initiated
                let tquery = {
                    TripID: TripData.TripID,
                    DriverID: DriverData.DriverID,
                    Route_Status: {
                        $in: [1, 2]
                    },
                    Route_Action_Next: {
                        $in: [1, 3]//Only pickups
                    },
                    Status: true
                };
                let All_Trip_Orders_ID_Array = await Trips_Orders_Routings.distinct('All_Orders_ID_Array', tquery).lean();
                for (const OrderID of All_Trip_Orders_ID_Array) {
                    let oquery = {
                        OrderID: OrderID
                    };
                    let OrderData = await CommonController.Check_for_Order(oquery);
                    if (OrderData.Order_Status == 11) {
                        let Cancellation_Pickup_Status = 3;
                        let Return_Driver_Logs = {
                            LogID: uuid.v4(),
                            TripID: TripData.TripID,
                            DriverID: DriverData.DriverID,
                            Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                            Time: new Date()
                        };
                        let Return_Event_Logs = {
                            LogID: uuid.v4(),
                            DriverID: DriverData.DriverID,
                            Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                            Address: DriverData.Location.Address,
                            Comments: "Trip Started",
                            lat: DriverData.Location.Latitude,
                            lng: DriverData.Location.Longitude,
                            Time: new Date()
                        };
                        let ochanges = {
                            $set: {
                                Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                                updated_at: new Date()
                            },
                            $push: {
                                Return_Driver_Logs: Return_Driver_Logs,
                                Return_Event_Logs: Return_Event_Logs,
                                Cancellation_Order_Pickup_Report: {
                                    Title: "Trip Started",
                                    Description: "Driver on his way to shop to pickup order",
                                    Time: new Date()
                                },
                            }
                        };
                        let UpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                    } else {
                        let Driver_Logs = {
                            LogID: uuid.v4(),
                            TripID: TripData.TripID,
                            DriverID: DriverData.DriverID,
                            Order_Status: Order_Status,
                            Time: new Date()
                        };
                        let Event_Logs = {
                            LogID: uuid.v4(),
                            DriverID: DriverData.DriverID,
                            Order_Status: Order_Status,
                            Address: DriverData.Location.Address,
                            Comments: "Trip Started",
                            lat: DriverData.Location.Latitude,
                            lng: DriverData.Location.Longitude,
                            Time: new Date()
                        };

                        let ochanges = {
                            $set: {
                                Order_Status: Order_Status,
                                updated_at: new Date()

                            },
                            $push: {
                                Driver_Logs: Driver_Logs,
                                Event_Logs: Event_Logs,
                                Order_Report: {
                                    Title: "Trip Started",
                                    Description: "Driver on his way to shop to pickup order",
                                    Time: new Date()
                                },
                            }
                        };
                        let UpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                    }
                }
                resolve("Updated Successfully")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Start_Driver_Trip = (TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (TripData.Total_Trip_Status == 1) {
                    let query = {
                        TripID: TripData.TripID
                    };
                    let changes = {
                        $set: {
                            Total_Trip_Status: 2,//Trip Started
                            Trip_Start_Time: new Date(),
                            created_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Trips.updateOne(query, changes).lean();
                    resolve({ success: true, extras: { Status: "Trip Started Successfully" } })
                } else if (TripData.Total_Trip_Status == 2) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_ALREADY_STARTED } })
                } else if (TripData.Total_Trip_Status == 3) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_ALREADY_COMPLETED } })
                } else if (TripData.Total_Trip_Status == 4) {
                    reject({ success: false, extras: { msg: ApiMessages.TRIP_REPORTED } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Trips = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let toSkip = parseInt(values.skip);
                let toLimit = parseInt(values.limit);
                let query = {
                    Status: true,
                    $and: [
                        {
                            $or: [
                                {
                                    DriverID: values.DriverID
                                },
                                {
                                    "All_Driver_Logs.DriverID": values.DriverID
                                }
                            ]
                        },
                        {
                            $or: [
                                {
                                    Total_Trip_Status: {
                                        $in: [1, 2]
                                    }
                                }
                            ]
                        }
                    ]
                };
                let Count = await Trips.countDocuments(query).lean().exec();
                let Result = await Trips.find(query).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ created_at: -1 }).skip(toSkip).limit(toLimit).lean().exec();
                async.eachSeries(Result, async (TripData, callback) => {
                    try {
                        // if (values.DriverID != TripData.DriverID) {
                        //     TripData.Total_Trip_Status = 4;
                        // }
                        let q = {
                            OrderID: TripData.OrderID
                        };
                        let r = await Buyer_Orders.findOne(q).select('Order_Number Cart_Information Delivered_To Order_Report Driver_Information Order_Status Order_Invoice.Order_Delivery_Charge Branch_Information Delivery_Address_Information BuyerID').lean().exec();
                        TripData.Delivery_Charge = r.Order_Invoice.Order_Delivery_Charge;
                        TripData.Pickup_Address_Information = r.Branch_Information;
                        TripData.Drop_Address_Information = r.Delivery_Address_Information;
                        TripData.Order_Status = r.Order_Status
                        TripData.Delivered_To = r.Delivery_Address_Information.Name;
                        TripData.Order_Number = r.Order_Number;
                        TripData.Total_Duration = await CommonController.Common_Time_String_From_MS(TripData.Total_Duration);
                        let OrderReport = []
                        for (let Order_Report of r.Order_Report) {
                            let Data = new Object();
                            if (Order_Report.Title == "Order Received") {
                                Data = {
                                    Status: "Order Received",
                                    Description: r.Branch_Information.Branch_Name + " has received your order successfully",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Driver Assigned") {
                                Data = {
                                    Status: "Driver Assigned",
                                    Description: "Your assigned delivery executive for this order is Mr." + r.Driver_Information.Driver_Name,
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Order Picked") {
                                Data = {
                                    Status: "Order Picked Up",
                                    Description: "Mr." + r.Driver_Information.Driver_Name + " has arrived at the bookstore to pick up your order",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Drop Stated") {
                                // let q = {
                                //     OrderID: r.OrderID
                                // }
                                // let TripData = await Trips.findOne(q).lean().exec();
                                Data = {
                                    Status: "Order In Transit",
                                    Description: "Your order has been picked up and is on the way for delivery ETA: approximately " + TripData.Total_Eta.toFixed(2) + " Min",
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            } else if (Order_Report.Title == "Order Delivered") {
                                Data = {
                                    Status: "Order Completed",
                                    Description: "Your order has been delivered successfully, Received by: " + r.Delivered_To + ", Ordered by: " + r.Delivery_Address_Information.Name,
                                    Time: Order_Report.Time
                                }
                                OrderReport.push(Data)
                            }
                        }
                        TripData.Order_Report = OrderReport
                        TripData.Total_Eta = TripData.Total_Eta.toFixed(2)
                        TripData.Cart_Information = r.Cart_Information
                        let qb = {
                            BuyerID: r.BuyerID
                        };
                        let rb = await Buyers.findOne(qb).select('BuyerID Buyer_Name Buyer_Email Buyer_CountryCode Buyer_PhoneNumber ').lean().exec();
                        TripData.Order_Placed_By = rb
                        
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Trips_By_Date = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let from_date = moment(values.Date, config.Take_Date_Format).toDate();
                let to_date = moment(values.Date, config.Take_Date_Format).add(1, 'day').subtract(1, 'ms').toDate();
                let time_options = {
                    $gte: from_date,
                    $lt: to_date
                };
                let query = {
                    Status: true,
                    //Whether_Trip_Weights_Fulfilled: true,
                    $and: [
                        {
                            $or: [
                                {
                                    DriverID: values.DriverID
                                },
                                {
                                    "All_Driver_Logs.DriverID": values.DriverID
                                }
                            ]
                        },
                        {
                            $or: [
                                {
                                    Trip_Start_Time: time_options
                                },
                                {
                                    Trip_Completion_Time: time_options
                                },
                                {
                                    //Whether_Window_Trip: false,
                                    created_at: time_options
                                },
                                // {
                                //     Whether_Window_Trip: true,
                                //     Window_Start_Time: time_options
                                // },
                                {
                                    Total_Trip_Status: {
                                        $in: [1, 2]
                                    }
                                }
                            ]
                        }
                    ]
                };
                let Result = await Trips.find(query).select('-_id -__v -Status -Point -Geometry -Delivery_Pricings -PasswordHash -PasswordSalt -SessionID').sort({ created_at: -1 }).lean().exec();
                async.eachSeries(Result, async (TripData, callback) => {
                    try {
                        if (values.DriverID != TripData.DriverID) {
                            TripData.Total_Trip_Status = 4;
                        }
                        TripData.Total_Duration = await CommonController.Common_Time_String_From_MS(TripData.Total_Duration);
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Data: Result } });
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Trips_By_Date_Validate_Date = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (moment(values.Date, config.Take_Date_Format).isValid()) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DATE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



DriverController.Driver_Logout = (values, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DriverID: values.DriverID
                };
                let changes = {
                    $set: {
                        DeviceID: '',
                        SessionID: '',
                        Whether_Driver_Online: false,
                        "Driver_Device_Information.Last_Logout_Time": new Date(),
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Drivers.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Logout Successfully" } })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



DriverController.Driver_Update_Online_Offline = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DriverID: values.DriverID
                };
                let changes = {
                    $set: {
                        Whether_Driver_Online: Boolify(values.Whether_Driver_Online),
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Drivers.updateOne(query, changes).lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};


DriverController.Fetch_Complete_Driver_Information = (DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                DriverData.ImageData = await CommonController.Common_Image_Response_Single_Image(DriverData.ImageAvailable, DriverData.ImageData);
                let VehicleData = await CommonController.Check_for_Vehicle(DriverData);
                DriverData.Vehicle_Name = VehicleData.Vehicle_Name;
                DriverData.Vehicle_Description = VehicleData.Vehicle_Description;
                DriverData.Vehicle_Type = VehicleData.Vehicle_Type;
                let Rating_Information = await DriverController.Driving_Rating_Information(DriverData.DriverID);
                DriverData = await Object.assign(DriverData, Rating_Information);
                resolve({ success: true, extras: { Status: "Login Succeesfully", Data: DriverData } });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Generate_Driver_Session = (DriverData, DeviceData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: DriverData.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        SessionID: uuid.v4(),
                        DeviceID: DeviceData.DeviceID,
                        Whether_App_Login: true,
                        "Driver_Device_Information.Last_Login_Time": new Date(),
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                DriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v -Categorories_Data').lean();
                let Result = await DriverController.Fetch_Complete_Driver_Information(DriverData);
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

DriverController.Validate_Create_Driver_OTP = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let obj1 = {
                    $ne: null
                };
                let obj2 = {
                    $eq: values.OTP
                }
                let OTP_Query = (String(config.SECRET_OTP_CODE) == String(values.OTP)) ? obj1 : obj2;
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    OTP: OTP_Query,
                    Latest: true
                };
                let Result = await Create_Driver_OTPS.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                    let Data = {
                        Driver_Country_Code: values.Driver_Country_Code,
                        Driver_Phone_Number: values.Driver_Phone_Number,
                        Time: new Date()
                    };
                    let SaveResult = await Create_Driver_OTP_Tries(Data).save();
                } else {
                    resolve(Result);
                    let RemoveTries = await Create_Driver_OTP_Tries.deleteMany({
                        Driver_Country_Code: values.Driver_Country_Code,
                        Driver_Phone_Number: values.Driver_Phone_Number,
                    }).lean();
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Check_for_Create_Driver_OTP_Tries_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await Create_Driver_OTP_Tries.countDocuments(query).lean().exec();
                if (Count <= config.OTP_TRIES_COUNT) {
                    resolve('Validated Successfully');
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_TRIES_EXCEED_TRY_AFTER_SOME_TIME } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Validate_Driver_OTP = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let obj1 = {
                    $ne: null
                };
                let obj2 = {
                    $eq: values.OTP
                }
                let OTP_Query = (String(config.SECRET_OTP_CODE) == String(values.OTP)) ? obj1 : obj2;
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    OTP: OTP_Query,
                    Latest: true
                };
                let Result = await Driver_OTPS.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                    let Data = {
                        Driver_Country_Code: values.Driver_Country_Code,
                        Driver_Phone_Number: values.Driver_Phone_Number,
                        Time: new Date()
                    };
                    let SaveResult = await Driver_OTP_Tries(Data).save();
                } else {
                    resolve("Validated Successfully");
                    let RemoveTries = await Driver_OTP_Tries.deleteMany({
                        Driver_Country_Code: values.Driver_Country_Code,
                        Driver_Phone_Number: values.Driver_Phone_Number,
                    }).lean();
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Check_for_Driver_OTP_Tries_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_TRIES_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await Driver_OTP_Tries.countDocuments(query).lean().exec();
                if (Count <= config.OTP_TRIES_COUNT) {
                    resolve('Validated Successfully');
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_TRIES_EXCEED_TRY_AFTER_SOME_TIME } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Generate_Driver_OTP_Send_Message = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OTP = await CommonController.Random_OTP_Number();
                let Data = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    OTP: OTP,
                    Latest: true,
                    Time: new Date()
                }
                let SaveResult = await Driver_OTPS(Data).save();
                resolve({ success: true, extras: { Status: "OTP Sent Successfully" } })
                let PhoneNumber = `${values.Driver_Country_Code}${values.Driver_Phone_Number}`;
                let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, OTP);
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    OTP: { $ne: OTP }
                };
                let changes = {
                    Latest: false
                };
                let UpdatedStatus = await Driver_OTPS.updateMany(query, changes).lean();
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Check_for_Driver_OTP_Count = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let time = moment().subtract(config.OTP_COUNT_TIME_IN_MINUTES, 'minutes').toDate();
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    Time: {
                        $gte: time
                    }
                };
                let Count = await Driver_OTPS.countDocuments(query).lean();
                if (Count <= config.OTP_COUNT) {
                    resolve('Validated Successfully')
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.OTP_REQUEST_EXCEED_TRY_AFTER_SOME_TIME } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Check_Whether_Driver_Phone_Number_Registered = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number
                };
                let Result = await Drivers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.PHONE_NUMBER_NOT_REGISTERED } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Activate_Driver = (values, AdminData, OldDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        Status: true,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Activated Successfully" } });
                let StoreLog = await AdminLogController1.Activate_Driver_Log(AdminData, OldDriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Inactivate_Driver = (values, AdminData, OldDriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        Status: false,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Inactivated Successfully" } });
                let StoreLog = await AdminLogController1.Inactivate_Driver_Log(AdminData, OldDriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Zone = (values, AdminData, DriverData, CountryData, CityData, OldZoneData, NewZoneData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        CountryID: values.CountryID,
                        CityID: values.CityID,
                        ZoneID: values.ZoneID,
                        Service_Type: NewZoneData.Service_Type,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Zone_Log(AdminData, DriverData, CountryData, CityData, OldZoneData, NewZoneData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Vehicle = (values, AdminData, DriverData, NewVehicleData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OldVehicleData = await CommonController.Check_for_Vehicle(DriverData);
                OldVehicleData.VehicleNumber = DriverData.VehicleNumber;
                NewVehicleData.VehicleNumber = values.VehicleNumber;
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        VehicleID: values.VehicleID,
                        VehicleNumber: values.VehicleNumber,
                        updated_at: new Date()
                    },
                    $push: {
                        VehicleHistory: {
                            VehicleID: values.VehicleID,
                            VehicleNumber: values.VehicleNumber,
                            Time: new Date()
                        }
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Vehicle_Log(AdminData, DriverData, OldVehicleData, NewVehicleData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Timings = (values, AdminData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        Driver_Timings: {
                            Start_at: values.Start_at,
                            End_at: values.End_at
                        },
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Timings_Log(AdminData, DriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Address = (values, AdminData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude) || 0;
                let Longitude = parseFloat(values.Longitude) || 0;
                let Point = [
                    Longitude,
                    Latitude
                ];
                let Location = new Object();
                Location.Address = values.Address;
                Location.Latitude = Latitude;
                Location.Longitude = Longitude;
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        Location: Location,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                };
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Address_Log(AdminData, DriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Image = (values, AdminData, DriverData, ImageData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        ImageAvailable: true,
                        ImageData: ImageData,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Image_Log(AdminData, DriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Edit_Driver_Profile = (values, AdminData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    DriverID: values.DriverID
                };
                let fndupdchanges = {
                    $set: {
                        Driver_Name: values.Driver_Name,
                        Driver_Country_Code: values.Driver_Country_Code,
                        Driver_Phone_Number: values.Driver_Phone_Number,
                        Driver_EmailID: values.Driver_EmailID,
                        updated_at: new Date()
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let NewDriverData = await Drivers.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve({ success: true, extras: { Status: "Updated Successfully" } });
                let StoreLog = await AdminLogController1.Edit_Driver_Profile_Log(AdminData, DriverData, NewDriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driving_Rating_Information = (DriverID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Whether_Avg_Rating_Available = false;
                let Avg_Rating_Count = 0;
                let Avg_Rating = 0;
                let query = {
                    DriverID: DriverID,
                    Status: true
                };
                let gmap = {
                    _id: "Avg_Rating",
                    Avg_Rating_Count: {
                        $sum: 1
                    },
                    Avg_Rating: {
                        $avg: "$Rating_Points"
                    }
                }
                let Result = await Driver_Ratings.aggregate().match(query).group(gmap);
                if (Result.length > 0) {
                    Whether_Avg_Rating_Available = true;
                    Avg_Rating_Count = await Result[0].Avg_Rating_Count;
                    Avg_Rating = await Result[0].Avg_Rating;
                    resolve({ Whether_Avg_Rating_Available: Whether_Avg_Rating_Available, Avg_Rating_Count: Avg_Rating_Count, Avg_Rating: Avg_Rating });
                } else {
                    resolve({ Whether_Avg_Rating_Available: Whether_Avg_Rating_Available, Avg_Rating_Count: Avg_Rating_Count, Avg_Rating: Avg_Rating });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Drivers_with_Filter = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let sortOptions = {
                    Driver_Name: 1
                };
                let toSkip = Math.abs(parseInt(values.skip));
                let toLimit = parseInt(values.limit);
                if (values.sortOptions != null && Object.keys(values.sortOptions).length > 0) {
                    sortOptions = values.sortOptions;
                };
                let query = {
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    Service_Type: 1
                };
                if (values.CountryID != '') {
                    query.CountryID = values.CountryID;
                }
                if (values.CityID != '') {
                    query.CityID = values.CityID;
                }
                if (values.ZoneID != '') {
                    query.ZoneID = values.ZoneID;
                }
                if (Boolify(values.Whether_Search_Filter)) {
                    let SearchOptions = {
                        $regex: values.SearchValue, $options: "i"
                    };
                    query["$or"] = [
                        {
                            Driver_Name: SearchOptions
                        },
                        {
                            Driver_Phone_Number: SearchOptions
                        },
                        {
                            Driver_EmailID: SearchOptions
                        },
                        {
                            VehicleNumber: SearchOptions
                        }
                    ]
                };
                if (Boolify(values.Whether_Status_Filter)) {
                    query.Status = await Boolify(values.Status)
                };
                let Count = await Drivers.countDocuments(query).lean().exec();
                let Data = await Drivers.find(query).select('-_id -__v -Point -Geometry -Delivery_Pricings -Categorories_Data').skip(toSkip).limit(toLimit).sort(sortOptions).lean().exec();
                async.eachSeries(Data, async (item, callback) => {
                    let ZoneData = await CommonController.Check_for_Zone(item);
                    let VehicleData = await CommonController.Check_for_Vehicle(item);
                    let ImageData = await CommonController.Common_Image_Response_Single_Image(item.ImageAvailable, item.ImageData);
                    let Licence_ImageData = await Driver_Licence.findOne({ DriverID: item.DriverID, Status: true }).lean().exec();
                    let Licence_ImageData_ImageData = {
                        ImageID: '',
                        Image50: '',
                        Image100: '',
                        Image250: '',
                        Image550: '',
                        Image900: '',
                        ImageOriginal: '',
                    }
                    if (Licence_ImageData != null) {
                        Licence_ImageData_ImageData = await CommonController.Common_Image_Response_Single_Image(true, Licence_ImageData.ImageData);
                    }
                    let FileDatax = await Driver_Documents.find({ DriverID: item.DriverID, Status: true }).lean().exec();
                    let FileData = [];
                    if (FileDatax != null) {
                        for (const file of FileDatax) {
                            let fileData = await CommonController.Common_File_Response_Single_File(true, file);
                            FileData.push(fileData)
                        }
                    }
                    item.No_of_Documents = await Driver_Documents.countDocuments({ DriverID: item.DriverID, Status: true }).lean();
                    item.ImageData = ImageData;
                    item.Licence_ImageData = Licence_ImageData_ImageData;
                    item.FileData = FileData;
                    item.Zone_Number = ZoneData.Zone_Number;
                    item.Zone_Title = ZoneData.Zone_Title;
                    item.Vehicle_Name = VehicleData.Vehicle_Name;
                    item.Vehicle_Description = VehicleData.Vehicle_Description;
                    item.Vehicle_Type = VehicleData.Vehicle_Type;
                    let Rating_Information = await DriverController.Driving_Rating_Information(item.DriverID);
                    item = await Object.assign(item, Rating_Information);
                    callback();
                }, err => {
                    if (err) reject(err);
                    resolve({ success: true, extras: { Count: Count, Data: Data } });
                })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.List_All_Vehicles_with_Count = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Service_Type = 1;
            let unquery = {
                CountryID: values.CountryID,
                CityID: values.CityID,
                Service_Type: Service_Type,
            };
            let VehicleID_Array = await Drivers.distinct("VehicleID", unquery).lean();
            let query = {
                VehicleID: {
                    $in: VehicleID_Array
                },
                Status: true
            };
            let Result = await Vehicles.find(query).lean().select("-_id VehicleID Vehicle_Name Vehicle_Description ");
            async.eachSeries(Result, async (item, callback) => {
                try {
                    let thisQuery = {
                        CountryID: values.CountryID,
                        CityID: values.CityID,
                        Service_Type: Service_Type,
                        VehicleID: item.VehicleID
                    }
                    item.Vehicle_Count = await Drivers.countDocuments(thisQuery).lean();
                    callback();
                } catch (error) {
                    callback(error);
                }
            }, async (err) => {
                if (err) reject(err);
                resolve({ success: true, extras: { Data: Result } })
            });

        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

DriverController.Add_Update_RazorpayX_Driver_Contact = (DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let RazorpayXContactData = await RazorpayController.Create_Razorpay_Driver_Contact(DriverData);
                let query = {
                    DriverID: DriverData.DriverID
                };
                let changes = {
                    $set: {
                        Whether_RazorpayX_Driver_Register: true,
                        RazorpayX_ContactID: RazorpayXContactData.id,
                        updated_at: new Date()
                    }
                };
                let UpdatedStatus = await Drivers.updateOne(query, changes).lean();
                resolve("Updated Successfully");
            } catch (error) {
                // console.log(error)
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Create_Driver_OTP = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let OTP = await CommonController.Random_OTP_Number();
                let Data = {
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    VehicleID: values.VehicleID,
                    VehicleNumber: values.VehicleNumber,
                    Driver_Name: values.Driver_Name,
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    Driver_EmailID: values.Driver_EmailID,
                    Address: values.Address,
                    Latitude: values.Latitude,
                    Longitude: values.Longitude,
                    Start_at: values.Start_at,
                    End_at: values.End_at,
                    ImageID: values.ImageID,
                    Licence_ImageID: values.Licence_ImageID,
                    FileID_Array: values.FileID_Array,
                    OTP: OTP,
                    Latest: true,
                    Time: new Date()
                }
                let SaveData = await Create_Driver_OTPS(Data).save()
                resolve({
                    success: true, extras: {
                        Status: "OTP Sent Successfully",
                        Data: {
                            Driver_Name: values.Driver_Name,
                            Driver_Country_Code: values.Driver_Country_Code,
                            Driver_Phone_Number: values.Driver_Phone_Number,
                        }
                    }
                })
                let PhoneNumber = `${values.Driver_Country_Code}${values.Driver_Phone_Number}`;
                let MsgData = 'Welcome To Bookafella Driver Application. OTP for Verification is ' + OTP + ' Share OTP with Bookafella team for account verification.  \n --Thank you,  \n Bookafella Team'
                let OTPStatus = await MessagesController.Send_OTP_TO_Mobile(PhoneNumber, MsgData);
                let query = {
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    OTP: { $ne: OTP }
                };
                let changes = {
                    Latest: false
                };
                let UpdatedStatus = await Create_Driver_OTPS.updateMany(query, changes).lean();
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}



DriverController.Create_Driver = (values, ImageData, ZoneData, VehicleData, AdminData, CountryData, CityData, Licence_ImageData, FileData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Latitude = parseFloat(values.Latitude) || 0;
                let Longitude = parseFloat(values.Longitude) || 0;
                let Point = [
                    Longitude,
                    Latitude
                ];
                let Location = new Object();
                Location.Address = values.Address;
                Location.Latitude = Latitude;
                Location.Longitude = Longitude;
                let DriverID = uuid.v4()
                let Data = {
                    DriverID: DriverID,
                    CountryID: values.CountryID,
                    CityID: values.CityID,
                    ZoneID: values.ZoneID,
                    Service_Type: ZoneData.Service_Type,
                    Driver_Name: values.Driver_Name,
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number,
                    Driver_EmailID: values.Driver_EmailID,
                    Driver_Timings: {
                        Start_at: values.Start_at,
                        End_at: values.End_at
                    },
                    ImageAvailable: (ImageData == null) ? false : true,
                    ImageData: ImageData,
                    Location: Location,
                    Point: Point,
                    VehicleID: values.VehicleID,
                    VehicleNumber: values.VehicleNumber,
                    VehicleHistory: {
                        VehicleID: values.VehicleID,
                        VehicleNumber: values.VehicleNumber,
                        Time: new Date()
                    },
                    //Whether_All_Categories_Delivery: Boolify(values.Whether_All_Categories_Delivery),
                    //Categorories_Data: Categorories_Data,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let DriverData = await Drivers(Data).save();
                // Save Licence
                let LData = {
                    DriverID: DriverID,
                    LicenceID: uuid.v4(),
                    ImageData: Licence_ImageData,
                    Status: true,
                    created_at: new Date(),
                    updated_at: new Date()
                }
                let LicenceData = await Driver_Licence(LData).save();
                //Save Doc
                for (const File of FileData) {
                    let DData = {
                        DriverID: DriverID,
                        FileID: File.FileID,
                        File_Path: File.File_Path,
                        contentType: File.contentType,
                        contentSize: File.contentSize,
                        Status: true,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                    let DocData = await Driver_Documents(DData).save();
                }
                DriverData = await JSON.parse(JSON.stringify(DriverData));
                resolve({ success: true, extras: { Status: "Driver Added Successfully" } });
                let StoreLog = await AdminLogController1.Create_Driver_Log(AdminData, DriverData, CountryData, CityData, ZoneData);
                let RazorpayXContactStoring = await DriverController.Add_Update_RazorpayX_Driver_Contact(DriverData);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Check_Whether_Driver_Phone_Number_Already_Registered = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                values.DriverID = (values.DriverID == null) ? "" : values.DriverID;
                let query = {
                    DriverID: { $ne: values.DriverID },
                    Driver_Country_Code: values.Driver_Country_Code,
                    Driver_Phone_Number: values.Driver_Phone_Number
                };
                let Result = await Drivers.findOne(query).lean();
                if (Result == null) {
                    resolve("Validity Status");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.PHONE_NUMBER_ALREADY_REGISTERED } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Common_Validate_Vehicle_Number_Already_Exist = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                values = JSON.parse(JSON.stringify(values));
                if (values.DriverID == null || values.DriverID == undefined || values.DriverID == '') {
                    values.DriverID = '';
                }
                let query = {
                    DriverID: {
                        $ne: values.DriverID
                    },
                    VehicleNumber: values.VehicleNumber
                };
                let Result = await Drivers.findOne(query).lean();
                if (Result == null) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.VEHICLE_NUMBER_ALREADY_EXIST } })
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driver_LatLong_Storing_along_with_Distance = (Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DriverID = Data.DriverID;
                let Latitude = parseFloat(Data.Latitude);
                let Longitude = parseFloat(Data.Longitude);
                let Point = [Longitude, Latitude];
                let This_Point_Last_Point_Distance = await DriverController.Driver_LatLong_Storing_along_with_Distance_Find_Fetch_The_Last_Distance(Data);
                let SaveLatlongs = {
                    DriverID: DriverID,
                    Latitude: Latitude,
                    Longitude: Longitude,
                    Point: Point,
                    This_Point_Last_Point_Distance: This_Point_Last_Point_Distance,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SaveResult = await Driver_Location_Logs(SaveLatlongs).save();
                resolve("Stored Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

DriverController.Driver_LatLong_Storing_along_with_Distance_Find_Fetch_The_Last_Distance = (Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let DriverID = Data.DriverID;
                let x1 = parseFloat(Data.Latitude);
                let y1 = parseFloat(Data.Longitude);
                let now = moment();
                let today_start = moment(now).startOf('day').toDate();
                let today_end = moment(now).endOf('day').toDate();
                let query = {
                    DriverID: DriverID,
                    Status: true,
                    created_at: {
                        $gte: today_start,
                        $lt: today_end
                    }
                };
                let LastLocationData = await Driver_Location_Logs.findOne(query).sort({ created_at: -1 }).lean();
                if (LastLocationData == null) {
                    resolve(0);
                } else {
                    let x2 = parseFloat(LastLocationData.Latitude);
                    let y2 = parseFloat(LastLocationData.Longitude);
                    let This_Point_Last_Point_Distance = await GeoDistance.Find_Geo_Distance(x1, y1, x2, y2);
                    resolve(This_Point_Last_Point_Distance);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}
export default DriverController;