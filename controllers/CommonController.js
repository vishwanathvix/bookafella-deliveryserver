let CommonController = function () { };

import App_SMS_Providers from "../models/App_SMS_Providers";
import validator from "validator";
import ApiMessages from "../models/ApiMessages";
import Counters from "../models/Counters";
import config from "../config/config";
import Admin_User from "../models/Admin_User";
import ZONES from "../models/ZONES";
import City_Zone_Hub_Version from "../models/City_Zone_Hub_Version";
import State from "../models/State";
import City from "../models/City";
import City_Zone_Version from "../models/City_Zone_Version";
import Country from "../models/Country";
import Store_Branch from "../models/Store_Branch";
import Customers from "../models/Customers";
import Store_Entity from "../models/Store_Entity";
import uuid from "uuid";
import ApiResponce from "../models/Apiresponce";
import Images from "../models/Images";
import Drivers from "../models/Drivers";
import Vehicles from "../models/Vehicles";
import App_Versions_Settings from "../models/App_Versions_Settings";
import Devices from "../models/Devices";
import Buyers from "../models/Buyers";
import Store_Products from "../models/Store_Products";
import moment from "moment";
import Driver_Devices from "../models/Driver_Devices";
import App_Driver_Settings from "../models/App_Driver_Settings";
import Trips from "../models/Trips";
import distance from "google-distance-matrix";
import axios from "axios";
import async from "async";
import mongoose from "mongoose";
import Instant_Order_Request from "../models/Instant_Order_Request";
import ifsc from "ifsc";
import Driver_Bank_Beneficiary_Accounts from "../models/Driver_Bank_Beneficiary_Accounts";
import Buyer_Orders from "../models/Buyer_Orders";
import PubnubController from "./PubnubController";
import Branch_Bank_Beneficiary_Accounts from "../models/Branch_Bank_Beneficiary_Accounts";
import Sub_Trips from "../models/Sub_Trips";
import Sub_Trips_Orders from "../models/Sub_Trips_Orders";
import Trips_Orders_Routings from "../models/Trips_Orders_Routings";
import GeoDistance from "./GeoDistance";
import Discounts from "../models/Discounts";
import App_Automated_Offers from "../models/App_Automated_Offers";
import Special_Offers from "../models/Special_Offers";
import User_Discount_Apply from "../models/User_Discount_Apply";
import Stack_Log from "../models/Stack_Log";
import Branch_Stack_Log from "../models/Branch_Stack_Log";
import Files from "../models/Files";
import Store_Branch_OTP from "../models/Store_Branch_OTP";
import Store_OTP_Tries from "../models/Store_OTP_Tries";
import Title_Management from "../models/Title_Management";
import Title_Management_Products from "../models/Title_Management_Products";
import Zone_Fees from "../models/Zone_Fees";
import Banner from "../models/Banner";
import Help_Data from "../models/Help_Data";
import Category from "../models/Category";
import Custom_Notification_Log from "../models/Custom_Notification_Log";
import Buyers_Current_Location from "../models/Buyers_Current_Location";

distance.key(config.Google.key);
distance.mode('driving');

CommonController.Check_for_Stack = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StackID: values.StackID
                }
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {
                    resolve(Result)
                } else {
                    resolve({ success: false, extras: { msg: ApiMessages.INVALID_STACK } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Get_Buyer_Current_Location = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                }
                let Result = await Buyers_Current_Location.findOne(query).lean().exec();
                if (Result != null) {
                    resolve(Result)
                } else {
                    resolve({ success: false, extras: { msg: ApiMessages.LOCATION_NOT_IN_ZONE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Suggest_Custom_Notification_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                }
                let Result = await Custom_Notification_Log.find(query).sort({ SNo: -1 }).limit(1).lean().exec();
                if (Result.length == 0) {
                    resolve({ success: true, extras: { Data: 1 } })
                } else {
                    resolve({ success: true, extras: { Data: Result[0].SNo + 1 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Suggest_Category_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                }
                let Result = await Category.find(query).sort({ SNo: -1 }).limit(1).lean().exec();
                if (Result.length == 0) {
                    resolve({ success: true, extras: { Data: 1 } })
                } else {
                    resolve({ success: true, extras: { Data: Result[0].SNo + 1 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Category_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log(values.Old_SNO, values.SNo)
                let Old_SNO;
                if (values.CategoryID != '' && values.CategoryID != null) {
                    let Queryx = {
                        CategoryID: values.CategoryID
                    }
                    let Resultx = await Category.findOne(Queryx).lean().exec();
                    Old_SNO = Resultx.SNo
                }
                let query = {
                    SNo: values.SNo
                }
                let Result = await Category.findOne(query).lean().exec();
                if (Result == null || Old_SNO == values.SNo) {
                    resolve('SNo Available');
                } else {
                    reject({ success: false, extras: { msg: "SERIAL NUMBER NOT AVAILABLE" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Suggest_Help_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                }
                let Result = await Help_Data.find(query).sort({ SNo: -1 }).limit(1).lean().exec();
                if (Result.length == 0) {
                    resolve({ success: true, extras: { Data: 1 } })
                } else {
                    resolve({ success: true, extras: { Data: Result[0].SNo + 1 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Help_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log(values.Old_SNO, values.SNo)
                let Old_SNO;
                if (values.HelpDataID != '' && values.HelpDataID != null) {
                    let Queryx = {
                        HelpDataID: values.HelpDataID
                    }
                    let Resultx = await Help_Data.findOne(Queryx).lean().exec();
                    Old_SNO = Resultx.SNo
                }
                let query = {
                    SNo: values.SNo
                }
                let Result = await Help_Data.findOne(query).lean().exec();
                if (Result == null || Old_SNO == values.SNo) {
                    resolve('SNo Available');
                } else {
                    reject({ success: false, extras: { msg: "SERIAL NUMBER NOT AVAILABLE" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_For_Product_Dynamic = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ProductID: values.ProductID,
                    TitleID: values.TitleID
                }
                let Result = await Title_Management_Products.findOne(query).lean().exec();
                if (Result == null) {
                    resolve({ success: true, extras: { msg: ApiMessages.INVALID_PRODUCT_DYNAMIC } })
                } else {
                    resolve(Result)
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Suggest_Banner_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                }
                let Result = await Banner.find(query).sort({ SNo: -1 }).limit(1).lean().exec();
                if (Result.length == 0) {
                    resolve({ success: true, extras: { Data: 1 } })
                } else {
                    resolve({ success: true, extras: { Data: Result[0].SNo + 1 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Banner_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log(values.Old_SNO, values.SNo)
                let Old_SNO;
                if (values.BannerID != '' && values.BannerID != null) {
                    let Queryx = {
                        BannerID: values.BannerID
                    }
                    let Resultx = await Banner.findOne(Queryx).lean().exec();
                    Old_SNO = Resultx.SNo
                }
                let query = {
                    SNo: values.SNo
                }
                let Result = await Banner.findOne(query).lean().exec();
                if (Result == null || Old_SNO == values.SNo) {
                    resolve('SNo Available');
                } else {
                    reject({ success: false, extras: { msg: "SERIAL NUMBER NOT AVAILABLE" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Suggest_Title_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {

                }
                let Result = await Title_Management.find(query).sort({ SNo: -1 }).limit(1).lean().exec();
                if (Result.length == 0) {
                    resolve({ success: true, extras: { Data: 1 } })
                } else {
                    resolve({ success: true, extras: { Data: Result[0].SNo + 1 } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Title_SNo = (values) => {
    return new Promise(async (resolve, reject) => {
        setImmediate(async () => {
            try {
                // console.log(values.Old_SNO, values.SNo)
                let Old_SNO;
                if (values.TitleID != '' && values.TitleID != null) {
                    let Queryx = {
                        TitleID: values.TitleID
                    }
                    let Resultx = await Title_Management.findOne(Queryx).lean().exec();
                    Old_SNO = Resultx.SNo
                }
                let query = {
                    SNo: values.SNo
                }
                let Result = await Title_Management.findOne(query).lean().exec();
                if (Result == null || Old_SNO == values.SNo) {
                    resolve('SNo Available');
                } else {
                    reject({ success: false, extras: { msg: "SERIAL NUMBER NOT AVAILABLE" } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Title_Available = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let Old_Title;
            if (values.TitleID != '' && values.TitleID != null) {
                let Queryx = {
                    TitleID: values.TitleID
                }
                let Resultx = await Title_Management.findOne(Queryx).lean().exec();
                Old_Title = Resultx.Title
            }
            let query = {
                Title: values.Title
            }
            let Result = await Title_Management.findOne(query).lean().exec();
            if (Result == null || Old_Title == values.Title) {
                resolve('Title Available');
            } else {
                reject({ success: false, extras: { msg: ApiMessages.TITLE_ALREADY_EXIST } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_Title = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                TitleID: values.TitleID
            };
            let result = await Title_Management.findOne(query).lean().exec();
            if (result === null) {
                reject({ success: false, extras: { msg: ApiMessages.INVALID_TITLE } })
            } else {
                resolve(result);
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_For_Store_BranchID = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BranchID: values.BranchID
            };
            let result = await Store_Branch.findOne(query).lean().exec();
            if (result === null) {
                reject({ success: false, extras: { msg: ApiMessages.INVALID_BRANCH } })
            } else {
                resolve(result);
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_StoreID = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let queryProduct = {
                StoreID: values.StoreID
            };
            let resultProduct = await Store_Branch.findOne(queryProduct).lean().exec();
            if (resultProduct === null) {
                resolve("Validated Successfully");
            } else {
                reject({ success: false, extras: { msg: ApiMessages.STORE_ALREADY_EXIST } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_For_Unique_Product = (Unique_ProductID) => {
    return new Promise(async (resolve, reject) => {
        try {
            let queryProduct = {
                Unique_ProductID: Unique_ProductID.toUpperCase()
            };
            let resultProduct = await Store_Products.findOne(queryProduct).lean().exec();
            if (resultProduct === null) {
                resolve("Validated Successfully");
            } else {
                reject({ success: false, extras: { msg: ApiMessages.PRODUCT_ALREADY_EXIST } })
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Common_Date_Validation = (date) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (moment(date, config.Take_Date_Format).isValid()) {
                resolve("Validated Successfully");
            } else {
                reject({ success: false, extras: { code: 2, msg: ApiMessages.INVALID_DATE } });
            }
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_Branch = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BranchID: values.BranchID
            };
            let Result = await Store_Branch.findOne(query).select('-_id -__v').lean();
            if (Result === null) {
                reject({ success: false, extras: { code: 2, msg: ApiMessages.INVALID_Branch } })
            } else {
                resolve(Result);
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_For_Buyer_Cart_And_Stack_Processing_For_OutofStock = (values, CartData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                async.eachSeries(CartData, async (item, callback) => {
                    try {
                        if (item.Avaiable_Quantity <= 0) {
                            let Pquery = {
                                ProductID: item.ProductID,
                                BuyerID: values.BuyerID,
                            }
                            let Presult = await Stack_Log.findOne(Pquery).lean().exec()
                            // let Pqueryy = {
                            //     ProductID: item.ProductID,
                            // }
                            // let Presultt = await Stack_Log.findOne(Pqueryy).lean().exec()
                            if (Presult == null) {
                                let Data = {
                                    StackID: uuid.v4(),
                                    ProductID: item.ProductID,
                                    Unique_ProductID: item.Unique_ProductID,
                                    Product_Name: item.Product_Name,
                                    Format_of_the_Book: item.Format_of_the_Book,
                                    CountryID: item.CountryID,
                                    CityID: item.CityID,
                                    ZoneID: item.ZoneID,
                                    StoreID: item.StoreID,
                                    Branch_Name: item.Branch_Name,
                                    BranchID: item.BranchID,
                                    Product_Quantity: item.Product_Quantity,
                                    BuyerID: values.BuyerID,
                                    Status: true,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                };
                                let SaveStack = await Stack_Log(Data).save();
                                // pull item from cart
                                let BQuery = {
                                    BuyerID: values.BuyerID,
                                }
                                let BuyerData = await Buyers.findOne(BQuery).lean().exec();
                                let Qty_Count = 0;
                                let PDatax = BuyerData.Cart_Information.filter(function (ResultTemp) {
                                    if (ResultTemp.ProductID == item.ProductID) {
                                        Qty_Count = ResultTemp.Product_Quantity
                                    }
                                });
                                let changespull = {
                                    $pull: {
                                        Cart_Information: {
                                            ProductID: item.ProductID
                                        }
                                    },
                                    $inc: {
                                        Cart_Total_Items: Qty_Count * -1
                                    }
                                }
                                let ResultPull = await Buyers.updateOne(BQuery, changespull).lean().exec();
                                // Store Product for each branch

                                // if (Presultt == null) {
                                //     let ProductData = await Store_Products.findOne(Pqueryy).lean().exec()
                                //     let Product_Data_For_Branch = ProductData.Product_Data_For_Branch;
                                //     let BranchStackProcessing = await CommonController.Add_Product_To_Stack_For_Each_Branch(Product_Data_For_Branch, item.ProductID);
                                // }
                            }
                            // Send notification to Branches
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Successfully Updated");
                });

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

// CommonController.Add_Product_To_Stack_For_Each_Branch = (Product_Data_For_Branch, ProductID) => {
//     return new Promise((resolve, reject) => {
//         setImmediate(async () => {
//             try {
//                 async.eachSeries(Product_Data_For_Branch, async (item, callback) => {
//                     try {

//                         let Data = {
//                             BranchStackID: uuid.v4(),
//                             ProductID: ProductID,
//                             BranchID: item.BranchID,
//                             Status: true,
//                             created_at: new Date(),
//                             updated_at: new Date()
//                         };
//                         let SaveBranchStack = await Branch_Stack_Log(Data).save();
//                         callback();
//                     } catch (error) {
//                         callback(error);
//                     }
//                 }, async (err) => {
//                     if (err) reject(err);
//                     resolve("Process completed")
//                 });
//             } catch (error) {
//                 reject(await CommonController.Common_Error_Handler(error));
//             }
//         });
//     });
// }

CommonController.Check_for_User_Discount_Apply = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerDiscountID: values.BuyerDiscountID
                };
                let Result = await User_Discount_Apply.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_USER_DISCOUNT } });
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Special_Offer = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    SpecialOfferID: values.SpecialOfferID
                };
                let Result = await Special_Offers.findOne(query).select('-_id -__v').lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_SPECAIL_OFFER } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Automated_Offer = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    AutomatedOfferID: values.AutomatedOfferID
                };
                let Result = await App_Automated_Offers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_AUTOMATED_OFFER_ID } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Discount = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DiscountID: values.DiscountID
                };
                let Result = await Discounts.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DISCOUNT } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation_Total_Jouney_Time_Distance = (TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Total_Distance = 0;
                let Total_Duration = 0;
                let All_Trip_Orders_Data = await Trips_Orders_Routings.find({ TripID: TripData.TripID }).sort({ Route_Number: 1 }).lean();
                let xlat = await All_Trip_Orders_Data[0].lat;
                let xlng = await All_Trip_Orders_Data[0].lng;
                async.eachSeries(All_Trip_Orders_Data, async (TripRouteOrderData, callback) => {
                    try {
                        let ylat = TripRouteOrderData.lat;
                        let ylng = TripRouteOrderData.lng;
                        let DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(xlat, xlng, ylat, ylng);
                        let Distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                        let Duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
                        Distance /= 1000;//mtrs to Kms
                        Duration *= 1000;//seconds to milliseconds
                        Total_Distance += Distance;
                        Total_Duration += Duration;
                        if (Distance > 0 && Duration > 0) {
                            xlat = ylat;
                            xlng = ylng;
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    let query = {
                        TripID: TripData.TripID
                    };
                    let changes = {
                        $set: {
                            Total_Distance: Total_Distance,
                            Total_Duration: Total_Duration,
                            updated_at: new Date()
                        }
                    };
                    let UpdatedStatus = await Trips.updateOne(query, changes).lean();
                    resolve("Updated Successfully")
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Update_Route_Trips_Route_Number = (Final_Divided_Orders_Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                var Route_Number = 1;
                async.eachSeries(Final_Divided_Orders_Data, async (TripRouteOrdersData, callback) => {
                    try {
                        let query = {
                            TripRouteID: TripRouteOrdersData.TripRouteID
                        };
                        let changes = {
                            $set: {
                                Route_Number: Route_Number,
                                updated_at: new Date()
                            }
                        };
                        let UpdatedStatus = await Trips_Orders_Routings.updateOne(query, changes).lean();
                        Route_Number++;
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Routes Updated Successfully")
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


CommonController.Queue_Fulfilled_Trips_Driver_Routing_Functionality_Update_Available_Queue = (Nearest_Order_Data, Final_Sorted_Orders_Data, Final_Divided_Orders_Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Nearest_Order_Data.Route_Action_Next == 1 || Nearest_Order_Data.Route_Action_Next == 3) {
                    if (Nearest_Order_Data.Sub_Trip_Type == 1 || Nearest_Order_Data.Sub_Trip_Type == 2) {
                        //SP-SD and SP-MD
                        //Make Drop Available
                        async.eachSeries(Nearest_Order_Data.All_Orders_ID_Array, async (OrderID, callback) => {
                            try {
                                let MatchedData = await Final_Sorted_Orders_Data.filter(item => (item.All_Orders_ID_Array.indexOf(OrderID) >= 0));
                                let UnMatchedData = await Final_Sorted_Orders_Data.filter(item => (item.All_Orders_ID_Array.indexOf(OrderID) < 0));
                                await MatchedData.forEach(item => item.Whether_to_Process_Route = true);
                                Final_Sorted_Orders_Data = await MatchedData.concat(UnMatchedData);
                                callback();
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            resolve(Final_Sorted_Orders_Data);
                        });
                    } else if (Nearest_Order_Data.Sub_Trip_Type == 3) {
                        //MP-SD
                        let MapOrderID = Nearest_Order_Data.All_Orders_ID_Array[0];
                        let MatchedData = await Final_Sorted_Orders_Data.filter(item => (item.All_Orders_ID_Array.indexOf(MapOrderID) >= 0));
                        let UnMatchedData = await Final_Sorted_Orders_Data.filter(item => (item.All_Orders_ID_Array.indexOf(MapOrderID) < 0));
                        let MappingGroupDropData = await MatchedData[0];
                        async.eachSeries(MappingGroupDropData.All_Orders_ID_Array, async (OrderID, callback) => {
                            try {
                                let Whether_Routed_Data = await Final_Divided_Orders_Data.find(item => (item.All_Orders_ID_Array.indexOf(OrderID) >= 0));
                                if (Whether_Routed_Data == null) {
                                    callback("one of the order is not picked");
                                } else {
                                    callback();
                                }
                            } catch (error) {
                                callback(error);
                            }
                        }, async (err) => {
                            if (err) {
                                resolve(Final_Sorted_Orders_Data);
                            } else {
                                await MatchedData.forEach(item => item.Whether_to_Process_Route = true);
                                Final_Sorted_Orders_Data = await MatchedData.concat(UnMatchedData);
                                resolve(Final_Sorted_Orders_Data);
                            }
                        });
                    }
                } else if (Nearest_Order_Data.Route_Action_Next == 2 || Nearest_Order_Data.Route_Action_Next == 4) {
                    //Drop
                    resolve(Final_Sorted_Orders_Data);
                } else {
                    //Currently Not Available
                    resolve(Final_Sorted_Orders_Data);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Queue_Fulfilled_Trips_Driver_Routing_Functionality = (Final_Divided_Orders_Data, All_Divided_Orders_Data_Array, Location) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                //Location must be in {lat: , lng:} 
                if (All_Divided_Orders_Data_Array.length > 0) {
                    let origins = await Location;
                    let Final_Sorted_Orders_Data = await GeoDistance.Single_Origin_Multiple_Distance_Trip_Routing_with_Sort(origins, All_Divided_Orders_Data_Array);
                    let Nearest_Order_Data = Final_Sorted_Orders_Data[0];
                    await Final_Divided_Orders_Data.push(Nearest_Order_Data);
                    await Final_Sorted_Orders_Data.splice(0, 1);
                    Final_Sorted_Orders_Data = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Functionality_Update_Available_Queue(Nearest_Order_Data, Final_Sorted_Orders_Data, Final_Divided_Orders_Data);
                    let final_last_index = Final_Divided_Orders_Data.length - 1;
                    let send_Location = Final_Divided_Orders_Data[final_last_index];
                    Final_Divided_Orders_Data = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Functionality(Final_Divided_Orders_Data, Final_Sorted_Orders_Data, send_Location);
                    resolve(Final_Divided_Orders_Data);
                } else {
                    resolve(Final_Divided_Orders_Data);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation_Update_Pickup_Orders_Available = (All_Divided_Orders_Data_Array) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                await All_Divided_Orders_Data_Array.forEach(item => (item.Whether_to_Process_Route = false, item.geo_distance = 0));
                let Only_Pickups_All_Divided_Orders_Data_Array = await All_Divided_Orders_Data_Array.filter(item => (item.Route_Action_Next == 1 || item.Route_Action_Next == 3));
                All_Divided_Orders_Data_Array = await All_Divided_Orders_Data_Array.filter(item => (item.Route_Action_Next !== 1 && item.Route_Action_Next !== 3));
                await Only_Pickups_All_Divided_Orders_Data_Array.forEach(item => item.Whether_to_Process_Route = true);
                All_Divided_Orders_Data_Array = await All_Divided_Orders_Data_Array.concat(Only_Pickups_All_Divided_Orders_Data_Array);
                All_Divided_Orders_Data_Array = await All_Divided_Orders_Data_Array.sort((a, b) => b.Whether_to_Process_Route - a.Whether_to_Process_Route || a.created_at - b.created_at);
                resolve(All_Divided_Orders_Data_Array);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation = (TripData, Location) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let torquery = {
                    TripID: TripData.TripID,
                    Status: true,
                    Route_Action_Completed: 0,
                    Route_Status: {
                        $in: [1]
                    }
                };
                let All_Divided_Orders_Data_Array = await Trips_Orders_Routings.find(torquery).lean();
                All_Divided_Orders_Data_Array = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation_Update_Pickup_Orders_Available(All_Divided_Orders_Data_Array);
                if (Location == undefined || Location == null) {
                    Location = All_Divided_Orders_Data_Array[0];
                }
                let Final_Divided_Orders_Data = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Functionality([], All_Divided_Orders_Data_Array, Location);
                let Update_Route_Trips_Route_Number = await CommonController.Update_Route_Trips_Route_Number(Final_Divided_Orders_Data);
                let Update_Trip_Journey_Time_Distance = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Implementation_Total_Jouney_Time_Distance(TripData);
                let query = {
                    TripID: TripData.TripID
                };
                let changes = {
                    $set: {
                        Whether_Trip_Routing_Available: true
                    }
                };
                let UpdatedStatus = await Trips.updateOne(query, changes).lean();
                resolve("Trip Routing and status Updated Successfully");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Store_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CustomerID: values.StoreAdminID
                };
                let Result = await Customers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.Store_Admin_Not_Found } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Order = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ORDER } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.OrderData_Trip_Creation_Instant_Processing = (OrderData, InstantRequestData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let No_of_Groups = 1;
                let No_of_Routes = 1;
                let No_of_Orders = 1;
                let No_of_Pickups = 1;
                let No_of_Drops = 1;
                let Total_Distance = 1;
                let Total_Duration = 1;
                Total_Distance = await InstantRequestData.Total_Distance;
                Total_Duration = await InstantRequestData.Total_Duration_Minutes;
                let Vehicle_Data = await CommonController.Check_for_Vehicle(DriverData);
                let All_Driver_Logs = {
                    LogID: uuid.v4(),
                    DriverID: DriverData.DriverID,
                    VehicleID: Vehicle_Data.VehicleID,
                    VehicleNumber: DriverData.VehicleNumber,
                    Time: new Date()
                }
                let TripSaving = {
                    TripID: uuid.v4(),
                    TripNumber: await CommonController.GENERATE_TEN_DIGIT_INCREMENT_COUNTER_SEQUENCE('DRIVER-TRIP', 'TRIP'),
                    OrderID: OrderData.OrderID,
                    BranchID: OrderData.Branch_Information.BranchID,
                    Branch_Name: OrderData.Branch_Information.Branch_Name,
                    Branch_PhoneNumber: OrderData.Branch_Information.Branch_PhoneNumber,
                    Branch_Address: OrderData.Branch_Information.Address,
                    DriverID: DriverData.DriverID,
                    VehicleID: DriverData.VehicleID,
                    VehicleNumber: DriverData.VehicleNumber,
                    All_Driver_Logs: All_Driver_Logs,
                    CountryID: DriverData.CountryID,
                    CityID: DriverData.CityID,
                    ZoneID: DriverData.ZoneID,
                    Service_Type: DriverData.Service_Type,
                    Whether_Instant_Trip: true,
                    InstantOrderRequestID: InstantRequestData.InstantOrderRequestID,
                    No_of_Groups: No_of_Groups,
                    No_of_Routes: No_of_Routes,
                    No_of_Orders: No_of_Orders,
                    No_of_Pickups: No_of_Pickups,
                    No_of_Drops: No_of_Drops,
                    Total_Eta: Total_Duration,
                    Total_Distance: Total_Distance,
                    Total_Duration: Total_Duration,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let TripData = await Trips(TripSaving).save();
                TripData = await JSON.parse(JSON.stringify(TripData));
                let SubTripSaving = {
                    Sub_Trip_ID: uuid.v4(),
                    TripID: TripData.TripID,
                    TripNumber: TripData.TripNumber,
                    Group_Number: 1,
                    Number_of_Orders: No_of_Orders,
                    Sub_Trip_Type: 1,
                    USERID: OrderData.BuyerID,
                    DriverID: TripData.DriverID,
                    VehicleID: TripData.VehicleID,
                    VehicleNumber: TripData.VehicleNumber,
                    CountryID: TripData.CountryID,
                    CityID: TripData.CityID,
                    Service_Type: TripData.Service_Type,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                let SubTripData = await Sub_Trips(SubTripSaving).save();
                SubTripData = await JSON.parse(JSON.stringify(SubTripData));
                let All_Orders_Data = await Buyer_Orders.find({ OrderID: OrderData.OrderID }).lean();

                async.eachSeries(All_Orders_Data, async (OrderDat, callback) => {
                    try {
                        if (OrderDat.Order_Status == 11) {
                            let Cancellation_Pickup_Status = 2
                            let Return_Driver_Logs = {
                                LogID: uuid.v4(),
                                TripID: TripData.TripID,
                                DriverID: TripData.DriverID,
                                Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                                Time: new Date()
                            };
                            let Address = OrderDat.Delivery_Address_Information.Name + ", "
                                + OrderDat.Delivery_Address_Information.Flat_No + ", "
                                + OrderDat.Delivery_Address_Information.Plot_No + ", "
                                + OrderDat.Delivery_Address_Information.City + ", "
                                + OrderDat.Delivery_Address_Information.State + ", "
                                + "Postal Code: " + OrderDat.Delivery_Address_Information.Postal_Code + ", "
                                + "PhoneNumber: " + OrderDat.Delivery_Address_Information.Postal_Code + ", "
                            let Return_Event_Logs = {
                                LogID: uuid.v4(),
                                DriverID: TripData.DriverID,
                                Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                                Comments: "Order Queued",
                                Address: Address,
                                lat: OrderDat.Delivery_Address_Information.Latitude,
                                lng: OrderDat.Delivery_Address_Information.Longitude,
                                Time: new Date()
                            };
                            let oquery = {
                                OrderID: OrderDat.OrderID
                            };
                            let ochanges = {
                                $set: {
                                    Whether_Driver_Available: true,
                                    DriverID: TripData.DriverID,
                                    Cancellation_Pickup_Status: Cancellation_Pickup_Status,
                                    updated_at: new Date()
                                },
                                $push: {
                                    Return_Driver_Logs: Return_Driver_Logs,
                                    Return_Event_Logs: Return_Event_Logs
                                }
                            };
                            let oUpdatedStatusx = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                            let SubTripOrderSaving = {
                                Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                TripID: TripData.TripID,
                                TripNumber: TripData.TripNumber,
                                Group_Number: 1,
                                Sub_Trip_Type: 1,
                                DriverID: TripData.DriverID,
                                VehicleID: TripData.VehicleID,
                                VehicleNumber: TripData.VehicleNumber,
                                CountryID: TripData.CountryID,
                                CityID: TripData.CityID,
                                Service_Type: TripData.Service_Type,
                                USERID: OrderData.BuyerID,
                                OrderID: OrderDat.OrderID,
                                Order_Number: OrderDat.Order_Number,
                                created_at: new Date(),
                                updated_at: new Date()
                            };
                            let SubTripOrderData = await Sub_Trips_Orders(SubTripOrderSaving).save();
                        } else {
                            let Order_Status = 2;
                            let Driver_Logs = {
                                LogID: uuid.v4(),
                                TripID: TripData.TripID,
                                DriverID: TripData.DriverID,
                                Order_Status: Order_Status,
                                Time: new Date()
                            };
                            let Event_Logs = {
                                LogID: uuid.v4(),
                                DriverID: TripData.DriverID,
                                Order_Status: Order_Status,
                                Comments: "Order Queued",
                                Address: OrderDat.Branch_Information.Address,
                                lat: OrderDat.Branch_Information.Latitude,
                                lng: OrderDat.Branch_Information.Longitude,
                                Time: new Date()
                            };
                            let oquery = {
                                OrderID: OrderDat.OrderID
                            };
                            let ochanges = {
                                $set: {
                                    Whether_Driver_Available: true,
                                    DriverID: TripData.DriverID,
                                    Order_Status: Order_Status,
                                    updated_at: new Date()
                                },
                                $push: {
                                    Driver_Logs: Driver_Logs,
                                    Event_Logs: Event_Logs
                                }
                            };
                            let oUpdatedStatus = await Buyer_Orders.updateOne(oquery, ochanges).lean();
                            let SubTripOrderSaving = {
                                Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                TripID: TripData.TripID,
                                TripNumber: TripData.TripNumber,
                                Group_Number: 1,
                                Sub_Trip_Type: 1,
                                DriverID: TripData.DriverID,
                                VehicleID: TripData.VehicleID,
                                VehicleNumber: TripData.VehicleNumber,
                                CountryID: TripData.CountryID,
                                CityID: TripData.CityID,
                                Service_Type: TripData.Service_Type,
                                USERID: OrderData.BuyerID,
                                OrderID: OrderDat.OrderID,
                                Order_Number: OrderDat.Order_Number,
                                created_at: new Date(),
                                updated_at: new Date()
                            };
                            let SubTripOrderData = await Sub_Trips_Orders(SubTripOrderSaving).save();
                        }
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    let RouteFunctionality = await CommonController.OrderData_Trip_Creation_Instant_Processing_Route_Processing(OrderData, All_Orders_Data, TripData, SubTripData);
                    //let MainOrderStatusTripped = await CommonController.Common_Main_Order_Status_Updation(MainOrderData, 3);
                    resolve(TripData);
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.OrderData_Trip_Creation_Instant_Processing_Route_Processing = (OrderData, All_Orders_Data, TripData, SubTripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // let SinglePickupMultipleDeliveryProcessing = await CronController.Queue_Fulfilled_Trips_Driver_Routing_Creation_Single_Pickup_Multiple_Delivery_Processing(TripData);
                // let MultiplePickupSingleDeliveryProcessing = await CronController.Queue_Fulfilled_Trips_Driver_Routing_Creation_Multiple_Pickup_Single_Delivery_Processing(TripData);
                let SinglePickupSingleDeliveryProcessing = await CommonController.Queue_Fulfilled_Trips_Driver_Routing_Creation_Single_Pickup_Single_Delivery_Processing(TripData);
                // let All_Trip_Orders_Routing = await CronController.Queue_Fulfilled_Trips_Driver_Routing_Implementation(TripData);
                resolve("Route Processing Completed");
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Queue_Fulfilled_Trips_Driver_Routing_Creation_Single_Pickup_Single_Delivery_Processing = (TripData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let subtripquery = {
                    TripID: TripData.TripID,
                    Sub_Trip_Type: 1,//Single Pickup Single Drop
                    Number_of_Orders: {
                        $gt: 0
                    },
                    Whether_Routed_Created: false,
                    Status: true
                };
                let All_Sub_Trips_Data = await Sub_Trips.find(subtripquery).lean();
                async.eachSeries(All_Sub_Trips_Data, async (SubTripData, callback) => {
                    try {
                        let subtriporderquery = {
                            Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                            Status: true
                        };
                        let All_Orders_ID_Array = await Sub_Trips_Orders.distinct('OrderID', subtriporderquery).lean();
                        let All_Orders_Data = await Buyer_Orders.find({ OrderID: { $in: All_Orders_ID_Array }, Status: true }).lean();
                        let All_Order_Single_Pickup_Trip_Routes = await CommonController.Common_Storing_Trip_Routing_Single_Pickup(TripData, SubTripData, All_Orders_Data);
                        let All_Order_Single_Drop_Trip_Routes = await CommonController.Common_Storing_Trip_Routing_Single_Drop(TripData, SubTripData, All_Orders_Data);
                        let strquery = {
                            Sub_Trip_ID: SubTripData.Sub_Trip_ID
                        };
                        let strchanges = {
                            Whether_Routed_Created: true
                        };
                        let strUpdatedStatus = await Sub_Trips.updateOne(strquery, strchanges).lean();
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Route Creation Completed")
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Storing_Trip_Routing_Single_Drop = (TripData, SubTripData, All_Orders_Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                async.eachSeries(All_Orders_Data, async (OrderData, callback) => {
                    try {
                        let OTP = await CommonController.Random_OTP_Number();
                        let Route_Number = await CommonController.Common_Find_Next_Trip_Route_Number(TripData.TripID);
                        let Mapping_Information = []
                        async.eachSeries(OrderData.Cart_Information, async (item, callbackx) => {
                            try {
                                let Item_Information = {
                                    ItemID: item.ProductID,
                                    Item_Name: item.Product_Name,
                                    Item_Description: "Book with Quantity of " + item.Product_Quantity,
                                    Item_Price: item.Total_Final_Price,
                                    Image_Available: true,
                                    Image_Data: item.Product_Image_Data,
                                }
                                if (OrderData.Order_Status == 11) {
                                    let Mapping_Informationx = {
                                        OrderID: OrderData.OrderID,
                                        Order_Number: OrderData.Order_Number,
                                        MainOrderID: "", //OrderData.MainOrderID,
                                        Main_Order_Number: "", //OrderData.Main_Order_Number,
                                        Name: OrderData.Delivery_Address_Information.Name,
                                        CountryCode: "+91",//OrderData.Delivery_Address_Information.CountryCode,
                                        PhoneNumber: OrderData.Delivery_Address_Information.PhoneNumber,
                                        Flat_Details: OrderData.Delivery_Address_Information.Flat_No,
                                        Address: OrderData.Delivery_Address_Information.City + ", " + OrderData.Delivery_Address_Information.State + ", " + OrderData.Delivery_Address_Information.Postal_Code,
                                        Landmark: OrderData.Delivery_Address_Information.Land_Mark,
                                        lat: OrderData.Delivery_Address_Information.Latitude,
                                        lng: OrderData.Delivery_Address_Information.Longitude,
                                        Item_Information: Item_Information
                                    };
                                    Mapping_Information.push(Mapping_Informationx);
                                } else {
                                    let Mapping_Informationx = {
                                        OrderID: OrderData.OrderID,
                                        Order_Number: OrderData.Order_Number,
                                        MainOrderID: "", //OrderData.MainOrderID,
                                        Main_Order_Number: "", //OrderData.Main_Order_Number,
                                        Name: OrderData.Branch_Information.Branch_Name,
                                        CountryCode: "+91", //OrderData.Branch_Information.CountryCode,
                                        PhoneNumber: OrderData.Branch_Information.Branch_PhoneNumber,
                                        Flat_Details: "", //OrderData.Branch_Information.Flat_Details,
                                        Address: OrderData.Branch_Information.Address,
                                        Landmark: "", //OrderData.Branch_Information.Landmark,
                                        lat: OrderData.Branch_Information.Latitude,
                                        lng: OrderData.Branch_Information.Longitude,
                                        Item_Information: Item_Information
                                    };
                                    Mapping_Information.push(Mapping_Informationx);
                                }
                                callbackx();
                            } catch (error) {
                                callbackx(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            let Route_Status = 1;
                            let Route_Status_Comment = '';
                            let Route_Status_Logs = {
                                LogID: uuid.v4(),
                                Route_Status: Route_Status,
                                Route_Status_Comment: Route_Status_Comment,
                                Time: new Date()
                            };
                            let Route_Action_Next = 2;//Single Drop
                            let Total_Final_Delivery_Price = parseFloat(OrderData.Order_Invoice.Order_Delivery_Charge);
                            if (OrderData.Order_Status == 11) {
                                let Storing_Trip_Route_Data = {
                                    TripRouteID: uuid.v4(),
                                    TripID: TripData.TripID,
                                    TripNumber: TripData.TripNumber,
                                    Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                    Sub_Trip_Type: SubTripData.Sub_Trip_Type,
                                    Group_Number: SubTripData.Group_Number,
                                    DriverID: TripData.DriverID,
                                    VehicleID: TripData.VehicleID,
                                    VehicleNumber: TripData.VehicleNumber,
                                    CountryID: TripData.CountryID,
                                    CityID: TripData.CityID,
                                    Service_Type: TripData.Service_Type,
                                    USERID: SubTripData.USERID,
                                    All_Orders_ID_Array: OrderData.OrderID,
                                    All_Orders_Number_Array: OrderData.Order_Number,
                                    Route_Action_Next: Route_Action_Next,
                                    Route_Status: Route_Status,
                                    Route_Status_Comment: Route_Status_Comment,
                                    Route_Status_Logs: Route_Status_Logs,
                                    Route_Number: Route_Number,
                                    OTP: OTP,
                                    Name: OrderData.Branch_Information.Branch_Name,
                                    CountryCode: "+91", //OrderData.Branch_Information.CountryCode,
                                    PhoneNumber: OrderData.Branch_Information.Branch_PhoneNumber,
                                    Flat_Details: "", //OrderData.Branch_Information.Flat_Details,
                                    Address: OrderData.Branch_Information.Address,
                                    Landmark: "", //OrderData.Branch_Information.Landmark,
                                    lat: OrderData.Branch_Information.Latitude,
                                    lng: OrderData.Branch_Information.Longitude,
                                    Point: [OrderData.Branch_Information.Latitude, OrderData.Branch_Information.Longitude],
                                    Mapping_Information: Mapping_Information,
                                    Order_Number: OrderData.Order_Number,
                                    No_of_Orders: 1,
                                    Total_Item_Gross_Weight: "", //OrderData.Item_Information.Item_Gross_Weight,
                                    Total_Orders_Volume_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 1 : 0,
                                    Total_Orders_Volume_Not_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 0 : 1,
                                    Total_Available_Item_Gross_Volume: "", //OrderData.Item_Information.Dimensions.Item_Volume,
                                    Total_Final_Delivery_Price: Total_Final_Delivery_Price,
                                    Status: true,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                };
                                let SaveResult = await Trips_Orders_Routings(Storing_Trip_Route_Data).save();
                            } else {
                                let Storing_Trip_Route_Data = {
                                    TripRouteID: uuid.v4(),
                                    TripID: TripData.TripID,
                                    TripNumber: TripData.TripNumber,
                                    Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                    Sub_Trip_Type: SubTripData.Sub_Trip_Type,
                                    Group_Number: SubTripData.Group_Number,
                                    DriverID: TripData.DriverID,
                                    VehicleID: TripData.VehicleID,
                                    VehicleNumber: TripData.VehicleNumber,
                                    CountryID: TripData.CountryID,
                                    CityID: TripData.CityID,
                                    Service_Type: TripData.Service_Type,
                                    USERID: SubTripData.USERID,
                                    All_Orders_ID_Array: OrderData.OrderID,
                                    All_Orders_Number_Array: OrderData.Order_Number,
                                    Route_Action_Next: Route_Action_Next,
                                    Route_Status: Route_Status,
                                    Route_Status_Comment: Route_Status_Comment,
                                    Route_Status_Logs: Route_Status_Logs,
                                    Route_Number: Route_Number,
                                    OTP: OTP,
                                    Name: OrderData.Delivery_Address_Information.Name,
                                    CountryCode: "+91",//OrderData.Delivery_Address_Information.CountryCode,
                                    PhoneNumber: OrderData.Delivery_Address_Information.PhoneNumber,
                                    Flat_Details: OrderData.Delivery_Address_Information.Flat_No,
                                    Address: OrderData.Delivery_Address_Information.City + ", " + OrderData.Delivery_Address_Information.State + ", " + OrderData.Delivery_Address_Information.Postal_Code,
                                    Landmark: OrderData.Delivery_Address_Information.Land_Mark,
                                    lat: OrderData.Delivery_Address_Information.Latitude,
                                    lng: OrderData.Delivery_Address_Information.Longitude,
                                    Point: [OrderData.Delivery_Address_Information.Latitude, OrderData.Delivery_Address_Information.Longitude],
                                    Mapping_Information: Mapping_Information,
                                    Order_Number: OrderData.Order_Number,
                                    No_of_Orders: 1,
                                    Total_Item_Gross_Weight: "", //OrderData.Item_Information.Item_Gross_Weight,
                                    Total_Orders_Volume_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 1 : 0,
                                    Total_Orders_Volume_Not_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 0 : 1,
                                    Total_Available_Item_Gross_Volume: "", //OrderData.Item_Information.Dimensions.Item_Volume,
                                    Total_Final_Delivery_Price: Total_Final_Delivery_Price,
                                    Status: true,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                };
                                let SaveResult = await Trips_Orders_Routings(Storing_Trip_Route_Data).save();
                            }
                        });
                        callback();
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Single Drop Routes Created Successfully");
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Find_Next_Trip_Route_Number = (TripID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    TripID: TripID
                };
                let fndupdchanges = {
                    $set: {
                        updated_at: new Date()
                    },
                    $inc: {
                        No_of_Routes: 1
                    }
                };
                let fndupdoptions = {
                    new: true
                }
                let TripData = await Trips.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve(TripData.No_of_Routes);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}


CommonController.Common_Storing_Trip_Routing_Single_Pickup = (TripData, SubTripData, All_Orders_Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                async.eachSeries(All_Orders_Data, async (OrderData, callback) => {
                    try {
                        let OTP = await CommonController.Random_OTP_Number();
                        let Route_Number = await CommonController.Common_Find_Next_Trip_Route_Number(TripData.TripID);
                        let Mapping_Information = []
                        async.eachSeries(OrderData.Cart_Information, async (item, callbackx) => {
                            try {
                                let Item_Information = {
                                    ItemID: item.ProductID,
                                    Item_Name: item.Product_Name,
                                    Item_Description: "Book with Quantity of " + item.Product_Quantity,
                                    Item_Price: item.Total_Final_Price,
                                    Image_Available: true,
                                    Image_Data: item.Product_Image_Data,
                                }
                                if (OrderData.Order_Status == 11) {
                                    let Mapping_Informationx = {
                                        OrderID: OrderData.OrderID,
                                        Order_Number: OrderData.Order_Number,
                                        MainOrderID: "", //OrderData.MainOrderID,
                                        Main_Order_Number: "", //OrderData.Main_Order_Number,
                                        Name: OrderData.Branch_Information.Branch_Name,
                                        CountryCode: "+91", //OrderData.Branch_Information.CountryCode,
                                        PhoneNumber: OrderData.Branch_Information.Branch_PhoneNumber,
                                        Flat_Details: "", //OrderData.Branch_Information.Flat_Details,
                                        Address: OrderData.Branch_Information.Address,
                                        Landmark: "", //OrderData.Branch_Information.Landmark,
                                        lat: OrderData.Branch_Information.Latitude,
                                        lng: OrderData.Branch_Information.Longitude,
                                        Item_Information: Item_Information
                                    };
                                    Mapping_Information.push(Mapping_Informationx);
                                } else {
                                    let Mapping_Informationx = {
                                        OrderID: OrderData.OrderID,
                                        Order_Number: OrderData.Order_Number,
                                        MainOrderID: "", //OrderData.MainOrderID,
                                        Main_Order_Number: "", //OrderData.Main_Order_Number,
                                        Name: OrderData.Delivery_Address_Information.Name,
                                        CountryCode: "+91",//OrderData.Delivery_Address_Information.CountryCode,
                                        PhoneNumber: OrderData.Delivery_Address_Information.PhoneNumber,
                                        Flat_Details: OrderData.Delivery_Address_Information.Flat_No,
                                        Address: OrderData.Delivery_Address_Information.City + ", " + OrderData.Delivery_Address_Information.State + ", " + OrderData.Delivery_Address_Information.Postal_Code,
                                        Landmark: OrderData.Delivery_Address_Information.Land_Mark,
                                        lat: OrderData.Delivery_Address_Information.Latitude,
                                        lng: OrderData.Delivery_Address_Information.Longitude,
                                        Item_Information: Item_Information
                                    };
                                    Mapping_Information.push(Mapping_Informationx);
                                }
                                callbackx();
                            } catch (error) {
                                callbackx(error);
                            }
                        }, async (err) => {
                            if (err) reject(err);
                            let Route_Status = 1;
                            let Route_Status_Comment = '';
                            let Route_Status_Logs = {
                                LogID: uuid.v4(),
                                Route_Status: Route_Status,
                                Route_Status_Comment: Route_Status_Comment,
                                Time: new Date()
                            };
                            let Route_Action_Next = 1;//Single Pickup
                            let Total_Final_Delivery_Price = parseFloat(OrderData.Order_Invoice.Order_Delivery_Charge);
                            if (OrderData.Order_Status == 11) {
                                let Storing_Trip_Route_Data = {
                                    TripRouteID: uuid.v4(),
                                    TripID: TripData.TripID,
                                    TripNumber: TripData.TripNumber,
                                    Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                    Sub_Trip_Type: SubTripData.Sub_Trip_Type,
                                    Group_Number: SubTripData.Group_Number,
                                    DriverID: TripData.DriverID,
                                    VehicleID: TripData.VehicleID,
                                    VehicleNumber: TripData.VehicleNumber,
                                    CountryID: TripData.CountryID,
                                    CityID: TripData.CityID,
                                    Service_Type: TripData.Service_Type,
                                    USERID: SubTripData.USERID,
                                    All_Orders_ID_Array: OrderData.OrderID,
                                    All_Orders_Number_Array: OrderData.Order_Number,
                                    Route_Action_Next: Route_Action_Next,
                                    Route_Status: Route_Status,
                                    Route_Status_Comment: Route_Status_Comment,
                                    Route_Status_Logs: Route_Status_Logs,
                                    Route_Number: Route_Number,
                                    OTP: OTP,
                                    Name: OrderData.Delivery_Address_Information.Name,
                                    CountryCode: "+91",//OrderData.Delivery_Address_Information.CountryCode,
                                    PhoneNumber: OrderData.Delivery_Address_Information.PhoneNumber,
                                    Flat_Details: OrderData.Delivery_Address_Information.Flat_No,
                                    Address: OrderData.Delivery_Address_Information.City + ", " + OrderData.Delivery_Address_Information.State + ", " + OrderData.Delivery_Address_Information.Postal_Code,
                                    Landmark: OrderData.Delivery_Address_Information.Land_Mark,
                                    lat: OrderData.Delivery_Address_Information.Latitude,
                                    lng: OrderData.Delivery_Address_Information.Longitude,
                                    Point: [OrderData.Delivery_Address_Information.Latitude, OrderData.Delivery_Address_Information.Longitude],
                                    Mapping_Information: Mapping_Information,
                                    Order_Number: OrderData.Order_Number,
                                    No_of_Orders: 1,
                                    Total_Item_Gross_Weight: "", //OrderData.Item_Information.Item_Gross_Weight,
                                    Total_Orders_Volume_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 1 : 0,
                                    Total_Orders_Volume_Not_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 0 : 1,
                                    Total_Available_Item_Gross_Volume: "", //OrderData.Item_Information.Dimensions.Item_Volume,
                                    Total_Final_Delivery_Price: Total_Final_Delivery_Price,
                                    Status: true,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                };
                                let SaveResult = await Trips_Orders_Routings(Storing_Trip_Route_Data).save();
                            } else {
                                let Storing_Trip_Route_Data = {
                                    TripRouteID: uuid.v4(),
                                    TripID: TripData.TripID,
                                    TripNumber: TripData.TripNumber,
                                    Sub_Trip_ID: SubTripData.Sub_Trip_ID,
                                    Sub_Trip_Type: SubTripData.Sub_Trip_Type,
                                    Group_Number: SubTripData.Group_Number,
                                    DriverID: TripData.DriverID,
                                    VehicleID: TripData.VehicleID,
                                    VehicleNumber: TripData.VehicleNumber,
                                    CountryID: TripData.CountryID,
                                    CityID: TripData.CityID,
                                    Service_Type: TripData.Service_Type,
                                    USERID: SubTripData.USERID,
                                    All_Orders_ID_Array: OrderData.OrderID,
                                    All_Orders_Number_Array: OrderData.Order_Number,
                                    Route_Action_Next: Route_Action_Next,
                                    Route_Status: Route_Status,
                                    Route_Status_Comment: Route_Status_Comment,
                                    Route_Status_Logs: Route_Status_Logs,
                                    Route_Number: Route_Number,
                                    OTP: OTP,
                                    Name: OrderData.Branch_Information.Branch_Name,
                                    CountryCode: "+91", //OrderData.Branch_Information.CountryCode,
                                    PhoneNumber: OrderData.Branch_Information.Branch_PhoneNumber,
                                    Flat_Details: "", //OrderData.Branch_Information.Flat_Details,
                                    Address: OrderData.Branch_Information.Address,
                                    Landmark: "", //OrderData.Branch_Information.Landmark,
                                    lat: OrderData.Branch_Information.Latitude,
                                    lng: OrderData.Branch_Information.Longitude,
                                    Point: [OrderData.Branch_Information.Latitude, OrderData.Branch_Information.Longitude],
                                    Mapping_Information: Mapping_Information,
                                    Order_Number: OrderData.Order_Number,
                                    No_of_Orders: 1,
                                    Total_Item_Gross_Weight: "", //OrderData.Item_Information.Item_Gross_Weight,
                                    Total_Orders_Volume_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 1 : 0,
                                    Total_Orders_Volume_Not_Available: "", //(OrderData.Item_Information.Whether_Dimensions_Available) ? 0 : 1,
                                    Total_Available_Item_Gross_Volume: "", //OrderData.Item_Information.Dimensions.Item_Volume,
                                    Total_Final_Delivery_Price: Total_Final_Delivery_Price,
                                    Status: true,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                };
                                let SaveResult = await Trips_Orders_Routings(Storing_Trip_Route_Data).save();
                            }
                        });
                        callback();
                    } catch (error) {
                        callback(error);
                    }

                }, async (err) => {
                    if (err) reject(err);
                    resolve("Single Drop Routes Created Successfully");
                });
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.OrderData_Trip_Creation = (OrderData, InstantRequestData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let InstantProcessing = await CommonController.OrderData_Trip_Creation_Instant_Processing(OrderData, InstantRequestData, DriverData);
                let SendingPubnubMessage = await PubnubController.Send_Common_Instant_Request_Pubnub_Messaging_for_Common_Channel(InstantRequestData, 3);
                resolve(InstantProcessing);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Trip_Route = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    TripRouteID: values.TripRouteID
                };
                let Result = await Trips_Orders_Routings.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_TRIP_ROUTE } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Order_Creation_Order_Placing_Processing = (InstantRequestData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                // let q = {
                //     InstantOrderRequestID : InstantRequestData.InstantOrderRequestID
                // }
                // InstantRequestData = await Instant_Order_Request.findOne(q).lean.exec()
                let OrderData = InstantRequestData.Order_Data
                if (OrderData.Order_Status == 11) {
                    let query = {
                        OrderID: OrderData.OrderID
                    };
                    let Description = DriverData.Driver_Name + " is the delivery executive for this order with Phone Number: " + DriverData.Driver_Country_Code + "-" + DriverData.Driver_Phone_Number;
                    let changes = {
                        $set: {
                            Cancellation_Pickup_Status: 2,
                            Return_Driver_Information: {
                                Whether_Driver_Available: true,
                                DriverID: DriverData.DriverID,
                                Driver_Name: DriverData.Driver_Name,
                                Driver_Country_Code: DriverData.Driver_Country_Code,
                                Driver_Phone_Number: DriverData.Driver_Phone_Number,
                                VehicleID: DriverData.VehicleID,
                                VehicleNumber: DriverData.VehicleNumber,
                                Order_Accepted_Time: new Date(),
                            },
                            updated_at: new Date()
                        },
                        $push: {
                            Cancellation_Order_Pickup_Report: {
                                Title: "Driver Assigned",
                                Description: Description,
                                Time: new Date()
                            },
                        }
                    }
                    OrderData = await Buyer_Orders.findOneAndUpdate(query, changes).lean().exec();
                    let Processing = await CommonController.OrderData_Trip_Creation(OrderData, InstantRequestData, DriverData);
                    resolve(Processing);
                } else {
                    let query = {
                        OrderID: OrderData.OrderID
                    };
                    let Description = DriverData.Driver_Name + " is your Driver with Phone Number: " + DriverData.Driver_Country_Code + "-" + DriverData.Driver_Phone_Number;
                    let changes = {
                        $set: {
                            Order_Status: 4,
                            Driver_Information: {
                                Whether_Driver_Available: true,
                                DriverID: DriverData.DriverID,
                                Driver_Name: DriverData.Driver_Name,
                                Driver_Country_Code: DriverData.Driver_Country_Code,
                                Driver_Phone_Number: DriverData.Driver_Phone_Number,
                                VehicleID: DriverData.VehicleID,
                                VehicleNumber: DriverData.VehicleNumber,
                                Order_Accepted_Time: new Date(),
                            },
                            updated_at: new Date()
                        },
                        $push: {
                            Order_Report: {
                                Title: "Driver Assigned",
                                Description: Description,
                                Time: new Date()
                            },
                        }
                    }
                    OrderData = await Buyer_Orders.findOneAndUpdate(query, changes).lean().exec();
                    let Processing = await CommonController.OrderData_Trip_Creation(OrderData, InstantRequestData, DriverData);
                    resolve(Processing);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Branch_Bank_Beneficiary_Account = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BeneficiaryID: values.BeneficiaryID
            };
            let Result = await Branch_Bank_Beneficiary_Accounts.findOne(query).select('-_id -__v').lean();
            if (Result === null) {
                reject({ success: false, extras: { msg: ApiMessages.INVALID_BENEFICIARY_ACCOUNT } })
            } else {
                resolve(Result);
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_Driver_Bank_Beneficiary_Account = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                BeneficiaryID: values.BeneficiaryID
            };
            let Result = await Driver_Bank_Beneficiary_Accounts.findOne(query).select('-_id -__v').lean();
            if (Result === null) {
                reject({ success: false, extras: { msg: ApiMessages.INVALID_BENEFICIARY_ACCOUNT } })
            } else {
                resolve(Result);
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Validate_IFSC_Code = values => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (ifsc.validate(values.IFSC)) {
                    let Data = await ifsc.fetchDetails(values.IFSC);
                    resolve(Data);
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_IFSC_CODE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Instant_Order_Request = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    InstantOrderRequestID: values.InstantOrderRequestID
                };
                let Result = await Instant_Order_Request.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_INSTANT_ORDER_REQUEST } });
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Routing_Processing_With_Location = (OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let x1, y1, x2, y2;
                x1 = OrderData.Branch_Information.Latitude;
                y1 = OrderData.Branch_Information.Longitude;

                x2 = OrderData.Delivery_Address_Information.Latitude;
                y2 = OrderData.Delivery_Address_Information.Longitude;
                let DistanceMatrixData = new Object();

                DistanceMatrixData = await CommonController.Common_Road_Distance_Duration_Calculating_Function(x1, y1, x2, y2);
                let Trip_Route_Order_Data = new Object();
                // console.log(DistanceMatrixData.origin_addresses[0])
                Trip_Route_Order_Data.geo_origin_address = await DistanceMatrixData.origin_addresses[0];
                Trip_Route_Order_Data.geo_destination_address = await DistanceMatrixData.destination_addresses[0];
                Trip_Route_Order_Data.geo_distance = await DistanceMatrixData.rows[0].elements[0].distance.value;
                Trip_Route_Order_Data.geo_duration = await DistanceMatrixData.rows[0].elements[0].duration.value;
                Trip_Route_Order_Data.geo_distance /= 1000; //mtrs to Kms
                Trip_Route_Order_Data.geo_duration_minutes = (Trip_Route_Order_Data.geo_duration / 60);
                Trip_Route_Order_Data.geo_duration *= 1000; //seconds to milliseconds

                resolve(Trip_Route_Order_Data);

            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

CommonController.Google_Distance_Matrix_Error_Default_Data = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Data = {
                    destination_addresses: [
                        "Backend API Error, Backend Default Values, Hyderabad, India."
                    ],
                    origin_addresses: [
                        "Backend API Error, Backend Default Values, Hyderabad, India."
                    ],
                    rows: [
                        {
                            elements: [
                                {
                                    distance: {
                                        text: config.Default_Error_Google_API_Distance_Text,
                                        value: config.Default_Error_Google_API_Distance
                                    },
                                    duration: {
                                        text: config.Default_Error_Google_API_Duration_Text,
                                        value: config.Default_Error_Google_API_Duration
                                    },
                                    status: "OK"
                                }
                            ]
                        }
                    ],
                    status: "OK"
                };
                resolve(Data);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Road_Distance_Duration_Calculating_Function = (xlat, xlng, ylat, ylng) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let platlong = `${parseFloat(xlat)},${parseFloat(xlng)}`;
                let dlatlong = `${parseFloat(ylat)},${parseFloat(ylng)}`;
                var origins = [platlong.toString()];
                var destinations = [dlatlong.toString()];
                distance.matrix(origins, destinations, async (error, DistanceMatrixData) => {
                    if (error) {
                        console.error("Google Distance Matrix Error------>", error);
                        let Data = await CommonController.Google_Distance_Matrix_Error_Default_Data();
                        resolve(Data);
                    } else {
                        if (DistanceMatrixData.status == 'OK') {
                            resolve(DistanceMatrixData);
                        } else {
                            console.error("Google Distance Matrix Error------>", DistanceMatrixData);
                            let Data = await CommonController.Google_Distance_Matrix_Error_Default_Data();
                            resolve(Data);
                        }
                    }
                })
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Trip = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    TripID: values.TripID
                };
                let Result = await Trips.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_TRIP } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Validate_Service_Zone_Location = (Service_Type, lat, lng) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Service_Type: parseInt(Service_Type),
                    Geometry: {
                        // '$near': {
                        //     '$minDistance': 0,
                        //     '$maxDistance': 20000,
                        //     '$geometry': {
                        //         type: "Point",
                        //         coordinates: [
                        //             parseFloat(lng),
                        //             parseFloat(lat)
                        //         ]
                        //     }
                        // },
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: [
                                    parseFloat(lng),
                                    parseFloat(lat)
                                ]
                            }
                        }
                    },
                    Status: true
                };
                let Result = await ZONES.findOne(query).lean();
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Time_String_From_MS = (ms) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let d = moment.duration(ms, 'milliseconds');
                let hours = Math.floor(d.asHours());
                let mins = Math.floor(d.asMinutes()) - hours * 60;
                let secs = Math.floor(d.asSeconds()) - (hours * 60 * 60) - (mins * 60);
                hours = hours < 10 ? '0' + hours : hours;
                mins = mins < 10 ? '0' + mins : mins;
                secs = secs < 10 ? '0' + secs : secs;
                resolve(`${hours}:${mins}:${secs}`);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Fetch_App_Driver_Settings = () => {
    return new Promise(async (resolve, reject) => {
        try {
            let fndupdquery = {

            };
            let fndupdchanges = {

            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let Result = await App_Driver_Settings.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve(Result);
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_Driver_Api_Key = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ApiKey: values.ApiKey
                };
                let Result = await Driver_Devices.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_For_Store_BranchID = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let BranchData = await Store_Branch.findOne({ BranchID: values.BranchID }).lean().exec();
                if (BranchData == null) {
                    reject({ success: false, extras: { msg: ApiMessages.Store_Branch_Not_Found } })
                } else {
                    resolve(BranchData)
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Start_Date_End_Date_Validation = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date, config.Take_Date_Format).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date, config.Take_Date_Format).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                if (Start_Date.isValid() && End_Date.isValid()) {
                    if (Start_Date.isBefore(End_Date)) {
                        resolve("Validated Successfully")
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.START_DATE_MUST_BE_LESS_THAN_END_DATE } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DATE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Start_Date_End_Date_Validation_Normal = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_Date = moment(values.Start_Date).subtract(330, 'minutes');
                let End_Date = moment(values.End_Date).subtract(330, 'minutes').add(1, 'day').subtract(1, 'ms');
                if (Start_Date.isValid() && End_Date.isValid()) {
                    if (Start_Date.isBefore(End_Date)) {
                        resolve("Validated Successfully")
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.START_DATE_MUST_BE_LESS_THAN_END_DATE } })
                    }
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DATE } })
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Random_12_Digit_Number = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let charBank = "123456789";
                let str = '';
                for (let i = 0; i < 12; i++) {
                    str += charBank[parseInt(Math.random() * charBank.length)];
                };
                resolve(str);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_For_Product = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ProductID: values.ProductID
                };
                let Result = await Store_Products.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PRODUCT } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
};

CommonController.Check_For_Buyer = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                };
                let Result = await Buyers.findOne(query).select('-_id -__v -Cart_Information -Cart_Total_Price -Cart_Total_Items').lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_BUYER } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_For_Buyer_Session = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: values.BuyerID
                };
                let Result = await Buyers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_BUYER } })
                } else {
                    if (Result.SessionID == values.SessionID) {
                        resolve(Result);
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } });
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Api_Key = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ApiKey: values.ApiKey
                };
                let Result = await Devices.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_API_KEY } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Register_or_Get_App_versions = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Result = await App_Versions_Settings.findOne().lean();
                if (Result == null) {
                    let VersionData = await App_Versions_Settings().save();
                    resolve(JSON.parse(JSON.stringify(VersionData)));
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_IP_Address = (req) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let IPAddress = req.headers["x-forwarded-for"];
                if (IPAddress) {
                    let list = IPAddress.split(",");
                    IPAddress = list[list.length - 1];
                } else {
                    IPAddress = req.connection.remoteAddress;
                }
                IPAddress = (IPAddress == '::1') ? '0:0:0:0' : IPAddress;
                resolve(IPAddress);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.GENERATE_TEN_DIGIT_INCREMENT_COUNTER_SEQUENCE = (CounterID, StartCode) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                CounterID = String(CounterID);
                StartCode = String(StartCode);
                let seq = await CommonController.Generate_Counter_Sequence(CounterID);
                let Unique_Sequnce = StartCode;
                let seq_length = (String(seq).length <= 10) ? String(seq).length : 10;
                let length = 10 - seq_length;
                for (let t = 0; t < length; t++) {
                    Unique_Sequnce += 0;
                }
                Unique_Sequnce += seq;
                resolve(Unique_Sequnce);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Generate_Counter_Sequence = (CounterID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    _id: CounterID
                };
                let changes = {
                    $set: {
                        _id: CounterID
                    },
                    $inc: {
                        "seq": 1
                    }
                };
                let options = {
                    upsert: true,
                    new: true
                };
                let Result = await Counters.findByIdAndUpdate(query, changes, options).lean();
                resolve(Result.seq);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

//Dependencies
CommonController.Common_PhoneNumber_Validation = PhoneNumber => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (validator.isMobilePhone(PhoneNumber, "en-IN")) {
                    resolve("Validated Successfully");
                } else {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_PHONE_NUMBER_FORMAT } });
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Email_Validation = EmailID => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (EmailID == "") {
                    resolve("Validated Successfully");
                } else {
                    if (validator.isEmail(EmailID)) {
                        resolve("Validated Successfully");
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.INVALID_EMAIL_FORMAT } });
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Find_Default_SMS_Provider = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Selected_Provider: true,
                    Status: true
                };
                let Result = await App_SMS_Providers.findOne(query).lean();
                resolve(Result);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Random_OTP_Number = () => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let charBank = "123456789";
                let str = '';
                for (let i = 0; i < 4; i++) {
                    str += charBank[parseInt(Math.random() * charBank.length)];
                };
                resolve(str);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Error_Handler = (error) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                console.error("Common_Error_Handler---------->", error);
                if (error.success == null || error.success == undefined) {
                    if (error instanceof SyntaxError) {
                        resolve({ success: false, extras: { msg: ApiMessages.SERVER_ERROR } })
                    } else {
                        resolve({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } })
                    }
                } else {
                    resolve(error);
                }
            } catch (error) {
                console.error('Something Error Handler--->', error);
            }
        });
    });
};

CommonController.Check_for_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = new Object();
                if (config.AdminID == values.AdminID) {
                    query = {
                        Status: true
                    };
                } else {
                    query = {
                        AdminID: values.AdminID
                    };
                }
                let Result = await Admin_User.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ADMIN } })
                } else {
                    if (Result.SessionID == values.SessionID || config.SECRET_SESSIONID == values.SessionID || config.SessionID == values.SessionID) {
                        resolve(Result);
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } })
                    }
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Country = async (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CountryID: values.CountryID
                };
                let Result = await Country.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_COUNTRY } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Zone = async (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ZoneID: values.ZoneID
                };
                let Result = await ZONES.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ZONE } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Zone_Fee = async (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ZoneID: values.ZoneID
                };
                let Result = await Zone_Fees.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_ZONE } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Validate_Location_in_Zone_with_error = (ZoneID, lat, lng) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    'ZoneID': ZoneID,
                    'Geometry': {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: [
                                    parseFloat(lng),
                                    parseFloat(lat)
                                ]
                            }
                        }
                    }
                };
                let Result = await ZONES.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.LOCATION_NOT_IN_ZONE } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Increment_and_Get_City_Zone_Hub_Version = (CityID, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let fndupdchanges = {
                    $inc: {
                        Version: 1
                    }
                };
                let fndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let Result = await City_Zone_Hub_Version.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve(Result.Version);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_State = async (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StateID: values.StateID
                };
                let Result = await State.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_STATE } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_City = async (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    CityID: values.CityID
                };
                let Result = await City.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_CITY } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Increment_and_Get_City_Zone_Version = (CityID, Service_Type) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let fndupdquery = {
                    CityID: CityID,
                    Service_Type: Service_Type
                };
                let fndupdchanges = {
                    $inc: {
                        Version: 1
                    }
                };
                let fndupdoptions = {
                    upsert: true,
                    setDefaultsOnInsert: true,
                    new: true
                }
                let Result = await City_Zone_Version.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
                resolve(Result.Version);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Fetch_Or_Creat_Store = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = {
                Store_Entity_Name: values.Store_Entity_Name
            };
            let Result = await Store_Entity.findOne(query).select('-_id -__v').lean();
            if (Result == null) {
                var date = new Date();
                let Data = {
                    EntityID: uuid.v4(),
                    Store_Entity_Name: values.Store_Entity_Name,
                    Website: values.Website,
                    Description: values.Description,
                    Status: true,
                    created_at: date,
                    updated_at: date
                }
                let NewResult = await Store_Entity(Data).save();
                resolve(Data);
            } else {
                resolve(Result)
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Validate_OTP_Data = (values) => {
    return new Promise(async (resolve, reject) => {
        try {
            let obj1 = {
                $ne: null
            };
            let obj2 = {
                $eq: values.OTP
            }
            let OTP_Query = (config.SECRET_OTP_CODE === String(values.OTP)) ? obj1 : obj2;
            let query = {
                StoreOTPID: values.StoreOTPID,
                OTP: OTP_Query,
                Latest: true
            };
            let Result = await Store_Branch_OTP.findOne(query).lean();
            if (Result == null) {
                reject({ success: false, extras: { msg: ApiMessages.INVALID_OTP } });
                let Data = {
                    StoreOTPID: values.StoreOTPID,
                    Time: new Date()
                };
                let SaveResult = await Store_OTP_Tries(Data).save();
            } else {
                resolve(Result);
                let RemoveTries = await Store_OTP_Tries.deleteOne({
                    StoreOTPID: values.StoreOTPID,
                }).lean();
            };
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_Whether_Store_Branch_Name_Already_Exists = function (Branch_Name, callback) {
    Store_Branch.findOne({ Branch_Name: Branch_Name }).exec(function (err, Result) {
        if (err) {
            console.log(err);
            return callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result == null) {
                callback(false);
            } else if (Result != null) {
                return callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.STORE_BRANCH_NAME_ALREADY_EXIST
                    }
                }));
            }
        }
    })
};

CommonController.Validate_Store_Branch_URL = (BranchURL, callback) => {
    Store_Branch.findOne({ BranchURL: BranchURL }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.BRANCH_URL_ALREADY_IN_USE
                    }
                }));
            } else if (Result == null) {
                callback(false, new ApiResponce({
                    success: true,
                    extras: {
                        msg: "Branch URL can be used"
                    }
                }));
            }
        }
    })
};

CommonController.Validate_Store_Branch_ID = (StoreID, callback) => {
    Store_Branch.findOne({ StoreID: StoreID }, (err, Result) => {
        if (err) {
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result != null) {
                callback(true, new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.STORE_ID_ALREADY_IN_USE
                    }
                }));
            } else if (Result == null) {
                callback(false, new ApiResponce({
                    success: true,
                    extras: {
                        msg: "Store ID can be used"
                    }
                }));
            }
        }
    })
};

//Check Whether User Already Exist 
CommonController.Check_For_Admin_Phone_Branch = function (Password, Name, PhoneNumber, EmailID, BranchData, callback) {
    var BranchDetails = {
        BranchID: BranchData.BranchID,
        Branch_Name: BranchData.Branch_Name
    };
    // console.log("PhoneNumber");
    // console.log(PhoneNumber);
    Customers.findOne({ Phone: PhoneNumber }).exec(function (err, Result) {
        if (err) {
            console.log(err);
        } else {
            if (Result == null) {
                // console.log("USer Not Found");
                callback(false);
            } else if (Result != null) {
                Customers.findOne({ Phone: PhoneNumber, Whether_Store_Admin: true }).exec(function (err, NewResult) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (NewResult == null) {
                            // console.log("USer is not Admin");
                            var squery = {
                                _id: Result._id
                            };
                            var branchArray = [];
                            branchArray.push(BranchDetails);
                            var schanges = {
                                Whether_Store_Admin: true,
                                StoreAdminStatus: true,
                                BranchData: branchArray,
                                Active_BranchID_Exist: true,
                                Active_BranchID: BranchData.BranchID
                            }
                            var smultiplicity = {
                                multi: false
                            }
                            Customers.update(squery, schanges, smultiplicity).exec(function (err, AdminUpdatedStatus) {
                                if (!err) {
                                    var newquery = {
                                        BranchID: BranchData.BranchID
                                    };
                                    var newchanges = {
                                        $push: {
                                            AdminData: {
                                                StoreAdminID: Result._id
                                            }
                                        }
                                    };
                                    var newmultiplicity = {
                                        multi: false
                                    };
                                    Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log("Admin Updated in Branch");
                                            callback(true, false);
                                        }
                                    })
                                }
                            })
                        } else if (NewResult != null) {
                            // console.log("User and Admin")
                            Customers.findOne({
                                _id: Result._id,
                                BranchData: {
                                    $elemMatch: {
                                        "BranchID": String(BranchData.BranchID)
                                    }
                                }
                            }).exec(function (err, BranchStaus) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    if (BranchStaus == null) {
                                        var query = {
                                            _id: Result._id
                                        };

                                        var changes = {
                                            $push: {
                                                BranchData: BranchDetails
                                            }
                                        }
                                        var multiplicity = {
                                            multi: false
                                        }
                                        // console.log("Branch Changes");
                                        // console.log(changes);
                                        Customers.update(query, changes, multiplicity).exec(function (err, UpdatedStatus) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                // console.log("Branch Updated in Admin");
                                                var newquery = {
                                                    BranchID: BranchData.BranchID
                                                };
                                                var newchanges = {
                                                    $push: {
                                                        AdminData: {
                                                            StoreAdminID: Result._id
                                                        }
                                                    }
                                                };
                                                var newmultiplicity = {
                                                    multi: false
                                                };
                                                Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        // console.log("Admin Updated in Branch");
                                                        callback(true, false);
                                                    }
                                                })
                                            }
                                        })
                                    } else if (BranchStaus != null) {
                                        console.log("Admin Branch Already Exist");
                                        callback(true, true);
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}

CommonController.Find_and_Update_CustomerSeqID = function (callback) {
    Counters.findOneAndUpdate({
        "_id": "customerid"
    }, {
        $set: {
            _id: "customerid"
        },
        $inc: {
            "seq": 1
        }
    }, {
        upsert: true,
        returnNewDocument: true
    }).exec(function (err, Result) {
        // console.log(Result);
        var SequenceNumber = Result.seq;
        return callback(false, parseInt(SequenceNumber));
    })
};

//Generating the Random Number for Security
CommonController.RandomNumber = function () {
    var charBank = "123456789";
    var fstring = '';
    for (var i = 0; i < 6; i++) {
        fstring += charBank[parseInt(Math.random() * charBank.length)];
    }
    // console.log("Random")
    // console.log(fstring);
    return parseInt(fstring);
};
//Current Date Time;
CommonController.DateTime = function () {
    var fulldate = new Date();
    var moment = require('moment');
    var date = moment().utcOffset(330).format('YYYY-MM-DD');
    var time = moment().utcOffset(330).format('H:mm:ss');

    var datetime = date + ' ' + time;
    return datetime;
};

CommonController.Check_for_Vehicle = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    VehicleID: values.VehicleID
                };
                let Result = await Vehicles.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_VEHICLE } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Validate_Start_End_Time = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let Start_at = moment(values.Start_at, config.Common_Time_Format);
                let End_at = moment(values.End_at, config.Common_Time_Format);
                if (Start_at.isSameOrAfter(End_at)) {
                    reject({ success: false, extras: { msg: ApiMessages.START_TIME_MUST_BE_LESS_THAN_END_TIME } });
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Upcoming_Date = (Day) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                Day = parseInt(Day);
                var date = moment().add(Day, 'days').startOf('day').add(1, 'ms').toDate();
                resolve(date);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Start_Date = (qDate) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (qDate == null || qDate == undefined) {
                    var date = moment().startOf('day').add(1, 'ms').toDate();
                } else {
                    var date = moment(qDate, config.Take_Date_Format).add(1, 'ms').toDate();
                }
                resolve(date);
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Floating_Beautify_Value = (Num) => {
    return new Promise(async (resolve, reject) => {
        try {
            Num = (Math.round(Num * 100) / 100);
            resolve(Num);
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.GENERATE_TEN_DIGIT_INCREMENT_COUNTER_SEQUENCE = (CounterID, StartCode, StringLength) => {
    return new Promise(async (resolve, reject) => {
        try {

            let mapplength = 10;
            if (StringLength != null && StringLength != undefined && isFinite(StringLength) && !isNaN(StringLength)) {
                StringLength = parseInt(StringLength) || 10;
                if (StringLength > 10) {
                    mapplength = StringLength;
                }
            }
            CounterID = String(CounterID);
            StartCode = String(StartCode);
            let seq = await CommonController.Generate_Counter_Sequence(CounterID);
            let Unique_Sequnce = StartCode;
            let seq_length = (String(seq).length <= 10) ? String(seq).length : 10;
            let length = mapplength - seq_length;
            for (let t = 0; t < length; t++) {
                Unique_Sequnce += 0;
            }
            Unique_Sequnce += seq;
            resolve(Unique_Sequnce);
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}


CommonController.Generate_Counter_Sequence = (CounterID) => {
    return new Promise(async (resolve, reject) => {
        try {
            let fndupdquery = {
                _id: CounterID
            };
            let fndupdchanges = {
                $inc: {
                    "seq": 1
                }
            };
            let fndupdoptions = {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            }
            let Result = await Counters.findOneAndUpdate(fndupdquery, fndupdchanges, fndupdoptions).select('-_id -__v').lean();
            resolve(Result.seq);
        } catch (error) {
            reject(await CommonController.Common_Error_Handler(error));
        }
    });
}

CommonController.Check_for_Image = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ImageID: values.ImageID
                };
                let Result = await Images.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.IMAGE_NOT_FOUND } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_File = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    FileID: values.FileID
                };
                let Result = await Files.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.FILE_NOT_FOUND } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_File_Response_Single_File = (Available, Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Available) {
                    Data.File_Path = config.S3URL + Data.File_Path;
                    resolve(Data);
                } else {
                    resolve(new Object());
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Licence_Image = (ImageID) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    ImageID: ImageID
                };
                let Result = await Images.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.IMAGE_NOT_FOUND } })
                } else {
                    resolve(Result);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Common_Image_Response_Single_Image = (Available, Data) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                if (Available) {
                    Data.Image50 = config.S3URL + Data.Image50;
                    Data.Image100 = config.S3URL + Data.Image100;
                    Data.Image250 = config.S3URL + Data.Image250;
                    Data.Image550 = config.S3URL + Data.Image550;
                    Data.Image900 = config.S3URL + Data.Image900;
                    Data.ImageOriginal = config.S3URL + Data.ImageOriginal;
                    resolve(Data);
                } else {
                    resolve(Data);
                }
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_Only_Driver = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DriverID: values.DriverID
                };
                let Result = await Drivers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DRIVER } })
                } else {
                    resolve(Result);
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

CommonController.Check_for_Driver = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    DriverID: values.DriverID
                };
                let Result = await Drivers.findOne(query).lean();
                if (Result == null) {
                    reject({ success: false, extras: { msg: ApiMessages.INVALID_DRIVER } })
                } else {
                    if ((Result.SessionID == values.SessionID) || (config.SECRET_SESSIONID == values.SessionID)) {
                        if (Result.Status) {
                            resolve(Result);
                        } else {
                            reject({ success: false, extras: { msg: ApiMessages.ACCOUNT_NOT_ACTIVE } })
                        }
                    } else {
                        reject({ success: false, extras: { msg: ApiMessages.SESSION_EXPIRED } })
                    }
                };
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

export default CommonController;