
let NotificationController = function () { };
import async from "async";
import uuid from "uuid";
import moment from "moment";
import ApiMessages from "../models/ApiMessages.js";
import config from "../config/config.js";
import CommonController from "./CommonController.js";
import Branch_Notification_Log from "../models/Branch_Notification_Log.js";
import Customers from "../models/Customers.js";
import Store_Branch from "../models/Store_Branch.js";
import Buyer_Orders from "../models/Buyer_Orders.js";
import Stack_Log from "../models/Stack_Log.js";
import Reorder_log from "../models/Reorder_log.js";
import Admin_Notification_Log from "../models/Admin_Notification_Log.js";
import Buyers_Current_Location from "../models/Buyers_Current_Location.js";

NotificationController.SendNotification_Driver_Order_Cancel_Request = (values, OrderData, DriverData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let CountryData = await CommonController.Check_for_Country(DriverData)
                let CityDataData = await CommonController.Check_for_City(DriverData)
                let ZoneData = await CommonController.Check_for_Zone(DriverData)
                let Data = {
                    NotificationID: uuid.v4(),
                    DriverData: DriverData.DriverID,
                    Driver_Name: DriverData.Driver_Name,
                    Driver_Phone_Number: DriverData.Driver_Phone_Number,
                    Driver_EmailID: DriverData.Driver_EmailID,

                    CountryID: CountryData.CountryID,
                    CountryName: CountryData.CountryName,

                    CityID: CityDataData.CityID,
                    CityName: CityDataData.CityName,

                    ZoneID: ZoneData.ZoneID,
                    Zone_Title: ZoneData.Zone_Title,

                    Type: 8,//Cancel request from driver
                    NotificationTitle: "Cancel request from driver",
                    NotificationBody: "Driver with Name: " + DriverData.Driver_Name + "has requested for cancel order with order number " + OrderData.Order_Number + " \n Cancel reason: " + values.Cancel_Reason + ".",
                    NotificationData: OrderData,
                    Created_at: new Date(),
                }
                let SaveData = await Admin_Notification_Log.create(Data);
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Request_Stack_ETA = (values, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: BuyerData.BuyerID
                }
                let CurrentBuyerLocation = await Buyers_Current_Location.findOne(query).lean().select('-_id -__v').exec()
                let CountryData = await CommonController.Check_for_Country(CurrentBuyerLocation)
                let CityDataData = await CommonController.Check_for_City(CurrentBuyerLocation)
                let ZoneData = await CommonController.Check_for_Zone(CurrentBuyerLocation)
                
                let queryx = {
                    BuyerID: values.BuyerID,
                    StackID: values.StackID,
                };
                let StackData = await Stack_Log.findOne(queryx).lean().select('-_id -__v').exec();
                let Data = {
                    NotificationID: uuid.v4(),
                    BuyerID: BuyerData.BuyerID,
                    Buyer_PhoneNumber: BuyerData.Buyer_PhoneNumber,
                    Buyer_Name: BuyerData.Buyer_Name,
                    Buyer_Email: BuyerData.Buyer_Email,
                    CountryID: CountryData.CountryID,
                    CountryName: CountryData.CountryName,

                    CityID: CityDataData.CityID,
                    CityName: CityDataData.CityName,

                    ZoneID: ZoneData.ZoneID,
                    Zone_Title: ZoneData.Zone_Title,

                    Type: 7,//Stack ETA Request
                    NotificationTitle: "Stack ETA request from buyer",
                    NotificationBody: "Buyer with Buyer Name: " + BuyerData.Buyer_Name + "has requested for stack ETA for Product with Product Name: " + StackData.Product_Name + "(" + StackData.Unique_ProductID + "). \nRequested quantity: " + StackData.Product_Quantity + ".",
                    NotificationData: StackData,
                    Created_at: new Date(),
                }
                let SaveData = await Admin_Notification_Log.create(Data);
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Cancel_Single_Order = (values, OrderData, BuyerData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BuyerID: BuyerData.BuyerID
                }
                let CurrentBuyerLocation = await Buyers_Current_Location.findOne(query).lean().select('-_id -__v').exec()
                let CountryData = await CommonController.Check_for_Country(CurrentBuyerLocation)
                let CityDataData = await CommonController.Check_for_City(CurrentBuyerLocation)
                let ZoneData = await CommonController.Check_for_Zone(CurrentBuyerLocation)
                let Data = {
                    NotificationID: uuid.v4(),
                    BuyerID: BuyerData.BuyerID,
                    Buyer_PhoneNumber: BuyerData.Buyer_PhoneNumber,
                    Buyer_Name: BuyerData.Buyer_Name,
                    Buyer_Email: BuyerData.Buyer_Email,
                    CountryID: CountryData.CountryID,
                    CountryName: CountryData.CountryName,

                    CityID: CityDataData.CityID,
                    CityName: CityDataData.CityName,

                    ZoneID: ZoneData.ZoneID,
                    Zone_Title: ZoneData.Zone_Title,

                    Type: 6,//Accept Notified order
                    NotificationTitle: "Cancelation request from buyer",
                    NotificationBody: "An Order with Order Number: " + OrderData.Order_Number + " is Requested for Cancellation. \nBuyer Name: " + BuyerData.Buyer_Name + ", \nCancel Reason: " + values.Cancel_Reason + ".",
                    NotificationData: OrderData,
                    Created_at: new Date(),
                }
                let SaveData = await Admin_Notification_Log.create(Data);
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Accept_Notified_Cancelled_Order = (values, BranchData, OrderData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let CountryData = await CommonController.Check_for_Country(BranchData)
                let CityDataData = await CommonController.Check_for_City(BranchData)
                let ZoneData = await CommonController.Check_for_Zone(BranchData)
                
                let Data = {
                    NotificationID: uuid.v4(),
                    BranchID: values.BranchID,
                    StoreID: BranchData.StoreID,
                    Branch_Name: BranchData.Branch_Name,
                    CountryID: CountryData.CountryID,
                    CountryName: CountryData.CountryName,

                    CityID: CityDataData.CityID,
                    CityName: CityDataData.CityName,

                    ZoneID: ZoneData.ZoneID,
                    Zone_Title: ZoneData.Zone_Title,

                    Type: 5,//Accept Notified order
                    NotificationTitle: "Store Accepted notified order",
                    NotificationBody: BranchData.Branch_Name + " has accepted cancel for order with Order Number: " + OrderData.Order_Number + ".",
                    NotificationData: OrderData,
                    Created_at: new Date(),
                }
                let SaveData = await Admin_Notification_Log.create(Data);
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Cancel_Order_Request = (values, BranchData) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let CountryData = await CommonController.Check_for_Country(BranchData)
                let CityDataData = await CommonController.Check_for_City(BranchData)
                let ZoneData = await CommonController.Check_for_Zone(BranchData)
                
                let query = {
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        StoreID: BranchData.StoreID,
                        Branch_Name: BranchData.Branch_Name,
                        CountryID: CountryData.CountryID,
                        CountryName: CountryData.CountryName,

                        CityID: CityDataData.CityID,
                        CityName: CityDataData.CityName,

                        ZoneID: ZoneData.ZoneID,
                        Zone_Title: ZoneData.Zone_Title,

                        Type: 4,//Order cancel request by store
                        NotificationTitle: "Order cancel requested by store",
                        NotificationBody: BranchData.Branch_Name + " has requested for order cancel.\n Order Number: " + Result.Order_Number + ", \n Cancel reason: " + values.Cancel_Reason + ".",
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Admin_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Fullfill_Stack_Quantity = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let q = {
                    BranchID: values.BranchID
                }
                let BranchData = await Store_Branch.findOne(q).lean().exec()
                let CountryData = await CommonController.Check_for_Country(BranchData)
                let CityDataData = await CommonController.Check_for_City(BranchData)
                let ZoneData = await CommonController.Check_for_Zone(BranchData)
                let query = {
                    BranchID: values.BranchID,
                    StackID: values.StackID
                };
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        StoreID: BranchData.StoreID,
                        Branch_Name: BranchData.Branch_Name,
                        CountryID: CountryData.CountryID,
                        CountryName: CountryData.CountryName,

                        CityID: CityDataData.CityID,
                        CityName: CityDataData.CityName,

                        ZoneID: ZoneData.ZoneID,
                        Zone_Title: ZoneData.Zone_Title,

                        Type: 3,//Branch Stack Quantity update
                        NotificationTitle: "Store Product Stack Quantity updated",
                        NotificationBody: BranchData.Branch_Name + " has update product stack Quantity. Fullfilled Quantity: " + values.Fullfilled_Quantity,
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Admin_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Update_Stack_ETA = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let q = {
                    BranchID: values.BranchID
                }
                let BranchData = await Store_Branch.findOne(q).lean().exec()
                let CountryData = await CommonController.Check_for_Country(BranchData)
                let CityDataData = await CommonController.Check_for_City(BranchData)
                let ZoneData = await CommonController.Check_for_Zone(BranchData)
                
                let query = {
                    BranchID: values.BranchID,
                    StackID: values.StackID
                };
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        StoreID: BranchData.StoreID,
                        Branch_Name: BranchData.Branch_Name,
                        CountryID: CountryData.CountryID,
                        CountryName: CountryData.CountryName,

                        CityID: CityDataData.CityID,
                        CityName: CityDataData.CityName,

                        ZoneID: ZoneData.ZoneID,
                        Zone_Title: ZoneData.Zone_Title,

                        Type: 2,//Branch Stack ETA update
                        NotificationTitle: "Store Product Stack ETA updated",
                        NotificationBody: BranchData.Branch_Name + " has update product stack ETA. ETA: " + values.Branch_ETA + ", \nETA Description: " + values.Branch_ETA_Description + "\n ETA Date is" + moment().add(values.Branch_ETA, 'd').format(),
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Admin_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Update_Branch_Product_Reorder = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let q = {
                    BranchID: values.BranchID
                }
                let BranchData = await Store_Branch.findOne(q).lean().exec()
                let CountryData = await CommonController.Check_for_Country(BranchData)
                let CityDataData = await CommonController.Check_for_City(BranchData)
                let ZoneData = await CommonController.Check_for_Zone(BranchData)
                
                let query = {
                    ProductID: values.ProductID,
                    BranchID: values.BranchID,
                    ReorderLogID: values.ReorderLogID
                };
                let Result = await Reorder_log.findOne(query).lean().exec();
                let Reorder_Status = "Not updated"
                if (values.Availablity == 1) {
                    Reorder_Status = "Stock Available"
                } else if (values.Availablity == 2) {
                    Reorder_Status = "Stock Partial Available"
                } else if (values.Availablity == 3) {
                    Reorder_Status = "Stock Not Available"
                }
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        StoreID: BranchData.StoreID,
                        Branch_Name: BranchData.Branch_Name,
                        CountryID: CountryData.CountryID,
                        CountryName: CountryData.CountryName,

                        CityID: CityDataData.CityID,
                        CityName: CityDataData.CityName,

                        ZoneID: ZoneData.ZoneID,
                        Zone_Title: ZoneData.Zone_Title,

                        Type: 1,//Branch reorder update
                        NotificationTitle: "Store updated Rorder status",
                        NotificationBody: BranchData.Branch_Name + "has update reorder status and updated reorder status is " + Reorder_Status,
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Admin_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Cancel_Order_Return_Initiate = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: Result.Branch_Information.BranchID,
                        Type: 8,//ETA requested for Stack
                        NotificationTitle: "Cancelled order return initiated",
                        NotificationBody: "Cancelled order with order number " + Result.Order_Number + " return is started. Soon you will receive cancelled product/books.",
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Request_Branch_Stack_Reorder = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    StackID: values.StackID
                };
                let Result = await Stack_Log.findOne(query).lean().exec();
                if (Result != null) {

                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: Result.BranchID,
                        Type: 7,//ETA requested for Stack
                        NotificationTitle: "ETA requested for Stack",
                        NotificationBody: "ETA is requested for stacked product/Book: " + Result.Product_Name + " with Quantity of " + Result.Reorder_Quantity + ". Please kindly update.",
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Cancel_Order_Notify_To_Branch = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    OrderID: values.OrderID
                };
                let Result = await Buyer_Orders.findOne(query).lean().exec();
                if (Result != null) {

                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: Result.Branch_Information.BranchID,
                        Type: 6,//Order requested for cancellation
                        NotificationTitle: "Order requested for cancellation",
                        NotificationBody: "An order with order number : " + Result.Order_Number + " processed by your store is cancelled.",
                        NotificationData: Result,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Add_Store_Product = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                delete values.ReorderID
                let query = {
                    BranchID: values.BranchID
                };
                let Result = await Store_Branch.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        Type: 5,//Product Added to Store
                        NotificationTitle: "Product added to store",
                        NotificationBody: "A book: " + values.Product_Name + " is added to your store",
                        NotificationData: values,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Branch_Product_Reorder = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let Result = await Store_Branch.findOne(query).lean().exec();
                if (Result != null) {
                    updateQuery.BranchID = values.BranchID
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        Type: 4,//Branch Details Updated
                        NotificationTitle: "Reorder request",
                        NotificationBody: "Your store book: " + values.Product_Name + " is requested for reorder with Quantity of " + values.Quantity_Intake,
                        NotificationData: values,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Update_Branch_Timings = (values, updateQuery) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let Result = await Store_Branch.findOne(query).lean().exec();
                if (Result != null) {
                    updateQuery.BranchID = values.BranchID
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        Type: 3,//Branch Details Updated
                        NotificationTitle: "Store Timings updated",
                        NotificationBody: "Your store Timings are updated",
                        NotificationData: updateQuery,
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);
                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Update_Branch_Details = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    BranchID: values.BranchID
                };
                let Result = await Store_Branch.findOne(query).lean().exec();
                if (Result != null) {
                    let Data = {
                        NotificationID: uuid.v4(),
                        BranchID: values.BranchID,
                        Type: 2,//Branch Details Updated
                        NotificationTitle: "Store details updated",
                        NotificationBody: "Your store details are updated and updated details are Store Name: " + values.Branch_Name + ". PhoneNumber: " + values.Branch_PhoneNumber + ", etc..!",
                        NotificationData: {
                            BranchID: Branch.BranchID,
                            Branch_Name: values.Branch_Name,
                            Branch_PhoneNumber: values.Branch_PhoneNumber,
                            BranchURL: values.BranchURL,
                            Description: values.Description,
                            ImageID: values.ImageID,
                            Address: values.Address,
                            Latitude: parseFloat(values.Latitude),
                            Longitude: parseFloat(values.Longitude),
                            Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
                            CountryID: values.CountryID,
                            CityID: values.CityID,
                            ZoneID: values.ZoneID
                        },
                        Created_at: new Date(),
                    }
                    let SaveData = await Branch_Notification_Log.create(Data);


                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

NotificationController.SendNotification_Update_Branch_Admin = (values) => {
    return new Promise((resolve, reject) => {
        setImmediate(async () => {
            try {
                let query = {
                    Phone: values.PhoneNumber
                };
                let Result = await Customers.findOne(query).lean().exec();
                if (Result != null) {
                    for (let Branch of Result.BranchData) {
                        let Data = {
                            NotificationID: uuid.v4(),
                            BranchID: Branch.BranchID,
                            Type: 1,//Branch admin Details Updated
                            NotificationTitle: "Store admin details updated",
                            NotificationBody: "Your store admin details are updated and updated details are Name: " + Result.First_name + " and Emailid: " + Result.Email,
                            NotificationData: {
                                BranchID: Branch.BranchID,
                                Branch_Name: Branch.Branch_Name,
                                Branch_AdminID: Result.CustomerID
                            },
                            Created_at: new Date(),
                        }
                        let SaveData = await Branch_Notification_Log.create(Data);
                    }

                }
                resolve("process completed")
            } catch (error) {
                reject(await CommonController.Common_Error_Handler(error));
            }
        });
    });
}

export default NotificationController;