let config = function() {};
import os from "os";

//Body Parser Limit
config.BodyParserLimit = '2mb';

//Date Time Format
config.Take_Date_Time_Format = 'DD-MM-YYYY HH:mm:ss';
config.Take_Date_Format = 'DD-MM-YYYY';

config.Common_Date_Time_Format = 'DD-MM-YYYY, HH:mm:ss A';
config.Common_Date_Format = 'DD-MM-YYYY';
config.Common_Time_Format = 'HH:mm:ss';

config.Number_of_Display_Schedule_Dates = 7;
config.Schedule_Time_Slots_Interval_In_Milliseconds = 1 * 60 * 60 * 1000; //1 hour
config.Minimun_App_Automated_Discount_Percentage = 0;
config.Maximum_App_Automated_Discount_Percentage = 100;
config.Default_Error_Google_API_Distance_Text = "10 kms"; //10 Kms
config.Default_Error_Google_API_Duration_Text = "1 Hours"; //1 Hour
config.Default_Error_Google_API_Distance = 10000; //10 Kms
config.Default_Error_Google_API_Duration = 3600; //1 Hour
config.Convenience_Fees_Increment_Price_Split_Multi_Orders = 10; //Rs.10
config.SP_MD_AND_MP_SD_VARIABLE_WINDOW_TIME = 30 * 60 * 1000; //30 minutes to ms
config.INSTANT_DRIVER_VEHICLE_DIFFERENCE_WEIGHT = 20; //20 kgs
config.Operation_Percentage = 5; //Service Fee
config.Gig_Driver_KMS = 10; //10 Kms
config.Driver_Order_Near_KMS = 1; //1 Kms
config.INSTANT_DRIVER_ACCEPTANCE_TIME_NEW_REQUEST = 190; //15 sec
config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME = 10; //10 minutes
config.INSTANT_DRIVER_REQUEST_ACCEPTED_TIME_CASH = 2; //2 minutes
config.Instant_Order_Time_Interval = 4; //4 hours
config.earthRadius = 6378137;
config.MINIMUM_SINGLE_PACKAGE_WEIGHT = 0;
config.MAXIMUM_SINGLE_PACKAGE_WEIGHT = 1500;
config.DRIVER_PAYOUT_DAY = 3; //3 days
config.MAIN_ORDER_RETRY_WINDOW_SLOT_TIME = 5; //15 SECS
config.UNIQUE_REFUND_CODE_LENGTH = 10;
config.ALERT_SMS_LENGTH = 120;
config.Common_Offset = 330;
config.Cancel_Time = 99; // in Sec
config.Points_to_Amount = 2;

//User OTP Tries and OTP Request
config.OTP_COUNT = 4;
config.OTP_TRIES_COUNT = 4;
config.OTP_COUNT_TIME_IN_MINUTES = 30
config.OTP_TRIES_COUNT_TIME_IN_MINUTES = 30
config.MAX_SIGNUP_AGE = 15;

//Upload Limit Size
config.Image_Size = 5 * 1024 * 1024;

//File Size
config.File_Size = 5 * 1024 * 1024;

config.RazorpayX_Service_Amount = Amount => {
    return new Promise(async(resolve, reject) => {
        try {
            let Service_Amount = 0;
            if (Amount < 1000) {
                Service_Amount = 10;
            } else if (Amount >= 1000 && Amount < 25000) {
                Service_Amount = 15;
            } else if (Amount >= 25000 && Amount < 50000) {
                Service_Amount = 20;
            } else if (Amount >= 50000 && Amount < 100000) {
                Service_Amount = 30;
            } else if (Amount >= 100000) {
                Service_Amount = 50;
            };
            if (Amount <= Service_Amount) {
                Service_Amount = 0;
            };
            resolve(Service_Amount);
        } catch (error) {
            console.error("config error----->");
            console.error(error);
            resolve(0);
        }
    });
}

//RazorpayX Transaction Statuses
config.Razorpay_Transaction_Data = [{
        Transaction_Status: 0,
        Comment: "transaction initiated",
        status: "initiated"
    },
    {
        Transaction_Status: 1,
        Comment: "transaction queued",
        status: "queued"
    },
    {
        Transaction_Status: 2,
        Comment: "transaction processed",
        status: "processed"
    },
    {
        Transaction_Status: 3,
        Comment: "transaction issued",
        status: "issued"
    },
    {
        Transaction_Status: 4,
        Comment: "transaction initiated",
        status: "processing"
    },
    {
        Transaction_Status: 5,
        Comment: "transaction reversed",
        status: "reversed"
    },
    {
        Transaction_Status: 6,
        Comment: "transaction created",
        status: "processing"
    },
    {
        Transaction_Status: 7,
        Comment: "transaction cancelled",
        status: "cancelled"
    }
];

config.firebase = {
    customer_app: {
        databaseURL: "https://bookafella-customer-262405.firebaseio.com",
        serverkey: "AAAADbkkEpI:APA91bEGk8wzXnpLjFBujzhEUFNvp-K89gPNCkktl4994oh_20zeKftcT0xaijkm7iTHpl6vzwv1k9PBRi2abKOOFxnf8ANSJlO_tItTNfeEdQ-_PNQkTXlCjfRZB8GOwJr9_J4YYaUL"
    },
    driver_app: {
        serverkey: "AAAA5CTeVNQ:APA91bGpe_li3FFuDFFQemu1MRkvdqMUmdIwolCIGWmuWeoc1lsTm4bNtn7NkezOO-LbKa5rXI_fViHFRz8aZ6CmjQU3WIEu3SrPLgweo6FDwgqtlcYH4jt5NL_m7s5QHT0MDtKrGAIy"
    }
};


config.Customer__Care = '1033'

//AWS Credentials
config.S3AccessKey = "AKIAIK34CYP5ZVBXIOOQ";
config.S3Secret = "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";
config.S3Bucket = "bookafella1";
config.S3URL = "https://bookafella1.s3-us-west-2.amazonaws.com/";

//MSG91 Credentials
config.msg91 = {
    "host": "http://control.msg91.com/api/",
    "authkey": "304938AMQ7XSSD5dd62aff",
    "sender_id": "BOOKFL",
    "route_no": "4"
}

//Pubnub Credentials bookafella
config.Pubnub = {
    publishKey: 'pub-c-84f92ea1-b958-4d6a-82d0-cd558552c553',
    subscribeKey: 'sub-c-484734b8-2004-11ea-92cf-86076a99d5da',
    secretKey: 'sec-c-OTZlMmI4NTMtOTI5MC00OWZmLTgwMTEtMmVhMmI4ODQ5OWY4',
    masterChannelName: 'bookafella-driver-live-0001',
    instantRequestChannelName: 'Book_Order_Request_Status',
    ssl: true
}

//GoogleCredential
config.Google = {
    "key": "AIzaSyARpTY0AOvZ7cXZiEdKzB05EPDrpQn_RnY", //Bookafella
    "reverse_geocode_url": "https://maps.googleapis.com/maps/api/geocode/json?",
    "text_search_url": "https://maps.googleapis.com/maps/api/place/textsearch/json?",
    "autocomplete_url": "https://maps.googleapis.com/maps/api/place/autocomplete/json?",
    "place_detail_url": "https://maps.googleapis.com/maps/api/place/details/json?",
    "places_radius": 200000 //200 kms
};

config.twilio = {
    "ACCOUNT_SID": "AC09d5f9206a051c299b5a46009abd7098",
    "AUTH_TOKEN": "06241cc47a61da0cf394afd9025c4371",
    "TWILIO_NUMBER": "+12403033568"
};

//Admin Dashboard Config
config.AdminID = 'ADMIN123456789-0123456789-ADMIN123456789';
config.SessionID = 'SESSION123456789-0123456789-SESSION123456789';

//Application Secret Credentials
config.SECRETCODE = "EDFGJS-DHSHAJ-DASHJDJH-DHJSGAJH";
config.SECRET_OTP_CODE = '9009';
config.SECRET_SESSIONID = 'TESTING123';

/***************************
 * 
 * Server Setting for Production and Development
 * 
 * **********************************************************/

config.Whether_Production_Settings = false;

config.api_port = 3001;
config.admin_port = 3002;
config.store_port = 3003;
config.track_port = 3004;
config.web_port = 3005;

if (os.hostname() === "bookafella.in") {
    //Production Settings
    console.log('Production Settings')
    console.log(os.hostname())
    config.host = 'https://api.bookafella.in';

    //ports
    // config.MongoURL = `mongodb://userevonbookafella:userevonbookafella123@db.bookafella.in:1234/bookafella?replicaSet=bookafella`;
    config.MongoURL = `mongodb+srv://vixspace:VIXspace@123@cluster0.u5j33.mongodb.net/Clustet0?retryWrites=true&w=majority`;
    //config.MongoURL = `mongodb://bookfella:evontex123@ds137508.mlab.com:37508/bookfella`;

    config.razorpay = {
        host: "api.razorpay.com/v1",
        baseURL: "https://api.razorpay.com/v1",
        merchant_id: "Diwu5M8mU3Z8KY",
        key_id: "rzp_test_dCzF1rfm0BrX0f",
        key_secret: "OFdq8SlJgmn2rAe5HsChhHzU",
        razorpayx_account_number: "7878780032871059",
        webhook_secret: "indian153"
    }

} else {
    //Testing Settings

    console.log('Development Settings')
    console.log(os.hostname())
    config.host = 'https://xapi.bookafella.in';

    //ports
    // config.MongoURL = `mongodb+srv://vixbookafella:vixbookafella123@bookafella-wn0td.mongodb.net/Bookafella?retryWrites=true&w=majority`;
    config.MongoURL = `mongodb+srv://vixspace:VIXspace@123@cluster0.u5j33.mongodb.net/Clustet0?retryWrites=true&w=majority`;
    // config.MongoURL = `mongodb://userevonbookafella:userevonbookafella123@xdb.bookafella.in:1234/bookafella?replicaSet=bookafella`;
    //config.MongoURL = `mongodb://bookfella:evontex123@ds137508.mlab.com:37508/bookfella`;

    config.razorpay = {
        host: "api.razorpay.com/v1",
        baseURL: "https://api.razorpay.com/v1",
        merchant_id: "Diwu5M8mU3Z8KY",
        key_id: "rzp_test_dCzF1rfm0BrX0f",
        key_secret: "OFdq8SlJgmn2rAe5HsChhHzU",
        razorpayx_account_number: "2323230094381768", // testing
        webhook_secret: "indian153"
    }
}



export default config;