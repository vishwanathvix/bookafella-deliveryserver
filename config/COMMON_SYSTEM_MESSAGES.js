let COMMON_SYSTEM_MESSAGES = function () { };

COMMON_SYSTEM_MESSAGES.ORDER_QUEUE_FAILED_MESSAGE = "Could not process order queue due to City already in Use for this Service Type with Other Processing";
COMMON_SYSTEM_MESSAGES.PICKUP_DELIVERY_PRICING_ZONE_ERROR = "We don't serve for given input locations";
COMMON_SYSTEM_MESSAGES.PICKUP_DELIVERY_PRICING_CITY_ERROR = "We don't serve for given input locations because all location must be in the same city.";
COMMON_SYSTEM_MESSAGES.PICKUP_DELIVERY_WINDOW_SLOTS_ZONE_ERROR = "We don't serve for given input locations";
COMMON_SYSTEM_MESSAGES.PICKUP_DELIVERY_WINDOW_SLOTS_CITY_ERROR = "We don't serve for given input locations because all location must be in the same city.";
COMMON_SYSTEM_MESSAGES.APPLY_OFFER_VERSION_MIS_MATCH = "VERSION MISMATCH. PLEASE UPDATE YOUR QUOTE";
COMMON_SYSTEM_MESSAGES.APPLY_OFFER_PRICE_NOT_AVAILABLE = "Price Not Available for Given Inputs";
COMMON_SYSTEM_MESSAGES.INSTANT_ORDER_PLACING_DRIVER_NOT_AVAILABLE = "We can't place your order right now. Please try after some time."
COMMON_SYSTEM_MESSAGES.INSTANT_ORDER_STATUS_EXPIRED = "Instant Order Request Expired. Please try again later."
COMMON_SYSTEM_MESSAGES.INSTANT_ORDER_STATUS_PENDING = "Instant Order Request Initiated. Please be patient until our driver will accept your order.";
COMMON_SYSTEM_MESSAGES.COMMON_ORDER_PLACING_OFFER_VERSION_MIS_MATCH = "VERSION MISMATCH. PLEASE UPDATE YOUR QUOTE";
COMMON_SYSTEM_MESSAGES.COMMON_ORDER_PLACING_INSURANCE_VERSION_MIS_MATCH = "VERSION MISMATCH. PLEASE UPDATE YOUR INSURANCE";

export default COMMON_SYSTEM_MESSAGES;