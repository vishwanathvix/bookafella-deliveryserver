import Mongoose from 'mongoose';
import config from './config';
Mongoose.Promise = global.Promise;
const connectToDb = async() => {
    let MongoURL = config.MongoURL;
    let options = {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
    try {
        await Mongoose.connect(MongoURL, options);
        console.log(`Connected to mongo db--->${MongoURL}`);
    } catch (error) {
        console.error('Could not connect to MongoDB');
    }
}

export default connectToDb;