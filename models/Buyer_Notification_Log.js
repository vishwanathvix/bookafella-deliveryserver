import mongoose from 'mongoose';
const Buyer_Notification_Log = mongoose.Schema({
    NotificationID: { type: String, default: ""},
    BuyerID: { type: String, default: ""},
    NotificationTitle: { type: String, default: ""},
    NotificationBody: { type: String, default: ""},
    NotificationData:{},
    Created_at: { type: Date, default: null},
    Viewed: { type: Boolean, default: false},
}, { collection: 'Buyer_Notification_Log' });
export default mongoose.model('Buyer_Notification_Log', Buyer_Notification_Log);