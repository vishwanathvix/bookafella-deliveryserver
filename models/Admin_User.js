import mongoose from 'mongoose';
const Admin_User = mongoose.Schema({
    AdminID: { type: String, default: "" },
    Whether_Master: { type: Boolean, default: false },
    Admin_Permissions: { },
    Name: { type: String, default: "" },
    EmailID: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    PasswordHash: { type: String, default: "" },
    PasswordSalt: { type: String, default: "" },
    FirstTimeLogin: { type: Boolean, default: true },
    SessionID: { type: String, default: "" },
    WHO_CREATED: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() },
}, { collection: 'Admin_User' });
export default mongoose.model('Admin_User', Admin_User);