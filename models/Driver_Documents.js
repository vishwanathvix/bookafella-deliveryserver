import mongoose from 'mongoose';
const Driver_Documents = mongoose.Schema({
    DriverID: { type: String, default: "" },
    FileID: { type: String, default: "" },
    File_Path: { type: String, default: "" },
    contentType: { type: String, default: "" },
    contentSize: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Documents" });
export default mongoose.model('Driver_Documents', Driver_Documents);