import mongoose from 'mongoose';
const Write_And_Earn_Log = mongoose.Schema({
    LogID: { type: String },
    BuyerID: { type: String, default: "" },
    Buyer_Name: { type: String, default: "" },
    Buyer_PhoneNumber: { type: String, default: "" },
    ProductID: { type: String, default: "" },
    Unique_ProductID: { type: String, default: "" }, //Manual
    Product_Name: { type: String, default: "" },
    Description: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" }, //Manual
    Branch_Name: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    Approve: { type: Number, default: 0 }, //0- pending, 1- Accepted, 2- rejected
    Reward_Points: { type: Number, default: 0 },
    Reward_Amount: { type: Number, default: 0 },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Write_And_Earn_Log' });
export default mongoose.model('Write_And_Earn_Log', Write_And_Earn_Log);