import mongoose from 'mongoose';
const Order_Bulk_Excel_Requests_Orders = mongoose.Schema({
    RequestID: { type: String, default: "" },
    OrderRequestID: { type: String, default: "" },
    Request_Body: {
        USER_API_KEY: { type: String, default: "" },
        ItemID: { type: String, default: "" },
        Weight_Type: { type: Number, default: 1 },
        Item_Gross_Weight: { type: Number, default: 1 },
        pName: { type: String, default: "" },
        pCountryCode: { type: String, default: "" },
        pPhoneNumber: { type: String, default: "" },
        pAddress: { type: String, default: "" },
        dName: { type: String, default: "" },
        dCountryCode: { type: String, default: "" },
        dPhoneNumber: { type: String, default: "" },
        dAddress: { type: String, default: "" }
    },
    Whether_Order_Placed_Successfully: { type: Boolean, default: false },
    Whether_Order_Placed_Response: {
        success: { type: Boolean, default: false },
        extras: {
            Status: { type: String, default: "" },
            msg: { type: Number, default: 0 }
        },
        OrderID: { type: String, default: "" },
        Order_Number: { type: String, default: "" },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Order_Bulk_Excel_Requests_Orders' });
export default mongoose.model('Order_Bulk_Excel_Requests_Orders', Order_Bulk_Excel_Requests_Orders);