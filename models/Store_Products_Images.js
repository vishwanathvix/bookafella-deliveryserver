import mongoose from 'mongoose';
const Store_Products_Images = mongoose.Schema({
    BranchID: { type: String },
    ImageID: { type: String, default: "" },
    ImageURL: { type: String, default: "" },
    Whether_Image_Used: { type: Boolean, default: false },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_Products_Images' });
export default mongoose.model('Store_Products_Images', Store_Products_Images);