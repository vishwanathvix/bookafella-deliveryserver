import mongoose from 'mongoose';
const Country = mongoose.Schema({
    CountryID: { type: String, default: "" },
    CountryNumber: { type: Number, default: 0 },
    CountryName: { type: String, default: "" },
    CountryCode: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Country" });
export default mongoose.model('Country', Country);