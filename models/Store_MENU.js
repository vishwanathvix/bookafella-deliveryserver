import mongoose from 'mongoose';
const Store_MENU = mongoose.Schema({
    MENU_ID: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    MENU_NAME: { type: String, default: "" },
    MENU_DESCRIPTION: { type: String, default: "" },
    MENU_URL: { type: String, default: "" },
    MENU_TYPE: { type: Number, default: 1 },//1.Veg 2.Non Veg
    MENU_PRICE: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_MENU' });
export default mongoose.model('Store_MENU', Store_MENU);