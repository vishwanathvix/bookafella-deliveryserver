import mongoose from 'mongoose';
const Store_Admin_Sessions = mongoose.Schema({
    StoreAdminID: String,
    SessionID: String,
    created_at: Date,
    updated_at: Date
},{ collection: 'Store_Admin_Sessions' });
export default mongoose.model('Store_Admin_Sessions', Store_Admin_Sessions);