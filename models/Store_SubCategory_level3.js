import mongoose from 'mongoose';
const Store_SubCategory_level3 = mongoose.Schema({
    CategoryID: { type: String },
    CategoryName: { type: String, default: "" },
    Level2CategoryID: { type: String },
    Level2CategoryName: { type: String, default: "" },
    Level3CategoryID: { type: String },
    Level3CategoryName: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_SubCategory_level3' });
export default mongoose.model('Store_SubCategory_level3', Store_SubCategory_level3);