import mongoose from 'mongoose';
const Users = mongoose.Schema({
    USERID: { type: String, default: "" },
    USER_SESSIONS: {
        SessionID: { type: String, default: "" }
    },
    Name: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    EmailID: { type: String, default: "" },
    PasswordHash: { type: String, default: "" },
    PasswordSalt: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Users" });
export default mongoose.model('Users', Users);