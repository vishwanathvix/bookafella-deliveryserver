import mongoose from 'mongoose';
const Driver_Salary_Record_Payout_Transactions = mongoose.Schema({
    TransactionID: { type: String, default: "" },
    RazorpayX_TransactionID: { type: String, default: "" },
    All_DRIVER_Payout_ID: { type: String, default: "" },
    DriverDateID: { type: String, default: "" },
    DriverDate: { type: Date, default: null },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    DriverID: { type: String, default: "" },
    DriverRecordID: { type: String, default: "" },
    Actual_Amount: { type: Number, default: 0 },
    Service_Amount: { type: Number, default: 0 },
    Amount: { type: Number, default: 0 },//Rupees //Final Amount for Razorpay
    BeneficiaryID: { type: String, default: "" },
    RazorpayX_BeneficiaryID: { type: String, default: "" },
    RazorpayX_ContactID: { type: String, default: "" },
    BeneficiaryType: { type: Number, default: 1 },//1. Bank Account 2.UPI 
    Name: { type: String, default: "" },
    Account_Number: { type: String, default: "" },
    IFSC: { type: String, default: "" },
    Whether_Default_Account: { type: Boolean, default: false },
    Bank_Details: {
        BANK: { type: String, default: "" },
        BANKCODE: { type: String, default: "" },
        IFSC: { type: String, default: "" },
        CONTACT: { type: String, default: "" },
        BRANCH: { type: String, default: "" },
        ADDRESS: { type: String, default: "" },
        CITY: { type: String, default: "" },
        DISTRICT: { type: String, default: "" },
        STATE: { type: String, default: "" }
    },
    UPI: { type: String, default: "" },
    Transaction_Status: { type: Number, default: 1 }, //0.queue initiated 1. queued 2. processed 3. issued 4.payout initiated 5. reversed 6.created 7.cancelled
    Transaction_Reference_ID: { type: String, default: "" },
    Transaction_Completion_Remarks: { type: String, default: "" },
    Transaction_Status_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        Transaction_Status: { type: Number, default: 1 }, //0.queue initiated 1. queued 2. processed 3. issued 4.payout initiated 5. reversed 6.created 7.cancelled
        Comment: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    RazorpayXPayoutData: {
        id: { type: String, default: "" },
        amount: { type: Number, default: 0 },
        currency: { type: String, default: "INR" },
        fees: { type: Number, default: 0 },
        tax: { type: Number, default: 0 },
        status: { type: String, default: "" },
        utr: { type: String, default: "" },
        mode: { type: String, default: "" },
        reference_id: { type: String, default: "" },
        failure_reason: { type: String, default: "" }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Salary_Record_Payout_Transactions" });
export default mongoose.model('Driver_Salary_Record_Payout_Transactions', Driver_Salary_Record_Payout_Transactions);