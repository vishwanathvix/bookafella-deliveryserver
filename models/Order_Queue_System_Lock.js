import mongoose from 'mongoose';
const Order_Queue_System_Lock = mongoose.Schema({
    CityID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Whether_Queue_Processing_Locked: { type: Boolean, default: false },
    Status: { type: Boolean, default: true }, 
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Order_Queue_System_Lock" });
export default mongoose.model('Order_Queue_System_Lock', Order_Queue_System_Lock);