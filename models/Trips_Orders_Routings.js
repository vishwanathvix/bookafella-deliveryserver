import mongoose from 'mongoose';
const Trips_Orders_Routings = mongoose.Schema({
    TripRouteID: { type: String, default: "" },
    TripID: { type: String, default: "" },
    TripNumber: { type: String, default: "" },
    Sub_Trip_ID: { type: String, default: "" },
    Sub_Trip_Type: { type: Number, default: 1 }, //1.Single Pickup - Single Delivery   2.Single Pickup - Multiple Delivery  3.Multiple Pickup - Single Delivery
    Group_Number: { type: Number, default: 0 },
    DriverID: { type: String, default: "" },
    VehicleID: { type: String, default: "" },
    VehicleNumber: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery 3.Vendor
    USERID: { type: String, default: "" },// User Who Placed Orders
    All_Orders_ID_Array: { type: [String], default: [] },
    Selected_Orders_ID_Array: { type: [String], default: [] },//Backend Purpose
    Un_Selected_Orders_ID_Array: { type: [String], default: [] },//Backend Purpose
    All_Orders_Number_Array: { type: [String], default: [] },
    Route_Action_Next: { type: Number, default: 0 }, // Statuses are below
    Route_Action_Completed: { type: Number, default: 0 }, // Statuses are below
    Route_Action_Completed_Time: { type: Date, default: null },
    Route_Status: { type: Number, default: 1 },//1.Route Created 2.Pickup/Drop Started 3. Reached Location 4.Load/Unload Packages 5.Route Completed  6. Route Cancelled
    Route_Status_Comment: { type: String, default: "" },
    Route_Status_Logs: [
        {
            _id: false,
            LogID: { type: String, default: "" },
            Route_Status: { type: Number, default: 1 },
            Route_Status_Comment: { type: String, default: "" },
            lat: { type: Number, default: 0 },
            lng: { type: Number, default: 0 },
            Time: { type: Date, default: null }
        }
    ],
    Whether_Cancelled: { type: Boolean, default: false },
    CancellationType: { type: Number, default: 1 },//1.Cancelled by Driver 2.Cancelled by Customer 3.Cancelled by Admin
    Route_Number: { type: Number, default: 0 },
    OTP: { type: Number, default: 0 },//OTP to be verified
    //Basic Information
    Name: { type: String, default: "" },
    CountryCode: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    Flat_Details: { type: String, default: "" },
    Address: { type: String, default: "" },
    Landmark: { type: String, default: "" },
    lat: { type: Number, default: 0 },
    lng: { type: Number, default: 0 },
    Point: { type: Array, default: [] },
    Mapping_Information: [{
        OrderID: { type: String, default: "" },
        Order_Number: { type: String, default: "" },
        MainOrderID: { type: String, default: "" },
        Main_Order_Number: { type: String, default: "" },
        Name: { type: String, default: "" },
        CountryCode: { type: String, default: "" },
        PhoneNumber: { type: String, default: "" },
        Flat_Details: { type: String, default: "" },
        Address: { type: String, default: "" },
        Landmark: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 },
        Item_Information: {
            ItemID: { type: String, default: "" },
            CategoryID: { type: String, default: "" },
            Category_Name: { type: String, default: "" },
            Item_Name: { type: String, default: "" },
            Item_Description: { type: String, default: "" },
            Item_Price:{ type : String, default: "" },
            Image_Available: { type: Boolean, default: false },
            Image_Data: {
                ImageID: { type: String, default: "" },
                Image50: { type: String, default: "" },
                Image100: { type: String, default: "" },
                Image250: { type: String, default: "" },
                Image550: { type: String, default: "" },
                Image900: { type: String, default: "" },
                ImageOriginal: { type: String, default: "" }
            },
            Weight_Type: { type: Number, default: 1 },
            Item_Gross_Weight: { type: Number, default: 1 },
            Whether_Dimensions_Available: { type: Boolean, default: false },
            Dimensions: {
                Dimensions_Type: { type: Number, default: 1 },
                Length: { type: Number, default: 0 },
                Breadth: { type: Number, default: 0 },
                Height: { type: Number, default: 0 },
                Item_Volume: { type: Number, default: 0 }
            },
            Whether_Fragile_Item: { type: Boolean, default: false },
            Container_Type: { type: Number, default: 1 }//1. Normal Container 2. Ice Container
        }
    }],
    //Additional Information
    Order_Number: { type: String, default: "" },
    No_of_Orders: { type: Number, default: 0 },
    Total_Item_Gross_Weight: { type: Number, default: 0 },
    Total_Orders_Volume_Available: { type: Number, default: 0 },
    Total_Orders_Volume_Not_Available: { type: Number, default: 0 },
    Total_Available_Item_Gross_Volume: { type: Number, default: 0 },
    Total_Final_Delivery_Price: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Trips_Orders_Routings" });
export default mongoose.model('Trips_Orders_Routings', Trips_Orders_Routings);



/***************************************************************************
 * Route_Action_Next Status
 *  1. Single Pickup
 *  2. Single Drop
 *  3. Group Pickup
 *  4. Group Drop
 *  5. Single Pickup Hub
 *  6. Single Drop Hub
 *  7. Group Pickup Hub
 *  8. Group Drop Hub
 *
 */