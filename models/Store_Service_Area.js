import mongoose from 'mongoose';
const Store_Service_Area = mongoose.Schema({
    ServiceableID: { type: String },
    WHO_CREATED: {type: String},
    AddressID: { type: String, default: "" },
    Address_Content: { type: String, default: "" },
    Latitude: Number,
    Longitude: Number,
    Point: {
        type: [Number],
        index: '2d'
    },
    BranchID: { type: String, default: "" },
    Status: { type: Boolean, default: true }, 
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: "Store_Service_Area" });
Store_Service_Area.index({ Point: '2dsphere' });
export default mongoose.model('Store_Service_Area', Store_Service_Area);