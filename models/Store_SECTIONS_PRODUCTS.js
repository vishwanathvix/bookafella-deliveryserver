import mongoose from 'mongoose';
const Store_SECTIONS_PRODUCTS = mongoose.Schema({
    MENU_ID: { type: String, default: "" },
    SECTION_ID: { type: String, default: "" },
    ProductID: { type: String, default: "" },
    Sl_NO: { type: Number, default: 0 },
    Default: { type: Boolean, default: true },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_SECTIONS_PRODUCTS' });
export default mongoose.model('Store_SECTIONS_PRODUCTS', Store_SECTIONS_PRODUCTS);