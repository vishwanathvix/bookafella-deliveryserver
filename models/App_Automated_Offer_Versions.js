import mongoose from 'mongoose';
const App_Automated_Offer_Versions = mongoose.Schema({
    AutomatedOfferID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Version: { type: Number, default: 1 },
    Discount_Day_Number: { type: Number, default: 0 },
    Discount_Description: { type: String, default: "" },
    Discount_Percentage: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'App_Automated_Offer_Versions' });
export default mongoose.model('App_Automated_Offer_Versions', App_Automated_Offer_Versions);