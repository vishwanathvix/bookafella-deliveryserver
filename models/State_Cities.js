import mongoose from 'mongoose';
const State_Cities = mongoose.Schema({
    StateID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "State_Cities" });
export default mongoose.model('State_Cities', State_Cities);