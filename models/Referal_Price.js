import mongoose from 'mongoose';
const Referal_Price = mongoose.Schema({
    First_Time_Login: { type: Number, default: 1 },
    First_Time_Purchase: { type: Number, default: 1 }
}, { collection: "Referal_Price" });
export default mongoose.model('Referal_Price', Referal_Price);