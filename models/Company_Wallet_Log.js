import mongoose from 'mongoose';
const Company_Wallet_Log = mongoose.Schema({
    LogID: { type: String, default: "" },
    AdminID: { type: String, default: "" },
    Type: { type: Number, default: 1 }, //1.Credited 2.Debited
    Amount: { type: Number, default: 0 },
    Data: {},
    Time: { type: Date, default: null }
}, { collection: "Company_Wallet_Log" });
export default mongoose.model('Company_Wallet_Log', Company_Wallet_Log);