import mongoose from 'mongoose';
const Driver_Contract_Salary_Versions = mongoose.Schema({
    Driver_Contract_Salary_ID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Salary_Type: { type: Number, default: 1 },//1.Driver Salary Rule 2. Driver Vehicle Leased Owner
    Driver_Salary_Rule_ID: { type: String, default: "" },
    Owner_ID: { type: String, default: "" },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Contract_Salary_Versions" });
export default mongoose.model('Driver_Contract_Salary_Versions', Driver_Contract_Salary_Versions);