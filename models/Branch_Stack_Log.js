import mongoose from 'mongoose';
const Branch_Stack_Log = mongoose.Schema({
    BranchStackID: { type: String, default: "" },
    ProductID: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    ETA: {type: Number, default: null},
    Approved:  { type: Number, default: 0 }, //0- pending, 1- Approved, 2 - Rejected
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null },
}, { collection: 'Branch_Stack_Log' });
export default mongoose.model('Branch_Stack_Log', Branch_Stack_Log);