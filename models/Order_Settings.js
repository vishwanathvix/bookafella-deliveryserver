import mongoose from 'mongoose';
const Order_Settings = mongoose.Schema({
    Order_SettingID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Whether_Gig: { type: Boolean, default: true },
    Whether_Schedule: { type: Boolean, default: true },
    Whether_Offer: { type: Boolean, default: true },
    Whether_Insurance: { type: Boolean, default: true },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Order_Settings" });
export default mongoose.model('Order_Settings', Order_Settings);