import mongoose from 'mongoose';
const Driver_Order_OTP = mongoose.Schema({
    OTPID: { type: String, default: "" },
    TripID: { type: String, default: "" },
    TripRouteID: {type: String, default: "" },
    OrderID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    Branch_Name: { type: String, default: "" },
    Branch_PhoneNumber: { type: String, default: "" },
    OTP: { type: Number, default: 0 },
    Whether_OTP_Verified: { type: Boolean, default: false },
    OTP_Verified_Time: { type: Date, default: null },
    Latest: { type: Boolean, default: true },
    Time: { type: Date, default: null }
}, { collection: "Driver_Order_OTP" });
export default mongoose.model('Driver_Order_OTP', Driver_Order_OTP);