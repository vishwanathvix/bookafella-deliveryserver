import mongoose from 'mongoose';
const Vehicles = mongoose.Schema({
    VehicleID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    Vehicle_Name: { type: String, default: "" },
    Vehicle_Description: { type: String, default: "" },
    Vehicle_Type: { type: Number, default: 1 },//1.Two Wheeler 2. Three 3.Four 4.Above Four
    Weight_Type: { type: Number, default: 1 },
    Maximum_Weight: { type: Number, default: 0 },
    Dimensions: {
        Dimensions_Type: { type: Number, default: 1 },
        Length: { type: Number, default: 0 },
        Breadth: { type: Number, default: 0 },
        Height: { type: Number, default: 0 }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Vehicles" });
export default mongoose.model('Vehicles', Vehicles);