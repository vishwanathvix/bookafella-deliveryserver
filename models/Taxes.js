import mongoose from 'mongoose';
const Taxes = mongoose.Schema({
    Bookstore_GST: { type: Number, default: 0 },
    Delivery_GST: { type: Number, default: 0 },
    Status: { type: Boolean, default: true }
}, { collection: "Taxes" });
export default mongoose.model('Taxes', Taxes);