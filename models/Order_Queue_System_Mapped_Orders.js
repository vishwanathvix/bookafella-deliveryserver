import mongoose from 'mongoose';
const Order_Queue_System_Mapped_Orders = mongoose.Schema({
    Order_Queue_ID: { type: String, default: "" },
    Order_Queue_Number: { type: String, default: "" },
    CityID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Order_Queue_Order_Status: { type: Number, default: 1 },//1. Queued 2.Processing 3.Processed(Assigned to Driver) 4. Delivery Pending 5. Delivery Completed  6. Not Processed 7.Driver Vehicle Weight Not Fulfilled
    USERID: { type: String, default: "" },// User Who Placed Orders
    OrderID: { type: String, default: "" },
    Order_Number: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Order_Queue_System_Mapped_Orders' });
export default mongoose.model('Order_Queue_System_Mapped_Orders', Order_Queue_System_Mapped_Orders);