import mongoose from 'mongoose';
const Driver_OTP_Tries = mongoose.Schema({
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    Time: { type: Date, default: null }
}, { collection: "Driver_OTP_Tries" })
export default mongoose.model('Driver_OTP_Tries', Driver_OTP_Tries);