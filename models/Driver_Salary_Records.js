import mongoose from 'mongoose';
const Driver_Salary_Records = mongoose.Schema({
    DriverRecordID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    DriverDateID: { type: String, default: "" },
    DriverDate: { type: Date, default: null },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Whether_Settlement_Completed: { type: Boolean, default: false },
    Whether_Day_Calculation_Completed: { type: Boolean, default: false },
    No_of_Trips: { type: Number, default: 0 },
    No_of_Kilometers: { type: Number, default: 0 },
    Total_Task_Business_Amount: { type: Number, default: 0 },
    Whether_Driver_Contract_Salary_Linked: { type: Boolean, default: false },
    Driver_Contract_Salary_ID: { type: String, default: "" },
    Salary_Type: { type: Number, default: 1 },//1.Driver Salary Rule 2. Driver Vehicle Leased Owner
    Salary_Version: { type: Number, default: 0 },
    Salary_Rule_Data: {
        Driver_Salary_Rule_ID: { type: String, default: "" },
        Driver_Salary_Rule_Title: { type: String, default: "" },
        Service_Type: { type: Number, default: 1 },
        Time_Period_Type: { type: Number, default: 1 },//1.Daily 2.Weekly 3.Monthly 4.Yearly
        Contract_Type: { type: Number, default: 1 },//1.Commision Based 2.FRE
        Leased_Salary_Per_Day: { type: Number, default: 0 },
        Whether_Incentive_Rule: { type: Boolean, default: false },
        Driver_Salary_Qualifier_Type: { type: Number, default: 1 },//1.Tasks 2.Kms
        Current_Incentive_Tier_Number: { type: Number, default: 0 },//Tier Number
        Rule_Tasks_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Tasks: { type: Number, default: 0 },
            Max_Tasks: { type: Number, default: 0 },
            Driver_Share: { type: Number, default: 0 },
            Company_Share: { type: Number, default: 0 }
        },
        Rule_KMS_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Rule_Price_Per_KM: { type: Number, default: 0 }
        },
        Whether_Next_Incentive_Tier_Available: { type: Boolean, default: false },
        Next_Incentive_Tier_Number: { type: Number, default: 0 },//Tier Number
        Next_Rule_Tasks_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Tasks: { type: Number, default: 0 },
            Max_Tasks: { type: Number, default: 0 },
            Driver_Share: { type: Number, default: 0 },
            Company_Share: { type: Number, default: 0 }
        },
        Next_Rule_KMS_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Rule_Price_Per_KM: { type: Number, default: 0 }
        },
        Version: { type: Number, default: 0 },//Versioning
    },
    Leased_Owner_Data: {
        Owner_ID: { type: String, default: "" },
        Owner_Name: { type: String, default: "" },
        Owner_Phone_Number: { type: String, default: "" },
        Owner_GST_Number: { type: String, default: "" },
        Driver_Vehicle_Price_Per_Day: { type: Number, default: 0 },
        Total_Vehicle_and_Drivers: { type: Number, default: 0 },
        Version: { type: Number, default: 0 }
    },
    Complete_Amount_Information: {
        Total_Earned_Amount: { type: Number, default: 0 },
        Total_Collected_Amount: { type: Number, default: 0 },
        Previous_Day_Balance_Amount: { type: Number, default: 0 },
        Bonus_Earned_Amount: { type: Number, default: 0 },
        Current_Balance_Amount: { type: Number, default: 0 }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Salary_Records" });
export default mongoose.model('Driver_Salary_Records', Driver_Salary_Records);