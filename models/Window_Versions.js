import mongoose from 'mongoose';
const Window_Versions = mongoose.Schema({
    WindowID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    Window_Number: { type: Number, default: 0 },
    Window_Name: { type: String, default: "" },
    Window_Timings: {
        Offset_Value: { type: Number, default: 0 },
        Start_at: { type: String, default: "" },
        End_at: { type: String, default: "" }
    },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Window_Versions" });
export default mongoose.model('Window_Versions', Window_Versions);