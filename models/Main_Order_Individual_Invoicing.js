import mongoose from 'mongoose';
const Main_Order_Individual_Invoicing = mongoose.Schema({
    Individual_InvoiceID: { type: String, default: "" },
    Individual_Invoice_Number: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Invoice_ID: { type: String, default: "" },
    Whether_PDF_Generated: { type: Boolean, default: false },
    Individual_Invoice_Number_Link: { type: String, default: "" },
    Current_Invoice_Number: { type: Number, default: 1 },
    // DriverID: { type: String, default: "" },
    // Driver_Name: { type: String, default: "" },
    // Driver_Country_Code: { type: String, default: "" },
    // Driver_Phone_Number: { type: String, default: "" },
    // VehicleID: { type: String, default: "" },
    // Vehicle_Name: { type: String, default: "" },
    // VehicleNumber: { type: String, default: "" },
    Raw_Data: [
        {
            _id: false,
            OrderID: { type: String, default: "" },
            Order_Number: { type: String, default: "" },
            Shipping_Distance: { type: Number, default: 0 },//In kms
            Order_Time: { type: Date, default: null },
            Order_Requested_Time: { type: Date, default: null },
            Order_Accepted_Time: { type: Date, default: null },
            Order_Picked_Time: { type: Date, default: null },
            Order_Completed_Time: { type: Date, default: null },
            Order_Journey_Time: { type: Number, default: 0 },
            Order_Total_Time: { type: Number, default: 0 },
            Order_Journey_Time_String: { type: String, default: '' },
            Order_Total_Time_String: { type: String, default: '' },
            Pickup_Information: {
                PlaceID: { type: String, default: "" },
                Name: { type: String, default: "" },
                CountryCode: { type: String, default: "" },
                PhoneNumber: { type: String, default: "" },
                Flat_Details: { type: String, default: "" },
                Address: { type: String, default: "" },
                Landmark: { type: String, default: "" },
                lat: { type: Number, default: 0 },
                lng: { type: Number, default: 0 }
            },
            Drop_Information: {
                PlaceID: { type: String, default: "" },
                Name: { type: String, default: "" },
                CountryCode: { type: String, default: "" },
                PhoneNumber: { type: String, default: "" },
                Flat_Details: { type: String, default: "" },
                Address: { type: String, default: "" },
                Landmark: { type: String, default: "" },
                lat: { type: Number, default: 0 },
                lng: { type: Number, default: 0 }
            },
            Order_Status: { type: Number, default: 1 },
        }
    ],
    Delivery_Amount_Information: {
        Final_Driver_Delivery_Price: { type: Number, default: 0 },
        Whether_Discount_Offer_Applied: { type: Boolean, default: false },
        Discount_Offer_Percentage: { type: Number, default: 100 },
        Discount_Offer_Amount: { type: Number, default: 0 },
        Whether_Convenience_Fee_Included: { type: Boolean, default: false },
        Convenience_Fee_Prices: { type: Number, default: 0 },
        Insurance_Amount: { type: Number, default: 0 },
        Whether_GST_Included: { type: Boolean, default: false },
        GST_Percentage: { type: Number, default: 0 },
        GST_Amount: { type: Number, default: 0 },
        Whether_Operation_Percentage_Applied: { type: Boolean, default: false },
        Operation_Percentage: { type: Number, default: 2 },
        Operation_Amount: { type: Number, default: 0 },
        Final_Delivery_Price: { type: Number, default: 0 }
    },
    Whether_Amount_Refunded: { type: Boolean, default: false },
    Total_Amount_Refunded: { type: Number, default: 0 },
    Refund_Information: [{
        _id: false,
        RefundID: { type: String, default: "" },// Public link id
        Name: { type: String, default: "" },
        Amount: { type: Number, default: 0 },//Rupees
        created_at: { type: Date, default: null },
    }],
    Whether_Excess_Amount_Added: { type: Boolean, default: false },
    Total_Excess_Amount: { type: Number, default: 0 },
    Excess_Amount_Information: [{
        _id: false,
        InvoiceID: { type: String, default: "" },
        Invoice_Number: { type: String, default: "" },
        Amount: { type: Number, default: 0 },//Rupees
        created_at: { type: Date, default: null },
    }],
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Main_Order_Individual_Invoicing' });
export default mongoose.model('Main_Order_Individual_Invoicing', Main_Order_Individual_Invoicing);