import mongoose from 'mongoose';
const Admin_Notification_Log = mongoose.Schema({
    NotificationID: { type: String, default: "" },
    
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" },
    Branch_Name: { type: String, default: "" },
    
    BuyerID: { type: String, default: "" },
    Buyer_PhoneNumber: { type: String, default: "" },
    Buyer_Name: { type: String, default: "" },
    Buyer_Email: { type: String, default: "" },
    
    DriverID: { type: String, default: "" },
    Driver_Name: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    Driver_EmailID: { type: String, default: "" },
    
    CountryID: { type: String, default: "" },
    CountryName: { type: String, default: "" },

    CityID: { type: String, default: "" },
    CityName: { type: String, default: "" },
    
    ZoneID: { type: String, default: "" },
    Zone_Title: { type: String, default: "" },

    Type: { type: Number, default: 0 },
    NotificationTitle: { type: String, default: "" },
    NotificationBody: { type: String, default: "" },
    NotificationData: {},
    Created_at: { type: Date, default: null },
    Viewed: { type: Boolean, default: false },
}, { collection: 'Admin_Notification_Log' });
export default mongoose.model('Admin_Notification_Log', Admin_Notification_Log);