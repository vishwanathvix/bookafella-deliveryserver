import mongoose from 'mongoose';
const App_Driver_Settings = mongoose.Schema({
    Razorpay_Processing_Fee_Percentage: { type: Number, default: 3 },
    Minimum_Withdraw_Transaction_Amount: { type: Number, default: 100 },//1K
    Maximum_Withdraw_Transaction_Amount: { type: Number, default: 100000 }//1 Lakh
}, { collection: 'App_Driver_Settings' });
export default mongoose.model('App_Driver_Settings', App_Driver_Settings);