import mongoose from 'mongoose';
const Branch_Notification_Log = mongoose.Schema({
    NotificationID: { type: String, default: ""},
    BranchID: { type: String, default: ""},
    Type: { type: Number, default: 0},
    NotificationTitle: { type: String, default: ""},
    NotificationBody: { type: String, default: ""},
    NotificationData:{},
    Created_at: { type: Date, default: null},
    Viewed: { type: Boolean, default: false},
}, { collection: 'Branch_Notification_Log' });
export default mongoose.model('Branch_Notification_Log', Branch_Notification_Log);