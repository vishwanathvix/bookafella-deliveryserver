import mongoose from 'mongoose';
const Admin_Logs = mongoose.Schema({
    AdminID: String,
    Whether_Master: { type: Boolean, default: false },
    Name: { type: String, default: "" },
    EmailID: { type: String, default: "" },
    Message: { type: String, default: "" },
    Key: { type: String, default: "" },
    Purpose: { type: String, default: "" },
    created_at: Date,
    updated_at: Date
}, { collection: 'Admin_Logs' });
export default mongoose.model('Admin_Logs', Admin_Logs);

