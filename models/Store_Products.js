import mongoose from 'mongoose';
const Store_Products = mongoose.Schema({
    ProductID: { type: String, default: ""},
    Unique_ProductID: { type: String, default: ""},
    ZoneID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Product_Name: { type: String, default: "" },
    Product_Edition: { type: String, default: "" },
    Product_Published_On: { type: String, default: "" },
    Product_Publisher: { type: String, default: "" },
    Format_of_the_Book: {type: Number, default: 0}, //1- HardCover 2-PaperBack 3-ScanedCopy 0- Other
    Product_Description: { type: String, default: "" },
    About_Authors: { type: String, default: "" },
    Authors: [{
        _id: false,
        Name: { type: String, default: "" }
    }],
    Total_Pages: { type: Number, default: 0 },
    Product_Image_Data: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" }
    },
    Product_Dimensions: {
        Height: { type: Number },
        Width: { type: Number },
        Length: { type: Number }
    },
    Product_ISBN:[String],
    Avg_Rating: { type: Number, default: 0 },
    Rating: { type: Number, default: 0 },
    CategoryID: { type: String, default: "" },
    Category: { type: String, default: "" },
    SubCategoryID: { type: String, default: "" },
    SubCategory: { type: String, default: "" },
    Rating_Count: { type: Number, default: 0 }, // rating Data in another Schema //rating log
    Product_Data_For_Branch: [{
        _id: false,
        // Approve: { type: Number, default: 0 }, //0- pending, 1- Approved, 2- Declined, 3- blocked
        BranchID: { type: String },
        StoreID: { type: String },
        Branch_Name: { type: String },
        Avaiable_Quantity: { type: Number, default: 0 },
        Used_Quantity: { type: Number, default: 0 },
        Total_Quantity: { type: Number, default: 0 },
        Status: { type: Boolean, default: true },
        Avg_Rating: { type: Number, default: 0 },
        Rating:{type: Number, default: 0}, 
        Rating_Count:{type: Number, default: 0},
        Price:{type: Number, default: 0},
        Branch_Discount_Percent:{type: Number, default: 0},
        Branch_Discount_Price:{type: Number, default: 0},
        Bookafella_Discount_Percent:{type: Number, default: 0},
        Bookafella_Discount_Price:{type: Number, default: 0},
        Total_Discount_Price:{type: Number, default: 0},
        Final_Price:{type: Number, default: 0}, 
        Condition:{type: Number , default: 1}, // 1- new, 2- old
        Admin_Share_Percent:{type: Number , default: 0}, // Commission % 
        Admin_Share_Amount:{type: Number , default: 0}, // Comission Amount
        Branch_Share_Amount:{type: Number , default: 0}, //Amount to Branch
    }],
    Status: { type: Boolean, default: true },
    Views:{type: Number, default: 0},
    // Product_Approve: { type: Number, default: 0 },//0- pending, 1- Approved, 2- Declined
    Who_Created: { type: String, default: "" }, // store admin id
    Who_Approved: { type: String, default: "" }, // admin id
    Whether_Deleted: {type: Boolean, default: false},
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_Products' });
export default mongoose.model('Store_Products', Store_Products);