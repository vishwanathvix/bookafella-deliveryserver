import mongoose from 'mongoose';
const Store_Products_Addons = mongoose.Schema({
    BranchID: { type: String },
    Product_AddonId: { type: String, default: "" },
    Product_Addons: { type: Array, default: [] },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_Products_Addons' });
export default mongoose.model('Store_Products_Addons', Store_Products_Addons);