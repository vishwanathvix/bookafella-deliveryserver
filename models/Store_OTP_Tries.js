import mongoose from 'mongoose';
const Store_OTP_Tries = mongoose.Schema({
    StoreOTPID: { type: String, default: "" },
    Time: { type: Date, default: null }
}, { collection: "Store_OTP_Tries" })
export default mongoose.model('Store_OTP_Tries', Store_OTP_Tries);