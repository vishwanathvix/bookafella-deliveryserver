import mongoose from 'mongoose';
const Special_Offers = mongoose.Schema({
    SpecialOfferID: { type: String, default: "" },
    DiscountID: { type: String, default: "" },
    SpecialOfferNumber: { type: Number, default: 0 },
    SpecialOfferName: { type: String, default: "" },
    ImageData: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" },
        contentType: { type: String, default: "" },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Special_Offers" });
export default mongoose.model('Special_Offers', Special_Offers);