import mongoose from 'mongoose';
const Main_Order_Invoicing = mongoose.Schema({
    Main_Invoice_ID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    USERID: { type: String, default: "" },// User Who Placed Orders
    Sub_Trip_Type: { type: Number, default: 1 },//1.SP-SD 2.SP-MD 3.MP-SD
    Service_Type: { type: Number, default: 1 },//1.Instant 2.Schedule
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    Collection_Type: { type: Number, default: 1 }, // Only used for Cash on Delivery Orders(COD) 1.Pickup 2. Drop
    Whether_Split: { type: Boolean, default: false },
    Name: { type: String, default: "" },
    CountryCode: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    EmailID: { type: String, default: "" },
    Total_Invoices: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Main_Order_Invoicing' });
export default mongoose.model('Main_Order_Invoicing', Main_Order_Invoicing);