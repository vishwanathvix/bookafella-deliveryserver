import mongoose from 'mongoose';
const Admin_Images = mongoose.Schema({
    ImageID: { type: String, default: "" },
    AdminID: { type: String, default: "" },
    Serial_Number: { type: Number, default: 0 },
    Image100: { type: String, default: "" },
    Image250: { type: String, default: "" },
    Image550: { type: String, default: "" },
    Image900: { type: String, default: "" },
    ImageOriginal: { type: String, default: "" },
    ImageUsed: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
}, { collection: 'Admin_Images' });
export default mongoose.model('Admin_Images', Admin_Images);