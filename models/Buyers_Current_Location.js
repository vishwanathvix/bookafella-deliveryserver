import mongoose from 'mongoose';
import { now } from 'moment';
const Buyers_Current_Location = mongoose.Schema({
    BuyerID: { type: String, default: "" }, //Common for both firebase and mongodb
    DeviceID: { type: String, default: "" },
    Buyer_Name: { type: String, default: "" },
    Buyer_Email: { type: String, default: "" },
    Buyer_CountryCode: { type: String, default: "" },
    Buyer_PhoneNumber: { type: String, default: "" },
    Latitude: Number,
    Longitude: Number,
    Point: {
        type: [Number],
        index: '2d'
    },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: null }
}, { collection: "Buyers_Current_Location" });
Buyers_Current_Location.index({ Point: '2dsphere' });
export default mongoose.model('Buyers_Current_Location', Buyers_Current_Location);