import mongoose from 'mongoose';
const Category = mongoose.Schema({
    CategoryID: { type: String, default: "" },
    Category:{ type: String, default: "" },
    Status:{ type: Boolean, default: false },
    SNo: {type: Number, default: 0},
    SubCategory_Data : [{
        _id: false,
        SubCategoryID: {type: String, default: ""},
        SubCategory: {type: String, default: ""}
    }],
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Category" });
export default mongoose.model('Category', Category);
