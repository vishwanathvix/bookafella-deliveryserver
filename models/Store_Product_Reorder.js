import mongoose from 'mongoose';
const Store_Product_Reorder = mongoose.Schema({
    ReorderID: { type: String },
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" }, // manual StoreID
    ProductID: { type: String, default: "" },
    Unique_ProductID: { type: String, default: "" },
    Product_Name: { type: String, default: "" },
    Format_of_the_Book: { type: String, default: "" },
    Reorder_Count: { type: Number, default: 0 },
    ZoneID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    CategoryID: { type: String, default: "" },
    Category: { type: String, default: "" },
    SubCategoryID: { type: String, default: "" },
    SubCategory: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_Product_Reorder' });
export default mongoose.model('Store_Product_Reorder', Store_Product_Reorder);