import mongoose from 'mongoose';
const Driver_Ratings = mongoose.Schema({
    DriverID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    Rating_Points: { type: Number, default: 1 }, // From 1 to 5
    Remarks: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Ratings" });
export default mongoose.model('Driver_Ratings', Driver_Ratings);