import mongoose from 'mongoose';
const Stack_Log = mongoose.Schema({
    StackID: { type: String, default: "" },
    
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" },
    Branch_Name: { type: String, default: "" },

    ProductID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Product_Name: { type: String, default: "" },
    Format_of_the_Book: { type: String, default: "" },
    Unique_ProductID: { type: String, default: "" },
    
    BuyerID: { type: String, default: "" },
   
    Product_Quantity: {type: Number, default: 0},
    Reorder_Quantity: {type: Number, default: 0},
    Fullfilled_Quantity: {type: Number, default: 0},
    Reorder_Status: {type: Number, default: 0}, //0 - Send request, 1-request sent, 2- Accepted, 3- Fullfilled
    Status: { type: Boolean, default: true },
    
    Whether_ETA_Requested: {type: Boolean, default: false},
    Whether_ETA_Updated: {type: Boolean, default: false},
    ETA_Description: {type: String, default: ""},
    ETA: {type: Number, default: null},
    ETA_Date: {type: Date, default: null},
    
    Whether_Branch_ETA_Requested: {type: Boolean, default: false},
    Whether_Branch_ETA_Updated: {type: Boolean, default: false},
    Branch_ETA_Description: {type: String, default: ""},
    Branch_ETA: {type: Number, default: 0},
    Branch_ETA_Date: {type: Date, default: null},
    
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null },
}, { collection: 'Stack_Log' });
export default mongoose.model('Stack_Log', Stack_Log);