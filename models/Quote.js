import mongoose from 'mongoose';
const Quote = mongoose.Schema({
    QuoteID: { type: String, default: "" },
    Quote:{ type: String, default: "" },
    Quote_Author:{ type: String, default: "" },
    
    Status:{ type: Boolean, default: false },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Quote" });
export default mongoose.model('Quote', Quote);
