import mongoose from 'mongoose';
const ZONES = mongoose.Schema({
    ZoneID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    City_Zone_Version: { type: Number, default: 0 },
    Zone_Number: { type: Number, default: 0 },
    Zone_Title: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery
    Polygon_Properties: {
        strokeColor: { type: String, default: "" },
        strokeOpacity: { type: Number, default: 0 },
        strokeWeight: { type: Number, default: 0 },
        fillColor: { type: String, default: "" },
        fillOpacity: { type: Number, default: 0 },
        draggable: { type: Boolean, default: false },
        editable: { type: Boolean, default: false },
        visible: { type: Boolean, default: false }
    },
    Polygon_Paths: [{
        _id: false,
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 }
    }],
    Geometry: {
        type: { type: String, default: "Polygon" },
        coordinates: { type: [[[Number]]], default: [] },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'ZONES' });
ZONES.index({ coordinates: '2dsphere' });
ZONES.index({ Geometry: '2dsphere' });
export default mongoose.model('ZONES', ZONES);