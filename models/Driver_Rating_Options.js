import mongoose from 'mongoose';
const Driver_Rating_Options = mongoose.Schema({
    DriverRatingID: { type: String, default: "" },
    Rating_Question: { type: String, default: "" },
    RatingLevel: { type: Number, default: 1 },//1. Negative 2.Positive
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Rating_Options" });
export default mongoose.model('Driver_Rating_Options', Driver_Rating_Options);