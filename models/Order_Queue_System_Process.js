import mongoose from 'mongoose';
const Order_Queue_System_Process = mongoose.Schema({
    Order_Queue_ID: { type: String, default: "" },
    Order_Queue_Number: { type: String, default: "" },
    CityID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Driver_Information: {
        Total_Available_Drivers: { type: Number, default: 0 },
        Allocated_Drivers: { type: Number, default: 0 },
        Non_Allocated_Drivers: { type: Number, default: 0 },
    },
    Order_Information: {
        Total_Orders: { type: Number, default: 0 },
        Processed_Orders: { type: Number, default: 0 },
        Completed_Orders: { type: Number, default: 0 },
        Delivery_Pending_Orders: { type: Number, default: 0 },
        Non_Processed_Orders: { type: Number, default: 0 },
        Driver_Weight_Unfulfilled_Orders: { type: Number, default: 0 },
    },
    Weight_Information: {
        Total_Weight: { type: Number, default: 0 },
        Allocated_Weight: { type: Number, default: 0 },
        Non_Allocated_Weight: { type: Number, default: 0 },
    },
    Queue_Status: { type: Number, default: 1 }, //1.Initiated 2.Failed  3.Processing 4.Processed
    Queue_Message: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Order_Queue_System_Process' });
export default mongoose.model('Order_Queue_System_Process', Order_Queue_System_Process);