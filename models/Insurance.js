import mongoose from 'mongoose';
const Insurance = mongoose.Schema({
    InsuranceID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Insurance_Percentage: { type: Number, default: 2 },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Insurance" });
export default mongoose.model('Insurance', Insurance);