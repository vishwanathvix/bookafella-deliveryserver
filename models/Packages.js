import mongoose from 'mongoose';
const Packages = mongoose.Schema({
    PackageID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    PackageNumber: { type: Number, default: 0 },
    PackageName: { type: String, default: "" },
    Weight_Type: { type: Number, default: 1 },
    Minimum_Weight: { type: Number, default: 0 },
    Maximum_Weight: { type: Number, default: 0 },
    Dimensions: {
        Dimensions_Type: { type: Number, default: 1 },
        Length: { type: Number, default: 0 },
        Breadth: { type: Number, default: 0 },
        Height: { type: Number, default: 0 }
    },
    Package_Processing_Times: {
        Load_Time: { type: String, default: "" },
        Unload_Time: { type: String, default: "" }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Packages" });
export default mongoose.model('Packages', Packages);