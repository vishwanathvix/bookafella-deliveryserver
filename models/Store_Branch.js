import mongoose from 'mongoose';
const Store_Branch = mongoose.Schema({
    EntityID: { type: String },
    BranchID: { type: String },
    StoreID:{ type: String }, // manual entry by admin
    Store_Entity_Name: { type: String },
    Branch_Name: { type: String },
    Bookstore_Type: { type: Boolean, default: false }, 
    Whether_RazorpayX_Branch_Register: { type: Boolean, default: false },
    RazorpayX_ContactID: { type: String, default: "" },
    Branch_PhoneNumber: { type: String, default: "" },
    BranchURL: { type: String, default: "" },
    Description: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Branch_Gallery_Images:  [{
        ImageID:{ type: String, default: "" },
        Sl_No:{ type: String, default: "" }
    }],
    Branch_Cover_ImageID:  { type: String, default: "" },
    ImageID: { type: String, default: "" },
    FileID: { type: String, default: "" },
    Address: { type: String, default: "" },
    Latitude: Number,
    Longitude: Number,
    Point: {
        type: [Number],
        index: '2d'
    },
    Monday_Available: { type: Boolean, default: true },
    Monday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Tuesday_Available: { type: Boolean, default: true },
    Tuesday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Wednesday_Available: { type: Boolean, default: true },
    Wednesday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Thursday_Available: { type: Boolean, default: true },
    Thursday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Friday_Available: { type: Boolean, default: true },
    Friday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Saturday_Available: { type: Boolean, default: true },
    Saturday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Sunday_Available: { type: Boolean, default: true },
    Sunday_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Today_Working: { type: Boolean, default: true },
    Today_Timings: [{
        From_Time: Date,
        To_Time: Date
    }],
    Status: { type: Boolean, default: true },
    Whether_Branch_Online: { type: Boolean, default: true },
    Branch_Approval_Accepted: { type: Boolean, default: true },
    AdminData: [{
        StoreAdminID: { type: String, default: "" }
    }],
    Branch_Wallet_Information: {
        Available_Amount: { type: Number, default: 0 },//This is main Amount
        Total_Amount: { type: Number, default: 0 },
        Total_Withdrawn_Amount: { type: Number, default: 0 },
        Total_Withdrawn_Service_Tax_Amount: { type: Number, default: 0 },
    },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_Branch' });
Store_Branch.index({ Point: '2dsphere' });
export default mongoose.model('Store_Branch', Store_Branch);