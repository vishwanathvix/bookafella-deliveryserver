import mongoose from 'mongoose';
const Orders = mongoose.Schema({
    OrderID: { type: String, default: "" },
    Order_Number: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },//Main Order ID
    Main_Order_Number: { type: String, default: "" },//Main Order ID
    USERID: { type: String, default: "" },
    Order_Index_Number: { type: Number, default: 0 },
    Average_Distance_Per_Order: { type: Number, default: 0 },//kms
    Average_Final_Driver_Delivery_Price_Per_Order: { type: Number, default: 0 },
    Delivery_Amount_Information: {
        Whether_Fragile_Item: { type: Boolean, default: false },
        BasePriceID: { type: String, default: "" },
        BasePrice_Version: { type: Number, default: 0 },
        ConvenienceFeeID: { type: String, default: "" },
        ConvenienceFee_Version: { type: Number, default: 0 },
        AutomatedOfferID: { type: String, default: "" },
        AutomatedOffer_Version: { type: Number, default: 0 },
        GST_ID: { type: String, default: "" },
        GST_Version: { type: Number, default: 0 },
        DiscountID: { type: String, default: "" },
        Discount_Version: { type: Number, default: 0 },
        Base_Price: { type: Number, default: 0 },
        Distance: { type: Number, default: 0 },
        Duration_Minutes: { type: Number, default: 0 },
        Load_Time: { type: Number, default: 0 },
        Unload_Time: { type: Number, default: 0 },
        Total_Duration_Time: { type: Number, default: 0 },
        Extra_Price_KM: { type: Number, default: 0 },
        Extra_Price_Minutes: { type: Number, default: 0 },
        Extra_Price_KM_Amount: { type: Number, default: 0 },
        Extra_Price_Minutes_Amount: { type: Number, default: 0 },
        Whether_Automated_Offer_Applied: { type: Boolean, default: false },
        Discount_Percentage: { type: Number, default: 0 },
        Automated_Offer_Amount: { type: Number, default: 0 },
        Final_Driver_Delivery_Price: { type: Number, default: 0 },
        Whether_Discount_Offer_Applied: { type: Boolean, default: false },
        Discount_Offer_Percentage: { type: Number, default: 100 },
        Discount_Offer_Amount: { type: Number, default: 0 },
        Whether_Convenience_Fee_Included: { type: Boolean, default: false },
        Convenience_Fee_Prices: { type: Number, default: 0 },
        Insurance_Amount: { type: Number, default: 0 },
        Whether_GST_Included: { type: Boolean, default: false },
        GST_Percentage: { type: Number, default: 0 },
        GST_Amount: { type: Number, default: 0 },
        Whether_Operation_Percentage_Applied: { type: Boolean, default: false },
        Operation_Percentage: { type: Number, default: 2 },
        Operation_Amount: { type: Number, default: 0 },
        Final_Delivery_Price: { type: Number, default: 0 }
    },
    Shipping_Distance: { type: Number, default: 0 },//In kms
    Order_Time: { type: Date, default: null },
    Order_Requested_Time: { type: Date, default: null },
    Order_Accepted_Time: { type: Date, default: null },
    Order_Picked_Time: { type: Date, default: null },
    Order_Completed_Time: { type: Date, default: null },
    Order_Journey_Time: { type: Number, default: 0 },
    Order_Total_Time: { type: Number, default: 0 },
    Order_Journey_Time_String: { type: String, default: '' },
    Order_Total_Time_String: { type: String, default: '' },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    Order_Delivery_Date: { type: Date, default: null }, // If Booking Type 2 End of Day Delivery 
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    Collection_Type: { type: Number, default: 1 }, // Only used for Cash on Delivery Orders(COD) 1.Pickup 2. Drop
    Payment_Type: { type: Number, default: 1 }, //1. Razorpay 2. Paytm
    PaymentID: { type: String, default: "" },
    PaymentData: {},
    Device_Information: {
        DeviceID: { type: String, default: "" },
        DeviceType: { type: Number, default: 1 },//1. Android 2.IOS 3.Web
        DeviceName: { type: String, default: "" },
        AppVersion: { type: Number, default: 0 },
        IPAddress: { type: String, default: "" }
    },
    Item_Information: {
        ItemID: { type: String, default: "" },
        CategoryID: { type: String, default: "" },
        Category_Name: { type: String, default: "" },
        Item_Name: { type: String, default: "" },
        Item_Description: { type: String, default: "" },
        Item_Description_User: { type: String, default: "" },
        Item_Price: { type: String, default: "" },
        Image_Available: { type: Boolean, default: false },
        Image_Data: {
            ImageID: { type: String, default: "" },
            Image50: { type: String, default: "" },
            Image100: { type: String, default: "" },
            Image250: { type: String, default: "" },
            Image550: { type: String, default: "" },
            Image900: { type: String, default: "" },
            ImageOriginal: { type: String, default: "" }
        },
        Weight_Type: { type: Number, default: 1 },
        Item_Gross_Weight: { type: Number, default: 1 },
        Whether_Dimensions_Available: { type: Boolean, default: false },
        Dimensions: {
            Dimensions_Type: { type: Number, default: 1 },
            Length: { type: Number, default: 0 },
            Breadth: { type: Number, default: 0 },
            Height: { type: Number, default: 0 },
            Item_Volume: { type: Number, default: 0 }
        },
        Whether_Fragile_Item: { type: Boolean, default: false },
        Container_Type: { type: Number, default: 1 }//1. Normal Container 2. Ice Container
    },
    Order_Location: {
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 }
    }, // Current Order Location
    Order_Location_Point: { type: [Number], index: '2d' },
    Pickup_Information: {
        PlaceID: { type: String, default: "" },
        Name: { type: String, default: "" },
        CountryCode: { type: String, default: "" },
        PhoneNumber: { type: String, default: "" },
        Flat_Details: { type: String, default: "" },
        Address: { type: String, default: "" },
        Landmark: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 }
    },
    Pickup_Point: { type: [Number], index: '2d' },
    Drop_Information: {
        PlaceID: { type: String, default: "" },
        Name: { type: String, default: "" },
        CountryCode: { type: String, default: "" },
        PhoneNumber: { type: String, default: "" },
        Flat_Details: { type: String, default: "" },
        Address: { type: String, default: "" },
        Landmark: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 }
    },
    Drop_Point: { type: [Number], index: '2d' },
    Order_Status: { type: Number, default: 1 }, //1.Order Placed
    Whether_Driver_Available: { type: Boolean, default: false },
    DriverID: { type: String, default: "" },
    Driver_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        TripID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Order_Status: { type: Number, default: 1 },
        Time: { type: Date, default: null },
    }],
    Event_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Order_Status: { type: Number, default: 1 },
        Comments: { type: String, default: "" },
        Address: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 },
        Time: { type: Date, default: null },
    }],
    Pickup_Zone_Information_Available: { type: Boolean, default: false },
    Pickup_Zone_Information: {
        ZoneID: { type: String, default: "" },
        CountryID: { type: String, default: "" },
        CityID: { type: String, default: "" },
        City_Zone_Version: { type: Number, default: 0 },
        Zone_Number: { type: Number, default: 0 },
        Zone_Title: { type: String, default: "" },
        Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    },
    Drop_Zone_Information_Available: { type: Boolean, default: false },
    Drop_Zone_Information: {
        ZoneID: { type: String, default: "" },
        CountryID: { type: String, default: "" },
        CityID: { type: String, default: "" },
        City_Zone_Version: { type: Number, default: 0 },
        Zone_Number: { type: Number, default: 0 },
        Zone_Title: { type: String, default: "" },
        Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    },
    Whether_Barcode_Number_Available: { type: Boolean, default: false },
    Barcode_Number: { type: String, default: "" },
    Whether_Pickup_Items_Image_Available: { type: Boolean, default: false },
    Pickup_Items_Image_Data: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" }
    },
    Whether_Drop_Items_Image_Available: { type: Boolean, default: false },
    Drop_Items_Image_Data: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" }
    },
    Whether_Receiver_Signature_Image_Available: { type: Boolean, default: false },
    Receiver_Signature_Image_Data: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Orders' });
Orders.index({ Order_Location_Point: '2dsphere' });
Orders.index({ Pickup_Point: '2dsphere' });
Orders.index({ Drop_Point: '2dsphere' });
export default mongoose.model('Orders', Orders);

/*****************************************************
 * Order_Status
 * 
 * 1. Order Placed/Initiated
 * 2. Queued in Driver Trip
 * 3. Trip Initiated
 * 4. Pickup Start
 * 5. Reached at pickup
 * 6. Order Picked(Loaded and Barcoded)
 * 7. Drop Start
 * 8. Reach at drop
 * 9. Order Unloaded(Unloaded and Barcoded)
 * 10. Order ReInitiated or Available for Queue/Routing
 * 15. Order Cancelled
 * 16. Order Cancelled due to Pickup Not Available
 * 20. Order Dropped/Completed
 *
 *
 */