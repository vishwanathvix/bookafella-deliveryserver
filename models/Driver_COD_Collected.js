import mongoose from 'mongoose';
const Driver_COD_Collected = mongoose.Schema({
    DriverID: { type: String, default: "" },
    Whether_Amount_Submited: { type: Boolean, default: true },
    Amount_Collected: { type: Number, default: 0 },
    Delivery_Amount: { type: Number, default: 0 },
    Branch_Amount: { type: Number, default: 0 },
    Admin_Amount: { type: Number, default: 0 },
    Branch_Data: [{
        BranchID: { type: String, default: "" },
        StoreID: { type: String, default: "" },
        Branch_Name: { type: String, default: "" },
        OrderID: { type: String, default: "" },
        Branch_Amount: { type: Number, default: 0 },
    }],
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_COD_Collected" });
export default mongoose.model('Driver_COD_Collected', Driver_COD_Collected);