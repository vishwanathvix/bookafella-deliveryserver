import mongoose from 'mongoose';
const Custom_Notification_Log = mongoose.Schema({
    NotificationID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    SNo: { type: Number, default: 0 },
    BuyerID_Array: [String],
    Notification_Type: { type: Number, default: 0 }, // 1- Zone, 2- All_Users 3- Selected_Users
    NotificationTitle: { type: String, default: "" },
    NotificationBody: { type: String, default: "" },
    NotificationData: {},
    Latest_Send_Time: { type: Date, default: null },
    Status: { type: Boolean, default: true },
    Send_Time_Log: [{ Send_Time: { type: Date, default: null } }],
    updated_at: { type: Date, default: null },
    Created_at: { type: Date, default: null },
}, { collection: 'Custom_Notification_Log' });
export default mongoose.model('Custom_Notification_Log', Custom_Notification_Log);