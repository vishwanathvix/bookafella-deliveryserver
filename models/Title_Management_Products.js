import mongoose from 'mongoose';
const Title_Management_Products = mongoose.Schema({
    TitleID: { type: String, default: "" },
    Title: { type: String, default: "" },
    Description: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    ProductID: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" },
    Branch_Name: { type: String, default: "" },
    Unique_ProductID: { type: String, default: "" },
    Product_Name: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    Format_of_the_Book: { type: Number, default: 0 }, //1- HardCover 2-PaperBack 3-ScanedCopy 0- Other
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Title_Management_Products' });
export default mongoose.model('Title_Management_Products', Title_Management_Products);