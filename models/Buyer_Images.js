import mongoose from 'mongoose';
const Buyer_Images = mongoose.Schema({
    BuyerImageID:  { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    Image_Data: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Buyer_Images" });
export default mongoose.model('Buyer_Images', Buyer_Images);