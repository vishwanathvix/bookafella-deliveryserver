import mongoose from 'mongoose';
const Items = mongoose.Schema({
    ItemID: { type: String, default: "" },
    CategoryID: { type: String, default: "" },
    Category_Name: { type: String, default: "" },
    Item_Name: { type: String, default: "" },
    Item_Weight: { type: Number, default: 0 },
    Item_Description: { type: String, default: "" },
    Whether_Fragile_Item: { type: Boolean, default: false },
    Container_Type: { type: Number, default: 1 },//1. Normal Container 2. Ice Container
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Items' });
export default mongoose.model('Items', Items);