import mongoose from 'mongoose';
const City = mongoose.Schema({
    CityID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityNumber: { type: Number, default: 0 },
    CityName: { type: String, default: "" },
    CountryName: { type: String, default: "" },
    Location: {
        Latitude: { type: Number, default: 0 },
        Longitude: { type: Number, default: 0 }
    },
    Point: { type: [Number], index: '2d' },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "City" });
City.index({ Point: '2dsphere' });
export default mongoose.model('City', City);