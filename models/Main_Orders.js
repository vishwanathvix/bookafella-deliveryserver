import mongoose from 'mongoose';
const Main_Orders = mongoose.Schema({
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    USERID: { type: String, default: "" },// User Who Placed Orders
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Sub_Trip_Type: { type: Number, default: 1 },//1.SP-SD 2.SP-MD 3.MP-SD
    Service_Type: { type: Number, default: 1 },//1.Instant 2.Schedule
    Instant_Details: {
        InstantOrderRequestID: { type: String, default: "" },
        DriverID: { type: String, default: "" }
    },
    Schedule_Details: {
        UserSlotRequestID: { type: String, default: "" },
        Schedule_Date: { type: Date, default: null },
        WindowID: { type: String, default: "" },
        Start_at: { type: String, default: "" },
        End_at: { type: String, default: "" },
        Whether_Slot_Allocation_Fulfilled: { type: Boolean, default: false }
    },
    Main_Order_Placed_Time: { type: Date, default: null },
    Main_Order_Completed_Time: { type: Date, default: null },
    Total_Package_Volume_Weight: { type: Number, default: 0 },//kgs
    Average_Distance_Per_Order: { type: Number, default: 0 },//kms
    Total_Distance: { type: Number, default: 0 },//kms
    Total_Load_Time: { type: Number, default: 0 },//minutes
    Total_Unload_Time: { type: Number, default: 0 },//minutes
    Total_Duration_Minutes: { type: Number, default: 0 },//minutes
    Total_Delivery_Duration_Minutes: { type: Number, default: 0 },//minutes
    Order_Input_Information: [
        {
            _id: false,
            Package_Volume_Weight: { type: Number, default: 0 },
            CategoryID: { type: String, default: "" },
            ItemID: { type: String, default: "" },
            Item_Description_User: { type: String, default: "" },
            Item_Price: { type: Number, default: 0 },
            Weight_Type: { type: Number, default: 0 },
            Item_Gross_Weight: { type: Number, default: 0 },
            Image_Available: { type: Boolean, default: false },
            ImageID: { type: String, default: "" },
            Whether_Dimensions_Available: { type: Boolean, default: false },
            Dimensions_Type: { type: Number, default: 0 },
            Length: { type: Number, default: 0 },
            Breadth: { type: Number, default: 0 },
            Height: { type: Number, default: 0 },
            pPlaceID: { type: String, default: "" },
            pName: { type: String, default: "" },
            pCountryCode: { type: String, default: "" },
            pPhoneNumber: { type: String, default: "" },
            pFlat_Details: { type: String, default: "" },
            pAddress: { type: String, default: "" },
            pLandmark: { type: String, default: "" },
            plat: { type: Number, default: 0 },
            plng: { type: Number, default: 0 },
            dPlaceID: { type: String, default: "" },
            dName: { type: String, default: "" },
            dCountryCode: { type: String, default: "" },
            dPhoneNumber: { type: String, default: "" },
            dFlat_Details: { type: String, default: "" },
            dAddress: { type: String, default: "" },
            dLandmark: { type: String, default: "" },
            dlat: { type: Number, default: 0 },
            dlng: { type: Number, default: 0 }
        }
    ],
    Device_Information: {
        DeviceID: { type: String, default: "" },
        DeviceType: { type: Number, default: 1 },//1. Android 2.IOS 3.Web
        DeviceName: { type: String, default: "" },
        AppVersion: { type: Number, default: 0 },
        IPAddress: { type: String, default: "" }
    },
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    Collection_Type: { type: Number, default: 1 }, // Only used for Cash on Delivery Orders(COD) 1.Pickup 2. Drop
    Payment_Type: { type: Number, default: 1 }, //1. Razorpay 2. Paytm
    PaymentID: { type: String, default: "" },
    PaymentData: {},
    PriceQuoteID: { type: String, default: "" },
    PriceQuoteLogs: [
        {
            _id: false,
            PriceQuoteID: { type: String, default: "" },
            Remarks: { type: String, default: "" },
            Time: { type: Date, default: null }
        }
    ],
    Whether_Insurance_Applied: { type: Boolean, default: false },
    Insurance_Details: {
        UserInsuranceID: { type: String, default: "" },
        InsuranceID: { type: String, default: "" },
        Insurance_Version: { type: Number, default: 0 },
        Insurance_Percentage: { type: Number, default: 0 },
        Total_Package_Amount: { type: Number, default: 0 },
        Final_Insurance_Amount: { type: Number, default: 0 }
    },
    Whether_Offer_Applied: { type: Boolean, default: false },
    Discount_Information: {
        UserDiscountID: { type: String, default: "" },
        DiscountID: { type: String, default: "" }
    },
    Pricing_Information: [{
        _id: false,
        Whether_Fragile_Item: { type: Boolean, default: false },
        BasePriceID: { type: String, default: "" },
        BasePrice_Version: { type: Number, default: 0 },
        ConvenienceFeeID: { type: String, default: "" },
        ConvenienceFee_Version: { type: Number, default: 0 },
        AutomatedOfferID: { type: String, default: "" },
        AutomatedOffer_Version: { type: Number, default: 0 },
        GST_ID: { type: String, default: "" },
        GST_Version: { type: Number, default: 0 },
        DiscountID: { type: String, default: "" },
        Discount_Version: { type: Number, default: 0 },
        Base_Price: { type: Number, default: 0 },
        Distance: { type: Number, default: 0 },
        Duration_Minutes: { type: Number, default: 0 },
        Load_Time: { type: Number, default: 0 },
        Unload_Time: { type: Number, default: 0 },
        Total_Duration_Time: { type: Number, default: 0 },
        Extra_Price_KM: { type: Number, default: 0 },
        Extra_Price_Minutes: { type: Number, default: 0 },
        Extra_Price_KM_Amount: { type: Number, default: 0 },
        Extra_Price_Minutes_Amount: { type: Number, default: 0 },
        Whether_Automated_Offer_Applied: { type: Boolean, default: false },
        Discount_Percentage: { type: Number, default: 0 },
        Automated_Offer_Amount: { type: Number, default: 0 },
        Final_Driver_Delivery_Price: { type: Number, default: 0 },
        Whether_Discount_Offer_Applied: { type: Boolean, default: false },
        Discount_Offer_Percentage: { type: Number, default: 100 },
        Discount_Offer_Amount: { type: Number, default: 0 },
        Whether_Convenience_Fee_Included: { type: Boolean, default: false },
        Convenience_Fee_Prices: { type: Number, default: 0 },
        Insurance_Amount: { type: Number, default: 0 },
        Whether_GST_Included: { type: Boolean, default: false },
        GST_Percentage: { type: Number, default: 0 },
        GST_Amount: { type: Number, default: 0 },
        Whether_Operation_Percentage_Applied: { type: Boolean, default: false },
        Operation_Percentage: { type: Number, default: 2 },
        Operation_Amount: { type: Number, default: 0 },
        Final_Delivery_Price: { type: Number, default: 0 }
    }],
    No_Of_Orders: { type: Number, default: 1 },
    Average_Final_Driver_Delivery_Price_Per_Order: { type: Number, default: 0 },
    Total_Final_Driver_Delivery_Price: { type: Number, default: 0 },
    Total_Final_Delivery_Price: { type: Number, default: 0 },
    Whether_Excess_Amount_Added: { type: Boolean, default: false },
    Total_Excess_Amount_Collected: { type: Number, default: 0 },
    Main_Order_Status: { type: Number, default: 1 },
    Main_Order_Status_Logs: [
        {
            _id: false,
            LogID: { type: String, default: "" },
            Main_Order_Status: { type: Number, default: 1 },
            Time: { type: Date, default: null }
        }
    ],
    Whether_Rating_Available: { type: Boolean, default: false },
    Rating_Review_Information: [{
        DriverID: { type: String, default: "" },
        Rating_Points: { type: Number, default: 1 }, // From 1 to 5
        Remarks: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    Whether_Cancelled: { type: Boolean, default: false },
    Cancellation_Information: {
        ReasonID: { type: String, default: "" },
        Reason_Title: { type: String, default: "" },
        Reason_Description: { type: String, default: "" },
        Comments: { type: String, default: "" },
        Time: { type: Date, default: null }
    },
    Whether_Amount_Refunded: { type: Boolean, default: false },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Main_Orders' });
export default mongoose.model('Main_Orders', Main_Orders);

/*************
 * Main_Order_Status
 *
 *
 *  1. Order Initiated
 *
 *  2. Order Placed
 *
 *  3. Order Tripped
 *
 *  4. Order Delivered
 *
 *  5. Payment Failed
 *
 *  6. Slot allocation failed
 *
 *  7. Main Order Cancelled
 *
 *  8. Awaiting for Cancellation
 */