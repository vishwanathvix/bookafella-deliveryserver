import mongoose from 'mongoose';
const Main_Orders_Collection = mongoose.Schema({
    CollectionID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    USERID: { type: String, default: "" },// User Who Placed Orders
    Service_Type: { type: Number, default: 1 },
    TripID: { type: String, default: "" },
    Sub_Trip_ID: { type: String, default: "" },
    Sub_Trip_Type: { type: Number, default: 1 }, //1.Single Pickup - Single Delivery   2.Single Pickup - Multiple Delivery  3.Multiple Pickup - Single Delivery
    DriverID: { type: String, default: "" },
    Total_Final_Delivery_Price: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Main_Orders_Collection' });
export default mongoose.model('Main_Orders_Collection', Main_Orders_Collection);