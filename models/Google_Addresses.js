import mongoose from 'mongoose';
const Google_Addresses = mongoose.Schema({
    AddressID: { type: String, default: "" },
    Address: { type: String, default: "" },
    lat: { type: Number, default: 0 },
    lng: { type: Number, default: 0 },
    Point: { type: [Number], index: '2d' },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Google_Addresses' });
Google_Addresses.index({ Point: '2dsphere' });
export default mongoose.model('Google_Addresses', Google_Addresses);