import mongoose from 'mongoose';
const App_Pool_Drivers = mongoose.Schema({
    PoolDriverID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    Version: { type: Number, default: 1 },
    Pool_Day_Number: { type: Number, default: 0 },
    Pool_Drivers: { type: Number, default: 0 },
    Non_Pool_Drivers: { type: Number, default: 0 },
    Total_Drivers: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'App_Pool_Drivers' });
export default mongoose.model('App_Pool_Drivers', App_Pool_Drivers);