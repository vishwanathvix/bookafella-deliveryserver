import mongoose from 'mongoose';
const Order_Delivery_Setting = mongoose.Schema({
    DeliverySettingID: { type: String, default: "" },
    Operation_Percentage: { type: Number, default: 5 },
    Gig_Driver_KMS: { type: Number, default: 10 },
    Driver_Order_Near_KMS: { type: Number, default: 1 },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Order_Delivery_Setting" });
export default mongoose.model('Order_Delivery_Setting', Order_Delivery_Setting);