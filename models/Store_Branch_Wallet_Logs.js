import mongoose from 'mongoose';
const Store_Branch_Wallet_Logs = mongoose.Schema({
    LogID: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    Type: { type: Number, default: 1 }, //1. Credited from Razorpay
    Amount: { type: Number, default: 0 },
    Data: {},
    Time: { type: Date, default: null }
}, { collection: "Store_Branch_Wallet_Logs" });
export default mongoose.model('Store_Branch_Wallet_Logs', Store_Branch_Wallet_Logs);