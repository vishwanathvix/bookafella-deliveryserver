import mongoose from 'mongoose';
const Driver_Add_Money_Requests = mongoose.Schema({
    Driver_Add_Money_RequestID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    Amount: { type: Number, default: 0 },
    Razorpay_Processing_Fee_Percentage: { type: Number, default: 5 },
    Razorpay_Processing_Fee_Amount: { type: Number, default: 0 },
    Total_Amount: { type: Number, default: 0 },
    Whether_Request: { type: Boolean, default: true },
    Request_Time: { type: Date, default: null },
    Whether_Payment_Done: { type: Boolean, default: false },
    Payment_Time: { type: Date, default: null },
    PaymentID: { type: String, default: "" },
    PaymentData: {},
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Add_Money_Requests" });
export default mongoose.model('Driver_Add_Money_Requests', Driver_Add_Money_Requests);