import mongoose from 'mongoose';
const Window_Fillings = mongoose.Schema({
    WindowID: { type: String, default: "" },
    Window_DateID: { type: String, default: "" },
    Date: { type: Date, default: null }, //IST format Date
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    No_of_Drivers: { type: Number, default: 0 },
    Weight_Information: {
        Total_Weight: { type: Number, default: 0 },
        Allocated_Weight: { type: Number, default: 0 },
        Non_Allocated_Weight: { type: Number, default: 0 },
    },
    Time_Information: {
        Total_Journey_Time: { type: Number, default: 0 },
        Allocated_Journey_Time: { type: Number, default: 0 },
        Non_Allocated_Journey_Time: { type: Number, default: 0 },
        Each_Driver_Journey_Time: { type: Number, default: 0 }
    },//All in milliseconds
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Window_Fillings" });
export default mongoose.model('Window_Fillings', Window_Fillings);