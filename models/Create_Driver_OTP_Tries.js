import mongoose from 'mongoose';
const Create_Driver_OTP_Tries = mongoose.Schema({
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    Time: { type: Date, default: null }
}, { collection: "Create_Driver_OTP_Tries" })
export default mongoose.model('Create_Driver_OTP_Tries', Create_Driver_OTP_Tries);