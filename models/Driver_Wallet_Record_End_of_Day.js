import mongoose from 'mongoose';
const Driver_Wallet_Record_End_of_Day = mongoose.Schema({
    DriverID: { type: String, default: "" },
    DriverDateID: { type: String, default: "" },
    DriverDate: { type: Date, default: null },
    Driver_Wallet_Information: {
        Available_Amount: { type: Number, default: 0 },//This is main Amount
        Total_Earned_Amount: { type: Number, default: 0 },
        Total_Bonus_Earned_Amount: { type: Number, default: 0 },
        Total_Collected_Amount: { type: Number, default: 0 },
        Total_Wallet_Added_Online_Amount: { type: Number, default: 0 },//Amount Added from Razorpay
        Total_Wallet_Added_Online_Processing_Fee_Amount: { type: Number, default: 0 },//Razorpay Processing Fee
        Total_Withdrawn_Amount: { type: Number, default: 0 },
        Total_Withdrawn_Service_Tax_Amount: { type: Number, default: 0 },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Wallet_Record_End_of_Day" });
export default mongoose.model('Driver_Wallet_Record_End_of_Day', Driver_Wallet_Record_End_of_Day);