import mongoose from 'mongoose';
const Zone_Hubs = mongoose.Schema({
    Zone_Hub_ID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    City_Zone_Hub_Version: { type: Number, default: 0 },
    ZoneID: { type: String, default: "" },
    Zone_Hub_Name: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    Location: {
        Address: { type: String, default: "" },
        Latitude: { type: Number, default: 0 },
        Longitude: { type: Number, default: 0 }
    },
    Point: { type: [Number], index: '2d' },
    Weight_Type: { type: Number, default: 1 },
    Maximum_Weight: { type: Number, default: 0 },
    Dimensions: {
        Dimensions_Type: { type: Number, default: 1 },
        Length: { type: Number, default: 0 },
        Breadth: { type: Number, default: 0 },
        Height: { type: Number, default: 0 }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Zone_Hubs" });
Zone_Hubs.index({ Point: '2dsphere' });
export default mongoose.model('Zone_Hubs', Zone_Hubs);