import mongoose from 'mongoose';
const CustomerPasswordTries = mongoose.Schema({
    Phone : String,
    time: Date
},{ collection: 'CustomerPasswordTries' });
export default mongoose.model('CustomerPasswordTries', CustomerPasswordTries);