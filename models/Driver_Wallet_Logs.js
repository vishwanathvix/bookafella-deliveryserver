import mongoose from 'mongoose';
const Driver_Wallet_Logs = mongoose.Schema({
    LogID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    Type: { type: Number, default: 1 }, //1. Credited from Razorpay
    Amount: { type: Number, default: 0 },
    Data: {},
    Time: { type: Date, default: null }
}, { collection: "Driver_Wallet_Logs" });
export default mongoose.model('Driver_Wallet_Logs', Driver_Wallet_Logs);