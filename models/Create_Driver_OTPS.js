import mongoose from 'mongoose';
const Create_Driver_OTPS = mongoose.Schema({
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    VehicleID: { type: String, default: "" },
    VehicleNumber: { type: String, default: "" },
    Driver_Name: { type: String, default: "" },
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    Driver_EmailID: { type: String, default: "" },
    Address: { type: String, default: "" },
    Latitude: { type: Number, default: 0 },
    Longitude: { type: Number, default: 0 },
    Start_at: { type: String, default: "" },
    End_at: { type: String, default: "" },
    ImageID: { type: String, default: "" },
    Licence_ImageID: { type: String, default: "" },
    FileID_Array: [String],
    OTP: { type: Number, default: 0 },
    Latest: { type: Boolean, default: true },
    Time: { type: Date, default: null },
}, { collection: 'Create_Driver_OTPS' });
export default mongoose.model('Create_Driver_OTPS', Create_Driver_OTPS);