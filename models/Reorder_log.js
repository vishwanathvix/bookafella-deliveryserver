import mongoose from 'mongoose';
const Reorder_Log = mongoose.Schema({
    ReorderLogID: { type: String },
    ReorderID: { type: String },
    BranchID: { type: String, default: "" },
    StoreID: { type: String, default: "" }, // manual StoreID
    ProductID: { type: String, default: "" },
    Unique_ProductID: { type: String, default: "" },
    Product_Name: { type: String, default: "" },
    Format_of_the_Book: { type: String, default: "" },
    Quantity_Intake: { type: Number, default: 0 },
    Quantity_Available: { type: Number, default: 0 },
    BookStore_Reorder: { type: Boolean, default: false },
    ZoneID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    CategoryID: { type: String, default: "" },
    Category: { type: String, default: "" },
    SubCategoryID: { type: String, default: "" },
    SubCategory: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    Reorder_Status : {type: Number, default: 0}, //0- not updated, 1- Stock Available, 2- Stock Partial Available, 3- Stock Not Available
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Reorder_Log' });
export default mongoose.model('Reorder_Log', Reorder_Log);