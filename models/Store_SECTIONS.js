import mongoose from 'mongoose';
const Store_SECTIONS = mongoose.Schema({
    SECTION_ID: { type: String, default: "" },
    Sl_NO: { type: Number, default: 0 },
    MENU_ID: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    SECTION_NAME: { type: String, default: "" },
    SECTION_DESCRIPTION: { type: String, default: "" },
    MAX_SELECTION: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Store_SECTIONS' });
export default mongoose.model('Store_SECTIONS', Store_SECTIONS);