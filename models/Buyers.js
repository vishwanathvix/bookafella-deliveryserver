import mongoose from 'mongoose';
const Buyers = mongoose.Schema({
    BuyerID: { type: String, default: "" }, //Common for both firebase and mongodb
    SessionID: { type: String, default: "" },
    DeviceID: { type: String, default: "" },
    Buyer_Name: { type: String, default: "" },
    Buyer_Email: { type: String, default: "" },
    Buyer_CountryCode: { type: String, default: "" },
    Buyer_PhoneNumber: { type: String, default: "" },
    Ref_PhoneNumber: { type: String, default: "" },
    Buyer_Basic_Info_Available: { type: Boolean, default: false },
    Ref_PhoneNumber_Available: { type: String, default: false },
    Buyer_Password: { type: String, default: "" },
    DOB: { type: Date, default: null },
    Available_Amount: { type: Number, default: 0 },
    Withdrawn_Amount: { type: Number, default: 0 },
    Total_Amount: { type: Number, default: 0 },
    Available_Points: { type: Number, default: 0 },
    Withdrawn_Points: { type: Number, default: 0 },
    Total_Points: { type: Number, default: 0 },
    Orders_Count:{type: Number, default: 0},
    Status: { type: Boolean, default: true },
    Cart_Total_Items: { type: Number, default: 0 },
    Cart_Information: [{
        _id: false,
        ProductID: { type: String, default: "" },
        Product_Name: { type: String, default: "" },
        Product_Quantity: { type: Number, default: 0 },
        BranchID: { type: String, default: "" }
    }],
    My_3_Referals: [{
        _id: false,
        BuyerID: { type: String, default: "" },
        Buyer_Name: { type: String, default: "" },
        Buyer_PhoneNumber: { type: String, default: "" },
        Buyer_CountryCode: { type: String, default: "" },
        wether_Order_Completed:{ type: Boolean, default: false}
    }],
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Buyers" });
export default mongoose.model('Buyers', Buyers);