import mongoose from 'mongoose';
const Title_Management = mongoose.Schema({
    TitleID: { type: String, default: "" },
    Title: { type: String, default: "" },
    Description: { type: String, default: "" },
    SNo: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date },
    updated_at: { type: Date }
}, { collection: 'Title_Management' });
export default mongoose.model('Title_Management', Title_Management);