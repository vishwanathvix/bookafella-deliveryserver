import mongoose from 'mongoose';
const Counters = mongoose.Schema({
    _id: String,
    seq: Number
}, { collection: 'Counters' });
export default mongoose.model('Counters', Counters);