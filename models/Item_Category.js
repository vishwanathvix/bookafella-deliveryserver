import mongoose from 'mongoose';
const Item_Category = mongoose.Schema({
    CategoryID: { type: String, default: "" },
    Category_Name: { type: String, default: "" },
    Whether_Display_All: { type: Boolean, default: true },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Item_Category' });
export default mongoose.model('Item_Category', Item_Category);