import mongoose from 'mongoose';
const Driver_All_Salary_Record_Payouts = mongoose.Schema({
    All_DRIVER_Payout_ID: { type: String, default: "" },
    DriverDateID: { type: String, default: "" },
    DriverDate: { type: Date, default: null },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Total_Drivers: { type: Number, default: 0 },
    Payout_Drivers: { type: Number, default: 0 },
    Non_Payout_Drivers: { type: Number, default: 0 },
    Whether_Payout_Completed: { type: Boolean, default: false },
    Settled_Payout_Amount: { type: Number, default: 0 },
    Non_Settled_Payout_Amount: { type: Number, default: 0 },
    Total_Actual_Payout_Amount: { type: Number, default: 0 },
    Total_Payout_Service_Amount: { type: Number, default: 0 },
    Total_Payout_Amount: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_All_Salary_Record_Payouts" });
export default mongoose.model('Driver_All_Salary_Record_Payouts', Driver_All_Salary_Record_Payouts);