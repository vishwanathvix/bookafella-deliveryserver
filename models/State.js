import mongoose from 'mongoose';
const State = mongoose.Schema({
    StateID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    StateNumber: { type: Number, default: 0 },
    StateName: { type: String, default: "" },
    CountryName: { type: String, default: "" },
    Location: {
        Latitude: { type: Number, default: 0 },
        Longitude: { type: Number, default: 0 }
    },
    Point: { type: [Number], index: '2d' },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "State" });
State.index({ Point: '2dsphere' });
export default mongoose.model('State', State);