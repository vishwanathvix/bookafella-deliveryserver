import mongoose from 'mongoose';
const Company_Wallet_Tranx = mongoose.Schema({
    TransactionID: { type: String, default: "" },
    AdminID: { type: String, default: "" },
    Amount: { type: Number, default: 0 },
    WebHookData: {},
    Status: { type: Boolean, default: false }, 
    Payment_Status: { type: Number, default: 0 }, // 1- initial, 2- fail, 3- Success
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Company_Wallet_Tranx" });
export default mongoose.model('Company_Wallet_Tranx', Company_Wallet_Tranx);