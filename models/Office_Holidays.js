import mongoose from 'mongoose';
const Office_Holidays = mongoose.Schema({
    OfficeHolidayID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Holiday_Date: { type: Date, default: null },
    Reason: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
}, { collection: "Office_Holidays" });
export default mongoose.model('Office_Holidays', Office_Holidays);