import mongoose from 'mongoose';
const Trips = mongoose.Schema({
    TripID: { type: String, default: "" },
    TripNumber: { type: String, default: "" },
    OrderID: { type: String, default: "" },
    /******Branch Information Below */
    // Whether_Vendor_Order_Queue: { type: Boolean, default: false },
    // Order_Queue_ID: { type: String, default: "" },
    // Order_Queue_Number: { type: String, default: "" },
    BranchID: { type: String, default: "" },
    Branch_Name: { type: String, default: "" },
    Branch_PhoneNumber: { type: String, default: "" },
    Branch_Address: { type: String, default: "" },
    /******Branch Information Above */
    DriverID: { type: String, default: "" },
    VehicleID: { type: String, default: "" },
    VehicleNumber: { type: String, default: "" },
    All_Driver_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        VehicleID: { type: String, default: "" },
        VehicleNumber: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery
    // Whether_Trip_Weights_Fulfilled: { type: Boolean, default: false },
    Whether_Trip_Routing_Available: { type: Boolean, default: false },
    Whether_Reported_Trip: { type: Boolean, default: false },
    Total_Trip_Status: { type: Number, default: 1 }, //1.Initiated/Reinitiated Driver Changed 2. Trip Started 3. Trip Completed 4. Trip Reported 
    Driver_Comments: [{
        _id: false,
        CommentID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Driver_Name: { type: String, default: "" },
        Driver_Country_Code: { type: String, default: "" },
        Driver_Phone_Number: { type: String, default: "" },
        Comment: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    Trip_Start_Time: { type: Date, default: null },
    Trip_Completion_Time: { type: Date, default: null },
    /********Instant Information Below */
    Whether_Instant_Trip: { type: Boolean, default: false },
    InstantOrderRequestID: { type: String, default: "" },
    /********Instant Information above */

    /******Window Information Below */
    // Whether_Window_Trip: { type: Boolean, default: false },
    // WindowID: { type: String, default: "" },
    // Window_DateID: { type: String, default: "" },
    // Date: { type: Date, default: null }, //IST format Date
    // Window_Start_Time: { type: Date, default: null },
    // Window_End_Time: { type: Date, default: null },
    // Time_Information: {
    //     Total_Journey_Time: { type: Number, default: 0 },
    //     Allocated_Journey_Time: { type: Number, default: 0 },
    //     Non_Allocated_Journey_Time: { type: Number, default: 0 },
    //     Each_Driver_Journey_Time: { type: Number, default: 0 }
    // },
    /******Window Information Above */
    Total_Eta: { type: Number, default: 0 }, // in min
    Total_Distance: { type: Number, default: 0 },
    Total_Duration: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Trips" });
export default mongoose.model('Trips', Trips);