import mongoose from 'mongoose';
const Wishlist_Log = mongoose.Schema({
    WishlistID: { type: String, default: "" },
    ProductID: { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null },
}, { collection: 'Wishlist_Log' });
export default mongoose.model('Wishlist_Log', Wishlist_Log);