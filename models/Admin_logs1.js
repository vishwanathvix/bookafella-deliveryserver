import mongoose from 'mongoose';
const Admin_logs = mongoose.Schema({
    LogID: { type: String, default: "" },
    AdminID: { type: String, default: "" },
    Whether_God: { type: Boolean, default: false },
    Name: { type: String, default: "" },
    EmailID: { type: String, default: "" },
    LogType: { type: Number, default: 1 },//
    LogSubType: { type: Number, default: 1 },//
    Data: {},
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Admin_logs' });
export default mongoose.model('Admin_logs', Admin_logs);