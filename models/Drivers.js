import mongoose from 'mongoose';
const Drivers = mongoose.Schema({
    DriverID: { type: String, default: "" },
    SessionID: { type: String, default: "" },
    Whether_RazorpayX_Driver_Register: { type: Boolean, default: false },
    RazorpayX_ContactID: { type: String, default: "" },
    DeviceID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery
    Driver_Name: { type: String, default: "" },
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    Driver_EmailID: { type: String, default: "" },
    Driver_Timings: {
        Start_at: { type: String, default: "" },
        End_at: { type: String, default: "" }
    },
    ImageAvailable: { type: Boolean, default: false },
    ImageData: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" },
        contentType: { type: String, default: "" },
    },
    Location: {
        Address: { type: String, default: "" },
        Latitude: { type: Number, default: 0 },
        Longitude: { type: Number, default: 0 }
    },
    Driver_Current_Location: {
        Latitude: { type: Number, default: 0 },
        Longitude: { type: Number, default: 0 }
    },
    Driver_Current_Location_Point: { type: [Number], index: '2d' },
    Point: { type: [Number], index: '2d' },
    VehicleID: { type: String, default: "" },
    VehicleNumber: { type: String, default: "" },
    VehicleHistory: [{
        _id: false,
        VehicleID: { type: String, default: "" },
        VehicleNumber: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    Whether_All_Categories_Delivery: { type: Boolean, default: true },
    Categorories_Data: [
        {
            _id: false,
            CategoryID: { type: String, default: "" },
            Category_Name: { type: String, default: "" }
        }
    ],
    Whether_Driver_Online: { type: Boolean, default: false },
    Whether_App_Login: { type: Boolean, default: false },
    Driver_Device_Information: {
        Last_Login_Time: { type: Date, default: null },
        Last_Logout_Time: { type: Date, default: null },
        Last_Location_Update_Time: { type: Date, default: null }
    },
    Driver_Wallet_Information: {
        Available_Amount: { type: Number, default: 0 },//This is main Amount
        Total_Earned_Amount: { type: Number, default: 0 },
        Total_Bonus_Earned_Amount: { type: Number, default: 0 },
        Total_Collected_Amount: { type: Number, default: 0 },
        Total_Wallet_Added_Online_Amount: { type: Number, default: 0 },//Amount Added from Razorpay
        Total_Wallet_Added_Online_Processing_Fee_Amount: { type: Number, default: 0 },//Razorpay Processing Fee
        Total_Withdrawn_Amount: { type: Number, default: 0 },
        Total_Withdrawn_Service_Tax_Amount: { type: Number, default: 0 },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Drivers" });
Drivers.index({ Point: '2dsphere' });
Drivers.index({ Driver_Current_Location_Point: '2dsphere' });
export default mongoose.model('Drivers', Drivers);