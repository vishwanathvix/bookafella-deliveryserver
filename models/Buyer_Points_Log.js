import mongoose from 'mongoose';
const Buyer_Points_Log = mongoose.Schema({
    LogID: { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    Type: { type: Number, default: 1 }, //1.Credited 2.Debited
    Points: { type: Number, default: 0 },
    Data: {},
    Time: { type: Date, default: null }
}, { collection: "Buyer_Points_Log" });
export default mongoose.model('Buyer_Points_Log', Buyer_Points_Log);



/*********
 *
 *
 *

//Type 1 - Points Credited for Write And Review

 */