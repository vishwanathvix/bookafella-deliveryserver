import mongoose from 'mongoose';
const Driver_Trip_Journey_Route_Collection = mongoose.Schema({
    ReferenceID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    TripID: { type: String, default: "" },
    TripNumber: { type: String, default: "" },
    TripRouteID: { type: String, default: "" },
    Amount: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Trip_Journey_Route_Collection" });
export default mongoose.model('Driver_Trip_Journey_Route_Collection', Driver_Trip_Journey_Route_Collection);