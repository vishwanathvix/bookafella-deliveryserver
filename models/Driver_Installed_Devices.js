import mongoose from 'mongoose';
const Driver_Installed_Devices = mongoose.Schema({
    DeviceID: { type: String, default: "" },
    DeviceType: { type: Number, default: 1 },//1. Android 2.IOS 3. Web
    DeviceName: { type: String, default: "" },
    AppVersion: { type: Number, default: 0 },
    IPAddress: { type: String, default: "" },
    InstallTime: { type: Date, default: null },
    Interval: { type: Number, default: 0 },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Installed_Devices" });
export default mongoose.model('Driver_Installed_Devices', Driver_Installed_Devices);