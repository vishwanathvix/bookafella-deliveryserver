import mongoose from 'mongoose';
const Main_Order_Excess_Amount_Invoice = mongoose.Schema({
    InvoiceID: { type: String, default: "" },
    Invoice_Number: { type: String, default: "" },
    Razorpay_InvoiceID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    Whether_Main_Order_Total: { type: Boolean, default: true },
    OrderID: { type: String, default: "" },
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    DriverID: { type: String, default: "" },
    USERID: { type: String, default: "" },
    Razorpay_CustomerID: { type: String, default: "" },
    Amount: { type: Number, default: 0 },//Rupees
    Excess_Amount_Status: { type: Number, default: 1 }, // 1.Excess Amount Initiated 2.Payment Link Send 3. Amount Paid 4. Expired
    Remark: { type: String, default: "" },
    Excess_Amount_Status_Logs: [
        {
            _id: false,
            LogID: { type: String, default: "" },
            Excess_Amount_Status: { type: Number, default: 1 },
            Remark: { type: String, default: "" },
            Time: { type: Date, default: null }
        }
    ],
    RazorpayInvoiceData: {
        id: { type: String, default: "" },
        entity: { type: String, default: "" },
        amount: { type: Number, default: 0 },
        currency: { type: String, default: "INR" },
        status: { type: String, default: "" },
        order_id: { type: String, default: "" },
        payment_id: { type: String, default: "" },
        short_url: { type: String, default: "" }
    },
    Excess_Amount_Collected_Time: { type: Date, default: null },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Main_Order_Excess_Amount_Invoice" });
export default mongoose.model('Main_Order_Excess_Amount_Invoice', Main_Order_Excess_Amount_Invoice);