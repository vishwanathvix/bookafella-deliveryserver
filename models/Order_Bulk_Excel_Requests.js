import mongoose from 'mongoose';
const Order_Bulk_Excel_Requests = mongoose.Schema({
    RequestID: { type: String, default: "" },
    ReferenceNumber: { type: String, default: "" },
    USERID: { type: String, default: "" },
    filename: { type: String, default: "" },
    contentType: { type: String, default: "" },
    size: { type: Number, default: 0 },//File Size
    Whether_File_Uploaded: { type: Boolean, default: false },
    File_URL: { type: String, default: "" },
    Total_Orders: { type: Number, default: 0 },
    Placed_Orders: { type: Number, default: 0 },
    Failed_Orders: { type: Number, default: 0 },
    Processing_Status: { type: Number, default: 0 }, //1.Initiated 2. Uploaded or Processing 3.Processed
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Order_Bulk_Excel_Requests' });
export default mongoose.model('Order_Bulk_Excel_Requests', Order_Bulk_Excel_Requests);