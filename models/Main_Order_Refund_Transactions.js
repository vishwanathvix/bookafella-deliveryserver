import mongoose from 'mongoose';
const Main_Order_Refund_Transactions = mongoose.Schema({
    TransactionID: { type: String, default: "" },
    RefundID: { type: String, default: "" },// Public link id
    RazorpayX_TransactionID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    Whether_Main_Order_Total_Refund: { type: Boolean, default: true },
    OrderID: { type: String, default: "" },
    PhoneNumber: { type: String, default: "" },
    USERID: { type: String, default: "" },
    Amount: { type: Number, default: 0 },//Rupees
    BeneficiaryID: { type: String, default: "" },
    RazorpayX_BeneficiaryID: { type: String, default: "" },
    RazorpayX_ContactID: { type: String, default: "" },
    BeneficiaryType: { type: Number, default: 1 },//1. Bank Account 2.UPI 
    Name: { type: String, default: "" },
    Account_Number: { type: String, default: "" },
    IFSC: { type: String, default: "" },
    Bank_Details: {
        BANK: { type: String, default: "" },
        BANKCODE: { type: String, default: "" },
        IFSC: { type: String, default: "" },
        CONTACT: { type: String, default: "" },
        BRANCH: { type: String, default: "" },
        ADDRESS: { type: String, default: "" },
        CITY: { type: String, default: "" },
        DISTRICT: { type: String, default: "" },
        STATE: { type: String, default: "" }
    },
    UPI: { type: String, default: "" },
    Refund_Status: { type: Number, default: 1 }, // 1.Refund Initiated 2.Bank Details Added 3. Payment Processed 4. Cancelled
    Remark: { type: String, default: "" },
    Refund_Status_Logs: [
        {
            _id: false,
            LogID: { type: String, default: "" },
            Refund_Status: { type: Number, default: 1 }, // 1.Refund Initiated 2.Bank Details Added 3. Payment Processed 4. Cancelled
            Remark: { type: String, default: "" },
            Time: { type: Date, default: null }
        }
    ],
    Transaction_Status: { type: Number, default: 1 }, // 1. queued 2. processed 3. issued 4.payout initiated 5. reversed 6.created 7.cancelled
    Transaction_Reference_ID: { type: String, default: "" },
    Transaction_Completion_Remarks: { type: String, default: "" },
    Transaction_Status_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        Transaction_Status: { type: Number, default: 1 }, // 1. queued 2. processed 3. issued 4.payout initiated 5. reversed 6.created 7.cancelled
        Comment: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    RazorpayXPayoutData: {
        id: { type: String, default: "" },
        amount: { type: Number, default: 0 },
        currency: { type: String, default: "INR" },
        fees: { type: Number, default: 0 },
        tax: { type: Number, default: 0 },
        status: { type: String, default: "" },
        utr: { type: String, default: "" },
        mode: { type: String, default: "" },
        reference_id: { type: String, default: "" },
        failure_reason: { type: String, default: "" }
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Main_Order_Refund_Transactions" });
export default mongoose.model('Main_Order_Refund_Transactions', Main_Order_Refund_Transactions);