import mongoose from 'mongoose';
import { now } from 'moment';
const Zone_Fees = mongoose.Schema({
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Zone_Title: { type: String, default: "" },
    Delivery_Fee: { type: Number, default: 0 },
    Packaging_Fee: { type: Number, default: 0 },
    Surge_Fee: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: null }
}, { collection: "Zone_Fees" });
export default mongoose.model('Zone_Fees', Zone_Fees);