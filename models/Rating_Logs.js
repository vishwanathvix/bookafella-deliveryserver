import mongoose from 'mongoose';
const Rating_Logs = mongoose.Schema({
    RatingID: { type: String },
    BranchID: { type: String },
    OrderID: { type: String },
    ProductID: { type: String },
    Branch_Name: { type: String },
    BuyerID:{ type: String },
    Buyer_Name:{ type: String },
    Rating: {type: Number},
    Rating_Description: { type: String },
    Time: { type: Date}
}, { collection: 'Rating_Logs' });
export default mongoose.model('Rating_Logs', Rating_Logs);