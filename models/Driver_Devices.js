import mongoose from 'mongoose';
const Driver_Devices = mongoose.Schema({
    ApiKey: { type: String, default: "" },
    DeviceID: { type: String, default: "" },
    DeviceType: { type: Number, default: 1 },//1. Android 2.IOS
    DeviceName: { type: String, default: "" },
    AppVersion: { type: Number, default: 0 },
    IPAddress: { type: String, default: "" },
    FCM_Token: { type: String, default: "" },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Devices" });
export default mongoose.model('Driver_Devices', Driver_Devices);