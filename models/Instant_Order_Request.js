import mongoose from 'mongoose';
const Instant_Order_Request = mongoose.Schema({
    InstantOrderRequestID: { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    // PriceQuoteID: { type: String, default: "" },
    Sub_Trip_Type: { type: Number, default: 1 },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    //Whether_Discount_Apply: { type: Boolean, default: false },
    //UserDiscountID: { type: String, default: "" },
    //Total_Package_Volume_Weight: { type: Number, default: 0 },//kgs
    Total_Distance: { type: Number, default: 0 },//kms
    Total_Time: { type: Number, default: 0 },//minutes
    //Total_Unload_Time: { type: Number, default: 0 },//minutes
    Total_Duration_Minutes: { type: Number, default: 0 },//minutes
    //Total_Delivery_Duration_Minutes: { type: Number, default: 0 },//minutes
    DriverID: { type: String, default: "" },
    // Customer_Selected_Price: { type: Number, default: 0 },
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    ALL_Driver_Information: [
        {
            _id: false,
            DriverID: { type: String, default: "" },
            VehicleID: { type: String, default: "" },
            VehicleNumber: { type: String, default: "" },
            Vehicle_Type: { type: Number, default: 1 },
            // Actual_Maximum_Weight: { type: Number, default: 1 },
            // Maximum_Weight: { type: Number, default: 1 },
            // Minimum_Weight: { type: Number, default: 1 }
        }
    ],
    Instant_Order_Request_Status: { type: Number, default: 1 },//1.Requested 2. Accepted 3.Order Placed 4. Cancelled 5.Drivers Not Available
    Instant_Order_Request_Status_Logs: [{
        _id: false,
        ReferenceID: { type: String, default: "" },
        Instant_Order_Request_Status: { type: Number, default: 1 },
        DriverID: { type: String, default: "" },
        time: { type: Date, default: null }
    }],
    Order_Data: {},
    No_Of_Orders: { type: Number, default: 1 },
    // Order_Input_Information: [
    //     {
    //         _id: false,
    //         Package_Volume_Weight: { type: Number, default: 0 },
    //         CategoryID: { type: String, default: "" },
    //         ItemID: { type: String, default: "" },
    //         Item_Description_User: { type: String, default: "" },
    //         Item_Price: { type: Number, default: 0 },
    //         Weight_Type: { type: Number, default: 0 },
    //         Item_Gross_Weight: { type: Number, default: 0 },
    //         Image_Available: { type: Boolean, default: false },
    //         ImageID: { type: String, default: "" },
    //         Whether_Dimensions_Available: { type: Boolean, default: false },
    //         Dimensions_Type: { type: Number, default: 0 },
    //         Length: { type: Number, default: 0 },
    //         Breadth: { type: Number, default: 0 },
    //         Height: { type: Number, default: 0 },
    //         pPlaceID: { type: String, default: "" },
    //         pName: { type: String, default: "" },
    //         pCountryCode: { type: String, default: "" },
    //         pPhoneNumber: { type: String, default: "" },
    //         pFlat_Details: { type: String, default: "" },
    //         pAddress: { type: String, default: "" },
    //         pLandmark: { type: String, default: "" },
    //         plat: { type: Number, default: 0 },
    //         plng: { type: Number, default: 0 },
    //         dPlaceID: { type: String, default: "" },
    //         dName: { type: String, default: "" },
    //         dCountryCode: { type: String, default: "" },
    //         dPhoneNumber: { type: String, default: "" },
    //         dFlat_Details: { type: String, default: "" },
    //         dAddress: { type: String, default: "" },
    //         dLandmark: { type: String, default: "" },
    //         dlat: { type: Number, default: 0 },
    //         dlng: { type: Number, default: 0 }
    //     }
    // ],
    Whether_Checked: { type: Boolean, default: false },
    Checked_Time: { type: Date, default: null },
    Whether_Request: { type: Boolean, default: false },
    Request_Time: { type: Date, default: null },
    Request_Accepted_Time: { type: Date, default: null },
    Whether_Order_Placed: { type: Boolean, default: false },
    Ordered_Time: { type: Date, default: null },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Instant_Order_Request' });
export default mongoose.model('Instant_Order_Request', Instant_Order_Request);