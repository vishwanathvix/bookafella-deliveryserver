import mongoose from 'mongoose';
const City_Zone_Version = mongoose.Schema({
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },
    Version: { type: Number, default: 0 }
}, { collection: "City_Zone_Version" });
City_Zone_Version.index({ Point: '2dsphere' });
export default mongoose.model('City_Zone_Version', City_Zone_Version);