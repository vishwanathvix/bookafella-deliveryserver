import mongoose from 'mongoose';
const Driver_Location_Logs = mongoose.Schema({
    DriverID: { type: String, default: "" },
    Latitude: { type: Number, default: 0 },
    Longitude: { type: Number, default: 0 },
    Point: { type: [Number], index: '2d' },
    This_Point_Last_Point_Distance: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Location_Logs" });
Driver_Location_Logs.index({ Point: '2dsphere' });
export default mongoose.model('Driver_Location_Logs', Driver_Location_Logs);