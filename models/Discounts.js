import mongoose from 'mongoose';
const Discounts = mongoose.Schema({
    DiscountID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Version: { type: Number, default: 1 },
    Discount_Code: { type: String, default: "" },
    Discount_Title: { type: String, default: ""},
    Discount_Type: { type: Number, default: 1 }, //1.Variable 2.Flat
    Payment_Type: { type: Number, default: 1 },//1.Task 2.Trip
    Minimum_Invoice_Amount: { type: Number, default: 0 },
    Discount_Percentage: { type: Number, default: 100 },
    Max_Discount_Amount: { type: Number, default: 100 },
    Max_Discount_For_User: { type: Number, default: 10 }, //The discount can be use by user in offer period
    Start_Date: { type: Date, default: null },
    End_Date: { type: Date, default: null },
    //Filter Rules
    Whether_Referral_Discount: { type: Boolean, default: false },
    Whether_Customer_Target_Discount: { type: Boolean, default: false },
    Customer_Target_Type: { type: Number, default: 1 },//New Customer 2.Ordered Customer 3.All Customer 4.Selected Customers
    Targeted_Customers: [{
        BuyerID: { type: String, default: "" },
    }],
    Whether_Discount_Age_Filter: { type: Boolean, default: false },
    Minimum_Age: { type: Number, default: 0 },
    Maximum_Age: { type: Number, default: 0 },
    Whether_Order_Filter: { type: Boolean, default: false },
    Minimum_Orders: { type: Number, default: 0 },
    Maximum_Orders: { type: Number, default: 0 },
    Whether_Inactive_Days_Filter: { type: Boolean, default: false },
    Minimum_Days: { type: Number, default: 0 },
    Maximum_Days: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Discounts" });
export default mongoose.model('Discounts', Discounts);