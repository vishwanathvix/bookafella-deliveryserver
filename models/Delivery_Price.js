import mongoose from 'mongoose';
const Delivery_Price = mongoose.Schema({
    Slab_Km: { type: Number, default: 1 },
    Slab_Amount: { type: Number, default: 1 },
    Per_Km_Amount: { type: Number, default: 0 }
}, { collection: "Delivery_Price" });
export default mongoose.model('Delivery_Price', Delivery_Price);