import mongoose from 'mongoose';
const Driver_Licence = mongoose.Schema({
    DriverID: { type: String, default: "" },
    LicenceID: { type: String, default: "" },
    ImageData: {
        ImageID: { type: String, default: "" },
        Image50: { type: String, default: "" },
        Image100: { type: String, default: "" },
        Image250: { type: String, default: "" },
        Image550: { type: String, default: "" },
        Image900: { type: String, default: "" },
        ImageOriginal: { type: String, default: "" },
        contentType: { type: String, default: "" },
    },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Licence" });
export default mongoose.model('Driver_Licence', Driver_Licence);