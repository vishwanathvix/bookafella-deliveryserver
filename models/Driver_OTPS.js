import mongoose from 'mongoose';
const Driver_OTPS = mongoose.Schema({
    Driver_Country_Code: { type: String, default: "" },
    Driver_Phone_Number: { type: String, default: "" },
    OTP: { type: Number, default: 0 },
    Latest: { type: Boolean, default: true },
    Time: { type: Date, default: null }
}, { collection: "Driver_OTPS" });
export default mongoose.model('Driver_OTPS', Driver_OTPS);