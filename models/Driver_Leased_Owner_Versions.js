import mongoose from 'mongoose';
const Driver_Leased_Owner_Versions = mongoose.Schema({
    Owner_ID: { type: String, default: "" },
    Owner_Name: { type: String, default: "" },
    Owner_Phone_Number: { type: String, default: "" },
    Owner_GST_Number: { type: String, default: "" },
    Driver_Vehicle_Price_Per_Day: { type: Number, default: 0 },
    Total_Vehicle_and_Drivers: { type: Number, default: 0 },
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Leased_Owner_Versions" });
export default mongoose.model('Driver_Leased_Owner_Versions', Driver_Leased_Owner_Versions);