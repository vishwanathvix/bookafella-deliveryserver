import mongoose from 'mongoose';
const Driver_Main_Order_Complete_Transaction = mongoose.Schema({
    TransactionRecordID: { type: String, default: "" },
    DriverID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    MainOrderID: { type: String, default: "" },
    Main_Order_Number: { type: String, default: "" },
    USERID: { type: String, default: "" },// User Who Placed Orders
    Sub_Trip_Type: { type: Number, default: 1 },//1.SP-SD 2.SP-MD 3.MP-SD
    Service_Type: { type: Number, default: 1 },//1.Instant 2.Schedule
    Order_Type: { type: Number, default: 1 },//1.Cash 2.Online 3.Monthly Invoice
    Collection_Type: { type: Number, default: 1 }, // Only used for Cash on Delivery Orders(COD) 1.Pickup 2. Drop
    Whether_Driver_Contract_Salary_Linked: { type: Boolean, default: false },
    Driver_Contract_Salary_ID: { type: String, default: "" },
    Salary_Type: { type: Number, default: 1 },//1.Driver Salary Rule 2. Driver Vehicle Leased Owner
    Salary_Version: { type: Number, default: 0 },
    Salary_Rule_Data: {
        Driver_Salary_Rule_ID: { type: String, default: "" },
        Driver_Salary_Rule_Title: { type: String, default: "" },
        Service_Type: { type: Number, default: 1 },
        Time_Period_Type: { type: Number, default: 1 },//1.Daily 2.Weekly 3.Monthly 4.Yearly
        Contract_Type: { type: Number, default: 1 },//1.Commision Based 2.FRE
        Leased_Salary_Per_Day: { type: Number, default: 0 },
        Whether_Incentive_Rule: { type: Boolean, default: false },
        Driver_Salary_Qualifier_Type: { type: Number, default: 1 },//1.Tasks 2.Kms
        Current_Incentive_Tier_Number: { type: Number, default: 0 },//Tier Number
        Rule_Tasks_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Tasks: { type: Number, default: 0 },
            Max_Tasks: { type: Number, default: 0 },
            Driver_Share: { type: Number, default: 0 },
            Company_Share: { type: Number, default: 0 }
        },
        Rule_KMS_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Rule_Price_Per_KM: { type: Number, default: 0 }
        },
        Version: { type: Number, default: 0 },//Rule Versioning
        Whether_Bonus_Calculated: { type: Boolean, default: false },
        Whether_Bonus_Awarded: { type: Boolean, default: false },//If user Received Bonus then bonus will be awarded
        Bonus_Incentive_Tier_Number: { type: Number, default: 0 },//Tier Number
        Bonus_Driver_Salary_Qualifier_Type: { type: Number, default: 1 },//1.Tasks 2.Kms
        Bonus_Rule_Tasks_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Tasks: { type: Number, default: 0 },
            Max_Tasks: { type: Number, default: 0 },
            Driver_Share: { type: Number, default: 0 },
            Company_Share: { type: Number, default: 0 }
        },
        Bonus_Rule_KMS_Data: {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Rule_Price_Per_KM: { type: Number, default: 0 }
        },
        Bonus_Version: { type: Number, default: 0 },//Bonus Versioning
    },
    Leased_Owner_Data: {
        Owner_ID: { type: String, default: "" },
        Owner_Name: { type: String, default: "" },
        Owner_Phone_Number: { type: String, default: "" },
        Owner_GST_Number: { type: String, default: "" },
        Driver_Vehicle_Price_Per_Day: { type: Number, default: 0 },
        Total_Vehicle_and_Drivers: { type: Number, default: 0 },
        Version: { type: Number, default: 0 }
    },
    Total_Sub_Orders: { type: Number, default: 0 },
    Selected_OrderID_Array: { type: [String], default: [] },
    Total_Kms: { type: Number, default: 0 },
    Total_Business_Amount: { type: Number, default: 0 },
    Total_Business_Excess_Amount: { type: Number, default: 0 },
    Final_Total_Business_Amount: { type: Number, default: 0 },
    Total_Collected_Amount: { type: Number, default: 0 },
    Total_Collected_Excess_Amount: { type: Number, default: 0 },
    Final_Total_Collected_Amount: { type: Number, default: 0 },
    Total_Earned_Amount: { type: Number, default: 0 },
    Total_Bonus_Earned_Amount: { type: Number, default: 0 },
    Final_Total_Earned_Amount: { type: Number, default: 0 },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: 'Driver_Main_Order_Complete_Transaction' });
export default mongoose.model('Driver_Main_Order_Complete_Transaction', Driver_Main_Order_Complete_Transaction);