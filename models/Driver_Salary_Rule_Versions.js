import mongoose from 'mongoose';
const Driver_Salary_Rule_Versions = mongoose.Schema({
    Driver_Salary_Rule_ID: { type: String, default: "" },
    Driver_Salary_Rule_Title: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    Time_Period_Type: { type: Number, default: 1 },//1.Daily 2.Weekly 3.Monthly 4.Yearly
    Contract_Type: { type: Number, default: 1 },//1.Commision Based 2.FRE
    Whether_Incentive_Rule: { type: Boolean, default: false },
    Driver_Salary_Qualifier_Type: { type: Number, default: 1 },//1.Tasks 2.Kms
    Rule_Tasks_Data: [
        {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Driver_Share: { type: Number, default: 0 },
            Company_Share: { type: Number, default: 0 }
        }
    ],
    Rule_KMS_Data: [
        {
            ReferenceID: { type: String, default: "" },
            Min_Kms: { type: Number, default: 0 },
            Max_Kms: { type: Number, default: 0 },
            Price_Per_KM: { type: Number, default: 0 }
        }
    ],
    Version: { type: Number, default: 1 },//Versioning
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Driver_Salary_Rule_Versions" });
export default mongoose.model('Driver_Salary_Rule_Versions', Driver_Salary_Rule_Versions);