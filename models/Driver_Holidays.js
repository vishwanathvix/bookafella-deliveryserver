import mongoose from 'mongoose';
const Driver_Holidays = mongoose.Schema({
    DriverID: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    ZoneID: { type: String, default: "" },
    Holiday_Date: { type: Date, default: null },
    Reason: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now() },
    updated_at: { type: Date, default: Date.now() }
}, { collection: "Driver_Holidays" });
export default mongoose.model('Driver_Holidays', Driver_Holidays);