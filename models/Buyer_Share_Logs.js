import mongoose from 'mongoose';
const Buyer_Share_Logs = mongoose.Schema({
    LogID: { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    Type: { type: Number, default: 1 }, 
    Amount: { type: Number, default: 0 },
    Data: {},
    Time: { type: Date, default: null }
}, { collection: "Buyer_Share_Logs" });
export default mongoose.model('Buyer_Share_Logs', Buyer_Share_Logs);



/*********
 *
 *
 *
 * 1. Amount Credited for Referal login
 * 2. 
 * 3. Amount debited from user wallet for completing Order
 * 4. Amount Credited from Order Refund 
 * 5.
 * 6.
 * 7. Amount Credited for Referal First Order
 * 8.
 * 14. Amount Credited from Order Refund
 * 15. Amount Credited for Write And Review
 * 16. Amount Credited By Bookafella
 * 
 */