import mongoose from 'mongoose';
const Buyer_Orders = mongoose.Schema({
    OrderID: { type: String, default: "" },
    BuyerID: { type: String, default: "" },
    Status: { type: Boolean, default: false },
    Order_Status: { type: Number, default: 0 }, //1- started, 2- Accepted, 3- completed 4- Tripped 5- Trip Initiated 6- Pickup Start 7- Reached at pickup 8- Order Picked 9- Drop Start 10- Reach at drop 11- Cancelled
    Order_Report: [{
        _id: false,
        Title: { type: String, default: "" },
        Description: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    Amount_Paid: {
        Amount_Used_From_Wallet: { type: Number, default: 0 },
        Amount_Paid_Online: { type: Number, default: 0 },
        Total_Amount_Paid: { type: Number, default: 0 }
    },
    Payment_Type: { type: Number, default: 0 }, //1- Wallet, 2- RazorPay 3- Wallet and Razorpay 4- COD
    Order_Number: { type: String, default: "" }, // 12 digits random number
    Payment_Status: { type: Number, default: 0 }, // 1- initial, 2- fail, 3- Success, 4- COD
    TransactionID: { type: String, default: "" },
    Delivered_To: { type: String, default: "" },
    Amount_COD: { type: Number, default: 0 },
    Whether_Amount_COD_Received: { type: Boolean, default: false },
    Cancellation_Allowed: { type: Boolean, default: true },
    Cancellation_Allowed_DateTime: { type: Date, default: null },
    Cancel_Status: { type: Number, default: 0 }, //0 - not Cancelled, 1- by Buyer 2- by Admin, 3- By Store, 4- by Driver
    Whether_Cancellation_Prodessed: { type: Number, default: 0 }, //0-pending, 1- Accept, 2- Rejected
    Whether_Cancellation_Refund_Prodessed: { type: Boolean, default: false }, 
    Whether_Cancellation_Notified: { type: Boolean, default: false }, 
    Whether_Cancellation_Notified_Accepted: { type: Boolean, default: false }, 
    Cancellation_Pickup_Status: { type: Number, default: 0 }, //1- initiated 2-Assigned 3-Trip Started 4- pickup Started 5- Pickup reached 6- order picked 7- Drop Started 8- Drop Reached 9-Order Delivered/completed
    Cancellation_Order_Pickup_Report: [{
        _id: false,
        Title: { type: String, default: "" },
        Description: { type: String, default: "" },
        Time: { type: Date, default: null }
    }],
    Whether_Cancellation_Charges_Applied: { type: Boolean, default: false },
    Whether_Cancellation_Charges_For_Next_Order: { type: Boolean, default: false },
    Cancellation_Charges: { type: Number, default: 0 },
    Refund_Amount: { type: Number, default: 0 },
    Cancelled_By: {
        BuyerID: { type: String, default: "" },
        AdminID: { type: String, default: "" },
        BranchID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
    },
    Cancel_Reason: { type: String, default: "" },
    Cancel_Message_Log: [{
        Message: { type: String, default: "" },
        Date: { type: Date, default: null },
        Time: { type: String, default: "" },
        Resend: { type: Boolean, default: false },
        Whether_Edited: { type: Boolean, default: false },
        Orginal_Message: { type: String, default: "" },
    }],
    Whether_Latest_Order: { type: Boolean, default: true },
    Cart_Information: [{
        _id: false,
        ProductID: { type: String, default: "" },
        Unique_ProductID: { type: String, default: "" },
        Product_Name: { type: String, default: "" },
        Product_Edition: { type: String, default: "" },
        Product_Published_On: { type: String, default: "" },
        Product_Publisher: { type: String, default: "" },
        Total_Pages: { type: Number, default: 0 },
        Product_Image_Data: {
            ImageID: { type: String, default: "" },
            Image50: { type: String, default: "" },
            Image100: { type: String, default: "" },
            Image250: { type: String, default: "" },
            Image550: { type: String, default: "" },
            Image900: { type: String, default: "" },
            ImageOriginal: { type: String, default: "" }
        },
        Avg_Rating: { type: Number, default: 0 },
        Total_Rating_Count: { type: Number, default: 0 }, // rating Data in another Schema //rating log
        Price: { type: Number, default: 0 },
        CategoryID: { type: String, default: "" },
        Category: { type: String, default: "" },
        SubCategoryID: { type: String, default: "" },
        SubCategory: { type: String, default: "" },
        Discount_Percent: { type: Number, default: 0 },
        Discount_Price: { type: Number, default: 0 },
        Final_Price: { type: Number, default: 0 },
        Admin_Share_Percent: { type: Number, default: 0 },
        Admin_Share_Amount: { type: Number, default: 0 },
        Branch_Share_Amount: { type: Number, default: 0 },
        Product_Quantity: { type: Number, default: 0 },
        Total_Price: { type: Number, default: 0 },
        Total_Discount_Price: { type: Number, default: 0 },
        Total_Final_Price: { type: Number, default: 0 },
        Total_Admin_Share_Amount: { type: Number, default: 0 },
        Total_Branch_Share_Amount: { type: Number, default: 0 },
    }],
    Order_Invoice: {
        Order_Total_Price: { type: Number, default: 0 },
        Order_Book_Discount: { type: Number, default: 0 },
        Order_Other_Discount: { type: Number, default: 0 },
        Order_Bookstore_Taxes: { type: Number, default: 0 },
        Order_Delivery_Taxes: { type: Number, default: 0 },
        Order_Taxes: { type: Number, default: 0 },
        Order_Delivery_Charge: { type: Number, default: 0 },
        Order_Package_Charge: { type: Number, default: 0 },
        Order_Surge_Charge: { type: Number, default: 0 },
        Previous_Order_Cancellation_Charge: { type: Number, default: 0 },
        Order_Total_Final_Price: { type: Number, default: 0 },
        Order_Total_Admin_Share_Amount: { type: Number, default: 0 },
        Order_Total_Branch_Share_Amount: { type: Number, default: 0 },
    },
    Delivery_Address_Information: {
        AddressID: { type: String, default: "" },
        Name: { type: String, default: "" },
        PhoneNumber: { type: String, default: '' },
        Flat_No: { type: String, default: "" },
        Plot_No: { type: String, default: "" },
        Postal_Code: { type: Number, default: 0 },
        State: { type: String, default: "" },
        City: { type: String, default: "" },
        Land_Mark: { type: String, default: "" },
        Address_Type: { type: String, default: 0 },
        Latitude: Number,
        Longitude: Number,
        Point: {
            type: [Number],
            index: '2d'
        },
    },
    Branch_Information: {
        BranchID: { type: String, default: "" },
        StoreID: { type: String, default: "" },
        Branch_Name: { type: String, default: "" },
        Branch_PhoneNumber: { type: String, default: "" },
        Latitude: Number,
        Longitude: Number,
        Point: {
            type: [Number],
            index: '2d'
        },
        Address: { type: String, default: "" },
    },
    Driver_Information: {},
    Return_Driver_Information: {},
    Driver_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        TripID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Order_Status: { type: Number, default: 1 },
        Time: { type: Date, default: null },
    }],
    Return_Driver_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        TripID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Cancellation_Pickup_Status: { type: Number, default: 1 },
        Time: { type: Date, default: null },
    }],
    Event_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Order_Status: { type: Number, default: 1 },
        Comments: { type: String, default: "" },
        Address: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 },
        Time: { type: Date, default: null },
    }],
    Return_Event_Logs: [{
        _id: false,
        LogID: { type: String, default: "" },
        DriverID: { type: String, default: "" },
        Cancellation_Pickup_Status: { type: Number, default: 1 },
        Comments: { type: String, default: "" },
        Address: { type: String, default: "" },
        lat: { type: Number, default: 0 },
        lng: { type: Number, default: 0 },
        Time: { type: Date, default: null },
    }],
    OTP: { type: Number, default: 0 },
    WebHookData: {},
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Buyer_Orders" });
Buyer_Orders.index({ 'Branch_Information.Point': '2dsphere' });
Buyer_Orders.index({ 'Delivery_Address_Information.Point': '2dsphere' });
Buyer_Orders.index({ Point: '2dsphere' });
export default mongoose.model('Buyer_Orders', Buyer_Orders);