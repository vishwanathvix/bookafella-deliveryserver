import mongoose from 'mongoose';
const Sub_Trips_Orders = mongoose.Schema({
    Sub_Trip_ID: { type: String, default: "" },
    TripID: { type: String, default: "" },
    TripNumber: { type: String, default: "" },
    Order_Queue_ID: { type: String, default: "" },
    Order_Queue_Number: { type: String, default: "" },
    Group_Number: { type: Number, default: 0 },
    Sub_Trip_Type: { type: Number, default: 1 }, //1.Single Pickup - Single Delivery   2.Single Pickup - Multiple Delivery  3.Multiple Pickup - Single Delivery
    DriverID: { type: String, default: "" },
    VehicleID: { type: String, default: "" },
    VehicleNumber: { type: String, default: "" },
    CountryID: { type: String, default: "" },
    CityID: { type: String, default: "" },
    Service_Type: { type: Number, default: 1 },//1. Instant_Delivery 2.End_of_Day_Delivery
    USERID: { type: String, default: "" },// User Who Placed Orders
    OrderID: { type: String, default: "" },
    Order_Number: { type: String, default: "" },
    Status: { type: Boolean, default: true },
    created_at: { type: Date, default: null },
    updated_at: { type: Date, default: null }
}, { collection: "Sub_Trips_Orders" });
export default mongoose.model('Sub_Trips_Orders', Sub_Trips_Orders);