import express from "express";
import UserMediator from "../mediators/UserMediator";
import DeviceMediator from "../mediators/DeviceMediator";
const router = express.Router();

router.post('/Generate_DeviceID', DeviceMediator.Generate_DeviceID); 

router.post('/Splash_Screen', DeviceMediator.Splash_Screen); 

router.post('/Update_Device_FCM_Token', DeviceMediator.Update_Device_FCM_Token); 

router.post('/Generate_Buyer_OTP', UserMediator.Generate_Buyer_OTP); 

router.post('/Validate_Buyer_OTP', UserMediator.Validate_Buyer_OTP); 

router.post('/Buyer_Register', UserMediator.Buyer_Register); 

router.post('/Update_Buyer_Location', UserMediator.Update_Buyer_Location); 

router.post('/Validate_Referral_Phone_Number', UserMediator.Validate_Referral_Phone_Number); 

router.post('/Add_Buyer_Address', UserMediator.Add_Buyer_Address); 

router.post('/Edit_Buyer_Address', UserMediator.Edit_Buyer_Address); 

router.post('/Remove_Buyer_Address', UserMediator.Remove_Buyer_Address); 

router.post('/List_Buyer_Address', UserMediator.List_Buyer_Address); 

router.post('/List_All_Products_Dynamic', UserMediator.List_All_Products_Dynamic); 

router.post('/List_All_Products_Dynamic_Title', UserMediator.List_All_Products_Dynamic_Title); 

router.post('/Get_Single_Product', UserMediator.Get_Single_Product); 


// router.post('/List_All_Products_For_Buyer', UserMediator.List_All_Products_For_Buyer); // based on location

// router.post('/List_All_Rainning_Discount_Products_For_Buyer', UserMediator.List_All_Rainning_Discount_Products_For_Buyer); // based on location

// router.post('/Get_Single_Products_For_Buyer', UserMediator.Get_Single_Products_For_Buyer); // based on location

router.post('/Add_Product_To_Cart', UserMediator.Add_Product_To_Cart); 

router.post('/Remove_Product_From_Cart', UserMediator.Remove_Product_From_Cart);  

router.post('/Remove_All_Product_From_Cart', UserMediator.Remove_All_Product_From_Cart); 

router.post('/List_All_Products_In_Buyer_Cart', UserMediator.List_All_Products_In_Buyer_Cart); 

router.post('/Buyer_Pre_Order_Summary', UserMediator.Buyer_Pre_Order_Summary); 

router.post('/Buyer_Order_Summary', UserMediator.Buyer_Order_Summary); 

router.post('/Buyer_Direct_Order_Summary', UserMediator.Buyer_Direct_Order_Summary); 

router.post('/Buyer_ReOrder_Summary', UserMediator.Buyer_ReOrder_Summary);  

router.post('/Payment_for_Buyer_Order', UserMediator.Payment_for_Buyer_Order); 

router.post('/Payment_for_Buyer_Direct_Order', UserMediator.Payment_for_Buyer_Direct_Order); 

router.post('/Payment_for_Buyer_ReOrder', UserMediator.Payment_for_Buyer_ReOrder); 

router.post('/List_Buyer_Orders', UserMediator.List_Buyer_Orders); 

router.post('/Get_Buyer_Single_Order', UserMediator.Get_Buyer_Single_Order); 

// router.post('/Check_Cancellation_Refund_Amount', UserMediator.Check_Cancellation_Refund_Amount); //updated in doc 

router.post('/Check_Cancellation_Timer', UserMediator.Check_Cancellation_Timer); 

router.post('/Cancel_Single_Order', UserMediator.Cancel_Single_Order);  

router.post('/List_Buyer_Cancelled_Orders', UserMediator.List_Buyer_Cancelled_Orders); 

router.post('/Home_Screen_Details', UserMediator.Home_Screen_Details); 

router.post('/Buyer_Wallet_log', UserMediator.Buyer_Wallet_log); 

router.post('/List_All_Help_Data', UserMediator.List_All_Help_Data); 

router.post('/Product_Rating_On_Order', UserMediator.Product_Rating_On_Order); 

router.post('/List_All_Buyer_Rating', UserMediator.List_All_Buyer_Rating); 

router.post('/List_Product_Rating', UserMediator.List_Product_Rating); 

router.post('/Update_Buyer_Image', UserMediator.Update_Buyer_Image);                                                                                                                                                                                                                                                                                                            

router.post('/Update_Buyer_Profile', UserMediator.Update_Buyer_Profile);  

router.post('/Get_Buyer_Recent_Orders', UserMediator.Get_Buyer_Recent_Orders); 

router.post('/List_All_Banner', UserMediator.List_All_Banner); 

router.post('/Get_Quote', UserMediator.Get_Quote); 

router.post('/List_All_Categories', UserMediator.List_All_Categories); 

router.post('/Get_Similar_Products', UserMediator.Get_Similar_Products); 

router.post('/List_All_Available_Discounts', UserMediator.List_All_Available_Discounts); 

router.post('/Apply_Discount_Offer', UserMediator.Apply_Discount_Offer); 

router.post('/Add_Product_To_Wishlist', UserMediator.Add_Product_To_Wishlist); 

router.post('/List_Product_In_Wishlist', UserMediator.List_Product_In_Wishlist); //updated in doc

router.post('/Remove_Product_In_Wishlist', UserMediator.Remove_Product_In_Wishlist); 

router.post('/List_User_Stack_Items', UserMediator.List_User_Stack_Items); 

router.post('/Request_Stack_ETA', UserMediator.Request_Stack_ETA); 

router.post('/Remove_Stack', UserMediator.Remove_Stack); 

router.post('/Search_All_Products_For_Buyer', UserMediator.Search_All_Products_For_Buyer); 

// For Socail media login

router.post('/Validate_User_Credentials', UserMediator.Validate_User_Credentials);

router.post('/Add_Write_And_Earn', UserMediator.Add_Write_And_Earn); 

router.post('/List_All_Buyer_Notifications', UserMediator.List_All_Buyer_Notifications); 


export default router;