import express from "express";
import StoreMediator from "../mediators/StoreMediator";
import RazorpayController from '../controllers/RazorpayController'
const router = express.Router();

// router.get('/', function(req, res){
//     res.render('catere', {foo:"foose ball"});
//  });

//Store Management
// router.post('/CREATE_MENU', StoreMediator.Create_Menu);

// router.post('/List_ALL_Menu', StoreMediator.List_All_Menu);

// router.post('/Create_MENU_SECTION', StoreMediator.Create_MENU_SECTION);

// router.post('/List_ALL_Menu_Sections', StoreMediator.List_ALL_Menu_Sections);

///

// router.post('/Add_Store_Product', StoreMediator.Add_Store_Product); //

// router.post('/Add_Store_Product_Existing', StoreMediator.Add_Store_Product_Existing); //

router.post('/Find_All_Store_Products', StoreMediator.Find_All_Store_Products); // updated in doc

// router.post('/Find_All_Products', StoreMediator.Find_All_Products); //

// router.post('/Find_All_Products_Lite', StoreMediator.Find_All_Products_Lite); //

router.post('/Get_Single_Product_Details', StoreMediator.Get_Single_Product_Details); // updated in doc

// router.post('/Update_Store_Product', StoreMediator.Update_Store_Product); //

// router.post('/Delete_Store_Product', StoreMediator.Delete_Store_Product); //

router.post('/List_Branch_Orders', StoreMediator.List_Branch_Orders); // updated in doc

router.post('/Accept_Orders', StoreMediator.Accept_Orders); // upddated in doc

router.post('/Cancel_Order_Request', StoreMediator.Cancel_Order_Request); // upddated in doc

router.post('/List_Branch_Cancelled_Orders', StoreMediator.List_Branch_Cancelled_Orders); // updated in doc

router.post('/Get_Single_Orders', StoreMediator.Get_Single_Orders); // updated in doc

router.post('/List_All_Notified_Cancelled_Orders', StoreMediator.List_All_Notified_Cancelled_Orders); // updated in doc

router.post('/Accept_Notified_Cancelled_Order', StoreMediator.Accept_Notified_Cancelled_Order); // updated in doc

router.post('/Validate_Driver_OTP_For_Pickup', StoreMediator.Validate_Driver_OTP_For_Pickup); // updated in doc

router.post('/Add_Branch_Beneficiary_Account_For_Bank_Account', StoreMediator.Add_Branch_Beneficiary_Account_For_Bank_Account); // updated in doc 

router.post('/List_All_Branch_Beneficiary_Accounts', StoreMediator.List_All_Branch_Beneficiary_Accounts); // updated in doc

router.post('/Make_Default_Branch_Beneficiary_Account_For_Bank_Account', StoreMediator.Make_Default_Branch_Beneficiary_Account_For_Bank_Account); // updated in doc

router.post('/Update_Branch_Beneficiary_Account_For_Bank_Account', StoreMediator.Update_Branch_Beneficiary_Account_For_Bank_Account); // updated in doc

router.post('/Delete_Branch_Beneficiary_Account', StoreMediator.Delete_Branch_Beneficiary_Account); // updated in doc

router.post('/Branch_Withdraw_Amount', StoreMediator.Branch_Withdraw_Amount); //

router.post('/List_All_Categories', StoreMediator.List_All_Categories); // updated in doc

router.post('/Branch_Update_Online_Offline', StoreMediator.Branch_Update_Online_Offline); // updated in doc

router.post('/List_Branch_Stack_Log', StoreMediator.List_Branch_Stack_Log); // update din doc

router.post('/Update_Stack_ETA', StoreMediator.Update_Stack_ETA); // updated in doc

router.post('/Fullfill_Stack_Quantity', StoreMediator.Fullfill_Stack_Quantity); // updated in doc

router.post('/Get_Branch_Wallet', StoreMediator.Get_Branch_Wallet); //

router.post('/List_Branch_Wallet_Log', StoreMediator.List_Branch_Wallet_Log); //

router.post('/List_All_Branch_Reorder', StoreMediator.List_All_Branch_Reorder); // updated in doc

router.post('/Update_Branch_Product_Reorder', StoreMediator.Update_Branch_Product_Reorder); // updated in doc

router.post('/Get_Branch_Data', StoreMediator.Get_Branch_Data); // updated in doc

router.post('/Branch_Password_Change', StoreMediator.Branch_Password_Change); // updated in doc

// router.post('/Link_MENU_SECTION_PRODUCT', StoreMediator.Link_MENU_SECTION_PRODUCT); 

// router.post('/List_ALL_Section_Products', StoreMediator.List_ALL_Section_Products);

// router.post('/Update_MENU', StoreMediator.Update_MENU);

// router.post('/INACTIVATE_STORE_MENU', StoreMediator.INACTIVATE_STORE_MENU);

// router.post('/List_ALL_Inactive_Menu', StoreMediator.List_ALL_Inactive_Menu);

// router.post('/ACTIVATE_STORE_MENU', StoreMediator.ACTIVATE_STORE_MENU);

// router.post('/UPDATE_MENU_SECTION', StoreMediator.UPDATE_MENU_SECTION);

// router.post('/INACTIVATE_MENU_SECTION', StoreMediator.INACTIVATE_MENU_SECTION);

// router.post('/ACTIVATE_MENU_SECTION', StoreMediator.ACTIVATE_MENU_SECTION);

// router.post('/List_ALL_Inactive_Menu_Sections', StoreMediator.List_ALL_Inactive_Menu_Sections);

// router.post('/Search_All_Store_Branch_Products', StoreMediator.Search_All_Store_Branch_Products);

// router.post('/Unlink_MENU_SECTION_PRODUCT', StoreMediator.Unlink_MENU_SECTION_PRODUCT);

// router.post('/Menu_URL_Validation', StoreMediator.Menu_URL_Validation);

// router.post('/StoreAdmin_Branches', StoreMediator.StoreAdmin_Branches);

// router.post('/Store_Admin_Password_Change', StoreMediator.Store_Admin_Password_Change);

// router.post('/List_Menu', StoreMediator.List_Menu);

// router.post('/Suggeste_Serial_Number', StoreMediator.Suggeste_Serial_Number);

// router.post('/Check_Serial_Number_Available', StoreMediator.Check_Serial_Number_Available);

// router.post('/Sc_Suggeste_Serial_Number', StoreMediator.Sc_Suggeste_Serial_Number);

// router.post('/Sc_Check_Serial_Number_Available', StoreMediator.Sc_Check_Serial_Number_Available);

// router.post('/List_All_Orders', StoreMediator.List_All_Orders);

// router.post('/CallbackFunctionality', RazorpayController.CallbackFunctionality)

router.post('/List_All_Branch_Notifications', StoreMediator.List_All_Branch_Notifications)

export default router;