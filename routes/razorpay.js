import express from "express";
import RazorpayController from "../controllers/RazorpayController";
const router = express.Router();

router.post('/callback', RazorpayController.CallbackFunctionality);

export default router;