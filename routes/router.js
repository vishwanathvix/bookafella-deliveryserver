import express from "express";
let router = express.Router();
import AppRouter from "./app";
import AdminRouter from "./admin";
import StoreRouter from "./store";
import UploadRouter from "./upload";
import DriverRouter from "./driver";
import RazorpayRouter from "./razorpay";
import AdminController from '../controllers/AdminController';
import StoreController from '../controllers/StoreController';
import ApiMessages from '../models/ApiMessages';
import WebsiteMediator from '../mediators/WebsiteMediator';

//Mobile Applications Apis
router.use('/app', AppRouter);

//Admin Dashboard Api's
router.use('/admin', AdminRouter);

//Store Admin Api's
router.use('/store', StoreRouter);

//Driver Application Api's
router.use('/driver', DriverRouter);

//Upload Router
router.use('/upload', UploadRouter);


//Razorpay Callback Api
router.use('/razorpay', RazorpayRouter);

//Web Api's

router.get('/demo', WebsiteMediator.Get_Home_Page);

router.get('/:BranchURL', WebsiteMediator.Get_Branch_Render_Data);

router.get('/ca/:BranchURL', WebsiteMediator.Get_Branch_json_Data);

router.post('/Fetch_All_Nearest_Branches', WebsiteMediator.Fetch_All_Nearest_Branches);

router.post('/Generate_User_OTP', WebsiteMediator.Generate_User_OTP);

router.post('/Register', WebsiteMediator.Register);

router.post('/Login', WebsiteMediator.Login);

router.post('/Create_Order', WebsiteMediator.Create_Order);

router.post('/List_All_Orders', WebsiteMediator.List_All_Orders);

export default router;