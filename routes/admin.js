import express from "express";
import AdminMediator from "../mediators/AdminMediator";
const router = express.Router();

// router.get('/', function(req, res){
//     res.render('catere', {foo:"foose ball"});
//  });

//Admin Management

router.post('/Create_Admin_User', AdminMediator.Create_Admin_User); 

router.post('/List_All_Admin_Users', AdminMediator.List_All_Admin_Users); 

router.post('/Edit_Admin_Users', AdminMediator.Edit_Admin_Users); 

//router.post('/Admin_Upload_Image', AdminMediator.Admin_Upload_Image);

router.post('/Check_for_StoreID_Availability', AdminMediator.Check_for_StoreID_Availability); 

router.post('/Create_Store', AdminMediator.Create_Store); 

router.post('/Create_Store_Validate_OTP', AdminMediator.Create_Store_Validate_OTP); 

router.post('/Branch_Cover_Image_Data_Update', AdminMediator.Branch_Cover_Image_Data_Update); 

router.post('/Branch_Gallery_Image_Data_Update', AdminMediator.Branch_Gallery_Image_Data_Update); 

router.post('/Store_Admin_Login', AdminMediator.Store_Admin_Login); 

router.post('/Branch_URL_Validation', AdminMediator.Branch_URL_Validation); 

router.post('/Branch_In_Detail', AdminMediator.Branch_In_Detail); 

router.post('/Admin_Login', AdminMediator.Admin_Login); 

router.post('/List_All_Stores', AdminMediator.List_All_Stores); 

router.post('/List_All_Branches', AdminMediator.List_All_Branches); 

router.post('/Activate_Inactivate_Branch', AdminMediator.Activate_Inactivate_Branch); 

router.post('/List_All_Branches_Lite', AdminMediator.List_All_Branches_Lite); 

router.post('/Find_All_Products_Lite', AdminMediator.Find_All_Products_Lite); 

router.post('/Check_for_Store_ProductID_Availability', AdminMediator.Check_for_Store_ProductID_Availability); 

router.post('/Add_Store_Product', AdminMediator.Add_Store_Product); 

// router.post('/Add_Store_Product_Existing', AdminMediator.Add_Store_Product_Existing); // 

router.post('/Update_Store_Product', AdminMediator.Update_Store_Product); 

router.post('/Delete_Store_Product', AdminMediator.Delete_Store_Product); 

router.post('/Activate_Inactivate_Store_Product', AdminMediator.Activate_Inactivate_Store_Product); 

router.post('/List_All_Products_With_Filters', AdminMediator.List_All_Products_With_Filters); 

router.post('/Get_Single_Product_Details', AdminMediator.Get_Single_Product_Details); 

router.post('/List_All_Branch_Admins', AdminMediator.List_All_Branch_Admins); 

router.post('/Delete_Branch_Admins', AdminMediator.Delete_Branch_Admins); 

router.post('/Update_Branch_Admin', AdminMediator.Update_Branch_Admin); 

router.post('/Add_Branch_Admin', AdminMediator.Add_Branch_Admin); 

router.post('/Update_Branch_Details', AdminMediator.Update_Branch_Details); 

router.post('/Update_Branch_Timings', AdminMediator.Update_Branch_Timings); 

router.post('/Active_InActive_Store_Branch', AdminMediator.Active_InActive_Store_Branch); 

router.post('/Admin_Password_Change', AdminMediator.Admin_Password_Change); 

router.post('/Admin_Password_Update', AdminMediator.Admin_Password_Update); 

router.post('/Inactive_Admin', AdminMediator.Inactive_Admin); 

// router.post('/Add_Serviceable_Area', AdminMediator.Add_Serviceable_Area);

// router.post('/List_Serviceable_Area', AdminMediator.List_Serviceable_Area);

// router.post('/InActive_Serviceable_Area', AdminMediator.InActive_Serviceable_Area);


////////////New Code For bookafella
//Zones
router.post('/Create_Zone', AdminMediator.Create_Zone); 

router.post('/List_All_City_Zones', AdminMediator.List_All_City_Zones); 

router.post('/Delete_Zone', AdminMediator.Delete_Zone); 

router.post('/Edit_Zone', AdminMediator.Edit_Zone); 

router.post('/Get_Zone_Information_In_Details', AdminMediator.Get_Zone_Information_In_Details); 

router.post('/List_All_City_Zones_Lite', AdminMediator.List_All_City_Zones_Lite); 
//Zone Hubs
router.post('/Add_Zone_Hub', AdminMediator.Add_Zone_Hub); 

router.post('/List_All_Zone_Hubs', AdminMediator.List_All_Zone_Hubs); 

router.post('/Get_Country_JSON', AdminMediator.Get_Country_JSON); 

router.post('/Get_City_JSON', AdminMediator.Get_City_JSON); 
//Country
router.post('/Validate_Country_Number', AdminMediator.Validate_Country_Number); 

router.post('/Add_Country', AdminMediator.Add_Country); 

router.post('/List_All_Countries_with_Filter', AdminMediator.List_All_Countries_with_Filter); 

router.post('/Inactivate_Country', AdminMediator.Inactivate_Country); 

router.post('/Activate_Country', AdminMediator.Activate_Country); 
//State
router.post('/Validate_State_Number', AdminMediator.Validate_State_Number); 

router.post('/Add_State', AdminMediator.Add_State); 

router.post('/List_All_States_with_Filter', AdminMediator.List_All_States_with_Filter); 

router.post('/Update_State_Information', AdminMediator.Update_State_Information); 

router.post('/Inactivate_State_Information', AdminMediator.Inactivate_State_Information); 

router.post('/Activate_State_Information', AdminMediator.Activate_State_Information); 
//city
router.post('/Validate_City_Number', AdminMediator.Validate_City_Number); 

router.post('/Add_City', AdminMediator.Add_City); 

router.post('/List_All_Country_Cities', AdminMediator.List_All_Country_Cities); 

router.post('/List_All_Cities', AdminMediator.List_All_Cities); 

router.post('/Update_City', AdminMediator.Update_City); 

router.post('/Inactivate_City', AdminMediator.Inactivate_City); 

router.post('/Activate_City', AdminMediator.Activate_City); 

router.post('/Link_States_City', AdminMediator.Link_States_City); 

router.post('/List_All_State_Cities', AdminMediator.List_All_State_Cities); 


/////////////////////////////////////// Drivers And Vehile Related

router.post('/Add_Vehicle', AdminMediator.Add_Vehicle); 

router.post('/List_All_Vehicles', AdminMediator.List_All_Vehicles); 

router.post('/Edit_Vehicle', AdminMediator.Edit_Vehicle); 

router.post('/Inactivate_Vehicle', AdminMediator.Inactivate_Vehicle); 

router.post('/Activate_Vehicle', AdminMediator.Activate_Vehicle); 

// router.post('/Add_Window', AdminMediator.Add_Window);

// router.post('/List_All_Windows', AdminMediator.List_All_Windows);

router.post('/Create_Driver', AdminMediator.Create_Driver); 

router.post('/Create_Driver_Validate_OTP', AdminMediator.Create_Driver_Validate_OTP); 

router.post('/List_All_Vehicles_with_Count', AdminMediator.List_All_Vehicles_with_Count); 

router.post('/List_All_Drivers_with_Filter', AdminMediator.List_All_Drivers_with_Filter);  

router.post('/Edit_Driver_Profile', AdminMediator.Edit_Driver_Profile); 

router.post('/Edit_Driver_Image', AdminMediator.Edit_Driver_Image); 

router.post('/Edit_Driver_Address', AdminMediator.Edit_Driver_Address); 

router.post('/Edit_Driver_Timings', AdminMediator.Edit_Driver_Timings); 

router.post('/Edit_Driver_Vehicle', AdminMediator.Edit_Driver_Vehicle); 

router.post('/Edit_Driver_Zone', AdminMediator.Edit_Driver_Zone); 

router.post('/Inactivate_Driver', AdminMediator.Inactivate_Driver); 

router.post('/Activate_Driver', AdminMediator.Activate_Driver); 

router.post('/List_All_Driver_Trips', AdminMediator.List_All_Driver_Trips);

router.post('/Driver_Order_History', AdminMediator.Driver_Order_History);

////////////////////// PRoduct Related

router.post('/List_All_Pending_Products', AdminMediator.List_All_Pending_Products);

router.post('/Approve_Product', AdminMediator.Approve_Product);

router.post('/Approve_Branch_Product', AdminMediator.Approve_Branch_Product);

router.post('/List_All_Approved_Products', AdminMediator.List_All_Approved_Products);

router.post('/List_All_Declined_Products', AdminMediator.List_All_Declined_Products);

router.post('/List_All_Blocked_Products', AdminMediator.List_All_Blocked_Products);

router.post('/List_All_Category_Products', AdminMediator.List_All_Category_Products); 

/////////////////////////// Help Related

router.post('/Create_Help', AdminMediator.Create_Help); 

router.post('/Edit_Help', AdminMediator.Edit_Help); 

router.post('/Active_Inactive_Help', AdminMediator.Active_Inactive_Help); 

router.post('/List_All_Help_Data', AdminMediator.List_All_Help_Data); 
 
router.post('/Check_Help_SNo_Available', AdminMediator.Check_Help_SNo_Available); 

router.post('/Suggest_Help_SNo', AdminMediator.Suggest_Help_SNo); 

/////////////////////// Referal amount related

router.post('/Update_Referal_Price_Data', AdminMediator.Update_Referal_Price_Data); 

router.post('/Get_Referal_Price_Data', AdminMediator.Get_Referal_Price_Data); 

//////////////////////////Buyers Related 

router.post('/List_All_Buyers_Details', AdminMediator.List_All_Buyers_Details); 

router.post('/Get_Buyer_Details', AdminMediator.Get_Buyer_Details); 

router.post('/List_All_Buyers_With_Orders', AdminMediator.List_All_Buyers_With_Orders); 


///////////////Orders Related

router.post('/List_All_Buyers_Orders', AdminMediator.List_All_Buyers_Orders); 

router.post('/List_Buyer_Orders', AdminMediator.List_Buyer_Orders); 

router.post('/List_Branch_Orders', AdminMediator.List_Branch_Orders); 

router.post('/List_All_Branch_Category_Order', AdminMediator.List_All_Branch_Category_Order); 

router.post('/List_All_Buyers_Cancelled_Orders', AdminMediator.List_All_Buyers_Cancelled_Orders); 

router.post('/List_Buyer_Cancelled_Orders', AdminMediator.List_Buyer_Cancelled_Orders); 

router.post('/List_Branch_Cancelled_Orders', AdminMediator.List_Branch_Cancelled_Orders);  

router.post('/List_All_Branch_Category_Cancelled_Order', AdminMediator.List_All_Branch_Category_Cancelled_Order); 

router.post('/Get_Single_Orders', AdminMediator.Get_Single_Orders); 

router.post('/Provide_Refund_For_Order', AdminMediator.Provide_Refund_For_Order);

router.post('/List_All_Orders', AdminMediator.List_All_Orders); 

router.post('/List_All_Cancelled_Orders_By_Buyers', AdminMediator.List_All_Cancelled_Orders_By_Buyers);  

router.post('/List_All_Cancelled_Orders_By_Branches', AdminMediator.List_All_Cancelled_Orders_By_Branches); 

router.post('/List_All_Cancelled_Orders_By_Drivers', AdminMediator.List_All_Cancelled_Orders_By_Drivers); 

router.post('/List_All_Cancelled_Orders_By_Admin', AdminMediator.List_All_Cancelled_Orders_By_Admin); 

router.post('/Cancel_Order_Return_Initiate', AdminMediator.Cancel_Order_Return_Initiate); 


router.post('/Accept_Reject_Cancelled_Order', AdminMediator.Accept_Reject_Cancelled_Orders_By_Buyers);  

router.post('/List_All_Messages_for_Order', AdminMediator.List_All_Messages_for_Order); 

router.post('/Resend_Message_for_Order', AdminMediator.Resend_Message_for_Order); 

router.post('/Edit_And_Resend_Messages_for_Order', AdminMediator.Edit_And_Resend_Messages_for_Order); 

router.post('/Cancel_Order_Notify_To_Branch', AdminMediator.Cancel_Order_Notify_To_Branch); 

router.post('/Cancel_Order_Refund_Calculation', AdminMediator.Cancel_Order_Refund_Calculation); 

router.post('/Cancel_Order_Refund', AdminMediator.Cancel_Order_Refund); 

router.post('/List_All_Cancelled_Order', AdminMediator.List_Canceled_Order); 

router.post('/List_All_Drivers_With_COD_Amount', AdminMediator.List_All_Drivers_With_COD_Amount); 

router.post('/Check_COD_Amount_at_Driver', AdminMediator.Check_COD_Amount_at_Driver);  

router.post('/Collect_COD_Amount_From_Driver', AdminMediator.Collect_COD_Amount_From_Driver); 



////////////////

router.post('/Add_Amount_Company_Wallet', AdminMediator.Add_Amount_Company_Wallet);

router.post('/Get_Company_Wallet', AdminMediator.Get_Company_Wallet);

router.post('/List_Company_Wallet_Log', AdminMediator.List_Company_Wallet_Log);

// router.post('/Update_Delivery_Charges', AdminMediator.Update_Delivery_Charges);

// router.post('/Get_Delivery_Charges_Data', AdminMediator.Get_Delivery_Charges_Data);

router.post('/Update_Fee_Details_For_Zones', AdminMediator.Update_Fee_Details_For_Zones); 

router.post('/List_All_Fee_Details_For_Zones', AdminMediator.List_All_Fee_Details_For_Zones); 

router.post('/Activate_Inactivate_Fee_Details_For_Zone', AdminMediator.Activate_Inactivate_Fee_Details_For_Zone); 

router.post('/Update_Taxes', AdminMediator.Update_Taxes); 

router.post('/Get_Taxes', AdminMediator.Get_Taxes); 

router.post('/Activate_Inactivate_Taxes', AdminMediator.Activate_Inactivate_Taxes); 

router.post('/Create_Banner', AdminMediator.Create_Banner); 

router.post('/List_All_Banner', AdminMediator.List_All_Banner); 

router.post('/Active_Inactive_Banner', AdminMediator.Active_Inactive_Banner); 

router.post('/Edit_Banner', AdminMediator.Edit_Banner); 

router.post('/Suggest_Banner_SNo', AdminMediator.Suggest_Banner_SNo); 

router.post('/Check_Banner_SNo_Available', AdminMediator.Check_Banner_SNo_Available); 

router.post('/Add_Quote', AdminMediator.Add_Quote); 

router.post('/List_All_Quotes', AdminMediator.List_All_Quotes); 

router.post('/Active_Inactive_Quote', AdminMediator.Active_Inactive_Quote); 

router.post('/Edit_Quote', AdminMediator.Edit_Quote); 

///////////////////////////////// Category And Subcategory

router.post('/Suggest_Category_SNo', AdminMediator.Suggest_Category_SNo); 

router.post('/Check_Category_SNo_Available', AdminMediator.Check_Category_SNo_Available); 

router.post('/Add_Category', AdminMediator.Add_Category); 

router.post('/List_All_Category', AdminMediator.List_All_Category); 

router.post('/Active_Inactive_Category', AdminMediator.Active_Inactive_Category); 

router.post('/Edit_Category', AdminMediator.Edit_Category); 

router.post('/Add_SubCategory', AdminMediator.Add_SubCategory); 

router.post('/List_All_SubCategory', AdminMediator.List_All_SubCategory); 

router.post('/Remove_SubCategory', AdminMediator.Remove_SubCategory); 

router.post('/Edit_SubCategory', AdminMediator.Edit_SubCategory); 

router.post('/Category_Sold', AdminMediator.Category_Sold); 

///////////////////// reorder related

router.post('/Branch_Product_Reorder', AdminMediator.Branch_Product_Reorder); 

router.post('/List_All_Product_Reorder_Log', AdminMediator.List_All_Product_Reorder_Log); 

router.post('/List_All_Products_For_Reorder', AdminMediator.List_All_Products_For_Reorder); 

//offer related

router.post('/Validate_Discount_Code', AdminMediator.Validate_Discount_Code); 

router.post('/Create_Discount', AdminMediator.Create_Discount); 

router.post('/List_All_Discounts', AdminMediator.List_All_Discounts); 

router.post('/Update_Discount', AdminMediator.Update_Discount); 

router.post('/Inactivate_Discount', AdminMediator.Inactivate_Discount); 

router.post('/Activate_Discount', AdminMediator.Activate_Discount); 

router.post('/Validate_App_Automated_Offer_Discount_Day', AdminMediator.Validate_App_Automated_Offer_Discount_Day); 

router.post('/Add_App_Automated_Offer', AdminMediator.Add_App_Automated_Offer); 

router.post('/List_All_Automated_Offers', AdminMediator.List_All_Automated_Offers); 

router.post('/Update_App_Automated_Offer', AdminMediator.Update_App_Automated_Offer); 

router.post('/Validate_Special_Offer_Number_Already_Exist', AdminMediator.Validate_Special_Offer_Number_Already_Exist); 

router.post('/Add_Special_Offer', AdminMediator.Add_Special_Offer); 

router.post('/List_All_Special_Offers_with_Filter', AdminMediator.List_All_Special_Offers_with_Filter); 

router.post('/Update_Special_Offer', AdminMediator.Update_Special_Offer); 

router.post('/Inactivate_Special_Offer', AdminMediator.Inactivate_Special_Offer); 

router.post('/Activate_Special_Offer', AdminMediator.Activate_Special_Offer); 

///Stack Related

router.post('/List_All_Stack_Log', AdminMediator.List_All_Stack_Log); 

router.post('/Request_Branch_Stack_Reorder', AdminMediator.Request_Branch_Stack_Reorder); 

router.post('/Update_Stack', AdminMediator.Update_Stack); 

router.post('/List_Branch_Products', AdminMediator.List_Branch_Products);

/// Bookafella resource

router.post('/Get_MSG91_BALANCE', AdminMediator.Get_MSG91_BALANCE); 

/// User Analatics

router.post('/Fetch_Total_Number_of_Downloads', AdminMediator.Fetch_Total_Number_of_Downloads); 

/// Notification Related

router.post('/Create_Custom_Notification', AdminMediator.Create_Custom_Notification); 

router.post('/Check_Custom_Notification_SNo_Availble', AdminMediator.Check_Custom_Notification_Availble); 

router.post('/Suggest_Custom_Notification_SNo', AdminMediator.Suggest_Custom_Notification_SNo); 

router.post('/List_All_Custom_Notifications', AdminMediator.List_All_Custom_Notifications); 

router.post('/Edit_Custom_Notification', AdminMediator.Edit_Custom_Notification);  

router.post('/Active_Inactive_Custom_Notification', AdminMediator.Active_Inactive_Custom_Notification);  

router.post('/Send_Custom_Notification', AdminMediator.Send_Custom_Notification); 

router.post('/List_All_Buyers_In_City', AdminMediator.List_All_Buyers_In_City); 

/// Title Managament

router.post('/Create_Title', AdminMediator.Create_Title); 

router.post('/List_All_Titles', AdminMediator.List_All_Titles); 

router.post('/Check_Title_Available', AdminMediator.Check_Title_Available); 

router.post('/Edit_Title', AdminMediator.Edit_Title); 

router.post('/Check_Title_SNo_Available', AdminMediator.Check_Title_SNo_Available); 

router.post('/Suggest_Title_SNo', AdminMediator.Suggest_Title_SNo); 

router.post('/Activate_Inactivate_Title', AdminMediator.Activate_Inactivate_Title); 

//Dynamic Home Page Management

router.post('/List_All_Titles_Dynamic', AdminMediator.List_All_Titles_Dynamic); 

router.post('/Add_Products_Title_Dynamic', AdminMediator.Add_Products_Title_Dynamic); //for List Use List_All_Products_With_Filters

router.post('/Activate_Inactivate_Products_Title_Dynamic', AdminMediator.Activate_Inactivate_Products_Title_Dynamic); 

router.post('/List_All_Products_Titles_Dynamic_With_Filter', AdminMediator.List_All_Products_Titles_Dynamic_With_Filter); 

//////////////////// Write and Earn

router.post('/List_All_Write_and_Earn', AdminMediator.List_All_Write_and_Earn); 

router.post('/Accept_Reject_Write_and_Earn', AdminMediator.Accept_Reject_Write_and_Earn); 

router.post('/Amount_For_Write_and_Earn', AdminMediator.Amount_For_Write_and_Earn); 

// Buyer Wallet and Points Related

router.post('/List_All_Buyers_Wallets', AdminMediator.List_All_Buyers_Wallets); 

router.post('/Credits_Rewards_To_Multiple_Buyers', AdminMediator.Credits_Rewards_To_Multiple_Buyers); 

router.post('/List_Buyer_Points_Log', AdminMediator.List_Buyer_Points_Log); 

router.post('/List_Buyer_Wallet_Log_Date', AdminMediator.List_Buyer_Wallet_Log_Date); 

/////////////Notifications

router.post('/List_All_Notifications', AdminMediator.List_All_Notifications)


export default router;