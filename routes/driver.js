import express from "express";

import DeviceMediator from "../mediators/DeviceMediator";
import DriverMediator from "../mediators/DriverMediator";

const router = express.Router();

router.post('/Driver_Splash_Screen', DeviceMediator.Driver_Splash_Screen); // updated in doc

router.post('/Generate_Driver_OTP', DriverMediator.Generate_Driver_OTP); // updated in doc

router.post('/Validate_Driver_OTP', DriverMediator.Validate_Driver_OTP); // updated in doc

router.post('/Driver_Update_Online_Offline', DriverMediator.Driver_Update_Online_Offline); // updated in doc

router.post('/Fetch_Complete_Driver_Information', DriverMediator.Fetch_Complete_Driver_Information); // updated in doc

router.post('/Driver_Logout', DriverMediator.Driver_Logout); // updated in doc

router.post('/Driver_Update_FCM_Token', DriverMediator.Driver_Update_FCM_Token); // updated in doc

router.post('/List_All_Instant_Order_Request', DriverMediator.List_All_Instant_Order_Request); // updated in doc

router.post('/Accept_Instant_Order_Request', DriverMediator.Accept_Instant_Order_Request); // updated in doc

router.post('/Reject_Instant_Order_Request', DriverMediator.Reject_Instant_Order_Request); // updated in doc

router.post('/Fetch_Google_Road_Distance', DriverMediator.Fetch_Google_Road_Distance); // updated in doc

router.post('/Driver_Fetch_Complete_Wallet_Information', DriverMediator.Driver_Fetch_Complete_Wallet_Information); // updated in doc

router.post('/Driver_Fetch_Complete_Wallet_Logs_with_Filter', DriverMediator.Driver_Fetch_Complete_Wallet_Logs_with_Filter); // not updated in doc

router.post('/Add_Driver_Beneficiary_Account_For_Bank_Account', DriverMediator.Add_Driver_Beneficiary_Account_For_Bank_Account);// updated in doc

router.post('/List_All_Driver_Beneficiary_Accounts', DriverMediator.List_All_Driver_Beneficiary_Accounts); //updated in doc

router.post('/Make_Default_Driver_Beneficiary_Account_For_Bank_Account', DriverMediator.Make_Default_Driver_Beneficiary_Account_For_Bank_Account); //updated in doc

router.post('/Update_Driver_Beneficiary_Account_For_Bank_Account', DriverMediator.Update_Driver_Beneficiary_Account_For_Bank_Account);//updated in doc

router.post('/Delete_Driver_Beneficiary_Account', DriverMediator.Delete_Driver_Beneficiary_Account); // updated in doc

router.post('/Driver_Fetch_Service_Amount', DriverMediator.Driver_Fetch_Service_Amount); //updated in doc

router.post('/Driver_Withdraw_Amount', DriverMediator.Driver_Withdraw_Amount);//updated in doc

// router.post('/Driver_Fetch_Complete_Add_Money_Total_Amount_Information', DriverMediator.Driver_Fetch_Complete_Add_Money_Total_Amount_Information); //updated in doc

// router.post('/Driver_Complete_Amount_Information_Between_Dates', DriverMediator.Driver_Complete_Amount_Information_Between_Dates); // updated in doc

router.post('/List_All_Trips_By_Date', DriverMediator.List_All_Trips_By_Date); //updated in doc

router.post('/List_All_Trips', DriverMediator.List_All_Trips); // updated in doc

router.post('/Start_Driver_Trip', DriverMediator.Start_Driver_Trip); //updated in doc

router.post('/List_All_Trip_Journey_Routes', DriverMediator.List_All_Trip_Journey_Routes); //updated in doc

router.post('/List_All_Trip_Journey_Routes_with_all_Pending_Tasks', DriverMediator.List_All_Trip_Journey_Routes_with_all_Pending_Tasks); //updated in doc

router.post('/Driver_Journey_Trip_Route_Reload', DriverMediator.Driver_Journey_Trip_Route_Reload); // updated in doc

router.post('/Trip_Route_Pickup_Drop_Initiated', DriverMediator.Trip_Pickup_Drop_Initiated); // updated in doc

router.post('/Trip_Route_Pickup_Drop_Reached', DriverMediator.Trip_Pickup_Drop_Reached); // updated in doc

router.post('/Validate_Drop_OTP_At_Buyer', DriverMediator.Validate_Drop_OTP_At_Buyer); // updated in doc

router.post('/Complete_Pickup_Type3', DriverMediator.Complete_Pickup_Type3); // updated in doc

router.post('/Complete_Drop_Type4', DriverMediator.Complete_Drop_Type4); // updated in doc

//router.post('/Report_Trip', DriverMediator.Report_Trip);

router.post('/Driver_Order_History', DriverMediator.Driver_Order_History); // updated ini doc

router.post('/Driver_Order_Cancel_Request', DriverMediator.Driver_Order_Cancel_Request); // updated in doc

router.post('/List_All_Driver_Cancelled_Orders', DriverMediator.List_All_Driver_Cancelled_Orders); // updated in doc

// router.post('/List_All_Trips_with_all_Pending_Tasks', DriverMediator.List_All_Trips_with_all_Pending_Tasks);

export default router;