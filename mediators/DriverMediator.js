let DriverMediator = function () { };
import ApiMessages from "../models/ApiMessages";
import config from "../config/config";
import { isBoolean } from "node-boolify";
import CommonController from "../controllers/CommonController";
import DriverController from "../controllers/DriverController";
import PubnubController from "../controllers/PubnubController";
import DeviceController from "../controllers/DeviceController";

DriverMediator.List_All_Driver_Cancelled_Orders = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.List_All_Driver_Cancelled_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Order_Cancel_Request = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null && req.body.TripID != ""
            && req.body.TripRouteID != null && req.body.TripRouteID != ""
            && req.body.Cancel_Reason != null && req.body.Cancel_Reason != ""
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let OrderData = await CommonController.Check_for_Order(TripData);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);
            if (OrderData.Order_Status >= 8) {
                let Result = await DriverController.Driver_Order_Cancel_Request(req.body, TripData, TripRouteData, OrderData, DriverData);
                res.json(Result);
            } else if( OrderData.Order_Status == 3) {
                throw { success: false, extras: { msg: ApiMessages.ORDER_CAN_NOT_BE_CANCELLED } };
            } else {
                throw { success: false, extras: { msg: ApiMessages.ORDER_CAN_NOT_BE_CANCELLED_BEFORE_PICKUP } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Journey_Trip_Route_Reload = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.lat != null && isFinite(req.body.lat)
            && req.body.lng != null && isFinite(req.body.lng)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let Result = await DriverController.Driver_Journey_Trip_Route_Reload(req.body, TripData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Trip_Journey_Routes_with_all_Pending_Tasks = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.List_All_Trip_Journey_Routes_with_all_Pending_Tasks(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Trip_Journey_Routes = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let Result = await DriverController.List_All_Trip_Journey_Routes(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

// DriverMediator.List_All_Trips_with_all_Pending_Tasks = async (req, res) => {
//     try {
//         if (
//             req.body.ApiKey != null
//             && req.body.DriverID != null && req.body.SessionID != null
//         ) {
//             let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
//             let DriverData = await CommonController.Check_for_Driver(req.body);
//             let Result = await DriverController.List_All_Trips_with_all_Pending_Tasks(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(await CommonController.Common_Error_Handler(error));
//         }
//     }
// }

DriverMediator.Driver_Order_History = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Order_History(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Report_Trip = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.Comment != null && req.body.Comment != ''
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let Result = await DriverController.Report_Trip(req.body, TripData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Complete_Pickup_Type3 = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.TripRouteID != null
            // && req.body.Delivered_To != null && req.body.Delivered_To != ""
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let OrderData = await CommonController.Check_for_Order(TripData);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);

            if (TripRouteData.Route_Action_Next == 1 || TripRouteData.Route_Action_Next == 3) {
                if (TripRouteData.Route_Status == 1) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_INITIATED } };
                } else if (TripRouteData.Route_Status == 2) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_REACHED } };
                } else if (TripRouteData.Route_Status == 3) {
                    let Result = await DriverController.Complete_Pickup_Type3(DriverData, TripData, TripRouteData, OrderData);
                    res.json(Result);
                } else if (TripRouteData.Route_Status == 4) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
                } else if (TripRouteData.Route_Status == 5) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
                } else if (TripRouteData.Route_Status == 6) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_TRIP_ROUTE } };
            }

            // let Result = await DriverController.Validate_Drop_OTP_At_Buyer(req.body, TripData, OrderData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Complete_Drop_Type4 = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.TripRouteID != null
            && req.body.Delivered_To != null && req.body.Delivered_To != ""
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let OrderData = await CommonController.Check_for_Order(TripData);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);

            if (TripRouteData.Route_Action_Next == 2 || TripRouteData.Route_Action_Next == 4) {
                if (TripRouteData.Route_Status == 1) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_INITIATED } };
                } else if (TripRouteData.Route_Status == 2) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_REACHED } };
                } else if (TripRouteData.Route_Status == 3) {
                    let Result = await DriverController.Complete_Drop_Type4(req.body, TripData, TripRouteData, OrderData, DriverData);
                    res.json(Result);
                } else if (TripRouteData.Route_Status == 4) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
                } else if (TripRouteData.Route_Status == 5) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
                } else if (TripRouteData.Route_Status == 6) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_TRIP_ROUTE } };
            }

            // let Result = await DriverController.Validate_Drop_OTP_At_Buyer(req.body, TripData, OrderData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


DriverMediator.Validate_Drop_OTP_At_Buyer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.TripRouteID != null
            && req.body.Delivered_To != null && req.body.Delivered_To != ""
            && req.body.OTP != null && isFinite(req.body.OTP)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let OrderData = await CommonController.Check_for_Order(TripData);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);

            if (TripRouteData.Route_Action_Next == 2 || TripRouteData.Route_Action_Next == 4) {
                if (TripRouteData.Route_Status == 1) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_INITIATED } };
                } else if (TripRouteData.Route_Status == 2) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_REACHED } };
                } else if (TripRouteData.Route_Status == 3) {
                    // let SelectedOrderBarcodeProcessing = await DriverController.Validate_Selected_Orders_Barcode_Array_Drop(req.body, TripRouteData);
                    // let Selected_Orders_ID_Array = SelectedOrderBarcodeProcessing[0];
                    // let Un_Selected_Orders_ID_Array = SelectedOrderBarcodeProcessing[1];

                    let Result = await DriverController.Validate_Drop_OTP_At_Buyer(req.body, TripData, TripRouteData, OrderData, DriverData);
                    // let Result = await DriverController.Trip_Route_Drop_Barcode_Reciever_Signature_Processed(req.body, DriverData, TripData, TripRouteData, req.body.Selected_Orders_Barcode_Array, Selected_Orders_ID_Array, Un_Selected_Orders_ID_Array, ImageData);
                    res.json(Result);
                    //let ItemImageProcessing = await DriverController.Selected_Orders_Drop_Image(Selected_Orders_ID_Array, ItemImageData);
                } else if (TripRouteData.Route_Status == 4) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
                } else if (TripRouteData.Route_Status == 5) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
                } else if (TripRouteData.Route_Status == 6) {
                    throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_TRIP_ROUTE } };
            }

            // let Result = await DriverController.Validate_Drop_OTP_At_Buyer(req.body, TripData, OrderData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Trip_Pickup_Drop_Reached = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.TripRouteID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);
            if (TripRouteData.Route_Status == 1) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_NOT_INITIATED } };
            } else if (TripRouteData.Route_Status == 2) {
                let Result = await DriverController.Trip_Pickup_Drop_Reached(req.body, TripRouteData, DriverData);
                res.json(Result);
            } else if (TripRouteData.Route_Status == 3) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_REACHED } };
            } else if (TripRouteData.Route_Status == 4) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
            } else if (TripRouteData.Route_Status == 5) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
            } else if (TripRouteData.Route_Status == 6) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Trip_Pickup_Drop_Initiated = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
            && req.body.TripRouteID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let ValidityStatus = await DriverController.Common_Check_Whether_Trip_Started(TripData);
            let TripRouteData = await CommonController.Check_for_Trip_Route(req.body);
            if (TripRouteData.Route_Status == 1) {
                let Result = await DriverController.Trip_Pickup_Drop_Initiated(req.body,TripData, TripRouteData, DriverData);
                res.json(Result);
            } else if (TripRouteData.Route_Status == 2) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_INITIATED } };
            } else if (TripRouteData.Route_Status == 3) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_REACHED } };
            } else if (TripRouteData.Route_Status == 4) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PICKED } };
            } else if (TripRouteData.Route_Status == 5) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_ALREADY_PROCESSED } };
            } else if (TripRouteData.Route_Status == 6) {
                throw { success: false, extras: { msg: ApiMessages.TRIP_ROUTE_CANCELLED } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Complete_Amount_Information_Between_Dates = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Start_Date != null
            && req.body.End_Date != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let ValidityStatus = await CommonController.Common_Validate_Start_End_Time(req.body);
            let Result = await DriverController.Driver_Complete_Amount_Information_Between_Dates(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Fetch_Complete_Add_Money_Total_Amount_Information = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Amount != null && isFinite(req.body.Amount) && !isNaN(req.body.Amount)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Fetch_Complete_Add_Money_Total_Amount_Information(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Withdraw_Amount = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.BeneficiaryID != null
            && req.body.Amount != null && isFinite(req.body.Amount) && !isNaN(req.body.Amount)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let BeneficiaryData = await CommonController.Check_for_Driver_Bank_Beneficiary_Account(req.body);
            if (BeneficiaryData.Status) {
                let Result = await DriverController.Driver_Withdraw_Amount(req.body, DriverData, BeneficiaryData);
                res.json(Result);
            } else {
                throw { success: false, extras: { msg: ApiMessages.ACCOUNT_NOT_ACTIVE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Fetch_Service_Amount = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Amount != null && isFinite(req.body.Amount) && !isNaN(req.body.Amount)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Fetch_Service_Amount(req.body, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Delete_Driver_Beneficiary_Account = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.BeneficiaryID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let BeneficiaryData = await CommonController.Check_for_Driver_Bank_Beneficiary_Account(req.body);
            if (BeneficiaryData.Whether_Default_Account) {
                throw { success: false, extras: { msg: ApiMessages.CANNOT_REMOVE_DEFAULT_BENEFICIARY_ACCOUNT } };
            } else {
                let Result = await DriverController.Inactivate_Driver_Beneficiary_Account_For_Bank_Account(req.body);
                res.json(Result);
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Update_Driver_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.BeneficiaryID != null
            && req.body.Name != null && req.body.Name != ''
            && req.body.Account_Number != null && isFinite(req.body.Account_Number) && !isNaN(req.body.Account_Number)
            && req.body.IFSC != null && req.body.IFSC != ''
        ) {
            req.body = JSON.parse(JSON.stringify(req.body));
            req.body.IFSC = req.body.IFSC.toUpperCase();
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let BeneficiaryData = await CommonController.Check_for_Driver_Bank_Beneficiary_Account(req.body);
            let BankData = await CommonController.Validate_IFSC_Code(req.body);
            let ValidityStatus = await DriverController.Validate_Driver_Beneficiary_Account_For_Bank_Account_Number_Already_Exist(req.body);
            let Result = await DriverController.Update_Driver_Beneficiary_Account_For_Bank_Account(req.body, BankData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Make_Default_Driver_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.BeneficiaryID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let BeneficiaryData = await CommonController.Check_for_Driver_Bank_Beneficiary_Account(req.body);
            let Result = await DriverController.Make_Default_Driver_Beneficiary_Account_For_Bank_Account(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Driver_Beneficiary_Accounts = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.List_All_Driver_Beneficiary_Accounts(req.body, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Add_Driver_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Whether_Default_Account != null && isBoolean(req.body.Whether_Default_Account)
            && req.body.Name != null && req.body.Name != ''
            && req.body.Account_Number != null && isFinite(req.body.Account_Number) && !isNaN(req.body.Account_Number)
            && req.body.IFSC != null && req.body.IFSC != ''
        ) {
            req.body = JSON.parse(JSON.stringify(req.body));
            req.body.IFSC = req.body.IFSC.toUpperCase();
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let BankData = await CommonController.Validate_IFSC_Code(req.body);
            let ValidityStatus = await DriverController.Validate_Driver_Beneficiary_Account_For_Bank_Account_Number_Already_Exist(req.body);
            let Result = await DriverController.Add_Driver_Beneficiary_Account_For_Bank_Account(req.body, BankData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Fetch_Complete_Wallet_Logs_with_Filter = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Fetch_Complete_Wallet_Logs_with_Filter(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Fetch_Complete_Wallet_Information = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Fetch_Complete_Wallet_Information(req.body, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Fetch_Google_Road_Distance = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.lat1 != null && isFinite(req.body.lat1) && !isNaN(req.body.lat1)
            && req.body.lng1 != null && isFinite(req.body.lng1) && !isNaN(req.body.lng1)
            && req.body.lat2 != null && isFinite(req.body.lat2) && !isNaN(req.body.lat2)
            && req.body.lng2 != null && isFinite(req.body.lng2) && !isNaN(req.body.lng2)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Fetch_Google_Road_Distance(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Reject_Instant_Order_Request = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.InstantOrderRequestID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let InstantRequestData = await CommonController.Check_for_Instant_Order_Request(req.body);
            let Result = await DriverController.Reject_Instant_Order_Request(req.body, InstantRequestData);
            res.json(Result);
            let SendingPubnubMessage = await PubnubController.Send_Common_Instant_Request_Pubnub_Messaging_for_Common_Channel(InstantRequestData, 2);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Accept_Instant_Order_Request = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.InstantOrderRequestID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let InstantRequestData = await CommonController.Check_for_Instant_Order_Request(req.body);
            let ValidityStatus = await DriverController.Accept_Instant_Order_Request_Validate_Already_Accepted(req.body, DriverData);
            let Result = await DriverController.Accept_Instant_Order_Request(req.body, InstantRequestData);
            res.json(Result);
            InstantRequestData.newDriverID = req.body.DriverID;
            let SendingPubnubMessage = await PubnubController.Send_Common_Instant_Request_Pubnub_Messaging_for_Common_Channel(InstantRequestData, 1);
            let OrderProcessing = await CommonController.Common_Order_Creation_Order_Placing_Processing(InstantRequestData, DriverData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Instant_Order_Request = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.List_All_Instant_Order_Request(req.body, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Start_Driver_Trip = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.TripID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let TripData = await CommonController.Check_for_Trip(req.body);
            let Result = await DriverController.Start_Driver_Trip(TripData);
            await res.json(Result);
            let UpdateOrders = await DriverController.Start_Driver_Trip_Update_Order_Status(TripData, DriverData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Trips = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.List_All_Trips(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.List_All_Trips_By_Date = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Date != null && req.body.Date != ''
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let ValidityStatus = await DriverController.List_All_Trips_By_Date_Validate_Date(req.body);
            let Result = await DriverController.List_All_Trips_By_Date(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Update_FCM_Token = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.FCM_Token != null && req.body.FCM_Token != ''
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let Result = await DeviceController.Driver_Update_FCM_Token(req.body, DeviceData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Logout = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.Driver_Logout(req.body, DeviceData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Fetch_Complete_Driver_Information = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Fetch_Complete_Driver_Information(DriverData);
            delete Result.extras.Status;
            delete Result.extras.Data.Categorories_Data;
            delete Result.extras.Data.__v;
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Driver_Update_Online_Offline = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.DriverID != null && req.body.SessionID != null
            && req.body.Whether_Driver_Online != null && isBoolean(req.body.Whether_Driver_Online)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await CommonController.Check_for_Driver(req.body);
            let Result = await DriverController.Driver_Update_Online_Offline(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Validate_Driver_OTP = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.Driver_Country_Code != null && req.body.Driver_Country_Code != ''
            && req.body.Driver_Phone_Number != null && req.body.Driver_Phone_Number != ''
            && req.body.OTP != null && isFinite(req.body.OTP)
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await DriverController.Check_Whether_Driver_Phone_Number_Registered(req.body);
            let ValidityStatus = await DriverController.Check_for_Driver_OTP_Tries_Count(req.body);
            ValidityStatus = await DriverController.Validate_Driver_OTP(req.body);
            let Result = await DriverController.Generate_Driver_Session(DriverData, DeviceData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

DriverMediator.Generate_Driver_OTP = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.Driver_Country_Code != null && req.body.Driver_Country_Code != ''
            && req.body.Driver_Phone_Number != null && req.body.Driver_Phone_Number != ''
        ) {
            let DeviceData = await CommonController.Check_for_Driver_Api_Key(req.body);
            let DriverData = await DriverController.Check_Whether_Driver_Phone_Number_Registered(req.body);
            let ValidityStatus = await DriverController.Check_for_Driver_OTP_Count(req.body);
            let Result = await DriverController.Generate_Driver_OTP_Send_Message(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

export default DriverMediator;