let websiteMediator = function () { };
import AdminController from '../controllers/AdminController';
import StoreController from '../controllers/StoreController';
import ApiResponce from "../models/Apiresponce.js";
import ApiMessages from '../models/ApiMessages';
import UserController from '../controllers/UserController';
import CommonController from '../controllers/CommonController';
import Store_SECTIONS from '../models/Store_SECTIONS'
import Store_SECTIONS_PRODUCTS from '../models/Store_SECTIONS_PRODUCTS'
import async from 'async';

websiteMediator.Login = async (req, res) => {
    try {
        if (
            req.body.PhoneNumber != null && req.body.PhoneNumber != ''
            && req.body.Password != null && req.body.Password != ''
        ) {
            let UserData = await UserController.Check_for_Phone_Number_Registered(req.body);
            let Result = await UserController.Login(req.body, UserData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

websiteMediator.Register = async (req, res) => {
    try {
        if (
            req.body.Name != null && req.body.Name != ''
            && req.body.PhoneNumber != null && req.body.PhoneNumber != ''
            && req.body.OTP != null && isFinite(req.body.OTP)
            && req.body.EmailID != null && req.body.EmailID != ''
            && req.body.Password != null && req.body.Password != ''
        ) {
            let ValidityStatus = await CommonController.Common_PhoneNumber_Validation(req.body.PhoneNumber);
            ValidityStatus = await UserController.Check_for_Phone_Already_Registered(req.body);
            ValidityStatus = await CommonController.Common_Email_Validation(req.body.EmailID);
            ValidityStatus = await UserController.Check_for_User_OTP_Tries_Count(req.body);
            ValidityStatus = await UserController.Validate_User_OTP(req.body);
            let Result = await UserController.Register(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

websiteMediator.Generate_User_OTP = async (req, res) => {
    try {
        if (
            req.body.PhoneNumber != null && req.body.PhoneNumber != ''
        ) {
            let ValidityStatus = await CommonController.Common_PhoneNumber_Validation(req.body.PhoneNumber);
            ValidityStatus = await UserController.Check_for_Phone_Already_Registered(req.body);
            ValidityStatus = await UserController.Check_for_OTP_Count(req.body);
            let Result = await UserController.Generate_User_OTP_Send_Message(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

websiteMediator.List_All_Orders = async (req, res) => {
    try {
        if (
            req.body.UserID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let UserData = await UserController.Check_for_User(req.body);
            let Result = await UserController.List_All_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

websiteMediator.Create_Order = (req, res) => {
    if (req.body.UserID != null && req.body.BranchID != null && req.body.MenuData[0].MENU_ID != null && req.body.MenuData[0].Sectionvalues != null &&
        req.body.MenuData[0].Sectionvalues[0].SECTION_ID != null && req.body.MenuData[0].Sectionvalues[0].ProductID_Array != null && req.body.MenuData[0].NumberOfPlates != null &&
        req.body.contactName != null && req.body.contactNumber != null && req.body.amountToPaid != null && req.body.dueAmount != null &&
        req.body.totalAmount != null && req.body.eventDate != null && req.body.eventTime != null && req.body.eventAddress != null &&
        req.body.eventPincode != null && req.body.eventType != null) {
        if (!req.body.MenuData[0].NumberOfPlates < 20) {
            if (new Date(req.body.eventDate) > new Date()) {
                UserController.Check_for_User(req.body).then((Users) => {
                    StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                        StoreController.Check_for_STORE_MENU_forWebsite(req.body).then((Result) => {
                            let TotalAmount = (Result);
                            let SummedValue = parseFloat(parseFloat((TotalAmount * 20) / 100).toFixed(2));
                            if (req.body.amountToPaid == SummedValue) {
                                websiteMediator.Check_for_STORE_SECTION(req.body, 0).then((SECTION_Data) => {
                                    websiteMediator.Check_for_STORE_SECTION(req.body, 1).then((SECTION_Data) => {
                                        UserController.Create_Order(req.body, TotalAmount).then((Result) => {
                                            res.json(Result);
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            } else {
                                res.json({ success: false, extras: { msg: ApiMessages.AMOUNT_NOT_MATCHED } });
                            }
                        }).catch(err => res.json(err));
                    }).catch(err => res.json(err));
                });
            } else {
                res.json({ success: false, extras: { msg: ApiMessages.PLEASE_SUBMIT_FUTURE_EVENT_DATE } });
            }
        } else {
            res.json({ success: false, extras: { msg: ApiMessages.TOTAL_ORDER_PLATES_LESSTHAN_TWENTY } });
        }
    } else {
        res.json({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


websiteMediator.Check_for_STORE_SECTION = (values, postion) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                let index = 0;
                if (values.MenuData[postion] != null && values.MenuData[postion] != undefined && values.MenuData[postion].Sectionvalues.length > 0) {
                    async.eachSeries(values.MenuData[postion].Sectionvalues, async (item, callback) => {
                        try {
                            let query = {
                                MENU_ID: values.MenuData[postion].MENU_ID,
                                SECTION_ID: item.SECTION_ID
                            };
                            Store_SECTIONS.findOne(query).lean().exec().then((Result) => {
                                if (Result == null) {
                                    callback({ success: false, extras: { msg: ApiMessages.INVALID_SECTION } });
                                } else {
                                    console.log(JSON.stringify(Result));
                                    if (Result.MAX_SELECTION == item.ProductID_Array.length) {
                                        websiteMediator.Check_for_STORE_SECTION_products(values, index, postion).then((PRODUCT_Data) => {
                                            index++;
                                            callback();
                                        }).catch(err => callback(err));
                                    } else {
                                        callback({ success: false, extras: { msg: ApiMessages.PLEASE_SELECT_ALL_THE_PRODUCTS } });
                                    }
                                }
                            }).catch((err) => {
                                console.error('Some DB Error--->', err);
                                callback({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                            })
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                        resolve("Validated Successfully");
                    });
                } else {
                    resolve("Validated Successfully");
                }
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}

websiteMediator.Check_for_STORE_SECTION_products = (values, sectionpostion, postion) => {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            try {
                async.eachSeries(values.MenuData[postion].Sectionvalues[sectionpostion].ProductID_Array, async (item, callback) => {
                    try {
                        let query = {
                            MENU_ID: values.MenuData[postion].MENU_ID,
                            SECTION_ID: values.MenuData[postion].Sectionvalues[sectionpostion].SECTION_ID,
                            ProductID: item,
                            Status: true
                        };
                        Store_SECTIONS_PRODUCTS.findOne(query).lean().exec().then((Result) => {
                            if (Result == null) {
                                callback({ success: false, extras: { msg: ApiMessages.INVALID_SECTION_PRODUCT } });
                            } else if (Result != null) {
                                callback();
                            }
                        }).catch((err) => {
                            console.error('Some DB Error--->', err);
                            callback({ success: false, extras: { msg: ApiMessages.DATABASE_ERROR } });
                        })
                    } catch (error) {
                        callback(error);
                    }
                }, async (err) => {
                    if (err) reject(err);
                    resolve("Validated Successfully");
                });
            } catch (error) {
                console.error('Something Error--->', error);
            }
        });
    });
}


websiteMediator.Fetch_All_Nearest_Branches = (req, res) => {
    if (
        req.body.Latitude != null && req.body.Longitude != null
        && req.body.skip != null && req.body.limit != null
    ) {
        StoreController.Fetch_All_Nearest_Branches(req.body).then((Result) => {
            res.json(Result);
        }).catch(err => res.json(err));
    } else {
        res.json({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
}

websiteMediator.Get_Branch_Render_Data = (req, res) => {
    let BranchURL = req.params.BranchURL;
    websiteMediator.Get_Store_Branch_Data(BranchURL, (err, BranchDetails) => {
        if (err) {
            console.log(err);
            //res.render("error");
        } else {

            res.render('catere', new ApiResponce({
                success: true,
                extras: {
                    BranchData: BranchDetails
                }
            }));
            // res.send();
        }
    });
};


websiteMediator.Get_Branch_json_Data = (req, res) => {
    let BranchURL = req.params.BranchURL;
    websiteMediator.Get_Store_Branch_Data(BranchURL, (err, BranchDetails) => {
        if (err) {
            console.log(err);
            //res.render("error");
        } else {

            res.json(new ApiResponce({
                success: true,
                extras: {
                    BranchData: BranchDetails
                }
            }));
            // res.send();
        }
    });
};

websiteMediator.Get_Store_Branch_Data = (BranchURL, callback) => {
    AdminController.Get_Store_Branch_Data_By_URL(BranchURL, function (err, BranchData) {
        if (err) {
            callback(true);
        } else {
            AdminController.Branch_In_Detail(BranchData, function (err, Result) {
                if (err) {
                    console.log(err);
                    callback(true);
                } else {
                    AdminController.Branch_Gallery_Details(Result, function (err, BranchDetails) {
                        if (err) {
                            console.log(err);
                            callback(true);
                        } else {
                            AdminController.Gets_Image_Data(BranchDetails.Branch_Cover_ImageID, function (err, CoverImageData) {
                                if (!err) {
                                    BranchDetails.Branch_Cover_ImageURL = CoverImageData;
                                    AdminController.Gets_Image_Data(BranchDetails.ImageID, function (err, ImageData) {
                                        if (!err) {
                                            BranchDetails.ImageURL = ImageData;

                                            StoreController.Get_ALL_Menu(BranchDetails.BranchID, function (err, MenuList) {
                                                if (err) {
                                                    console.log(err);;
                                                } else {
                                                    BranchDetails.MenuList = MenuList;
                                                    callback(false, BranchDetails);
                                                }
                                            });
                                        }
                                    })
                                }
                            });
                        }
                    })
                }
            });
        }
    })
};

websiteMediator.Get_Home_Page = (req, res) => {
    res.render('home');
};

export default websiteMediator;