let AdminMediator = function () { };
// import logger from '../core/logger/logger';
import ApiMessages from '../models/ApiMessages';
import ApiResponce from "../models/Apiresponce.js";
import Admin_Images from '../models/Admin_Images';
import Admin_Logs from '../models/Admin_Logs';
import AdminController from '../controllers/AdminController';
import AdminLogController from '../controllers/AdminLogController';
import Admin_User from '../models/Admin_User';
import config from '../config/config';
import Store_Entity from '../models/Store_Entity';
import Store_Branch from '../models/Store_Branch';
import Customers from '../models/Customers';
import Counters from '../models/Counters';
import Validator from "validator";
let EmailOptions = { allow_display_name: false, allow_utf8_local_part: false, require_tld: true };
var msg91mod = require('../controllers/msg91mod.js'); // Setting the Path for Message91 Modules
var MSG91MOD = new msg91mod();
var MSG91Control = require('../controllers/MSG91Controller.js');


//Dependencies
import uuid from "uuid";
import async from "async";
import sync from "sync";
import moment from "moment";
import crypto from "crypto";
import rand from "csprng";
import Jimp from "jimp";
import formidable from "formidable";
import base64ImageToFile from "base64image-to-file";
import fs, { Stats } from "fs";
import path from "path";
import os from "os";
import CommonController from '../controllers/CommonController';
import { isBoolean, Boolify } from 'node-boolify';
import DriverController from '../controllers/DriverController';
import AWSController from '../controllers/AWSController';
import StoreController from '../controllers/StoreController';
import MSG91Controller from '../controllers/MSG91Controller';
import Buyers_Current_Location from '../models/Buyers_Current_Location';
import NotificationController from '../controllers/NotificationController';
import FcmController from '../controllers/FcmController';

AdminMediator.List_All_Notifications = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.CountryID != null
        && req.body.CityID != null
        && req.body.ZoneID != null
        && req.body.User_Type != null && isFinite(req.body.User_Type) // 0- All, 1-Buyers, 2-Store, 3-Driver
        && req.body.DriverID != null
        && req.body.BranchID != null
        && req.body.BuyerID != null
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.skip)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        if (req.body.User_Type >= 0 && req.body.User_Type <= 3) {
            if (req.body.CountryID != "") {
                let CountryData = await CommonController.Check_for_Country(req.body)
            }
            if (req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body)
            }
            if (req.body.ZoneID != "") {
                let ZoneData = await CommonController.Check_for_Zone(req.body)
            }
            if (req.body.DriverID != "") {
                let DriverData = await CommonController.Check_for_Driver(req.body)
            }
            if (req.body.BranchID != "") {
                let BranchData = await CommonController.Check_for_Branch(req.body)
            }
            if (req.body.BuyerID != "") {
                let BuyerData = await CommonController.Check_For_Buyer(req.body)
            }
            let Result = await AdminController.List_All_Notifications(req.body);
            res.json(Result);
        } else {
            res.send({ success: false, extras: { msg: ApiMessages.INVALID_INPUT } })
        }
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_Buyer_Wallet_Log_Date = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.BuyerID != null && req.body.BuyerID != ""
        && req.body.Date != null && req.body.Date != ""
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.skip)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let BuyerData = await CommonController.Check_For_Buyer(req.body);
        let Result = await AdminController.List_Buyer_Wallet_Log_Date(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_Buyer_Points_Log = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.BuyerID != null && req.body.BuyerID != ""
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.skip)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let BuyerData = await CommonController.Check_For_Buyer(req.body);
        let Result = await AdminController.List_Buyer_Points_Log(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Credits_Rewards_To_Multiple_Buyers = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.BuyerID_Array != null && typeof req.body.BuyerID_Array === "object"
        && req.body.BuyerID_Array.length > 0
        && req.body.Points > 0 && req.body.Points != null && isFinite(req.body.Points)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let Result = await AdminController.Credits_Rewards_To_Multiple_Buyers(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Buyers_Wallets = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.CityID != null
        && req.body.ZoneID != null
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.skip)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        if (req.body.CityID != "") {
            let CityData = await CommonController.Check_for_City(req.body)
        }
        if (req.body.ZoneID != "") {
            let ZoneData = await CommonController.Check_for_Zone(req.body)
        }
        let Result = await AdminController.List_All_Buyers_Wallets(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Request_Branch_Stack_Reorder = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.StackID != null
        && req.body.Reorder_Quantity != null && isFinite(req.body.Reorder_Quantity)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let StackData = await CommonController.Check_for_Stack(req.body);
        let Result = await AdminController.Request_Branch_Stack_Reorder(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Stack_Log = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.limit)
        && req.body.ZoneID != null
        && req.body.ProductID != null
        && req.body.Search != null
        && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
        && req.body.Whether_Incoming != null && isBoolean(req.body.Whether_Incoming)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        if (Boolify(req.body.Whether_Date_Filter)) {
            if (req.body.Start_Date != null && req.body.Start_Date != ""
                && req.body.End_Date != null && req.body.End_Date != ""
            ) {
                let ValidateData = await CommonController.Common_Start_Date_End_Date_Validation(req.body)
            } else {
                res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
            }
        }
        if (req.body.ZoneID != "") {
            let ZoneData = await CommonController.Check_for_Zone(req.body)
        }
        if (req.body.ProductID != "") {
            let ProductData = await CommonController.Check_For_Product(req.body)
        }
        let Result = await AdminController.List_All_Stack_Log(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Collect_COD_Amount_From_Driver = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.DriverID != null && req.body.DriverID != ""
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let DriverData = await CommonController.Check_Only_Driver(req.body);
        let Amount_Collected = await AdminController.Check_COD_Amount_at_Driver(req.body);
        let Result = await AdminController.Collect_COD_Amount_From_Driver(req.body, Amount_Collected[1], DriverData);
        res.json(Result);
        let SendNotification = await FcmController.SendNotification_Collect_COD_Amount_From_Driver(Amount_Collected[1], DriverData)
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Check_COD_Amount_at_Driver = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.DriverID != null && req.body.DriverID != ""
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let DriverData = await CommonController.Check_Only_Driver(req.body);
        let Result = await AdminController.Check_COD_Amount_at_Driver1(req.body);
        res.json({ success: true, extras: { Data: { Amount_Collected: Result } } });
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Drivers_With_COD_Amount = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.ZoneID != null && req.body.CityID != null
        && req.body.CountryID != null && req.body.Search != null
        && req.body.skip != null && isFinite(req.body.skip)
        && req.body.limit != null && isFinite(req.body.limit)
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let Result = await AdminController.List_All_Drivers_With_COD_Amount(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Fetch_Total_Number_of_Downloads = async (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
    ) {
        let AdminData = await CommonController.Check_for_Admin(req.body);
        let Result = await AdminController.Fetch_Total_Number_of_Downloads(req.body);
        res.json(Result);
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Products_Titles_Dynamic_With_Filter = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.skip != null && req.body.limit != null
        && req.body.TitleID != null && req.body.TitleID != ''
        && req.body.Status != null
        && req.body.CountryID != null
        && req.body.CityID != null
        && req.body.ZoneID != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                CommonController.Check_for_Title(req.body).then((TitleData) => {
                    AdminController.List_All_Products_Titles_Dynamic_With_Filter(req.body, TitleData).then((Result) => {
                        res.json(Result);
                    }).catch(err => res.json(err));
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Activate_Inactivate_Products_Title_Dynamic = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.TitleID != null && req.body.TitleID != ""
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let TitleData = await CommonController.Check_for_Title(req.body);
            // let Product_Data = await CommonController.Check_For_Product(req.body)
            let Dynamic_Product_Data = await CommonController.Check_For_Product_Dynamic(req.body)
            let Result = await AdminController.Activate_Inactivate_Products_Title_Dynamic(req.body, Dynamic_Product_Data, TitleData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_Products_Title_Dynamic = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.TitleID != null && req.body.TitleID != ""
            && req.body.Products_Array != null && typeof req.body.Products_Array === "object"
            && req.body.Products_Array.length > 0
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let TitleData = await CommonController.Check_for_Title(req.body);
            let Products_Array_Data = await AdminController.Products_Array_Processing(req.body.Products_Array)
            let Result = await AdminController.Add_Products_Title_Dynamic(req.body, Products_Array_Data, TitleData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Titles_Dynamic = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Titles_Dynamic(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Titles = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Status != null && isBoolean(req.body.Status)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Titles(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Activate_Inactivate_Title = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.TitleID != null && req.body.TitleID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let TitleData = await CommonController.Check_for_Title(req.body);
            let Result = await AdminController.Activate_Inactivate_Title(req.body, TitleData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Suggest_Banner_SNo = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Suggest_Banner_SNo(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Banner_SNo_Available = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Banner_SNo(req.body);
            res.json({ success: true, extras: { Status: ValidateData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Suggest_Title_SNo = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Suggest_Title_SNo(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Title_SNo_Available = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Title_SNo(req.body);
            res.json({ success: true, extras: { Status: ValidateData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Title = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.TitleID != null && req.body.TitleID != ''
            && req.body.Title != null && req.body.Title != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_for_Title(req.body);
            ValidateData = await CommonController.Check_Title_Available(req.body);
            ValidateData = await CommonController.Check_Title_SNo(req.body);
            let Result = await AdminController.Edit_Title(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Title_Available = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Title != null && req.body.Title != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Title_Available(req.body);
            res.json({ success: true, extras: { Status: "Title Available" } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Create_Title = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Title != null && req.body.Title != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Title_Available(req.body);
            ValidateData = await CommonController.Check_Title_SNo(req.body);
            let Result = await AdminController.Create_Title(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Send_Custom_Notification = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.NotificationID != null && req.body.NotificationID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await AdminController.Check_for_Custom_Notification(req.body);
            let Result = await AdminController.Send_Custom_Notification(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Custom_Notification = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.NotificationID != null && req.body.NotificationID != ''
            && req.body.CountryID != null && req.body.CountryID != ''
            && req.body.CityID != null && req.body.CityID != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
            && req.body.NotificationTitle != null && req.body.NotificationTitle != ''
            && req.body.NotificationBody != null && req.body.NotificationBody != ''
            && req.body.Notification_Type != null && isFinite(req.body.Notification_Type) // 1- Zone, 2- All_Users 3- Selected_Users
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body)
            let CityData = await CommonController.Check_for_City(req.body)
            let SerialNumberValidate = await AdminController.Check_for_Custom_Notification_SNo(req.body)
            if (req.body.Notification_Type == 1) {
                if (req.body.ZoneID != null && req.body.ZoneID != '') {
                    let ZoneData = await CommonController.Check_for_Zone(req.body)
                    let UserData = await Buyers_Current_Location.find({ ZoneID: req.body.ZoneID }).lean().exec();
                    let UserData1 = UserData.filter(function (ResultTemp) {
                        req.body.BuyerID_Array.push(ResultTemp.BuyerID)
                    });
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            } else if (req.body.Notification_Type == 2) {
                // All users in that City
                let UserData = await Buyers_Current_Location.find({ CityID: req.body.CityID }).lean().exec();
                let UserData1 = UserData.filter(function (ResultTemp) {
                    req.body.BuyerID_Array.push(ResultTemp.BuyerID)
                });

            } else if (req.body.Notification_Type == 3) {
                // Selected Users in that city
                if (
                    req.body.BuyerID_Array != null && typeof req.body.BuyerID_Array == 'object'
                    && req.body.BuyerID_Array.length > 0
                ) {
                    async.eachSeries(req.body.BuyerID_Array, async (item, callback) => {
                        try {
                            let ValidatedUserArray = await CommonController.Check_For_Buyer({ BuyerID: item })
                            callback()
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                    });
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_NOTIFICATION_TYPE } };
            }
            let Result = await AdminController.Edit_Custom_Notification(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Custom_Notification = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.NotificationID != null && req.body.NotificationID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Custom_Notification(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Custom_Notifications = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
            && req.body.ZoneID != null
            && req.body.Whether_Notification_Type_Filter != null && isBoolean(req.body.Whether_Notification_Type_Filter)
            && req.body.Status != null && isBoolean(req.body.Status)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.CountryID != "") {
                let CountryData = await CommonController.Check_for_Country(req.body)
            }
            if (req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body)
            }
            if (req.body.ZoneID != "") {
                let ZoneData = await CommonController.Check_for_Zone(req.body)
            }
            if (isBoolean(req.body.Whether_Notification_Type_Filter)) {
                if (req.body.Notification_Type != null && isFinite(req.body.Notification_Type)) {
                    if (req.body.Notification_Type < 1 || req.body.Notification_Type > 3) {
                        throw { success: false, extras: { msg: ApiMessages.INVALID_NOTIFICATION_TYPE } };
                    }
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            }
            let Result = await AdminController.List_All_Custom_Notifications(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Custom_Notification_Availble = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let SerialNumberValidate = await AdminController.Check_for_Custom_Notification_SNo(req.body)
            res.json({ success: true, extras: { Status: "Serial Number Available" } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Buyers_In_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CountryID != ''
            && req.body.CityID != null && req.body.CityID != ''
            && req.body.Search != null //&& req.body.Search != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)

        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body)
            let CityData = await CommonController.Check_for_City(req.body)
            let Result = await AdminController.List_All_Buyers_In_City(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Create_Custom_Notification = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CountryID != ''
            && req.body.CityID != null && req.body.CityID != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
            && req.body.NotificationTitle != null && req.body.NotificationTitle != ''
            && req.body.NotificationBody != null && req.body.NotificationBody != ''
            && req.body.Notification_Type != null && isFinite(req.body.Notification_Type) // 1- Zone, 2- All_Users 3- Selected_Users
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body)
            let CityData = await CommonController.Check_for_City(req.body)
            let SerialNumberValidate = await AdminController.Check_for_Custom_Notification_SNo(req.body)
            if (req.body.Notification_Type == 1) {
                req.body.BuyerID_Array = [];
                if (req.body.ZoneID != null && req.body.ZoneID != '') {
                    let ZoneData = await CommonController.Check_for_Zone(req.body)
                    let UserData = await Buyers_Current_Location.find({ ZoneID: req.body.ZoneID }).lean().exec();
                    let UserData1 = UserData.filter(function (ResultTemp) {
                        req.body.BuyerID_Array.push(ResultTemp.BuyerID)
                    });
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            } else if (req.body.Notification_Type == 2) {
                // All users in that City
                req.body.BuyerID_Array = [];
                let UserData = await Buyers_Current_Location.find({ CityID: req.body.CityID }).lean().exec();
                let UserData1 = UserData.filter(function (ResultTemp) {
                    req.body.BuyerID_Array.push(ResultTemp.BuyerID)
                });

            } else if (req.body.Notification_Type == 3) {
                // Selected Users in that city
                if (
                    req.body.BuyerID_Array != null && typeof req.body.BuyerID_Array === 'object'
                    && req.body.BuyerID_Array.length > 0
                ) {
                    async.eachSeries(req.body.USRID_Array, async (item, callback) => {
                        try {
                            let ValidatedUserArray = await CommonController.Check_For_Buyer({ BuyerID: item })
                            callback()
                        } catch (error) {
                            callback(error);
                        }
                    }, async (err) => {
                        if (err) reject(err);
                    });
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_NOTIFICATION_TYPE } };
            }
            let Result = await AdminController.Create_Custom_Notification(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_for_Store_ProductID_Availability = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Unique_ProductID != null && req.body.Unique_ProductID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Check_For_Unique_Product(req.body.Unique_ProductID);
            res.json({ success: true, extras: { Status: "Unique ProductID Available" } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Amount_For_Write_and_Earn = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Points != null && isFinite(req.body.Points)
            && req.body.LogID != null && req.body.LogID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Amount_For_Write_and_Earn(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Accept_Reject_Write_and_Earn = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.LogID != null && req.body.LogID != ''
            && req.body.Approve != null && isFinite(req.body.Approve) && !isNaN(req.body.Approve)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Accept_Reject_Write_and_Earn(req.body);
            res.json(Result);
            let SendNotification = await FcmController.SendNotification_Accept_Reject_Write_and_Earn(values)
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Write_and_Earn = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Approve != null && isFinite(req.body.Approve) && !isNaN(req.body.Approve)
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Write_and_Earn(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_MSG91_BALANCE = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BalanceData = await MSG91Controller.Get_MSG91_BALANCE();
            res.json({ success: true, extras: { Balance: BalanceData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Update_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null && req.body.BranchID != null) {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                if (err) {
                    res.send(JSON.stringify(BranchData));
                } else {
                    if (req.body.ProductID != null && req.body.ProductID != ""
                        && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                        && req.body.Price != null && req.body.Price != ''
                        && req.body.Branch_Discount_Price != null && isFinite(req.body.Branch_Discount_Price) && !isNaN(req.body.Branch_Discount_Price)
                        && req.body.Bookafella_Discount_Price != null && isFinite(req.body.Bookafella_Discount_Price) && !isNaN(req.body.Bookafella_Discount_Price)
                        && req.body.Format_of_the_Book != null && isFinite(req.body.Format_of_the_Book)
                        && req.body.Admin_Share_Amount != null && isFinite(req.body.Admin_Share_Amount) && !isNaN(req.body.Admin_Share_Amount)

                        && req.body.Condition != null
                        //&& req.body.Admin_Share_Percent != null
                    ) {
                        //Store Product Information
                        StoreController.Update_Store_Product(req.body, BranchData).then((Result) => {
                            res.json(Result);
                        }).catch(err => res.json(err));
                    } else {
                        // console.log("resp8")
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.ENTER_ALL_TAGS
                            }
                        }));
                    }
                }
            })
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Activate_Inactivate_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null && req.body.BranchID != null) {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                if (err) {
                    res.send(JSON.stringify(BranchData));
                } else {
                    if (req.body.ProductID != null && req.body.ProductID != "") {
                        //Store Product Information
                        StoreController.Activate_Inactivate_Store_Product(req.body, BranchData).then((Result) => {
                            res.json(Result);
                        }).catch(err => res.json(err));
                    } else {
                        // console.log("resp8")
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.ENTER_ALL_TAGS
                            }
                        }));
                    }
                }
            })
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Delete_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null && req.body.BranchID != null) {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                if (err) {
                    res.send(JSON.stringify(BranchData));
                } else {
                    if (req.body.ProductID != null && req.body.ProductID != "") {
                        //Store Product Information
                        StoreController.Delete_Store_Product(req.body, BranchData).then((Result) => {
                            res.json(Result);
                        }).catch(err => res.json(err));
                    } else {
                        // console.log("resp8")
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.ENTER_ALL_TAGS
                            }
                        }));
                    }
                }
            })
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Add_Store_Product_Existing = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null && req.body.BranchID != null) {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                if (err) {
                    res.send(JSON.stringify(BranchData));
                } else {
                    if (req.body.ProductID != null && req.body.ProductID != ""
                        && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                        && req.body.Price != null && req.body.Price != ''
                        && req.body.Discount_Percent != null && req.body.Discount_Percent != ''
                        && req.body.Condition != null
                        //&& req.body.Admin_Share_Percent != null
                    ) {
                        //Store Product Information
                        StoreController.Store_Product_Information_Existing(req.body, BranchData).then((Result) => {
                            res.json(Result);
                        }).catch(err => res.json(err));
                    } else {
                        // console.log("resp8")
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.ENTER_ALL_TAGS
                            }
                        }));
                    }
                }
            })
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Add_Store_Product = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ""
            && req.body.Product_Name != null && req.body.Product_Name != ""
            && req.body.Unique_ProductID != null && req.body.Unique_ProductID != ""
            && req.body.ZoneID != null && req.body.ZoneID != ""
            && req.body.CountryID != null && req.body.CountryID != ""
            && req.body.CityID != null && req.body.CityID != ""
            && req.body.About_Authors != null && req.body.About_Authors != ""
            && req.body.Product_Description != null && req.body.Product_Description != ""
            && req.body.Authors != null && req.body.Authors != "" && typeof req.body.Authors === "object"
            && req.body.Authors.length > 0
            && req.body.Sales_Info != null && req.body.Sales_Info != "" && typeof req.body.Sales_Info === "object"
            && req.body.Sales_Info.length > 0
            && req.body.Product_ISBN != null && req.body.Product_ISBN != "" && typeof req.body.Product_ISBN === "object"
            && req.body.Product_ISBN.length > 0
            && req.body.CategoryID != null
            && req.body.SubCategoryID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BranchData = await CommonController.Check_For_Store_BranchID(req.body);
            let ValidateData = await CommonController.Check_For_Unique_Product(req.body.Unique_ProductID)
            ValidateData = await CommonController.Check_for_Country(req.body)
            ValidateData = await CommonController.Check_for_City(req.body)
            ValidateData = await CommonController.Check_for_Zone(req.body)
            for (const item of req.body.Sales_Info) {
                if (
                    item.Avaiable_Quantity != null && isFinite(item.Avaiable_Quantity)
                    && item.Condition != null && isFinite(item.Condition) && !isNaN(item.Condition)
                    && item.Branch_Discount_Price != null && isFinite(item.Branch_Discount_Price) && !isNaN(item.Branch_Discount_Price)
                    && item.Bookafella_Discount_Price != null && isFinite(item.Bookafella_Discount_Price) && !isNaN(item.Bookafella_Discount_Price)
                    && item.Format_of_the_Book != null && isFinite(item.Format_of_the_Book)
                    && item.Admin_Share_Amount != null && isFinite(item.Admin_Share_Amount) && !isNaN(item.Admin_Share_Amount)
                    && item.Length != null && isFinite(item.Length)
                    && item.Total_Pages != null && isFinite(item.Total_Pages)
                    && item.ImageID != null && item.ImageID != ''
                    && item.Height != null && isFinite(item.Height)
                    && item.Width != null && isFinite(item.Width)
                    && item.Price != null && isFinite(item.Price) && !isNaN(item.Price)
                    && item.Product_Edition != null && item.Product_Edition != ''
                    && item.Product_Published_On != null
                    && item.Product_Publisher != null && item.Product_Publisher != ''
                ) {
                    if (parseInt(item.Avaiable_Quantity) < 1) {
                        throw { success: false, extras: { msg: ApiMessages.QUANTITY_SHOULD_NOT_ZERO } };
                    }
                    if (parseInt(item.Format_of_the_Book) < 0 || parseInt(item.Format_of_the_Book) > 3) {
                        throw { success: false, extras: { msg: ApiMessages.INVALID_BOOK_FORMAT } };
                    }
                    let ImageData = await CommonController.Check_for_Image(item)
                    item.ImageData = ImageData
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            }
            let Result = await AdminController.Add_Store_Product(req.body, BranchData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_Store_Product_Old = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null && req.body.BranchID != null) {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                if (err) {
                    res.send(JSON.stringify(BranchData));
                } else {
                    if (
                        req.body.Product_Name != null && req.body.Product_Name != ""
                        && req.body.Unique_ProductID != null && req.body.Unique_ProductID != ""
                        && req.body.ZoneID != null && req.body.ZoneID != ""
                        && req.body.CountryID != null && req.body.CountryID != ""
                        && req.body.CityID != null && req.body.CityID != ""
                        && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                        && req.body.Product_Edition != null && req.body.Product_Edition != ''
                        && req.body.Product_Published_On != null && req.body.Product_Published_On != ''
                        && req.body.Product_Publisher != null && req.body.Product_Publisher != ''
                        && req.body.Review != null && req.body.About_Author != null
                        && req.body.Authors != null && req.body.Length != null
                        && req.body.Total_Pages != null && isFinite(req.body.Total_Pages)
                        && req.body.ImageID != null && req.body.ImageID != ''
                        && req.body.Height != null && req.body.Width != null
                        && req.body.Price != null && req.body.Price != ''
                        && req.body.Discount_Percent != null && req.body.Discount_Percent != ''
                        && req.body.Product_ISBN != null
                        && req.body.Condition != null
                        && req.body.CategoryID != null
                        && req.body.SubCategoryID != null
                        && req.body.Format_of_the_Book != null && isFinite(req.body.Format_of_the_Book)
                    ) {
                        StoreController.Store_Product_Information(req.body, BranchData).then((Result) => {
                            res.json(Result);
                        }).catch(err => res.json(err));
                    } else {
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.ENTER_ALL_TAGS
                            }
                        }));
                    }
                }
            })
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Create_Admin_User = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.Name != null
        && req.body.EmailID != null && req.body.PhoneNumber != null
        && req.body.Name != ""
        && req.body.EmailID != ""
        && isBoolean(req.body.Whether_Master) && req.body.Whether_Master != null
        && req.body.Admin_Permissions != null && typeof req.body.Admin_Permissions === 'object'
        && req.body.Password != null && req.body.Password != "") {

        AdminController.Check_for_Admin_and_Session(req.body, (err, MasterData) => {
            if (err) {
                res.send(AdminData);
            } else {
                if (Validator.isEmail(req.body.EmailID, EmailOptions)) {
                    AdminController.Check_Whether_Admin_Email_Already_Exist(req.body, function (err, ExistStatus) {
                        if (err) {
                            res.send(ExistStatus);
                        } else {
                            AdminController.Create_Admin_User(req.body, function (err, Result, Password) {
                                if (err) {
                                    res.send(Result);
                                } else {
                                    res.send(Result);
                                    // logger.info("Password");
                                    // logger.info(Password);
                                    AdminLogController.Create_Admin_User_Log(req.body, MasterData);
                                }
                            })
                        }
                    })
                } else {
                    res.send({ success: false, extras: { msg: ApiMessages.INVALID_EMAIL_FORMAT } })
                }
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Admin_Users = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.skip != null && req.body.limit != null && req.body.sortOptions != null) {
        //&& req.body.sortOptions != null
        AdminController.Check_for_Admin_and_Session(req.body, (err, MasterData) => {
            if (err) {
                res.send(MasterData);
            } else {
                AdminController.List_All_Admin_Users(req.body, (Result) => {
                    res.send(Result);
                    AdminLogController.List_All_Admin_Users_Log(req.body, MasterData);
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

// AdminMediator.Admin_Upload_Image = (req, res) => {
//     let generateFilename = (type) => {
//         let date = new Date().getTime();
//         let charBank = "abcdefghijklmnopqrstuvwxyz";
//         let fstring = '';
//         for (let i = 0; i < 15; i++) {
//             fstring += charBank[parseInt(Math.random() * charBank.length)];
//         }
//         return (fstring += date + '_' + type);
//     }
//     let values, filename, tmpFile, extension, contentType;
//     let nfile_100, nfile_250, nfile_550, nfile_900, nfile_Original;
//     let Image100, Image250, Image550, Image900, ImageOriginal;

//     values = req.body;
//     Image100 = generateFilename('100');
//     Image250 = generateFilename('250');
//     Image550 = generateFilename('550');
//     Image900 = generateFilename('900');
//     ImageOriginal = generateFilename('Original');

//     //var regex = /^data:([A-Za-z-+\/]+);base64,(.+)$/;
//     let mac = values.file//.match(regex);    
//     base64ImageToFile(mac, os.tmpdir() + '', ImageOriginal, function (err, imgPath) {

//         tmpFile = os.tmpdir() + '/' + ImageOriginal + '.jpeg';
//         filename = ImageOriginal + '.jpeg';
//         extension = 'jpeg';
//         contentType = 'image/jepg'

//         if (values.AdminID != null && values.SessionID != null && values.Serial_Number != null) {
//             AdminController.Check_for_Admin_and_Session(values, (err, AdminData) => {
//                 if (err) {
//                     res.send(AdminData);
//                 } else {
//                     // AdminController.Check_For_Admin_Image_Serial_Number(values, AdminData, (err, SerialNumberStatus) => {
//                     //     if (err) {
//                     //         res.send(SerialNumberStatus);
//                     //     } else {
//                     let ImageID = uuid.v4();
//                     nfile_Original = os.tmpdir() + '/' + ImageOriginal + '.' + extension;
//                     ImageOriginal = ImageOriginal + '.' + extension;
//                     nfile_100 = os.tmpdir() + '/' + Image100 + '.' + extension;
//                     Image100 = Image100 + '.' + extension;
//                     nfile_250 = os.tmpdir() + '/' + Image250 + '.' + extension;
//                     Image250 = Image250 + '.' + extension;
//                     nfile_550 = os.tmpdir() + '/' + Image550 + '.' + extension;
//                     Image550 = Image550 + '.' + extension;
//                     nfile_900 = os.tmpdir() + '/' + Image900 + '.' + extension;
//                     Image900 = Image900 + '.' + extension;
//                     let date = new Date();
//                     let Data = {
//                         ImageID: ImageID,
//                         Serial_Number: parseInt(values.Serial_Number),
//                         AdminID: AdminData.AdminID,
//                         Image100: Image100,
//                         Image250: Image250,
//                         Image550: Image550,
//                         Image900: Image900,
//                         ImageOriginal: ImageOriginal,
//                         created_at: date,
//                         updated_at: date
//                     };
//                     Admin_Images(Data).save();
//                     res.send({
//                         success: true, extras: {
//                             Status: "Image Uploaded Successfully",
//                             ImageID: ImageID,
//                             Image100: config.S3URL + Image100,
//                             Image250: config.S3URL + Image250,
//                             Image550: config.S3URL + Image550,
//                             Image900: config.S3URL + Image900,
//                             ImageOriginal: config.S3URL + ImageOriginal
//                         }
//                     })
//                     fs.rename(tmpFile, nfile_Original, () => {
//                         let ResizeData = [
//                             {
//                                 height: 100,
//                                 fname: Image100,
//                                 nfile: nfile_100
//                             }, {
//                                 height: 250,
//                                 fname: Image250,
//                                 nfile: nfile_250
//                             }, {
//                                 height: 550,
//                                 fname: Image550,
//                                 nfile: nfile_550
//                             }, {
//                                 height: 900,
//                                 fname: Image900,
//                                 nfile: nfile_900
//                             }
//                         ];
//                         AdminController.Admin_Upload_Image_Functionality(values, ResizeData, AdminData, contentType, ImageID, Image100, Image250, Image550, Image900, ImageOriginal, nfile_Original, (Result) => {
//                             logger.info(Result);
//                         })
//                     })
//                     //     }
//                     // })
//                 }
//             })
//         } else {
//             res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
//         }
//     })
// }

// AdminMediator.Create_Store = (req, res) => {
//     if (req.body.Store_Entity_Name != null && req.body.Store_Entity_Name != "" && req.body.Branch_Name != "" && req.body.ImageID != "") {
//         var Store_Entity_Name = String(req.body.Store_Entity_Name);
//         Store_Entity_Name = Store_Entity_Name.replace(/\s\s+/g, ' ');
//         Store_Entity_Name = Store_Entity_Name.replace(/  +/g, ' ');
//         Store_Entity_Name = Store_Entity_Name.replace(/^ /, '');
//         Store_Entity_Name = Store_Entity_Name.replace(/\s\s*$/, '');
//         Store_Entity_Name = format_str(Store_Entity_Name);
//         AdminMediator.Check_Whether_StoreName_Exist_Or_Not(Store_Entity_Name, function (err, EntityData) {
//             if (err) {
//                 AdminMediator.Create_Store_Entity(req.body, function (err, Entity_Status, EntityData, EntityID) {
//                     if (!err) {
//                         Store_Branch(EntityData);
//                     }
//                 })
//             } else {
//                 Store_Branch(EntityData);
//             }
//         })
//     } else {
//         res.send(new ApiResponce({
//             success: false,
//             extras: {
//                 msg: ApiMessages.ENTER_ALL_TAGS
//             }
//         }));
//     }
//     function Store_Branch(EntityData) {
//         if (req.body.Branch_Name != null && req.body.AdminData != null
//             && req.body.ImageID != null && req.body.Address != null
//             && req.body.Latitude != null && req.body.Longitude != null && req.body.BranchURL != null
//             && req.body.Monday_Available != null && req.body.Tuesday_Available != null
//             && req.body.Wednesday_Available != null && req.body.Thursday_Available != null
//             && req.body.Friday_Available != null && req.body.Saturday_Available != null && req.body.Sunday_Available != null) {
//             var Branch_Name = String(req.body.Branch_Name);
//             Branch_Name = Branch_Name.replace(/\s\s+/g, ' ');
//             Branch_Name = Branch_Name.replace(/  +/g, ' ');
//             Branch_Name = Branch_Name.replace(/^ /, '');
//             Branch_Name = Branch_Name.replace(/\s\s*$/, '');
//             Branch_Name = format_str(Branch_Name);
//             if (req.body.AdminData.length > 0) {
//                 AdminMediator.Check_Whether_Store_Branch_Name_Already_Exists(Branch_Name, function (err, BranchStaus) {
//                     if (err) {
//                         res.send(JSON.stringify(BranchStaus));
//                     } else {
//                         AdminController.Validate_Store_Branch_URL(req.body.BranchURL, (err, MasterData) => {
//                             if (err) {
//                                 res.send(MasterData);
//                             } else {
//                                 AdminMediator.Add_Entity_Branch(req.body, Branch_Name, EntityData, function (Result, BranchData) {
//                                     res.send(JSON.stringify(Result));
//                                     async.eachSeries(req.body.AdminData, function (item, callback) {
//                                         var Name = item.Name;
//                                         var PhoneNumber = item.PhoneNumber;
//                                         var EmailID = item.EmailID;
//                                         var Password = item.Password;
//                                         AdminMediator.Check_For_Admin_Phone_Branch(Name, PhoneNumber, EmailID, BranchData, function (err, AdminValidityStatus) {
//                                             if (err) {
//                                                 console.log("Admin Operations finishesd");
//                                                 callback();
//                                             } else {
//                                                 AdminMediator.Find_and_Update_CustomerSeqID(function (err, SequenceNumber) {
//                                                     if (!err) {
//                                                         AdminMediator.Create_Admin_Users(Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, function (err, AdminStatus) {
//                                                             console.log("Admin Created and MEssage Sent");
//                                                             callback();
//                                                         })
//                                                     }
//                                                 })
//                                             }
//                                         })
//                                     }, function (err) {
//                                         if (!err) {
//                                             console.log("All Admin Operations Completed");
//                                         } else {
//                                             console.log(err);
//                                         }
//                                     })
//                                 })

//                             }
//                         })
//                     }
//                 })
//             } else if (req.body.AdminData.length <= 0) {
//                 res.send(new ApiResponce({
//                     success: false,
//                     extras: {
//                         msg: ApiMessages.DETAILS_ALREADY_EXISTS
//                     }
//                 }));
//             }
//         } else {
//             res.send(new ApiResponce({
//                 success: false,
//                 extras: {
//                     msg: ApiMessages.ENTER_ALL_TAGS
//                 }
//             }));
//         }
//     }
// }

// AdminMediator.Find_and_Update_CustomerSeqID = function (callback) {
//     Counters.findOneAndUpdate({
//         "_id": "customerid"
//     }, {
//             $set: {
//                 _id: "customerid"
//             },
//             $inc: {
//                 "seq": 1
//             }
//         }, {
//             upsert: true,
//             returnNewDocument: true
//         }).exec(function (err, Result) {
//             console.log(Result);
//             var SequenceNumber = Result.seq;
//             return callback(false, parseInt(SequenceNumber));
//         })
// };

// //CHECK WHETHER CATEGORY NAME EXIST OR NOT
// AdminMediator.Check_Whether_Store_Branch_Name_Already_Exists = function (Branch_Name, callback) {
//     Store_Branch.findOne({ Branch_Name: Branch_Name }).exec(function (err, Result) {
//         if (err) {
//             console.log(err);
//             return callback(true, new ApiResponce({
//                 success: false,
//                 extras: {
//                     msg: ApiMessages.DATABASE_ERROR
//                 }
//             }));
//         } else {
//             if (Result == null) {
//                 callback(false);
//             } else if (Result != null) {
//                 return callback(true, new ApiResponce({
//                     success: false,
//                     extras: {
//                         msg: ApiMessages.STORE_BRANCH_NAME_ALREADY_EXIST
//                     }
//                 }));
//             }
//         }
//     })
// };
// AdminMediator.Add_Entity_Branch = function (values, Branch_Name, EntityData, callback) {
//     var Monday_Available;
//     var Tuesday_Available;
//     var Wednesday_Available;
//     var Thursday_Available;
//     var Friday_Available;
//     var Saturday_Available;
//     var Sunday_Available;
//     var Monday_Timings = {};
//     var Tuesday_Timings = {};
//     var Wednesday_Timings = {};
//     var Thursday_Timings = {};
//     var Friday_Timings = {};
//     var Saturday_Timings = {};
//     var Sunday_Timings = {};

//     if (values.Monday_Available == true || values.Monday_Available == "true") {
//         Monday_Available = true;
//         Monday_Timings = values.Monday_Timings;
//     } else {
//         Monday_Available = false;
//     }
//     if (values.Tuesday_Available == true || values.Tuesday_Available == "true") {
//         Tuesday_Available = true;
//         Tuesday_Timings = values.Tuesday_Timings;
//     } else {
//         Tuesday_Available = false;
//     }
//     if (values.Wednesday_Available == true || values.Wednesday_Available == "true") {
//         Wednesday_Available = true;
//         Wednesday_Timings = values.Wednesday_Timings;
//     } else {
//         Wednesday_Available = false;
//     }
//     if (values.Thursday_Available == true || values.Thursday_Available == "true") {
//         Thursday_Timings = values.Thursday_Timings;
//         Thursday_Available = true;
//     } else {
//         Thursday_Available = false;
//     }
//     if (values.Friday_Available == true || values.Friday_Available == "true") {
//         Friday_Timings = values.Friday_Timings;
//         Friday_Available = true;
//     } else {
//         Friday_Available = false;
//     }
//     if (values.Saturday_Available == true || values.Saturday_Available == "true") {
//         Saturday_Timings = values.Saturday_Timings;
//         Saturday_Available = true;
//     } else {
//         Saturday_Available = false;
//     }
//     if (values.Sunday_Available == true || values.Sunday_Available == "true") {
//         Sunday_Timings = values.Sunday_Timings;
//         Sunday_Available = true;
//     } else {
//         Sunday_Available = false;
//     }

//     var BranchID = uuid();
//     var date = new Date();
//     var BranchData = new Store_Branch({
//         EntityID: EntityData.EntityID,
//         BranchID: BranchID,
//         Store_Entity_Name: EntityData.Store_Entity_Name,
//         Branch_Name: Branch_Name,
//         Branch_PhoneNumber: values.Branch_PhoneNumber,
//         BranchURL: values.BranchURL,
//         Description: values.Description,
//         ImageID: values.ImageID,
//         Address: values.Address,
//         Latitude: parseFloat(values.Latitude),
//         Longitude: parseFloat(values.Longitude),
//         Point: [parseFloat(values.Longitude), parseFloat(values.Latitude)],
//         Monday_Available: Monday_Available,
//         Monday_Timings: Monday_Timings,
//         Tuesday_Available: Tuesday_Available,
//         Tuesday_Timings: Tuesday_Timings,
//         Wednesday_Available: Wednesday_Available,
//         Wednesday_Timings: Wednesday_Timings,
//         Thursday_Available: Thursday_Available,
//         Thursday_Timings: Thursday_Timings,
//         Friday_Available: Friday_Available,
//         Friday_Timings: Friday_Timings,
//         Saturday_Available: Saturday_Available,
//         Saturday_Timings: Saturday_Timings,
//         Sunday_Available: Sunday_Available,
//         Sunday_Timings: Sunday_Timings,
//         AdminData: [],
//         created_at: date,
//         updated_at: date
//     });
//     BranchData.save(function (err, Result) {
//         if (err) {
//             console.log(err);
//         } else {
//             return callback(new ApiResponce({
//                 success: true,
//                 extras: {
//                     Status: 'Store Branch Added Successfully'
//                 }
//             }), JSON.parse(JSON.stringify(Result)));
//         }
//     });
// };
// //Generating the Random Number for Security
// AdminMediator.RandomNumber = function () {
//     var charBank = "123456789";
//     var fstring = '';
//     for (var i = 0; i < 6; i++) {
//         fstring += charBank[parseInt(Math.random() * charBank.length)];
//     }
//     console.log("Random")
//     console.log(fstring);
//     return parseInt(fstring);
// };
// //Current Date Time;
// AdminMediator.DateTime = function () {
//     var fulldate = new Date();
//     var moment = require('moment');
//     var date = moment().utcOffset(330).format('YYYY-MM-DD');
//     var time = moment().utcOffset(330).format('H:mm:ss');

//     var datetime = date + ' ' + time;
//     return datetime;
// };
// AdminMediator.Create_Admin_Users = function (Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, callback) {
//     var salt = rand(160, 36);
//     //var Password = AdminMediator.RandomNumber();
//     console.log("Password " + Password);
//     var pass = Password + salt;
//     var date = AdminMediator.DateTime();
//     var BranchDetails = {
//         BranchID: BranchData.BranchID,
//         Branch_Name: BranchData.Branch_Name
//     }
//     var signup_date = new Date();
//     var moment = require('moment');
//     var format = 'H:mm:ss';
//     var time = moment().utcOffset(330).format(format);
//     var timearray = time.split(':');
//     var hour = parseInt(timearray[0]);
//     var minute = parseInt(timearray[1]);
//     var interval;
//     if (minute == 0) {
//         interval = hour;
//     } else {
//         interval = hour + 1;
//     }
//     var AdminData = new Customers({
//         CustomerID: uuid(),
//         acc_status: 1,
//         customerseqId: SequenceNumber,
//         First_name: Name,
//         Phone: PhoneNumber,
//         countryCode: "+91",
//         Email: EmailID,
//         Verify: 0,
//         Code: 0,
//         PasswordHash: crypto.createHash('sha512').update(pass).digest("hex"),
//         PasswordSalt: salt,
//         sessionToken: '',
//         First_Time_Login: true,
//         CurrentStatus: 1,
//         terms_cond: 1,
//         Created_dt: date,
//         Agreement_Time: date,
//         Whether_Store_Admin: true,
//         BranchData: BranchDetails,
//         Active_BranchID_Exist: true,
//         Active_BranchID: BranchData.BranchID,
//         StoreAdminStatus: true,
//         Signup_Date: signup_date,
//         Signup_Interval: interval
//     });
//     AdminData.save(function (err, Result) {
//         if (err) {
//             console.log(err);
//         } else {
//             // var URL = 'https://';
//             // var Message = 'Hi ' + BranchData.Branch_Name + ', Your UserName:' + PhoneNumber + ' ,Password: ' + Password + ' ,use this Credential and login at ' + URL;
//            // MSG91MOD.sendsmstocustomer(PhoneNumber, Message, function (err, msgStatus) {
//                 console.log("Admin Creadted Branch Stored and Message Sent Successfully");
//                 callback(false, 'Admin Created Successfully and Message Sent Successfully');
//                 var newquery = {
//                     BranchID: BranchData.BranchID
//                 };
//                 var newchanges = {
//                     $push: {
//                         AdminData: {
//                             StoreAdminID: Result.CustomerID
//                         }
//                     }
//                 };
//                 //var Message = "OTP is : " + Password;
//                 // MSG91MOD.sendsms(PhoneNumber,Message, function(err, msg){

//                 // });

//                 console.log("New Query ");
//                 console.log(newchanges);
//                 var newmultiplicity = {
//                     multi: false
//                 };
//                 Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
//                     if (err) {
//                         console.log(err);
//                     } else {
//                         console.log("Admin Updated in Branch");
//                     }
//                 })
//            // })
//         }
//     })
// }
// //Check for StoreName
// AdminMediator.Check_Whether_CategoryName_Exist_Or_Not = function (CategoryName, callback) {
//     Store_Categories.findOne({ CategoryName: CategoryName }).exec(function (err, CategoryData) {
//         if (err) {
//             console.log(err);
//         } else {
//             if (CategoryData == null) {
//                 callback(false, {});
//             } else if (CategoryData != null) {
//                 callback(true, CategoryData);
//             }
//         }
//     })
// }
// //Check Whether User Already Exist 
// AdminMediator.Check_For_Admin_Phone_Branch = function (Name, PhoneNumber, EmailID, BranchData, callback) {
//     var BranchDetails = {
//         BranchID: BranchData.BranchID,
//         Branch_Name: BranchData.Branch_Name
//     };
//     console.log("PhoneNumber");
//     console.log(PhoneNumber);
//     Customers.findOne({ Phone: PhoneNumber }).exec(function (err, Result) {
//         if (err) {
//             console.log(err);
//         } else {
//             if (Result == null) {
//                 console.log("USer Not Found");
//                 callback(false);
//             } else if (Result != null) {
//                 Customers.findOne({ Phone: PhoneNumber, Whether_Store_Admin: true }).exec(function (err, NewResult) {
//                     if (err) {
//                         console.log(err);
//                     } else {
//                         if (NewResult == null) {
//                             console.log("USer is not Admin");
//                             var squery = {
//                                 _id: Result._id
//                             };
//                             var branchArray = [];
//                             branchArray.push(BranchDetails);
//                             var schanges = {
//                                 Whether_Store_Admin: true,
//                                 StoreAdminStatus: true,
//                                 BranchData: branchArray,
//                                 Active_BranchID_Exist: true,
//                                 Active_BranchID: BranchData.BranchID
//                             }
//                             var smultiplicity = {
//                                 multi: false
//                             }
//                             Customers.update(squery, schanges, smultiplicity).exec(function (err, AdminUpdatedStatus) {
//                                 if (!err) {
//                                     var newquery = {
//                                         BranchID: BranchData.BranchID
//                                     };
//                                     var newchanges = {
//                                         $push: {
//                                             AdminData: {
//                                                 StoreAdminID: Result._id
//                                             }
//                                         }
//                                     };
//                                     var newmultiplicity = {
//                                         multi: false
//                                     };
//                                     Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
//                                         if (err) {
//                                             console.log(err);
//                                         } else {
//                                             console.log("Admin Updated in Branch");
//                                             callback(true, false);
//                                         }
//                                     })
//                                 }
//                             })
//                         } else if (NewResult != null) {
//                             console.log("User and Admin")
//                             Customers.findOne({
//                                 _id: Result._id,
//                                 BranchData: {
//                                     $elemMatch: {
//                                         "BranchID": String(BranchData.BranchID)
//                                     }
//                                 }
//                             }).exec(function (err, BranchStaus) {
//                                 if (err) {
//                                     console.log(err);
//                                 } else {
//                                     if (BranchStaus == null) {
//                                         var query = {
//                                             _id: Result._id
//                                         };

//                                         var changes = {
//                                             $push: {
//                                                 BranchData: BranchDetails
//                                             }
//                                         }
//                                         var multiplicity = {
//                                             multi: false
//                                         }
//                                         console.log("Branch Changes");
//                                         console.log(changes);
//                                         Customers.update(query, changes, multiplicity).exec(function (err, UpdatedStatus) {
//                                             if (err) {
//                                                 console.log(err);
//                                             } else {
//                                                 console.log("Branch Updated in Admin");
//                                                 var newquery = {
//                                                     BranchID: BranchData.BranchID
//                                                 };
//                                                 var newchanges = {
//                                                     $push: {
//                                                         AdminData: {
//                                                             StoreAdminID: Result._id
//                                                         }
//                                                     }
//                                                 };
//                                                 var newmultiplicity = {
//                                                     multi: false
//                                                 };
//                                                 Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
//                                                     if (err) {
//                                                         console.log(err);
//                                                     } else {
//                                                         console.log("Admin Updated in Branch");
//                                                         callback(true, false);
//                                                     }
//                                                 })
//                                             }
//                                         })
//                                     } else if (BranchStaus != null) {
//                                         console.log("Admin Branch Already Exist");
//                                         callback(true, true);
//                                     }
//                                 }
//                             })
//                         }
//                     }
//                 })
//             }
//         }
//     })
// }
// AdminMediator.Check_Whether_StoreName_Exist_Or_Not = (Store_Entity_Name, callback) => {
//     Store_Entity.findOne({ Store_Entity_Name: Store_Entity_Name }).exec(function (err, EntityData) {
//         if (err) {
//             console.log(err);
//         } else {
//             if (EntityData == null) {
//                 callback(true, {});
//             } else if (EntityData != null) {
//                 callback(false, EntityData);
//             }
//         }
//     })

// }
// //Store Category Details
// AdminMediator.Add_Category = function (CategoryName, callback) {
//     var CategoryID = uuid();
//     var date = new Date();
//     var CategoryData = new Store_Categories({
//         CategoryID: CategoryID,
//         CategoryName: CategoryName,
//         created_at: date,
//         updated_at: date
//     });
//     CategoryData.save(function (err, Result) {
//         if (err) {
//             console.log(err);
//         } else {
//             return callback(new ApiResponce({
//                 success: true,
//                 extras: {
//                     Status: 'Category Stored Successfully'
//                 }
//             }), Result);
//         }
//     })
// }
// //CHECK WHETHER CATEGORY NAME EXIST OR NOT
// AdminMediator.Check_Whether_Store_Branch_Name_Already_Exists_Update = function (values, Branch_Name, callback) {
//     Store_Branch.findOne({ BranchID: { $ne: values.BranchID }, Branch_Name: Branch_Name }).exec(function (err, Result) {
//         if (err) {
//             console.log(err);
//         } else {
//             if (Result == null) {
//                 callback(false);
//             } else if (Result != null) {
//                 return callback(true, new ApiResponce({
//                     success: false,
//                     extras: {
//                         msg: ApiMessages.STORE_BRANCH_NAME_ALREADY_EXIST
//                     }
//                 }));
//             }
//         }
//     })
// }
// AdminMediator.Create_Store_Entity = function (values, callback) {
//     var EntityID = uuid();
//     var date = new Date();
//     var Store_Entity_Name = String(values.Store_Entity_Name);
//     Store_Entity_Name = Store_Entity_Name.replace(/\s\s+/g, ' ');
//     Store_Entity_Name = Store_Entity_Name.replace(/  +/g, ' ');
//     Store_Entity_Name = Store_Entity_Name.replace(/^ /, '');
//     Store_Entity_Name = Store_Entity_Name.replace(/\s\s*$/, '');
//     Store_Entity_Name = format_str(Store_Entity_Name);
//     var EntityData = new Store_Entity({
//         EntityID: EntityID,
//         Store_Entity_Name: Store_Entity_Name,
//         Website: values.Website,
//         Description: values.Description,
//         Status: true,
//         created_at: date,
//         updated_at: date
//     })
//     EntityData.save(function (err, Result) {
//         if (err) {
//             console.log(err);
//         }
//         callback(false, 'Entity Added Successfully', JSON.parse(JSON.stringify(Result)), EntityID);
//     })
// };

AdminMediator.Branch_Cover_Image_Data_Update = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null && req.body.ImageID != null
        && req.body.BranchID != "" && req.body.ImageID != "") {
        var values = req.body;
        AdminController.Check_for_Admin_and_Session(values, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Get_Branch_Data(values.BranchID, (err, ImageData) => {
                    if (err) {
                        res.send(ImageData);
                    } else {
                        if (ImageData.Branch_Cover_ImageID !== "") {
                            AWSController.DeleteAWSImage(ImageData.Branch_Cover_ImageID).then((status) => {
                                console.log(status);
                            });
                        }
                    }
                });
                var newquery = {
                    BranchID: values.BranchID
                };
                var newchanges = {
                    Branch_Cover_ImageID: values.ImageID,
                    updated_at: new Date()
                };
                var newmultiplicity = {
                    multi: false
                };
                Store_Branch.updateOne(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.send({ success: true, extras: { Status: "Branch Cover image data updated successfully." } })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
};

AdminMediator.Branch_Gallery_Image_Data_Update = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null && req.body.NewImageID != null
        && req.body.BranchID != "" && req.body.NewImageID != "" && req.body.ExistingImageID != null) {
        var values = req.body;
        AdminController.Check_for_Admin_and_Session(values, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                if (req.body.ExistingImageID != '') {
                    AWSController.DeleteAWSImage(values.ExistingImageID).then((status) => {
                        console.log(status);
                    });
                    var newquery = {
                        BranchID: values.BranchID,
                        "Branch_Gallery_Images.ImageID": values.ExistingImageID
                    };
                    var newchanges = {
                        $set: {
                            "Branch_Gallery_Images.$.ImageID": values.NewImageID,
                            updated_at: new Date()
                        }
                    };
                    var newmultiplicity = {
                        multi: false
                    };
                    Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                        if (err) {
                            console.log(err);
                        } else {
                            res.send({ success: true, extras: { Status: "Branch gallery image data updated successfully." } })
                        }
                    })
                } else {
                    var newquery = {
                        BranchID: values.BranchID,

                    };
                    var newchanges = {
                        $push: {
                            Branch_Gallery_Images: {
                                ImageID: values.NewImageID
                            }
                        },
                        $set: {
                            updated_at: new Date()
                        }
                    };
                    var newmultiplicity = {
                        multi: false
                    };
                    Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                        if (err) {
                            console.log(err);
                        } else {
                            res.send({ success: true, extras: { Status: "Branch gallery image data updated successfully." } })
                        }
                    })
                }



            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
};

AdminMediator.Store_Admin_Login = (req, res) => {
    if (req.body.Phone != null && req.body.Password != null) {
        AdminController.findCustomerPasswordTries(req.body, function (err, responcer) {
            if (err) {
                res.send(JSON.stringify(responcer));
            } else {
                AdminController.Check_Whether_PhoneNumber_Exist(req.body, function (err, CustomerData) {
                    if (err) {
                        res.send(new ApiResponce({
                            success: false,
                            extras: {
                                LoginStatus: false,
                                Status: "You are Not Registered.Please Contact Customer Care at " + config.Customer__Care
                            }
                        }));
                    } else {
                        // console.log(CustomerData.BranchData);
                        if (CustomerData.Whether_Store_Admin == false || CustomerData.Whether_Store_Admin == null) {
                            res.send(new ApiResponce({
                                success: false,
                                extras: {
                                    LoginStatus: false,
                                    Status: "You are Not Registered.Please Contact Customer Care at " + config.Customer__Care
                                }
                            }));
                        } else if (CustomerData.Whether_Store_Admin == true) {
                            if (CustomerData.StoreAdminStatus == false || CustomerData.StoreAdminStatus == null) {
                                res.send(new ApiResponce({
                                    success: false,
                                    extras: {
                                        LoginStatus: false,
                                        Status: "You Account is Inactive.Please Contact Customer Care at " + config.Customer__Care
                                    }
                                }));
                            } else if (CustomerData.StoreAdminStatus == true) {
                                AdminController.CustomerSignIn(req.body, CustomerData, function (err, LoginStatus) {
                                    if (err) {
                                        res.send(JSON.stringify(LoginStatus));
                                    } else {
                                        AdminController.Check_Customer_Session_Stores(CustomerData, function (err, SessionStatus) {
                                            if (err) {
                                                AdminController.RegisteringCustomerSession_Store(CustomerData, function (err, SessionData) {
                                                    console.log("Entering New One")
                                                    SendBranchValues(SessionData);
                                                })
                                            } else {
                                                AdminController.UpdatingCustomerSession_Store(CustomerData, function (err, SessionData) {
                                                    console.log("Entering Updated One")
                                                    SendBranchValues(SessionData);
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        }
                    }
                })
            }
        })
        function SendBranchValues(SessionData) {
            var query = {
                'AdminData.StoreAdminID': SessionData.extras.StoreAdminData.StoreAdminID
            };

            Store_Branch.find(query).sort({ created_at: -1 }).exec(function (err, Result) {
                if (!err) {
                    var BranchData = [];
                    async.each(Result, function (item, callback) {
                        BranchData.push({
                            BranchID: item.BranchID,
                            Branch_Name: item.Branch_Name,
                            Branch_PhoneNumber: item.Branch_PhoneNumber,
                            Address: item.Address,
                            Branch_Image_URL: config.S3URL + item.Branch_Image_URL
                        })
                        callback();
                    }, function (err) {
                        if (!err) {
                            res.send(new ApiResponce({
                                success: true,
                                extras: {
                                    Status: "Login Successfully",
                                    StoreAdminData: {
                                        StoreAdminID: SessionData.extras.StoreAdminData.StoreAdminID,
                                        SessionID: SessionData.extras.StoreAdminData.SessionID,
                                        First_name: SessionData.extras.StoreAdminData.First_name,
                                        First_Time_Login: SessionData.extras.StoreAdminData.First_Time_Login,
                                        BranchData: BranchData,
                                        Active_BranchID_Exist: SessionData.extras.StoreAdminData.Active_BranchID_Exist,
                                        Active_BranchID: SessionData.extras.StoreAdminData.Active_BranchID
                                    },
                                    LoginStatus: true
                                }
                            }));
                        }
                    })
                }
            })
        }
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Branch_URL_Validation = function (req, res) {
    if (req.body.BranchURL != null && req.body.BranchURL != "" && req.body.AdminID != null && req.body.SessionID != "") {
        AdminController.Check_for_Admin_and_Session(req.body, (err, MasterData) => {
            if (err) {
                res.send(MasterData);
            } else {
                AdminController.Validate_Store_Branch_URL(req.body.BranchURL, (err, MasterData) => {
                    if (err) {
                        res.send(MasterData);
                    } else {
                        res.send(MasterData);
                    }
                })
            }
        });
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.Branch_In_Detail = function (req, res) {
    if (req.body.BranchID != null && req.body.BranchID != "" && req.body.AdminID != null && req.body.SessionID != "") {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                    if (err) {
                        res.send(BranchData);
                    } else {
                        AdminController.Branch_In_Detail(BranchData, function (err, Result) {
                            if (err) {
                                res.send(new ApiResponce({
                                    success: false,
                                    extras: {
                                        msg: ApiMessages.DATABASE_ERROR,
                                    }
                                }));
                            } else {
                                if (Result.Branch_Gallery_Images.length > 0) {
                                    AdminController.Branch_Gallery_Details(Result, function (err, BranchDetails) {
                                        if (err) {
                                            res.send(new ApiResponce({
                                                success: false,
                                                extras: {
                                                    msg: ApiMessages.DATABASE_ERROR,
                                                }
                                            }));
                                        } else {
                                            AdminController.Gets_Image_Data(BranchDetails.Branch_Cover_ImageID, function (err, CoverImageData) {
                                                if (!err) {
                                                    BranchDetails.Branch_Cover_ImageURL = CoverImageData;
                                                    AdminController.Gets_Image_Data(BranchDetails.ImageID, function (err, ImageData) {
                                                        if (!err) {
                                                            BranchDetails.ImageURL = ImageData;
                                                            res.send(new ApiResponce({
                                                                success: true,
                                                                extras: {
                                                                    BranchData: BranchDetails
                                                                }
                                                            }));
                                                        }
                                                    })
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    res.send(new ApiResponce({
                                        success: true,
                                        extras: {
                                            Result: Result
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        });
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};



AdminMediator.Admin_Login = function (req, res) {
    if (req.body.EmailID != null && req.body.Password != null) {
        Admin_User.findOne({
            EmailID: req.body.EmailID
        }).exec(function (err, AdminData) {
            if (AdminData != null) {
                if (AdminData.Status == true) {
                    AdminController.AdminSignIn(req.body, AdminData, function (err, LoginStatus) {
                        if (err) {
                            res.send(JSON.stringify(LoginStatus));
                        } else {
                            AdminController.UpdatingAdmin_Session(AdminData, function (err, SessionId) {
                                if (!err) {
                                    var AdminID = AdminData.AdminID;
                                    var Admin_Name = AdminData.Name;
                                    var EmailID = AdminData.EmailID;
                                    var SessionID = SessionId;
                                    var AdminDetails = {
                                        AdminID: AdminID,
                                        Admin_Name: Admin_Name,
                                        EmailID: EmailID,
                                        SessionID: SessionID,
                                        PhoneNumber: AdminData.PhoneNumber,
                                        Whether_Master: AdminData.Whether_Master,
                                        Admin_Permissions: AdminData.Admin_Permissions,
                                        CreatedAt: AdminData.created_at
                                    }
                                    res.send(new ApiResponce({
                                        success: true,
                                        extras: {
                                            Status: "Login Successfully",
                                            AdminData: AdminDetails
                                        }
                                    }));
                                    var date = new Date();
                                    var moment = require('moment');
                                    var time = moment().utcOffset(330).format('MMMM Do YYYY, h:mm:ss a');
                                    var Message = Admin_Name + ' have login on ' + time;
                                    var LogData = new Admin_Logs({
                                        AdminID: AdminID,
                                        Admin_Name: Admin_Name,
                                        Message: Message,
                                        created_at: date,
                                        updated_at: date
                                    });
                                    LogData.save();
                                }

                            })
                        }
                    })
                } else {
                    res.send(new ApiResponce({
                        success: false,
                        extras: {
                            msg: ApiMessages.Admin_Not_Active
                        }
                    }));
                }
            } else if (AdminData == null) {
                res.send(new ApiResponce({
                    success: false,
                    extras: {
                        msg: ApiMessages.Email_Not_Registered
                    }
                }));
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

AdminMediator.List_All_Stores = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_All_Stores(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}


//Check Whether User Already Exist 
AdminMediator.Check_For_Admin_Phone_And_Update_IF_Exists = function (Name, PhoneNumber, EmailID, BranchData, callback) {
    var BranchDetails = {
        BranchID: BranchData.BranchID,
        Branch_Name: BranchData.Branch_Name
    };
    // console.log("PhoneNumber");
    // console.log(PhoneNumber);
    Customers.findOne({ Phone: PhoneNumber }).exec(function (err, Result) {
        if (err) {
            console.log(err);
            callback(true, new ApiResponce({
                success: false,
                extras: {
                    msg: ApiMessages.DATABASE_ERROR
                }
            }));
        } else {
            if (Result == null) {
                console.log("USer Not Found");
                callback(false);
            } else if (Result != null) {
                Customers.findOne({ Phone: PhoneNumber, Whether_Store_Admin: true }).exec(function (err, NewResult) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (NewResult == null) {
                            console.log("USer is not Admin");
                            var squery = {
                                CustomerID: Result.CustomerID
                            };
                            var branchArray = [];
                            branchArray.push(BranchDetails);
                            var schanges = {
                                Whether_Store_Admin: true,
                                StoreAdminStatus: true,
                                BranchData: branchArray,
                                Active_BranchID_Exist: true,
                                Active_BranchID: BranchData.BranchID
                            }
                            var smultiplicity = {
                                multi: false
                            }
                            Customers.update(squery, schanges, smultiplicity).exec(function (err, AdminUpdatedStatus) {
                                if (!err) {
                                    var newquery = {
                                        BranchID: BranchData.BranchID
                                    };
                                    var newchanges = {
                                        $push: {
                                            AdminData: {
                                                StoreAdminID: Result.CustomerID
                                            }
                                        }
                                    };
                                    var newmultiplicity = {
                                        multi: false
                                    };
                                    Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log("Admin Updated in Branch");
                                            callback(true, new ApiResponce({
                                                success: true,
                                                extras: {
                                                    msg: "Admin Updated in Branch successfully"
                                                }
                                            }));
                                        }
                                    })
                                }
                            })
                        } else if (NewResult != null) {
                            console.log("User and Admin")
                            Customers.findOne({
                                CustomerID: Result.CustomerID,
                                BranchData: {
                                    $elemMatch: {
                                        "BranchID": String(BranchData.BranchID)
                                    }
                                }
                            }).exec(function (err, BranchStaus) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    if (BranchStaus == null) {
                                        var query = {
                                            CustomerID: Result.CustomerID
                                        };

                                        var changes = {
                                            $push: {
                                                BranchData: BranchDetails
                                            }
                                        }
                                        var multiplicity = {
                                            multi: false
                                        }
                                        console.log("Branch Changes");
                                        console.log(changes);
                                        Customers.update(query, changes, multiplicity).exec(function (err, UpdatedStatus) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                console.log("Branch Updated in Admin");
                                                var newquery = {
                                                    BranchID: BranchData.BranchID
                                                };
                                                var newchanges = {
                                                    $push: {
                                                        AdminData: {
                                                            StoreAdminID: Result.CustomerID
                                                        }
                                                    }
                                                };
                                                var newmultiplicity = {
                                                    multi: false
                                                };
                                                Store_Branch.update(newquery, newchanges, newmultiplicity).exec(function (err, Result) {
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        console.log("Admin Updated in Branch");
                                                        callback(true, new ApiResponce({
                                                            success: true,
                                                            extras: {
                                                                msg: "Admin Updated in Branch"
                                                            }
                                                        }));
                                                    }
                                                })
                                            }
                                        })
                                    } else if (BranchStaus != null) {
                                        console.log("Admin Branch Already Exist");
                                        callback(true, new ApiResponce({
                                            success: false,
                                            extras: {
                                                msg: "Admin Already Exists in Branch"
                                            }
                                        }));
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}

AdminMediator.Get_Single_Product_Details = function (req, res) {
    if (req.body.SessionID != null && req.body.AdminID != null
        && req.body.ProductID != null && req.body.ProductID != '') {
        CommonController.Check_for_Admin(req.body).then((AdminData) => {
            StoreController.Get_Single_Product_Details(req.body).then((Result) => {
                res.json(Result)
            }).catch(err => res.json(err));
        }).catch(err => res.json(err));
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};


AdminMediator.List_All_Products_With_Filters = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.skip != null && req.body.limit != null
        && req.body.Status != null
        && req.body.CountryID != null
        && req.body.CityID != null
        && req.body.ZoneID != null
        && req.body.BranchID != null
        && req.body.Search != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_All_Products_With_Filters(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Branches = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.skip != null && req.body.limit != null && req.body.Status != null
        && req.body.CountryID != null
        && req.body.CityID != null
        && req.body.ZoneID != null
        && req.body.BranchID != null
        && req.body.Search != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_All_Branches(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Branches_Lite = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_All_Branches_Lite(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Activate_Inactivate_Branch = async (req, res) => {
    try {
        // console.log('enter')
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.ChangeStatus != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BranchData = await CommonController.Check_for_Branch(req.body);
            let Result = await AdminController.Inactivate_Branch(req.body, BranchData);
            res.json(Result);
        } else {
            throw { success: false, extras: { code: 2, msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Find_All_Products_Lite = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.Search != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Find_All_Products_Lite(req.body, function (Result) {
                    res.json(Result)
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_All_Branch_Admins = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.BranchID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_All_Branch_Admins(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Delete_Branch_Admins = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.StoreAdminID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Delete_Branch_Admins(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Update_Branch_Admin = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.PhoneNumber != null
        && req.body.Name != null && req.body.EmailID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Update_Branch_Admins(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Add_Branch_Admin = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null
        && req.body.Name != null && req.body.PhoneNumber != null && req.body.EmailID != null
        && req.body.Password != null
    ) {
        var Name = req.body.Name, PhoneNumber = req.body.PhoneNumber, EmailID = req.body.EmailID, Password = req.body.Password;
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                Store_Branch.findOne({ BranchID: req.body.BranchID }).exec(function (err, BranchData) {
                    if (err) {
                        console.log(err);
                        return callback(true, new ApiResponce({
                            success: false,
                            extras: {
                                msg: ApiMessages.DATABASE_ERROR
                            }
                        }));
                    } else {

                        AdminMediator.Check_For_Admin_Phone_And_Update_IF_Exists(Name, PhoneNumber, EmailID, BranchData, function (err, AdminValidityStatus) {
                            if (err) {
                                res.send(AdminValidityStatus);
                            } else {
                                CommonController.Find_and_Update_CustomerSeqID(function (err, SequenceNumber) {
                                    if (!err) {
                                        AdminController.Create_Admin_Users(Password, Name, PhoneNumber, EmailID, BranchData, SequenceNumber, function (err, AdminStatus) {
                                            res.send(new ApiResponce({
                                                success: true,
                                                extras: {
                                                    Status: "Admin added successfully."
                                                }
                                            }));
                                        })
                                    }
                                })
                            }
                        })

                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Update_Branch_Details = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null
        && req.body.Branch_Name != null && req.body.Branch_PhoneNumber != null && req.body.BranchURL != null
        && req.body.Description != null && req.body.Address != null && req.body.Latitude != null
        && req.body.Longitude != null && req.body.ImageID != null
        && req.body.CountryID != null && req.body.CityID != null
        && req.body.ZoneID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Check_Whether_Store_Branch_Name_Already_Exists_Update(req.body, function (err, BranchStaus) {
                    if (err) {
                        res.send(JSON.stringify(BranchStaus));
                    } else {
                        AdminController.Validate_Store_Branch_URL_Update(req.body, (err, MasterData) => {
                            if (err) {
                                res.send(MasterData);
                            } else {
                                AdminController.Update_Branch_Details(req.body).then((Result) => {
                                    res.json(Result);
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Update_Branch_Timings = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null
        && req.body.Monday_Available != null && req.body.Tuesday_Available != null
        && req.body.Wednesday_Available != null && req.body.Thursday_Available != null
        && req.body.Friday_Available != null && req.body.Saturday_Available != null && req.body.Sunday_Available != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Update_Branch_Timings(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Active_InActive_Store_Branch = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.BranchID != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Active_InActive_Store_Branch(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Admin_Password_Change = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.New_Password != null && req.body.Old_Password != null) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Admin_Change_Password(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}


AdminMediator.Inactive_Admin = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null
        && req.body.XAdminID != null && req.body.XAdminID != ""
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Inactive_Admin(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.Admin_Password_Update = (req, res) => {
    if (req.body.AdminID != null && req.body.SessionID != null && req.body.Password != null
        && req.body.XAdminID != null && req.body.XAdminID != ""
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Admin_Update_Password(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}


// function format_str(str) {
//     var myArr = str.toLowerCase().split(" ");
//     for (var a = 0; a < myArr.length; a++) {
//         myArr[a] = myArr[a].charAt(0).toUpperCase() + myArr[a].substr(1);
//     }
//     return myArr.join(" ");
// }

AdminMediator.Add_Serviceable_Area = (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.AddressID != null
        && req.body.Address_Content != null
        && req.body.Latitude != null
        && req.body.Longitude != null
        && req.body.BranchID != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.Add_Serviceable_Area(req.body, (err, Result) => {
                    res.json(Result);
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.List_Serviceable_Area = (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.BranchID != null
        && req.body.Status != null
        && req.body.skip != null
        && req.body.limit != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.List_Serviceable_Area(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

AdminMediator.InActive_Serviceable_Area = (req, res) => {
    if (
        req.body.AdminID != null && req.body.SessionID != null
        && req.body.ServiceableID != null
    ) {
        AdminController.Check_for_Admin_and_Session(req.body, (err, AdminData) => {
            if (err) {
                res.send(AdminData);
            } else {
                AdminController.InActive_Serviceable_Area(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err));
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

///////////////New Code for Bookafella

AdminMediator.List_All_City_Zones = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Service_Type != null && isFinite(req.body.Service_Type)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.List_All_City_Zones(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}
AdminMediator.Edit_Zone = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
            && req.body.Zone_Title != null && req.body.Zone_Title != ''
            && req.body.strokeColor != null && req.body.strokeColor != ''
            && req.body.strokeOpacity != null && isFinite(req.body.strokeOpacity)
            && req.body.strokeWeight != null && isFinite(req.body.strokeWeight)
            && req.body.fillColor != null && req.body.fillColor != ''
            && req.body.fillOpacity != null && isFinite(req.body.fillOpacity)
            && req.body.draggable != null && isBoolean(req.body.draggable)
            && req.body.editable != null && isBoolean(req.body.editable)
            && req.body.visible != null && isBoolean(req.body.visible)
            && req.body.Polygon_Paths != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ZoneData = await CommonController.Check_for_Zone(req.body);
            let Service_Type = ZoneData.Service_Type;
            let coordinates = await AdminController.Validate_Zone_Path_and_Get_Geometry(req.body);
            let City_Zone_Version = await CommonController.Common_Increment_and_Get_City_Zone_Version(req.body.CityID, Service_Type);
            let UpdateExistingZones = await AdminController.Create_Zone_Update_All_City_Zones_Version(req.body.CityID, City_Zone_Version, Service_Type);
            let NewZoneData = await AdminController.Edit_Zone(req.body, coordinates, City_Zone_Version);
            res.json({ success: true, extras: { Status: "Updated Successfully" } });
            let Archive_All_City_Zones = await AdminController.Create_Update_Zone_Move_All_Exist_City_Zones_Archive(req.body.CityID, Service_Type);
            let StoreLog = await AdminLogController.Edit_Zone_Log(AdminData, ZoneData, NewZoneData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
};

AdminMediator.Create_Zone = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Zone_Title != null && req.body.Zone_Title != ''
            && req.body.Service_Type != null && isFinite(req.body.Service_Type)
            && req.body.strokeColor != null && req.body.strokeColor != ''
            && req.body.strokeOpacity != null && isFinite(req.body.strokeOpacity)
            && req.body.strokeWeight != null && isFinite(req.body.strokeWeight)
            && req.body.fillColor != null && req.body.fillColor != ''
            && req.body.fillOpacity != null && isFinite(req.body.fillOpacity)
            && req.body.draggable != null && isBoolean(req.body.draggable)
            && req.body.editable != null && isBoolean(req.body.editable)
            && req.body.visible != null && isBoolean(req.body.visible)
            && req.body.Polygon_Paths != null
        ) {
            let Service_Type = parseInt(req.body.Service_Type);
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let coordinates = await AdminController.Validate_Zone_Path_and_Get_Geometry(req.body);
            let Zone_Number = await CommonController.Generate_Counter_Sequence(`Zone-Service-Type-${Service_Type}`);
            let City_Zone_Version = await CommonController.Common_Increment_and_Get_City_Zone_Version(req.body.CityID, Service_Type);
            let UpdateExistingZones = await AdminController.Create_Zone_Update_All_City_Zones_Version(req.body.CityID, City_Zone_Version, Service_Type);
            let Result = await AdminController.Create_Zone(req.body, coordinates, Zone_Number, City_Zone_Version, Service_Type);
            res.json(Result);
            let Archive_All_City_Zones = await AdminController.Create_Update_Zone_Move_All_Exist_City_Zones_Archive(req.body.CityID, Service_Type);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
};

AdminMediator.Get_Zone_Information_In_Details = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ZoneData = await CommonController.Check_for_Zone(req.body);
            res.json({ success: true, extras: { Data: ZoneData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Delete_Zone = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ZoneID != null && req.body.ZoneID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Delete_Zone(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_City_Zones_Lite = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Service_Type != null && isFinite(req.body.Service_Type)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.List_All_City_Zones_Lite(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_Zone_Hub = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
            && req.body.Zone_Hub_Name != null && req.body.Zone_Hub_Name != ''
            && req.body.Address != null && req.body.Address != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude)
            && req.body.Dimensions_Type != null && isFinite(req.body.Dimensions_Type)
            && req.body.Length != null && isFinite(req.body.Length)
            && req.body.Breadth != null && isFinite(req.body.Breadth)
            && req.body.Height != null && isFinite(req.body.Height)
            && req.body.Weight_Type != null && isFinite(req.body.Weight_Type)
            && req.body.Maximum_Weight != null && isFinite(req.body.Maximum_Weight)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ZoneData = await CommonController.Check_for_Zone(req.body);
            let Service_Type = await ZoneData.Service_Type;
            let ValidityStatus = await CommonController.Common_Validate_Location_in_Zone_with_error(req.body.ZoneID, req.body.Latitude, req.body.Longitude);
            let City_Zone_Hub_Version = await CommonController.Common_Increment_and_Get_City_Zone_Hub_Version(req.body.CityID, Service_Type);
            let UpdateExistingZoneHubs = await AdminController.Add_Zone_Hub_Update_All_City_Zone_Hubs_Version(req.body.CityID, City_Zone_Hub_Version, Service_Type);
            let Result = await AdminController.Add_Zone_Hub(req.body, City_Zone_Hub_Version, Service_Type);
            res.json(Result);
            let Archive_All_City_Zone_Hubs = await AdminController.Add_Update_Zone_Hob_Move_All_Exist_City_Zone_Hubs_Archive(req.body.CityID, Service_Type);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Zone_Hubs = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ZoneData = await CommonController.Check_for_Zone(req.body);
            let Result = await AdminController.List_All_Zone_Hubs(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_Country = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryNumber != null && isFinite(req.body.CountryNumber)
            && req.body.CountryName != null && req.body.CountryName != ''
            && req.body.CountryCode != null && req.body.CountryCode != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Check_Whether_Country_Number_Already_Exist(req.body);
            ValidityStatus = await AdminController.Check_Whether_Country_Name_Already_Exist(req.body);
            let Result = await AdminController.Add_Country(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Validate_Country_Number = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CountryNumber != null && isFinite(req.body.CountryNumber)

        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Check_Whether_Country_Number_Already_Exist(req.body);
            res.json(ValidityStatus);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Get_City_JSON = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryCode != null && req.body.CountryCode != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_City_JSON(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Get_Country_JSON = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Country_JSON(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_Country = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let Result = await AdminController.Activate_Country(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_Country = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let Result = await AdminController.Inactivate_Country(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Countries_with_Filter = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let Result = await AdminController.List_All_Countries_with_Filter(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_State_Information = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.StateID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let StateData = await CommonController.Check_for_State(req.body);
            let Result = await AdminController.Activate_State_Information(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_State_Information = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.StateID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let StateData = await CommonController.Check_for_State(req.body);
            let Result = await AdminController.Inactivate_State_Information(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Update_State_Information = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.StateID != null
            && req.body.StateNumber != null && isFinite(req.body.StateNumber) && !isNaN(req.body.StateNumber)
            && req.body.StateName != null && req.body.StateName != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude) && !isNaN(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude) && !isNaN(req.body.Longitude)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let StateData = await CommonController.Check_for_State(req.body);
            let ValidityStatus = await AdminController.Check_Whether_State_Number_Already_Exist(req.body);
            ValidityStatus = await AdminController.Check_Whether_State_Name_Already_Exist(req.body);
            let Result = await AdminController.Update_State_Information(req.body, CountryData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_States_with_Filter = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let Result = await AdminController.List_All_States_with_Filter(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Add_State = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.StateNumber != null && isFinite(req.body.StateNumber) && !isNaN(req.body.StateNumber)
            && req.body.StateName != null && req.body.StateName != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude) && !isNaN(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude) && !isNaN(req.body.Longitude)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Check_Whether_State_Number_Already_Exist(req.body);
            ValidityStatus = await AdminController.Check_Whether_State_Name_Already_Exist(req.body);
            let Result = await AdminController.Add_State(req.body, CountryData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Validate_State_Number = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.StateID != null
            && req.body.StateNumber != null && isFinite(req.body.StateNumber) && !isNaN(req.body.StateNumber)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Check_Whether_State_Number_Already_Exist(req.body);
            res.json(ValidityStatus);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.Activate_City(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Inactivate_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.Inactivate_City(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Cities = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Cities(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Country_Cities = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let Result = await AdminController.List_All_Country_Cities(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Update_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
            && req.body.CityNumber != null && isFinite(req.body.CityNumber) && !isNaN(req.body.CityNumber)
            && req.body.CityName != null && req.body.CityName != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude) && !isNaN(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude) && !isNaN(req.body.Longitude)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ValidityStatus = await AdminController.Check_Whether_City_Number_Already_Exist(req.body);
            ValidityStatus = await AdminController.Check_Whether_City_Name_Already_Exist(req.body);
            let Result = await AdminController.Update_City(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityNumber != null && isFinite(req.body.CityNumber) && !isNaN(req.body.CityNumber)
            && req.body.CityName != null && req.body.CityName != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude) && !isNaN(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude) && !isNaN(req.body.Longitude)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Check_Whether_City_Number_Already_Exist(req.body);
            ValidityStatus = await AdminController.Check_Whether_City_Name_Already_Exist(req.body);
            let Result = await AdminController.Add_City(req.body, CountryData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Validate_City_Number = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
            && req.body.CityNumber != null && isFinite(req.body.CityNumber)

        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let ValidityStatus = await AdminController.Check_Whether_City_Number_Already_Exist(req.body);
            res.json(ValidityStatus);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_State_Cities = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.StateID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let StateData = await CommonController.Check_for_State(req.body);
            let Result = await AdminController.List_All_State_Cities(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Link_States_City = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.StateID != null && req.body.CityID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let StateData = await CommonController.Check_for_State(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ValidityStatus = await AdminController.Check_Whether_State_City_Validity(req.body);
            let Result = await AdminController.Link_States_City(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AdminMediator.Check_for_StoreID_Availability = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.StoreID != null && req.body.StoreID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_for_StoreID(req.body);
            res.json({ success: true, extras: { Status: "StoreID Available" } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Create_Store_Validate_OTP = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.StoreOTPID != null && req.body.StoreOTPID != ""
            && req.body.OTP != null && isFinite(req.body.OTP)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Check_for_Store_OTP_Tries_Count(req.body);
            let OTPData = await CommonController.Validate_OTP_Data(req.body)
            let ValidateData = await CommonController.Check_for_StoreID(OTPData);
            let StoreData = await CommonController.Fetch_Or_Creat_Store(OTPData);
            let BranchData = await AdminController.Store_Branch(OTPData, StoreData);
            res.json(BranchData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Create_Store = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Store_Entity_Name != null && req.body.Store_Entity_Name != ""
            && req.body.StoreID != null && req.body.StoreID != ""
            && req.body.Branch_Name != null && req.body.Branch_Name != ""
            && req.body.Website != null && req.body.Website != ""
            && req.body.Description != null && req.body.Description != ""
            && req.body.AdminData != null && typeof (req.body.AdminData) == "object" // Contains --> Name, PhoneNumber, EmailID, Password
            && req.body.ImageID != null && req.body.Latitude != null
            && req.body.Address != null && req.body.Longitude != null
            && req.body.BranchURL != null && req.body.Monday_Available != null
            && req.body.Tuesday_Available != null && req.body.Wednesday_Available != null
            && req.body.Thursday_Available != null && req.body.Friday_Available != null
            && req.body.Saturday_Available != null && req.body.Sunday_Available != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.ZoneID != null
            && req.body.FileID != null
            && req.body.FileID != ''
            && req.body.Bookstore_Type != null && isBoolean(req.body.Bookstore_Type)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_for_StoreID(req.body);
            let BranchData = await AdminController.Store_Branch_With_OTP(req.body);
            res.json(BranchData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_Vehicle = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.Vehicle_Name != null && req.body.Vehicle_Name != ''
            && req.body.Vehicle_Description != null && req.body.Vehicle_Description != ''
            && req.body.Vehicle_Type != null && isFinite(req.body.Vehicle_Type)
            && req.body.Weight_Type != null && isFinite(req.body.Weight_Type)
            && req.body.Maximum_Weight != null && isFinite(req.body.Maximum_Weight)
            && req.body.Dimensions_Type != null && isFinite(req.body.Dimensions_Type)
            && req.body.Length != null && isFinite(req.body.Length)
            && req.body.Breadth != null && isFinite(req.body.Breadth)
            && req.body.Height != null && isFinite(req.body.Height)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let Result = await AdminController.Add_Vehicle(req.body, AdminData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        console.error(error);
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Vehicles = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let Result = await AdminController.List_All_Vehicles(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Vehicle = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.VehicleID != null
            && req.body.Vehicle_Name != null && req.body.Vehicle_Name != ''
            && req.body.Vehicle_Description != null && req.body.Vehicle_Description != ''
            && req.body.Vehicle_Type != null && isFinite(req.body.Vehicle_Type)
            && req.body.Weight_Type != null && isFinite(req.body.Weight_Type)
            && req.body.Maximum_Weight != null && isFinite(req.body.Maximum_Weight)
            && req.body.Dimensions_Type != null && isFinite(req.body.Dimensions_Type)
            && req.body.Length != null && isFinite(req.body.Length)
            && req.body.Breadth != null && isFinite(req.body.Breadth)
            && req.body.Height != null && isFinite(req.body.Height)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let VehicleData = await CommonController.Check_for_Vehicle(req.body);
            let Result = await AdminController.Edit_Vehicle(req.body, AdminData, VehicleData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_Vehicle = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.VehicleID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let VehicleData = await CommonController.Check_for_Vehicle(req.body);
            let Result = await AdminController.Activate_Vehicle(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_Vehicle = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.VehicleID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let VehicleData = await CommonController.Check_for_Vehicle(req.body);
            // let ValidityStatus = await AdminController.Inactivate_Vehicle_Check_Driver_Assigned(req.body);
            let Result = await AdminController.Inactivate_Vehicle(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

// AdminMediator.List_All_Windows = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//             && req.body.CountryID != null && req.body.CityID != null
//             && req.body.Service_Type != null && isFinite(req.body.Service_Type)
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let CountryData = await CommonController.Check_for_Country(req.body);
//             let CityData = await CommonController.Check_for_City(req.body);
//             let Result = await WindowController.List_All_Windows(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(await CommonController.Common_Error_Handler(error));
//         }
//     }
// }

// AdminMediator.Add_Window = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//             && req.body.CountryID != null && req.body.CityID != null
//             && req.body.Whether_All_Zone != null && isBoolean(req.body.Whether_All_Zone)
//             && req.body.Service_Type != null && isFinite(req.body.Service_Type)
//             && req.body.Offset_Value != null && isFinite(req.body.Offset_Value) && !isNaN(req.body.Offset_Value)
//             && req.body.Window_Name != null && req.body.Window_Name != ''
//             && req.body.Start_at != null && req.body.Start_at != ''
//             && req.body.End_at != null && req.body.End_at != ''
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let CountryData = await CommonController.Check_for_Country(req.body);
//             let CityData = await CommonController.Check_for_City(req.body);
//             let ValidityStatus = await CommonController.Common_Validate_Start_End_Time(req.body);
//             let ZoneIDArray = await WindowController.Add_Window_Validate_Zones_Information(req.body);
//             ZoneIDArray = await WindowController.Add_Window_Validate_All_Zone_Array_with_Existing_Time(req.body, ZoneIDArray);
//             let Result = await WindowController.Add_Window(req.body, ZoneIDArray, CityData);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(await CommonController.Common_Error_Handler(error));
//         }
//     }
// }

AdminMediator.Create_Driver_Validate_OTP = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Driver_Country_Code != null && req.body.Driver_Country_Code != ''
            && req.body.Driver_Phone_Number != null && req.body.Driver_Phone_Number != ''
            && req.body.OTP != null && isFinite(req.body.OTP)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await DriverController.Check_for_Create_Driver_OTP_Tries_Count(req.body);
            let DriverOTPData = await DriverController.Validate_Create_Driver_OTP(req.body);
            let CountryData = await CommonController.Check_for_Country(DriverOTPData);
            let CityData = await CommonController.Check_for_City(DriverOTPData);
            let ZoneData = await CommonController.Check_for_Zone(DriverOTPData);
            let VehicleData = await CommonController.Check_for_Vehicle(DriverOTPData);
            let ImageData = await CommonController.Check_for_Image(DriverOTPData);
            let Licence_ImageData = await CommonController.Check_for_Licence_Image(DriverOTPData.Licence_ImageID);
            let FileData = [];
            for (const FileID of DriverOTPData.FileID_Array) {
                let FileDatax = await CommonController.Check_for_File({ FileID: FileID });
                FileData.push(FileDatax)
            }
            // ValidityStatus = await CommonController.Common_Email_Validation(DriverOTPData.Driver_EmailID);
            ValidityStatus = await CommonController.Common_Validate_Start_End_Time(DriverOTPData);
            ValidityStatus = await DriverController.Check_Whether_Driver_Phone_Number_Already_Registered(DriverOTPData);
            // ValidityStatus = await DriverController.Common_Validate_Vehicle_Number_Already_Exist(DriverOTPData);
            //let Categorories_Data = await AdminController.Common_Validate_CategoryID_Array(req.body);
            let Result = await DriverController.Create_Driver(DriverOTPData, ImageData, ZoneData, VehicleData, AdminData, CountryData, CityData, Licence_ImageData, FileData)//, Categorories_Data);
            res.json(Result);
            // if (ZoneData.Service_Type == 2) {
            //     let WindowFillingFunctionality = await CronController.Create_Date_Windows();
            // }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Create_Driver = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
            && req.body.VehicleID != null
            // && req.body.VehicleNumber != null // && req.body.VehicleNumber != ''
            && req.body.Driver_Name != null && req.body.Driver_Name != ''
            && req.body.Driver_Country_Code != null && req.body.Driver_Country_Code != ''
            && req.body.Driver_Phone_Number != null && req.body.Driver_Phone_Number != ''
            // && req.body.Driver_EmailID != null
            && req.body.Address != null && req.body.Address != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude)
            && req.body.Start_at != null && req.body.Start_at != ''
            && req.body.End_at != null && req.body.End_at != ''
            && req.body.ImageID != null
            && req.body.Licence_ImageID != null
            && req.body.FileID_Array != null && typeof req.body.FileID_Array === "object"
        ) {
            if (req.body.FileID_Array.length > 0 || req.body.FileID_Array < 4) {
                let AdminData = await CommonController.Check_for_Admin(req.body);
                let CountryData = await CommonController.Check_for_Country(req.body);
                let CityData = await CommonController.Check_for_City(req.body);
                let ZoneData = await CommonController.Check_for_Zone(req.body);
                let VehicleData = await CommonController.Check_for_Vehicle(req.body);
                let ImageData = await CommonController.Check_for_Image(req.body);
                let Licence_ImageData = await CommonController.Check_for_Licence_Image(req.body.Licence_ImageID);
                // let FileData = await CommonController.Check_for_File(req.body);
                let FileData = [];
                for (const FileID of req.body.FileID_Array) {
                    let FileDatax = await CommonController.Check_for_File({ FileID: FileID });
                    FileData.push(FileDatax)
                }
                // let ValidityStatus = await CommonController.Common_Email_Validation(req.body.Driver_EmailID);
                let ValidityStatus = await CommonController.Common_Validate_Start_End_Time(req.body);
                ValidityStatus = await DriverController.Check_Whether_Driver_Phone_Number_Already_Registered(req.body);
                // ValidityStatus = await DriverController.Common_Validate_Vehicle_Number_Already_Exist(req.body);
                let Result = await DriverController.Create_Driver_OTP(req.body);
                res.json(Result);
            } else {
                if (req.body.FileID_Array.length == 0) {
                    throw { success: false, extras: { msg: ApiMessages.UPLOAD_ATLEAST_ONE_DOCUMENT } }
                }
                if (req.body.FileID_Array.length >= 4) {
                    throw { success: false, extras: { msg: ApiMessages.MAXIMUM_DOCUMENT_UPLOAD_LIMIT_3 } }
                }
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Vehicles_with_Count = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await DriverController.List_All_Vehicles_with_Count(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Drivers_with_Filter = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null
            && req.body.CityID != null
            && req.body.ZoneID != null
            && req.body.Whether_Search_Filter != null && isBoolean(req.body.Whether_Search_Filter)
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.CountryID != "") {
                let CountryData = await CommonController.Check_for_Country(req.body);
            }
            if (req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body);
            }
            if (req.body.ZoneID != "") {
                let ZoneData = await CommonController.Check_for_Zone(req.body);
            }
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            ValidityStatus = await AdminController.Common_Whether_Search_Filter(req.body);
            let Result = await DriverController.List_All_Drivers_with_Filter(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Profile = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.Driver_Name != null && req.body.Driver_Name != ''
            && req.body.Driver_Country_Code != null && req.body.Driver_Country_Code != ''
            && req.body.Driver_Phone_Number != null && req.body.Driver_Phone_Number != ''
            // && req.body.Driver_EmailID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            // let ValidityStatus = await CommonController.Common_Email_Validation(req.body.Driver_EmailID);
            let ValidityStatus = await DriverController.Check_Whether_Driver_Phone_Number_Already_Registered(req.body);
            let Result = await DriverController.Edit_Driver_Profile(req.body, AdminData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Image = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.ImageID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body);
            let Result = await DriverController.Edit_Driver_Image(req.body, AdminData, DriverData, ImageData);
            res.json(Result);
            if (DriverData.ImageAvailable) {
                let RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.Image50);
                RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.Image100);
                RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.Image250);
                RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.Image550);
                RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.Image900);
                RemoveImage = await AWSController.DeleteAWSImage(DriverData.ImageData.ImageOriginal);
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Address = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.Address != null && req.body.Address != ''
            && req.body.Latitude != null && isFinite(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.Edit_Driver_Address(req.body, AdminData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Timings = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.Start_at != null && req.body.Start_at != ''
            && req.body.End_at != null && req.body.End_at != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let ValidityStatus = await CommonController.Common_Validate_Start_End_Time(req.body);
            let Result = await DriverController.Edit_Driver_Timings(req.body, AdminData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Vehicle = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.VehicleID != null
            // && req.body.VehicleNumber != null && req.body.VehicleNumber != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let VehicleData = await CommonController.Check_for_Vehicle(req.body);
            // let ValidityStatus = await DriverController.Common_Validate_Vehicle_Number_Already_Exist(req.body);
            let Result = await DriverController.Edit_Driver_Vehicle(req.body, AdminData, DriverData, VehicleData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Driver_Zone = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null && req.body.ZoneID != null
            && req.body.DriverID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let OldZoneData = await CommonController.Check_for_Zone(DriverData);
            let NewZoneData = await CommonController.Check_for_Zone(req.body);
            let Result = await DriverController.Edit_Driver_Zone(req.body, AdminData, DriverData, CountryData, CityData, OldZoneData, NewZoneData);
            res.json(Result);
            if (OldZoneData.Service_Type == 1 && NewZoneData.Service_Type == 2) {
                let WindowFillingFunctionality = await CronController.Create_Date_Windows();
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_Driver = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.Inactivate_Driver(req.body, AdminData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_Driver = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.Activate_Driver(req.body, AdminData, DriverData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Driver_Trips = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.List_All_Trips(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Driver_Order_History = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DriverID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DriverData = await CommonController.Check_Only_Driver(req.body);
            let Result = await DriverController.Driver_Order_History(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Pending_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Pending_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Approve_Product = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.Product_Approve != null && isFinite(req.body.Product_Approve)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Approve_Product(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Approve_Branch_Product = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Approve != null && isFinite(req.body.Approve)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Approve_Branch_Product(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Approved_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Approved_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Category_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.SubCategoryID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Category_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Declined_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Declined_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Blocked_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Blocked_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Suggest_Custom_Notification_SNo = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Suggest_Custom_Notification_SNo(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Suggest_Help_SNo = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Suggest_Help_SNo(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Help_SNo_Available = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Help_SNo(req.body);
            res.json({ success: true, extras: { Status: ValidateData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Help_Data = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Help_Data(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Help = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.HelpDataID != null && req.body.HelpDataID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Help(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Help = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.HelpDataID != null && req.body.HelpDataID != ''
            && req.body.Title != null && req.body.Title != ''
            && req.body.Description != null && req.body.Description != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Help_SNo(req.body);
            let Result = await AdminController.Edit_Help(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Create_Help = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Title != null && req.body.Title != ''
            && req.body.Description != null && req.body.Description != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Help_SNo(req.body);
            let Result = await AdminController.Create_Help(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Update_Referal_Price_Data = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.First_Time_Login != null && req.body.First_Time_Purchase != ''
            && isFinite(req.body.First_Time_Login) && isFinite(req.body.First_Time_Purchase)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Update_Referal_Price_Data(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_Referal_Price_Data = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Referal_Price_Data(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


AdminMediator.List_All_Buyers_Cancelled_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isBoolean(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await AdminController.List_All_Buyers_Cancelled_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Canceled_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            // && req.body.BranchID != null //&& req.body.BranchID != ''
            && req.body.CityID != null //&& req.body.CityID != ''
            && req.body.ZoneID != null //&& req.body.CityID != ''
            && req.body.Cancel_Status != null && isFinite(req.body.Cancel_Status) //1,2,3,4
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.Whether_Cancellation_Refund_Prodessed != null && isBoolean(req.body.Whether_Cancellation_Refund_Prodessed)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            if (Boolify(req.body.Whether_Date_Filter)) {
                if (req.body.Start_Date != null && req.body.Start_Date != ''
                    && req.body.End_Date != null && req.body.End_Date != ''
                ) {
                    let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            }
            let AdminData = await CommonController.Check_for_Admin(req.body);
            // if (req.body.BranchID != "") {
            //     let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            // }
            if (req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body);
            }
            if (req.body.ZoneID != "") {
                let ZoneData = await CommonController.Check_for_Zone(req.body);
            }
            let Result = await AdminController.List_Canceled_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null //&& req.body.BranchID != ''
            && req.body.CityID != null //&& req.body.CityID != ''
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)//true - Completed -3 // false - on going - not 3
        ) {
            if (Boolify(req.body.Whether_Date_Filter)) {
                if (req.body.Start_Date != null && req.body.Start_Date != ''
                    && req.body.End_Date != null && req.body.End_Date != ''
                ) {
                    let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
                } else {
                    throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
                }
            }
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.BranchID != "") {
                let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            }
            if (req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body);
            }
            let Result = await AdminController.List_All_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Cancel_Order_Refund = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body.Message != null //&& req.body.Message != ""
            && req.body.Cancellation_Amount != null && isFinite(req.body.Cancellation_Amount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Cancel_Calculation = await AdminController.Cancel_Order_Refund_Calculation(req.body)
            let Result = await AdminController.Cancel_Order_Refund(req.body, Cancel_Calculation);
            res.json(Result);
            let SendNotification = await FcmController.SendNotification_Cancel_Order_Refund(values, Cancel_Calculation)
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Cancel_Order_Refund_Calculation = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body.Cancellation_Amount != null && isFinite(req.body.Cancellation_Amount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Cancel_Order_Refund_Calculation(req.body);
            res.json({ success: true, extras: { Data: Result } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Cancel_Order_Notify_To_Branch = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Cancel_Order_Notify_To_Branch(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_And_Resend_Messages_for_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body._id != null && req.body._id != ""
            && req.body.Message != null && req.body.Message != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Edit_And_Resend_Messages_for_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Resend_Message_for_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body._id != null && req.body._id != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Resend_Message_for_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Messages_for_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body.List_Type != null && isFinite(req.body.List_Type) // 1- All, 2- Resnd list, 3- Edit List
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Messages_for_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Cancel_Order_Return_Initiate = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let OrderData = await CommonController.Check_for_Order(req.body);
            if (OrderData.Cancel_Status != 0) {

                if (OrderData.Order_Status == 3) { // Order got delivered/completed
                    // console.log(1)
                    let Result = await AdminController.Cancel_Order_Return_Initiate(req.body);
                    res.json(Result);
                    // console.log(OrderData.Order_Status)
                    // assign new Driver and continue the process
                    let Return_New = await AdminController.Order_Return_From_Buyer(OrderData);
                    let SendNotification = await NotificationController.SendNotification_Cancel_Order_Return_Initiate(OrderData)
                    let SendNotification1 = await FcmController.SendNotification_Cancel_Order_Return_Initiate(OrderData)
                } else if (OrderData.Order_Status == 11) { // return already initiated
                    // console.log(OrderData.Order_Status)
                    throw { success: false, extras: { msg: ApiMessages.ORDER_ALREADY_CANCELLED_AND_RETURN_STARTED } };
                } else if (OrderData.Order_Status >= 8 && OrderData.Order_Status <= 10) { // Order still with Driver
                    //Cancel the driver current trip and assign new trip and return to branch with order
                    let Result = await AdminController.Cancel_Order_Return_Initiate(req.body);
                    res.json(Result);
                    // console.log(OrderData.Order_Status)
                    let Return_Continue = await AdminController.Order_Return_From_Driver(OrderData);
                    let SendNotification = await NotificationController.SendNotification_Cancel_Order_Return_Initiate(OrderData)
                } else { // order not picked up from shop
                    throw { success: false, extras: { msg: ApiMessages.ORDER_CAN_NOT_BE_RETURN_BEFORE_PICKUP } };
                }
            } else {
                throw { success: false, extras: { msg: ApiMessages.ORDER_NOT_CANCELLED } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Accept_Reject_Cancelled_Orders_By_Buyers = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ""
            && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
            && req.body.Message != null && req.body.Message != ""
        ) {
            if (parseInt(req.body.Whether_Cancellation_Prodessed) == 1 || parseInt(req.body.Whether_Cancellation_Prodessed) == 2) {
                let AdminData = await CommonController.Check_for_Admin(req.body);
                let Result = await AdminController.Accept_Reject_Cancelled_Orders_By_Buyers(req.body);
                res.json(Result);
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_INPUT } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Cancelled_Orders_By_Buyers = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.Start_Date != null // && req.body.Start_Date != ''
            && req.body.End_Date != null // && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (Boolify(req.body.Whether_Date_Filter)) {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            }
            let Result = await AdminController.List_All_Cancelled_Orders_By_Buyers(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Cancelled_Orders_By_Drivers = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.Start_Date != null // && req.body.Start_Date != ''
            && req.body.End_Date != null // && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (Boolify(req.body.Whether_Date_Filter)) {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            }
            let Result = await AdminController.List_All_Cancelled_Orders_By_Drivers(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Cancelled_Orders_By_Branches = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.Start_Date != null // && req.body.Start_Date != ''
            && req.body.End_Date != null // && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (Boolify(req.body.Whether_Date_Filter)) {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            }
            let Result = await AdminController.List_All_Cancelled_Orders_By_Branches(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Cancelled_Orders_By_Admin = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Date_Filter != null && isBoolean(req.body.Whether_Date_Filter)
            && req.body.Start_Date != null // && req.body.Start_Date != ''
            && req.body.End_Date != null // && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (Boolify(req.body.Whether_Date_Filter)) {
                let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            }
            let Result = await AdminController.List_All_Cancelled_Orders_By_Admin(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

// AdminMediator.List_All_Cancelled_Orders_By_Buyers = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//             && req.body.Whether_Date_Filter != null && req.body.Whether_Date_Filter != ''
//             && req.body.Start_Date != null // && req.body.Start_Date != ''
//             && req.body.End_Date != null // && req.body.End_Date != ''
//             && req.body.skip != null && isFinite(req.body.skip)
//             && req.body.limit != null && isFinite(req.body.limit)
//             && req.body.Whether_Cancellation_Prodessed != null && isFinite(req.body.Whether_Cancellation_Prodessed)
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
//             let Result = await AdminController.List_All_Cancelled_Orders_By_Buyers(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(error);
//         }
//     }
// }

AdminMediator.List_All_Buyers_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await AdminController.List_All_Buyers_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Buyer_Cancelled_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BuyerID != null && req.body.BuyerID != ''
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isBoolean(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await AdminController.List_Buyer_Cancelled_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Buyer_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BuyerID != null && req.body.BuyerID != ''
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation_Normal(req.body);
            let Result = await AdminController.List_Buyer_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Branch_Cancelled_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isBoolean(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await AdminController.List_Branch_Cancelled_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Branch_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await AdminController.List_Branch_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Branch_Category_Cancelled_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.CategoryID != null && req.body.CategoryID != ''
            //&& req.body.SubCategoryID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Whether_Cancellation_Prodessed != null && isBoolean(req.body.Whether_Cancellation_Prodessed)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            let Result = await AdminController.List_All_Branch_Category_Cancelled_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Branch_Category_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.CategoryID != null && req.body.CategoryID != ''
            //&& req.body.SubCategoryID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BrabchData = await CommonController.Check_For_Store_BranchID(req.body);
            let Result = await AdminController.List_All_Branch_Category_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Buyers_With_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Orders_Count != null && isFinite(req.body.Orders_Count)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Buyers_With_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Buyers_Details = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.CityID != null && req.body.CityID != "") {
                let CityData = await CommonController.Check_for_City(req.body)
            }
            if (req.body.ZoneID != null && req.body.ZoneID != "") {
                let ZoneData = await CommonController.Check_for_Zone(req.body)
            }
            if (req.body.CountryID != null && req.body.CountryID != "") {
                let CountryData = await CommonController.Check_for_Country(req.body)
            }
            let Result = await AdminController.List_All_Buyers_Details(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_Buyer_Details = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BuyerID != null && req.body.BuyerID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let BuyerData = await CommonController.Check_For_Buyer(req.body);
            res.json({ success: true, extras: { Data: BuyerData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_Single_Orders = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Single_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Provide_Refund_For_Order = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ''
            && req.body.Refund_Amount != null && isFinite(req.body.Refund_Amount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Provide_Refund_For_Order(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_Amount_Company_Wallet = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Amount != null && isFinite(req.body.Amount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_Amount_Company_Wallet(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_Company_Wallet = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Company_Wallet(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_Company_Wallet_Log = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_Company_Wallet_Log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Activate_Inactivate_Fee_Details_For_Zone = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ZoneID != null && req.body.ZoneID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ZoneData = await CommonController.Check_for_Zone(req.body);
            let ZoneFeeData = await CommonController.Check_for_Zone_Fee(req.body);
            let Result = await AdminController.Activate_Inactivate_Fee_Details_For_Zone(req.body, ZoneFeeData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Fee_Details_For_Zones = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ZoneID != null
            && req.body.Search != null
            && req.body.Status != null && isBoolean(req.body.Status)
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.ZoneID != '') {
                let ZoneData = await CommonController.Check_for_Zone(req.body);
            }
            let Result = await AdminController.List_All_Fee_Details_For_Zones(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Update_Fee_Details_For_Zones = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Delivery_Fee != null && isFinite(req.body.Delivery_Fee) && !isNaN(req.body.Delivery_Fee)
            && req.body.Packaging_Fee != null && isFinite(req.body.Packaging_Fee) && !isNaN(req.body.Packaging_Fee)
            && req.body.Surge_Fee != null && isFinite(req.body.Surge_Fee) && !isNaN(req.body.Surge_Fee)
            && req.body.ZoneID_Array != null && typeof req.body.ZoneID_Array === "object"
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            if (req.body.ZoneID_Array.length > 0) {
                let ZoneData = [];
                for (const zone of req.body.ZoneID_Array) {
                    let Zone_Data = await CommonController.Check_for_Zone({ ZoneID: zone })
                    ZoneData.push(Zone_Data)
                }
                let Result = await AdminController.Update_Fee_Details_For_Zones(req.body, ZoneData);
                res.json(Result);
            } else {
                throw { success: false, extras: { msg: ApiMessages.PLEASE_SELECT_ATLEAST_ONE_ZONE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Activate_Inactivate_Taxes = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let TaxesData = await AdminController.Get_Taxes(req.body);
            let Result = await AdminController.Activate_Inactivate_Taxes(req.body, TaxesData.extras.Data);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Update_Taxes = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Bookstore_GST != null && isFinite(req.body.Bookstore_GST) && !isNaN(req.body.Bookstore_GST)
            && req.body.Delivery_GST != null && isFinite(req.body.Delivery_GST) && !isNaN(req.body.Delivery_GST)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Update_Taxes(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Get_Taxes = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Get_Taxes(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
// AdminMediator.Update_Delivery_Charges = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//             && req.body.Slab_Km != null && isFinite(req.body.Slab_Km) // first Km slab
//             && req.body.Slab_Amount != null && isFinite(req.body.Slab_Amount) // first slab Amount
//             && req.body.Per_Km_Amount != null && isFinite(req.body.Per_Km_Amount)
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let Result = await AdminController.Update_Delivery_Charges(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(error);
//         }
//     }
// }

// AdminMediator.Get_Delivery_Charges_Data = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let Result = await AdminController.Get_Delivery_Charges_Data(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         if (!res.headersSent) {
//             res.json(error);
//         }
//     }
// }

AdminMediator.Create_Banner = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ImageID != null && req.body.ImageID != ''
            && req.body.Banner_Name != null && req.body.Banner_Name != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body);
            let ValidateData = await CommonController.Check_Banner_SNo(req.body);
            let Result = await AdminController.Create_Banner(req.body, ImageData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Banner = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BannerID != null && req.body.BannerID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Banner(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Banner = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BannerID != null && req.body.BannerID != ''
            && req.body.ImageID != null && req.body.ImageID != ''
            && req.body.Banner_Name != null && req.body.Banner_Name != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body);
            let validateData = await CommonController.Check_Banner_SNo(req.body);
            let Result = await AdminController.Edit_Banner(req.body, ImageData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Banner = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Banner(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_Quote = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Quote != null && req.body.Quote != ''
            && req.body.Quote_Author != null && req.body.Quote_Author != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_Quote(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


AdminMediator.List_All_Quotes = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Quotes(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Quote = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.QuoteID != null && req.body.QuoteID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Quote(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Quote = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.QuoteID != null && req.body.QuoteID != ''
            && req.body.Quote != null && req.body.Quote != ''
            && req.body.Quote_Author != null && req.body.Quote_Author != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Edit_Quote(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Suggest_Category_SNo = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await CommonController.Suggest_Category_SNo(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Check_Category_SNo_Available = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Category_SNo(req.body);
            res.json({ success: true, extras: { Status: ValidateData } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_Category = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Category != null && req.body.Category != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Category_SNo(req.body);
            let Result = await AdminController.Add_Category(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Add_SubCategory = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
            && req.body.SubCategory != null && req.body.SubCategory != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Add_SubCategory(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Active_Inactive_Category = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Active_Inactive_Category(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Remove_SubCategory = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
            && req.body.SubCategoryID != null && req.body.SubCategoryID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Remove_SubCategory(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_Category = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
            && req.body.Category != null && req.body.Category != ''
            && req.body.SNo != null && isFinite(req.body.SNo)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData = await CommonController.Check_Category_SNo(req.body);
            let Result = await AdminController.Edit_Category(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Edit_SubCategory = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
            && req.body.SubCategoryID != null && req.body.SubCategoryID != ''
            && req.body.SubCategory != null && req.body.SubCategory != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Edit_SubCategory(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Category_Sold = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null && req.body.BranchID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Category_Sold(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Branch_Product_Reorder = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ReorderID != null && req.body.ReorderID != ''
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Quantity_Intake != null && isFinite(req.body.Quantity_Intake)
            && req.body.ProductID != null && req.body.ProductID
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ProductData = await CommonController.Check_For_Product(req.body)
            let Result = await AdminController.Branch_Product_Reorder(req.body, ProductData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

// AdminMediator.Update_Store_Product = async (req, res) => {
//     try {
//         if (
//             req.body.AdminID != null && req.body.SessionID != null
//             && req.body.BranchID != null && req.body.BranchID != ''
//             && req.body.skip != null && isFinite(req.body.skip)
//             && req.body.limit != null && isFinite(req.body.limit)
//         ) {
//             let AdminData = await CommonController.Check_for_Admin(req.body);
//             let Result = await StoreController.Update_Store_Product(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         console.log(error)
//         if (!res.headersSent) {
//             res.json(error);
//         }
//     }
// }

AdminMediator.List_All_Product_Reorder_Log = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Product_Reorder_Log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Products_For_Reorder = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.ZoneID != null && req.body.CategoryID != null
            && req.body.SubCategoryID != null && req.body.BranchID != null
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidateData
            if (req.body.CountryID != "") {
                ValidateData = await CommonController.Check_for_Country(req.body)
            }
            if (req.body.CityID != "") {
                ValidateData = await CommonController.Check_for_City(req.body)
            }
            if (req.body.ZoneID != "") {
                ValidateData = await CommonController.Check_for_Zone(req.body)
            }
            if (req.body.BranchID != "") {
                ValidateData = await CommonController.Check_for_Branch(req.body)
            }
            // if(req.body.CategoryID != ""){
            //     ValidateData = await CommonController.Check_for_Category(req.body)
            // }
            // if(req.body.SubCategoryID != ""){
            //     ValidateData = await CommonController.Check_for_SubCategory(req.body)
            // }
            let Result = await AdminController.List_All_Products_For_Reorder(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_Category = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            & req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_Category(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.List_All_SubCategory = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CategoryID != null && req.body.CategoryID != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_All_SubCategory(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

AdminMediator.Validate_Discount_Code = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Discount_Code != null && req.body.Discount_Code != ''
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.Validate_Discount_Code(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Create_Discount = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Discount_Code != null && req.body.Discount_Code != ''
            && req.body.Discount_Title != null && req.body.Discount_Title != ''
            && req.body.Minimum_Invoice_Amount != null && isFinite(req.body.Minimum_Invoice_Amount)
            && req.body.Discount_Percentage != null && isFinite(req.body.Discount_Percentage)
            && req.body.Max_Discount_Amount != null && isFinite(req.body.Max_Discount_Amount)
            && req.body.Max_Discount_For_User != null && isFinite(req.body.Max_Discount_For_User)
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.Whether_Referral_Discount != null && isBoolean(req.body.Whether_Referral_Discount)
            && req.body.Whether_Discount_Age_Filter != null && isBoolean(req.body.Whether_Discount_Age_Filter)
            && req.body.Whether_Order_Filter != null && isBoolean(req.body.Whether_Order_Filter)
            && req.body.Whether_Inactive_Days_Filter != null && isBoolean(req.body.Whether_Inactive_Days_Filter)
            && req.body.Whether_Customer_Target_Discount != null && isBoolean(req.body.Whether_Customer_Target_Discount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ValidityStatus = await AdminController.Validate_Discount_Code(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Start_End_Time(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Default_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Age_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Order_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Inactive_Days_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Whether_Customer_Target_Discount(req.body);
            let Result = await AdminController.Create_Discount(req.body, AdminData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Discounts = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.sortOptions != null && typeof (req.body.sortOptions) == 'object'
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let Result = await AdminController.List_All_Discounts(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Update_Discount = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.DiscountID != null
            && req.body.Discount_Code != null && req.body.Discount_Code != ''
            && req.body.Discount_Title != null && req.body.Discount_Title != ''
            && req.body.Minimum_Invoice_Amount != null && isFinite(req.body.Minimum_Invoice_Amount)
            && req.body.Discount_Percentage != null && isFinite(req.body.Discount_Percentage)
            && req.body.Max_Discount_Amount != null && isFinite(req.body.Max_Discount_Amount)
            && req.body.Max_Discount_For_User != null && isFinite(req.body.Max_Discount_For_User)
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.Whether_Referral_Discount != null && isBoolean(req.body.Whether_Referral_Discount)
            && req.body.Whether_Discount_Age_Filter != null && isBoolean(req.body.Whether_Discount_Age_Filter)
            && req.body.Whether_Order_Filter != null && isBoolean(req.body.Whether_Order_Filter)
            && req.body.Whether_Inactive_Days_Filter != null && isBoolean(req.body.Whether_Inactive_Days_Filter)
            && req.body.Whether_Customer_Target_Discount != null && isBoolean(req.body.Whether_Customer_Target_Discount)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let DiscountData = await CommonController.Check_for_Discount(req.body);
            let ValidityStatus = await AdminController.Validate_Discount_Code(req.body);
            ValidityStatus = await AdminController.Update_Discount_Validate_Start_End_Time(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Default_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Age_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Order_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Inactive_Days_Filter_Params(req.body);
            ValidityStatus = await AdminController.Create_Discount_Validate_Whether_Customer_Target_Discount(req.body);
            let Result = await AdminController.Update_Discount(req.body, AdminData, DiscountData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_Discount = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.DiscountID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let DiscountData = await CommonController.Check_for_Discount(req.body);
            let Result = await AdminController.Inactivate_Discount(req.body, AdminData, DiscountData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Activate_Discount = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.DiscountID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let DiscountData = await CommonController.Check_for_Discount(req.body);
            let Result = await AdminController.Activate_Discount(req.body, AdminData, DiscountData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Validate_App_Automated_Offer_Discount_Day = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Discount_Day_Number != null && isFinite(req.body.Discount_Day_Number)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.Validate_App_Automated_Offer_Discount_Day(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_App_Automated_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.Discount_Day_Number != null && isFinite(req.body.Discount_Day_Number)
            && req.body.Discount_Description != null && req.body.Discount_Description != ''
            && req.body.Discount_Percentage != null && isFinite(req.body.Discount_Percentage)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let ValidityStatus = await AdminController.Validate_App_Automated_Offer_Discount_Day(req.body);
            let Result = await AdminController.Add_App_Automated_Offer(req.body, AdminData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Automated_Offers = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.sortOptions != null && typeof (req.body.sortOptions) == 'object'
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let Result = await AdminController.List_All_Automated_Offers(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } }
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Update_App_Automated_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.CountryID != null && req.body.CityID != null
            && req.body.AutomatedOfferID != null
            && req.body.Discount_Description != null && req.body.Discount_Description != ''
            && req.body.Discount_Percentage != null && isFinite(req.body.Discount_Percentage)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let CountryData = await CommonController.Check_for_Country(req.body);
            let CityData = await CommonController.Check_for_City(req.body);
            let OfferData = await CommonController.Check_for_Automated_Offer(req.body);
            let Result = await AdminController.Update_App_Automated_Offer(req.body, AdminData, OfferData, CountryData, CityData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Validate_Special_Offer_Number_Already_Exist = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SpecialOfferID != null
            && req.body.SpecialOfferNumber != null && isFinite(req.body.SpecialOfferNumber) && !isNaN(req.body.SpecialOfferNumber)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Validate_Special_Offer_Number_Already_Exist(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Add_Special_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.DiscountID != null
            && req.body.ImageID != null
            && req.body.SpecialOfferName != null && req.body.SpecialOfferName != ''
            && req.body.SpecialOfferNumber != null && isFinite(req.body.SpecialOfferNumber) && !isNaN(req.body.SpecialOfferNumber)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let DiscountData = await CommonController.Check_for_Discount(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body);
            let ValidityStatus = await AdminController.Validate_Special_Offer_Number_Already_Exist(req.body);
            let Result = await AdminController.Add_Special_Offer(req.body, ImageData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_All_Special_Offers_with_Filter = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Whether_Status_Filter != null && isBoolean(req.body.Whether_Status_Filter)
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
            && req.body.sortOptions != null && typeof (req.body.sortOptions) == 'object'
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let ValidityStatus = await AdminController.Common_Whether_Status_Filter(req.body);
            let Result = await AdminController.List_All_Special_Offers_with_Filter(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}


AdminMediator.Activate_Special_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SpecialOfferID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let SpecialOfferData = await CommonController.Check_for_Special_Offer(req.body);
            let Result = await AdminController.Activate_Special_Offer(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Inactivate_Special_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SpecialOfferID != null
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let SpecialOfferData = await CommonController.Check_for_Special_Offer(req.body);
            let Result = await AdminController.Inactivate_Special_Offer(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Update_Special_Offer = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.SpecialOfferID != null
            && req.body.ImageID != null
            && req.body.SpecialOfferName != null && req.body.SpecialOfferName != ''
            && req.body.SpecialOfferNumber != null && isFinite(req.body.SpecialOfferNumber) && !isNaN(req.body.SpecialOfferNumber)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let SpecialOfferData = await CommonController.Check_for_Special_Offer(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body);
            let ValidityStatus = await AdminController.Validate_Special_Offer_Number_Already_Exist(req.body);
            let Result = await AdminController.Update_Special_Offer(req.body, ImageData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_Branch_Stack_Log = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
            && req.body.sortOptions != null && typeof (req.body.sortOptions) == 'object'
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_Branch_Stack_Log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Update_Stack = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.StackID != null && req.body.StackID != ''
            && req.body.ETA_Description != null && req.body.ETA_Description != ''
            && req.body.ETA != null && isFinite(req.body.ETA)
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let StackData = await CommonController.Check_for_Stack(req.body);
            let Result = await AdminController.Update_Stack(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.List_Branch_Products = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.BranchID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
            && req.body.sortOptions != null && typeof (req.body.sortOptions) == 'object'
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.List_Branch_Products(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

AdminMediator.Edit_Admin_Users = async (req, res) => {
    try {
        if (
            req.body.AdminID != null && req.body.SessionID != null
            && req.body.Name != null && req.body.Name != ""
            && req.body.XAdminID != null && req.body.XAdminID != ""
        ) {
            let AdminData = await CommonController.Check_for_Admin(req.body);
            let Result = await AdminController.Edit_Admin_Users(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

export default AdminMediator;