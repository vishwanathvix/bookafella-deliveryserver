let UserMediator = function () { };

import UserController from "../controllers/UserController";
import CommonController from "../controllers/CommonController";
import { isBoolean, Boolify } from "node-boolify";
import ApiMessages from "../models/ApiMessages";
import FirebaseController from "../controllers/FirebaseController";
import Admin_Notification_Log from "../models/Admin_Notification_Log";
import NotificationController from "../controllers/NotificationController";

UserMediator.List_All_Buyer_Notifications = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Buyer_Notifications(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Add_Write_And_Earn = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Unique_ProductID != null
            && req.body.Description != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Add_Write_And_Earn(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Validate_User_Credentials = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.BuyerID != null && req.body.BuyerID != ''
            && req.body.accessToken != null && req.body.accessToken != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let FirebaseUserData = await FirebaseController.Check_for_Firebase_User(req.body.BuyerID);
            let FirebaseUserTokenData = await FirebaseController.Check_for_Firebase_ID_Token(req.body.BuyerID, req.body.accessToken);
            let Result = await UserController.Create_or_Generate_Buyer_Session(req.body, FirebaseUserData, FirebaseUserTokenData, DeviceData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

///////////////////////////////////

UserMediator.Search_All_Products_For_Buyer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Search != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CurrentLocationData = await CommonController.Get_Buyer_Current_Location(req.body);
            let Result = await UserController.List_All_Products_Search(req.body, CurrentLocationData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Remove_Stack = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.StackID != null && req.body.StackID != ""
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let StackProcessing = await CommonController.Check_For_Buyer_Cart_And_Stack_Processing_For_OutofStock(req.body, CartData.extras.Data.Cart_Information);
            let Result = await UserController.Remove_Stack(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Request_Stack_ETA = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.StackID != null && req.body.StackID != ""
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let StackProcessing = await CommonController.Check_For_Buyer_Cart_And_Stack_Processing_For_OutofStock(req.body, CartData.extras.Data.Cart_Information);
            let Result = await UserController.Request_Stack_ETA(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.List_User_Stack_Items = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let StackProcessing = await CommonController.Check_For_Buyer_Cart_And_Stack_Processing_For_OutofStock(req.body, CartData.extras.Data.Cart_Information);
            let Result = await UserController.List_User_Stack_Items(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Remove_Product_In_Wishlist = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Remove_Product_In_Wishlist(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.List_Product_In_Wishlist = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_Product_In_Wishlist(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Add_Product_To_Wishlist = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Add_Product_To_Wishlist(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Apply_Discount_Offer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Discount_Code != null
            && req.body.lat != null && isFinite(req.body.lat)
            && req.body.lng != null && isFinite(req.body.lng)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            // let PriceQuoteData = await CommonController.Check_for_User_Price_Quote(req.body);
            // let ValidityStatus = await OrderController.Pricing_Quote_Validate_Country_City_State_Active(PriceQuoteData);
            let ZoneData = await UserController.Apply_Discount_Offer_Fetch_Zone_Data(req.body);
            let DiscountData = await UserController.Apply_Discount_Offer_Validate_Discount_Code(req.body, UserData, ZoneData);
            //let Price_Data_Array = await OrderController.Apply_Discount_Offer_Fetch_Price_Data_Array(req.body, PriceQuoteData);
            //Price_Data_Array = await OrderController.Apply_Discount_Offer_Validate_Pricing_Array(req.body, DiscountData, Price_Data_Array);
            let Result = await UserController.Apply_Discount_Offer(req.body, UserData, DiscountData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.List_All_Available_Discounts = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.lat != null && isFinite(req.body.lat)
            && req.body.lng != null && isFinite(req.body.lng)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ZoneData = await UserController.List_All_Available_Discounts_Fetch_Zone_Data(req.body);
            let Result = await UserController.List_All_Available_Discounts(req.body, BuyerData, ZoneData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

UserMediator.Get_Similar_Products = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null
            && req.body.Show_Current_Product != null
            && isBoolean(req.body.Show_Current_Product)
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ProductData = await CommonController.Check_For_Product(req.body);
            let CurrentLocationData = await CommonController.Get_Buyer_Current_Location(req.body);
            let Result = await UserController.Get_Similar_Products(req.body, CurrentLocationData, ProductData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Categories = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Categories(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Get_Quote = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Get_Quote(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Banner = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Banner(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Get_Buyer_Recent_Orders = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Get_Buyer_Recent_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Update_Buyer_Profile = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Buyer_Name != null && req.body.Buyer_Name != ''
            && req.body.Buyer_Email != null && req.body.Buyer_Email != ''
            && req.body.DOB != null && req.body.DOB != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ValidityStatus = await CommonController.Common_Date_Validation(req.body.DOB);
            let Email_Validation = await CommonController.Common_Email_Validation(req.body.Buyer_Email);
            let Result = await UserController.Update_Buyer_Profile(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Update_Buyer_Image = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ImageID != null && req.body.ImageID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ImageData = await CommonController.Check_for_Image(req.body)
            let Result = await UserController.Update_Buyer_Image(req.body, ImageData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_Product_Rating = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_Product_Rating(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Buyer_Rating = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Buyer_Rating(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Product_Rating_On_Order = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.OrderID != null && req.body.OrderID != ''
            && req.body.Rating != null && isFinite(req.body.Rating) // 0-5
            && req.body.Rating_Description != null && req.body.Rating_Description != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Product_Rating_On_Order(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Help_Data = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Help_Data(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Buyer_Wallet_log = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Buyer_Wallet_log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Home_Screen_Details = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let StackProcessing = await CommonController.Check_For_Buyer_Cart_And_Stack_Processing_For_OutofStock(req.body, CartData.extras.Data.Cart_Information);
            let Result = await UserController.Home_Screen_Details(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

// UserMediator.Check_Cancellation_Refund_Amount = async (req, res) => {
//     try {
//         if (
//             req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
//             && req.body.OrderID != null && req.body.OrderID != ''
//         ) {
//             let DeviceData = await CommonController.Check_for_Api_Key(req.body);
//             let UserData = await CommonController.Check_For_Buyer_Session(req.body);
//             let OrderData = await CommonController.Check_for_Order(req.body);
//             let Result = await UserController.Check_Cancellation_Refund_Amount(req.body);
//             res.json(Result);
//         } else {
//             throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
//         }
//     } catch (error) {
//         
//         if (!res.headersSent) {
//             res.json(error);
//         }
//     }
// }


UserMediator.Check_Cancellation_Timer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let OrderData = await CommonController.Check_for_Order(req.body);
            let Result = await UserController.Check_Cancellation_Validatity(req.body);
            // console.log(Result)
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Cancel_Single_Order = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ''
            && req.body.Cancel_Reason != null && req.body.Cancel_Reason != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let OrderData = await CommonController.Check_for_Order(req.body);
            let CancellationData = await UserController.Check_Cancellation_Validatity(req.body);
            let Result = await UserController.Cancel_Single_Order(req.body, OrderData);
            res.json(Result);
            let SendNotification = await NotificationController.SendNotification_Cancel_Single_Order( req.body, OrderData, UserData)
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Get_Buyer_Single_Order = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.OrderID != null && req.body.OrderID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Get_Buyer_Single_Order(req.body, 1);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_Buyer_Cancelled_Orders = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await UserController.List_Buyer_Cancelled_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_Buyer_Orders = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Start_Date != null && req.body.Start_Date != ''
            && req.body.End_Date != null && req.body.End_Date != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
            && req.body.Status != null && isBoolean(req.body.Status)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let ValidityStatus = await CommonController.Common_Start_Date_End_Date_Validation(req.body);
            let Result = await UserController.List_Buyer_Orders(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Payment_for_Buyer_Order = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null
            && req.body.BranchID != null && req.body.BranchID != ""
            && req.body.Payment_Mode != null && isFinite(req.body.Payment_Mode) && !isNaN(req.body.Payment_Mode)
            // Payment_Mode == 1 - Online Mode, == 2 - COD
        ) {
            if (req.body.Payment_Mode == 1 || req.body.Payment_Mode == 2) {
                let DeviceData = await CommonController.Check_for_Api_Key(req.body);
                let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
                let Branch_Status = await UserController.Check_Store_Available(req.body.BranchID);
                // if (Boolify(Branch_Status)) {
                let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
                let PickupDropValidation = await UserController.Validate_Pickup_Drop_Location(req.body)
                let OrderData = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
                // console.log(OrderData)
                let OrderIDArray = await UserController.Save_Buyer_Order(req.body, OrderData.extras.Data);
                let Result = await UserController.Payment_for_Buyer_Order(req.body, OrderIDArray, OrderData.extras.Data, BuyerData);
                res.json(Result);
                // } else {
                //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
                // }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_PAYMENT_MODE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Payment_for_Buyer_ReOrder = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null && req.body.OrderID != null
            && req.body.Payment_Mode != null && isFinite(req.body.Payment_Mode) && !isNaN(req.body.Payment_Mode)
            // Payment_Mode == 1 - Online Mode, == 2 - COD
        ) {
            if (req.body.Payment_Mode == 1 || req.body.Payment_Mode == 2) {
                let DeviceData = await CommonController.Check_for_Api_Key(req.body);
                let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
                let OrderData1 = await CommonController.Check_for_Order(req.body)
                let Branch_Status = await UserController.Check_Store_Available(OrderData1.Branch_Information.BranchID);
                // if (Boolify(Branch_Status)) {
                req.body.BranchID = OrderData1.Branch_Information.BranchID
                let CartData = await UserController.List_All_Products_In_Buyer_Order(req.body, OrderData1);
                let PickupDropValidation = await UserController.Validate_Pickup_Drop_Location(req.body)
                let OrderData = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
                let OrderIDArray = await UserController.Save_Buyer_Order(req.body, OrderData.extras.Data);
                let Result = await UserController.Payment_for_Buyer_Order(req.body, OrderIDArray, OrderData.extras.Data, BuyerData);
                res.json(Result);
                // } else {
                //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
                // }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_PAYMENT_MODE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Payment_for_Buyer_Direct_Order = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Product_Quantity != null && isFinite(req.body.Product_Quantity)
            && req.body.AddressID != null
            && req.body.Payment_Mode != null && isFinite(req.body.Payment_Mode) && !isNaN(req.body.Payment_Mode)
            // Payment_Mode == 1 - Online Mode, == 2 - COD
        ) {
            if (req.body.Payment_Mode == 1 || req.body.Payment_Mode == 2) {
                let DeviceData = await CommonController.Check_for_Api_Key(req.body);
                let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
                let Branch_Status = await UserController.Check_Store_Available(req.body.BranchID);
                // if (Boolify(Branch_Status)) {
                let Buyer_Data = {
                    Cart_Information: [{
                        ProductID: req.body.ProductID,
                        Product_Quantity: req.body.Product_Quantity,
                        BranchID: req.body.BranchID
                    }]
                }
                let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, Buyer_Data);
                let PickupDropValidation = await UserController.Validate_Pickup_Drop_Location(req.body)
                let OrderData = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
                // console.log(OrderData)
                let OrderIDArray = await UserController.Save_Buyer_Order(req.body, OrderData.extras.Data);
                let Result = await UserController.Payment_for_Buyer_Order(req.body, OrderIDArray, OrderData.extras.Data, BuyerData);
                res.json(Result);
                // } else {
                //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
                // }
            } else {
                throw { success: false, extras: { msg: ApiMessages.INVALID_PAYMENT_MODE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Buyer_Order_Summary = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null
            && req.body.BranchID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Branch_Status = await UserController.Check_Store_Available(req.body.BranchID);
            // if (Boolify(Branch_Status)) {
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let Result = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
            res.json(Result);
            // } else {
            //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
            // }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


UserMediator.Buyer_Pre_Order_Summary = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            let Result = await UserController.Buyer_Pre_Order_Array_Summary(req.body, CartData.extras.Data);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Buyer_Direct_Order_Summary = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Product_Quantity != null && isFinite(req.body.Product_Quantity)
            && req.body.AddressID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Branch_Status = await UserController.Check_Store_Available(req.body.BranchID);
            // if (Boolify(Branch_Status)) {
            let Buyer_Data = {
                Cart_Information: [{
                    ProductID: req.body.ProductID,
                    Product_Quantity: req.body.Product_Quantity,
                    BranchID: req.body.BranchID
                }]
            }
            let CartData = await UserController.List_All_Products_In_Buyer_Cart(req.body, Buyer_Data);
            let Result = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
            res.json(Result);
            // } else {
            //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
            // }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Buyer_ReOrder_Summary = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null && req.body.OrderID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let OrderData = await CommonController.Check_for_Order(req.body)
            let Branch_Status = await UserController.Check_Store_Available(OrderData.Branch_Information.BranchID);
            // if (Boolify(Branch_Status)) {
            // console.log(OrderData)
            req.body.BranchID = OrderData.Branch_Information.BranchID
            let CartData = await UserController.List_All_Products_In_Buyer_Order(req.body, OrderData);
            let Result = await UserController.Buyer_Order_Array_Summary(req.body, CartData.extras.Data);
            res.json(Result);
            // } else {
            //     throw { success: false, extras: { msg: ApiMessages.BRANCH_NOT_ACTIVE_FOR_ORDERS } };
            // }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Products_In_Buyer_Cart = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_All_Products_In_Buyer_Cart(req.body, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Remove_All_Product_From_Cart = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Remove_All_Product_From_Cart(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Remove_Product_From_Cart = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Remove_Product_From_Cart(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Add_Product_To_Cart = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.BranchID != null && req.body.BranchID != ''
            && req.body.Add_Remove != null && isFinite(req.body.Add_Remove)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ProductData = await CommonController.Check_For_Product(req.body);
            let Result = await UserController.Add_Product_To_Cart(req.body, ProductData, BuyerData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Get_Single_Products_For_Buyer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Latitude != null && req.body.Latitude != ''
            && req.body.Longitude != null && req.body.Longitude != ''
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ResultX = await UserController.List_All_Products_For_BuyerX(req.body, BuyerData);
            let Result = await UserController.Get_Single_Products_For_Buyer(req.body, ResultX);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Rainning_Discount_Products_For_Buyer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Latitude != null && req.body.Latitude != ''
            && req.body.Longitude != null && req.body.Longitude != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ResultX = await UserController.List_All_Products_For_BuyerX(req.body, BuyerData);
            let Result = await UserController.List_All_Rainning_Discount_Products_For_Buyer(req.body, ResultX);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


UserMediator.Get_Single_Product = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.ProductID != null && req.body.ProductID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Single_Product_Data(req.body, req.body.ProductID);
            res.json({ success: true, extras: { Data: Result } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Products_Dynamic_Title = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.TitleID != null //&& req.body.TitleID != ""
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            if (req.body.TitleID != "") {
                let TitleData = await CommonController.Check_for_Title(req.body);
                if (Boolify(TitleData.Status)) {
                    let CurrentLocationData = await CommonController.Get_Buyer_Current_Location(req.body)
                    let Result = await UserController.List_All_Products_Dynamic_Title(req.body, CurrentLocationData, TitleData);
                    res.json(Result);
                } else {
                    throw { success: false, extras: { msg: ApiMessages.TITLE_NOT_ACTIVE } };
                }
            } else {
                let CurrentLocationData = await CommonController.Get_Buyer_Current_Location(req.body)
                let Result = await UserController.List_All_Products(req.body, CurrentLocationData);
                res.json(Result);
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_All_Products_Dynamic = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let CurrentLocationData = await CommonController.Get_Buyer_Current_Location(req.body)
            let Result = await UserController.List_All_Products_Dynamic(req.body, CurrentLocationData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}


UserMediator.List_All_Products_For_Buyer = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Latitude != null && req.body.Latitude != ''
            && req.body.Longitude != null && req.body.Longitude != ''
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let ResultX = await UserController.List_All_Products_For_BuyerX(req.body, BuyerData);
            let Result = await UserController.List_All_Products_For_Buyer(req.body, ResultX);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.List_Buyer_Address = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.List_Buyer_Address(req.body);
            res.json(Result);

        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Remove_Buyer_Address = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null && req.body.AddressID != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Remove_Buyer_Address(req.body);
            res.json(Result);

        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Edit_Buyer_Address = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.AddressID != null && req.body.AddressID != ''
            && req.body.Name != null && req.body.Name != ''
            && req.body.PhoneNumber != null && req.body.PhoneNumber != ''
            && req.body.Flat_No != null
            && req.body.Plot_No != null
            && req.body.Postal_Code != null && req.body.Postal_Code != ''
            && req.body.State != null
            && req.body.City != null
            && req.body.Land_Mark != null
            && req.body.Address_Type != null && req.body.Address_Type != ''
            && req.body.Address_Default != null && isBoolean(req.body.Address_Default)
            && req.body.Latitude != null && req.body.Latitude != ''
            && req.body.Longitude != null && req.body.Longitude != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Edit_Buyer_Address(req.body);
            res.json(Result);

        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Add_Buyer_Address = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Name != null && req.body.Name != ''
            && req.body.PhoneNumber != null && req.body.PhoneNumber != ''
            && req.body.Flat_No != null
            && req.body.Plot_No != null
            && req.body.Postal_Code != null && req.body.Postal_Code != ''
            && req.body.State != null
            && req.body.City != null
            && req.body.Land_Mark != null
            && req.body.Address_Type != null && req.body.Address_Type != ''
            && req.body.Address_Default != null && isBoolean(req.body.Address_Default)
            && req.body.Latitude != null && req.body.Latitude != ''
            && req.body.Longitude != null && req.body.Longitude != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Add_Buyer_Address(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Update_Buyer_Location = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Latitude != null && isFinite(req.body.Latitude)
            && req.body.Longitude != null && isFinite(req.body.Longitude)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let Result = await UserController.Update_Buyer_Location(req.body, UserData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Validate_Referral_Phone_Number = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.PhoneNumber != null && req.body.PhoneNumber != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let UserData = await CommonController.Check_For_Buyer_Session(req.body);
            let ReferralData = await UserController.Validate_Referral_Phone_Number(req.body, UserData);
            res.json({ success: true, extras: { Data: { Buyer_Name: ReferralData }, Status: "Referral Available" } });
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Buyer_Register = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null && req.body.BuyerID != null && req.body.SessionID != null
            && req.body.Buyer_Name != null && req.body.Buyer_Name != ''
            && req.body.Buyer_Email != null && req.body.Buyer_Email != ''
            && req.body.DOB != null && req.body.DOB != ''
            && req.body.Ref_PhoneNumber != null
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let BuyerData = await CommonController.Check_For_Buyer_Session(req.body);
            let Email_Validation = await CommonController.Common_Email_Validation(req.body.Buyer_Email);
            let ValidityStatus = await CommonController.Common_Date_Validation(req.body.DOB);
            let Result = await UserController.Buyer_Register(req.body);
            res.json(Result);
            //let Buyer_Network_Processing = await ShopController.Buyer_Network_Processing(req.body, BuyerData);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        
        if (!res.headersSent) {
            res.json(error);
        }
    }
};

UserMediator.Validate_Buyer_OTP = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.Buyer_CountryCode != null && req.body.Buyer_CountryCode != ''
            && req.body.Buyer_PhoneNumber != null && req.body.Buyer_PhoneNumber != ''
            && req.body.OTP != null && isFinite(req.body.OTP)
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let ValidityStatus = await UserController.Check_for_User_OTP_Tries_Count(req.body);
            ValidityStatus = await UserController.Validate_Buyer_OTP(req.body);
            let Result = await UserController.Find_or_Create_Buyer_Information(req.body, DeviceData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

UserMediator.Generate_Buyer_OTP = async (req, res) => {
    try {
        if (
            req.body.ApiKey != null
            && req.body.Buyer_CountryCode != null && req.body.Buyer_CountryCode != ''
            && req.body.Buyer_PhoneNumber != null && req.body.Buyer_PhoneNumber != ''
        ) {
            let DeviceData = await CommonController.Check_for_Api_Key(req.body);
            let ValidityStatus = await UserController.Check_for_OTP_Count(req.body);
            let Result = await UserController.Generate_Buyer_OTP_Send_Message(req.body);

            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
};


export default UserMediator;