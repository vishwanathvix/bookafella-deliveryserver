let StoreMediator = function () { };
// import logger from '../core/logger/logger';
import ApiMessages from '../models/ApiMessages';
import ApiResponce from "../models/Apiresponce.js";
import Admin_Images from '../models/Admin_Images';
import StoreController from '../controllers/StoreController';
import AdminLogController from '../controllers/AdminLogController';
// import config from '../core/config/config';
import Store_Entity from '../models/Store_Entity';
import Store_Branch from '../models/Store_Branch';
import Customers from '../models/Customers';
import Counters from '../models/Counters';
import Validator from "validator";
let EmailOptions = { allow_display_name: false, allow_utf8_local_part: false, require_tld: true };
var msg91mod = require('../controllers/msg91mod.js'); // Setting the Path for Message91 Modules
var MSG91MOD = new msg91mod();

//Dependencies
import uuid from "uuid";
import async from "async";
import sync from "sync";
import moment from "moment";
import crypto from "crypto";
import rand from "csprng";
import Jimp from "jimp";
import formidable from "formidable";
import fs, { Stats } from "fs";
import path from "path";
import os from "os";
import { isBoolean } from 'node-boolify';
import CommonController from '../controllers/CommonController';
import AdminController from '../controllers/AdminController';
import NotificationController from '../controllers/NotificationController';

StoreMediator.List_All_Branch_Notifications = async (req, res) => {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.skip != null && !isNaN(req.body.skip) && req.body.BranchID != null
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.List_All_Branch_Notifications(req.body).then((Result) => {
                                    res.json(Result);
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.List_All_Menu = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.skip != null && !isNaN(req.body.skip) && req.body.BranchID != null
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.List_ALL_Menu(req.body).then((Result) => {
                                    res.json(Result);
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Create_Menu = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_URL != null
        && req.body.MENU_NAME != null && req.body.MENU_NAME != ""
        && req.body.MENU_DESCRIPTION != null && req.body.MENU_DESCRIPTION != ""
        && req.body.MENU_TYPE != null && !isNaN(req.body.MENU_TYPE)
        && req.body.MENU_PRICE != null && !isNaN(req.body.MENU_PRICE)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Get_Store_Menu_Data_By_URL(req.body.MENU_URL, BranchData.BranchID, req.body.MENU_ID, (err, MasterData) => {
                                    if (err) {
                                        res.send(MasterData);
                                    } else {
                                        StoreController.Create_MENU(req.body).then((Result) => {
                                            res.json(Result);
                                        }).catch(err => res.json(err));
                                    }
                                });
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Create_MENU_SECTION = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.SECTION_NAME != null && req.body.SECTION_NAME != ""
        && req.body.SECTION_DESCRIPTION != null && req.body.SECTION_DESCRIPTION != ""
        && req.body.Sl_NO != null && req.body.Sl_NO != ""
        && req.body.MAX_SELECTION != null && !isNaN(req.body.MAX_SELECTION)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.Create_MENU_SECTION(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


StoreMediator.List_ALL_Menu_Sections = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.skip != null && !isNaN(req.body.skip)
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.List_ALL_Menu_Sections(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Add_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.Product_Name != null && req.body.Product_Name != ""
                                    && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                                    && req.body.Product_Edition != null && req.body.Product_Edition != ''
                                    && req.body.Product_Published_On != null && req.body.Product_Published_On != ''
                                    && req.body.Product_Publisher != null && req.body.Product_Publisher != ''
                                    && req.body.Review != null && req.body.About_Author != null
                                    && req.body.Authors != null && req.body.Length != null
                                    && req.body.Total_Pages != null && isFinite(req.body.Total_Pages)
                                    && req.body.ImageID != null && req.body.ImageID != ''
                                    && req.body.Height != null && req.body.Width != null
                                    && req.body.Price != null && req.body.Price != ''
                                    && req.body.Discount_Percent != null && req.body.Discount_Percent != ''
                                    && req.body.Product_ISBN != null
                                    && req.body.Condition != null
                                    && req.body.CategoryID != null
                                    //&& req.body.Admin_Share_Percent != null
                                ) {
                                    //Store Product Information
                                    StoreController.Store_Product_Information(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Add_Store_Product_Existing = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.ProductID != null && req.body.ProductID != ""
                                    && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                                    && req.body.Price != null && req.body.Price != ''
                                    && req.body.Discount_Percent != null && req.body.Discount_Percent != ''
                                    && req.body.Condition != null
                                    //&& req.body.Admin_Share_Percent != null
                                ) {

                                    //Store Product Information
                                    StoreController.Store_Product_Information_Existing(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Find_All_Store_Products = function (req, res) {
    if (req.body.SessionID != null && req.body.skip != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.json(SessionStatus)
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.json(StoreAdminData);
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {

                                res.json(BranchData);
                            } else {
                                StoreController.Find_All_Store_Products(req.body, function (Result) {
                                    res.json(Result)
                                })
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Find_All_Products = function (req, res) {
    if (req.body.SessionID != null && req.body.skip != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.json(SessionStatus)
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.json(StoreAdminData);
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {

                                res.json(BranchData);
                            } else {
                                StoreController.Find_All_Products(req.body, function (Result) {
                                    res.json(Result)
                                })
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Find_All_Products_Lite = function (req, res) {
    if (req.body.SessionID != null
        && req.body.StoreAdminID != null && req.body.BranchID != null
        && req.body.Search != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.json(SessionStatus)
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.json(StoreAdminData);
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {

                                res.json(BranchData);
                            } else {
                                StoreController.Find_All_Products_Lite(req.body, function (Result) {
                                    res.json(Result)
                                })
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Get_Single_Product_Details = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null
        && req.body.ProductID != null && req.body.ProductID != '') {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.json(SessionStatus)
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.json(StoreAdminData);
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {

                                res.json(BranchData);
                            } else {
                                StoreController.Get_Single_Product_Details(req.body).then((Result) => {
                                    res.json(Result)
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Update_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.ProductID != null && req.body.ProductID != ""
                                    && req.body.Avaiable_Quantity != null && isFinite(req.body.Avaiable_Quantity)
                                    && req.body.Price != null && req.body.Price != ''
                                    && req.body.Discount_Percent != null && req.body.Discount_Percent != ''
                                    && req.body.Condition != null
                                    //&& req.body.Admin_Share_Percent != null
                                ) {
                                    //Store Product Information
                                    StoreController.Update_Store_Product(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Delete_Store_Product = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.ProductID != null && req.body.ProductID != "") {
                                    //Store Product Information
                                    StoreController.Delete_Store_Product(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.List_All_Notified_Cancelled_Orders = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.skip != null && isFinite(req.body.skip)
                                    && req.body.limit != null && isFinite(req.body.limit)
                                    && req.body.Whether_Cancellation_Notified_Accepted != null && isBoolean(req.body.Whether_Cancellation_Notified_Accepted)) {
                                    //Store Product Information
                                    StoreController.List_All_Notified_Cancelled_Orders(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.List_Branch_Cancelled_Orders = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.Start_Date != null && req.body.Start_Date != ''
                                    && req.body.End_Date != null && req.body.End_Date != ''
                                    && req.body.skip != null && isFinite(req.body.skip)
                                    && req.body.limit != null && isFinite(req.body.limit)
                                ) {
                                    //Store Product Information
                                    StoreController.List_Branch_Cancelled_Orders(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.List_Branch_Orders = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.Start_Date != null && req.body.Start_Date != ''
                                    && req.body.End_Date != null && req.body.End_Date != ''
                                    && req.body.skip != null && isFinite(req.body.skip)
                                    && req.body.limit != null && isFinite(req.body.limit)
                                    && req.body.Status != null && isBoolean(req.body.Status)) {
                                    //Store Product Information
                                    StoreController.List_Branch_Orders(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Cancel_Order_Request = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.OrderID != null && req.body.OrderID != ''
                                    && req.body.Cancel_Reason != null && req.body.Cancel_Reason != ""
                                ) {
                                    //Store Product Information
                                    StoreController.Cancel_Order_Request(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Accept_Notified_Cancelled_Order = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.OrderID != null && req.body.OrderID != '') {
                                    //Store Product Information
                                    StoreController.Accept_Notified_Cancelled_Order(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Accept_Orders = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.OrderID != null && req.body.OrderID != '') {
                                    //Store Product Information
                                    StoreController.Accept_Orders(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Get_Single_Orders = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.OrderID != null && req.body.OrderID != '') {
                                    //Store Product Information
                                    StoreController.Get_Single_Orders(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Validate_Driver_OTP_For_Pickup = function (req, res) {
    if (req.body.SessionID != null && req.body.StoreAdminID != null && req.body.BranchID != null) {
        StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
            if (err) {
                res.send(JSON.stringify(SessionStatus))
            } else {
                StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
                    if (err) {
                        res.send(JSON.stringify(StoreAdminData));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                if (req.body.OrderID != null && req.body.OrderID != ''
                                    && req.body.OTP != null && isFinite(req.body.OTP)) {
                                    //Store Product Information
                                    StoreController.Validate_Driver_OTP_For_Pickup(req.body, BranchData).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                } else {

                                    res.send(new ApiResponce({
                                        success: false,
                                        extras: {
                                            msg: ApiMessages.ENTER_ALL_TAGS
                                        }
                                    }));
                                }
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send(new ApiResponce({
            success: false,
            extras: {
                msg: ApiMessages.ENTER_ALL_TAGS
            }
        }));
    }
};

StoreMediator.Link_MENU_SECTION_PRODUCT = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.SECTION_ID != null && req.body.ProductID != null && req.body.Sl_NO != null
        && req.body.Default != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                            StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                    StoreController.Check_for_STORE_PRODUCT(req.body).then((PRODUCT_Data) => {
                                        StoreController.Check_Whether_Store_Section_Product_Already_Linked(req.body).then((ValidatityStatus) => {
                                            StoreController.Link_MENU_SECTION_PRODUCT(req.body).then((Result) => {
                                                res.json(Result);
                                            }).catch(err => res.json(err));
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }).catch(err => res.json(err));
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.List_ALL_Section_Products = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.SECTION_ID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                            StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                    StoreController.List_ALL_Section_Products(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }).catch(err => res.json(err));
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Update_MENU = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.MENU_NAME != null && req.body.MENU_NAME != "" && req.body.MENU_URL != null
        && req.body.MENU_DESCRIPTION != null && req.body.MENU_DESCRIPTION != ""
        && req.body.MENU_TYPE != null && !isNaN(req.body.MENU_TYPE)
        && req.body.MENU_PRICE != null && !isNaN(req.body.MENU_PRICE)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Get_Store_Menu_Data_By_URL(req.body.MENU_URL, BranchData.BranchID, req.body.MENU_ID, (err, MasterData) => {
                                    if (err) {
                                        res.send(MasterData);
                                    } else {

                                        StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                            StoreController.Update_MENU(req.body).then((Result) => {
                                                res.json(Result);
                                            }).catch(err => res.json(err));
                                        }).catch(err => res.json(err));
                                    }
                                });

                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


StoreMediator.INACTIVATE_STORE_MENU = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.INACTIVATE_STORE_MENU(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.ACTIVATE_STORE_MENU = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.ACTIVATE_STORE_MENU(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.List_ALL_Inactive_Menu = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.skip != null && !isNaN(req.body.skip)
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.List_ALL_Inactive_Menu(req.body).then((Result) => {
                                    res.json(Result);
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.UPDATE_MENU_SECTION = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null && req.body.SECTION_ID != null
        && req.body.SECTION_NAME != null && req.body.SECTION_NAME != ""
        && req.body.SECTION_DESCRIPTION != null && req.body.SECTION_DESCRIPTION != ""
        && req.body.Sl_NO != null && req.body.Sl_NO != ""
        && req.body.MAX_SELECTION != null && !isNaN(req.body.MAX_SELECTION)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                        StoreController.UPDATE_MENU_SECTION(req.body).then((Result) => {
                                            res.json(Result);
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.INACTIVATE_MENU_SECTION = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null && req.body.SECTION_ID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                        StoreController.INACTIVATE_MENU_SECTION(req.body).then((Result) => {
                                            res.json(Result);
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


StoreMediator.ACTIVATE_MENU_SECTION = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null && req.body.SECTION_ID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                        StoreController.ACTIVATE_MENU_SECTION(req.body).then((Result) => {
                                            res.json(Result);
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


StoreMediator.List_ALL_Inactive_Menu_Sections = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.skip != null && !isNaN(req.body.skip)
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                    StoreController.List_ALL_Inactive_Menu_Sections(req.body).then((Result) => {
                                        res.json(Result);
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Search_All_Store_Branch_Products = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        && req.body.SearchValue != null && req.body.SearchValue != ''
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                            StoreController.Search_All_Store_Branch_Products(req.body).then((Result) => {
                                res.json(Result);
                            }).catch(err => res.json(err));
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};


StoreMediator.Unlink_MENU_SECTION_PRODUCT = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_ID != null
        && req.body.SECTION_ID != null && req.body.ProductID != null
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                            StoreController.Check_for_STORE_MENU(req.body).then((MENU_Data) => {
                                StoreController.Check_for_STORE_SECTION(req.body).then((SECTION_Data) => {
                                    StoreController.Check_for_STORE_PRODUCT(req.body).then((PRODUCT_Data) => {
                                        StoreController.Check_Whether_Store_Section_Product_Linked(req.body).then((SECTION_PRODUCT_DATA) => {
                                            StoreController.Unlink_MENU_SECTION_PRODUCT(req.body).then((Result) => {
                                                res.json(Result);
                                            }).catch(err => res.json(err));
                                        }).catch(err => res.json(err));
                                    }).catch(err => res.json(err));
                                }).catch(err => res.json(err));
                            }).catch(err => res.json(err));
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Menu_URL_Validation = (req, res) => {
    if (req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.BranchID != null && req.body.MENU_URL != null && req.body.MENU_ID != null) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_for_STORE_BRANCH(req.body).then((BranchData) => {
                            StoreController.Get_Store_Menu_Data_By_URL(req.body.MENU_URL, req.body.BranchID, req.body.MENU_ID, (err, MasterData) => {
                                if (err) {
                                    res.send(MasterData);
                                } else {
                                    res.send(MasterData);
                                }
                            })
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
}

StoreMediator.StoreAdmin_Branches = (req, res) => {
    if (req.body.StoreAdminID != null && req.body.SessionID != null) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Get_Store_Admin_Branch_Data(req.body.StoreAdminID, (BranchData) => {
                            res.send(BranchData);
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
}

StoreMediator.Store_Admin_Password_Change = (req, res) => {
    if (req.body.StoreAdminID != null && req.body.SessionID != null && req.body.Old_Password != null && req.body.New_Password != null) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Update_Password(req.body).then((Result) => {
                            res.send(Result);
                        }).catch(err => res.json(err));
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
}


StoreMediator.List_Menu = function (req, res) {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null
        && req.body.skip != null && !isNaN(req.body.skip) && req.body.BranchID != null
        && req.body.limit != null && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.CheckSessionID(req.body, function (err, SessionStatus) {
                    if (err) {
                        res.send(JSON.stringify(SessionStatus));
                    } else {
                        StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                            if (err) {
                                res.send(JSON.stringify(BranchData));
                            } else {
                                StoreController.List_Menu(req.body).then((Result) => {
                                    res.json(Result);
                                }).catch(err => res.json(err));
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } });
    }
};

StoreMediator.Suggeste_Serial_Number = (req, res) => {
    if (req.body.StoreAdminID != null && req.body.SessionID != null && req.body.SECTION_ID != null) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.Suggeste_Serial_Number(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err))
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

StoreMediator.Sc_Suggeste_Serial_Number = (req, res) => {
    if (req.body.StoreAdminID != null && req.body.SessionID != null && req.body.MENU_ID != null) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.Sc_Suggeste_Serial_Number(req.body).then((Result) => {
                    res.json(Result);
                }).catch(err => res.json(err))
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

StoreMediator.Check_Serial_Number_Available = (req, res) => {
    //console.log(req)
    if (req.body.StoreAdminID != null && req.body.SessionID != null && req.body.Sl_NO) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.Check_Serial_Number_Available(req.body).then((Result) => {
                    res.json(Result);
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

StoreMediator.Sc_Check_Serial_Number_Available = (req, res) => {
    //console.log(req)
    if (req.body.StoreAdminID != null && req.body.SessionID != null && req.body.Sl_NO != null && req.body.MENU_ID) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.Sc_Check_Serial_Number_Available(req.body).then((Result) => {
                    res.json(Result);
                })
            }
        })
    } else {
        res.send({ success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } })
    }
}

StoreMediator.Create_Order = (req, res) => {

}

StoreMediator.List_All_Orders = async (req, res) => {
    if (
        req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
        && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
    ) {
        StoreController.Check_for_Store_Admin(req.body, function (err, StoreAdminData) {
            if (err) {
                res.send(JSON.stringify(StoreAdminData));
            } else {
                StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                    StoreController.List_All_Orders(req.body).then((Result) => {
                        res.json(Result);
                    })
                })
            }
        })
    } else {
        throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
    }

}

StoreMediator.Add_Branch_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.Whether_Default_Account != null && isBoolean(req.body.Whether_Default_Account)
            && req.body.Name != null && req.body.Name != ''
            && req.body.Account_Number != null && isFinite(req.body.Account_Number) && !isNaN(req.body.Account_Number)
            && req.body.IFSC != null && req.body.IFSC != ''
        ) {
            req.body = JSON.parse(JSON.stringify(req.body));
            req.body.IFSC = req.body.IFSC.toUpperCase();
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let BankData = await CommonController.Validate_IFSC_Code(req.body);
            let ValidityStatus = await StoreController.Validate_Branch_Beneficiary_Account_For_Bank_Account_Number_Already_Exist(req.body);
            let Result = await StoreController.Add_Branch_Beneficiary_Account_For_Bank_Account(req.body, BankData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.List_All_Branch_Beneficiary_Accounts = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.List_All_Branch_Beneficiary_Accounts(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Make_Default_Branch_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.BeneficiaryID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let BeneficiaryData = await CommonController.Check_for_Branch_Bank_Beneficiary_Account(req.body);
            let Result = await StoreController.Make_Default_Branch_Beneficiary_Account_For_Bank_Account(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Update_Branch_Beneficiary_Account_For_Bank_Account = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.BeneficiaryID != null
            && req.body.Name != null && req.body.Name != ''
            && req.body.Account_Number != null && isFinite(req.body.Account_Number) && !isNaN(req.body.Account_Number)
            && req.body.IFSC != null && req.body.IFSC != ''
        ) {
            req.body = JSON.parse(JSON.stringify(req.body));
            req.body.IFSC = req.body.IFSC.toUpperCase();
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let BeneficiaryData = await CommonController.Check_for_Branch_Bank_Beneficiary_Account(req.body);
            let BankData = await CommonController.Validate_IFSC_Code(req.body);
            let ValidityStatus = await StoreController.Validate_Branch_Beneficiary_Account_For_Bank_Account_Number_Already_Exist(req.body);
            let Result = await StoreController.Update_Branch_Beneficiary_Account_For_Bank_Account(req.body, BankData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Delete_Branch_Beneficiary_Account = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.BeneficiaryID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let BeneficiaryData = await CommonController.Check_for_Branch_Bank_Beneficiary_Account(req.body);
            if (BeneficiaryData.Whether_Default_Account) {
                throw { success: false, extras: { msg: ApiMessages.CANNOT_REMOVE_DEFAULT_BENEFICIARY_ACCOUNT } };
            } else {
                let Result = await StoreController.Inactivate_Branch_Beneficiary_Account_For_Bank_Account(req.body);
                res.json(Result);
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Branch_Withdraw_Amount = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.BeneficiaryID != null
            && req.body.Amount != null && isFinite(req.body.Amount) && !isNaN(req.body.Amount)
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let BeneficiaryData = await CommonController.Check_for_Branch_Bank_Beneficiary_Account(req.body);
            if (BeneficiaryData.Status) {
                let Result = await StoreController.Branch_Withdraw_Amount(req.body, BeneficiaryData);
                res.json(Result);
            } else {
                throw { success: false, extras: { msg: ApiMessages.ACCOUNT_NOT_ACTIVE } };
            }
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.List_All_Categories = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.List_All_Categories(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Branch_Update_Online_Offline = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.Branch_Update_Online_Offline(req.body, StoreAdminData);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.List_Branch_Stack_Log = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.skip != null && isFinite(req.body.skip) && !isNaN(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit) && !isNaN(req.body.limit)
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body)
            let Result = await AdminController.List_Branch_Stack_Log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Fullfill_Stack_Quantity = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.StackID != null && req.body.StackID != ''
            && req.body.Fullfilled_Quantity != null && isFinite(req.body.Fullfilled_Quantity)
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body)
            let Result = await StoreController.Fullfill_Stack_Quantity(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Update_Stack_ETA = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.StackID != null && req.body.StackID != ''
            && req.body.Branch_ETA_Description != null && req.body.Branch_ETA_Description != ''
            && req.body.Branch_ETA != null && isFinite(req.body.Branch_ETA)
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body)
            let Result = await StoreController.Update_Stack_ETA(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.Get_Branch_Wallet = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        ) {
            let StoreAdminData = await CommonController.Check_for_Store_Admin(req.body)
            let Result = await StoreController.Get_Branch_Wallet(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(await CommonController.Common_Error_Handler(error));
        }
    }
}

StoreMediator.List_Branch_Wallet_Log = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.List_Branch_Wallet_Log(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

StoreMediator.List_All_Branch_Reorder = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.skip != null && isFinite(req.body.skip)
            && req.body.limit != null && isFinite(req.body.limit)
        ) {
            let AdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.List_All_Branch_Reorder(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

StoreMediator.Update_Branch_Product_Reorder = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.Availablity != null && isFinite(req.body.Availablity)
            && req.body.Quantity_Available != null && isFinite(req.body.Quantity_Available)
            && req.body.ProductID != null && req.body.ProductID != ''
            && req.body.ReorderLogID != null && req.body.ReorderLogID != ''
        ) {
            let AdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.Update_Branch_Product_Reorder(req.body);
            res.json(Result);
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

StoreMediator.Get_Branch_Data = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
        ) {
            let AdminData = await CommonController.Check_for_Store_Admin(req.body);
            StoreController.Check_For_Store_BranchID(req.body, function (err, BranchData) {
                res.json({ success: true, extras: { Data: BranchData } });
            })
        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}

StoreMediator.Branch_Password_Change = async (req, res) => {
    try {
        if (
            req.body.StoreAdminID != null && req.body.SessionID != null && req.body.BranchID != null
            && req.body.New_Password != null && req.body.Old_Password != null
        ) {
            let AdminData = await CommonController.Check_for_Store_Admin(req.body);
            let Result = await StoreController.Branch_Password_Change(req.body);
            res.json(Result);

        } else {
            throw { success: false, extras: { msg: ApiMessages.ENTER_ALL_TAGS } };
        }
    } catch (error) {
        // console.log(error)
        if (!res.headersSent) {
            res.json(error);
        }
    }
}
export default StoreMediator;